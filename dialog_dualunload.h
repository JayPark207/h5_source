#ifndef DIALOG_DUALUNLOAD_H
#define DIALOG_DUALUNLOAD_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_mode_select.h"

#define DU_PLUS     0
#define DU_MINUS    1

namespace Ui {
class dialog_dualunload;
}

class dialog_dualunload : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_dualunload(QWidget *parent = 0);
    ~dialog_dualunload();

    void Update();


private:
    Ui::dialog_dualunload *ui;

    QVector<QLabel4*> m_vtXPic, m_vtYPic, m_vtZPic;
    QVector<QLabel3*> m_vtX, m_vtY, m_vtZ;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;
    QStringList m_ItemName;

    void Display_X(float data);
    void Display_Y(float data);
    void Display_Z(float data);

    bool bSave;

public slots:
    void onClose();

    void onUse();
    void onX();
    void onY();
    void onZ();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_DUALUNLOAD_H
