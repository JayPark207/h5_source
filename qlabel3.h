#ifndef QLABEL3
#define QLABEL3

#include <QLabel>
#include <QMouseEvent>
#include <QDebug>

#include "global.h"

// 이미지변환없는라벨.
class QLabel3 : public QLabel
{
    Q_OBJECT
public:
    explicit QLabel3(QWidget *parent=0):QLabel(parent){}

signals:
    void mouse_press();
    void mouse_release();

public slots:

protected:
    void mousePressEvent(QMouseEvent *)
    {
        gLabelWidget = (QWidget*)this;
        emit mouse_press();
    }

    void mouseReleaseEvent(QMouseEvent *event)
    {
        gLabelReleasePoint = event->pos();

        emit mouse_release();

        gLabelWidget = 0;
        gLabelReleasePoint.setX(0);
        gLabelReleasePoint.setY(0);
    }

};

#endif // QLABEL3

