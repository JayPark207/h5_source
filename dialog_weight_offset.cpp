#include "dialog_weight_offset.h"
#include "ui_dialog_weight_offset.h"

dialog_weight_offset::dialog_weight_offset(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_weight_offset)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtVal.clear();            m_vtValPic.clear();               m_vtTitle.clear();
    m_vtVal.append(ui->tbVal);  m_vtValPic.append(ui->tbValPic);  m_vtTitle.append(ui->tbTitle);
    m_vtVal.append(ui->tbVal_2);m_vtValPic.append(ui->tbValPic_2);m_vtTitle.append(ui->tbTitle_2);
    m_vtVal.append(ui->tbVal_3);m_vtValPic.append(ui->tbValPic_3);m_vtTitle.append(ui->tbTitle_3);
    m_vtVal.append(ui->tbVal_4);m_vtValPic.append(ui->tbValPic_4);m_vtTitle.append(ui->tbTitle_4);

    int i;
    for(i=0;i<m_vtValPic.count();i++)
    {
        connect(m_vtValPic[i],SIGNAL(mouse_release()),this,SLOT(onOffset()));
        connect(m_vtVal[i],SIGNAL(mouse_press()),m_vtValPic[i],SLOT(press()));
        connect(m_vtVal[i],SIGNAL(mouse_release()),m_vtValPic[i],SLOT(release()));
    }


}

dialog_weight_offset::~dialog_weight_offset()
{
    delete ui;
}
void dialog_weight_offset::showEvent(QShowEvent *)
{
    Update();
    m_bSave = false;
}
void dialog_weight_offset::hideEvent(QHideEvent *)
{

}
void dialog_weight_offset::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_weight_offset::onClose()
{
    if(m_bSave)
        Recipe->SaveVariable();

    emit accept();
}

void dialog_weight_offset::Update()
{
    vars.clear();
    vars.append(HyRecipe::vWeightOffset1);
    vars.append(HyRecipe::vWeightOffset2);
    vars.append(HyRecipe::vWeightOffset3);
    vars.append(HyRecipe::vWeightOffset4);

    datas.clear();

    QString str;
    if(Recipe->Gets(vars, datas))
    {
        for(int i=0;i<vars.count();i++)
        {
            str.sprintf("%.1f", datas[i]);
            m_vtVal[i]->setText(str);
        }
    }
}

void dialog_weight_offset::onOffset()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtValPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-99999.9);
    np->m_numpad->SetMaxValue(99999.9);
    np->m_numpad->SetSosuNum(1);
    np->m_numpad->SetTitle(m_vtTitle[index]->text());
    np->m_numpad->SetNum((double)datas[index]);
    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(vars[index],(float)np->m_numpad->GetNumDouble(), false))
        {
            m_bSave = true;
            Update();
        }
    }
}
