#ifndef DIALOG_TP_IPGW_H
#define DIALOG_TP_IPGW_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"
#include "dialog_numpad.h"
#include "dialog_confirm.h"
#include "dialog_message.h"


namespace Ui {
class dialog_tp_ipgw;
}

class dialog_tp_ipgw : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_tp_ipgw(QWidget *parent = 0);
    ~dialog_tp_ipgw();

    void Update();

private:
    Ui::dialog_tp_ipgw *ui;

    QVector<QLabel4*> m_vtIpPic;
    QVector<QLabel3*> m_vtIp, m_vtGw;

    QString m_Ip, m_Gw;

    void Display_Ip(QString ip);
    void Display_Gw(QString gw);
    void Display_Gw();

    QString Make_Ip(int ip1, int ip2, int ip3, int ip4);
    bool Save();
    void Enable_Save(bool enable);
    bool IsSame(QString ip);

public slots:
    void onOK();
    void onCancel();

    void onIp();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_TP_IPGW_H
