#ifndef DIALOG_BACKUP_H
#define DIALOG_BACKUP_H

#include <QDialog>
#include <QFrame>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "widget_backup.h"
#include "widget_backup_recovery.h"
#include "widget_backup_recovery2.h"


namespace Ui {
class dialog_backup;
}

class dialog_backup : public QDialog
{
    Q_OBJECT

private:
    enum ENUM_PAGE
    {
        PAGE_BACKUP=0,
        PAGE_RECOVERY,
        PAGE_RECOVERY2,
    };

public:
    explicit dialog_backup(QWidget *parent = 0);
    ~dialog_backup();

private slots:
    void onTab();
    void onTimer();

    void onRecoveryBack();
    void onRecoverySelect();

private:

    void Init();
    void ChangePage(ENUM_PAGE page);
    void Display_USB(bool bExist);

private:
    Ui::dialog_backup *ui;
    QStackedWidget* Stacked;

    widget_backup*              wigBackup;
    widget_backup_recovery*     wigRecovery;
    widget_backup_recovery2*    wigRecovery2;

    QTimer* timer;

    int m_nRefreshCount;
    bool m_bRefreshCheck;
    bool m_bNowCheck;



protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_BACKUP_H
