#include "dialog_tp_ipgw.h"
#include "ui_dialog_tp_ipgw.h"

dialog_tp_ipgw::dialog_tp_ipgw(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_tp_ipgw)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtIpPic.clear();               m_vtIp.clear();
    m_vtIpPic.append(ui->btnIpPic);  m_vtIp.append(ui->btnIp);
    m_vtIpPic.append(ui->btnIpPic_2);m_vtIp.append(ui->btnIp_2);
    m_vtIpPic.append(ui->btnIpPic_3);m_vtIp.append(ui->btnIp_3);
    m_vtIpPic.append(ui->btnIpPic_4);m_vtIp.append(ui->btnIp_4);

    int i;
    for(i=0;i<m_vtIpPic.count();i++)
    {
        connect(m_vtIpPic[i],SIGNAL(mouse_release()),this,SLOT(onIp()));
        connect(m_vtIp[i],SIGNAL(mouse_press()),m_vtIpPic[i],SLOT(press()));
        connect(m_vtIp[i],SIGNAL(mouse_release()),m_vtIpPic[i],SLOT(release()));
    }

    m_vtGw.clear();
    m_vtGw.append(ui->lbGw);
    m_vtGw.append(ui->lbGw_2);
    m_vtGw.append(ui->lbGw_3);
    m_vtGw.append(ui->lbGw_4);



}

dialog_tp_ipgw::~dialog_tp_ipgw()
{
    delete ui;
}
void dialog_tp_ipgw::showEvent(QShowEvent *)
{
    Update();
}
void dialog_tp_ipgw::hideEvent(QHideEvent *)
{

}
void dialog_tp_ipgw::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_tp_ipgw::onOK()
{
    // save & confirm reboot
    CNRobo* pCon = CNRobo::getInstance();

    if(Save())
    {
        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::GREEN);
        conf->Title(tr("Confirm"));
        conf->Message(tr("Do you want to System Rebooting?"),
                      tr("OK : now reboot, Cancel : later reboot"));
        if(conf->exec() == QDialog::Accepted)
            pCon->rebootSystem();

    }
    else
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail to ip & gateway setting!!"),
                     tr("Please try again."));
        msg->exec();
    }

    emit accept();
}
void dialog_tp_ipgw::onCancel()
{
    emit reject();
}

void dialog_tp_ipgw::Update()
{
    QString ip, gw;
    if(!Network->Get_IP(ip, gw))
    {
        ip = "0.0.0.0";
        gw = "0.0.0.0";
    }
    m_Ip = ip;
    m_Gw = gw;

    Display_Ip(m_Ip);
    Display_Gw(m_Gw);

    Enable_Save(!IsSame(m_Ip));
}

void dialog_tp_ipgw::Enable_Save(bool enable)
{
    ui->grpYes->setEnabled(enable);
}

void dialog_tp_ipgw::Display_Ip(QString ip)
{
    QStringList ip_list;

    ip_list.clear();
    ip_list = ip.split(".");
    if(ip_list.size() != 4)
        return;
    if(ip_list.size() != m_vtIp.size())
        return;

    for(int i=0;i<m_vtIp.size();i++)
    {
        m_vtIp[i]->setText(ip_list[i]);
    }
}

void dialog_tp_ipgw::Display_Gw(QString gw)
{
    QStringList gw_list;

    gw_list.clear();
    gw_list = gw.split(".");
    if(gw_list.size() != 4)
        return;
    if(gw_list.size() != m_vtGw.size())
        return;

    for(int i=0;i<m_vtGw.size();i++)
    {
        m_vtGw[i]->setText(gw_list[i]);
    }
}

bool dialog_tp_ipgw::IsSame(QString ip) // data vs display
{
    int temp[4] = {0,0,0,0};
    for(int i=0;i<4;i++)
        temp[i] = m_vtIp[i]->text().toInt();

    QString str;
    str = Make_Ip(temp[0],temp[1],temp[2],temp[3]);

    return (ip == str);
}

QString dialog_tp_ipgw::Make_Ip(int ip1, int ip2, int ip3, int ip4)
{
    QStringList temp;

    temp.clear();
    temp.append(QString().setNum(ip1));
    temp.append(QString().setNum(ip2));
    temp.append(QString().setNum(ip3));
    temp.append(QString().setNum(ip4));

    return temp.join(".");
}

void dialog_tp_ipgw::Display_Gw()
{
    for(int i=0;i<m_vtIp.count();i++)
    {
        if(i < 3)
            m_vtGw[i]->setText(m_vtIp[i]->text());
        else
            m_vtGw[i]->setText("1");
    }
}

void dialog_tp_ipgw::onIp()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtIpPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    if(index == 3)
        np->m_numpad->SetMinValue(2);
    else
        np->m_numpad->SetMinValue(0);

    np->m_numpad->SetMaxValue(255);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle(ui->lbIpTitle->text() + QString().setNum(index+1));
    np->m_numpad->SetNum(m_vtIp[index]->text().toDouble());
    if(np->exec() == QDialog::Accepted)
    {
        m_vtIp[index]->setText(QString().setNum((int)np->m_numpad->GetNumDouble()));
        Display_Gw();
        Enable_Save(!IsSame(m_Ip));
    }
}

bool dialog_tp_ipgw::Save()
{
    int temp[4] = {0,0,0,0};
    for(int i=0;i<4;i++)
        temp[i] = m_vtIp[i]->text().toInt();

    QString str;
    str = Make_Ip(temp[0],temp[1],temp[2],temp[3]);

    return(Network->Set_IP(str));
}
