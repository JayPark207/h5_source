#ifndef HYUSESKIP_H
#define HYUSESKIP_H

#include <QObject>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyrecipe.h"
#include "hyparam.h"


/** 기능의_사용여부를_설정한다. 유저파라미터저장소와 레시피양쪽에_세트로저장하
 *  유저파라미터는_데이터의저장역할.
 *  레시피는_시퀀스와의데이터공유역할.
 *  예) UI내부버튼비프음사용...
 * **/
class HyUseSkip : public QObject
{
    Q_OBJECT

public:
    enum ENUM_USESKIP
    {
        BTN_BEEP=0,
        CAPTURE,
        TOOL_COORD,
        SOFTSENSOR_MON,
        USESKIP_5,  // temp
        USESKIP_6,  // temp
        USESKIP_7,
        USESKIP_8,

        USESKIP_9,
        USESKIP_10,
        USESKIP_11,
        USESKIP_12,
        USESKIP_13,
        USESKIP_14,
        USESKIP_15,
        USESKIP_16,

        USESKIP_NUM,
    };

public:
    explicit HyUseSkip(HyParam* param, HyRecipe* recipe);

    void    Init();

    bool    Read(); // read param -> make data.
    bool    Write(bool save = true); // write param & recipe.
    bool    Sync(bool save = true); // Read() & Write()

    int     Count();
    bool    Get(ENUM_USESKIP index);
    void    Set(ENUM_USESKIP index, bool bUseSKip);
    QString GetName(ENUM_USESKIP index);


private:
    HyParam*    m_Param;
    HyRecipe*   m_Recipe;

    QString         m_Name[USESKIP_NUM];
    QVector<bool>   m_vtUseSkip;  // real data.

    void    Set_Default_Param();
    QString Get_Param_Format();

};

#endif // HYUSESKIP_H
