#ifndef DIALOG_UNLOAD_METHOD_H
#define DIALOG_UNLOAD_METHOD_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

namespace Ui {
class dialog_unload_method;
}

class dialog_unload_method : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_unload_method(QWidget *parent = 0);
    ~dialog_unload_method();

    void Update();

public slots:
    void onSelect();

private:
    Ui::dialog_unload_method *ui;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<int> outnums;
    QVector<float> datas;

    QVector<QLabel4*> m_vtPic;
    QVector<QLabel3*> m_vtIcon;
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtSel;

    void Display_Check();
    void ChangePic(int index, bool onoff);

    bool m_bSave;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // DIALOG_UNLOAD_METHOD_H
