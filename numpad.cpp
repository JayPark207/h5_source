#include "numpad.h"
#include "ui_numpad.h"

numpad::numpad(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::numpad)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    m_vtNoPic.clear();m_vtNo.clear();
    m_vtNoPic.append(ui->btnNoPic);   m_vtNo.append(ui->btnNo);
    m_vtNoPic.append(ui->btnNoPic_2); m_vtNo.append(ui->btnNo_2);
    m_vtNoPic.append(ui->btnNoPic_3); m_vtNo.append(ui->btnNo_3);
    m_vtNoPic.append(ui->btnNoPic_4); m_vtNo.append(ui->btnNo_4);
    m_vtNoPic.append(ui->btnNoPic_5); m_vtNo.append(ui->btnNo_5);
    m_vtNoPic.append(ui->btnNoPic_6); m_vtNo.append(ui->btnNo_6);
    m_vtNoPic.append(ui->btnNoPic_7); m_vtNo.append(ui->btnNo_7);
    m_vtNoPic.append(ui->btnNoPic_8); m_vtNo.append(ui->btnNo_8);
    m_vtNoPic.append(ui->btnNoPic_9); m_vtNo.append(ui->btnNo_9);
    m_vtNoPic.append(ui->btnNoPic_10);m_vtNo.append(ui->btnNo_10);
    m_vtNoPic.append(ui->btnNoPic_11);m_vtNo.append(ui->btnNo_11);

    int i;
    for(i=0;i<m_vtNoPic.count();i++)
    {
        connect(m_vtNoPic[i],SIGNAL(mouse_release()),this,SLOT(onNumpad()));
        connect(m_vtNo[i],SIGNAL(mouse_press()),m_vtNoPic[i],SLOT(press()));
        connect(m_vtNo[i],SIGNAL(mouse_release()),m_vtNoPic[i],SLOT(release()));
    }

    // ESC
    connect(ui->btnESCPic,SIGNAL(mouse_release()),this,SLOT(onESC()));
    connect(ui->btnESC,SIGNAL(mouse_press()),ui->btnESCPic,SLOT(press()));
    connect(ui->btnESC,SIGNAL(mouse_release()),ui->btnESCPic,SLOT(release()));

    // OK
    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    // PM
    connect(ui->btnPMPic,SIGNAL(mouse_release()),this,SLOT(onPM()));
    connect(ui->btnPM,SIGNAL(mouse_press()),ui->btnPMPic,SLOT(press()));
    connect(ui->btnPM,SIGNAL(mouse_release()),ui->btnPMPic,SLOT(release()));

    // BS
    connect(ui->btnBSPic,SIGNAL(mouse_release()),this,SLOT(onBS()));
    connect(ui->btnBS,SIGNAL(mouse_press()),ui->btnBSPic,SLOT(press()));
    connect(ui->btnBS,SIGNAL(mouse_release()),ui->btnBSPic,SLOT(release()));

    // AC
    connect(ui->btnACPic,SIGNAL(mouse_release()),this,SLOT(onAC()));
    connect(ui->btnAC,SIGNAL(mouse_press()),ui->btnACPic,SLOT(press()));
    connect(ui->btnAC,SIGNAL(mouse_release()),ui->btnACPic,SLOT(release()));

    m_vtSelPic.clear();m_vtSel.clear();
    m_vtSelPic.append(ui->btnSelPic);  m_vtSel.append(ui->btnSel);
    m_vtSelPic.append(ui->btnSelPic_2);m_vtSel.append(ui->btnSel_2);
    m_vtSelPic.append(ui->btnSelPic_3);m_vtSel.append(ui->btnSel_3);
    m_vtSelPic.append(ui->btnSelPic_4);m_vtSel.append(ui->btnSel_4);

    for(int i=0;i<m_vtSelPic.count();i++)
    {
        connect(m_vtSelPic[i],SIGNAL(mouse_release()),this,SLOT(onSelectNum()));
        connect(m_vtSel[i],SIGNAL(mouse_press()),m_vtSelPic[i],SLOT(press()));
        connect(m_vtSel[i],SIGNAL(mouse_release()),m_vtSelPic[i],SLOT(release()));
    }

    // Plus
    connect(ui->btnPlusPic,SIGNAL(mouse_release()),this,SLOT(onPlusMinus()));
    connect(ui->btnPlus,SIGNAL(mouse_press()),ui->btnPlusPic,SLOT(press()));
    connect(ui->btnPlus,SIGNAL(mouse_release()),ui->btnPlusPic,SLOT(release()));

    // Minus
    connect(ui->btnMinusPic,SIGNAL(mouse_release()),this,SLOT(onPlusMinus()));
    connect(ui->btnMinus,SIGNAL(mouse_press()),ui->btnMinusPic,SLOT(press()));
    connect(ui->btnMinus,SIGNAL(mouse_release()),ui->btnMinusPic,SLOT(release()));


    ChangeSelectValue(1);
}

numpad::~numpad()
{
    delete ui;
}
void numpad::showEvent(QShowEvent *)
{
    timer->start();
}
void numpad::hideEvent(QHideEvent *)
{
    timer->stop();
}
void numpad::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void numpad::onTimer()
{
    // min max check.
    bool bMin = CheckMin(m_strOutputNum);
    bool bMax = CheckMax(m_strOutputNum);

    ui->lbNewTitle->setAutoFillBackground(!(bMin && bMax));
    ui->lbMinTitle->setAutoFillBackground(!bMin);
    ui->lbMinVal->setAutoFillBackground(!bMin);
    ui->lbMaxTitle->setAutoFillBackground(!bMax);
    ui->lbMaxVal->setAutoFillBackground(!bMax);
}

void numpad::onNumpad()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtNoPic.indexOf(btn);
    if(index < 0) return;

    QString strTemp = m_vtNo[index]->text();

    int cursorpos = ui->txtView->cursorPosition();

    QString buf = m_strOutputNum;

    m_strOutputNum.insert(cursorpos, strTemp);

    if(CheckValidNum(m_strOutputNum))
    {
        ui->txtView->setText(m_strOutputNum);
        ui->txtView->setCursorPosition(cursorpos+1);
    }
    else
    {
        m_strOutputNum = buf;
        ui->txtView->setText(m_strOutputNum);
        ui->txtView->setCursorPosition(cursorpos);
    }

    ui->txtView->setFocus();
}

bool numpad::CheckValidNum(QString num)
{
    QString check = num;
    bool bOk;
    double dbVal;

    // 정확한 double형 숫자인지 체크.
    dbVal = check.toDouble(&bOk);
    if(!bOk)
    {
        qDebug() << dbVal;
        return false;
    }

    // 소수점 자리수 맞는지 체크
    int ntemp,nleng;
    if(num.indexOf(QChar('.')) != -1)
    {
        if(m_nSosuNum == 0)
            return false;

        ntemp = num.indexOf(QChar('.'));
        nleng = num.length()-1;
        if((nleng - ntemp) > m_nSosuNum)
            return false;
    }

    // 첫 입력이 0인경우.
    if(num[0] == QChar('0') && num.length() > 1)
    {
        if(num[1] != QChar('.'))
        {
            num.remove(0,1);
            m_strOutputNum = num;
        }
    }

    return true;
}

bool numpad::CheckValidNum_PlusMinus(QString num)
{

    if(!CheckValidNum(num))
        return false;
    if(!CheckMin(num))
        return false;
    if(!CheckMax(num))
        return false;

    return true;
}

bool numpad::CheckMin(QString num)
{
    // min max 값안에 있는지 체크.
    if(num.toDouble() < m_MinVal.toDouble())
        return false;
    return true;
}
bool numpad::CheckMax(QString num)
{
    if(num.toDouble() > m_MaxVal.toDouble())
        return false;
    return true;
}

bool numpad::IsMin()
{
    return CheckMin(m_strOutputNum);
}
bool numpad::IsMax()
{
    return CheckMax(m_strOutputNum);
}
void numpad::onESC()
{
    emit Cancel();
}
void numpad::onOK()
{
    emit Enter();
}
void numpad::onPM()
{
    // exception
    if(m_strOutputNum == ""
    || m_strOutputNum.toDouble() == 0)
        return;

    if(m_strOutputNum[0] == QChar('-'))
    {
        m_strOutputNum.remove(0,1);
    }
    else
    {
        m_strOutputNum.insert(0, "-");
    }
    ui->txtView->setText(m_strOutputNum);
    ui->txtView->setFocus();
}

void numpad::SetNum(QString num)
{
    m_strOutputNum = num;
    m_strOldNum = num;
    ui->txtView->setText(m_strOutputNum);
    ui->txtView_2->setText(m_strOldNum);
}
void numpad::SetNum(double num)
{
    m_strOutputNum.setNum(num);
    m_strOldNum.setNum(num);
    ui->txtView->setText(m_strOutputNum);
    ui->txtView_2->setText(m_strOldNum);
}

QString numpad::GetNum()
{
    return m_strOutputNum;
}
double numpad::GetNumDouble()
{
    return m_strOutputNum.toDouble();
}

void numpad::SetMask(QString mask)
{
    ui->txtView->setInputMask(mask);
}

void numpad::SetMinValue(double val)
{
    m_MinVal.setNum(val);
    ui->lbMinVal->setText(m_MinVal);
}

void numpad::SetMinValue(QString val)
{
    m_MinVal = val;
    ui->lbMinVal->setText(m_MinVal);
}

void numpad::SetMaxValue(double val)
{
    m_MaxVal.setNum(val);
    ui->lbMaxVal->setText(m_MaxVal);
}

void numpad::SetMaxValue(QString val)
{
    m_MaxVal = val;
    ui->lbMaxVal->setText(m_MaxVal);
}

void numpad::onBS()
{
    int cursorpos = ui->txtView->cursorPosition();

    if(cursorpos <= 0)
    {
        ui->txtView->setCursorPosition(cursorpos);
        ui->txtView->setFocus();
        return;
    }

    m_strOutputNum.remove(cursorpos-1,1);
    ui->txtView->setText(m_strOutputNum);
    ui->txtView->setCursorPosition(cursorpos-1);
}
void numpad::onAC()
{
    m_strOutputNum = "";
    ui->txtView->setText(m_strOutputNum);
    ui->txtView->setCursorPosition(0);
}

void numpad::on_txtView_textChanged(const QString &arg1)
{
    if(m_strOutputNum != arg1)
        ui->txtView->setText(m_strOutputNum);

    ui->txtView->setFocus();
}

void numpad::SetSosuNum(int num)
{
    if(num < 0) num = 0;
    m_nSosuNum = num;
}


void numpad::SetTitle(QString title)
{
    ui->lbTitle->setText(title);
}


void numpad::ChangeSelectValue(int index)
{
    for(int i=0;i<m_vtSelPic.count();i++)
    {
        if(i == index)
        {
            m_vtSelPic[i]->setAutoFillBackground(true);
            m_dbSelectValue = m_vtSel[i]->text().toDouble();
        }
        else
        {
            m_vtSelPic[i]->setAutoFillBackground(false);
        }
    }
}

void numpad::onSelectNum()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtSelPic.indexOf(sel);
    if(index < 0) return;

    ChangeSelectValue(index);
}

void numpad::onPlusMinus()
{
    QString sel = sender()->objectName();
    double prevalue;
    QString strprevalue;

    prevalue = m_strOutputNum.toDouble();
    strprevalue.clear();

    if(sel == ui->btnPlusPic->objectName())
    {
        prevalue += m_dbSelectValue;
        strprevalue.setNum(prevalue);
        if(CheckValidNum_PlusMinus(strprevalue))
            m_strOutputNum = strprevalue;

        ui->txtView->setText(m_strOutputNum);
    }
    else if(sel == ui->btnMinusPic->objectName())
    {
        prevalue -= m_dbSelectValue;
        strprevalue.setNum(prevalue);
        if(CheckValidNum_PlusMinus(strprevalue))
            m_strOutputNum = strprevalue;

        ui->txtView->setText(m_strOutputNum);
    }

    ui->txtView->setCursorPosition(m_strOutputNum.length());
    ui->txtView->setFocus();
}

