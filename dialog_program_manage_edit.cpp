#include "dialog_program_manage_edit.h"
#include "ui_dialog_program_manage_edit.h"

dialog_program_manage_edit::dialog_program_manage_edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_program_manage_edit)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    wig_program = new widget_stepedit_program(ui->widget);

    connect(wig_program,SIGNAL(sigClose()),this,SLOT(accept()));
}

dialog_program_manage_edit::~dialog_program_manage_edit()
{
    delete ui;
}
void dialog_program_manage_edit::showEvent(QShowEvent *)
{
    wig_program->show();
}
void dialog_program_manage_edit::hideEvent(QHideEvent *)
{

}
void dialog_program_manage_edit::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
