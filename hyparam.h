#ifndef HYPARAM_H
#define HYPARAM_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"



class HyParam : public QObject
{
    Q_OBJECT

public:
    enum PARAMS
    {
        LANGUAGE=0,
        RECIPE,
        // add here
        MAIN_SPEED,
        FIRST_SETTING,
        USER_LEVEL,
        LV0_PW,
        LV1_PW,
        LV2_PW,
        LV3_PW,
        LV4_PW,
        LV5_PW,
        LV6_PW,
        LV7_PW,
        LV8_PW,
        LV9_PW,
        START_DATE,
        JIG_POS,
        SAMPLE_POS,
        FAULTY_POS,
        MOTOR_TUNE_DATA,
        USESKIP,
        SOFT_SENSOR_MIN,
        SOFT_SENSOR_MAX,
        VAR_MONITOR_NAME,
        VAR_MONITOR_NAME2,
        RECENTLY_ERROR,
        SUBTASK_AUTORUN,
        SUBTASK_MACRO,

        PARAMS_NUM
    };

    enum PARAM_LANGUAGE
    {
        LANG_EN = 0,    // 영어
        LANG_KO,        // 한글.
        LANG_ZH,        // 중국어.
        LANG_JA,        // 일본어.
        LANG_DE,        // 독일어.
        LANG_RU,        // 러시아어.
        LANG_CS,        // 체코어.
        LANG_FR,        // 불어.어
        LANG_ES,        // 스페인어.
        LANG_IT,        // 이탈리아어.
        LANG_TH         // 태국어
    };

public:
    HyParam();

    void Init_String();

    bool Read();
    bool Write();

    void Set(PARAMS conf, QString data, bool bWrite = true);
    void Set(PARAMS conf, float data, bool bWrite = true);
    void Set(PARAMS conf, int data, bool bWrite = true);
    QString Get(PARAMS conf);
    QString GetName(PARAMS conf);

    QStringList m_Name;
    QStringList m_Param;

    // for common position data.
    void Set(PARAMS conf, cn_trans trans, cn_joint joint,
             float speed, float delay, bool bWrite = true);
    bool Get(PARAMS conf, cn_trans& trans, cn_joint& joint,
             float& speed, float& delay);

    // for motor tune data
    void Set(PARAMS conf, cn_joint joint,
             float speed, float delay, float accel, float decel, bool bWrite = true);
    bool Get(PARAMS conf, cn_joint& joint,
             float& speed, float& delay, float& accel, float& decel);

};


#endif // HYPARAM_H

