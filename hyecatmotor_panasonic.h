#ifndef HYECATMOTOR_PANASONIC_H
#define HYECATMOTOR_PANASONIC_H

#include "hyecatmotor.h"

class HyEcatMotor_Panasonic : public HyEcatMotor
{
public:
    HyEcatMotor_Panasonic();

    /*// for test.
    void Update(int motor_id);
    void SetError(int motor_id, bool on);
    void SetWarn(int motor_id, bool on);
    void Reset(int motor_id);
    */
};

#endif // HYECATMOTOR_PANASONIC_H
