#include "dialog_manual.h"
#include "ui_dialog_manual.h"

dialog_manual::dialog_manual(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_manual)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

#ifdef _H6_
    //wigJog = new widget_jogmodule4_p(ui->grpJog);
    wigJog = new widget_jogmodule4(ui->grpJog);
#else
    wigJog = new widget_jogmodule4(ui->grpJog);
#endif

    // timer for display update.
    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    // close page
    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // Postion
    m_vtAxis.clear();m_vtCurr.clear();
    m_vtAxis.append(ui->tbAxis);  m_vtCurr.append(ui->tbCurr);
    m_vtAxis.append(ui->tbAxis_2);m_vtCurr.append(ui->tbCurr_2);
    m_vtAxis.append(ui->tbAxis_3);m_vtCurr.append(ui->tbCurr_3);
    m_vtAxis.append(ui->tbAxis_4);m_vtCurr.append(ui->tbCurr_4);
    m_vtAxis.append(ui->tbAxis_5);m_vtCurr.append(ui->tbCurr_5);
    m_vtAxis.append(ui->tbAxis_6);m_vtCurr.append(ui->tbCurr_6);

    m_vtPosSelPic.clear();m_vtPosSel.clear();
    m_vtPosSelPic.append(ui->btnHyPic);   m_vtPosSel.append(ui->btnHy);
    m_vtPosSelPic.append(ui->btnJointPic);m_vtPosSel.append(ui->btnJoint);
    m_vtPosSelPic.append(ui->btnTransPic);m_vtPosSel.append(ui->btnTrans);

    int i;
    for(i=0;i<m_vtPosSelPic.count();i++)
    {
        connect(m_vtPosSelPic[i],SIGNAL(mouse_release()),this,SLOT(onPosSel()));
        connect(m_vtPosSel[i],SIGNAL(mouse_press()),m_vtPosSelPic[i],SLOT(press()));
        connect(m_vtPosSel[i],SIGNAL(mouse_release()),m_vtPosSelPic[i],SLOT(release()));
    }

    // io info.
    m_vtInIcon.clear();m_vtInSym.clear();
    m_vtInIcon.append(ui->tbInIcon);  m_vtInSym.append(ui->tbInSym);
    m_vtInIcon.append(ui->tbInIcon_2);m_vtInSym.append(ui->tbInSym_2);
    m_vtInIcon.append(ui->tbInIcon_3);m_vtInSym.append(ui->tbInSym_3);
    m_vtInIcon.append(ui->tbInIcon_4);m_vtInSym.append(ui->tbInSym_4);
    m_vtInIcon.append(ui->tbInIcon_5);m_vtInSym.append(ui->tbInSym_5);
    m_vtInIcon.append(ui->tbInIcon_6);m_vtInSym.append(ui->tbInSym_6);
    m_vtInIcon.append(ui->tbInIcon_7);m_vtInSym.append(ui->tbInSym_7);
    m_vtInIcon.append(ui->tbInIcon_8);m_vtInSym.append(ui->tbInSym_8);

    m_vtOutIcon.clear();m_vtOutSym.clear();
    m_vtOutIcon.append(ui->tbOutIcon);  m_vtOutSym.append(ui->tbOutSym);
    m_vtOutIcon.append(ui->tbOutIcon_2);m_vtOutSym.append(ui->tbOutSym_2);
    m_vtOutIcon.append(ui->tbOutIcon_3);m_vtOutSym.append(ui->tbOutSym_3);
    m_vtOutIcon.append(ui->tbOutIcon_4);m_vtOutSym.append(ui->tbOutSym_4);
    m_vtOutIcon.append(ui->tbOutIcon_5);m_vtOutSym.append(ui->tbOutSym_5);
    m_vtOutIcon.append(ui->tbOutIcon_6);m_vtOutSym.append(ui->tbOutSym_6);
    m_vtOutIcon.append(ui->tbOutIcon_7);m_vtOutSym.append(ui->tbOutSym_7);
    m_vtOutIcon.append(ui->tbOutIcon_8);m_vtOutSym.append(ui->tbOutSym_8);

    connect(ui->btnIOViewPic,SIGNAL(mouse_release()),this,SLOT(onIOView()));
    connect(ui->btnIOViewIcon,SIGNAL(mouse_press()),ui->btnIOViewPic,SLOT(press()));
    connect(ui->btnIOViewIcon,SIGNAL(mouse_release()),ui->btnIOViewPic,SLOT(release()));

    connect(ui->btnIUpPic,SIGNAL(mouse_release()),this,SLOT(onIUp()));
    connect(ui->btnIUpIcon,SIGNAL(mouse_press()),ui->btnIUpPic,SLOT(press()));
    connect(ui->btnIUpIcon,SIGNAL(mouse_release()),ui->btnIUpPic,SLOT(release()));
    connect(ui->btnIDownPic,SIGNAL(mouse_release()),this,SLOT(onIDown()));
    connect(ui->btnIDownIcon,SIGNAL(mouse_press()),ui->btnIDownPic,SLOT(press()));
    connect(ui->btnIDownIcon,SIGNAL(mouse_release()),ui->btnIDownPic,SLOT(release()));

    connect(ui->btnOUpPic,SIGNAL(mouse_release()),this,SLOT(onOUp()));
    connect(ui->btnOUpIcon,SIGNAL(mouse_press()),ui->btnOUpPic,SLOT(press()));
    connect(ui->btnOUpIcon,SIGNAL(mouse_release()),ui->btnOUpPic,SLOT(release()));
    connect(ui->btnODownPic,SIGNAL(mouse_release()),this,SLOT(onODown()));
    connect(ui->btnODownIcon,SIGNAL(mouse_press()),ui->btnODownPic,SLOT(press()));
    connect(ui->btnODownIcon,SIGNAL(mouse_release()),ui->btnODownPic,SLOT(release()));

    connect(ui->lbIcon,SIGNAL(mouse_release()),this,SLOT(onMotorPage()));

    QImage img;
    m_pxmIn.clear();
    img.load(":/icon/icon/icon992-9");  // in off
    m_pxmIn.append(QPixmap::fromImage(img));
    img.load(":/icon/icon/icon992-10"); // in on
    m_pxmIn.append(QPixmap::fromImage(img));

    m_pxmOut.clear();
    img.load(":/icon/icon/icon992-7"); // out off
    m_pxmOut.append(QPixmap::fromImage(img));
    img.load(":/icon/icon/icon992-4"); // out on
    m_pxmOut.append(QPixmap::fromImage(img));

    m_nMaxInputPage = USE_INPUT_NUM / m_vtInIcon.size();
    m_nMaxOutputPage = USE_OUTPUT_NUM / m_vtOutIcon.size();


    // controled output
    m_vtOutputPage.append(ui->grpOut);
    m_vtOutputPage.append(ui->grpOut_2);
    m_vtOutputPage.append(ui->grpOut_3);

    for(i=0;i<m_vtOutputPage.count();i++)
    {
        m_vtOutputPage[i]->setGeometry(ui->grpOut->x(),ui->grpOut->y(),
                                       ui->grpOut->width(),ui->grpOut->height());

        m_vtOutputPage[i]->hide();
    }

    connect(ui->btnOutUpPic,SIGNAL(mouse_release()),this,SLOT(onOutBtnUp()));
    connect(ui->btnOutUpIcon,SIGNAL(mouse_press()),ui->btnOutUpPic,SLOT(press()));
    connect(ui->btnOutUpIcon,SIGNAL(mouse_release()),ui->btnOutUpPic,SLOT(release()));

    connect(ui->btnOutDownPic,SIGNAL(mouse_release()),this,SLOT(onOutBtnDown()));
    connect(ui->btnOutDownIcon,SIGNAL(mouse_press()),ui->btnOutDownPic,SLOT(press()));
    connect(ui->btnOutDownIcon,SIGNAL(mouse_release()),ui->btnOutDownPic,SLOT(release()));

    // matching button & output num
    MatchingOutput();

    for(i=0;i<m_vtOutputPic.count();i++)
    {
        connect(m_vtOutputPic[i],SIGNAL(mouse_release()),this,SLOT(onOutBtn()));
        connect(m_vtOutputIcon[i],SIGNAL(mouse_press()),m_vtOutputPic[i],SLOT(press()));
        connect(m_vtOutputIcon[i],SIGNAL(mouse_release()),m_vtOutputPic[i],SLOT(release()));
    }

    // pointer clear
    dig_io = 0;
    dig_motor = 0;
    top = 0;

}

dialog_manual::~dialog_manual()
{
    delete ui;
}
void dialog_manual::showEvent(QShowEvent *)
{
    Update();

    timer->start();

    gReadyJog(true);

    gStart_IOUpdate();

    SubTop();
}
void dialog_manual::hideEvent(QHideEvent *)
{
    timer->stop();

    gReadyJog(false);

    gStop_IOUpdate();

    Recipe->Set(HyRecipe::bDualSolOff, 1, false);
}
void dialog_manual::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_manual::onClose()
{
    emit accept();
}

void dialog_manual::Update()
{
    // position
    Display_PosSel_Axis(POS_HY);

    // io
    m_nInputPageIndex = 0;
    m_nOutputPageIndex = 0;
    Display_InputPage(m_nInputPageIndex);
    Display_OutputPage(m_nOutputPageIndex);

    // controled output
    m_nOutBtnPageIndex = 0;
    Display_OutBtnPage(m_nOutBtnPageIndex);

}

void dialog_manual::MatchingOutput()
{
    // matching button & output num
    m_vtOutputPic.clear();m_vtOutputNum.clear();
    m_vtOutputPic.append(ui->btnVacPic);   m_vtOutputNum.append(HyOutput::VAC1);
    m_vtOutputPic.append(ui->btnVacPic_2); m_vtOutputNum.append(HyOutput::VAC2);
    m_vtOutputPic.append(ui->btnVacPic_3); m_vtOutputNum.append(HyOutput::VAC3);
    m_vtOutputPic.append(ui->btnVacPic_4); m_vtOutputNum.append(HyOutput::VAC4);
    m_vtOutputPic.append(ui->btnChuckPic); m_vtOutputNum.append(HyOutput::CHUCK);
    m_vtOutputPic.append(ui->btnGripPic);  m_vtOutputNum.append(HyOutput::GRIP);
    m_vtOutputPic.append(ui->btnNipperPic);m_vtOutputNum.append(HyOutput::NIPPER);
    m_vtOutputPic.append(ui->btnRGripPic); m_vtOutputNum.append(HyOutput::RGRIP);
    m_vtOutputPic.append(ui->btnUserPic);  m_vtOutputNum.append(HyOutput::USER1);
    m_vtOutputPic.append(ui->btnUserPic_2);m_vtOutputNum.append(HyOutput::USER2);
    m_vtOutputPic.append(ui->btnUserPic_3);m_vtOutputNum.append(HyOutput::USER3);
    m_vtOutputPic.append(ui->btnUserPic_4);m_vtOutputNum.append(HyOutput::USER4);
    m_vtOutputPic.append(ui->btnUserPic_5);m_vtOutputNum.append(HyOutput::USER5);
    m_vtOutputPic.append(ui->btnUserPic_6);m_vtOutputNum.append(HyOutput::USER6);
    m_vtOutputPic.append(ui->btnUserPic_7);m_vtOutputNum.append(HyOutput::USER7);
    m_vtOutputPic.append(ui->btnUserPic_8);m_vtOutputNum.append(HyOutput::USER8);

    m_vtOutputIcon.clear();
    m_vtOutputIcon.append(ui->btnVacIcon);
    m_vtOutputIcon.append(ui->btnVacIcon_2);
    m_vtOutputIcon.append(ui->btnVacIcon_3);
    m_vtOutputIcon.append(ui->btnVacIcon_4);
    m_vtOutputIcon.append(ui->btnChuckIcon);
    m_vtOutputIcon.append(ui->btnGripIcon);
    m_vtOutputIcon.append(ui->btnNipperIcon);
    m_vtOutputIcon.append(ui->btnRGripIcon);
    m_vtOutputIcon.append(ui->btnUserIcon);
    m_vtOutputIcon.append(ui->btnUserIcon_2);
    m_vtOutputIcon.append(ui->btnUserIcon_3);
    m_vtOutputIcon.append(ui->btnUserIcon_4);
    m_vtOutputIcon.append(ui->btnUserIcon_5);
    m_vtOutputIcon.append(ui->btnUserIcon_6);
    m_vtOutputIcon.append(ui->btnUserIcon_7);
    m_vtOutputIcon.append(ui->btnUserIcon_8);
}

void dialog_manual::onTimer()
{
    // position
    CNRobo* pCon = CNRobo::getInstance();

    float* trans = pCon->getCurTrans();
    float* joint = pCon->getCurJoint();

    Display_PosSel_Curr(m_nPosSel, trans, joint);

    // io
    Display_Input(m_nInputPageIndex);
    Display_Output(m_nOutputPageIndex);


    // controled output
    Display_OutBtn();

}
void dialog_manual::onPosSel()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtPosSelPic.indexOf(btn);
    if(index < 0) return;

    Display_PosSel_Axis((ENUM_POS_SEL)index);
}

void dialog_manual::Display_PosSel_Axis(ENUM_POS_SEL pos_sel)
{
    int i;
    QString str;
    QStringList datas;

    datas.clear();
    switch(pos_sel)
    {
    case POS_HY:
        datas.append(QString(tr("Traverse")));
        datas.append(QString(tr("FwdBwd")));
        datas.append(QString(tr("UpDown")));
        datas.append(QString(tr("Rotation")));
        datas.append(QString(tr("Swivel")));
#ifdef _H6_
        datas.append(QString(tr("J6")));
#endif
        break;

    case POS_JOINT:
        for(i=0;i<USE_AXIS_NUM;i++)
        {
            str.sprintf("J%d", i+1);
            datas.append(str);
        }
        break;

    case POS_TRANS:
        datas.append(QString("X"));
        datas.append(QString("Y"));
        datas.append(QString("Z"));
        datas.append(QString("A"));
        datas.append(QString("B"));
        datas.append(QString("C"));
        break;
    }

    for(i=0;i<m_vtCurr.count();i++)
    {
        if(i < datas.size())
            m_vtAxis[i]->setText(datas[i]);
        else
            m_vtAxis[i]->setText(QString(""));
    }

    ui->tbAxisTitle->setText(m_vtPosSel[pos_sel]->text());
    Display_PosSel_Button(pos_sel);
    gSetJogType((JOG_TYPE)pos_sel);
    m_nPosSel = pos_sel;
}
void dialog_manual::Display_PosSel_Button(ENUM_POS_SEL pos_sel)
{
    for(int i=0;i<m_vtPosSel.count();i++)
    {
        m_vtPosSel[i]->setAutoFillBackground(pos_sel == i);
    }
}

void dialog_manual::Display_PosSel_Curr(ENUM_POS_SEL pos_sel, float *trans, float *joint)
{
    QStringList datas;
    QString str;
    int i;
#ifdef _H6_
    cn_joint _joint;
#endif

    datas.clear();
    switch(pos_sel)
    {
    case POS_HY:
#ifdef _H6_
        str.sprintf(FORMAT_POS, trans[TRAV]);
        datas.append(str);
        str.sprintf(FORMAT_POS, trans[FWDBWD]);
        datas.append(str);
        str.sprintf(FORMAT_POS, trans[UPDN]);
        datas.append(str);

        _joint = gMakeCnJoint(joint);
        str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, _joint));
        datas.append(str);
        str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, _joint));
        datas.append(str);
        str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, _joint));
        datas.append(str);
#else
        str.sprintf(FORMAT_POS, trans[TRAV]);
        datas.append(str);
        str.sprintf(FORMAT_POS, trans[FWDBWD]);
        datas.append(str);
        str.sprintf(FORMAT_POS, trans[UPDN]);
        datas.append(str);
        str.sprintf(FORMAT_POS, gGetWristAngle(joint[J2], joint[J3], joint[J4]));
        datas.append(str);
        str.sprintf(FORMAT_POS, joint[JSWV]);
        datas.append(str);
#endif
        break;
    case POS_JOINT:
        for(i=0;i<USE_AXIS_NUM;i++)
        {
            str.sprintf(FORMAT_POS, joint[i]);
            datas.append(str);
        }
        break;
    case POS_TRANS:
        for(i=0;i<6;i++) // fix 6 (X,Y,Z,A,B,C)
        {
            str.sprintf(FORMAT_POS, trans[i]);
            datas.append(str);
        }
        break;
    }

    for(i=0;i<m_vtCurr.count();i++)
    {
        if(i < datas.size())
            m_vtCurr[i]->setText(datas[i]);
        else
            m_vtCurr[i]->setText(QString(""));
    }
}

void dialog_manual::onIOView()
{
    if(dig_io == 0)
        dig_io = new dialog_io();

    dig_io->exec();

    // because dig_io stop ioupate flag.
    gStart_IOUpdate();
}
void dialog_manual::onIUp()
{
    if(m_nInputPageIndex <= 0)
        return;

    Display_InputPage(--m_nInputPageIndex);
}
void dialog_manual::onIDown()
{
    if(m_nInputPageIndex >= (m_nMaxInputPage - 1))
        return;

    Display_InputPage(++m_nInputPageIndex);
}

void dialog_manual::onOUp()
{
    if(m_nOutputPageIndex <= 0)
        return;

    Display_OutputPage(--m_nOutputPageIndex);
}
void dialog_manual::onODown()
{
    if(m_nOutputPageIndex >= (m_nMaxOutputPage - 1))
        return;

    Display_OutputPage(++m_nOutputPageIndex);
}
void dialog_manual::Display_InputPage(int page_index)
{
    int index;
    for(int i=0;i<m_vtInSym.count();i++)
    {
        index = i + (page_index * m_vtInSym.size());
        m_vtInSym[i]->setText(Input->GetSym(index));
    }

    // display input page num.
    QString str;
    str.sprintf("%d/%d", page_index+1, m_nMaxInputPage);
    ui->tbInPage->setText(str);
}
void dialog_manual::Display_OutputPage(int page_index)
{
    int index;
    for(int i=0;i<m_vtOutSym.count();i++)
    {
        index = i + (page_index * m_vtOutSym.size());
        m_vtOutSym[i]->setText(Output->GetSym(index));
    }

    // display output page num.
    QString str;
    str.sprintf("%d/%d", page_index+1, m_nMaxOutputPage);
    ui->tbOutPage->setText(str);
}

void dialog_manual::Display_Input(int page_index)
{
    int index;
    for(int i=0;i<m_vtInIcon.count();i++)
    {
        index = i + (page_index * m_vtInIcon.size());
        if(Input->IsOn(index))
            m_vtInIcon[i]->setPixmap(m_pxmIn[1]);
        else
            m_vtInIcon[i]->setPixmap(m_pxmIn[0]);
    }
}
void dialog_manual::Display_Output(int page_index)
{
    int index;
    for(int i=0;i<m_vtOutIcon.count();i++)
    {
        index = i + (page_index * m_vtOutIcon.size());
        if(Output->IsOn(index))
            m_vtOutIcon[i]->setPixmap(m_pxmOut[1]);
        else
            m_vtOutIcon[i]->setPixmap(m_pxmOut[0]);
    }
}

/** Controled Output (Button Output) **/
void dialog_manual::onOutBtnUp()
{
    if(m_nOutBtnPageIndex <= 0)
        return;

    Display_OutBtnPage(--m_nOutBtnPageIndex);
}
void dialog_manual::onOutBtnDown()
{
    if(m_nOutBtnPageIndex >= (m_vtOutputPage.size() - 1))
        return;

    Display_OutBtnPage(++m_nOutBtnPageIndex);
}
void dialog_manual::onOutBtn()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtOutputPic.indexOf(btn);
    if(index < 0) return;

    CNRobo* pCon = CNRobo::getInstance();
    int out=0;
    if(Output->IsOn(m_vtOutputNum[index]-1))
        out = (-1) * m_vtOutputNum[index];
    else
        out = m_vtOutputNum[index];

    pCon->setDO(&out, 1);
}

void dialog_manual::Display_OutBtnPage(int page_index)
{
    for(int i=0;i<m_vtOutputPage.count();i++)
    {
        if(i == page_index)
            m_vtOutputPage[i]->show();
        else
            m_vtOutputPage[i]->hide();
    }
}

void dialog_manual::Display_OutBtn()
{
    for(int i=0;i<m_vtOutputIcon.count();i++)
    {
        m_vtOutputIcon[i]->setAutoFillBackground(Output->IsOn(m_vtOutputNum[i]-1));
    }
}

void dialog_manual::onMotorPage()
{
    if(dig_motor == 0)
        dig_motor = new dialog_motor();

    dig_motor->exec();
}

void dialog_manual::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
