#ifndef DIALOG_ARRAY_SET_H
#define DIALOG_ARRAY_SET_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_array_set;
}

class dialog_array_set : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_array_set(QWidget *parent = 0);
    ~dialog_array_set();

    void Update(int page_index);


public slots:
    void onClose();
    void onSel();

    void onNum();
    void onPitch();

    void onOrderChange();

    void onZOffset();

private:
    Ui::dialog_array_set *ui;

    dialog_numpad* np;

    // common.
    QStackedWidget* Stacked;
    QVector<QLabel4*> m_vtSelPic;
    QVector<QLabel3*> m_vtSel;

    QVector<HyRecipe::RECIPE_NUMBER> Vars;
    QVector<float> Datas, Data_Num, Data_Pitch, Data_Order, Data_ZOffset;

    void Display_Sel(int index);
    void Display_Data(int page_index);

    // page array basic.
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtOrder;
    QVector<QLabel4*> m_vtNumPic;
    QVector<QLabel3*> m_vtNum;
    QVector<QLabel4*> m_vtPitchPic;
    QVector<QLabel3*> m_vtPitch;

    // page order change.
    QVector<QLabel3*> m_vtName2;
    QVector<QLabel3*> m_vtOrder2;
    QVector<QLabel4*> m_vtOrderXPic;
    QVector<QLabel3*> m_vtOrderX;
    QVector<QLabel4*> m_vtOrderYPic;
    QVector<QLabel3*> m_vtOrderY;
    QVector<QLabel4*> m_vtOrderZPic;
    QVector<QLabel3*> m_vtOrderZ;

    void Display_Order(QVector<float>& order);
    void ChangeOrderX(int order);
    void ChangeOrderY(int order);
    void ChangeOrderZ(int order);

    // page floor offset
    QVector<QLabel3*> m_vtName3;
    QVector<QLabel4*> m_vtOffsetPic;
    QVector<QLabel3*> m_vtOffset;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ARRAY_SET_H
