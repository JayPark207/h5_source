#ifndef HYECATMOTOR_H
#define HYECATMOTOR_H

#include "defines.h"

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

/** EtherCAT driver common function
    This is mother class.
    son class is different function for each maker.

    This class make panasonic base. top functions.
**/

#define MT_PANASONIC_OVERLOAD_LEVEL_MIN 50
#define MT_PANASONIC_OVERLOAD_LEVEL_MAX 115

class HyEcatMotor : public QObject
{
    Q_OBJECT

protected:
    union UNI_STATUSWORD
    {
        struct {
            USHORT rtso:1;
            USHORT so:1;
            USHORT oe:1;
            USHORT f:1;
            USHORT ve:1;
            USHORT qs:1;
            USHORT sod:1;
            USHORT w:1;
            USHORT r8:1;
            USHORT rm:1;
            USHORT oms10:1;
            USHORT ila:1;
            USHORT oms12:1;
            USHORT oms13:1;
            USHORT r14:1;
            USHORT r15:1;
        }bit;

        USHORT ushort;
    };

    union UNI_CONTROLWORD
    {
        struct {
            USHORT so:1;
            USHORT ev:1;
            USHORT qs:1;
            USHORT eo:1;
            USHORT oms4:1;
            USHORT oms5:1;
            USHORT oms6:1;
            USHORT fr:1;
            USHORT h:1;
            USHORT oms9:1;
            USHORT r10:1;
            USHORT r11:1;
            USHORT r12:1;
            USHORT r13:1;
            USHORT r14:1;
            USHORT r15:1;
        }bit;

        USHORT ushort;
    };

public:
    HyEcatMotor();

    // cyclic update function.
    void Update(int motor_id); // SDO read & write.
    void Update();
    void Read();
    void Read(int motor_id);
    void Write(int motor_id);

    void Debug_StatusWord();

    // related cyclic update(read)
    bool IsServo(int motor_id);
    bool IsPower(int motor_id);
    bool IsError(int motor_id);
    bool IsWarning(int motor_id);
    bool IsLimit(int motor_id);
    USHORT GetErrorCode(int motor_id);
    int GetRealCount(int motor_id);
    int GetLocalCount(int motor_id);
    USHORT GetOverloadRate(int motor_id);
    SHORT GetTorqueDemand(int motor_id);
    SHORT GetTorqueActual(int motor_id);

    // related cyclic update(write)
    void Servo(int motor_id, bool on);
    void Reset(int motor_id);

    // multi-turn reset
    bool Ready_MultiTurnClear(int motor_id);
    bool Set_MultiTurnClear(int motor_id);
    bool Reset_MultiTurnClear(int motor_id);

    // overload level setup ~115% (if value=0, 115%)
    bool Set_OverloadLevel(int motor_id, int value);
    bool Get_OverloadLevel(int motor_id, int& value);
    void Save_EEPROM(int motor_id);

    // for standalone functions.
    bool GetSDO(int motor_id, int param, unsigned long& data);
    bool SetSDO(int motor_id, int param, unsigned long data);

    // standalone functions
    // add here..

protected:

    QVector<UNI_STATUSWORD>     m_Status;
    QVector<UNI_CONTROLWORD>    m_Control;
    QVector<USHORT>             m_ErrorCode;
    QVector<int>                m_Realcount;
    QVector<int>                m_Localcount;
    QVector<USHORT>             m_OverloadRate;
    QVector<SHORT>              m_TorqueDemand;
    QVector<SHORT>              m_TorqueActual;


};

#endif // HYECATMOTOR_H
