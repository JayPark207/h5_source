#include "dialog_message.h"
#include "ui_dialog_message.h"

dialog_message::dialog_message(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_message)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(50);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    m_nCloseTime_msec = 0; // not use

}

dialog_message::~dialog_message()
{
    delete ui;
}
void dialog_message::showEvent(QShowEvent *)
{
    timer->start();
    watch.start();
}
void dialog_message::hideEvent(QHideEvent *)
{
    timer->stop();
    m_nCloseTime_msec = 0;
}
void dialog_message::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_message::Message(QString msg1, QString msg2)
{
    ui->lbMessage1->setText(msg1);
    ui->lbMessage2->setText(msg2);
}

void dialog_message::Title(QString title)
{
    ui->lbTitle->setText(title);
}

void dialog_message::SetColor(DIAG_MSG_COLOR color)
{
    QImage img;

    if(color == dialog_message::RED)
        img.load(":/image/image/messagebox2");
    else if(color == dialog_message::GREEN)
        img.load(":/image/image/messagebox3");
    else if(color == dialog_message::SKYBLUE)
        img.load(":/image/image/messagebox4");
    else if(color == dialog_message::GRAY)
        img.load(":/image/image/messagebox5");
    else if(color == dialog_message::BLUE)
        img.load(":/image/image/messagebox6");
    else
        img.load(":/image/image/messagebox1");

    QPixmap pxm = QPixmap::fromImage(img);
    ui->lbBasePic->setPixmap(pxm);
}
void dialog_message::CloseTime(int msec)
{
    m_nCloseTime_msec = msec;
}
void dialog_message::onTimer()
{
    if(m_nCloseTime_msec <= 0) return;

    if(watch.hasExpired((qint64)m_nCloseTime_msec))
    {
        emit accept();
    }
}
