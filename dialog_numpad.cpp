#include "dialog_numpad.h"
#include "ui_dialog_numpad.h"

dialog_numpad::dialog_numpad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_numpad)
{
    ui->setupUi(this);

    m_numpad = new numpad(this);
    sw = new QStackedWidget(this);

    this->setWindowFlags(Qt::FramelessWindowHint /*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setGeometry(0,0,MAX_WIDTH,MAX_HEIGHT);

    sw->addWidget(m_numpad);
    sw->setGeometry(MAX_WIDTH-m_numpad->width(),MAX_HEIGHT-m_numpad->height(),m_numpad->width(),m_numpad->height());
    sw->setCurrentIndex(0);

    connect(m_numpad,SIGNAL(Cancel()),this,SLOT(reject()));
    connect(m_numpad,SIGNAL(Enter()),this,SLOT(onOK()));

    setModal(true);
}

dialog_numpad::~dialog_numpad()
{
    delete ui;
}

void dialog_numpad::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_numpad::onOK()
{
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    if(!m_numpad->IsMin())
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("MIN Error")));
        msg->Message(tr("Less than MIN Value!"));
        msg->exec();
        return;
    }
    else if(!m_numpad->IsMax())
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("MAX Error")));
        msg->Message(tr("Greater than MAX Value!"));
        msg->exec();
        return;
    }

    emit accept();
}
