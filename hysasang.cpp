#include "hysasang.h"

HySasang::HySasang()
{
    this->Motion = new HySasang_Motion();
    this->Pattern = new HySasang_Pattern();
    this->Group = new HySasang_Group();


}

void HySasang::Init_String()
{
    Motion->Init_String();
    //...
}

bool HySasang::Pattern2Motions(ST_SSPATTERN_DATA pattern, QList<ST_SSMOTION_DATA> &motions)
{
    if(pattern.motions.isEmpty()) return false;
    if(pattern.motions.size() != pattern.program.size()) return false;

    QList<ST_SSMOTION_DATA> result;
    ST_SSMOTION_DATA temp;

    result.clear();

    Motion->Default(temp);
    Motion->SetDefault(temp);

    for(int i=0;i<pattern.motions.count();i++)
    {
        temp.raw = pattern.motions[i];

        if(!Motion->Raw2Data(temp))
            return false;

        temp.program = pattern.program[i];

        result.append(temp);
    }

    motions = result;
    return true;
}

bool HySasang::Motions2Pattern(QList<ST_SSMOTION_DATA> motions, ST_SSPATTERN_DATA &pattern)
{
    if(motions.isEmpty()) return false;

    QStringList _motions;
    QList<QStringList> _program;

    _motions.clear();
    _program.clear();

    for(int i=0;i<motions.count();i++)
    {
        _motions << motions[i].raw;
        _program.append(motions[i].program);
    }

    pattern.motions = _motions;
    pattern.program = _program;
    return true;
}

bool HySasang::Group2Patterns(ST_SSGROUP_DATA group, QList<ST_SSPATTERN_DATA> &patterns)
{
    int i;

    QList<ST_SSPATTERN_DATA> result;
    ST_SSPATTERN_DATA temp;
    result.clear();
    for(i=0;i<group.data.count();i++)
    {
        if(!Pattern->GroupFormat2Data(group.data[i], temp))
            return false;

        temp.program = group.program[i];

        result.append(temp);
    }

    patterns = result;
    return true;
}

bool HySasang::Patterns2Group(QList<ST_SSPATTERN_DATA> patterns, ST_SSGROUP_DATA &group)
{
    ST_SSGROUP_DATA result = group;

    result.data.clear();
    result.program.clear();

    QStringList temp_data;

    for(int i=0;i<patterns.count();i++)
    {
        if(patterns[i].motions.size() != patterns[i].program.size())
            return false;

        if(!Pattern->Data2GroupFormat(patterns[i], temp_data))
            return false;

        result.data.append(temp_data);
        result.program.append(patterns[i].program);
    }

    group = result;
    return true;
}

