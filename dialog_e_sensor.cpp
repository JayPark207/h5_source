#include "dialog_e_sensor.h"
#include "ui_dialog_e_sensor.h"

dialog_e_sensor::dialog_e_sensor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_e_sensor)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onEnd()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtName.clear();
    m_vtName.append(ui->tbName);
    m_vtName.append(ui->tbName_2);
    m_vtName.append(ui->tbName_3);
    m_vtName.append(ui->tbName_4);

    m_vtUse.clear();m_vtUsePic.clear();
    m_vtUse.append(ui->tbUse);  m_vtUsePic.append(ui->tbUsePic);
    m_vtUse.append(ui->tbUse_2);m_vtUsePic.append(ui->tbUsePic_2);
    m_vtUse.append(ui->tbUse_3);m_vtUsePic.append(ui->tbUsePic_3);
    m_vtUse.append(ui->tbUse_4);m_vtUsePic.append(ui->tbUsePic_4);

    m_vtMin.clear();m_vtMinPic.clear();
    m_vtMin.append(ui->tbMin);  m_vtMinPic.append(ui->tbMinPic);
    m_vtMin.append(ui->tbMin_2);m_vtMinPic.append(ui->tbMinPic_2);
    m_vtMin.append(ui->tbMin_3);m_vtMinPic.append(ui->tbMinPic_3);
    m_vtMin.append(ui->tbMin_4);m_vtMinPic.append(ui->tbMinPic_4);

    m_vtMax.clear();m_vtMaxPic.clear();
    m_vtMax.append(ui->tbMax);  m_vtMaxPic.append(ui->tbMaxPic);
    m_vtMax.append(ui->tbMax_2);m_vtMaxPic.append(ui->tbMaxPic_2);
    m_vtMax.append(ui->tbMax_3);m_vtMaxPic.append(ui->tbMaxPic_3);
    m_vtMax.append(ui->tbMax_4);m_vtMaxPic.append(ui->tbMaxPic_4);

    int i;
    for(i=0;i<m_vtUsePic.count();i++)
    {
        connect(m_vtUsePic[i],SIGNAL(mouse_release()),this,SLOT(onUse()));
        connect(m_vtUse[i],SIGNAL(mouse_press()),m_vtUsePic[i],SLOT(press()));
        connect(m_vtUse[i],SIGNAL(mouse_release()),m_vtUsePic[i],SLOT(release()));
    }
    for(i=0;i<m_vtMinPic.count();i++)
    {
        connect(m_vtMinPic[i],SIGNAL(mouse_release()),this,SLOT(onMin()));
        connect(m_vtMin[i],SIGNAL(mouse_press()),m_vtMinPic[i],SLOT(press()));
        connect(m_vtMin[i],SIGNAL(mouse_release()),m_vtMinPic[i],SLOT(release()));
    }
    for(i=0;i<m_vtMaxPic.count();i++)
    {
        connect(m_vtMaxPic[i],SIGNAL(mouse_release()),this,SLOT(onMax()));
        connect(m_vtMax[i],SIGNAL(mouse_press()),m_vtMaxPic[i],SLOT(press()));
        connect(m_vtMax[i],SIGNAL(mouse_release()),m_vtMaxPic[i],SLOT(release()));
    }

    connect(ui->tbDelayTimePic,SIGNAL(mouse_release()),this,SLOT(onDelay()));
    connect(ui->tbDelayTime,SIGNAL(mouse_press()),ui->tbDelayTimePic,SLOT(press()));
    connect(ui->tbDelayTime,SIGNAL(mouse_release()),ui->tbDelayTimePic,SLOT(release()));

}

dialog_e_sensor::~dialog_e_sensor()
{
    delete ui;
}
void dialog_e_sensor::showEvent(QShowEvent *)
{
    Enable_Table(4);

    Update();
}
void dialog_e_sensor::hideEvent(QHideEvent *)
{

}
void dialog_e_sensor::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}


void dialog_e_sensor::onEnd()
{
    QString str;
    if(!Check_Values(str))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Value Error")));
        msg->Message(str,
                     tr("Please input Min < Max."));
        msg->exec();
        return;
    }
    emit accept();
}

void dialog_e_sensor::Enable_Table(int num)
{
    if(num < 0) num = 0;
    if(num > m_vtName.size()) num = m_vtName.size();

    m_nEnableNum = num;

    QString name = QString(tr("E-Sensor"));

    for(int i=0;i<m_vtName.count();i++)
    {
        if(i < num)
        {
            // enable
            m_vtUse[i]->setEnabled(true);
            m_vtUsePic[i]->setEnabled(true);
            m_vtMin[i]->setEnabled(true);
            m_vtMinPic[i]->setEnabled(true);
            m_vtMax[i]->setEnabled(true);
            m_vtMaxPic[i]->setEnabled(true);

            m_vtName[i]->setText(name + QString(" %1").arg(i+1));
        }
        else
        {
            // disable
            m_vtUse[i]->setEnabled(false);
            m_vtUsePic[i]->setEnabled(false);
            m_vtMin[i]->setEnabled(false);
            m_vtMinPic[i]->setEnabled(false);
            m_vtMax[i]->setEnabled(false);
            m_vtMaxPic[i]->setEnabled(false);

            m_vtName[i]->setText("");
            m_vtUse[i]->setText("");
            m_vtMin[i]->setText("");
            m_vtMax[i]->setText("");
        }
    }
}

void dialog_e_sensor::Update()
{

    m_ItemName.clear();
    m_ItemName.append(tr("No Use"));
    m_ItemName.append(tr("Use"));

    vars.clear();
    vars.append(HyRecipe::mdESen1);
    vars.append(HyRecipe::mdESen2);
    vars.append(HyRecipe::mdESen3);
    vars.append(HyRecipe::mdESen4);
    vars.append(HyRecipe::vESenMin1);
    vars.append(HyRecipe::vESenMin2);
    vars.append(HyRecipe::vESenMin3);
    vars.append(HyRecipe::vESenMin4);
    vars.append(HyRecipe::vESenMax1);
    vars.append(HyRecipe::vESenMax2);
    vars.append(HyRecipe::vESenMax3);
    vars.append(HyRecipe::vESenMax4);
    vars.append(HyRecipe::tESenDelay);

    if(Recipe->Gets(vars, datas))
    {
        QString str;
        for(int i=0;i<m_nEnableNum;i++)
        {
            // Use
            if(datas[i] < m_ItemName.size())
                m_vtUse[i]->setText(m_ItemName[(int)datas[i]]);
            // Min
            str.sprintf("%.1f",datas[4+i]);
            m_vtMin[i]->setText(str);
            // Max
            str.sprintf("%.1f",datas[8+i]);
            m_vtMax[i]->setText(str);
        }

        // delay time.
        str.sprintf(FORMAT_TIME, datas.last());
        str += " ";
        str += tr("sec");
        ui->tbDelayTime->setText(str);
    }

}

void dialog_e_sensor::onUse()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtUsePic.indexOf(btn);
    if(index < 0) return;

    sel = (dialog_mode_select*)gGetDialog(DIG_MODE_SELECT);
    sel->InitTitle(m_vtName[index]->text());
    sel->InitRecipe(vars[0+index]);
    sel->InitNum(m_ItemName.size());
    for(int i=0;i<sel->GetNum();i++)
        sel->InitString(i,m_ItemName[i]);

    if(sel->exec() == QDialog::Accepted)
        Update();

}
void dialog_e_sensor::onMin()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtMinPic.indexOf(btn);
    if(index < 0) return;

    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(1);
    QString str;
    str = m_vtName[index]->text() + " " + ui->tbMinTitle->text();
    np->m_numpad->SetTitle(str);
    np->m_numpad->SetNum((double)datas[4+index]);
    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(vars[4+index],(float)np->m_numpad->GetNumDouble()))
            Update();
    }
}

void dialog_e_sensor::onMax()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtMaxPic.indexOf(btn);
    if(index < 0) return;

    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(1);
    QString str;
    str = m_vtName[index]->text() + " " + ui->tbMaxTitle->text();
    np->m_numpad->SetTitle(str);
    np->m_numpad->SetNum((double)datas[8+index]);
    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(vars[8+index],(float)np->m_numpad->GetNumDouble()))
            Update();
    }
}

void dialog_e_sensor::onDelay()
{
    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->tbDelayName->text());
    np->m_numpad->SetNum((double)datas.last());
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(vars.last(),(float)np->m_numpad->GetNumDouble()))
            Update();
    }
}

bool dialog_e_sensor::Check_Values(QString& msg)
{
    msg.clear();

    for(int i=0;i<m_nEnableNum;i++)
    {
        if((int)datas[i] > 0)
        {
            if(datas[4+i] >= datas[8+i])
            {
                msg = m_vtName[i]->text();
                msg += " : ";
                msg += tr("Min Value > Max Value");
                return false;
            }
        }
    }

    return true;
}
