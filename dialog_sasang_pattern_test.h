#ifndef DIALOG_SASANG_PATTERN_TEST_H
#define DIALOG_SASANG_PATTERN_TEST_H

#include <QWidget>
#include <QTimer>

#include "global.h"

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_message.h"
#include "dialog_confirm.h"
#include "dialog_delaying.h"
#include "dialog_error.h"
#include "dialog_jog.h"

#define SSPTEST_SPEED_MAX   100 // [%] max speed.
#define SSPTEST_SPEED_MIN   5
#define SSPTEST_SPEED_GAP   5   // [%] up/down gap
#define SSPTEST_SPEED_INIT  50  // [%] init speed.(set low speed for safety)

namespace Ui {
class dialog_sasang_pattern_test;
}

class dialog_sasang_pattern_test : public QWidget
{
    Q_OBJECT

public:
    explicit dialog_sasang_pattern_test(QWidget *parent = 0);
    ~dialog_sasang_pattern_test();

    void Update();

private:
    Ui::dialog_sasang_pattern_test *ui;

    QTimer* timer;

    QVector<QLabel3*> m_vtTbIcon, m_vtTbNo, m_vtTbPat;

    int m_nStartIndex;
    int m_nMainRunIndex, m_nSubRunIndex, m_nSubPlanIndex;
    CNR_TASK_STATUS m_nTaskStatus;

    void Redraw(int run_index, CNR_TASK_STATUS task_status); // boss
    void Redraw_List(int start_index);

    QVector<QPixmap> pxmIcon;
    void Display_Icon(int table_index, CNR_TASK_STATUS status);

    bool m_bReady;
    void Control_Ready(bool bReady);

    bool LoadProgram();
    bool ResetProgram();

    void Control_Run(CNR_TASK_STATUS status);

    dialog_delaying* dig_delaying;
    dialog_error* dig_error;
    dialog_jog* dig_jog;

    int m_nRunType; // 1=Run, 2=Step, 0=Stop
    int m_nStopType; // 1=RunStop 2=StepStop 0=NormalStop

    bool ServoOnMode(int mode);

    float m_TestSpeed;
    bool SetSpeed(float speed);

    void Delay(int msec);

public slots:
    void onClose();
    void onTimer();

    void onReady();
    void onStepFwd();
    void onRun();
    void onPause();

    void onError();
    void onJog();
    void onReset();

    void onSpdUp();
    void onSpdDown();

signals:
    void sigClose();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SASANG_PATTERN_TEST_H
