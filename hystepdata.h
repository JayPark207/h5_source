#ifndef HYSTEPDATA
#define HYSTEPDATA

#include <QObject>
#include <QDebug>
#include <QString>
#include <QList>
#include <QVector>
#include "defines.h"

struct ST_STEP_DATA
{
    QString     DispName;       // GUI표시 이름. (HyStepName 활용)
    QString     ProgLine;       // main program Line 내용. (ex. Call takeout;)
    QString     NickName;       // DispName(NickName) Only English. - when read, buffer concept.
    //bool        bSetComplete;   // 세팅완료여부 (?)
    //bool        bReadonly;      // true: 삭제불가능. false:삭제가능.
    int         Id;
};


// 언어국제화 위한 스텝이름 클래스. (HyStepEdit 안에서 사용)
class HyStepData : public QObject
{
    Q_OBJECT

public:
    enum ENUM_STEP
    {
        // major (fix)
        WAIT=0,
        TAKEOUT,
        UP,
        UNLOAD,
        INSERT,
        INSERT_UNLOAD,

        // sub major (related to mode setting)
        EXT_WAIT,
        DEBURR,
        W_MEASURE,  // new
        W_RESET,    // new

        // minor (related to user option)
        // add position.
        ADD_POS0,
        ADD_POSN = ADD_POS0 + (MAX_USER_STEP_POS-1),
        // add output.
        ADD_OUT0,
        ADD_OUTN = ADD_OUT0 + (MAX_USER_STEP_OUT-1),
        // add wait input.
        ADD_IN0,
        ADD_INN = ADD_IN0 + (MAX_USER_STEP_IN-1),
        // add wait time.
        ADD_TIME0,
        ADD_TIMEN = ADD_TIME0 + (MAX_USER_STEP_TIME-1),
        // add work (sasang step)
        ADD_WORK0,
        ADD_WORKN = ADD_WORK0 + (MAX_USER_STEP_WORK-1),

        STEP_NUM,
    };


public:
    HyStepData()
    {
        Init();
    }

    void Init()
    {
        m_strMainProgName = MACRO_MAIN;

        Step[WAIT].DispName = QString(tr("Wait"));
        Step[WAIT].ProgLine = QString("Call wait");

        Step[TAKEOUT].DispName      = QString(tr("Take-Out"));
        Step[TAKEOUT].ProgLine      = QString("Call takeout");

        Step[UP].DispName           = QString(tr("Up"));
        Step[UP].ProgLine           = QString("Call up");

        Step[UNLOAD].DispName       = QString(tr("Unload"));
        Step[UNLOAD].ProgLine       = QString("Call unload");
        //...
        Step[EXT_WAIT].DispName     = QString(tr("External Wait")); // no use
        Step[EXT_WAIT].ProgLine     = QString("Call externalwait"); // no use

        Step[INSERT].DispName     = QString(tr("Insert Grip"));
        Step[INSERT].ProgLine     = QString("Call insertgrip");

        // add
        Step[INSERT_UNLOAD].DispName        = QString(tr("Insert Unload"));
        Step[INSERT_UNLOAD].ProgLine        = QString("Call insertul");

        Step[DEBURR].DispName       = QString(tr("Deburring"));
        Step[DEBURR].ProgLine       = QString("Call deburring");

        // new
        Step[W_MEASURE].DispName    = QString(tr("Weight Measure"));
        Step[W_MEASURE].ProgLine    = QString("Call w_measure");

        Step[W_RESET].DispName    = QString(tr("Weight Reset"));
        Step[W_RESET].ProgLine    = QString("Call w_reset");


        int i;
        for(i=0;i<MAX_USER_STEP_POS;i++)
        {
            Step[ADD_POS0+i].DispName = QString(tr("Position"));
            Step[ADD_POS0+i].DispName += QString(" %1").arg(i+1);
            Step[ADD_POS0+i].ProgLine = QString("Call userposition%1").arg(i+1);
        }
        m_strUserPositionProgBase = QString("userposition");
        for(i=0;i<MAX_USER_STEP_OUT;i++)
        {
            Step[ADD_OUT0+i].DispName = QString(tr("Output"));
            Step[ADD_OUT0+i].DispName += QString(" %1").arg(i+1);
            Step[ADD_OUT0+i].ProgLine = QString("Call useroutput%1").arg(i+1);
        }
        m_strUserOutputProgBase = QString("useroutput");
        for(i=0;i<MAX_USER_STEP_IN;i++)
        {
            Step[ADD_IN0+i].DispName = QString(tr("Wait Input"));
            Step[ADD_IN0+i].DispName += QString(" %1").arg(i+1);
            Step[ADD_IN0+i].ProgLine = QString("Call userinput%1").arg(i+1);
        }
        m_strUserWaitInputProgBase = QString("userinput");
        for(i=0;i<MAX_USER_STEP_TIME;i++)
        {
            Step[ADD_TIME0+i].DispName = QString(tr("Wait Time"));
            Step[ADD_TIME0+i].DispName += QString(" %1").arg(i+1);
            Step[ADD_TIME0+i].ProgLine = QString("Call usertime%1").arg(i+1);
        }
        m_strUserWaitTimeProgBase = QString("usertime");
        for(i=0;i<MAX_USER_STEP_WORK;i++)
        {
            Step[ADD_WORK0+i].DispName = QString(tr("S-Work"));
            Step[ADD_WORK0+i].DispName += QString(" %1").arg(i+1);
            Step[ADD_WORK0+i].ProgLine = QString("Call userwork%1").arg(i+1);
        }
        m_strUserWorkProgBase = QString("userwork");

        // all step id save.
        for(i=0;i<STEP_NUM;i++)
        {
            Step[i].Id = i;
            Step[i].NickName.clear();
        }
    }

    ST_STEP_DATA Step[STEP_NUM];
    QString m_strMainProgName;
    QString m_strUserPositionProgBase;
    QString m_strUserOutputProgBase;
    QString m_strUserWaitInputProgBase;
    QString m_strUserWaitTimeProgBase;
    QString m_strUserWorkProgBase;

};

#endif // HYSTEPDATA

