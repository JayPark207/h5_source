#ifndef DIALOG_STEPEDIT_H
#define DIALOG_STEPEDIT_H

#include <QDialog>
#include <QStackedWidget>

#include "global.h"

#include "qlabel3.h"
#include "dialog_keyboard.h"
#include "dialog_stepedit_add.h"
#include "dialog_confirm.h"
#include "dialog_message.h"

#include "form_stepedit_base.h"
#include "form_stepedit_userinput.h"
#include "form_stepedit_useroutput.h"
#include "form_stepedit_userposition.h"

#include "dialog_message.h"
#include "dialog_position_teaching2.h"
#include "dialog_home.h"
#include "dialog_home2.h"

#include "widget_stepedit_program.h"
#include "widget_stepedit_programrun.h"

#include "top_sub.h"
#include "dialog_delaying.h"

#include "dialog_sasang_addstep.h"

#include "hyui_longpress.h"

#define STEPTEST_SPEED_MAX   50 // [%] max speed.
#define STEPTEST_SPEED_MIN   5
#define STEPTEST_SPEED_GAP   5   // [%] up/down gap
#define STEPTEST_SPEED_INIT  30  // [%] init speed.(set low speed for safety)


namespace Ui {
class dialog_stepedit;
}

class dialog_stepedit : public QDialog
{
    Q_OBJECT

public:

    enum ENUM_PLAY_STATUS
    {
        NO_STATUS=0,
        STOP,
        PAUSE,
        PLAY,
    };

    enum ENUM_CHECK
    {
        NO_CHECK=0,
        MODE,
        USER,
        SUB_MODE,
    };

public:
    explicit dialog_stepedit(QWidget *parent = 0);
    ~dialog_stepedit();

    void Init_Widget();

    QStackedWidget*                 Stacked;
    form_stepedit_base*             frmBase;
    form_stepedit_useroutput*       frmOutput;
    form_stepedit_userinput*        frmInputTime;
    form_stepedit_userposition*     frmUserPos;

    widget_stepedit_program*        wigProgram;
    widget_stepedit_programrun*     wigProgramRun;

private slots:
    void onClose();
    void onDebug();

    void onUp();
    void onDown();
    void onUpLong();
    void onDownLong();

    void onStepUp();
    void onStepDown();
    void onAdd();
    void onDelete();
    void onDetail();

    void onSelectLine();

    void onPressBtm();
    void onReleaseBtm();

    void onProgram();

    // sub button slot
    void onRename();
    void onTest();

    void onOtherClose();

    // test run
    void onRunTimer();
    void onStepFwd();
    void onRun();
    void onPause();
    void onReset();
    void onHome();

    void onTestSpdUp();
    void onTestSpdDown();

    void onStepBwd();   // now not use
    void onCycle();     // now not use

private:
    Ui::dialog_stepedit *ui;
    dialog_message* msg;

    void UserLevel();       // must.

    top_sub* top;
    void SubTop();          // Need SubTop

    QString Title;

    QVector<QLabel3*> m_vtIcon;
    QVector<QLabel3*> m_vtNo;
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtLine;
    QVector<QLabel3*> m_vtCheck;

    void Update();
    void Redraw_List(int start_index);
    QVector<QPixmap> pxmSelectLine;
    void SelectLine(int line);

    int m_nStartIndex;
    int m_nSelectedIndex;
    int m_nPlayIndex;
    ENUM_PLAY_STATUS m_nPlayStatus;

    QVector<QPixmap> pxmCheckLine;
    void CheckLine(int line, ENUM_CHECK check = NO_CHECK);
    void CheckLineOff();
    void Control_SideButton(int list_index);



    dialog_stepedit_add* dig_add;

    void CheckMode_ChangeList();



    dialog_position_teaching2* posteach;

    /** tes run part **/
    void SetTestMode(bool on);
    bool m_bTestMode;

    QTimer* timerRun;
    bool LoadProgram();
    bool CleanProgram();
    bool ResetProgram();

    int m_nTaskStatus_Old;
    void Control_PlayButton(CNR_TASK_STATUS task_status, bool homed);
    QVector<QPixmap> pxmIconLine;
    void IconLine(int line, ENUM_PLAY_STATUS status = NO_STATUS);
    void DisplayRunIcon(int run_index, CNR_TASK_STATUS task_status);

    int m_nMainRunIndex;
    int m_nSubRunIndex;
    int m_nSubPlanIndex;
    CNR_TASK_STATUS m_nTaskStatus;

    QString m_strNowProgName, m_strNowProgName_Old;
    int m_nUserLevel;

    void Delaying(int msec);

    dialog_sasang_addstep* dig_sasang_addstep;


    // new functions...
    bool ServoOnMode(int mode);
    dialog_home2* dig_home2;
    bool m_bHome;
    bool Home(bool bCheck);
    void EntryHome();

    float m_TestSpeed;
    bool SetSpeed(float speed);

    HyUi_LongPress* uiLongPressUp;
    HyUi_LongPress* uiLongPressDown;

    bool m_bUpdate;

    bool m_bAddUserStep;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // DIALOG_STEPEDIT_H
