#ifndef DIALOG_MOTOR_H
#define DIALOG_MOTOR_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_delaying.h"
#include "dialog_numpad.h"
#include "dialog_message.h"

#include "top_sub.h"

namespace Ui {
class dialog_motor;
}


class dialog_motor : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_motor(QWidget *parent = 0);
    ~dialog_motor();

    void Update();

public slots:
    void onCheckbox();
    void onListUp();
    void onListDown();
    void onTimer();
    void onSelect();
    void onEcatReset();

    void onServo();
    void onReset();

    void onLevel();
    void onSet();

private:
    Ui::dialog_motor *ui;
    dialog_numpad* np;

    QVector<QLabel3*> m_vtID;
    QVector<QLabel3*> m_vtModel;
    QVector<QLabel3*> m_vtP;
    QVector<QLabel3*> m_vtS;
    QVector<QLabel3*> m_vtL;
    QVector<QLabel3*> m_vtErr;
    QVector<QLabel3*> m_vtRc;
    QVector<QLabel3*> m_vtTq;

    QStringList m_MotorID;
    QStringList m_MotorModel;
    void Read_MotorData();

    int m_nStartIndex;
    void Redraw_List(int start_index);

    void SetCheckbox(bool on);
    void EnableControl(bool enable);

    // status update.
    QTimer* timer;
    void Update_Status();

    void Display_Power(int line, bool on);
    void Display_Servo(int line, bool on);
    void Display_Limit(int line, bool on);
    void Display_Error(int line, USHORT code, bool warn = false);
    void Display_Realcount(int line, int count);
    void Display_OverloadRate(int line, USHORT rate);
    void Display_Torque(int line, SHORT demand, SHORT actual);

    QPalette Pon,Poff;
    QPalette Son,Soff;
    QPalette Lon,Loff;
    QPalette Eon,Won,Eoff;

    void Init_Color();

    void Display_SelectedLine(int line);

    top_sub* top;
    void SubTop();          // Need SubTop

    // overload level
    QVector<QLabel3*> m_vtLevel;
    QVector<QLabel4*> m_vtLevelPic;

    QVector<int> m_OverloadLevel_1st, m_OverloadLevel;
    void Read_OverloadLevel_1st();
    void Read_OverloadLevel(int id);
    void Display_OverloadLevel(int line, int level);
    void Comp_Save_EEPROM();
    void Display_BtnSet(bool enable);

    int m_nUserLevel;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

private slots:
    void on_btnTest1_clicked();

};

#endif // DIALOG_MOTOR_H
