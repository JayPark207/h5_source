#ifndef DIALOG_SOFTSENSOR_H
#define DIALOG_SOFTSENSOR_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"
#include "dialog_numpad.h"
#include "dialog_message.h"

namespace Ui {
class dialog_softsensor;
}

class dialog_softsensor : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_softsensor(QWidget *parent = 0);
    ~dialog_softsensor();

    void Update();

public slots:
    void onOK();

    void onMin();
    void onMax();

private:
    Ui::dialog_softsensor *ui;

    QVector<QLabel4*> m_vtMinPic,m_vtMaxPic;
    QVector<QLabel3*> m_vtMin,m_vtMax;

    bool Save();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SOFTSENSOR_H
