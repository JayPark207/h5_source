#ifndef DIALOG_POSITION_TEACHING_H
#define DIALOG_POSITION_TEACHING_H

#include <QDialog>
#include <QTimer>
#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_rotation.h"
#include "widget_jogmodule.h"
#include "dialog_call_position.h"

#include "dialog_message.h"

#include "top_sub.h"

namespace Ui {
class dialog_position_teaching;
}

class dialog_position_teaching : public QDialog
{
    Q_OBJECT

public:
    enum ENUM_TYPE
    {
        BASE=0,
        USER,
        WORK,
    };

    enum ENUM_MOVE_TYPE
    {
        TYPE_SEQ=0,
        TYPE_ALL,
    };

public:
    explicit dialog_position_teaching(QWidget *parent = 0);
    ~dialog_position_teaching();

    void Init(int pos_index);
    void Init(ENUM_TYPE type, int pos_index);   // pos_index is always 0~
    void Update();  // first view only one execution.

public slots:
    void onOK();
    void onCancel();
    void onPos();
    void onReal();
    void onCurr();
    void onTimer();
    void onSpeed();
    void onDelay();
    void onReset();
    void onRotation();
    void onSwivel();

    void onMoveType();

    void onCall();

private:
    Ui::dialog_position_teaching *ui;

    widget_jogmodule* wmJog;

    ENUM_TYPE m_nType;

    int m_nPosIndex;

    cn_trans m_RealTransData, m_TransData, m_TransDataOld;
    cn_joint m_RealJointData, m_JointData, m_JointDataOld;

    float m_SpdDataOld,m_DelayDataOld;    // before data.
    float m_TypeDataOld;

    QVector<QLabel3*> m_vtPos, m_vtReal, m_vtPosName;
    QVector<QLabel4*> m_vtPosPic, m_vtRealPic;

    QTimer* timer;

    bool Save();
    void Save_CommonPos(cn_trans trans, cn_joint joint, float speed, float delay);
    void Reset();

    QString m_strDelayUnit, m_strSpdUnit;
    void Init_String();

    void SetMoveType(ENUM_MOVE_TYPE move_type);
    ENUM_MOVE_TYPE m_nMoveType;

    top_sub* top;
    void SubTop();          // Need SubTop

    void Display_SetPos(cn_trans t, cn_joint j);
    void Display_CurPos(float* t, float* j);

    dialog_call_position* dig_call_position;

    //bool Trans2Joint(cn_trans in_trans, cn_joint old_joint, cn_joint& new_joint);
    //bool Joint2Trans(cn_joint in_joint, cn_trans& new_trans);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_POSITION_TEACHING_H
