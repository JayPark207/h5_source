#ifndef PAGE_MAIN2_SETTING_H
#define PAGE_MAIN2_SETTING_H

#include <QWidget>

#include "global.h"

#include "dialog_timeset.h"
#include "dialog_version.h"
#include "dialog_language.h"
#include "dialog_useskip.h"
#include "dialog_network.h"

namespace Ui {
class page_main2_setting;
}

class page_main2_setting : public QWidget
{
    Q_OBJECT
    
public:
    explicit page_main2_setting(QWidget *parent = 0);
    ~page_main2_setting();

private:
    Ui::page_main2_setting *ui;

    dialog_timeset* dig_timeset;
    dialog_version* dig_version;
    dialog_language* dig_language;
    dialog_useskip* dig_useskip;
    dialog_network* dig_network;

    void Display_Flag(int lang_index);

    void UserLevel();

private slots:
    void onLang();
    void onSpecial();
    void onTimeSet();
    void onVersion();
    void onUseSkip();
    void onNetwork();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // PAGE_MAIN2_SETTING_H
