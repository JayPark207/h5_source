#include "global.h"

//#include <QApplication>
//#include <QFontDatabase>
//#include <QStackedWidget>
//#include <QDesktopWidget>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow* mw;
void Init_Font(int lang_no);
int nConnState=0;
void Init_Connection();

int main(int argc, char *argv[])
{
    //QApplication a(argc, argv);
    HyKey a(argc, argv);

    // add font ///////
    int _id;
    _id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/NanumGothicBold.ttf");   //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);
    _id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/NanumGothic.ttf");       //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);

    _id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/NotoSansThai-Medium.ttf");       //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);
    _id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/NotoSansThai-Bold.ttf");       //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);
    _id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/NotoSansThai-SemiBold.ttf");       //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);

    _id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/SIMHEI.TTF");       //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);

    /*_id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/NanumMyeongjoBold.ttf");   //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);
    _id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/NanumMyeongjo.ttf");       //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);*/
    /*_id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/KoPubWorld_Batang_Medium.ttf");       //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);
    _id = QFontDatabase::addApplicationFont("/mnt/mtd5/fonts/KoPubWorld_Dotum_Medium.ttf");       //font 등록
    if(_id >= 0)
        qDebug() << _id << QFontDatabase::applicationFontFamilies(_id).at(0);*/

    // end (add font) ////////

    mw = new MainWindow();
    mw->show();

    // coreServer Connection routine
    Init_Connection();

    // here config.
    //Init_Language(Config->Get_Language());
    mw->Init();

    //Init_Language(Param->Get(HyParam::LANGUAGE).toInt());
    int lang_num = Param->Get(HyParam::LANGUAGE).toInt();
    Language->Init(lang_num,
                   gTranslator,
                   gLocale);
    Init_Font(lang_num);

    // add here.

    return a.exec();
}

void Init_Font(int lang_no)
{
    gChange_Font((HyParam::PARAM_LANGUAGE)lang_no);

    /*// Font Global Change
    //font.setFamily("NanumMyeongjo");
    QFont font("NanumGothic");
    QApplication::setFont(font);*/
}

void Init_Connection()  // while 문 안에서 사용.
{
    CNRobo* pCon = CNRobo::getInstance();
    bool bLogin,bLock;
    QString addr = "127.0.0.1";
    char pw[] = "corerobot";
    bool bLoop = true;

    pCon->setWatchDogLimit(600000);

    while(bLoop)
    {
        usleep(10000);

        switch(nConnState)
        {
        case 0:
            pCon->setServerAddress(addr);
            pCon->startService();
            qDebug() << "Try to connect coreServer!";
            nConnState = 1;
            break;

        case 1:
            if(pCon->isConnected())
            {
                qDebug() << "Success to connect coreServer!";
                nConnState = 2;
            }
            break;

        case 2:
            pCon->login(pw);
            qDebug() << "Try to log-in coreServer!";
            nConnState = 3;
            break;

        case 3:
            pCon->getLoginStatus(&bLogin);
            if(bLogin)
            {
                qDebug() << "Success to log-in coreServer!";
                nConnState = 4;
            }
            break;

        case 4:
            pCon->lock(0);
            qDebug() << "Try to get control ticket!";
            nConnState = 5;
            break;

        case 5:
            pCon->getLockStatus(&bLock);
            if(bLock)
            {
                qDebug() << "Success to get control ticket!";
                nConnState = 6;
            }
            break;

        case 6:
            pCon->resetError();
            pCon->resetEcatError();
            qDebug() << "Reset Error!";
            nConnState = 100;
            break;

        case 100:
            qDebug() << "Success to Init Connection!";
            mw->WatchDog->start(500);
            mw->ReConnect->start(200);
            mw->tErrCheck->start(200);
            mw->topMain->m_Timer->start(300);
            bLoop = false;
            break;
        }
    }
}






// ============================================
// 여기는 ui관련 위젯을 제어하고 싶은 경우에 추가한다.
// global 클래스

void gSetMainPage(PAGE_INDEX index)
{
    g_PreMainPage = g_CurrentMainPage;
    g_CurrentMainPage = index;
    mw->StackedForm->setCurrentIndex(index);
}
PAGE_INDEX gGetMainPage()
{
    return g_CurrentMainPage;
}
PAGE_INDEX gGetPreMainPage()
{
    return g_PreMainPage;
}
void* gGetMainPage(PAGE_INDEX index)
{
    if(index >= mw->StackedForm->count())
        return 0;

    return (void*)mw->StackedForm->widget(index);
}

void gSetTopPage(TOP_INDEX index)
{
    g_CurrentTopPage = index;
    mw->StackedTop->setCurrentIndex(index);
}
TOP_INDEX gGetTopPage()
{
    return g_CurrentTopPage;
}
void* gGetTopPage(TOP_INDEX index)
{
    if(index >= mw->StackedTop->count())
        return 0;

    return (void*)mw->StackedTop->widget(index);
}

TOP_INDEX gGetPreTopPage()
{
    return g_PreTopPage;
}

void* gGetDialog(DIALOG_INDEX index)
{
    if(index >= mw->Dialogs.size())
        return 0;

    return (void*)mw->Dialogs[index];
}

// capture current window display & save png file at sdcard
// now left bottom button mapping.
void Capture()
{
    if(!UseSkip->Get(HyUseSkip::CAPTURE))
    {
        qDebug() << "Disabled Capture!";
        return;
    }

    if(!RM->IsSD())
    {
        qDebug() << "No SDCard!";
        return;
    }

    QPixmap cap = QPixmap::grabWindow(QApplication::desktop()->winId(),0,0,800,480);
    QDateTime date;

    QString path = "/sdcard/";
    path += date.currentDateTime().toString("yyyyMMddhhmmss");
    path += ".png";

    if(!cap.save(path,"PNG"))
    {
        qDebug() << "Capture Save Fail!" << path;
        return;
    }
    qDebug() << "Capture Save Success!" << path;
}



