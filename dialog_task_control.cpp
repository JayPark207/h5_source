#include "dialog_task_control.h"
#include "ui_dialog_task_control.h"

dialog_task_control::dialog_task_control(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_task_control)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtTask.clear();m_vtStatus.clear();
    m_vtTask.append(ui->tbTask);  m_vtStatus.append(ui->tbStatus);
    m_vtTask.append(ui->tbTask_2);m_vtStatus.append(ui->tbStatus_2);
    m_vtTask.append(ui->tbTask_3);m_vtStatus.append(ui->tbStatus_3);
    m_vtTask.append(ui->tbTask_4);m_vtStatus.append(ui->tbStatus_4);

    m_vtContPic.clear();m_vtCont.clear();
    m_vtContPic.append(ui->tbContPic);  m_vtCont.append(ui->tbCont);
    m_vtContPic.append(ui->tbContPic_2);m_vtCont.append(ui->tbCont_2);
    m_vtContPic.append(ui->tbContPic_3);m_vtCont.append(ui->tbCont_3);
    m_vtContPic.append(ui->tbContPic_4);m_vtCont.append(ui->tbCont_4);

    int i;
    for(i=0;i<m_vtContPic.count();i++)
    {
        connect(m_vtContPic[i],SIGNAL(mouse_release()),this,SLOT(onCont()));
        connect(m_vtCont[i],SIGNAL(mouse_press()),m_vtContPic[i],SLOT(press()));
        connect(m_vtCont[i],SIGNAL(mouse_release()),m_vtContPic[i],SLOT(release()));
    }

    m_vtAutoPic.clear();m_vtAuto.clear();
    m_vtAutoPic.append(ui->tbAutoPic);  m_vtAuto.append(ui->tbAuto);
    m_vtAutoPic.append(ui->tbAutoPic_2);m_vtAuto.append(ui->tbAuto_2);
    m_vtAutoPic.append(ui->tbAutoPic_3);m_vtAuto.append(ui->tbAuto_3);
    m_vtAutoPic.append(ui->tbAutoPic_4);m_vtAuto.append(ui->tbAuto_4);

    for(i=0;i<m_vtContPic.count();i++)
    {
        connect(m_vtAutoPic[i],SIGNAL(mouse_release()),this,SLOT(onAuto()));
        connect(m_vtAuto[i],SIGNAL(mouse_press()),m_vtAutoPic[i],SLOT(press()));
        connect(m_vtAuto[i],SIGNAL(mouse_release()),m_vtAutoPic[i],SLOT(release()));
    }

    m_vtAutoBox.clear();m_vtAutoChk.clear();
    m_vtAutoBox.append(ui->tbAutoBox);  m_vtAutoChk.append(ui->tbAutoChk);
    m_vtAutoBox.append(ui->tbAutoBox_2);m_vtAutoChk.append(ui->tbAutoChk_2);
    m_vtAutoBox.append(ui->tbAutoBox_3);m_vtAutoChk.append(ui->tbAutoChk_3);
    m_vtAutoBox.append(ui->tbAutoBox_4);m_vtAutoChk.append(ui->tbAutoChk_4);

    m_vtMacroPic.clear();m_vtMacro.clear();m_vtMacroIcon.clear();
    m_vtMacroPic.append(ui->tbMacroPic);  m_vtMacro.append(ui->tbMacro);  m_vtMacroIcon.append(ui->tbMacroIcon);
    m_vtMacroPic.append(ui->tbMacroPic_2);m_vtMacro.append(ui->tbMacro_2);m_vtMacroIcon.append(ui->tbMacroIcon_2);
    m_vtMacroPic.append(ui->tbMacroPic_3);m_vtMacro.append(ui->tbMacro_3);m_vtMacroIcon.append(ui->tbMacroIcon_3);
    m_vtMacroPic.append(ui->tbMacroPic_4);m_vtMacro.append(ui->tbMacro_4);m_vtMacroIcon.append(ui->tbMacroIcon_4);

    for(i=0;i<m_vtContPic.count();i++)
    {
        connect(m_vtMacroPic[i],SIGNAL(mouse_release()),this,SLOT(onMacro()));
        connect(m_vtMacroIcon[i],SIGNAL(mouse_press()),m_vtMacroPic[i],SLOT(press()));
        connect(m_vtMacroIcon[i],SIGNAL(mouse_release()),m_vtMacroPic[i],SLOT(release()));
    }

    connect(ui->btnVarInitPic,SIGNAL(mouse_release()),this,SLOT(onVarInit()));
    connect(ui->btnVarInit,SIGNAL(mouse_press()),ui->btnVarInitPic,SLOT(press()));
    connect(ui->btnVarInit,SIGNAL(mouse_release()),ui->btnVarInitPic,SLOT(release()));
    connect(ui->btnVarInitIcon,SIGNAL(mouse_press()),ui->btnVarInitPic,SLOT(press()));
    connect(ui->btnVarInitIcon,SIGNAL(mouse_release()),ui->btnVarInitPic,SLOT(release()));

    connect(ui->btnDataViewPic,SIGNAL(mouse_release()),this,SLOT(onDataView()));
    connect(ui->btnDataView,SIGNAL(mouse_press()),ui->btnDataViewPic,SLOT(press()));
    connect(ui->btnDataView,SIGNAL(mouse_release()),ui->btnDataViewPic,SLOT(release()));
    connect(ui->btnDataViewIcon,SIGNAL(mouse_press()),ui->btnDataViewPic,SLOT(press()));
    connect(ui->btnDataViewIcon,SIGNAL(mouse_release()),ui->btnDataViewPic,SLOT(release()));

    // initial
    pxmRunIcon.clear();
    m_bChangeMacro.clear();
    m_bChangeMacro.fill(false, m_vtMacro.count());
    dig_varmonitor = 0;

}
dialog_task_control::~dialog_task_control()
{
    delete ui;
}
void dialog_task_control::showEvent(QShowEvent *)
{
    Update();

    timer->start();
}
void dialog_task_control::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_task_control::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_task_control::onClose()
{
    emit accept();
}

void dialog_task_control::Update()
{
    QString str;
    int i;

    str = Param->Get(HyParam::SUBTASK_MACRO);
    m_Macro.clear();
    m_Macro = str.split(",");
    if(m_Macro.count() != m_vtTask.count())
    {
        qDebug() << "Macro Param Error!!";
        m_Macro.clear();
        for(i=0;i<m_vtTask.count();i++)
            m_Macro.append(tr("N/A"));
    }
    m_Macro[0] = SUBMAIN_MACRO; // fix.

    str = Param->Get(HyParam::SUBTASK_AUTORUN);
    m_AutoRun.clear();
    m_AutoRun = str.split(",");
    if(m_AutoRun.count() != m_vtTask.count())
    {
        qDebug() << "AutoRun Param Error!! Make all zero!!";
        m_AutoRun.clear();
        for(i=0;i<m_vtTask.count();i++)
            m_AutoRun.append("0");
    }

    // Sub Task 1 is fix. don't change.
    m_vtMacroPic[0]->setEnabled(false);
    m_vtMacroIcon[0]->setEnabled(false);
}

void dialog_task_control::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    CNR_TASK_STATUS task_status[m_vtTask.count()];

    for(int i=0;i<m_vtTask.count();i++)
    {
        task_status[i] = pCon->getTaskStatus(SUB_TASK+i);

        Display_RunPause(i, task_status[i]);
        Display_AutoRun(i, m_AutoRun[i]);
        Display_Macro(i, m_Macro[i]);
        Display_EnableControl(i, task_status[i]);
        Display_MacroChanged(i, m_bChangeMacro[i]);
    }

}

void dialog_task_control::Display_RunPause(int table_index, CNR_TASK_STATUS status)
{
    if(pxmRunIcon.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon99-5.png");   // STOP
        pxmRunIcon.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon99-3.png");   // RUN
        pxmRunIcon.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon99-4.png");   // PAUSE
        pxmRunIcon.append(QPixmap::fromImage(img));
    }

    if(status == CNR_TASK_RUNNING)
        m_vtCont[table_index]->setText(tr("Pause"));
    else
        m_vtCont[table_index]->setText(tr("Run"));

    m_vtStatus[table_index]->setPixmap(pxmRunIcon[status]);
}
void dialog_task_control::Display_AutoRun(int table_index, QString autorun)
{
    if(autorun.toInt() > 0)
    {
        m_vtAuto[table_index]->setText(tr("OFF"));
        m_vtAutoChk[table_index]->show();
    }
    else
    {
        m_vtAuto[table_index]->setText(tr("ON"));
        m_vtAutoChk[table_index]->hide();
    }
}
void dialog_task_control::Display_Macro(int table_index, QString macro)
{
    m_vtMacro[table_index]->setText(macro);
}

void dialog_task_control::Display_EnableControl(int table_index, CNR_TASK_STATUS status)
{
    bool enable = true;
    if(status == CNR_TASK_RUNNING)
        enable = false;

    m_vtAutoPic[table_index]->setEnabled(enable);
    m_vtAuto[table_index]->setEnabled(enable);

    if(table_index != 0)
    {
        m_vtMacroPic[table_index]->setEnabled(enable);
        m_vtMacroIcon[table_index]->setEnabled(enable);
    }
}

void dialog_task_control::Display_MacroChanged(int table_index, bool changed)
{
    m_vtTask[table_index]->setAutoFillBackground(changed);
}

void dialog_task_control::onCont()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtContPic.indexOf(btn);
    if(index < 0) return;

    int task_num = SUB_TASK + index;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    CNR_TASK_STATUS status = pCon->getTaskStatus(task_num);

    if(status == CNR_TASK_RUNNING)
    {
        pCon->holdProgram(task_num);
    }
    else if(status == CNR_TASK_STOPPED)
    {
        if(m_bChangeMacro[index])
        {
            qDebug() << "Macro Name Change";

            m_bChangeMacro[index] = false;
            ret = pCon->setCurProgram(task_num, m_Macro[index]);
            if(ret < 0)
            {
                qDebug() << "setCurProgram ret = " << ret << task_num << m_Macro[index];
                return;
            }
        }

        ret = pCon->continueProgram(task_num);
        if(ret < 0)
        {
            qDebug() << "continueProgram ret = " << ret << task_num;
            return;
        }
    }
    else    // NOT_USED
    {
        qDebug() << "Macro Not Used -> executeProgram";
        ret = pCon->executeProgram(task_num, m_Macro[index], -1);
        if(ret < 0)
        {
            qDebug() << "executeProgram ret = " << ret << task_num << m_Macro[index];
            return;
        }
    }
}
void dialog_task_control::onAuto()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtAutoPic.indexOf(btn);
    if(index < 0) return;

    if(m_AutoRun[index].toInt() > 0)
        m_AutoRun[index] = "0";
    else
        m_AutoRun[index] = "1";

    QString str = m_AutoRun.join(",");
    Param->Set(HyParam::SUBTASK_AUTORUN, str);
}
void dialog_task_control::onMacro()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtMacroPic.indexOf(btn);
    if(index < 0) return;
    if(index == 0) return; // [0] is fix.

    CNRobo* pCon = CNRobo::getInstance();

    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->tbMacroTitle->text());
    kb->m_kb->SetText(m_vtMacro[index]->text());

    if(kb->exec() == QDialog::Accepted)
    {
        QString str;
        str = kb->m_kb->GetText();
        if(!pCon->findProgram(str))
        {
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("Error")));
            msg->Message(tr("Not Found!"), str);
            msg->exec();
            return;
        }

        if(m_Macro[index] != str)
        {
            m_Macro[index] = str;
            QString macro = m_Macro.join(",");
            Param->Set(HyParam::SUBTASK_MACRO, macro);

            m_bChangeMacro[index] = true;
        }

    }

}
void dialog_task_control::onVarInit()
{
    Recipe->Set(HyRecipe::bSubInit, 1.0, false);
}
void dialog_task_control::onDataView()
{
    if(dig_varmonitor == 0)
        dig_varmonitor = new dialog_varmonitor();

    dig_varmonitor->exec();
}
