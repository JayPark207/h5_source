#include "dialog_insertmain_set.h"
#include "ui_dialog_insertmain_set.h"

dialog_insertmain_set::dialog_insertmain_set(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_insertmain_set)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));


    Stacked = new QStackedWidget(ui->frame);
    Stacked->setGeometry(ui->wigPage0->x(),
                         ui->wigPage0->y(),
                         ui->wigPage0->width(),
                         ui->wigPage0->height());

    Stacked->addWidget(ui->wigPage0);
    Stacked->addWidget(ui->wigPage1);
    Stacked->addWidget(ui->wigPage2);
    Stacked->setCurrentIndex(0);
    Stacked->show();

    m_vtSelPic.clear();m_vtSel.clear();
    m_vtSelPic.append(ui->btnSel0Pic);m_vtSel.append(ui->btnSel0);
    m_vtSelPic.append(ui->btnSel1Pic);m_vtSel.append(ui->btnSel1);
    m_vtSelPic.append(ui->btnSel2Pic);m_vtSel.append(ui->btnSel2);

    int i;
    for(i=0;i<m_vtSelPic.count();i++)
    {
        connect(m_vtSelPic[i],SIGNAL(mouse_release()),this,SLOT(onSel()));
        connect(m_vtSel[i],SIGNAL(mouse_press()),m_vtSelPic[i],SLOT(press()));
        connect(m_vtSel[i],SIGNAL(mouse_release()),m_vtSelPic[i],SLOT(release()));
    }

    // page array basic.
    m_vtName.clear();
    m_vtName.append(ui->tbName);
    m_vtName.append(ui->tbName_2);
    m_vtName.append(ui->tbName_3);

    m_vtNumPic.clear();m_vtNum.clear();
    m_vtNumPic.append(ui->tbNumPic);  m_vtNum.append(ui->tbNum);
    m_vtNumPic.append(ui->tbNumPic_2);m_vtNum.append(ui->tbNum_2);
    m_vtNumPic.append(ui->tbNumPic_3);m_vtNum.append(ui->tbNum_3);

    m_vtPitchPic.clear();m_vtPitch.clear();
    m_vtPitchPic.append(ui->tbPitchPic);  m_vtPitch.append(ui->tbPitch);
    m_vtPitchPic.append(ui->tbPitchPic_2);m_vtPitch.append(ui->tbPitch_2);
    m_vtPitchPic.append(ui->tbPitchPic_3);m_vtPitch.append(ui->tbPitch_3);

    m_vtOrder.clear();
    m_vtOrder.append(ui->lbXOrder);
    m_vtOrder.append(ui->lbYOrder);
    m_vtOrder.append(ui->lbZOrder);

    for(i=0;i<m_vtNumPic.count();i++)
    {
        connect(m_vtNumPic[i],SIGNAL(mouse_release()),this,SLOT(onNum()));
        connect(m_vtNum[i],SIGNAL(mouse_press()),m_vtNumPic[i],SLOT(press()));
        connect(m_vtNum[i],SIGNAL(mouse_release()),m_vtNumPic[i],SLOT(release()));
    }
    for(i=0;i<m_vtPitchPic.count();i++)
    {
        connect(m_vtPitchPic[i],SIGNAL(mouse_release()),this,SLOT(onPitch()));
        connect(m_vtPitch[i],SIGNAL(mouse_press()),m_vtPitchPic[i],SLOT(press()));
        connect(m_vtPitch[i],SIGNAL(mouse_release()),m_vtPitchPic[i],SLOT(release()));
    }


    // page order change.
    m_vtName2.clear();
    m_vtName2.append(ui->tbName2);
    m_vtName2.append(ui->tbName2_2);
    m_vtName2.append(ui->tbName2_3);

    m_vtOrder2.clear();
    m_vtOrder2.append(ui->lbXOrder_2);
    m_vtOrder2.append(ui->lbYOrder_2);
    m_vtOrder2.append(ui->lbZOrder_2);

    m_vtOrderXPic.clear();m_vtOrderX.clear();
    m_vtOrderXPic.append(ui->tbOrder1Pic);m_vtOrderX.append(ui->tbOrder1);
    m_vtOrderXPic.append(ui->tbOrder2Pic);m_vtOrderX.append(ui->tbOrder2);
    m_vtOrderXPic.append(ui->tbOrder3Pic);m_vtOrderX.append(ui->tbOrder3);

    m_vtOrderYPic.clear();m_vtOrderY.clear();
    m_vtOrderYPic.append(ui->tbOrder1Pic_2); m_vtOrderY.append(ui->tbOrder1_2);
    m_vtOrderYPic.append(ui->tbOrder2Pic_2); m_vtOrderY.append(ui->tbOrder2_2);
    m_vtOrderYPic.append(ui->tbOrder3Pic_2); m_vtOrderY.append(ui->tbOrder3_2);

    m_vtOrderZPic.clear();m_vtOrderZ.clear();
    m_vtOrderZPic.append(ui->tbOrder1Pic_3);m_vtOrderZ.append(ui->tbOrder1_3);
    m_vtOrderZPic.append(ui->tbOrder2Pic_3);m_vtOrderZ.append(ui->tbOrder2_3);
    m_vtOrderZPic.append(ui->tbOrder3Pic_3);m_vtOrderZ.append(ui->tbOrder3_3);


    for(i=0;i<m_vtOrderXPic.count();i++)
    {
        connect(m_vtOrderXPic[i],SIGNAL(mouse_release()),this,SLOT(onOrderChange()));
        connect(m_vtOrderX[i],SIGNAL(mouse_press()),m_vtOrderXPic[i],SLOT(press()));
        connect(m_vtOrderX[i],SIGNAL(mouse_release()),m_vtOrderXPic[i],SLOT(release()));

        connect(m_vtOrderYPic[i],SIGNAL(mouse_release()),this,SLOT(onOrderChange()));
        connect(m_vtOrderY[i],SIGNAL(mouse_press()),m_vtOrderYPic[i],SLOT(press()));
        connect(m_vtOrderY[i],SIGNAL(mouse_release()),m_vtOrderYPic[i],SLOT(release()));

        connect(m_vtOrderZPic[i],SIGNAL(mouse_release()),this,SLOT(onOrderChange()));
        connect(m_vtOrderZ[i],SIGNAL(mouse_press()),m_vtOrderZPic[i],SLOT(press()));
        connect(m_vtOrderZ[i],SIGNAL(mouse_release()),m_vtOrderZPic[i],SLOT(release()));
    }

    // page floor offset
    m_vtName3.clear();
    m_vtName3.append(ui->tbName3);
    m_vtName3.append(ui->tbName3_2);

    m_vtOffsetPic.clear();m_vtOffset.clear();
    m_vtOffsetPic.append(ui->tbOffsetPic);  m_vtOffset.append(ui->tbOffset);
    m_vtOffsetPic.append(ui->tbOffsetPic_2);m_vtOffset.append(ui->tbOffset_2);

    for(i=0;i<m_vtOffsetPic.count();i++)
    {
        connect(m_vtOffsetPic[i],SIGNAL(mouse_release()),this,SLOT(onZOffset()));
        connect(m_vtOffset[i],SIGNAL(mouse_press()),m_vtOffsetPic[i],SLOT(press()));
        connect(m_vtOffset[i],SIGNAL(mouse_release()),m_vtOffsetPic[i],SLOT(release()));
    }

}

dialog_insertmain_set::~dialog_insertmain_set()
{
    delete ui;
}
void dialog_insertmain_set::showEvent(QShowEvent *)
{
    Stacked->setCurrentIndex(0);
    Display_Sel(Stacked->currentIndex());

    Update(Stacked->currentIndex());
}
void dialog_insertmain_set::hideEvent(QHideEvent *)
{

}
void dialog_insertmain_set::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_insertmain_set::onClose()
{
    emit accept();
}

void dialog_insertmain_set::onSel()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtSelPic.indexOf(sel);
    if(index < 0) return;

    if(index < Stacked->count())
        Stacked->setCurrentIndex(index);

    Display_Sel(Stacked->currentIndex());
    Update(Stacked->currentIndex());
}

void dialog_insertmain_set::Display_Sel(int index)
{
    for(int i=0;i<m_vtSelPic.count();i++)
        m_vtSelPic[i]->setAutoFillBackground(i==index);
}

void dialog_insertmain_set::Update(int page_index)
{
    Vars.clear();
    Datas.clear();

    Vars.append(HyRecipe::vIArrXNum);
    Vars.append(HyRecipe::vIArrYNum);
    Vars.append(HyRecipe::vIArrZNum);

    Vars.append(HyRecipe::vIArrXPitch);
    Vars.append(HyRecipe::vIArrYPitch);
    Vars.append(HyRecipe::vIArrZPitch);

    Vars.append(HyRecipe::vIArrXOrder);
    Vars.append(HyRecipe::vIArrYOrder);
    Vars.append(HyRecipe::vIArrZOrder);

    Vars.append(HyRecipe::vIArrZOffsetX);
    Vars.append(HyRecipe::vIArrZOffsetY);

    if(Recipe->Gets(Vars, Datas))
    {
        // data parsing
        Data_Num.clear();
        Data_Pitch.clear();
        Data_Order.clear();
        Data_ZOffset.clear();
        int i;
        for(i=0;i<3;i++)
        {
            Data_Num.append(Datas[i]);
            Data_Pitch.append(Datas[3+i]);
            Data_Order.append(Datas[6+i]);
            if(i<2)
                Data_ZOffset.append(Datas[9+i]);
        }

        // display.
        Display_Data(page_index);

    }

}

void dialog_insertmain_set::Display_Data(int page_index)
{
    QString str;
    int i;

    switch(page_index)
    {
    case 0:
        for(i=0;i<Data_Num.count();i++)
        {
            str.sprintf("%d", (int)Data_Num[i]);
            m_vtNum[i]->setText(str);
            str.sprintf(FORMAT_POS, Data_Pitch[i]);
            m_vtPitch[i]->setText(str);
            str.sprintf("%d", (int)Data_Order[i] + 1);
            m_vtOrder[i]->setText(str);
        }
        break;

    case 1:
        for(i=0;i<Data_Order.count();i++)
        {
            str.sprintf("%d", (int)Data_Order[i] + 1);
            m_vtOrder2[i]->setText(str);
        }
        Display_Order(Data_Order);
        break;

    case 2:
        for(i=0;i<Data_ZOffset.count();i++)
        {
            str.sprintf(FORMAT_POS,Data_ZOffset[i]);
            m_vtOffset[i]->setText(str);
        }
        break;
    }
}

/** Array Basic **/
void dialog_insertmain_set::onNum()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtNumPic.indexOf(sel);
    if(index < 0) return;

    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetMinValue(1);
    np->m_numpad->SetSosuNum(0);

    double max_val[3] = {MAX_ARR_X_NUM, MAX_ARR_Y_NUM, MAX_ARR_Z_NUM};

    np->m_numpad->SetMaxValue(max_val[index]);

    QString str;
    str = m_vtName[index]->text() + " " + ui->tbNumTitle->text();
    np->m_numpad->SetTitle(str);
    np->m_numpad->SetNum(m_vtNum[index]->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(Vars[0+index], (float)np->m_numpad->GetNumDouble()))
            Update(Stacked->currentIndex());
    }
}

void dialog_insertmain_set::onPitch()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtPitchPic.indexOf(sel);
    if(index < 0) return;

    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetMinValue(-99999.999);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(SOSU_POS);

    QString str;
    str = m_vtName[index]->text() + " " + ui->tbNumTitle->text();
    np->m_numpad->SetTitle(str);
    np->m_numpad->SetNum(m_vtPitch[index]->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(Vars[3+index], (float)np->m_numpad->GetNumDouble()))
            Update(Stacked->currentIndex());
    }
}


/** Order Change **/
void dialog_insertmain_set::Display_Order(QVector<float> &order)
{
    if(order.size() != 3) return;

    for(int i=0;i<order.count();i++)
    {
        m_vtOrderXPic[i]->setAutoFillBackground(i==order[0]);
        m_vtOrderYPic[i]->setAutoFillBackground(i==order[1]);
        m_vtOrderZPic[i]->setAutoFillBackground(i==order[2]);
    }
}
void dialog_insertmain_set::ChangeOrderX(int order)
{
    int _XOrder = (int)Data_Order[0];
    int _YOrder = (int)Data_Order[1];
    int _ZOrder = (int)Data_Order[2];

    QVector<HyRecipe::RECIPE_NUMBER> var;
    QVector<float> data;
    var.clear();data.clear();

    var.append(Vars[6]);
    var.append(Vars[7]);
    var.append(Vars[8]);

    int x=_XOrder,y=_YOrder,z=_ZOrder;

    if(order == _XOrder) return;

    if(order == 0)
    {
        x=0;

        if(_YOrder == order)
        {
            if(_ZOrder == 1)
                y=2;
            else
                y=1;
        }
        else if(_ZOrder == order)
        {
            if(_YOrder == 1)
                z=2;
            else
                z=1;
        }
    }
    else if(order == 1)
    {
        x=1;

        if(_YOrder == order)
        {
            if(_ZOrder == 2)
                y=0;
            else
                y=2;
        }
        else if(_ZOrder == order)
        {
            if(_YOrder == 2)
                z=0;
            else
                z=2;
        }
    }
    else if(order == 2)
    {
        x=2;

        if(_YOrder == order)
        {
            if(_ZOrder == 0)
                y=1;
            else
                y=0;
        }
        else if(_ZOrder == order)
        {
            if(_YOrder == 0)
                z=1;
            else
                z=0;
        }
    }

    data.append(x);
    data.append(y);
    data.append(z);

    Recipe->Sets(var, data);
}

void dialog_insertmain_set::ChangeOrderY(int order)
{
    int _XOrder = (int)Data_Order[0];
    int _YOrder = (int)Data_Order[1];
    int _ZOrder = (int)Data_Order[2];

    QVector<HyRecipe::RECIPE_NUMBER> var;
    QVector<float> data;
    var.clear();data.clear();

    var.append(Vars[6]);
    var.append(Vars[7]);
    var.append(Vars[8]);

    int x=_XOrder,y=_YOrder,z=_ZOrder;

    if(order == _YOrder) return;

    if(order == 0)
    {
        y=0;

        if(_XOrder == order)
        {
            if(_ZOrder == 1)
                x=2;
            else
                x=1;
        }
        else if(_ZOrder == order)
        {
            if(_XOrder == 1)
                z=2;
            else
                z=1;
        }
    }
    else if(order == 1)
    {
        y=1;

        if(_XOrder == order)
        {
            if(_ZOrder == 2)
                x=0;
            else
                x=2;
        }
        else if(_ZOrder == order)
        {
            if(_XOrder == 2)
                z=0;
            else
                z=2;
        }
    }
    else if(order == 2)
    {
        y=2;

        if(_XOrder == order)
        {
            if(_ZOrder == 0)
                x=1;
            else
                x=0;
        }
        else if(_ZOrder == order)
        {
            if(_XOrder == 0)
                z=1;
            else
                z=0;
        }
    }

    data.append(x);
    data.append(y);
    data.append(z);

    Recipe->Sets(var, data);
}
void dialog_insertmain_set::ChangeOrderZ(int order)
{
    int _XOrder = (int)Data_Order[0];
    int _YOrder = (int)Data_Order[1];
    int _ZOrder = (int)Data_Order[2];

    QVector<HyRecipe::RECIPE_NUMBER> var;
    QVector<float> data;
    var.clear();data.clear();

    var.append(Vars[6]);
    var.append(Vars[7]);
    var.append(Vars[8]);

    int x=_XOrder,y=_YOrder,z=_ZOrder;

    if(order == _ZOrder) return;

    if(order == 0)
    {
        z=0;

        if(_XOrder == order)
        {
            if(_YOrder == 1)
                x=2;
            else
                x=1;
        }
        else if(_YOrder == order)
        {
            if(_XOrder == 1)
                y=2;
            else
                y=1;
        }
    }
    else if(order == 1)
    {
        z=1;

        if(_XOrder == order)
        {
            if(_YOrder == 2)
                x=0;
            else
                x=2;
        }
        else if(_YOrder == order)
        {
            if(_XOrder == 2)
                y=0;
            else
                y=2;
        }
    }
    else if(order == 2)
    {
        z=2;

        if(_XOrder == order)
        {
            if(_YOrder == 0)
                x=1;
            else
                x=0;
        }
        else if(_YOrder == order)
        {
            if(_XOrder == 0)
                y=1;
            else
                y=0;
        }
    }

    data.append(x);
    data.append(y);
    data.append(z);

    Recipe->Sets(var, data);
}

void dialog_insertmain_set::onOrderChange()
{
    QLabel4* sel = (QLabel4*)sender();

    int x_index = m_vtOrderXPic.indexOf(sel);
    int y_index = m_vtOrderYPic.indexOf(sel);
    int z_index = m_vtOrderZPic.indexOf(sel);

    if(x_index >= 0)
        ChangeOrderX(x_index);
    else if(y_index >= 0)
        ChangeOrderY(y_index);
    else if(z_index >= 0)
        ChangeOrderZ(z_index);

    Update(Stacked->currentIndex());
}

/** Floor Offset **/
void dialog_insertmain_set::onZOffset()
{
    QLabel4* sel = (QLabel4*)sender();

    int index = m_vtOffsetPic.indexOf(sel);
    if(index < 0) return;

    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-99999.999);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(SOSU_POS);

    QString str;
    str = m_vtName3[index]->text() + " " + ui->tbOffsetTitle->text();
    np->m_numpad->SetTitle(str);
    np->m_numpad->SetNum(m_vtOffset[index]->text().toDouble());
    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(Vars[9+index], (float)np->m_numpad->GetNumDouble()))
            Update(Stacked->currentIndex());
    }
}
