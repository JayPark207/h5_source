#include "dialog_network.h"
#include "ui_dialog_network.h"

dialog_network::dialog_network(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_network)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(300);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // main tab & panel
    Panel = new QStackedWidget(this);
    //Panel->setGeometry(0, 60, 800, 335);
    Panel->setGeometry(ui->wigPanel->x(),
                       ui->wigPanel->y(),
                       ui->wigPanel->width(),
                       ui->wigPanel->height());
    Panel->addWidget(ui->wigPanel); // ref.panel
    Panel->addWidget(ui->wigPanel_2);
    Panel->addWidget(ui->wigPanel_3);
    Panel->addWidget(ui->wigPanel_4);
    Panel->setCurrentIndex(0);

    m_vtTab.clear();m_vtTabPic.clear();m_vtTabLed.clear();
    m_vtTab.append(ui->btnTab);  m_vtTabPic.append(ui->btnTabPic);  m_vtTabLed.append(ui->lbTabLed);
    m_vtTab.append(ui->btnTab_2);m_vtTabPic.append(ui->btnTabPic_2);m_vtTabLed.append(ui->lbTabLed_2);
    m_vtTab.append(ui->btnTab_3);m_vtTabPic.append(ui->btnTabPic_3);m_vtTabLed.append(ui->lbTabLed_3);
    m_vtTab.append(ui->btnTab_4);m_vtTabPic.append(ui->btnTabPic_4);m_vtTabLed.append(ui->lbTabLed_4);

    int i;
    for(i=0;i<m_vtTabPic.count();i++)
    {
        connect(m_vtTabPic[i],SIGNAL(mouse_release()),this,SLOT(onTab()));
        connect(m_vtTab[i],SIGNAL(mouse_press()),m_vtTabPic[i],SLOT(press()));
        connect(m_vtTab[i],SIGNAL(mouse_release()),m_vtTabPic[i],SLOT(release()));
    }

    // pop panel
    m_vtCodePic.clear();m_vtCode.clear();m_vtCodeName.clear();
    m_vtCodePic.append(ui->btnCodePic);  m_vtCode.append(ui->btnCode);  m_vtCodeName.append(ui->lbCodeName);
    m_vtCodePic.append(ui->btnCodePic_2);m_vtCode.append(ui->btnCode_2);m_vtCodeName.append(ui->lbCodeName_2);
    m_vtCodePic.append(ui->btnCodePic_3);m_vtCode.append(ui->btnCode_3);m_vtCodeName.append(ui->lbCodeName_3);
    m_vtCodePic.append(ui->btnCodePic_4);m_vtCode.append(ui->btnCode_4);m_vtCodeName.append(ui->lbCodeName_4);

    for(i=0;i<m_vtCodePic.count();i++)
    {
        connect(m_vtCodePic[i],SIGNAL(mouse_release()),this,SLOT(onCode()));
        connect(m_vtCode[i],SIGNAL(mouse_press()),m_vtCodePic[i],SLOT(press()));
        connect(m_vtCode[i],SIGNAL(mouse_release()),m_vtCodePic[i],SLOT(release()));
    }

    connect(ui->btnCodeSavePic,SIGNAL(mouse_release()),this,SLOT(onSave_Code()));
    connect(ui->btnCodeSaveIcon,SIGNAL(mouse_press()),ui->btnCodeSavePic,SLOT(press()));
    connect(ui->btnCodeSaveIcon,SIGNAL(mouse_release()),ui->btnCodeSavePic,SLOT(release()));

    m_vtPopIpPic.clear();m_vtPopIp.clear();
    m_vtPopIpPic.append(ui->btnPopIpPic);  m_vtPopIp.append(ui->btnPopIp);
    m_vtPopIpPic.append(ui->btnPopIpPic_2);m_vtPopIp.append(ui->btnPopIp_2);
    m_vtPopIpPic.append(ui->btnPopIpPic_3);m_vtPopIp.append(ui->btnPopIp_3);
    m_vtPopIpPic.append(ui->btnPopIpPic_4);m_vtPopIp.append(ui->btnPopIp_4);

    for(i=0;i<m_vtPopIpPic.count();i++)
    {
        connect(m_vtPopIpPic[i],SIGNAL(mouse_release()),this,SLOT(onPopIp()));
        connect(m_vtPopIp[i],SIGNAL(mouse_press()),m_vtPopIpPic[i],SLOT(press()));
        connect(m_vtPopIp[i],SIGNAL(mouse_release()),m_vtPopIpPic[i],SLOT(release()));
    }

    connect(ui->btnPopUsePic,SIGNAL(mouse_release()),this,SLOT(onPopUse()));
    connect(ui->btnPopUse,SIGNAL(mouse_press()),ui->btnPopUsePic,SLOT(press()));
    connect(ui->btnPopUse,SIGNAL(mouse_release()),ui->btnPopUsePic,SLOT(release()));

    connect(ui->btnPopConPic,SIGNAL(mouse_release()),this,SLOT(onPopConnect()));
    connect(ui->btnPopCon,SIGNAL(mouse_press()),ui->btnPopConPic,SLOT(press()));
    connect(ui->btnPopCon,SIGNAL(mouse_release()),ui->btnPopConPic,SLOT(release()));

    connect(ui->btnPopDisPic,SIGNAL(mouse_release()),this,SLOT(onPopDisconnect()));
    connect(ui->btnPopDis,SIGNAL(mouse_press()),ui->btnPopDisPic,SLOT(press()));
    connect(ui->btnPopDis,SIGNAL(mouse_release()),ui->btnPopDisPic,SLOT(release()));

    connect(ui->btnPopPortPic,SIGNAL(mouse_release()),this,SLOT(onPopPort()));
    connect(ui->btnPopPort,SIGNAL(mouse_press()),ui->btnPopPortPic,SLOT(press()));
    connect(ui->btnPopPort,SIGNAL(mouse_release()),ui->btnPopPortPic,SLOT(release()));

    connect(ui->btnPopSavePic,SIGNAL(mouse_release()),this,SLOT(onSave_PopIp()));
    connect(ui->btnPopSaveIcon,SIGNAL(mouse_press()),ui->btnPopSavePic,SLOT(press()));
    connect(ui->btnPopSaveIcon,SIGNAL(mouse_release()),ui->btnPopSavePic,SLOT(release()));

    // hy server panel
    m_vtCode2Pic.clear();m_vtCode2.clear();m_vtCode2Name.clear();
    m_vtCode2Pic.append(ui->btnCode2Pic);  m_vtCode2.append(ui->btnCode2);  m_vtCode2Name.append(ui->lbCode2Name);
    m_vtCode2Pic.append(ui->btnCode2Pic_2);m_vtCode2.append(ui->btnCode2_2);m_vtCode2Name.append(ui->lbCode2Name_2);
    m_vtCode2Pic.append(ui->btnCode2Pic_3);m_vtCode2.append(ui->btnCode2_3);m_vtCode2Name.append(ui->lbCode2Name_3);
    m_vtCode2Pic.append(ui->btnCode2Pic_4);m_vtCode2.append(ui->btnCode2_4);m_vtCode2Name.append(ui->lbCode2Name_4);

    for(i=0;i<m_vtCode2Pic.count();i++)
    {
        connect(m_vtCode2Pic[i],SIGNAL(mouse_release()),this,SLOT(onCode2()));
        connect(m_vtCode2[i],SIGNAL(mouse_press()),m_vtCode2Pic[i],SLOT(press()));
        connect(m_vtCode2[i],SIGNAL(mouse_release()),m_vtCode2Pic[i],SLOT(release()));
    }

    connect(ui->btnCodeSavePic_2,SIGNAL(mouse_release()),this,SLOT(onSave_Code2()));
    connect(ui->btnCodeSaveIcon_2,SIGNAL(mouse_press()),ui->btnCodeSavePic_2,SLOT(press()));
    connect(ui->btnCodeSaveIcon_2,SIGNAL(mouse_release()),ui->btnCodeSavePic_2,SLOT(release()));

    m_vtHysIpPic.clear();m_vtHysIp.clear();
    m_vtHysIpPic.append(ui->btnHysIpPic);  m_vtHysIp.append(ui->btnHysIp);
    m_vtHysIpPic.append(ui->btnHysIpPic_2);m_vtHysIp.append(ui->btnHysIp_2);
    m_vtHysIpPic.append(ui->btnHysIpPic_3);m_vtHysIp.append(ui->btnHysIp_3);
    m_vtHysIpPic.append(ui->btnHysIpPic_4);m_vtHysIp.append(ui->btnHysIp_4);

    for(i=0;i<m_vtHysIpPic.count();i++)
    {
        connect(m_vtHysIpPic[i],SIGNAL(mouse_release()),this,SLOT(onHysIp()));
        connect(m_vtHysIp[i],SIGNAL(mouse_press()),m_vtHysIpPic[i],SLOT(press()));
        connect(m_vtHysIp[i],SIGNAL(mouse_release()),m_vtHysIpPic[i],SLOT(release()));
    }

    connect(ui->btnHysUsePic,SIGNAL(mouse_release()),this,SLOT(onHysUse()));
    connect(ui->btnHysUse,SIGNAL(mouse_press()),ui->btnHysUsePic,SLOT(press()));
    connect(ui->btnHysUse,SIGNAL(mouse_release()),ui->btnHysUsePic,SLOT(release()));

    connect(ui->btnHysConPic,SIGNAL(mouse_release()),this,SLOT(onHysConnect()));
    connect(ui->btnHysCon,SIGNAL(mouse_press()),ui->btnHysConPic,SLOT(press()));
    connect(ui->btnHysCon,SIGNAL(mouse_release()),ui->btnHysConPic,SLOT(release()));

    connect(ui->btnHysDisPic,SIGNAL(mouse_release()),this,SLOT(onHysDisconnect()));
    connect(ui->btnHysDis,SIGNAL(mouse_press()),ui->btnHysDisPic,SLOT(press()));
    connect(ui->btnHysDis,SIGNAL(mouse_release()),ui->btnHysDisPic,SLOT(release()));

    connect(ui->btnHysPortPic,SIGNAL(mouse_release()),this,SLOT(onHysPort()));
    connect(ui->btnHysPort,SIGNAL(mouse_press()),ui->btnHysPortPic,SLOT(press()));
    connect(ui->btnHysPort,SIGNAL(mouse_release()),ui->btnHysPortPic,SLOT(release()));

    connect(ui->btnHysSavePic,SIGNAL(mouse_release()),this,SLOT(onSave_HysIp()));
    connect(ui->btnHysSaveIcon,SIGNAL(mouse_press()),ui->btnHysSavePic,SLOT(press()));
    connect(ui->btnHysSaveIcon,SIGNAL(mouse_release()),ui->btnHysSavePic,SLOT(release()));


    // serial tab & panel
    SrPanel = new QStackedWidget(ui->wigPanel_3);
    SrPanel->setGeometry(ui->wigSrPanel->x(),
                         ui->wigSrPanel->y(),
                         ui->wigSrPanel->width(),
                         ui->wigSrPanel->height());
    SrPanel->addWidget(ui->wigSrPanel);
    SrPanel->addWidget(ui->wigSrPanel_2);
    SrPanel->addWidget(ui->wigSrPanel_3);
    SrPanel->addWidget(ui->wigSrPanel_4);
    SrPanel->setCurrentIndex(0);

    m_vtSrTab.clear();m_vtSrTabPic.clear();m_vtSrTabLed.clear();
    m_vtSrTab.append(ui->btnSrTab);  m_vtSrTabPic.append(ui->btnSrTabPic);  m_vtSrTabLed.append(ui->lbSrTabLed);
    m_vtSrTab.append(ui->btnSrTab_2);m_vtSrTabPic.append(ui->btnSrTabPic_2);m_vtSrTabLed.append(ui->lbSrTabLed_2);
    m_vtSrTab.append(ui->btnSrTab_3);m_vtSrTabPic.append(ui->btnSrTabPic_3);m_vtSrTabLed.append(ui->lbSrTabLed_3);
    m_vtSrTab.append(ui->btnSrTab_4);m_vtSrTabPic.append(ui->btnSrTabPic_4);m_vtSrTabLed.append(ui->lbSrTabLed_4);

    for(i=0;i<m_vtSrTabPic.count();i++)
    {
        connect(m_vtSrTabPic[i],SIGNAL(mouse_release()),this,SLOT(onSrTab()));
        connect(m_vtSrTab[i],SIGNAL(mouse_press()),m_vtSrTabPic[i],SLOT(press()));
        connect(m_vtSrTab[i],SIGNAL(mouse_release()),m_vtSrTabPic[i],SLOT(release()));
    }

    connect(ui->btnSrConPic,SIGNAL(mouse_release()),this,SLOT(onSrConnect()));
    connect(ui->btnSrCon,SIGNAL(mouse_press()),ui->btnSrConPic,SLOT(press()));
    connect(ui->btnSrCon,SIGNAL(mouse_release()),ui->btnSrConPic,SLOT(release()));

    connect(ui->btnSrDisPic,SIGNAL(mouse_release()),this,SLOT(onSrDisconnect()));
    connect(ui->btnSrDis,SIGNAL(mouse_press()),ui->btnSrDisPic,SLOT(press()));
    connect(ui->btnSrDis,SIGNAL(mouse_release()),ui->btnSrDisPic,SLOT(release()));

    m_vtSrCountDispPic.clear();m_vtSrCountDisp.clear();
    m_vtSrCountDispPic.append(ui->btnCountDispPic);  m_vtSrCountDisp.append(ui->btnCountDisp);
    m_vtSrCountDispPic.append(ui->btnCountDispPic_2);m_vtSrCountDisp.append(ui->btnCountDisp_2);
    m_vtSrCountDispPic.append(ui->btnCountDispPic_3);m_vtSrCountDisp.append(ui->btnCountDisp_3);
    m_vtSrCountDispPic.append(ui->btnCountDispPic_4);m_vtSrCountDisp.append(ui->btnCountDisp_4);
    m_vtSrCountDispPic.append(ui->btnCountDispPic_5);m_vtSrCountDisp.append(ui->btnCountDisp_5);
    m_vtSrCountDispPic.append(ui->btnCountDispPic_6);m_vtSrCountDisp.append(ui->btnCountDisp_6);

    for(i=0;i<m_vtSrCountDispPic.count();i++)
    {
        connect(m_vtSrCountDispPic[i],SIGNAL(mouse_release()),this,SLOT(onCountDisplay()));
        connect(m_vtSrCountDisp[i],SIGNAL(mouse_press()),m_vtSrCountDispPic[i],SLOT(press()));
        connect(m_vtSrCountDisp[i],SIGNAL(mouse_release()),m_vtSrCountDispPic[i],SLOT(release()));
    }

    connect(ui->btnErrCodePic,SIGNAL(mouse_release()),this,SLOT(onErrorCode()));
    connect(ui->btnErrCode,SIGNAL(mouse_press()),ui->btnErrCodePic,SLOT(press()));
    connect(ui->btnErrCode,SIGNAL(mouse_release()),ui->btnErrCodePic,SLOT(release()));

    connect(ui->btnErrDispPic,SIGNAL(mouse_release()),this,SLOT(onErrorDisplay()));
    connect(ui->btnErrDisp,SIGNAL(mouse_press()),ui->btnErrDispPic,SLOT(press()));
    connect(ui->btnErrDisp,SIGNAL(mouse_release()),ui->btnErrDispPic,SLOT(release()));

    // weight
    m_vtWName.clear();m_vtWVal.clear();
    m_vtWName.append(ui->lbWName);  m_vtWVal.append(ui->lbWVal);
    m_vtWName.append(ui->lbWName_2);m_vtWVal.append(ui->lbWVal_2);
    m_vtWName.append(ui->lbWName_3);m_vtWVal.append(ui->lbWVal_3);
    m_vtWName.append(ui->lbWName_4);m_vtWVal.append(ui->lbWVal_4);

    m_vtWUsePic.clear();m_vtWUse.clear();
    m_vtWUsePic.append(ui->btnWUsePic);  m_vtWUse.append(ui->btnWUse);
    m_vtWUsePic.append(ui->btnWUsePic_2);m_vtWUse.append(ui->btnWUse_2);
    m_vtWUsePic.append(ui->btnWUsePic_3);m_vtWUse.append(ui->btnWUse_3);
    m_vtWUsePic.append(ui->btnWUsePic_4);m_vtWUse.append(ui->btnWUse_4);

    m_vtWMinPic.clear();m_vtWMin.clear();
    m_vtWMinPic.append(ui->btnWMinPic);  m_vtWMin.append(ui->btnWMin);
    m_vtWMinPic.append(ui->btnWMinPic_2);m_vtWMin.append(ui->btnWMin_2);
    m_vtWMinPic.append(ui->btnWMinPic_3);m_vtWMin.append(ui->btnWMin_3);
    m_vtWMinPic.append(ui->btnWMinPic_4);m_vtWMin.append(ui->btnWMin_4);

    m_vtWMaxPic.clear();m_vtWMax.clear();
    m_vtWMaxPic.append(ui->btnWMaxPic);  m_vtWMax.append(ui->btnWMax);
    m_vtWMaxPic.append(ui->btnWMaxPic_2);m_vtWMax.append(ui->btnWMax_2);
    m_vtWMaxPic.append(ui->btnWMaxPic_3);m_vtWMax.append(ui->btnWMax_3);
    m_vtWMaxPic.append(ui->btnWMaxPic_4);m_vtWMax.append(ui->btnWMax_4);

    for(i=0;i<m_vtWUsePic.count();i++)
    {
        connect(m_vtWUsePic[i],SIGNAL(mouse_release()),this,SLOT(onWUse()));
        connect(m_vtWUse[i],SIGNAL(mouse_press()),m_vtWUsePic[i],SLOT(press()));
        connect(m_vtWUse[i],SIGNAL(mouse_release()),m_vtWUsePic[i],SLOT(release()));
    }
    for(i=0;i<m_vtWMinPic.count();i++)
    {
        connect(m_vtWMinPic[i],SIGNAL(mouse_release()),this,SLOT(onWMin()));
        connect(m_vtWMin[i],SIGNAL(mouse_press()),m_vtWMinPic[i],SLOT(press()));
        connect(m_vtWMin[i],SIGNAL(mouse_release()),m_vtWMinPic[i],SLOT(release()));
    }
    for(i=0;i<m_vtWMaxPic.count();i++)
    {
        connect(m_vtWMaxPic[i],SIGNAL(mouse_release()),this,SLOT(onWMax()));
        connect(m_vtWMax[i],SIGNAL(mouse_press()),m_vtWMaxPic[i],SLOT(press()));
        connect(m_vtWMax[i],SIGNAL(mouse_release()),m_vtWMaxPic[i],SLOT(release()));
    }

    connect(ui->btnWSavePic,SIGNAL(mouse_release()),this,SLOT(onWSave()));
    connect(ui->btnWSaveIcon,SIGNAL(mouse_press()),ui->btnWSavePic,SLOT(press()));
    connect(ui->btnWSaveIcon,SIGNAL(mouse_release()),ui->btnWSavePic,SLOT(release()));

    connect(ui->btnWMeasurePic,SIGNAL(mouse_release()),this,SLOT(onWMeasure()));
    connect(ui->btnWMeasure,SIGNAL(mouse_press()),ui->btnWMeasurePic,SLOT(press()));
    connect(ui->btnWMeasure,SIGNAL(mouse_release()),ui->btnWMeasurePic,SLOT(release()));

    connect(ui->btnWResetPic,SIGNAL(mouse_release()),this,SLOT(onWReset()));
    connect(ui->btnWReset,SIGNAL(mouse_press()),ui->btnWResetPic,SLOT(press()));
    connect(ui->btnWReset,SIGNAL(mouse_release()),ui->btnWResetPic,SLOT(release()));

    connect(ui->btnWOffsetPic,SIGNAL(mouse_release()),this,SLOT(onWOffset()));
    connect(ui->btnWOffset,SIGNAL(mouse_press()),ui->btnWOffsetPic,SLOT(press()));
    connect(ui->btnWOffset,SIGNAL(mouse_release()),ui->btnWOffsetPic,SLOT(release()));

    // temp.
    m_vtTName.clear();m_vtTVal.clear();
    m_vtTName.append(ui->lbTName);  m_vtTVal.append(ui->lbTVal);
    m_vtTName.append(ui->lbTName_2);m_vtTVal.append(ui->lbTVal_2);
    m_vtTName.append(ui->lbTName_3);m_vtTVal.append(ui->lbTVal_3);
    m_vtTName.append(ui->lbTName_4);m_vtTVal.append(ui->lbTVal_4);

    m_vtTUsePic.clear();m_vtTUse.clear();
    m_vtTUsePic.append(ui->btnTUsePic);  m_vtTUse.append(ui->btnTUse);
    m_vtTUsePic.append(ui->btnTUsePic_2);m_vtTUse.append(ui->btnTUse_2);
    m_vtTUsePic.append(ui->btnTUsePic_3);m_vtTUse.append(ui->btnTUse_3);
    m_vtTUsePic.append(ui->btnTUsePic_4);m_vtTUse.append(ui->btnTUse_4);

    m_vtTMinPic.clear();m_vtTMin.clear();
    m_vtTMinPic.append(ui->btnTMinPic);  m_vtTMin.append(ui->btnTMin);
    m_vtTMinPic.append(ui->btnTMinPic_2);m_vtTMin.append(ui->btnTMin_2);
    m_vtTMinPic.append(ui->btnTMinPic_3);m_vtTMin.append(ui->btnTMin_3);
    m_vtTMinPic.append(ui->btnTMinPic_4);m_vtTMin.append(ui->btnTMin_4);

    m_vtTMaxPic.clear();m_vtTMax.clear();
    m_vtTMaxPic.append(ui->btnTMaxPic);  m_vtTMax.append(ui->btnTMax);
    m_vtTMaxPic.append(ui->btnTMaxPic_2);m_vtTMax.append(ui->btnTMax_2);
    m_vtTMaxPic.append(ui->btnTMaxPic_3);m_vtTMax.append(ui->btnTMax_3);
    m_vtTMaxPic.append(ui->btnTMaxPic_4);m_vtTMax.append(ui->btnTMax_4);

    for(i=0;i<m_vtTUsePic.count();i++)
    {
        connect(m_vtTUsePic[i],SIGNAL(mouse_release()),this,SLOT(onTUse()));
        connect(m_vtTUse[i],SIGNAL(mouse_press()),m_vtTUsePic[i],SLOT(press()));
        connect(m_vtTUse[i],SIGNAL(mouse_release()),m_vtTUsePic[i],SLOT(release()));
    }
    for(i=0;i<m_vtTMinPic.count();i++)
    {
        connect(m_vtTMinPic[i],SIGNAL(mouse_release()),this,SLOT(onTMin()));
        connect(m_vtTMin[i],SIGNAL(mouse_press()),m_vtTMinPic[i],SLOT(press()));
        connect(m_vtTMin[i],SIGNAL(mouse_release()),m_vtTMinPic[i],SLOT(release()));
    }
    for(i=0;i<m_vtTMaxPic.count();i++)
    {
        connect(m_vtTMaxPic[i],SIGNAL(mouse_release()),this,SLOT(onTMax()));
        connect(m_vtTMax[i],SIGNAL(mouse_press()),m_vtTMaxPic[i],SLOT(press()));
        connect(m_vtTMax[i],SIGNAL(mouse_release()),m_vtTMaxPic[i],SLOT(release()));
    }

    connect(ui->btnTSavePic,SIGNAL(mouse_release()),this,SLOT(onTSave()));
    connect(ui->btnTSaveIcon,SIGNAL(mouse_press()),ui->btnTSavePic,SLOT(press()));
    connect(ui->btnTSaveIcon,SIGNAL(mouse_release()),ui->btnTSavePic,SLOT(release()));

    connect(ui->btnTMeasurePic,SIGNAL(mouse_release()),this,SLOT(onTMeasure()));
    connect(ui->btnTMeasure,SIGNAL(mouse_press()),ui->btnTMeasurePic,SLOT(press()));
    connect(ui->btnTMeasure,SIGNAL(mouse_release()),ui->btnTMeasurePic,SLOT(release()));

    connect(ui->btnTResetPic,SIGNAL(mouse_release()),this,SLOT(onTReset()));
    connect(ui->btnTReset,SIGNAL(mouse_press()),ui->btnTResetPic,SLOT(press()));
    connect(ui->btnTReset,SIGNAL(mouse_release()),ui->btnTResetPic,SLOT(release()));


    // e-sensor
    m_vtEName.clear();m_vtEVal.clear();
    m_vtEName.append(ui->lbEName);  m_vtEVal.append(ui->lbEVal);
    m_vtEName.append(ui->lbEName_2);m_vtEVal.append(ui->lbEVal_2);
    m_vtEName.append(ui->lbEName_3);m_vtEVal.append(ui->lbEVal_3);
    m_vtEName.append(ui->lbEName_4);m_vtEVal.append(ui->lbEVal_4);

    m_vtEUsePic.clear();m_vtEUse.clear();
    m_vtEUsePic.append(ui->btnEUsePic);  m_vtEUse.append(ui->btnEUse);
    m_vtEUsePic.append(ui->btnEUsePic_2);m_vtEUse.append(ui->btnEUse_2);
    m_vtEUsePic.append(ui->btnEUsePic_3);m_vtEUse.append(ui->btnEUse_3);
    m_vtEUsePic.append(ui->btnEUsePic_4);m_vtEUse.append(ui->btnEUse_4);

    m_vtEMinPic.clear();m_vtEMin.clear();
    m_vtEMinPic.append(ui->btnEMinPic);  m_vtEMin.append(ui->btnEMin);
    m_vtEMinPic.append(ui->btnEMinPic_2);m_vtEMin.append(ui->btnEMin_2);
    m_vtEMinPic.append(ui->btnEMinPic_3);m_vtEMin.append(ui->btnEMin_3);
    m_vtEMinPic.append(ui->btnEMinPic_4);m_vtEMin.append(ui->btnEMin_4);

    m_vtEMaxPic.clear();m_vtEMax.clear();
    m_vtEMaxPic.append(ui->btnEMaxPic);  m_vtEMax.append(ui->btnEMax);
    m_vtEMaxPic.append(ui->btnEMaxPic_2);m_vtEMax.append(ui->btnEMax_2);
    m_vtEMaxPic.append(ui->btnEMaxPic_3);m_vtEMax.append(ui->btnEMax_3);
    m_vtEMaxPic.append(ui->btnEMaxPic_4);m_vtEMax.append(ui->btnEMax_4);

    for(i=0;i<m_vtEUsePic.count();i++)
    {
        connect(m_vtEUsePic[i],SIGNAL(mouse_release()),this,SLOT(onEUse()));
        connect(m_vtEUse[i],SIGNAL(mouse_press()),m_vtEUsePic[i],SLOT(press()));
        connect(m_vtEUse[i],SIGNAL(mouse_release()),m_vtEUsePic[i],SLOT(release()));
    }
    for(i=0;i<m_vtEMinPic.count();i++)
    {
        connect(m_vtEMinPic[i],SIGNAL(mouse_release()),this,SLOT(onEMin()));
        connect(m_vtEMin[i],SIGNAL(mouse_press()),m_vtEMinPic[i],SLOT(press()));
        connect(m_vtEMin[i],SIGNAL(mouse_release()),m_vtEMinPic[i],SLOT(release()));
    }
    for(i=0;i<m_vtEMaxPic.count();i++)
    {
        connect(m_vtEMaxPic[i],SIGNAL(mouse_release()),this,SLOT(onEMax()));
        connect(m_vtEMax[i],SIGNAL(mouse_press()),m_vtEMaxPic[i],SLOT(press()));
        connect(m_vtEMax[i],SIGNAL(mouse_release()),m_vtEMaxPic[i],SLOT(release()));
    }

    connect(ui->btnESavePic,SIGNAL(mouse_release()),this,SLOT(onESave()));
    connect(ui->btnESaveIcon,SIGNAL(mouse_press()),ui->btnESavePic,SLOT(press()));
    connect(ui->btnESaveIcon,SIGNAL(mouse_release()),ui->btnESavePic,SLOT(release()));

    connect(ui->btnEMeasurePic,SIGNAL(mouse_release()),this,SLOT(onEMeasure()));
    connect(ui->btnEMeasure,SIGNAL(mouse_press()),ui->btnEMeasurePic,SLOT(press()));
    connect(ui->btnEMeasure,SIGNAL(mouse_release()),ui->btnEMeasurePic,SLOT(release()));

    connect(ui->btnEResetPic,SIGNAL(mouse_release()),this,SLOT(onEReset()));
    connect(ui->btnEReset,SIGNAL(mouse_press()),ui->btnEResetPic,SLOT(press()));
    connect(ui->btnEReset,SIGNAL(mouse_release()),ui->btnEResetPic,SLOT(release()));


    // test tab & panel
    TsPanel = new QStackedWidget(ui->wigPanel_4);
    TsPanel->setGeometry(ui->wigTsPanel->x(),
                         ui->wigTsPanel->y(),
                         ui->wigTsPanel->width(),
                         ui->wigTsPanel->height());
    TsPanel->addWidget(ui->wigTsPanel);
    TsPanel->addWidget(ui->wigTsPanel_2);
    TsPanel->setCurrentIndex(0);

    m_vtTsTab.clear();m_vtTsTabPic.clear();m_vtTsTabLed.clear();
    m_vtTsTab.append(ui->btnTsTab);  m_vtTsTabPic.append(ui->btnTsTabPic);  m_vtTsTabLed.append(ui->lbTsTabLed);
    m_vtTsTab.append(ui->btnTsTab_2);m_vtTsTabPic.append(ui->btnTsTabPic_2);m_vtTsTabLed.append(ui->lbTsTabLed_2);

    for(i=0;i<m_vtTsTabPic.count();i++)
    {
        connect(m_vtTsTabPic[i],SIGNAL(mouse_release()),this,SLOT(onTsTab()));
        connect(m_vtTsTab[i],SIGNAL(mouse_press()),m_vtTsTabPic[i],SLOT(press()));
        connect(m_vtTsTab[i],SIGNAL(mouse_release()),m_vtTsTabPic[i],SLOT(release()));
    }

    // test-pop
    m_vtTpNo.clear();m_vtTpName.clear();
    m_vtTpNo.append(ui->lbTpNo);  m_vtTpName.append(ui->lbTpName);
    m_vtTpNo.append(ui->lbTpNo_2);m_vtTpName.append(ui->lbTpName_2);
    m_vtTpNo.append(ui->lbTpNo_3);m_vtTpName.append(ui->lbTpName_3);
    m_vtTpNo.append(ui->lbTpNo_4);m_vtTpName.append(ui->lbTpName_4);
    m_vtTpNo.append(ui->lbTpNo_5);m_vtTpName.append(ui->lbTpName_5);
    m_vtTpNo.append(ui->lbTpNo_6);m_vtTpName.append(ui->lbTpName_6);

    m_vtTpData.clear();m_vtTpDataPic.clear();
    m_vtTpData.append(ui->btnTpData);  m_vtTpDataPic.append(ui->btnTpDataPic);
    m_vtTpData.append(ui->btnTpData_2);m_vtTpDataPic.append(ui->btnTpDataPic_2);
    m_vtTpData.append(ui->btnTpData_3);m_vtTpDataPic.append(ui->btnTpDataPic_3);
    m_vtTpData.append(ui->btnTpData_4);m_vtTpDataPic.append(ui->btnTpDataPic_4);
    m_vtTpData.append(ui->btnTpData_5);m_vtTpDataPic.append(ui->btnTpDataPic_5);
    m_vtTpData.append(ui->btnTpData_6);m_vtTpDataPic.append(ui->btnTpDataPic_6);

    for(i=0;i<m_vtTpNo.count();i++)
    {
        connect(m_vtTpDataPic[i],SIGNAL(mouse_release()),this,SLOT(onTpData()));
        connect(m_vtTpData[i],SIGNAL(mouse_press()),m_vtTpDataPic[i],SLOT(press()));
        connect(m_vtTpData[i],SIGNAL(mouse_release()),m_vtTpDataPic[i],SLOT(release()));
    }

    connect(ui->btnTpUpPic,SIGNAL(mouse_release()),this,SLOT(onTpUp()));
    connect(ui->btnTpUpIcon,SIGNAL(mouse_press()),ui->btnTpUpPic,SLOT(press()));
    connect(ui->btnTpUpIcon,SIGNAL(mouse_release()),ui->btnTpUpPic,SLOT(release()));

    connect(ui->btnTpDownPic,SIGNAL(mouse_release()),this,SLOT(onTpDown()));
    connect(ui->btnTpDownIcon,SIGNAL(mouse_press()),ui->btnTpDownPic,SLOT(press()));
    connect(ui->btnTpDownIcon,SIGNAL(mouse_release()),ui->btnTpDownPic,SLOT(release()));

    connect(ui->btnTpSavePic,SIGNAL(mouse_release()),this,SLOT(onTpSave()));
    connect(ui->btnTpSaveIcon,SIGNAL(mouse_press()),ui->btnTpSavePic,SLOT(press()));
    connect(ui->btnTpSaveIcon,SIGNAL(mouse_release()),ui->btnTpSavePic,SLOT(release()));

    connect(ui->btnTpSendPic,SIGNAL(mouse_release()),this,SLOT(onTpSend()));
    connect(ui->btnTpSend,SIGNAL(mouse_press()),ui->btnTpSendPic,SLOT(press()));
    connect(ui->btnTpSend,SIGNAL(mouse_release()),ui->btnTpSendPic,SLOT(release()));

    connect(ui->btnTpResetPic,SIGNAL(mouse_release()),this,SLOT(onTpReset()));
    connect(ui->btnTpReset,SIGNAL(mouse_press()),ui->btnTpResetPic,SLOT(press()));
    connect(ui->btnTpReset,SIGNAL(mouse_release()),ui->btnTpResetPic,SLOT(release()));

    // test-hyserver
    m_vtThNo.clear();m_vtThName.clear();
    m_vtThNo.append(ui->lbThNo);  m_vtThName.append(ui->lbThName);
    m_vtThNo.append(ui->lbThNo_2);m_vtThName.append(ui->lbThName_2);
    m_vtThNo.append(ui->lbThNo_3);m_vtThName.append(ui->lbThName_3);
    m_vtThNo.append(ui->lbThNo_4);m_vtThName.append(ui->lbThName_4);
    m_vtThNo.append(ui->lbThNo_5);m_vtThName.append(ui->lbThName_5);
    m_vtThNo.append(ui->lbThNo_6);m_vtThName.append(ui->lbThName_6);

    m_vtThData.clear();m_vtThDataPic.clear();
    m_vtThData.append(ui->btnThData);  m_vtThDataPic.append(ui->btnThDataPic);
    m_vtThData.append(ui->btnThData_2);m_vtThDataPic.append(ui->btnThDataPic_2);
    m_vtThData.append(ui->btnThData_3);m_vtThDataPic.append(ui->btnThDataPic_3);
    m_vtThData.append(ui->btnThData_4);m_vtThDataPic.append(ui->btnThDataPic_4);
    m_vtThData.append(ui->btnThData_5);m_vtThDataPic.append(ui->btnThDataPic_5);
    m_vtThData.append(ui->btnThData_6);m_vtThDataPic.append(ui->btnThDataPic_6);

    for(i=0;i<m_vtThNo.count();i++)
    {
        connect(m_vtThDataPic[i],SIGNAL(mouse_release()),this,SLOT(onThData()));
        connect(m_vtThData[i],SIGNAL(mouse_press()),m_vtThDataPic[i],SLOT(press()));
        connect(m_vtThData[i],SIGNAL(mouse_release()),m_vtThDataPic[i],SLOT(release()));
    }

    connect(ui->btnThUpPic,SIGNAL(mouse_release()),this,SLOT(onThUp()));
    connect(ui->btnThUpIcon,SIGNAL(mouse_press()),ui->btnThUpPic,SLOT(press()));
    connect(ui->btnThUpIcon,SIGNAL(mouse_release()),ui->btnThUpPic,SLOT(release()));

    connect(ui->btnThDownPic,SIGNAL(mouse_release()),this,SLOT(onThDown()));
    connect(ui->btnThDownIcon,SIGNAL(mouse_press()),ui->btnThDownPic,SLOT(press()));
    connect(ui->btnThDownIcon,SIGNAL(mouse_release()),ui->btnThDownPic,SLOT(release()));

    connect(ui->btnThSavePic,SIGNAL(mouse_release()),this,SLOT(onThSave()));
    connect(ui->btnThSaveIcon,SIGNAL(mouse_press()),ui->btnThSavePic,SLOT(press()));
    connect(ui->btnThSaveIcon,SIGNAL(mouse_release()),ui->btnThSavePic,SLOT(release()));

    connect(ui->btnThSendPic,SIGNAL(mouse_release()),this,SLOT(onThSend()));
    connect(ui->btnThSend,SIGNAL(mouse_press()),ui->btnThSendPic,SLOT(press()));
    connect(ui->btnThSend,SIGNAL(mouse_release()),ui->btnThSendPic,SLOT(release()));

    connect(ui->btnThResetPic,SIGNAL(mouse_release()),this,SLOT(onThReset()));
    connect(ui->btnThReset,SIGNAL(mouse_press()),ui->btnThResetPic,SLOT(press()));
    connect(ui->btnThReset,SIGNAL(mouse_release()),ui->btnThResetPic,SLOT(release()));


    // init & clear
    m_bWMeasure = false;
    m_bWReset = false;
    m_bTMeasure = false;
    m_bEMeasure = false;
    top = 0;

}
dialog_network::~dialog_network()
{
    delete ui;
}
void dialog_network::showEvent(QShowEvent *)
{
    Update();
    timer->start();

    SubTop();
}
void dialog_network::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_network::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_network::onClose()
{
    emit accept();
}

void dialog_network::Update()
{
    Read_Code(Network->Info_Code);
    Read_Pop(Network->Info_Pop);Read_Pop(Network->Info_PopIp);
    Read_Hys(Network->Info_Hys);Read_Hys(Network->Info_HysIp);

    Display_Tab(0);

}

void dialog_network::Update(int panel_index)
{
    switch(panel_index)
    {
    case 0:
        Display_Code(Network->Info_Code, m_vtCode);
        Display_Pop(Network->Info_Pop);
        break;
    case 1:
        Display_Code(Network->Info_Code, m_vtCode2);
        Display_Hys(Network->Info_Hys);
        break;
    case 2:
        Display_SrTab(0);
        break;
    case 3:
        Display_TsTab(0);
        break;
    }
}

void dialog_network::onTimer()
{
    switch(Panel->currentIndex())
    {
    case 0:
        Enable(!IsSame_Code(Network->Info_Code, m_vtCode), ui->wigCodeSave);
        Refresh_PopCon();
        Enable(!IsSame_Pop_IpPort(Network->Info_PopIp), ui->wigPopSave);
        break;

    case 1:
        Enable(!IsSame_Code(Network->Info_Code, m_vtCode2), ui->wigCodeSave_2);
        Refresh_HysCon();
        Enable(!IsSame_Hys_IpPort(Network->Info_HysIp), ui->wigHysSave);
        break;

    case 2:
        switch(SrPanel->currentIndex())
        {
        case 0:
            Refresh_SrCon();
            break;
        case 1:
            Enable(!IsSame_SrW(Network->Info_Weight), ui->wigWSave);
            Refresh_SrW();
            break;
        case 2:
            Enable(!IsSame_SrT(Network->Info_Temp), ui->wigTSave);
            Refresh_SrT();
            break;
        case 3:
            Enable(!IsSame_SrE(Network->Info_ESen), ui->wigESave);
            Refresh_SrE();
            break;
        }

        break;

    case 3:
        switch(TsPanel->currentIndex())
        {
        case 0:
            Enable(!IsSame_Ts(Network->Info_Pop_Protocol, m_Info_Pop_Protocol), ui->wigTpSave);
            Refresh_Tp();
            break;
        case 1:
            Enable(!IsSame_Ts(Network->Info_Hys_Protocol, m_Info_Hys_Protocol), ui->wigThSave);
            Refresh_Th();
            break;
        }

        break;
    }
}


void dialog_network::Display_Tab(int tab_index)
{
    Panel->setCurrentIndex(tab_index);
    Update(tab_index);

    for(int i=0;i<m_vtTabLed.count();i++)
        m_vtTabLed[i]->setEnabled(i == tab_index);
}

void dialog_network::onTab()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtTabPic.indexOf(btn);
    if(index < 0) return;

    Display_Tab(index);
}

QString dialog_network::toQString(cn_variant variant, int sosu)
{
    QString result;

    switch(variant.type)
    {
    case CNVAR_FLOAT:
        if(sosu == 2)
            result.sprintf("%.2f", variant.val.f32);
        else if(sosu == 1)
            result.sprintf("%.1f", variant.val.f32);
        else
            result.sprintf("%.0f", variant.val.f32);

        break;

    case CNVAR_STRING:
        result = QString(variant.val.str);
        break;

    default:
        result.clear();
        break;
    }

    return result;
}

void dialog_network::Display_Code(QVector<HyNetwork::ST_NW_VARINFO> &code_info, QVector<QLabel3*>& code_ui)
{
    QString code;
    QString str;
    for(int i=0;i<code_ui.count();i++)
    {
        if(i >= code_info.size())
            code.clear();
        else
        {
            str = toQString(code_info[i].data);
            code.sprintf("%03d", str.toInt());
        }

        code_ui[i]->setText(code);
    }
}

void dialog_network::Display_Code(int index, float data, QVector<QLabel3*>& code_ui)
{
    if(index >= code_ui.size()) return;

    QString code;
    code.sprintf("%03d", (int)data);

    code_ui[index]->setText(code);
}

bool dialog_network::IsSame_Code(QVector<HyNetwork::ST_NW_VARINFO> &code_info, QVector<QLabel3*>& code_ui)
{
    bool bSame = true;
    QString display;
    QString data, str;

    for(int i=0;i<code_ui.count();i++)
    {
        if(i >= code_info.size())
            continue;

        display = code_ui[i]->text();
        str = toQString(code_info[i].data, 0);
        data.sprintf("%03d", str.toInt());

        if(display != data)
            bSame = false;
    }

    return bSame;
}

void dialog_network::onCode()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtCodePic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle(m_vtCodeName[index]->text());
    np->m_numpad->SetNum(m_vtCode[index]->text());

    if(np->exec() == QDialog::Accepted)
    {
        Display_Code(index, (float)np->m_numpad->GetNumDouble(), m_vtCode);
    }
}

bool dialog_network::Save_Code(QVector<HyNetwork::ST_NW_VARINFO> &code_info, QVector<QLabel3*>& code_ui)
{
    for(int i=0;i<code_ui.count();i++)
    {
        if(i >= code_info.size())
            continue;

        sprintf(code_info[i].data.val.str,"%s", code_ui[i]->text().toAscii().data());
    }

    if(!Network->Write(code_info))
    {
        qDebug() << "dialog_network::Save_Code() Network.Write fail!";
        return false;
    }

    return true;
}

bool dialog_network::Read_Code(QVector<HyNetwork::ST_NW_VARINFO> &code_info)
{
    if(!Network->Read(code_info))
    {
        qDebug() << "dialog_network::Read_Code() Network.Read fail!";
        return false;
    }
    return true;
}

void dialog_network::onSave_Code()
{
    if(IsSame_Code(Network->Info_Code, m_vtCode))
        return;

    Save_Code(Network->Info_Code, m_vtCode);
    Read_Code(Network->Info_Code);
    Display_Code(Network->Info_Code, m_vtCode);
}

void dialog_network::Enable(bool on, QWidget* wig)
{
    wig->setEnabled(on);
}

void dialog_network::onCode2()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtCode2Pic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle(m_vtCode2Name[index]->text());
    np->m_numpad->SetNum(m_vtCode2[index]->text());

    if(np->exec() == QDialog::Accepted)
    {
        Display_Code(index, (float)np->m_numpad->GetNumDouble(), m_vtCode2);
    }
}

void dialog_network::onSave_Code2()
{
    if(IsSame_Code(Network->Info_Code, m_vtCode2))
        return;

    Save_Code(Network->Info_Code, m_vtCode2);
    Read_Code(Network->Info_Code);
    Display_Code(Network->Info_Code, m_vtCode2);
}


bool dialog_network::Read_Pop(QVector<HyNetwork::ST_NW_VARINFO> &pop_info)
{
    if(!Network->Read(pop_info))
    {
        qDebug() << "dialog_network::Read_Pop() Network.Read fail!";
        return false;
    }
    return true;
}
bool dialog_network::Save_PopIp(QVector<HyNetwork::ST_NW_VARINFO> &popip_info)
{
    // ip [0]
    QStringList ip4;
    for(int i=0;i<m_vtPopIp.count();i++)
        ip4.append(m_vtPopIp[i]->text());

    QString ip = toIP(ip4);
    sprintf(popip_info[0].data.val.str,"%s",ip.toAscii().data());
    // port [1]
    popip_info[1].data.val.f32 = ui->btnPopPort->text().toFloat();

    if(!Network->Write(popip_info))
    {
        qDebug() << "dialog_network::Save_PopIp() Network.Write fail!";
        return false;
    }

    return true;
}

void dialog_network::Display_Pop(QVector<HyNetwork::ST_NW_VARINFO> &pop_info)
{
    QString data,data2;

    data = toQString(pop_info[0].data,0);
    Display_Pop_Use((bool)data.toInt());

    data = toQString(pop_info[4].data,0);
    Display_Pop_Connect(data.toInt() == NETWORK_STATE_CONNECT);

    data = toQString(pop_info[2].data);
    data2 = toQString(pop_info[3].data,0);

    Display_Pop_IpPort(data, data2.toInt());
}

void dialog_network::Refresh_PopCon()
{
    float data = 0;
    if(Network->Get_PopState(data))
    {
        Display_Pop_Connect((int)data == NETWORK_STATE_CONNECT);
    }
}

void dialog_network::Display_Pop_Use(bool use)
{
    ui->lbPopTitle->setEnabled(use);
    if(use)
        ui->btnPopUse->setText(tr("NOT USE"));
    else
        ui->btnPopUse->setText(tr("USE"));

    Enable(use, ui->wigPop);
}
void dialog_network::Display_Pop_Connect(bool connect)
{
    ui->ledPopCon->setEnabled(connect);
}
void dialog_network::Display_Pop_IpPort(QString ip, int port)
{
    QStringList ip4 = toIP(ip);
    for(int i=0;i<m_vtPopIp.count();i++)
        m_vtPopIp[i]->setText(ip4[i]);

    QString str;
    str.sprintf("%d", port);
    ui->btnPopPort->setText(str);
}

QString dialog_network::toIP(QStringList ip4)
{
    QString str;
    bool error = false;
    int temp;

    if(ip4.size() != 4)
        error = true;
    else
    {
        for(int i=0;i<ip4.count();i++)
        {
            temp = ip4[i].toInt();
            if(temp < 0 || temp > 255)
                error = true;
        }
    }

    if(error)
        str = "192.168.1.1"; // default
    else
        str = ip4.join(".");

    return str;
}
QStringList dialog_network::toIP(QString ip1)
{
    bool error = false;
    QStringList list = ip1.split(".");
    int temp;

    if(list.size() != 4)
        error = true;
    else
    {
        for(int i=0;i<list.count();i++)
        {
            temp = list[i].toInt();
            if(temp < 0 || temp > 255)
                error = true;
        }
    }

    if(error)
    {
        list.clear();
        list.append("192");
        list.append("168");
        list.append("1");
        list.append("1");
    }

    return list;
}

bool dialog_network::IsSame_Pop_IpPort(QVector<HyNetwork::ST_NW_VARINFO> &popip_info)
{
    bool same = true;
    QString ip = toQString(popip_info[0].data);
    QStringList ip4 = toIP(ip);

    for(int i=0;i<m_vtPopIp.count();i++)
    {
        if(m_vtPopIp[i]->text() != ip4[i])
            same = false;
    }

    if(!same)
        return same;

    QString port = toQString(popip_info[1].data, 0);
    if(port != ui->btnPopPort->text())
        same = false;

    return same;
}

void dialog_network::onPopUse()
{
    float data;
    if(ui->lbPopTitle->isEnabled())
        data = 0.0;
    else
        data = 1.0;

    Network->Set_PopUse(data);

    Read_Pop(Network->Info_Pop);
    Display_Pop(Network->Info_Pop);
}
void dialog_network::onPopConnect()
{
    if(ui->ledPopCon->isEnabled())
        return;

    Network->Set_PopFlag(NETWORK_CONNECT); // @@NETWORK_CONNECT
    //Network->Set_PopState(NETWORK_STATE_CONNECT); //@@test
}
void dialog_network::onPopDisconnect()
{
    if(!ui->ledPopCon->isEnabled())
        return;

    Network->Set_PopFlag(NETWORK_DISCONNECT); // @@NETWORK_DISCONNECT
    //Network->Set_PopState(NETWORK_STATE_DISCONNECT); //@@test
}
void dialog_network::onPopIp()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtPopIpPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(255);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle(ui->lbPopIpTitle->text());
    np->m_numpad->SetNum(m_vtPopIp[index]->text());

    if(np->exec() == QDialog::Accepted)
    {
        m_vtPopIp[index]->setText(np->m_numpad->GetNum());
    }
}
void dialog_network::onPopPort()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(9999);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle(ui->lbPopPortTitle->text());
    np->m_numpad->SetNum(ui->btnPopPort->text());

    if(np->exec() == QDialog::Accepted)
    {
        ui->btnPopPort->setText(np->m_numpad->GetNum());
    }
}
void dialog_network::onSave_PopIp()
{
    if(IsSame_Pop_IpPort(Network->Info_PopIp))
        return;

    Save_PopIp(Network->Info_PopIp);

    Read_Pop(Network->Info_Pop);
    Display_Pop(Network->Info_Pop);
}


/** ==================== **/
bool dialog_network::Read_Hys(QVector<HyNetwork::ST_NW_VARINFO> &hys_info)
{
    if(!Network->Read(hys_info))
    {
        qDebug() << "dialog_network::Read_Hys() Network.Read fail!";
        return false;
    }
    return true;
}
bool dialog_network::Save_HysIp(QVector<HyNetwork::ST_NW_VARINFO> &hysip_info)
{
    // ip [0]
    QStringList ip4;
    for(int i=0;i<m_vtHysIp.count();i++)
        ip4.append(m_vtHysIp[i]->text());

    QString ip = toIP(ip4);
    sprintf(hysip_info[0].data.val.str,"%s",ip.toAscii().data());
    // port [1]
    hysip_info[1].data.val.f32 = ui->btnHysPort->text().toFloat();

    if(!Network->Write(hysip_info))
    {
        qDebug() << "dialog_network::Save_HysIp() Network.Write fail!";
        return false;
    }

    return true;
}
void dialog_network::Display_Hys(QVector<HyNetwork::ST_NW_VARINFO> &hys_info)
{
    QString data,data2;

    data = toQString(hys_info[0].data,0);
    Display_Hys_Use((bool)data.toInt());

    data = toQString(hys_info[4].data,0);
    Display_Hys_Connect(data.toInt() == NETWORK_STATE_CONNECT);

    data = toQString(hys_info[2].data);
    data2 = toQString(hys_info[3].data,0);

    Display_Hys_IpPort(data, data2.toInt());
}

void dialog_network::Refresh_HysCon()
{
    float data = 0;
    if(Network->Get_HysState(data))
    {
        Display_Hys_Connect((int)data == NETWORK_STATE_CONNECT);
    }
}

void dialog_network::Display_Hys_Use(bool use)
{
    ui->lbHysTitle->setEnabled(use);
    if(use)
        ui->btnHysUse->setText(tr("NOT USE"));
    else
        ui->btnHysUse->setText(tr("USE"));

    Enable(use, ui->wigHys);
}
void dialog_network::Display_Hys_Connect(bool connect)
{
    ui->ledHysCon->setEnabled(connect);
}
void dialog_network::Display_Hys_IpPort(QString ip, int port)
{
    QStringList ip4 = toIP(ip);
    for(int i=0;i<m_vtHysIp.count();i++)
        m_vtHysIp[i]->setText(ip4[i]);

    QString str;
    str.sprintf("%d", port);
    ui->btnHysPort->setText(str);
}
bool dialog_network::IsSame_Hys_IpPort(QVector<HyNetwork::ST_NW_VARINFO> &hysip_info)
{
    bool same = true;
    QString ip = toQString(hysip_info[0].data);
    QStringList ip4 = toIP(ip);

    for(int i=0;i<m_vtHysIp.count();i++)
    {
        if(m_vtHysIp[i]->text() != ip4[i])
            same = false;
    }

    if(!same)
        return same;

    QString port = toQString(hysip_info[1].data, 0);
    if(port != ui->btnHysPort->text())
        same = false;

    return same;
}

void dialog_network::onHysUse()
{
    float data;
    if(ui->lbHysTitle->isEnabled())
        data = 0.0;
    else
        data = 1.0;

    Network->Set_HysUse(data);

    Read_Hys(Network->Info_Hys);
    Display_Hys(Network->Info_Hys);
}
void dialog_network::onHysConnect()
{
    if(ui->ledHysCon->isEnabled())
        return;

    Network->Set_HysFlag(NETWORK_CONNECT); // @@NETWORK_CONNECT
    //Network->Set_HysState(NETWORK_STATE_CONNECT); //@@test
}
void dialog_network::onHysDisconnect()
{
    if(!ui->ledHysCon->isEnabled())
        return;

    Network->Set_HysFlag(NETWORK_DISCONNECT); // @@NETWORK_DISCONNECT
    //Network->Set_HysState(NETWORK_STATE_DISCONNECT); //@@test
}
void dialog_network::onHysIp()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtHysIpPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(255);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle(ui->lbHysIpTitle->text());
    np->m_numpad->SetNum(m_vtHysIp[index]->text());

    if(np->exec() == QDialog::Accepted)
    {
        m_vtHysIp[index]->setText(np->m_numpad->GetNum());
    }
}
void dialog_network::onHysPort()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(9999);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle(ui->lbHysPortTitle->text());
    np->m_numpad->SetNum(ui->btnHysPort->text());

    if(np->exec() == QDialog::Accepted)
    {
        ui->btnHysPort->setText(np->m_numpad->GetNum());
    }
}
void dialog_network::onSave_HysIp()
{
    if(IsSame_Hys_IpPort(Network->Info_HysIp))
        return;

    Save_HysIp(Network->Info_HysIp);

    Read_Hys(Network->Info_Hys);
    Display_Hys(Network->Info_Hys);
}


void dialog_network::Update_Sr(int srpanel_index)
{
    switch(srpanel_index)
    {
    case 0: // connect
        Display_Sr_Spec();
        Display_Sr_Count();
        break;
    case 1: // weight
        Read_Sr(Network->Info_Weight);
        Display_SrW(Network->Info_Weight);
        break;
    case 2: // temp.
        Read_Sr(Network->Info_Temp);
        Display_SrT(Network->Info_Temp);
        break;
    case 3: // e-sensor
        Read_Sr(Network->Info_ESen);
        Display_SrE(Network->Info_ESen);
        break;
    }
}
void dialog_network::Display_SrTab(int tab_index)
{
    SrPanel->setCurrentIndex(tab_index);
    Update_Sr(tab_index);

    for(int i=0;i<m_vtSrTabLed.count();i++)
        m_vtSrTabLed[i]->setVisible(i == tab_index);
}

void dialog_network::onSrTab()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtSrTabPic.indexOf(btn);
    if(index < 0) return;

    Display_SrTab(index);
}


bool dialog_network::Read_Sr(QVector<HyNetwork::ST_NW_VARINFO> &sr_info)
{
    if(!Network->Read(sr_info))
    {
        qDebug() << "dialog_network::Read_Sr() Network.Read fail!";
        return false;
    }
    return true;
}

bool dialog_network::Save_SrW(QVector<HyNetwork::ST_NW_VARINFO> &sr_info)
{
    QVector<HyNetwork::ST_NW_VARINFO> temp = sr_info;

    // get display data
    for(int i=0;i<m_vtWName.count();i++)
    {
        // use
        temp[(i*4)+0].data.val.f32 = (float)m_vtWName[i]->isEnabled();
        // min
        temp[(i*4)+1].data.val.f32 = m_vtWMin[i]->text().toFloat();
        // max
        temp[(i*4)+2].data.val.f32 = m_vtWMax[i]->text().toFloat();
        // val
        temp[(i*4)+3].data.val.f32 = m_vtWVal[i]->text().toFloat();
    }

    if(!Network->Write(temp))
    {
        qDebug() << "dialog_network::Save_Sr() Network.Write fail!";
        return false;
    }

    sr_info = temp;
    return true;
}

bool dialog_network::Save_SrT(QVector<HyNetwork::ST_NW_VARINFO> &sr_info)
{
    QVector<HyNetwork::ST_NW_VARINFO> temp = sr_info;

    // get display data
    for(int i=0;i<m_vtTName.count();i++)
    {
        // use
        temp[(i*4)+0].data.val.f32 = (float)m_vtTName[i]->isEnabled();
        // min
        temp[(i*4)+1].data.val.f32 = m_vtTMin[i]->text().toFloat();
        // max
        temp[(i*4)+2].data.val.f32 = m_vtTMax[i]->text().toFloat();
        // val
        temp[(i*4)+3].data.val.f32 = m_vtTVal[i]->text().toFloat();
    }

    if(!Network->Write(temp))
    {
        qDebug() << "dialog_network::Save_Sr() Network.Write fail!";
        return false;
    }

    sr_info = temp;
    return true;
}

bool dialog_network::Save_SrE(QVector<HyNetwork::ST_NW_VARINFO> &sr_info)
{
    QVector<HyNetwork::ST_NW_VARINFO> temp = sr_info;

    // get display data
    for(int i=0;i<m_vtEName.count();i++)
    {
        // use
        temp[(i*4)+0].data.val.f32 = (float)m_vtEName[i]->isEnabled();
        // min
        temp[(i*4)+1].data.val.f32 = m_vtEMin[i]->text().toFloat();
        // max
        temp[(i*4)+2].data.val.f32 = m_vtEMax[i]->text().toFloat();
        // val
        temp[(i*4)+3].data.val.f32 = m_vtEVal[i]->text().toFloat();
    }

    if(!Network->Write(temp))
    {
        qDebug() << "dialog_network::Save_Sr() Network.Write fail!";
        return false;
    }

    sr_info = temp;
    return true;
}

void dialog_network::Refresh_SrCon()
{
    float flag=0, call=0, cnterr=0, cntcall=0;
    if(Network->Get_Serial(flag, call, cnterr, cntcall))
    {
        // flag
        Display_Sr_Connect((int)flag == NETWORK_STATE_CONNECT);
        Display_Sr_ConStatus(flag);

        // call
        Display_Sr_CallStatus(call);

        // cntcall
        Display_Sr_CountCall(cntcall);
    }
}

void dialog_network::Display_Sr_Connect(bool connect)
{
    ui->ledSrCon->setEnabled(connect);
}
void dialog_network::Display_Sr_ConStatus(float data)
{
    QString str;
    str.sprintf("%.0f", data);
    ui->lbSrConStatus->setText(str);
}
void dialog_network::Display_Sr_CallStatus(float data)
{
    QString str;
    str.sprintf("%.0f", data);
    ui->lbSrCallStatus->setText(str);
}
void dialog_network::Display_Sr_CountCall(float data)
{
    QString str;
    str.sprintf("%.0f", data);
    ui->lbSrCntCall->setText(str);
}

void dialog_network::onSrConnect()
{
    if(ui->ledSrCon->isEnabled())
        return;

    Network->Set_SerialFlag(NETWORK_CONNECT); // @@NETWORK_CONNECT
}
void dialog_network::onSrDisconnect()
{
    if(!ui->ledSrCon->isEnabled())
        return;

    Network->Set_SerialFlag(NETWORK_DISCONNECT); // @@NETWORK_DISCONNECT
}

void dialog_network::Display_Sr_Spec()
{
    QString spec = "9800 8N1";
    ui->lbSrSpec->setText(spec);
}

void dialog_network::Display_Sr_Count()
{
    float code = 0.0;
    QString str;
    Network->Get_CountError(code);
    str.sprintf("%.0f", code);
    ui->btnErrCode->setText(str);
}

void dialog_network::onCountDisplay()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtSrCountDispPic.indexOf(btn);
    if(index < 0) return;

    Network->Set_CountCall((float)index);
}

void dialog_network::onErrorCode()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle("Code");
    np->m_numpad->SetNum(ui->btnErrCode->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        QString str;
        str.sprintf("%.0f", (float)np->m_numpad->GetNumDouble());
        ui->btnErrCode->setText(str);
    }
}

void dialog_network::onErrorDisplay()
{
    float code = ui->btnErrCode->text().toFloat();
    if(code < 0) code = 0;
    if(code > 999) code = 999;

    Network->Set_CountError(code);

    float code2;
    Network->Get_CountError(code2);
    QString str;
    str.sprintf("%.0f", code2);
    ui->btnErrCode->setText(str);
}

void dialog_network::Display_SrW(QVector<HyNetwork::ST_NW_VARINFO> &srw_info)
{
    QString str;
    bool bUse;
    for(int i=0;i<m_vtWName.count();i++)
    {
        // use
        str = toQString(srw_info[(i*4)+0].data, 0);
        bUse = (str.toInt() >= 1);
        if(bUse)
            m_vtWUse[i]->setText(tr("ON"));
        else
            m_vtWUse[i]->setText(tr("OFF"));

        // enable control (related use)
        Enable(bUse, m_vtWName[i]);
        m_vtWMinPic[i]->setEnabled(bUse);
        m_vtWMin[i]->setEnabled(bUse);
        m_vtWMaxPic[i]->setEnabled(bUse);
        m_vtWMax[i]->setEnabled(bUse);
        m_vtWVal[i]->setEnabled(bUse);

        // min
        str = toQString(srw_info[(i*4)+1].data, 1);
        m_vtWMin[i]->setText(str);

        // max
        str = toQString(srw_info[(i*4)+2].data, 1);
        m_vtWMax[i]->setText(str);

        // val
        str = toQString(srw_info[(i*4)+3].data, 1);
        m_vtWVal[i]->setText(str);
    }
}

bool dialog_network::IsSame_SrW(QVector<HyNetwork::ST_NW_VARINFO> &srw_info)
{
    bool same = true;
    QString str, str2;

    for(int i=0;i<m_vtWName.count();i++)
    {
        //use
        str = QString().setNum((int)m_vtWName[i]->isEnabled());
        str2 = toQString(srw_info[(i*4)+0].data, 0);
        if(str != str2)
        {
            same = false;
            break;
        }

        //min
        str = m_vtWMin[i]->text();
        str2 = toQString(srw_info[(i*4)+1].data, 1);
        if(str != str2)
        {
            same = false;
            break;
        }

        //max
        str = m_vtWMax[i]->text();
        str2 = toQString(srw_info[(i*4)+2].data, 1);
        if(str != str2)
        {
            same = false;
            break;
        }

    }

    return same;
}

void dialog_network::onWUse()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtWUsePic.indexOf(btn);
    if(index < 0) return;

    bool enable = m_vtWName[index]->isEnabled();
    m_vtWName[index]->setEnabled(!enable);

    Save_SrW(Network->Info_Weight);
    Read_Sr(Network->Info_Weight);
    Display_SrW(Network->Info_Weight);

}
void dialog_network::onWMin()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtWMinPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999999.9);
    np->m_numpad->SetSosuNum(1);
    np->m_numpad->SetTitle(ui->lbWMinTitle->text() + " " + m_vtWName[index]->text());
    np->m_numpad->SetNum(m_vtWMin[index]->text());


    if(np->exec() == QDialog::Accepted)
    {
        QString str;
        str.sprintf("%.1f", (float)np->m_numpad->GetNumDouble());
        m_vtWMin[index]->setText(str);
    }

}
void dialog_network::onWMax()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtWMaxPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999999.9);
    np->m_numpad->SetSosuNum(1);
    np->m_numpad->SetTitle(ui->lbWMaxTitle->text() + " " + m_vtWName[index]->text());
    np->m_numpad->SetNum(m_vtWMax[index]->text());

    if(np->exec() == QDialog::Accepted)
    {
        QString str;
        str.sprintf("%.1f", (float)np->m_numpad->GetNumDouble());
        m_vtWMax[index]->setText(str);
    }
}

void dialog_network::onWSave()
{
    Save_SrW(Network->Info_Weight);
    Read_Sr(Network->Info_Weight);
    Display_SrW(Network->Info_Weight);
}

void dialog_network::Refresh_SrW()
{
    QString str;
    float flag=0, call=0, cnterr=0, cntcall=0;
    if(Network->Get_Serial(flag, call, cnterr, cntcall))
    {
        // flag
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigWMeasure);
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigWReset);
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigWStatus);

        // call
        str.sprintf("%.0f", call);
        ui->lbWStatus->setText(str);

        if((int)call == SERIAL_COMP_WEIGHT && m_bWMeasure)
        {
            m_bWMeasure = false;
            Read_Sr(Network->Info_Weight);
            Display_SrW(Network->Info_Weight);
        }
        else if((int)call == SERIAL_COMP_WRESET && m_bWReset)
        {
            m_bWReset = false;
            Read_Sr(Network->Info_Weight);
            Display_SrW(Network->Info_Weight);
        }
    }
}

void dialog_network::onWMeasure()
{
    m_bWMeasure=true;
    Network->Set_SerialCall(SERIAL_CALL_WEIGHT);
}

void dialog_network::onWReset()
{
    // real routine
    m_bWReset = true;
    Network->Set_SerialCall(SERIAL_CALL_WRESET);

    /*// @@for test (val change for random value)
    Network->Info_Weight[3].data.val.f32 = (float)(rand()%100);
    Network->Info_Weight[7].data.val.f32 = (float)(rand()%100);
    Network->Info_Weight[11].data.val.f32 = (float)(rand()%100);
    Network->Info_Weight[15].data.val.f32 = (float)(rand()%100);
    Network->Write(Network->Info_Weight);

    Network->Set_SerialCall(SERIAL_COMP_WEIGHT);*/
}

void dialog_network::onWOffset()
{
    dialog_weight_offset* dig = new dialog_weight_offset();
    dig->exec();
}

// temp.

void dialog_network::Display_SrT(QVector<HyNetwork::ST_NW_VARINFO> &srt_info)
{
    QString str;
    bool bUse;
    for(int i=0;i<m_vtTName.count();i++)
    {
        // use
        str = toQString(srt_info[(i*4)+0].data, 0);
        bUse = (str.toInt() >= 1);
        if(bUse)
            m_vtTUse[i]->setText(tr("ON"));
        else
            m_vtTUse[i]->setText(tr("OFF"));

        // enable control (related use)
        Enable(bUse, m_vtTName[i]);
        m_vtTMinPic[i]->setEnabled(bUse);
        m_vtTMin[i]->setEnabled(bUse);
        m_vtTMaxPic[i]->setEnabled(bUse);
        m_vtTMax[i]->setEnabled(bUse);
        m_vtTVal[i]->setEnabled(bUse);

        // min
        str = toQString(srt_info[(i*4)+1].data, 1);
        m_vtTMin[i]->setText(str);

        // max
        str = toQString(srt_info[(i*4)+2].data, 1);
        m_vtTMax[i]->setText(str);

        // val
        str = toQString(srt_info[(i*4)+3].data, 1);
        m_vtTVal[i]->setText(str);
    }
}

bool dialog_network::IsSame_SrT(QVector<HyNetwork::ST_NW_VARINFO> &srt_info)
{
    bool same = true;
    QString str, str2;

    for(int i=0;i<m_vtTName.count();i++)
    {
        //use
        str = QString().setNum((int)m_vtTName[i]->isEnabled());
        str2 = toQString(srt_info[(i*4)+0].data, 0);
        if(str != str2)
        {
            same = false;
            break;
        }

        //min
        str = m_vtTMin[i]->text();
        str2 = toQString(srt_info[(i*4)+1].data, 1);
        if(str != str2)
        {
            same = false;
            break;
        }

        //max
        str = m_vtTMax[i]->text();
        str2 = toQString(srt_info[(i*4)+2].data, 1);
        if(str != str2)
        {
            same = false;
            break;
        }

    }

    return same;
}

void dialog_network::onTUse()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtTUsePic.indexOf(btn);
    if(index < 0) return;

    bool enable = m_vtTName[index]->isEnabled();
    m_vtTName[index]->setEnabled(!enable);

    Save_SrT(Network->Info_Temp);
    Read_Sr(Network->Info_Temp);
    Display_SrT(Network->Info_Temp);

}
void dialog_network::onTMin()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtTMinPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999999.9);
    np->m_numpad->SetSosuNum(1);
    np->m_numpad->SetTitle(ui->lbTMinTitle->text() + " " + m_vtTName[index]->text());
    np->m_numpad->SetNum(m_vtTMin[index]->text());


    if(np->exec() == QDialog::Accepted)
    {
        QString str;
        str.sprintf("%.1f", (float)np->m_numpad->GetNumDouble());
        m_vtTMin[index]->setText(str);
    }

}
void dialog_network::onTMax()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtTMaxPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999999.9);
    np->m_numpad->SetSosuNum(1);
    np->m_numpad->SetTitle(ui->lbTMaxTitle->text() + " " + m_vtTName[index]->text());
    np->m_numpad->SetNum(m_vtTMax[index]->text());

    if(np->exec() == QDialog::Accepted)
    {
        QString str;
        str.sprintf("%.1f", (float)np->m_numpad->GetNumDouble());
        m_vtTMax[index]->setText(str);
    }
}

void dialog_network::onTSave()
{
    Save_SrT(Network->Info_Temp);
    Read_Sr(Network->Info_Temp);
    Display_SrT(Network->Info_Temp);
}

void dialog_network::Refresh_SrT()
{
    QString str;
    float flag=0, call=0, cnterr=0, cntcall=0;
    if(Network->Get_Serial(flag, call, cnterr, cntcall))
    {
        // flag
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigTMeasure);
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigTReset);
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigTStatus);

        // call
        str.sprintf("%.0f", call);
        ui->lbTStatus->setText(str);

        if((int)call == SERIAL_COMP_TEMP && m_bTMeasure)
        {
            m_bTMeasure = false;
            Read_Sr(Network->Info_Temp);
            Display_SrT(Network->Info_Temp);
        }
    }
}

void dialog_network::onTMeasure()
{
    m_bTMeasure=true;
    Network->Set_SerialCall(SERIAL_CALL_TEMP);
}

void dialog_network::onTReset()
{
    /*// @@for test (val change for random value)
    Network->Info_Temp[3].data.val.f32 = (float)(rand()%100);
    Network->Info_Temp[7].data.val.f32 = (float)(rand()%100);
    Network->Info_Temp[11].data.val.f32 = (float)(rand()%100);
    Network->Info_Temp[15].data.val.f32 = (float)(rand()%100);
    Network->Write(Network->Info_Temp);

    Network->Set_SerialCall(SERIAL_COMP_TEMP);*/
}


// e-sensor.

void dialog_network::Display_SrE(QVector<HyNetwork::ST_NW_VARINFO> &sre_info)
{
    QString str;
    bool bUse;
    for(int i=0;i<m_vtEName.count();i++)
    {
        // use
        str = toQString(sre_info[(i*4)+0].data, 0);
        bUse = (str.toInt() >= 1);
        if(bUse)
            m_vtEUse[i]->setText(tr("ON"));
        else
            m_vtEUse[i]->setText(tr("OFF"));

        // enable control (related use)
        Enable(bUse, m_vtEName[i]);
        m_vtEMinPic[i]->setEnabled(bUse);
        m_vtEMin[i]->setEnabled(bUse);
        m_vtEMaxPic[i]->setEnabled(bUse);
        m_vtEMax[i]->setEnabled(bUse);
        m_vtEVal[i]->setEnabled(bUse);

        // min
        str = toQString(sre_info[(i*4)+1].data, 1);
        m_vtEMin[i]->setText(str);

        // max
        str = toQString(sre_info[(i*4)+2].data, 1);
        m_vtEMax[i]->setText(str);

        // val
        str = toQString(sre_info[(i*4)+3].data, 1);
        m_vtEVal[i]->setText(str);
    }
}

bool dialog_network::IsSame_SrE(QVector<HyNetwork::ST_NW_VARINFO> &sre_info)
{
    bool same = true;
    QString str, str2;

    for(int i=0;i<m_vtEName.count();i++)
    {
        //use
        str = QString().setNum((int)m_vtEName[i]->isEnabled());
        str2 = toQString(sre_info[(i*4)+0].data, 0);
        if(str != str2)
        {
            same = false;
            break;
        }

        //min
        str = m_vtEMin[i]->text();
        str2 = toQString(sre_info[(i*4)+1].data, 1);
        if(str != str2)
        {
            same = false;
            break;
        }

        //max
        str = m_vtEMax[i]->text();
        str2 = toQString(sre_info[(i*4)+2].data, 1);
        if(str != str2)
        {
            same = false;
            break;
        }

    }

    return same;
}

void dialog_network::onEUse()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtEUsePic.indexOf(btn);
    if(index < 0) return;

    bool enable = m_vtEName[index]->isEnabled();
    m_vtEName[index]->setEnabled(!enable);

    Save_SrE(Network->Info_ESen);
    Read_Sr(Network->Info_ESen);
    Display_SrE(Network->Info_ESen);

}
void dialog_network::onEMin()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtEMinPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999999.9);
    np->m_numpad->SetSosuNum(1);
    np->m_numpad->SetTitle(ui->lbEMinTitle->text() + " " + m_vtEName[index]->text());
    np->m_numpad->SetNum(m_vtEMin[index]->text());


    if(np->exec() == QDialog::Accepted)
    {
        QString str;
        str.sprintf("%.1f", (float)np->m_numpad->GetNumDouble());
        m_vtEMin[index]->setText(str);
    }

}
void dialog_network::onEMax()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtEMaxPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999999.9);
    np->m_numpad->SetSosuNum(1);
    np->m_numpad->SetTitle(ui->lbEMaxTitle->text() + " " + m_vtEName[index]->text());
    np->m_numpad->SetNum(m_vtEMax[index]->text());

    if(np->exec() == QDialog::Accepted)
    {
        QString str;
        str.sprintf("%.1f", (float)np->m_numpad->GetNumDouble());
        m_vtEMax[index]->setText(str);
    }
}

void dialog_network::onESave()
{
    Save_SrE(Network->Info_ESen);
    Read_Sr(Network->Info_ESen);
    Display_SrE(Network->Info_ESen);
}

void dialog_network::Refresh_SrE()
{
    QString str;
    float flag=0, call=0, cnterr=0, cntcall=0;
    if(Network->Get_Serial(flag, call, cnterr, cntcall))
    {
        // flag
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigEMeasure);
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigEReset);
        Enable((int)flag == NETWORK_STATE_CONNECT, ui->wigEStatus);

        // call
        str.sprintf("%.0f", call);
        ui->lbEStatus->setText(str);

        if((int)call == SERIAL_COMP_ESEN && m_bEMeasure)
        {
            m_bEMeasure = false;
            Read_Sr(Network->Info_ESen);
            Display_SrE(Network->Info_ESen);
        }
    }
}

void dialog_network::onEMeasure()
{
    m_bEMeasure=true;
    Network->Set_SerialCall(SERIAL_CALL_ESEN);
}

void dialog_network::onEReset()
{
    /*// @@for test (val change for random value)
    Network->Info_ESen[3].data.val.f32 = (float)(rand()%100);
    Network->Info_ESen[7].data.val.f32 = (float)(rand()%100);
    Network->Info_ESen[11].data.val.f32 = (float)(rand()%100);
    Network->Info_ESen[15].data.val.f32 = (float)(rand()%100);
    Network->Write(Network->Info_ESen);

    Network->Set_SerialCall(SERIAL_COMP_ESEN);*/
}


// Tab4 - Test

void dialog_network::Update_Ts(int tspanel_index)
{
    switch(tspanel_index)
    {
    case 0: // pop test
        Read_Ts(Network->Info_Pop_Protocol);
        m_Info_Pop_Protocol = Network->Info_Pop_Protocol;
        m_nStartIndex_Tp=0;
        Redraw_List_Tp(m_nStartIndex_Tp);
        break;

    case 1: // hyserver test
        Read_Ts(Network->Info_Hys_Protocol);
        m_Info_Hys_Protocol = Network->Info_Hys_Protocol;
        m_nStartIndex_Th=0;
        Redraw_List_Th(m_nStartIndex_Th);
        break;
    }
}
void dialog_network::Display_TsTab(int tstab_index)
{
    TsPanel->setCurrentIndex(tstab_index);
    Update_Ts(tstab_index);

    for(int i=0;i<m_vtTsTabLed.count();i++)
        m_vtTsTabLed[i]->setVisible(i == tstab_index);
}

void dialog_network::onTsTab()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtTsTabPic.indexOf(btn);
    if(index < 0) return;

    Display_TsTab(index);
}

bool dialog_network::Read_Ts(QVector<HyNetwork::ST_NW_VARINFO> &ts_info)
{
    if(!Network->Read(ts_info))
    {
        qDebug() << "dialog_network::Read_Ts() Network.Read fail!";
        return false;
    }
    return true;
}

void dialog_network::Redraw_List_Tp(int start_index)
{
    int cal_size = m_Info_Pop_Protocol.size() - start_index;
    int index;
    QString no, name, data;

    for(int i=0;i<m_vtTpNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;

            no.setNum(index + 1);
            name = m_Info_Pop_Protocol[index].ui_name;
            data = toQString(m_Info_Pop_Protocol[index].data, 1);
        }
        else
        {
            // no data.
            no.clear();
            name.clear();
            data.clear();
        }

        m_vtTpNo[i]->setText(no);
        m_vtTpName[i]->setText(name);
        m_vtTpData[i]->setText(data);
    }

    ui->lbTpListSize->setText(QString().setNum(m_Info_Pop_Protocol.size()));
}

void dialog_network::Refresh_Tp()
{
    QString str;
    float use=0,flag=0,trig=0,state=0;
    if(Network->Get_Pop(use, flag, trig, state))
    {
        // use & flag
        Enable((int)use == 1 && (int)state == NETWORK_STATE_CONNECT, ui->wigTpSend);
        Enable((int)use == 1 && (int)state == NETWORK_STATE_CONNECT, ui->wigTpReset);
        Enable((int)use == 1 && (int)state == NETWORK_STATE_CONNECT, ui->wigTpStatus);

        // trig
        str.sprintf("%.0f", trig);
        ui->lbTpStatus->setText(str);
    }

}

void dialog_network::onTpData()
{
    QLabel4* btn = (QLabel4*)sender();
    int table_index = m_vtTpDataPic.indexOf(btn);
    if(table_index < 0) return;

    int index = m_nStartIndex_Tp + table_index;

    if(m_Info_Pop_Protocol[index].type == CNVAR_FLOAT)
    {
        // numpad
        dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
        np->m_numpad->SetMinValue(0);
        np->m_numpad->SetMaxValue(999999.9);
        np->m_numpad->SetSosuNum(1);
        np->m_numpad->SetTitle(m_Info_Pop_Protocol[index].name);
        np->m_numpad->SetNum(m_vtTpData[table_index]->text());

        if(np->exec() == QDialog::Accepted)
        {
            m_Info_Pop_Protocol[index].data.val.f32 = (float)np->m_numpad->GetNumDouble();
        }

    }
    else // CNVAR_STRING & ANY
    {
        // keyboard
        dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
        kb->m_kb->SetTitle(m_Info_Pop_Protocol[index].name);
        kb->m_kb->SetText(m_vtTpData[table_index]->text());

        if(kb->exec() == QDialog::Accepted)
        {
            sprintf(m_Info_Pop_Protocol[index].data.val.str,"%s", kb->m_kb->GetText().toAscii().data());
        }
    }

    Redraw_List_Tp(m_nStartIndex_Tp);

}
void dialog_network::onTpUp()
{
    if(m_nStartIndex_Tp <= 0)
        return;

    Redraw_List_Tp(--m_nStartIndex_Tp);
}
void dialog_network::onTpDown()
{
    if(m_nStartIndex_Tp >= (m_Info_Pop_Protocol.size()- m_vtTpNo.size()))
        return;

    Redraw_List_Tp(++m_nStartIndex_Tp);
}

bool dialog_network::IsSame_Ts(QVector<HyNetwork::ST_NW_VARINFO> &ts_info, QVector<HyNetwork::ST_NW_VARINFO> &dp_info)
{
    bool same = true;
    QString str,str2;
    for(int i=0;i<ts_info.count();i++)
    {
        str = toQString(ts_info[i].data, 1);
        str2 = toQString(dp_info[i].data, 1);

        if(str != str2)
        {
            same = false;
            break;
        }
    }

    return same;
}

bool dialog_network::Save_Ts(QVector<HyNetwork::ST_NW_VARINFO> &ts_info)
{
    if(!Network->Write(ts_info))
    {
        qDebug() << "dialog_network::Save_Ts() Network.Write fail!";
        return false;
    }

    return true;
}

void dialog_network::onTpSave()
{
    Save_Ts(m_Info_Pop_Protocol);
    Read_Ts(Network->Info_Pop_Protocol);
    m_Info_Pop_Protocol = Network->Info_Pop_Protocol;
    Redraw_List_Tp(m_nStartIndex_Tp);
}
void dialog_network::onTpSend()
{
    Network->Set_PopTrig(TCP_PUSH);
}
void dialog_network::onTpReset()
{
    Network->Set_PopTrig(TCP_READY);
}


// hyserver test

void dialog_network::Redraw_List_Th(int start_index)
{
    int cal_size = m_Info_Hys_Protocol.size() - start_index;
    int index;
    QString no, name, data;

    for(int i=0;i<m_vtThNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;

            no.setNum(index + 1);
            name = m_Info_Hys_Protocol[index].ui_name;
            data = toQString(m_Info_Hys_Protocol[index].data, 1);
        }
        else
        {
            // no data.
            no.clear();
            name.clear();
            data.clear();
        }

        m_vtThNo[i]->setText(no);
        m_vtThName[i]->setText(name);
        m_vtThData[i]->setText(data);
    }

    ui->lbThListSize->setText(QString().setNum(m_Info_Hys_Protocol.size()));
}

void dialog_network::Refresh_Th()
{
    QString str;
    float use=0,flag=0,trig=0,state=0;
    if(Network->Get_Hys(use, flag, trig, state))
    {
        // use & flag
        Enable((int)use == 1 && (int)state == NETWORK_STATE_CONNECT, ui->wigThSend);
        Enable((int)use == 1 && (int)state == NETWORK_STATE_CONNECT, ui->wigThReset);
        Enable((int)use == 1 && (int)state == NETWORK_STATE_CONNECT, ui->wigThStatus);

        // trig
        str.sprintf("%.0f", trig);
        ui->lbThStatus->setText(str);
    }
}

void dialog_network::onThData()
{
    QLabel4* btn = (QLabel4*)sender();
    int table_index = m_vtThDataPic.indexOf(btn);
    if(table_index < 0) return;

    int index = m_nStartIndex_Th + table_index;

    if(m_Info_Hys_Protocol[index].type == CNVAR_FLOAT)
    {
        // numpad
        dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
        np->m_numpad->SetMinValue(0);
        np->m_numpad->SetMaxValue(999999.9);
        np->m_numpad->SetSosuNum(1);
        np->m_numpad->SetTitle(m_Info_Hys_Protocol[index].name);
        np->m_numpad->SetNum(m_vtThData[table_index]->text());

        if(np->exec() == QDialog::Accepted)
        {
            m_Info_Hys_Protocol[index].data.val.f32 = (float)np->m_numpad->GetNumDouble();
        }

    }
    else // CNVAR_STRING & ANY
    {
        // keyboard
        dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
        kb->m_kb->SetTitle(m_Info_Hys_Protocol[index].name);
        kb->m_kb->SetText(m_vtThData[table_index]->text());

        if(kb->exec() == QDialog::Accepted)
        {
            sprintf(m_Info_Hys_Protocol[index].data.val.str,"%s", kb->m_kb->GetText().toAscii().data());
        }
    }

    Redraw_List_Th(m_nStartIndex_Th);

}
void dialog_network::onThUp()
{
    if(m_nStartIndex_Th <= 0)
        return;

    Redraw_List_Th(--m_nStartIndex_Th);
}
void dialog_network::onThDown()
{
    if(m_nStartIndex_Th >= (m_Info_Hys_Protocol.size()- m_vtThNo.size()))
        return;

    Redraw_List_Th(++m_nStartIndex_Th);
}

void dialog_network::onThSave()
{
    Save_Ts(m_Info_Hys_Protocol);
    Read_Ts(Network->Info_Hys_Protocol);
    m_Info_Hys_Protocol = Network->Info_Hys_Protocol;
    Redraw_List_Th(m_nStartIndex_Th);
}
void dialog_network::onThSend()
{
    Network->Set_HysTrig(TCP_PUSH);
}
void dialog_network::onThReset()
{
    Network->Set_HysTrig(TCP_READY);
}




void dialog_network::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
