#ifndef DIALOG_ROBOTDIMENSION_H
#define DIALOG_ROBOTDIMENSION_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_robotdimension;
}

class dialog_robotdimension : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_robotdimension(QWidget *parent = 0);
    ~dialog_robotdimension();

    void Update();

public slots:
    void onOK();
    void onSelect();

private:
    Ui::dialog_robotdimension *ui;

    QVector<QLabel3*> m_vtBtnTxt;
    QVector<QLabel4*> m_vtBtnPic;

    QStringList m_slRobotConfigName;
    QStringList m_slValue[6];
    QStringList m_slRobotName;

    void Save();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ROBOTDIMENSION_H
