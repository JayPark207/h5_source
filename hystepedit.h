#ifndef HYSTEPEDIT_H
#define HYSTEPEDIT_H

#include <QObject>
#include <QDebug>
#include <QString>
#include <QList>
#include <QVector>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hystepdata.h"

class HyStepEdit : public QObject
{
    Q_OBJECT

public:
    HyStepEdit();
    ~HyStepEdit();

    HyStepData m_StepData;
    QList<ST_STEP_DATA> m_Step;

    bool Write_Basic(); // 기본 프로그램을 써준다.

    bool Read_Main();   // 현재프로그램 main 에 내용을 읽어와서 리스트를 유지한다.
    bool Write_Main();  // 수정된 현재 스텝리스트를 main 에 써준다.

    bool IsCan();
    bool IsMode(ST_STEP_DATA StepData);
    bool IsMode(int i);
    bool IsSubMode(ST_STEP_DATA StepData);
    bool IsSubMode(int i);
    bool IsCanDelete(ST_STEP_DATA StepData);
    bool IsCanDelete(int i);
    bool IsCanRename(ST_STEP_DATA StepData); // for userwork (IsCanEdit=true but, IsCanRename=false)
    bool IsCanRename(int i);
    bool IsCanStepMove(ST_STEP_DATA StepData);
    bool IsCanStepMove(int i);

    ST_STEP_DATA View(int i);
    bool HasNickname(int i);

    bool Insert(int i, ST_STEP_DATA StepData);  // insert base function.
    bool Delete(int i);     // removeAt or takeAt

    // related to mode , insert or delete.
    bool Insert_forMode(int step);
    bool Delete_forMode(int step);
    bool HasStep(int step);

    bool Insert_Position(int pos);
    bool Insert_Output(int pos);
    bool Insert_WaitInput(int pos);
    bool Insert_WaitTime(int pos);
    bool Insert_Work(int pos, QStringList work_macro);

    bool Refresh_Work(int pos);

    bool Up(int pos);
    bool Down(int pos);

    QString MakeDispName(int i);

    int FindNext(QList<int> list);
    bool ForceDelete(int step);
    int FindPos(int step);

    // for user step addition.  (base macro => copy&modify => make macroN)
    bool Make_UserPosition(ST_STEP_DATA data); // no = 0 ~ MAX_USER_STEP_POS-1;
    bool Make_UserOutput(ST_STEP_DATA data);
    bool Make_UserWaitInput(ST_STEP_DATA data);
    bool Make_UserWaitTime(ST_STEP_DATA data);
    bool Make_UserWork(ST_STEP_DATA data, QStringList macro);

    // for mode add step (default macro => in tp)
    bool Make_forMode(ST_STEP_DATA data);

};



#endif // HYSTEPEDIT_H
