#ifndef HYKEY_H
#define HYKEY_H

#include <QApplication>
#include <QObject>

#include "global.h"

#include "dialog_delaying.h"


class HyKey : public QApplication
{
public:
    HyKey(int& argc, char** argv) : QApplication(argc, argv) {

        unsigned int val = 0x00000001;
        for(int i=0;i<32;i++)
        {
            MASK[i] = val << i;
        }
    }

    bool notify(QObject* object,QEvent* event);

    unsigned int MASK[32];

    void Proc_Right_Press(char key);
    void Proc_Right_Release(char key);

    void Proc_Left_Press(char key);
    void Proc_Left_Release(char key);

};

#endif // HYKEY_H
