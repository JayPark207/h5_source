#ifndef DIALOG_VARMONITOR_H
#define DIALOG_VARMONITOR_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"
#include "dialog_keyboard.h"

namespace Ui {
class dialog_varmonitor;
}

class dialog_varmonitor : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_varmonitor(QWidget *parent = 0);
    ~dialog_varmonitor();

    void Update_Name();
    void Update_Val();
    void SaveParam(QStringList& names);

public slots:
    void onTimer();

    void onTable();
    void onMode();
    void onEdit();

private:
    Ui::dialog_varmonitor *ui;

    QTimer* timer;
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtVal;

    QStringList m_Name;
    QStringList m_Data;

    bool m_bEditMode;
    void SetEditMode(bool onoff);

    int m_nSelectedIndex;
    void Display_Select(int select_index);

    QString MakeValue(cn_variant variant);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_VARMONITOR_H
