#ifndef FORM_STEPEDIT_BASE_H
#define FORM_STEPEDIT_BASE_H

#include <QWidget>
#include <QGroupBox>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_position_teaching2.h"
#include "dialog_takeout_method.h"
#include "dialog_jmotion.h"
#include "dialog_temperature.h"
#include "dialog_weight.h"
#include "dialog_syncback.h"

#include "dialog_array_set.h"
#include "dialog_insertmain_set.h"
#include "dialog_unloadseq_set.h"
#include "dialog_insertsub_set.h"
#include "dialog_dualunload_mon.h"

namespace Ui {
class form_stepedit_base;
}

class form_stepedit_base : public QWidget
{
    Q_OBJECT

public:
    explicit form_stepedit_base(QWidget *parent = 0);
    ~form_stepedit_base();

    void Init(int index, QString title = "", HyStepData::ENUM_STEP step_type = HyStepData::WAIT);

    void Update(); // first, seperate steps.

    void Update(HyStepData::ENUM_STEP step_type); // each steps.

    void Redraw_List(int start_index);
    void Display_Data(int list_index);


    void EnableControlButton(HyStepData::ENUM_STEP step_type);

signals:
    void sigClose();

public slots:
    void onClose();
    void onList();
    void onListUp();
    void onListDown();
    void onTeach();

    // for takeout
    void onMethod();
    void onJMotion();
    void onTemp();
    void onSyncBack();

    // for unload
    void onArray();
    void onSeqeuntial();
    void onWeight();
    void on2Unload();

    // for insert
    void onMain();
    void onSub();

private:
    Ui::form_stepedit_base *ui;

    int m_nSelectedIndex;
    QString m_strTitle;
    HyStepData::ENUM_STEP m_StepType;
    int mX,mY,mW,mH;
    QVector<QFrame*> m_vtGrp;

    void SetBtnGroup(HyStepData::ENUM_STEP step_type);

    QVector<int> m_UsePosi;

    QVector<QLabel3*> m_vtListItem;
    int m_nSelectedList;
    int m_nStartListIndex;

    void ListOn(int i);
    void ListOff();

    void Display_SetPos(cn_trans t, cn_joint j);
    void Display_Spd(float speed);
    void Display_Delay(float delay);

private:
    dialog_array_set* dig_array_set;
    dialog_insertmain_set* dig_insertmain_set;
    dialog_unloadseq_set* dig_unloadseq_set;
    dialog_insertsub_set* dig_insertsub_set;
    dialog_syncback* dig_syncback;
    dialog_dualunload_mon* dig_dualunload_mon;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // FORM_STEPEDIT_BASE_H
