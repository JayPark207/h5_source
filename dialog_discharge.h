#ifndef DIALOG_DISCHARGE_H
#define DIALOG_DISCHARGE_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_confirm.h"

namespace Ui {
class dialog_discharge;
}

class dialog_discharge : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_discharge(QWidget *parent = 0);
    ~dialog_discharge();

    void Update();
    bool IsUse_Disc();
    bool IsUse_1stDisc();

private:
    Ui::dialog_discharge *ui;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;
    QStringList m_ItemName;

public slots:
    void onClose();

    void onUse_Disc();
    void onUse_1stDisc();
    void onSet_1stDisc();
    void onCnt_1stDisc();
    void onReset_1stDisc();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_DISCHARGE_H
