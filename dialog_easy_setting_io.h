#ifndef DIALOG_EASY_SETTING_IO_H
#define DIALOG_EASY_SETTING_IO_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_io.h"

namespace Ui {
class dialog_easy_setting_io;
}

class dialog_easy_setting_io : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_easy_setting_io(QWidget *parent = 0);
    ~dialog_easy_setting_io();


private:
    Ui::dialog_easy_setting_io *ui;
    QTimer* timer;
    dialog_io* dig_io;

    QVector<QLabel4*> m_vtOutPic;
    QVector<QLabel3*> m_vtOut, m_vtOutIcon;
    QVector<int> m_vtOutNum;

    void MatchingOutput();

    void Display_OutName();
    void Display_OutSignal();


public slots:
    void onClose();
    void onTimer();

    void onOut();

    void onIOView();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_EASY_SETTING_IO_H
