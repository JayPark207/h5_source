#ifndef HYLANGUAGE_H
#define HYLANGUAGE_H

#include <QObject>
#include <QPixmap>
#include <QTranslator>
#include <QApplication>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyparam.h"

class HyLanguage : public QObject
{
    Q_OBJECT

public:
    explicit HyLanguage();

    QVector<QPixmap>    m_Flag;
    QVector<QLocale>    m_Locale;
    QStringList         m_LangName;
    QStringList         m_QmFile;

    void Init(int lang_index, QTranslator& translator, QLocale& locale);

private:

    void Init();

};

#endif // HYLANGUAGE_H
