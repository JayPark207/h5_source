#include "form_autorun.h"
#include "ui_form_autorun.h"

form_autorun::form_autorun(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form_autorun)
{
    ui->setupUi(this);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    timerData = new QTimer(this);
    timerData->setInterval(2000);
    connect(timerData,SIGNAL(timeout()),this,SLOT(onTimer_Data()));

    timerStatus = new QTimer(this);
    timerStatus->setInterval(200);
    connect(timerStatus,SIGNAL(timeout()),this,SLOT(onTimer_Status()));

    // product info.
    m_vtProd.clear();
    m_vtProd.append(ui->tbProdName);
    m_vtProd.append(ui->tbProdName_2);
    m_vtProd.append(ui->tbProdName_3);
    m_vtProd.append(ui->tbProdName_4);
    m_vtProd.append(ui->tbProdName_5);
    m_vtProd.append(ui->tbProdValue);
    m_vtProd.append(ui->tbProdValue_2);
    m_vtProd.append(ui->tbProdValue_3);
    m_vtProd.append(ui->tbProdValue_4);
    m_vtProd.append(ui->tbProdValue_5);

    int i;
    for(i=0;i<m_vtProd.count();i++)
        connect(m_vtProd[i],SIGNAL(mouse_release()),this,SLOT(onProductInfo()));

    // array info.
    m_vtArray.clear();
    m_vtArray.append(ui->tbArrU);
    m_vtArray.append(ui->tbArrUXset);
    m_vtArray.append(ui->tbArrUYset);
    m_vtArray.append(ui->tbArrUZset);
    m_vtArray.append(ui->tbArrUXnow);
    m_vtArray.append(ui->tbArrUYnow);
    m_vtArray.append(ui->tbArrUZnow);
    m_vtArray.append(ui->tbArrI);
    m_vtArray.append(ui->tbArrIXset);
    m_vtArray.append(ui->tbArrIYset);
    m_vtArray.append(ui->tbArrIZset);
    m_vtArray.append(ui->tbArrIXnow);
    m_vtArray.append(ui->tbArrIYnow);
    m_vtArray.append(ui->tbArrIZnow);

    for(i=0;i<m_vtArray.count();i++)
        connect(m_vtArray[i],SIGNAL(mouse_release()),this,SLOT(onArrayInfo()));


    // step info.
    m_vtStepIcon.clear();m_vtStepNo.clear();m_vtStepName.clear();
    m_vtStepIcon.append(ui->lbStepIcon);  m_vtStepNo.append(ui->lbStepNo);  m_vtStepName.append(ui->lbStepName);
    m_vtStepIcon.append(ui->lbStepIcon_2);m_vtStepNo.append(ui->lbStepNo_2);m_vtStepName.append(ui->lbStepName_2);
    m_vtStepIcon.append(ui->lbStepIcon_3);m_vtStepNo.append(ui->lbStepNo_3);m_vtStepName.append(ui->lbStepName_3);
    m_vtStepIcon.append(ui->lbStepIcon_4);m_vtStepNo.append(ui->lbStepNo_4);m_vtStepName.append(ui->lbStepName_4);
    m_vtStepIcon.append(ui->lbStepIcon_5);m_vtStepNo.append(ui->lbStepNo_5);m_vtStepName.append(ui->lbStepName_5);
    m_vtStepIcon.append(ui->lbStepIcon_6);m_vtStepNo.append(ui->lbStepNo_6);m_vtStepName.append(ui->lbStepName_6);
    m_vtStepIcon.append(ui->lbStepIcon_7);m_vtStepNo.append(ui->lbStepNo_7);m_vtStepName.append(ui->lbStepName_7);
    m_vtStepIcon.append(ui->lbStepIcon_8);m_vtStepNo.append(ui->lbStepNo_8);m_vtStepName.append(ui->lbStepName_8);


    // run control

    connect(ui->btnRunPic,SIGNAL(mouse_release()),this,SLOT(onRun()));
    connect(ui->btnRun,SIGNAL(mouse_press()),ui->btnRunPic,SLOT(press()));
    connect(ui->btnRun,SIGNAL(mouse_release()),ui->btnRunPic,SLOT(release()));
    connect(ui->btnRunIcon,SIGNAL(mouse_press()),ui->btnRunPic,SLOT(press()));
    connect(ui->btnRunIcon,SIGNAL(mouse_release()),ui->btnRunPic,SLOT(release()));

    connect(ui->btnStopPic,SIGNAL(mouse_release()),this,SLOT(onStop()));
    connect(ui->btnStop,SIGNAL(mouse_press()),ui->btnStopPic,SLOT(press()));
    connect(ui->btnStop,SIGNAL(mouse_release()),ui->btnStopPic,SLOT(release()));
    connect(ui->btnStopIcon,SIGNAL(mouse_press()),ui->btnStopPic,SLOT(press()));
    connect(ui->btnStopIcon,SIGNAL(mouse_release()),ui->btnStopPic,SLOT(release()));

    connect(ui->btn1CyclePic,SIGNAL(mouse_release()),this,SLOT(on1Cycle()));
    connect(ui->btn1Cycle,SIGNAL(mouse_press()),ui->btn1CyclePic,SLOT(press()));
    connect(ui->btn1Cycle,SIGNAL(mouse_release()),ui->btn1CyclePic,SLOT(release()));
    connect(ui->btn1CycleIcon,SIGNAL(mouse_press()),ui->btn1CyclePic,SLOT(press()));
    connect(ui->btn1CycleIcon,SIGNAL(mouse_release()),ui->btn1CyclePic,SLOT(release()));

    connect(ui->btnDischargePic,SIGNAL(mouse_release()),this,SLOT(onDischarge()));
    connect(ui->btnDischarge,SIGNAL(mouse_press()),ui->btnDischargePic,SLOT(press()));
    connect(ui->btnDischarge,SIGNAL(mouse_release()),ui->btnDischargePic,SLOT(release()));
    connect(ui->btnDischargeIcon,SIGNAL(mouse_press()),ui->btnDischargePic,SLOT(press()));
    connect(ui->btnDischargeIcon,SIGNAL(mouse_release()),ui->btnDischargePic,SLOT(release()));
    connect(ui->btnDischargeIcon_2,SIGNAL(mouse_press()),ui->btnDischargePic,SLOT(press()));
    connect(ui->btnDischargeIcon_2,SIGNAL(mouse_release()),ui->btnDischargePic,SLOT(release()));
    connect(ui->btnDischargeIcon_3,SIGNAL(mouse_press()),ui->btnDischargePic,SLOT(press()));
    connect(ui->btnDischargeIcon_3,SIGNAL(mouse_release()),ui->btnDischargePic,SLOT(release()));

    connect(ui->btnSamplePic,SIGNAL(mouse_release()),this,SLOT(onSample()));
    connect(ui->btnSample,SIGNAL(mouse_press()),ui->btnSamplePic,SLOT(press()));
    connect(ui->btnSample,SIGNAL(mouse_release()),ui->btnSamplePic,SLOT(release()));
    connect(ui->btnSampleIcon,SIGNAL(mouse_press()),ui->btnSamplePic,SLOT(press()));
    connect(ui->btnSampleIcon,SIGNAL(mouse_release()),ui->btnSamplePic,SLOT(release()));

    // short cut
    m_vtSCutPic.clear();m_vtSCut.clear();m_vtSCutIcon.clear();
    m_vtSCutPic.append(ui->btnSCutPic);   m_vtSCut.append(ui->btnSCut);   m_vtSCutIcon.append(ui->btnSCutIcon);
    m_vtSCutPic.append(ui->btnSCutPic_2); m_vtSCut.append(ui->btnSCut_2); m_vtSCutIcon.append(ui->btnSCutIcon_2);
    m_vtSCutPic.append(ui->btnSCutPic_3); m_vtSCut.append(ui->btnSCut_3); m_vtSCutIcon.append(ui->btnSCutIcon_3);
    m_vtSCutPic.append(ui->btnSCutPic_4); m_vtSCut.append(ui->btnSCut_4); m_vtSCutIcon.append(ui->btnSCutIcon_4);
    m_vtSCutPic.append(ui->btnSCutPic_5); m_vtSCut.append(ui->btnSCut_5); m_vtSCutIcon.append(ui->btnSCutIcon_5);
    m_vtSCutPic.append(ui->btnSCutPic_6); m_vtSCut.append(ui->btnSCut_6); m_vtSCutIcon.append(ui->btnSCutIcon_6);
    m_vtSCutPic.append(ui->btnSCutPic_7); m_vtSCut.append(ui->btnSCut_7); m_vtSCutIcon.append(ui->btnSCutIcon_7);
    m_vtSCutPic.append(ui->btnSCutPic_8); m_vtSCut.append(ui->btnSCut_8); m_vtSCutIcon.append(ui->btnSCutIcon_8);
    m_vtSCutPic.append(ui->btnSCutPic_9); m_vtSCut.append(ui->btnSCut_9); m_vtSCutIcon.append(ui->btnSCutIcon_9);
    m_vtSCutPic.append(ui->btnSCutPic_10);m_vtSCut.append(ui->btnSCut_10);m_vtSCutIcon.append(ui->btnSCutIcon_10);
    //m_vtSCutPic.append(ui->btnSCutPic_11);m_vtSCut.append(ui->btnSCut_11);m_vtSCutIcon.append(ui->btnSCutIcon_11);
    //m_vtSCutPic.append(ui->btnSCutPic_12);m_vtSCut.append(ui->btnSCut_12);m_vtSCutIcon.append(ui->btnSCutIcon_12);
    //m_vtSCutPic.append(ui->btnSCutPic_13);m_vtSCut.append(ui->btnSCut_13);m_vtSCutIcon.append(ui->btnSCutIcon_13);
    //m_vtSCutPic.append(ui->btnSCutPic_14);m_vtSCut.append(ui->btnSCut_14);m_vtSCutIcon.append(ui->btnSCutIcon_14);
    //m_vtSCutPic.append(ui->btnSCutPic_15);m_vtSCut.append(ui->btnSCut_15);m_vtSCutIcon.append(ui->btnSCutIcon_15);
    //m_vtSCutPic.append(ui->btnSCutPic_16);m_vtSCut.append(ui->btnSCut_16);m_vtSCutIcon.append(ui->btnSCutIcon_16);

    for(i=0;i<m_vtSCutPic.count();i++)
    {
        connect(m_vtSCutPic[i],SIGNAL(mouse_release()),this,SLOT(onShortCut()));
        connect(m_vtSCut[i],SIGNAL(mouse_press()),m_vtSCutPic[i],SLOT(press()));
        connect(m_vtSCut[i],SIGNAL(mouse_release()),m_vtSCutPic[i],SLOT(release()));
        connect(m_vtSCutIcon[i],SIGNAL(mouse_press()),m_vtSCutPic[i],SLOT(press()));
        connect(m_vtSCutIcon[i],SIGNAL(mouse_release()),m_vtSCutPic[i],SLOT(release()));
    }

    connect(ui->btnSCutFwdPic,SIGNAL(mouse_release()),this,SLOT(onSCutFwd()));
    connect(ui->btnSCutFwdIcon,SIGNAL(mouse_press()),ui->btnSCutFwdPic,SLOT(press()));
    connect(ui->btnSCutFwdIcon,SIGNAL(mouse_release()),ui->btnSCutFwdPic,SLOT(release()));
    connect(ui->btnSCutBwdPic,SIGNAL(mouse_release()),this,SLOT(onSCutBwd()));
    connect(ui->btnSCutBwdIcon,SIGNAL(mouse_press()),ui->btnSCutBwdPic,SLOT(press()));
    connect(ui->btnSCutBwdIcon,SIGNAL(mouse_release()),ui->btnSCutBwdPic,SLOT(release()));

    ui->wigShortcut->setParent(ui->wigShortcutView);
    ui->wigShortcut->setGeometry(0,
                                 0,
                                 ui->wigShortcut->width(),
                                 ui->wigShortcut->height());
    m_nSCutX = ui->wigShortcut->x();


    // var. & pointer clear.
    m_fCycleTime = 0.0;
    m_bRun=false;m_bPause=false;m_bStop=false;
    m_nCount_Refresh_ProductInfo = 0;
    m_nCount_Refresh_ArrayInfo = 0;
    dig_product_info = 0;
    dig_array_info = 0;
    m_bFisrt = false;
    pxmRunIcon.clear();
    pxmRun.clear();

    m_nSCutStartIndex = 0;
    dig_varmonitor = 0;
    dig_position_auto = 0;
    dig_error = 0;
    dig_time = 0;
    dig_io = 0;
    dig_motor = 0;
    dig_home = 0;
    dig_home2 = 0;
    dig_euromap = 0;
    dig_mode_detail = 0;
    dig_dualunload_mon = 0;

    dig_sampling = 0;
    dig_discharge = 0;

}

form_autorun::~form_autorun()
{
    delete ui;
}
void form_autorun::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void form_autorun::showEvent(QShowEvent *)
{
    gReadyMacro(true);

    Update();

    m_bFirstEntry = true;

    timerStatus->start();
    timerData->start();

    if(!m_bFisrt)
    {
        SetBtnFlagAll(false);
        // add here.

        m_bFisrt = true;
    }

    Redraw_SCut(m_nSCutStartIndex);

    Recipe->Set(HyRecipe::sAutoMode, 1, false);
    Log->Set(HY_INFO_LOG, "Enter Auto Page!");

    m_bVarSave = false;

}
void form_autorun::hideEvent(QHideEvent *)
{
    gReadyMacro(false);

    timerData->stop();
    timerStatus->stop();

    Recipe->Set(HyRecipe::sAutoMode, 0, false);
    Log->Set(HY_INFO_LOG, "Exit Auto Page!");

    if(m_bVarSave)
    {
        CNRobo* pCon = CNRobo::getInstance();
        pCon->saveVariable();
    }
}
void form_autorun::onClose()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title(tr("Close"));
    conf->Message(tr("Do you want to close?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    gSetMainPage(MAIN2_PAGE);
}

void form_autorun::Update()
{
    Update_ProductInfo();
    Update_ArrayInfo();
    Update_StepInfo();
    Update_RunCtrl();

    m_bKeyHome = false;
}


void form_autorun::onTimer_Data()
{
    // for running, don't close this page.
    ui->grpEnd->setEnabled(!m_bRun);
    // only running update.
    //if(!m_bRun)
    //    return;

    Refresh_ProductInfo();
    Refresh_ArrayInfo();
}

void form_autorun::onTimer_Status()
{
    Refresh_RunCtrl();
    Refresh_StepInfo();

    Display_SCut();

    Process_FirstEntry();
    Process_KeyHome();
}


/** Product info **/
void form_autorun::Update_ProductInfo()
{
    // read var. => display.
    float data;
    QString str;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    vars.append(HyRecipe::vTargetProd);
    vars.append(HyRecipe::vCurrProd);
    vars.append(HyRecipe::tCycleTime);
    vars.append(HyRecipe::tTOutTime);
    vars.append(HyRecipe::v1stBadCnt);

    QVector<float> datas;

    if(Recipe->Gets(vars, datas))
    {
        // Target Product Qty.
        str.sprintf("%d", (int)datas[0]);
        ui->tbProdValue->setText(str);

        // Current Product Qty.
        str.sprintf("%d", (int)datas[1]);
        ui->tbProdValue_2->setText(str);

        // 1 Cycle Time [sec]
        m_fCycleTime = datas[2];
        str.sprintf("%.2f", datas[2]);
        ui->tbProdValue_3->setText(str);

        // Takeout Time [sec]
        str.sprintf("%.2f", datas[3]);
        ui->tbProdValue_4->setText(str);

        // 1 Day Product Qty. (calculated value)
        if(m_fCycleTime >= 1.0)
        {
            data = 86400.0 / m_fCycleTime;
            str.sprintf("%d", (int)data);
        }
        else
            str = QString(tr("N/A"));

        ui->tbProdValue_5->setText(str);

        // 1st Discharge count [ea]
        if(ui->btnDischargeIcon_2->autoFillBackground())
        {
            str.sprintf("%.0f", datas[4]);
            ui->btnDischargeIcon_3->setText(str);
        }
        else
        {
            ui->btnDischargeIcon_3->setText("");
        }

    }

}



void form_autorun::Refresh_ProductInfo()
{
    // read var. => display.
    float data;
    QString str;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    vars.append(HyRecipe::vTargetProd);
    vars.append(HyRecipe::vCurrProd);
    vars.append(HyRecipe::tCycleTime);
    vars.append(HyRecipe::tTOutTime);
    vars.append(HyRecipe::v1stBadCnt);

    QVector<float> datas;

    if(Recipe->Gets(vars, datas))
    {
        // Target Product Qty.
        str.sprintf("%d", (int)datas[0]);
        ui->tbProdValue->setText(str);

        // Current Product Qty.
        str.sprintf("%d", (int)datas[1]);
        ui->tbProdValue_2->setText(str);

        // 1 Cycle Time [sec]
        m_fCycleTime = datas[2];
        str.sprintf("%.2f", datas[2]);
        ui->tbProdValue_3->setText(str);

        // Takeout Time [sec]
        str.sprintf("%.2f", datas[3]);
        ui->tbProdValue_4->setText(str);

        // 1 Day Product Qty. (calculated value)
        if(m_fCycleTime >= 1.0)
        {
            data = 86400.0 / m_fCycleTime;
            str.sprintf("%d", (int)data);
        }
        else
            str = QString(tr("N/A"));

        ui->tbProdValue_5->setText(str);

        // 1st Discharge count [ea]
        if(ui->btnDischargeIcon_2->autoFillBackground())
        {
            str.sprintf("%.0f", datas[4]);
            ui->btnDischargeIcon_3->setText(str);
        }
        else
        {
            ui->btnDischargeIcon_3->setText("");
        }
    }

}

void form_autorun::onProductInfo()
{
    if(dig_product_info == 0)
        dig_product_info = new dialog_product_info();

    dig_product_info->exec();

    Update_ProductInfo();
}

/** ------------------ **/


/** Array Info **/

void form_autorun::Update_ArrayInfo()
{
    // check unload arrry
    float data;
    bool bOn;
    QString str;
    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;

    if(Recipe->Get(HyRecipe::mdProdUnload, &data))
    {
        bOn = ((int)data == 2 || (int)data == 4);
        ui->tbArrU->setEnabled(bOn);
        ui->tbArrUXset->setEnabled(bOn);
        ui->tbArrUYset->setEnabled(bOn);
        ui->tbArrUZset->setEnabled(bOn);
        ui->tbArrUXnow->setEnabled(bOn);
        ui->tbArrUYnow->setEnabled(bOn);
        ui->tbArrUZnow->setEnabled(bOn);

        if(bOn) // unload array use
        {
            vars.clear();
            vars.append(HyRecipe::vArrXNum);
            vars.append(HyRecipe::vArrYNum);
            vars.append(HyRecipe::vArrZNum);
            vars.append(HyRecipe::vArrXCurr);
            vars.append(HyRecipe::vArrYCurr);
            vars.append(HyRecipe::vArrZCurr);

            if(Recipe->Gets(vars, datas))
            {
                str.sprintf("%d", (int)datas[0]);
                ui->tbArrUXset->setText(str);

                str.sprintf("%d", (int)datas[1]);
                ui->tbArrUYset->setText(str);

                str.sprintf("%d", (int)datas[2]);
                ui->tbArrUZset->setText(str);

                str.sprintf("%d", (int)datas[3]+1);
                ui->tbArrUXnow->setText(str);

                str.sprintf("%d", (int)datas[4]+1);
                ui->tbArrUYnow->setText(str);

                str.sprintf("%d", (int)datas[5]+1);
                ui->tbArrUZnow->setText(str);
            }

        }
        else // no use
        {
            str.sprintf("%d", 0);
            ui->tbArrUXset->setText(str);
            ui->tbArrUYset->setText(str);
            ui->tbArrUZset->setText(str);
            ui->tbArrUXnow->setText(str);
            ui->tbArrUYnow->setText(str);
            ui->tbArrUZnow->setText(str);
        }
    }

    // check insert use. (after modify~)

    if(Recipe->Get(HyRecipe::mdInsert, &data))
    {
        bOn = ((int)data == 1);

        ui->tbArrI->setEnabled(bOn);
        ui->tbArrIXset->setEnabled(bOn);
        ui->tbArrIYset->setEnabled(bOn);
        ui->tbArrIZset->setEnabled(bOn);
        ui->tbArrIXnow->setEnabled(bOn);
        ui->tbArrIYnow->setEnabled(bOn);
        ui->tbArrIZnow->setEnabled(bOn);

        if(bOn)
        {
            vars.clear();
            vars.append(HyRecipe::vIArrXNum);
            vars.append(HyRecipe::vIArrYNum);
            vars.append(HyRecipe::vIArrZNum);
            vars.append(HyRecipe::vIArrXCurr);
            vars.append(HyRecipe::vIArrYCurr);
            vars.append(HyRecipe::vIArrZCurr);

            if(Recipe->Gets(vars, datas))
            {
                str.sprintf("%d", (int)datas[0]);
                ui->tbArrIXset->setText(str);

                str.sprintf("%d", (int)datas[1]);
                ui->tbArrIYset->setText(str);

                str.sprintf("%d", (int)datas[2]);
                ui->tbArrIZset->setText(str);

                str.sprintf("%d", (int)datas[3]+1);
                ui->tbArrIXnow->setText(str);

                str.sprintf("%d", (int)datas[4]+1);
                ui->tbArrIYnow->setText(str);

                str.sprintf("%d", (int)datas[5]+1);
                ui->tbArrIZnow->setText(str);
            }
        }
        else
        {
            str.sprintf("%d", 0);
            ui->tbArrIXset->setText(str);
            ui->tbArrIYset->setText(str);
            ui->tbArrIZset->setText(str);
            ui->tbArrIXnow->setText(str);
            ui->tbArrIYnow->setText(str);
            ui->tbArrIZnow->setText(str);
        }
    }

}

void form_autorun::Refresh_ArrayInfo()
{
    QString str;
    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;

    vars.clear();
    if(ui->tbArrU->isEnabled())
    {
        vars.append(HyRecipe::vArrXCurr);
        vars.append(HyRecipe::vArrYCurr);
        vars.append(HyRecipe::vArrZCurr);
    }

    if(ui->tbArrI->isEnabled())
    {
        vars.append(HyRecipe::vIArrXCurr);
        vars.append(HyRecipe::vIArrYCurr);
        vars.append(HyRecipe::vIArrZCurr);
    }

    if(Recipe->Gets(vars, datas))
    {
        if(vars.size() <= 3)
        {
            if(vars.indexOf(HyRecipe::vIArrXCurr) < 0)
            {
                str.sprintf("%d", (int)datas[0]+1);
                ui->tbArrUXnow->setText(str);
                str.sprintf("%d", (int)datas[1]+1);
                ui->tbArrUYnow->setText(str);
                str.sprintf("%d", (int)datas[2]+1);
                ui->tbArrUZnow->setText(str);
            }
            else
            {
                str.sprintf("%d", (int)datas[0]+1);
                ui->tbArrIXnow->setText(str);
                str.sprintf("%d", (int)datas[1]+1);
                ui->tbArrIYnow->setText(str);
                str.sprintf("%d", (int)datas[2]+1);
                ui->tbArrIZnow->setText(str);
            }
        }
        else // 6
        {
            str.sprintf("%d", (int)datas[0]+1);
            ui->tbArrUXnow->setText(str);
            str.sprintf("%d", (int)datas[1]+1);
            ui->tbArrUYnow->setText(str);
            str.sprintf("%d", (int)datas[2]+1);
            ui->tbArrUZnow->setText(str);

            str.sprintf("%d", (int)datas[3]+1);
            ui->tbArrIXnow->setText(str);
            str.sprintf("%d", (int)datas[4]+1);
            ui->tbArrIYnow->setText(str);
            str.sprintf("%d", (int)datas[5]+1);
            ui->tbArrIZnow->setText(str);
        }
    }

}

void form_autorun::onArrayInfo()
{
    if(dig_array_info == 0)
        dig_array_info = new dialog_array_info();

    dig_array_info->Init(ui->tbArrU->isEnabled(),ui->tbArrI->isEnabled());
    dig_array_info->exec();
}

/** ------------------ **/

/** Step Info **/

void form_autorun::Update_StepInfo()
{
    StepEdit->Read_Main();
    m_nStartIndex = 0;
    Redraw_List(m_nStartIndex);
    Refresh_StepInfo();
}

void form_autorun::Refresh_StepInfo()
{
    CNRobo* pCon = CNRobo::getInstance();

    int _mainstep=0;
    pCon->getRunningMainStepIndex(&_mainstep);
    m_nTaskStatus =  pCon->getTaskStatus(0);

    m_nMainRunIndex = _mainstep-1;

    Display_RunIcon(m_nMainRunIndex, m_nTaskStatus);
}

void form_autorun::Redraw_List(int start_index)
{
    if(StepEdit->m_Step.size() <= m_vtStepNo.size())
        start_index = 0;

    QString strNo,strName;
    int step_index;
    for(int i=0;i<m_vtStepNo.count();i++)
    {
        step_index = start_index + i;

        if(StepEdit->m_Step.size() > step_index)
        {
            // have data.
            strNo.setNum(step_index + 1);
            strName = StepEdit->MakeDispName(step_index);
        }
        else
        {
            // don't have data.
            strNo.clear();
            strName.clear();
        }

        m_vtStepNo[i]->setText(strNo);
        m_vtStepName[i]->setText(strName);
    }

}


void form_autorun::IconLine(int line, CNR_TASK_STATUS status)
{
    if(pxmRunIcon.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon99-5.png");   // stop
        pxmRunIcon.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon99-3.png");   // run
        pxmRunIcon.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon99-4.png");   // pause
        pxmRunIcon.append(QPixmap::fromImage(img));
    }

    for(int i=0;i<m_vtStepIcon.count();i++)
        m_vtStepIcon[i]->setPixmap(0);

    if((int)status < pxmRunIcon.size())
        m_vtStepIcon[line]->setPixmap(pxmRunIcon[status]);
}

void form_autorun::Display_RunIcon(int run_index, CNR_TASK_STATUS task_status)
{
    if(run_index < 0) run_index = 0;

    // run_index match list view change
    if(run_index >= (m_nStartIndex + m_vtStepNo.size()))
    {
        m_nStartIndex = run_index - (m_vtStepNo.size()-1);
        Redraw_List(m_nStartIndex);
    }
    else if(run_index < m_nStartIndex)
    {
        m_nStartIndex = run_index;
        Redraw_List(m_nStartIndex);
    }

    int line_index = run_index - m_nStartIndex;

    IconLine(line_index, task_status);
}
/** ------------------ **/

/** Run Control **/

void form_autorun::Update_RunCtrl()
{
    Refresh_RunCtrl();
}
void form_autorun::Refresh_RunCtrl()
{
    CNRobo* pCon = CNRobo::getInstance();

    m_bRun = !pCon->getHoldRun();


    Display_BtnRun(m_bRun);
}

void form_autorun::Display_BtnRun(bool bRun)
{
    if(pxmRun.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon99-3.png");   // run (false)
        pxmRun.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon99-4.png");   // pause (true)
        pxmRun.append(QPixmap::fromImage(img));
    }

    if((int)bRun < pxmRun.size())
    {
        ui->btnRunIcon->setPixmap(pxmRun[bRun]);
        if(bRun)
            ui->btnRun->setText(tr("Pause"));
        else
            ui->btnRun->setText(tr("Run"));
    }
}

void form_autorun::onRun()
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = 0;
    QString prg_name = StepEdit->m_StepData.m_strMainProgName;
    int cycle = -1;
    float data;

    if(!m_bRun)
    {
        // 1cycle check ?
        // if ok , cycle = 1
        if(Recipe->Get(HyRecipe::bOneCycle, &data))
        {
            if(data > 0.0)
                cycle = 1;
            else
                cycle = -1;

            ret = pCon->setProgramRunCount(0, cycle);
            if(ret < 0)
                qDebug() << "setProgramRunCount ret=" << ret;
        }

        if(m_bPause)
        {
            m_bPause = false;
            ret = pCon->continueProgram(0);
            if(ret < 0)
                qDebug() << "continueProgram ret =" << ret;
        }
        else
        {
            if(m_nTaskStatus == CNR_TASK_NOTUSED)
            {
                if(Recipe->Set(HyRecipe::s1stAutoRun, 1, false))
                {
                    ret = pCon->executeProgram(0, prg_name, cycle);
                    if(ret < 0)
                        qDebug() << "executeProgram2 ret =" << ret << prg_name << cycle;
                }
            }
            else
            {
                ret = pCon->continueProgram(0);
                if(ret < 0)
                    qDebug() << "continueProgram ret =" << ret;
            }

        }
    }
    else
    {
        ret = pCon->holdProgram(0);
        if(ret < 0)
            qDebug() << "executeProgram2 ret =" << ret;

        m_bPause = true;
    }

    if(!m_bVarSave)
        m_bVarSave = true;

}
void form_autorun::onStop()
{
    m_bPause = false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setProgramRunCount(0, 1);
    if(ret < 0)
        qDebug() << "setProgramRunCount ret=" << ret;

//    // now test version. don't have stop function. stop is 1cycle end & stop.
//    CNRobo* pCon = CNRobo::getInstance();

//    pCon->holdProgram(0);
//    pCon->resetCurProgram(0);

}
void form_autorun::on1Cycle()
{
    int cycle = -1;
    float data;
    if(!Recipe->Get(HyRecipe::bOneCycle, &data))
        return;

    if((int)data > 0)
    {
        if(Recipe->Set(HyRecipe::bOneCycle, 0.0, false))
            Display_1Cycle(false);

        cycle = -1;
    }
    else
    {
        if(Recipe->Set(HyRecipe::bOneCycle, 1.0, false))
            Display_1Cycle(true);

        cycle = 1;
    }

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setProgramRunCount(0, cycle);
    if(ret < 0)
        qDebug() << "setProgramRunCount ret=" << ret;

}
void form_autorun::Display_1Cycle(bool enable)
{
    /*QImage img;
    QPixmap pxm;

    if(enable)
        img.load(":/image/image/button_midrect_2");
    else
        img.load(":/image/image/button_midrect_0");

    pxm = QPixmap::fromImage(img);
    ui->btn1CyclePic->setPixmap(pxm);*/

    ui->btn1CycleIcon->setAutoFillBackground(enable);
}
void form_autorun::onDischarge()
{
    /*float data;
    if(!Recipe->Get(HyRecipe::bDischarge, &data))
        return;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;

    vars.append(HyRecipe::bDischarge);
    vars.append(HyRecipe::bNGProd);

    if((int)data > 0)
    {
        datas.clear();
        datas.append(0.0);
        datas.append(0.0);
        if(Recipe->Sets(vars, datas, false))
            Display_Discharge(false);
    }
    else
    {
        datas.clear();
        datas.append(1.0);
        datas.append(0.0); //@@ for test. origin value is 0.0
        if(Recipe->Sets(vars, datas, false))
            Display_Discharge(true);
    }*/

    if(dig_discharge == 0)
        dig_discharge = new dialog_discharge();

    dig_discharge->exec();

    Display_Discharge(dig_discharge->IsUse_Disc(), dig_discharge->IsUse_1stDisc()); // need modify

}
void form_autorun::Display_Discharge(bool enable, bool enable_1stDisc)
{
    /*QImage img;
    QPixmap pxm;

    if(enable)
        img.load(":/image/image/button_midrect_2");
    else
        img.load(":/image/image/button_midrect_0");

    pxm = QPixmap::fromImage(img);
    ui->btnDischargePic->setPixmap(pxm);*/

    ui->btnDischargeIcon->setAutoFillBackground(enable);
    ui->btnDischargeIcon_2->setAutoFillBackground(enable_1stDisc);
    ui->btnDischargeIcon_3->setAutoFillBackground(enable_1stDisc);
    //ui->btnDischargeIcon_3->setEnabled(enable_1stDisc);
}
void form_autorun::onSample()
{
    /*float data;
    if(!Recipe->Get(HyRecipe::bSample, &data))
        return;

    if((int)data > 0)
    {
        if(Recipe->Set(HyRecipe::bSample, 0.0))
            Display_Sample(false);
    }
    else
    {
        if(Recipe->Set(HyRecipe::bSample, 1.0))
            Display_Sample(true);
    }*/

    if(dig_sampling == 0)
        dig_sampling = new dialog_sampling();

    dig_sampling->exec();
    Display_Sample(dig_sampling->IsUse());
}

void form_autorun::Display_Sample(bool enable)
{
    /*QImage img;
    QPixmap pxm;

    if(enable)
        img.load(":/image/image/button_midrect_2");
    else
        img.load(":/image/image/button_midrect_0");

    pxm = QPixmap::fromImage(img);
    ui->btnSamplePic->setPixmap(pxm);*/

    ui->btnSampleIcon->setAutoFillBackground(enable);
}
void form_autorun::SetBtnFlagAll(bool enable)
{
    QVector<float> datas;
    QVector<HyRecipe::RECIPE_NUMBER> vars;
    vars.append(HyRecipe::bOneCycle);
    datas.append((float)enable);
    vars.append(HyRecipe::bDischarge);
    datas.append((float)enable);
    vars.append(HyRecipe::bSampleUse);
    datas.append((float)enable);
    vars.append(HyRecipe::bOneCycleEnd);
    datas.append(0.0);
    vars.append(HyRecipe::b1stBad);
    datas.append((float)enable);

    if(Recipe->Sets(vars, datas))
    {
        Display_1Cycle(enable);
        Display_Discharge(enable, enable);
        Display_Sample(enable);
    }
}

/** ------------------ **/

/** Shortcut **/
void form_autorun::onShortCut()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtSCutPic.indexOf(btn);
    if(index < 0) return;

    switch(index)
    {
    case 0:
        onHome();
        break;

    case 1:
        if(dig_position_auto == 0)
            dig_position_auto = new dialog_position_auto();
        dig_position_auto->exec();
        break;

    case 2:
        if(dig_time == 0)
            dig_time = new dialog_time();
        dig_time->exec();
        break;

    case 3:
        if(dig_io == 0)
            dig_io = new dialog_io();
        dig_io->ForceOutput(false);
        dig_io->exec();
        break;

    case 4:
        dig_error = (dialog_error*)gGetDialog(DIG_ERR);
        dig_error->exec();
        break;

    case 5:
        if(dig_varmonitor == 0)
            dig_varmonitor = new dialog_varmonitor();
        dig_varmonitor->exec();
        break;

    case 6:
        if(dig_motor == 0)
            dig_motor = new dialog_motor();
        dig_motor->exec();
        break;

    case 7:
        if(dig_euromap == 0)
            dig_euromap = new dialog_euromap();
        dig_euromap->exec();
        break;

    case 8:
        if(dig_mode_detail == 0)
            dig_mode_detail = new dialog_mode_detail();
        dig_mode_detail->Set_Readonly(true);
        dig_mode_detail->exec();
        break;

    case 9:
        if(dig_dualunload_mon == 0)
            dig_dualunload_mon = new dialog_dualunload_mon();
        dig_dualunload_mon->exec();
        break;

    default:
        break;
    }
}

void form_autorun::Redraw_SCut(int start_index)
{
    if(start_index < 0)
        return;
    if(start_index > (m_vtSCutPic.size()-SHORTCUT_VIEW_NUM))
        return;

    int x = m_nSCutX - (start_index * SHORTCUT_MOVE_OFFSET);
    int y = ui->wigShortcut->y();
    int w = ui->wigShortcut->width();
    int h = ui->wigShortcut->height();

    ui->wigShortcut->setGeometry(x,y,w,h);
}

void form_autorun::Display_SCut()
{
    //ui->wigSCutFwd->setEnabled(m_nSCutStartIndex > 0);
    //ui->wigSCutBwd->setEnabled(m_nSCutStartIndex < (m_vtSCutPic.size()-SHORTCUT_VIEW_NUM));

    // if run, disable shortcut button.
    m_vtSCutPic[0]->setEnabled(!m_bRun);
    m_vtSCut[0]->setEnabled(!m_bRun);
    m_vtSCutIcon[0]->setEnabled(!m_bRun);

}

void form_autorun::onSCutFwd()
{
    if(m_nSCutStartIndex <= 0)
        return;

    Redraw_SCut(--m_nSCutStartIndex);
}
void form_autorun::onSCutBwd()
{
    if(m_nSCutStartIndex >= (m_vtSCutPic.size()-SHORTCUT_VIEW_NUM))
        return;

    Redraw_SCut(++m_nSCutStartIndex);
}


bool form_autorun::Home(bool bCheck)
{
    if(dig_home2 == 0)
        dig_home2 = new dialog_home2();

    if(bCheck)
        dig_home2->Enable_Check();

    dig_home2->m_bNoMacroOff = true;
    int res = dig_home2->exec();

    if(res == QDialog::Accepted)
        return true;

    return false;
}

void form_autorun::Process_FirstEntry()
{
    if(!m_bFirstEntry) return;

    if(Status->bError)
    {
        ui->grpControl->setEnabled(false);
        return;
    }

    m_bFirstEntry = false;

    if(!Home(true))
    {
        // no home, All control disable...
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Notice")));
        msg->Message(tr("Home not completed!!"),
                     tr("Please make sure home."));
        msg->exec();
        ui->grpControl->setEnabled(false);
    }
    else
    {
        m_bRun=false;m_bPause=false;m_bStop=false;
        ui->grpControl->setEnabled(true);
    }

}

void form_autorun::onHome()
{
    // confirm..
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(tr("Notice"));
    conf->Message(tr("Do you want to homing?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    if(!Home(true))
    {
        // no home, All control disable...
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Notice")));
        msg->Message(tr("Home not completed!!"),
                     tr("Please make sure home."));
        msg->exec();
        ui->grpControl->setEnabled(false);
    }
    else
    {
        m_bRun=false;m_bPause=false;m_bStop=false;
        ui->grpControl->setEnabled(true);
    }
}

void form_autorun::onHome_forKey()
{
    if(!Home(true))
    {
        // no home, All control disable...
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Notice")));
        msg->Message(tr("Home not completed!!"),
                     tr("Please make sure home."));
        msg->exec();
        ui->grpControl->setEnabled(false);
    }
    else
    {
        m_bRun=false;m_bPause=false;m_bStop=false;
        ui->grpControl->setEnabled(true);
    }
}

void form_autorun::Process_KeyHome()
{
    if(!m_bKeyHome) return;

    onHome_forKey();
    m_bKeyHome = false;
}

void form_autorun::BrokenHome()
{
    m_bFirstEntry = true;
}

/** -------- **/
