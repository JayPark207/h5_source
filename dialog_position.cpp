#include "dialog_position.h"
#include "ui_dialog_position.h"

dialog_position::dialog_position(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_position)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtList.clear();
    m_vtList.append(ui->lbPosList1);
    m_vtList.append(ui->lbPosList2);
    m_vtList.append(ui->lbPosList3);
    m_vtList.append(ui->lbPosList4);
    m_vtList.append(ui->lbPosList5);
    m_vtList.append(ui->lbPosList6);
    m_vtList.append(ui->lbPosList7);

    int i;
    for(i=0;i<m_vtList.count();i++)
    {
        m_vtList[i]->setText("");
        connect(m_vtList[i],SIGNAL(mouse_release()),this,SLOT(onList()));
    }

    connect(ui->btnListUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_press()),ui->btnListUpPic,SLOT(press()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_release()),ui->btnListUpPic,SLOT(release()));

    connect(ui->btnListDownPic,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnListDownIcon,SIGNAL(mouse_press()),ui->btnListDownPic,SLOT(press()));
    connect(ui->btnListDownIcon,SIGNAL(mouse_release()),ui->btnListDownPic,SLOT(release()));

    connect(ui->btnTeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnTeach,SIGNAL(mouse_press()),ui->btnTeachPic,SLOT(press()));
    connect(ui->btnTeach,SIGNAL(mouse_release()),ui->btnTeachPic,SLOT(release()));
    connect(ui->btnTeachIcon,SIGNAL(mouse_press()),ui->btnTeachPic,SLOT(press()));
    connect(ui->btnTeachIcon,SIGNAL(mouse_release()),ui->btnTeachPic,SLOT(release()));

#ifdef _H6_
    ui->lbJ6->setText(tr("J6"));

    ui->lbJ6->setEnabled(true);
    ui->lbJ6val->setEnabled(true);
#else
    ui->lbJ6->setText("");

    ui->lbJ6->setEnabled(false);
    ui->lbJ6val->setEnabled(false);
#endif

    top = 0;
}

dialog_position::~dialog_position()
{
    delete ui;
}

void dialog_position::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_position::showEvent(QShowEvent *)
{
    m_nStartIndex = 0;
    m_nSelectedIndex = 0;
    Update_List();
    Redraw_List(m_nStartIndex);
    Display_Data(m_nSelectedIndex);
    ListOn(0);

    SubTop();
}

void dialog_position::hideEvent(QHideEvent *){}

void dialog_position::onClose()
{
    emit accept();
}

void dialog_position::Update_List()
{
    if(StepEdit->Read_Main())
    {
        gMakeUsePosList(StepEdit->m_Step, m_UsePos);
    }
}

void dialog_position::Redraw_List(int start_index)
{
    if(m_UsePos.size() <= m_vtList.size())
        start_index = 0;

    int cal_size = m_UsePos.size() - start_index;

    int _index;
    int _user_pos_index;
    QString strPosName;

    ListOff();

    for(int i=0;i<m_vtList.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            _index = start_index + i;

            if(m_UsePos[_index] < USER_POS_KNOW_OFFSET)
            {
                // base pos
                strPosName = Posi->GetName(m_UsePos[_index]);
            }
            else
            {
                if(m_UsePos[_index] < (USER_POS_KNOW_OFFSET*2))
                {
                    // user pos
                    // user pos name = stepedit user position name
                    _user_pos_index = HyStepData::ADD_POS0 + (m_UsePos[_index] - USER_POS_KNOW_OFFSET);

                    strPosName = StepEdit->m_StepData.Step[_user_pos_index].DispName;
                    if(!StepEdit->m_StepData.Step[_user_pos_index].NickName.isEmpty())
                        strPosName += " ("+ StepEdit->m_StepData.Step[_user_pos_index].NickName + ")";
                }
                else
                {
                    // user work
                    // user pos name = stepedit user position name
                    _user_pos_index = HyStepData::ADD_WORK0 + (m_UsePos[_index] - (USER_POS_KNOW_OFFSET*2));

                    strPosName = StepEdit->m_StepData.Step[_user_pos_index].DispName;
                    if(!StepEdit->m_StepData.Step[_user_pos_index].NickName.isEmpty())
                        strPosName += " ("+ StepEdit->m_StepData.Step[_user_pos_index].NickName + ")";
                }

            }

        }
        else
        {
            // don't have data.
            strPosName.clear();
        }

        m_vtList[i]->setText(strPosName);


        if((start_index + i) == m_nSelectedIndex)
            ListOn(i);

    }

    ui->lbPosNum->setText(QString().setNum(m_UsePos.size()));

}

void dialog_position::onListUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_position::onListDown()
{
    if(m_nStartIndex  >= (m_UsePos.size() - m_vtList.size()))
        return;

    Redraw_List(++m_nStartIndex);
}

void dialog_position::onList()
{
    QString sel = sender()->objectName();

    for(int i=0;i<m_vtList.count();i++)
    {
        if(sel == m_vtList[i]->objectName())
        {
            int index = m_nStartIndex + i;
            if(index < m_UsePos.size())
            {
                m_nSelectedIndex = m_nStartIndex + i;
                ListOn(i);
                Display_Data(m_nSelectedIndex);
            }
        }
    }
}

void dialog_position::ListOn(int table_index)
{
    ListOff();

    QPalette* pal = new QPalette();
    pal->setBrush(QPalette::Window,QBrush(QColor(255,255,0)));

    m_vtList[table_index]->setPalette(*pal);
    m_vtList[table_index]->setAutoFillBackground(true);
}
void dialog_position::ListOff()
{
    for(int i=0;i<m_vtList.count();i++)
        m_vtList[i]->setAutoFillBackground(false);
}

void dialog_position::Display_Data(int list_index)
{
    int _index;
    cn_trans _pos;
    cn_joint _joint;
    float _speed,_delay;

    if(m_UsePos[list_index] < USER_POS_KNOW_OFFSET)
    {
        // base pos
        _index = m_UsePos[list_index];
        if(!Posi->RD(_index, _pos, _joint, _speed, _delay))
            return;
    }
    else
    {
        if(m_UsePos[list_index] < (USER_POS_KNOW_OFFSET*2))
        {
            // user pos
            ST_USERDATA_POS _userpos;
            _index = m_UsePos[list_index] - USER_POS_KNOW_OFFSET;
            if(!UserData->RD(_index, &_userpos))
                return;
            _pos = _userpos.Trans;
            _joint = _userpos.Joint;
            _speed = _userpos.Speed;
            _delay = _userpos.Delay;
        }
        else
        {
            // user work
            ST_USERDATA_WORK _userwork;
            _index = m_UsePos[list_index] - (USER_POS_KNOW_OFFSET*2);
            if(!UserData->RD(_index, &_userwork))
                return;
            _pos = _userwork.Trans;
            _joint = _userwork.Joint;
            _speed = _userwork.Speed;
            _delay = _userwork.Delay;
        }

    }

    Display_SetPos(_pos, _joint);

    QString str;

    str.sprintf(FORMAT_SPEED, _speed);
    ui->btnSpd->setText(str);
    ui->lbSpdVal->setText(str);

    str.sprintf(FORMAT_TIME, _delay);
    ui->btnDelay->setText(str);
    ui->lbDelayVal->setText(str);

    // check setting position
    qDebug() << "Position Value";
    qDebug() << "trans : "
             << _pos.p[0] << _pos.p[1] << _pos.p[2]
             << _pos.eu[0] << _pos.eu[1] << _pos.eu[2];
    qDebug() << "joint : "
             << _joint.joint[0] << _joint.joint[1] << _joint.joint[2]
             << _joint.joint[3] << _joint.joint[4] << _joint.joint[5];
}

void dialog_position::onTeach()
{
    dialog_position_teaching2* dig = (dialog_position_teaching2*)gGetDialog(DIG_POS_TEACH);

    if(m_UsePos[m_nSelectedIndex] < USER_POS_KNOW_OFFSET)
    {
        // base pos
        dig->Init(m_UsePos[m_nSelectedIndex]);

        if(dig->exec() == QDialog::Accepted) {}
        Display_Data(m_nSelectedIndex);

    }
    else
    {
        if(m_UsePos[m_nSelectedIndex] < (USER_POS_KNOW_OFFSET*2))
        {
            // user pos
            // dialog_position_teaching are need modification.
            dig->Init(dialog_position_teaching2::USER,
                           m_UsePos[m_nSelectedIndex] - USER_POS_KNOW_OFFSET);

            if(dig->exec() == QDialog::Accepted) {}
            Display_Data(m_nSelectedIndex);
        }
        else
        {
            // user work
            // dialog_position_teaching are need modification.
            dig->Init(dialog_position_teaching2::WORK,
                           m_UsePos[m_nSelectedIndex] - (USER_POS_KNOW_OFFSET*2));

            if(dig->exec() == QDialog::Accepted) {}
            Display_Data(m_nSelectedIndex);
        }


    }
}

void dialog_position::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}

void dialog_position::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    ui->lbXval->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    ui->lbYval->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    ui->lbZval->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    ui->lbRval->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, j));
    ui->lbSval->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, j));
    ui->lbJ6val->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    ui->lbXval->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    ui->lbYval->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    ui->lbZval->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    ui->lbRval->setText(str);
    str.sprintf(FORMAT_POS, j.joint[JSWV]);
    ui->lbSval->setText(str);

    ui->lbJ6val->setText("");
#endif

}
