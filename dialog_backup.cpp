#include "dialog_backup.h"
#include "ui_dialog_backup.h"

dialog_backup::dialog_backup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_backup)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    Stacked = new QStackedWidget(this);
    //Stacked->setGeometry(5,65,790,410);
    Stacked->setGeometry(ui->frame->x(),
                         ui->frame->y(),
                         ui->frame->width(),
                         ui->frame->height());

    wigBackup = new widget_backup(this);
    Stacked->addWidget(wigBackup);

    wigRecovery = new widget_backup_recovery(this);
    Stacked->addWidget(wigRecovery);

    wigRecovery2 = new widget_backup_recovery2(this);
    Stacked->addWidget(wigRecovery2);

    Stacked->setCurrentIndex(PAGE_BACKUP);
    Stacked->show();

    // ext. signal
    connect(wigBackup,SIGNAL(sigClose()),this,SLOT(accept()));
    connect(wigRecovery,SIGNAL(sigClose()),this,SLOT(accept()));
    connect(wigRecovery2,SIGNAL(sigBack()),this,SLOT(onRecoveryBack()));
    connect(wigRecovery,SIGNAL(sigSelect()),this,SLOT(onRecoverySelect()));
    connect(wigRecovery2,SIGNAL(sigRecovery()),this,SLOT(accept()));

    // top tab control
    connect(ui->lbBackupPic,SIGNAL(mouse_release()),this,SLOT(onTab()));
    connect(ui->lbBackup,SIGNAL(mouse_release()),ui->lbBackupPic,SIGNAL(mouse_release()));
    connect(ui->lbBackupIcon,SIGNAL(mouse_release()),ui->lbBackupPic,SIGNAL(mouse_release()));

    connect(ui->lbRecoveryPic,SIGNAL(mouse_release()),this,SLOT(onTab()));
    connect(ui->lbRecovery,SIGNAL(mouse_release()),ui->lbRecoveryPic,SIGNAL(mouse_release()));
    connect(ui->lbRecoveryIcon,SIGNAL(mouse_release()),ui->lbRecoveryPic,SIGNAL(mouse_release()));

}

dialog_backup::~dialog_backup()
{
    delete ui;
}
void dialog_backup::showEvent(QShowEvent *)
{
    Init();
    timer->start();
}
void dialog_backup::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_backup::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_backup::Init()
{
    ChangePage(PAGE_BACKUP);
    m_nRefreshCount = 0;
    m_bRefreshCheck = RM->IsExist(USB_EXIST_PATH);
}

void dialog_backup::onTimer()
{

    Display_USB(m_bNowCheck);


    // auto exist USB check & refresh
    if(++m_nRefreshCount > 5)
    {
        m_nRefreshCount = 0;
        m_bNowCheck = RM->IsExist(USB_EXIST_PATH);
        if(m_bNowCheck != m_bRefreshCheck)
        {

            m_bRefreshCheck = m_bNowCheck;

            switch(Stacked->currentIndex())
            {
            case PAGE_BACKUP:
                wigBackup->Update();
                break;
            case PAGE_RECOVERY:
                wigRecovery->Update();
                break;
            case PAGE_RECOVERY2:
                ChangePage(PAGE_RECOVERY);
                break;
            }
        }
    }

}

void dialog_backup::onTab()
{
    QString obj_name = sender()->objectName();

    if(obj_name == ui->lbBackupPic->objectName())
    {
        ChangePage(PAGE_BACKUP);
    }
    else if(obj_name == ui->lbRecoveryPic->objectName())
    {
        ChangePage(PAGE_RECOVERY);
    }
}

void dialog_backup::ChangePage(ENUM_PAGE page)
{
    Stacked->setCurrentIndex(page);

    switch (Stacked->currentIndex()) {
    case PAGE_BACKUP:
        ui->lbBackupPic->show();
        ui->lbRecoveryPic->hide();
        break;

    case PAGE_RECOVERY:
    case PAGE_RECOVERY2:
        ui->lbBackupPic->hide();
        ui->lbRecoveryPic->show();
        break;
    }
}
void dialog_backup::onRecoveryBack()
{
    ChangePage(PAGE_RECOVERY);
}
void dialog_backup::onRecoverySelect()
{
    // init recovery2 page.
    wigRecovery2->Init(wigRecovery->GetSelectedIndex());

    ChangePage(PAGE_RECOVERY2);
}

void dialog_backup::Display_USB(bool bExist)
{
    wigBackup->Display_USB(bExist);
    wigRecovery->Display_USB(bExist);
    wigRecovery2->Display_USB(bExist);
}
