#ifndef TOP_SUB_H
#define TOP_SUB_H

#include <QWidget>
#include <QTimer>
#include "global.h"

namespace Ui {
class top_sub;
}

class top_sub : public QWidget
{
    Q_OBJECT

public:
    explicit top_sub(QWidget *parent = 0);
    ~top_sub();

    QTimer* timer;

public slots:
    void onTimer();

private:
    Ui::top_sub *ui;

    void DispRobotStatus(HyStatus::ENUM_ROBOT_STATUS status);
    void DispServo(bool onoff);
    void DispEmo(bool onoff);
    void DispJog(bool onoff);
    void DispLed1(bool onoff);
    void DispLed2(bool onoff);
    void DispLed3(bool onoff);
    void DispSafetyZone(bool onoff);

    QVector<QPixmap> pxmRobotStatus;
    QVector<QPixmap> pxmServo;
    QVector<QPixmap> pxmEmo;
    QVector<QPixmap> pxmJog;
    QVector<QPixmap> pxmLed1;
    QVector<QPixmap> pxmLed2;
    QVector<QPixmap> pxmLed3;
    QVector<QPixmap> pxmSafetyZone;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // TOP_SUB_H
