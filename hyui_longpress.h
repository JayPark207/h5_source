#ifndef HYUI_LONGPRESS_H
#define HYUI_LONGPRESS_H

#include <QObject>
#include <QTimer>
#include <QElapsedTimer>

class HyUi_LongPress : public QObject
{
    Q_OBJECT

public:
    HyUi_LongPress();

    void Init(int long_press_time_ms = 2000,
              int signal_interval_ms = 100);

public slots:
    void onPressed();
    void onReleased();

signals:
    void sigTrigger();


private slots:
    void onTimer();

private:
    int m_nLongPressTime;
    int m_nSignalIntervalTime;

    QTimer* timer;
    QElapsedTimer watch;
    QElapsedTimer interval;

    bool m_bPressed;

};

#endif // HYUI_LONGPRESS_H
