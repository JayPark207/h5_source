#include "page_main2_mold.h"


page_main2_mold::page_main2_mold(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_main2_mold)
{
    ui->setupUi(this);
}

page_main2_mold::~page_main2_mold()
{
    delete ui;
}

void page_main2_mold::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
