#ifndef DIALOG_SASANG_MOTION_TEST_H
#define DIALOG_SASANG_MOTION_TEST_H

#include <QWidget>
#include <QTimer>

#include "global.h"

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_message.h"
#include "dialog_confirm.h"
#include "dialog_delaying.h"
#include "dialog_error.h"
#include "dialog_jog.h"

#define SSMTEST_SPEED_MAX   100 // [%] max speed.
#define SSMTEST_SPEED_MIN   5
#define SSMTEST_SPEED_GAP   5   // [%] up/down gap
#define SSMTEST_SPEED_INIT  50  // [%] init speed.(set low speed for safety)

namespace Ui {
class dialog_sasang_motion_test;
}

class dialog_sasang_motion_test : public QWidget
{
    Q_OBJECT

public:
    explicit dialog_sasang_motion_test(QWidget *parent = 0);
    ~dialog_sasang_motion_test();

    void Update();

private:
    Ui::dialog_sasang_motion_test *ui;

    QTimer* timer;

    QList<ST_SSMOTION_DATA> m_MotionList_Comp;

    QVector<QLabel3*> m_vtTbIcon;
    QVector<QLabel3*> m_vtTbNo;
    QVector<QLabel3*> m_vtTbMot;

    int m_nStartIndex;
    int m_nMainRunIndex, m_nSubRunIndex, m_nSubPlanIndex;
    CNR_TASK_STATUS m_nTaskStatus;

    void Redraw(int run_index, CNR_TASK_STATUS task_status); // boss
    void Redraw_List(int start_index);

    QVector<QPixmap> pxmIcon;
    void Display_Icon(int table_index, CNR_TASK_STATUS status);

    bool m_bReady;
    bool m_bEdit;
    void Control_Ready(bool bReady);

    bool LoadProgram();
    bool ResetProgram();

    void Control_Run(CNR_TASK_STATUS status);

    dialog_delaying* dig_delaying;
    dialog_error* dig_error;
    dialog_jog* dig_jog;

    bool ServoOnMode(int mode);

    float m_TestSpeed;
    bool SetSpeed(float speed);

    void Delay(int msec);

public slots:
    void onClose();
    void onTimer();

    void onReady();
    void onStepFwd();
    void onRun();
    void onPause();

    void onEdit();

    void onError();
    void onJog();
    void onReset();

    void onSpdUp();
    void onSpdDown();

signals:
    void sigClose();
    void sigEdit();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SASANG_MOTION_TEST_H
