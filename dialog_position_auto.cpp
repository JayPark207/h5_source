#include "dialog_position_auto.h"
#include "ui_dialog_position_auto.h"

dialog_position_auto::dialog_position_auto(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_position_auto)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));
    timer->setInterval(200);

    // position list
    m_vtList.clear();
    m_vtList.append(ui->lbPosList);
    m_vtList.append(ui->lbPosList_2);
    m_vtList.append(ui->lbPosList_3);
    m_vtList.append(ui->lbPosList_4);
    m_vtList.append(ui->lbPosList_5);
    m_vtList.append(ui->lbPosList_6);
    m_vtList.append(ui->lbPosList_7);

    int i;
    for(i=0;i<m_vtList.count();i++)
        connect(m_vtList[i],SIGNAL(mouse_release()),this,SLOT(onList()));

    connect(ui->btnListUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_press()),ui->btnListUpPic,SLOT(press()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_release()),ui->btnListUpPic,SLOT(release()));

    connect(ui->btnListDownPic,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnListDownIcon,SIGNAL(mouse_press()),ui->btnListDownPic,SLOT(press()));
    connect(ui->btnListDownIcon,SIGNAL(mouse_release()),ui->btnListDownPic,SLOT(release()));


    // position table
    m_vtValPic.clear();m_vtVal.clear();
    m_vtValPic.append(ui->tbValPic);  m_vtVal.append(ui->tbVal);
    m_vtValPic.append(ui->tbValPic_2);m_vtVal.append(ui->tbVal_2);
    m_vtValPic.append(ui->tbValPic_3);m_vtVal.append(ui->tbVal_3);
    m_vtValPic.append(ui->tbValPic_4);m_vtVal.append(ui->tbVal_4);
    m_vtValPic.append(ui->tbValPic_5);m_vtVal.append(ui->tbVal_5);
    m_vtValPic.append(ui->tbValPic_6);m_vtVal.append(ui->tbVal_6);

    m_vtAxis.clear();
    m_vtAxis.append(ui->tbAxis);
    m_vtAxis.append(ui->tbAxis_2);
    m_vtAxis.append(ui->tbAxis_3);
    m_vtAxis.append(ui->tbAxis_4);
    m_vtAxis.append(ui->tbAxis_5);
    m_vtAxis.append(ui->tbAxis_6);

    for(i=0;i<m_vtValPic.count();i++)
    {
        connect(m_vtValPic[i],SIGNAL(mouse_release()),this,SLOT(onValue()));
        connect(m_vtVal[i],SIGNAL(mouse_press()),m_vtValPic[i],SLOT(press()));
        connect(m_vtVal[i],SIGNAL(mouse_release()),m_vtValPic[i],SLOT(release()));
    }

    connect(ui->tbSpdPic,SIGNAL(mouse_release()),this,SLOT(onSpeed()));
    connect(ui->tbSpd,SIGNAL(mouse_press()),ui->tbSpdPic,SLOT(press()));
    connect(ui->tbSpd,SIGNAL(mouse_release()),ui->tbSpdPic,SLOT(release()));

    connect(ui->tbDelayPic,SIGNAL(mouse_release()),this,SLOT(onDelay()));
    connect(ui->tbDelay,SIGNAL(mouse_press()),ui->tbDelayPic,SLOT(press()));
    connect(ui->tbDelay,SIGNAL(mouse_release()),ui->tbDelayPic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnSavePic,SIGNAL(mouse_release()),this,SLOT(onSave()));
    connect(ui->btnSaveIcon,SIGNAL(mouse_press()),ui->btnSavePic,SLOT(press()));
    connect(ui->btnSaveIcon,SIGNAL(mouse_release()),ui->btnSavePic,SLOT(release()));

#ifdef _H6_
    ui->tbAxis_6->setText(tr("J6"));
    ui->tbAxis_6->setEnabled(true);
    ui->tbVal_6->setEnabled(false);
    ui->tbValPic_6->setEnabled(false);

    ui->tbVal_4->setEnabled(false);
    ui->tbValPic_4->setEnabled(false);
    ui->tbVal_5->setEnabled(false);
    ui->tbValPic_5->setEnabled(false);
#else
    ui->tbAxis_6->setText("");
    ui->tbAxis_6->setEnabled(false);
    ui->tbVal_6->setEnabled(false);
    ui->tbValPic_6->setEnabled(false);

    ui->tbVal_4->setEnabled(true);
    ui->tbValPic_4->setEnabled(true);
    ui->tbVal_5->setEnabled(true);
    ui->tbValPic_5->setEnabled(true);
#endif

}

dialog_position_auto::~dialog_position_auto()
{
    delete ui;
}
void dialog_position_auto::showEvent(QShowEvent *)
{
    m_nStartIndex = 0;
    m_nSelectedIndex = 0;
    Update_List();
    Redraw_List(m_nStartIndex);
    Display_Data(m_nSelectedIndex);

    timer->start();
}

void dialog_position_auto::hideEvent(QHideEvent *)
{
    timer->stop();
}

void dialog_position_auto::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_position_auto::onClose()
{
    emit accept();
}

void dialog_position_auto::onTimer()
{
    // button control
    bool bChange = false;

    int i;
    for(i=0;i<3;i++)
    {
        if(m_Trans.p[i] != m_TransNew.p[i])
            bChange = true;
        if(m_Trans.eu[i] != m_TransNew.eu[i])
            bChange = true;
    }

    for(i=0;i<USE_AXIS_NUM;i++)
    {
        if(m_Joint.joint[i] != m_JointNew.joint[i])
            bChange = true;
    }

    if(m_Speed != m_SpeedNew)
        bChange = true;

    if(m_Delay != m_DelayNew)
        bChange = true;

    ui->wigBtn->setEnabled(bChange);

}

void dialog_position_auto::Update_List()
{
    if(StepEdit->Read_Main())
    {
        gMakeUsePosList(StepEdit->m_Step, m_UsePos);
    }
}

void dialog_position_auto::Redraw_List(int start_index)
{
    if(m_UsePos.size() <= m_vtList.size())
        start_index = 0;

    int cal_size = m_UsePos.size() - start_index;

    int _index;
    int _user_pos_index;
    QString strPosName;

    Display_ListOff();

    for(int i=0;i<m_vtList.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            _index = start_index + i;

            if(m_UsePos[_index] < USER_POS_KNOW_OFFSET)
            {
                // base pos
                strPosName = Posi->GetName(m_UsePos[_index]);
            }
            else
            {

                if(m_UsePos[_index] < (USER_POS_KNOW_OFFSET * 2))
                {
                    // user pos
                    // user pos name = stepedit user position name
                    _user_pos_index = HyStepData::ADD_POS0 + (m_UsePos[_index] - USER_POS_KNOW_OFFSET);

                    strPosName = StepEdit->m_StepData.Step[_user_pos_index].DispName;
                    if(!StepEdit->m_StepData.Step[_user_pos_index].NickName.isEmpty())
                        strPosName += " ("+ StepEdit->m_StepData.Step[_user_pos_index].NickName + ")";
                }
                else
                {
                    // work pos
                    _user_pos_index = HyStepData::ADD_WORK0 + (m_UsePos[_index] - (USER_POS_KNOW_OFFSET*2));

                    strPosName = StepEdit->m_StepData.Step[_user_pos_index].DispName;
                    if(!StepEdit->m_StepData.Step[_user_pos_index].NickName.isEmpty())
                        strPosName += " ("+ StepEdit->m_StepData.Step[_user_pos_index].NickName + ")";
                }
            }

        }
        else
        {
            // don't have data.
            strPosName.clear();
        }

        m_vtList[i]->setText(strPosName);


        if((start_index + i) == m_nSelectedIndex)
        {
            Display_ListOn(i);
            ui->lbSelectPos->setText(m_vtList[i]->text());
        }

    }

    ui->lbPosNum->setText(QString().setNum(m_UsePos.size()));
}
void dialog_position_auto::Display_ListOn(int ui_list_index)
{
    m_vtList[ui_list_index]->setAutoFillBackground(true);
}
void dialog_position_auto::Display_ListOff()
{
    for(int i=0;i<m_vtList.count();i++)
        m_vtList[i]->setAutoFillBackground(false);
}

void dialog_position_auto::Display_Data(int list_index)
{
    int _index;
    cn_trans _pos;
    cn_joint _joint;

    float _speed; // no use
    float _delay; // no use

    if(m_UsePos[list_index] < USER_POS_KNOW_OFFSET)
    {
        // base pos
        _index = m_UsePos[list_index];
        if(!Posi->RD(_index, _pos, _joint, _speed, _delay))
            return;
    }
    else
    {
        if(m_UsePos[list_index] < (USER_POS_KNOW_OFFSET * 2))
        {
            // user pos
            ST_USERDATA_POS _userpos;
            _index = m_UsePos[list_index] - USER_POS_KNOW_OFFSET;
            if(!UserData->RD(_index, &_userpos))
                return;
            m_UserData = _userpos;
            _pos = _userpos.Trans;
            _joint = _userpos.Joint;
            _speed = _userpos.Speed;
            _delay = _userpos.Delay;
        }
        else
        {
            // work pos
            ST_USERDATA_WORK _userwork;
            _index = m_UsePos[list_index] - (USER_POS_KNOW_OFFSET*2);
            if(!UserData->RD(_index, &_userwork))
                return;
            m_UserWork = _userwork;
            _pos = _userwork.Trans;
            _joint = _userwork.Joint;
            _speed = _userwork.Speed;
            _delay = _userwork.Delay;
        }

    }

    m_Trans = _pos;
    m_Joint = _joint;
    m_TransNew = m_Trans;
    m_JointNew = m_Joint;
    Display_SetPos(m_Trans, m_Joint);

    m_Speed = _speed;
    m_Delay = _delay;
    m_SpeedNew = m_Speed;
    m_DelayNew = m_Delay;

    // speed, delay
    QString str;
    str.sprintf(FORMAT_SPEED, m_Speed);
    ui->tbSpd->setText(str);
    str.sprintf(FORMAT_TIME, m_Delay);
    ui->tbDelay->setText(str);

    // check setting position
    qDebug() << "Position Value";
    qDebug() << "trans : "
             << _pos.p[0] << _pos.p[1] << _pos.p[2]
             << _pos.eu[0] << _pos.eu[1] << _pos.eu[2];
    qDebug() << "joint : "
             << _joint.joint[0] << _joint.joint[1] << _joint.joint[2]
             << _joint.joint[3] << _joint.joint[4] << _joint.joint[5];
}

void dialog_position_auto::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtVal[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtVal[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtVal[2]->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    m_vtVal[3]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, j));
    m_vtVal[4]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, j));
    m_vtVal[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtVal[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtVal[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtVal[2]->setText(str);
    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    m_vtVal[3]->setText(str);
    str.sprintf(FORMAT_POS, j.joint[JSWV]);
    m_vtVal[4]->setText(str);
    m_vtVal[5]->setText("");
#endif

}


void dialog_position_auto::onList()
{
    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtList.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nStartIndex + index;
    if(list_index == m_nSelectedIndex) return;  // Avoid redundancy

    if(list_index < m_UsePos.size())
    {
        m_nSelectedIndex = list_index;
        Redraw_List(m_nStartIndex);
        Display_Data(m_nSelectedIndex);
    }

}
void dialog_position_auto::onListUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_position_auto::onListDown()
{
    if(m_nStartIndex  >= (m_UsePos.size() - m_vtList.size()))
        return;

    Redraw_List(++m_nStartIndex);
}

void dialog_position_auto::onValue()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtValPic.indexOf(sel);
    if(index < 0) return;

    cn_trans _trans;
    cn_joint _joint;
    float value, now_value;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    np->m_numpad->SetTitle(m_vtAxis[index]->text());
    now_value = m_vtVal[index]->text().toFloat();
    np->m_numpad->SetNum((double)now_value);
    value = now_value - POS_VALUE_SET_RANGE;
    np->m_numpad->SetMinValue((double)value);
    value = now_value + POS_VALUE_SET_RANGE;
    np->m_numpad->SetMaxValue((double)value);
    np->m_numpad->SetSosuNum(SOSU_POS);

    if(np->exec() == QDialog::Accepted)
    {
        if(index<JROT)
        {
            _trans = m_TransNew;
            _trans.p[index] = (float)np->m_numpad->GetNumDouble();
            if(Posi->Trans2Joint(_trans, m_Joint, _joint))
            {
                m_TransNew = _trans;
                m_JointNew = _joint;
            }
            else
            {
                // error
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
                return;
            }
        }
        else
        {
            _joint = m_JointNew;

#ifdef _H6_
            //_joint.joint[index] = (float)np->m_numpad->GetNumDouble();
            _joint.joint[index] = gGetDisp2Joint(index, (float)np->m_numpad->GetNumDouble(), _joint);
#else
            if(index == JROT)
            {
                float curr_wrist_angle = gGetWristAngle(_joint);
                float curr_input_angel = (float)np->m_numpad->GetNumDouble();
                float diff_angle = curr_input_angel - curr_wrist_angle;

                _joint.joint[index] += diff_angle;
            }
            else
                _joint.joint[index] = (float)np->m_numpad->GetNumDouble();
#endif

            if(Posi->Joint2Trans(_joint, _trans))
            {
                m_JointNew = _joint;
                m_TransNew = _trans;
            }
            else
            {
                //Error Message
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
                return;
            }
        }

        Display_SetPos(m_TransNew, m_JointNew);
    }

}
void dialog_position_auto::onSpeed()
{
    QString str;
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(tr("Speed(%)"));
    np->m_numpad->SetNum(m_SpeedNew);
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);

    if(np->exec() == QDialog::Accepted)
    {
        m_SpeedNew = (float)np->m_numpad->GetNumDouble();
        str.sprintf(FORMAT_SPEED, m_SpeedNew);
        ui->tbSpd->setText(str);
    }
}
void dialog_position_auto::onDelay()
{
    QString str;
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(tr("Delay(sec)"));
    np->m_numpad->SetNum(m_DelayNew);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(100000.0);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        m_DelayNew = (float)np->m_numpad->GetNumDouble();
        str.sprintf(FORMAT_TIME, m_DelayNew);
        ui->tbDelay->setText(str);
    }
}

void dialog_position_auto::onReset()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title("Reset");
    conf->Message(tr("Would you want to reset?"), ui->lbSelectPos->text());

    if(conf->exec() == QDialog::Accepted)
    {
        Redraw_List(m_nStartIndex);
        Display_Data(m_nSelectedIndex);
    }
}
void dialog_position_auto::onSave()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title("Save");
    conf->Message(tr("Would you want to save?"), ui->lbSelectPos->text());

    if(conf->exec() == QDialog::Accepted)
        Save(m_nSelectedIndex);
}

void dialog_position_auto::Save(int list_index)
{
    float speed;
    float delay;

    int pos_index;

    speed       = m_SpeedNew;
    delay       = m_DelayNew;

    if(m_UsePos[list_index] < USER_POS_KNOW_OFFSET)
    {
        pos_index = m_UsePos[list_index];
        Posi->WR(pos_index, m_TransNew, m_JointNew, speed, delay);
    }
    else
    {
        if(m_UsePos[list_index] < (USER_POS_KNOW_OFFSET * 2))
        {
            ST_USERDATA_POS _userpos;
            pos_index = m_UsePos[list_index] - USER_POS_KNOW_OFFSET;
            //UserData->SetBuf_Default(&_userpos);
            _userpos = m_UserData;
            _userpos.Trans = m_TransNew;
            _userpos.Joint = m_JointNew;
            _userpos.Speed = speed;
            _userpos.Delay = delay;

            UserData->WR(pos_index, _userpos);
        }
        else
        {
            ST_USERDATA_WORK _userwork;
            pos_index = m_UsePos[list_index] - (USER_POS_KNOW_OFFSET*2);
            //UserData->SetBuf_Default(&_userpos);
            _userwork = m_UserWork;
            _userwork.Trans = m_TransNew;
            _userwork.Joint = m_JointNew;
            _userwork.Speed = speed;
            _userwork.Delay = delay;

            UserData->WR(pos_index, _userwork);
        }

    }

    Redraw_List(m_nStartIndex);
    Display_Data(list_index);
}
