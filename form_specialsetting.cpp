#include "form_specialsetting.h"
#include "ui_form_specialsetting.h"

form_specialsetting::form_specialsetting(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form_specialsetting)
{
    ui->setupUi(this);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnParamPic,SIGNAL(mouse_release()),this,SLOT(onParam()));
    connect(ui->btnParam,SIGNAL(mouse_press()),ui->btnParamPic,SLOT(press()));
    connect(ui->btnParam,SIGNAL(mouse_release()),ui->btnParamPic,SLOT(release()));
    connect(ui->btnParamIcon,SIGNAL(mouse_press()),ui->btnParamPic,SLOT(press()));
    connect(ui->btnParamIcon,SIGNAL(mouse_release()),ui->btnParamPic,SLOT(release()));

    connect(ui->btnZeroPic,SIGNAL(mouse_release()),this,SLOT(onZero()));
    connect(ui->btnZero,SIGNAL(mouse_press()),ui->btnZeroPic,SLOT(press()));
    connect(ui->btnZero,SIGNAL(mouse_release()),ui->btnZeroPic,SLOT(release()));
    connect(ui->btnZeroIcon,SIGNAL(mouse_press()),ui->btnZeroPic,SLOT(press()));
    connect(ui->btnZeroIcon,SIGNAL(mouse_release()),ui->btnZeroPic,SLOT(release()));

    connect(ui->btnMotionPic,SIGNAL(mouse_release()),this,SLOT(onMotion()));
    connect(ui->btnMotion,SIGNAL(mouse_press()),ui->btnMotionPic,SLOT(press()));
    connect(ui->btnMotion,SIGNAL(mouse_release()),ui->btnMotionPic,SLOT(release()));
    connect(ui->btnMotionIcon,SIGNAL(mouse_press()),ui->btnMotionPic,SLOT(press()));
    connect(ui->btnMotionIcon,SIGNAL(mouse_release()),ui->btnMotionPic,SLOT(release()));

    connect(ui->btnRobotPic,SIGNAL(mouse_release()),this,SLOT(onRobot()));
    connect(ui->btnRobot,SIGNAL(mouse_press()),ui->btnRobotPic,SLOT(press()));
    connect(ui->btnRobot,SIGNAL(mouse_release()),ui->btnRobotPic,SLOT(release()));
    connect(ui->btnRobotIcon,SIGNAL(mouse_press()),ui->btnRobotPic,SLOT(press()));
    connect(ui->btnRobotIcon,SIGNAL(mouse_release()),ui->btnRobotPic,SLOT(release()));

    connect(ui->btnRebootPic,SIGNAL(mouse_press()),this,SLOT(onReboot_press()));
    connect(ui->btnRebootPic,SIGNAL(mouse_release()),this,SLOT(onReboot()));
    connect(ui->btnRebootIcon,SIGNAL(mouse_press()),ui->btnRebootPic,SIGNAL(mouse_press()));
    connect(ui->btnRebootIcon,SIGNAL(mouse_release()),ui->btnRebootPic,SIGNAL(mouse_release()));

    connect(ui->btnFirstPic,SIGNAL(mouse_release()),this,SLOT(onFirstView()));
    connect(ui->btnFirst,SIGNAL(mouse_press()),ui->btnFirstPic,SLOT(press()));
    connect(ui->btnFirst,SIGNAL(mouse_release()),ui->btnFirstPic,SLOT(release()));

    connect(ui->btnMotorPic,SIGNAL(mouse_release()),this,SLOT(onMotorTest()));
    connect(ui->btnMotor,SIGNAL(mouse_press()),ui->btnMotorPic,SLOT(press()));
    connect(ui->btnMotor,SIGNAL(mouse_release()),ui->btnMotorPic,SLOT(release()));
    connect(ui->btnMotorIcon,SIGNAL(mouse_press()),ui->btnMotorPic,SLOT(press()));
    connect(ui->btnMotorIcon,SIGNAL(mouse_release()),ui->btnMotorPic,SLOT(release()));

    connect(ui->btnSafetyZonePic,SIGNAL(mouse_release()),this,SLOT(onSafetyZone()));
    connect(ui->btnSafetyZone,SIGNAL(mouse_press()),ui->btnSafetyZonePic,SLOT(press()));
    connect(ui->btnSafetyZone,SIGNAL(mouse_release()),ui->btnSafetyZonePic,SLOT(release()));
    connect(ui->btnSafetyZoneIcon,SIGNAL(mouse_press()),ui->btnSafetyZonePic,SLOT(press()));
    connect(ui->btnSafetyZoneIcon,SIGNAL(mouse_release()),ui->btnSafetyZonePic,SLOT(release()));

    connect(ui->btnSoftSenPic,SIGNAL(mouse_release()),this,SLOT(onSoftSen()));
    connect(ui->btnSoftSen,SIGNAL(mouse_press()),ui->btnSoftSenPic,SLOT(press()));
    connect(ui->btnSoftSen,SIGNAL(mouse_release()),ui->btnSoftSenPic,SLOT(release()));
    connect(ui->btnSoftSenIcon,SIGNAL(mouse_press()),ui->btnSoftSenPic,SLOT(press()));
    connect(ui->btnSoftSenIcon,SIGNAL(mouse_release()),ui->btnSoftSenPic,SLOT(release()));

    connect(ui->btnProgManagePic,SIGNAL(mouse_release()),this,SLOT(onProgManage()));
    connect(ui->btnProgManage,SIGNAL(mouse_press()),ui->btnProgManagePic,SLOT(press()));
    connect(ui->btnProgManage,SIGNAL(mouse_release()),ui->btnProgManagePic,SLOT(release()));
    connect(ui->btnProgManageIcon,SIGNAL(mouse_press()),ui->btnProgManagePic,SLOT(press()));
    connect(ui->btnProgManageIcon,SIGNAL(mouse_release()),ui->btnProgManagePic,SLOT(release()));

    connect(ui->btnResvPic,SIGNAL(mouse_release()),this,SLOT(onIpGw()));
    connect(ui->btnResv,SIGNAL(mouse_press()),ui->btnResvPic,SLOT(press()));
    connect(ui->btnResv,SIGNAL(mouse_release()),ui->btnResvPic,SLOT(release()));
    connect(ui->btnResvIcon,SIGNAL(mouse_press()),ui->btnResvPic,SLOT(press()));
    connect(ui->btnResvIcon,SIGNAL(mouse_release()),ui->btnResvPic,SLOT(release()));

    connect(ui->btnResvPic_2,SIGNAL(mouse_release()),this,SLOT(onAntiVib()));
    connect(ui->btnResv_2,SIGNAL(mouse_press()),ui->btnResvPic_2,SLOT(press()));
    connect(ui->btnResv_2,SIGNAL(mouse_release()),ui->btnResvPic_2,SLOT(release()));
    connect(ui->btnResvIcon_2,SIGNAL(mouse_press()),ui->btnResvPic_2,SLOT(press()));
    connect(ui->btnResvIcon_2,SIGNAL(mouse_release()),ui->btnResvPic_2,SLOT(release()));

    connect(ui->btnResvPic_3,SIGNAL(mouse_release()),this,SLOT(onTask()));
    connect(ui->btnResv_3,SIGNAL(mouse_press()),ui->btnResvPic_3,SLOT(press()));
    connect(ui->btnResv_3,SIGNAL(mouse_release()),ui->btnResvPic_3,SLOT(release()));
    connect(ui->btnResvIcon_3,SIGNAL(mouse_press()),ui->btnResvPic_3,SLOT(press()));
    connect(ui->btnResvIcon_3,SIGNAL(mouse_release()),ui->btnResvPic_3,SLOT(release()));

    connect(ui->btnDataUpdatePic,SIGNAL(mouse_release()),this,SLOT(onDataUpdate()));
    connect(ui->btnDataUpdate,SIGNAL(mouse_press()),ui->btnDataUpdatePic,SLOT(press()));
    connect(ui->btnDataUpdate,SIGNAL(mouse_release()),ui->btnDataUpdatePic,SLOT(release()));

}

form_specialsetting::~form_specialsetting()
{
    delete ui;
}
void form_specialsetting::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void form_specialsetting::showEvent(QShowEvent *)
{
    Update();
}

void form_specialsetting::hideEvent(QHideEvent *)
{
}

void form_specialsetting::Update()
{
     SetFirstLed((Param->Get(HyParam::FIRST_SETTING).toInt() == 0));
}


void form_specialsetting::onClose()
{
    gSetMainPage(gGetPreMainPage());
}

void form_specialsetting::onParam()
{
    view_param* dig = new view_param();
    dig->exec();
}
void form_specialsetting::onZero()
{
    dialog_zeroing* dig = new dialog_zeroing();
    dig->exec();
}
void form_specialsetting::onMotion()
{
    dialog_robotconfig* dig = new dialog_robotconfig();
    dig->exec();
}
void form_specialsetting::onRobot()
{
    dialog_robotdimension* dig = new dialog_robotdimension();
    dig->exec();
}
void form_specialsetting::onFirstView()
{
    QString value = Param->Get(HyParam::FIRST_SETTING);
    if(value.toInt() == 0)
    {
        Param->Set(HyParam::FIRST_SETTING, 1);
        SetFirstLed(false);
    }
    else
    {
        Param->Set(HyParam::FIRST_SETTING, 0);
        SetFirstLed(true);
    }
}
void form_specialsetting::SetFirstLed(bool onoff)
{
    QImage img;

    if(onoff)
        img.load(":/icon/icon/icon992-4.png");
    else
        img.load(":/icon/icon/icon992-0.png");

    ui->btnFirstIcon->setPixmap(QPixmap::fromImage(img));
}

void form_specialsetting::onReboot_press()
{
    QImage img;
    img.load(":/icon/icon/icon992-10.png");
    ui->btnRebootPic->setPixmap(QPixmap::fromImage(img));
}

void form_specialsetting::onReboot()
{

    QImage img;
    img.load(":/icon/icon/icon992-1.png");
    ui->btnRebootPic->setPixmap(QPixmap::fromImage(img));

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(tr("Reboot"));
    conf->Message(tr("Do you want to reboot system?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    CNRobo* pCon = CNRobo::getInstance();
    pCon->saveVariable();

    dialog_delaying* dig = new dialog_delaying();
    dig->Init(2000);
    dig->exec();

    int ret = pCon->rebootSystem();
    qDebug() << "executeCommand ret =" << ret;
}

void form_specialsetting::onMotorTest()
{
    dialog_motor_test* dig = new dialog_motor_test();
    dig->exec();
}

void form_specialsetting::onSafetyZone()
{
    dialog_safetyzone* dig = new dialog_safetyzone();
    dig->exec();
}

void form_specialsetting::onSoftSen()
{
    dialog_softsensor* dig = new dialog_softsensor();
    dig->exec();
}

void form_specialsetting::onProgManage()
{
    dialog_program_manage* dig = new dialog_program_manage();
    dig->exec();
}

void form_specialsetting::onIpGw()
{
    dialog_tp_ipgw* dig = new dialog_tp_ipgw();
    dig->exec();
}

void form_specialsetting::onAntiVib()
{
    dialog_anti_vibration* dig = new dialog_anti_vibration();
    dig->exec();
}

void form_specialsetting::onTask()
{
    dialog_task_control* dig = new dialog_task_control();
    dig->exec();
}

void form_specialsetting::onDataUpdate()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(tr("Data Update"));
    conf->Message(tr("Do you want to update data?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    UpdateCount = 0;

    int i,j;
    QString temp;

    Check_update("mdclosewait", 0.0);
    Check_update("mddualunload", 0.0);
    Check_update("mdunloadmidpos", 0.0);
    Check_update("mdunloadback", 0.0);

    for(i=32;i<50;i++)
    {
        for(j=0;j<8;j++)
        {
            temp = "udi_sel";
            temp += QString("%1").arg(j);
            temp += QString("[%1]").arg(i);
            Check_update(temp, 0.0);
        }
    }

    Check_update("mdToutTiming", 0.0);
    Check_update("tiToutVac1", 0.0);
    Check_update("tiToutVac2", 0.0);
    Check_update("tiToutVac3", 0.0);
    Check_update("tiToutVac4", 0.0);
    Check_update("tiToutChuck", 0.0);
    Check_update("tiToutGrip", 0.0);
    Check_update("tiToutUser1", 0.0);
    Check_update("tiToutUser2", 0.0);
    Check_update("tiToutUser3", 0.0);
    Check_update("tiToutUser4", 0.0);

    Check_update("mdIStopTErr", 0.0);

    Check_update("b1stBad", 0.0);
    Check_update("v1stBadSet", 0.0);
    Check_update("v1stBadCnt", 0.0);

    Check_update("emFastEject", 0.0);

    /* add here

    */


    Recipe->SaveVariable();

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::GREEN);
    msg->Title(QString(tr("Data Update")));
    temp = QString("Updated data : %1 ea").arg(UpdateCount);
    msg->Message(tr("Success to update data!!"), temp);
    msg->exec();

}

void form_specialsetting::Check_update(QString var_name, float default_data)
{
    float data = 0.0;
    if(!Recipe->Get(var_name, data))
    {
        Recipe->Set(var_name, default_data, false);
        UpdateCount++;
        qDebug() << "update: "<< var_name << "=" << default_data;
    }
    else
        qDebug() << "no update: " << var_name << "=" << data;
}
