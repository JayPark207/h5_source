#include "dialog_unloadback.h"
#include "ui_dialog_unloadback.h"

dialog_unloadback::dialog_unloadback(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_unloadback)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnUsePic,SIGNAL(mouse_release()),this,SLOT(onUse()));
    connect(ui->btnUse,SIGNAL(mouse_press()),ui->btnUsePic,SLOT(press()));
    connect(ui->btnUse,SIGNAL(mouse_release()),ui->btnUsePic,SLOT(release()));

    connect(ui->tbPitchPic,SIGNAL(mouse_release()),this,SLOT(onX()));
    connect(ui->tbPitch,SIGNAL(mouse_press()),ui->tbPitchPic,SLOT(press()));
    connect(ui->tbPitch,SIGNAL(mouse_release()),ui->tbPitchPic,SLOT(release()));

    connect(ui->tbPitchPic_2,SIGNAL(mouse_release()),this,SLOT(onY()));
    connect(ui->tbPitch_2,SIGNAL(mouse_press()),ui->tbPitchPic_2,SLOT(press()));
    connect(ui->tbPitch_2,SIGNAL(mouse_release()),ui->tbPitchPic_2,SLOT(release()));

    connect(ui->tbPitchPic_3,SIGNAL(mouse_release()),this,SLOT(onZ()));
    connect(ui->tbPitch_3,SIGNAL(mouse_press()),ui->tbPitchPic_3,SLOT(press()));
    connect(ui->tbPitch_3,SIGNAL(mouse_release()),ui->tbPitchPic_3,SLOT(release()));

    connect(ui->tbPitchPic_4,SIGNAL(mouse_release()),this,SLOT(onSpeed()));
    connect(ui->tbPitch_4,SIGNAL(mouse_press()),ui->tbPitchPic_4,SLOT(press()));
    connect(ui->tbPitch_4,SIGNAL(mouse_release()),ui->tbPitchPic_4,SLOT(release()));

    connect(ui->tbPitchPic_5,SIGNAL(mouse_release()),this,SLOT(onDelay()));
    connect(ui->tbPitch_5,SIGNAL(mouse_press()),ui->tbPitchPic_5,SLOT(press()));
    connect(ui->tbPitch_5,SIGNAL(mouse_release()),ui->tbPitchPic_5,SLOT(release()));

    bSave = false;
}

dialog_unloadback::~dialog_unloadback()
{
    delete ui;
}
void dialog_unloadback::showEvent(QShowEvent *)
{
    Update();
    bSave = false;
}
void dialog_unloadback::hideEvent(QHideEvent *)
{
    if(bSave)
        Recipe->SaveVariable();
}
void dialog_unloadback::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_unloadback::onClose()
{
    emit accept();
}
void dialog_unloadback::Update()
{
    m_ItemName.clear();
    m_ItemName.append(tr("No Use"));
    m_ItemName.append(tr("Use"));

    vars.clear();
    vars.append(HyRecipe::mdUnloadBack);
    vars.append(HyRecipe::vUnloadBackX);
    vars.append(HyRecipe::vUnloadBackY);
    vars.append(HyRecipe::vUnloadBackZ);
    vars.append(HyRecipe::vUnloadBackSpd);
    vars.append(HyRecipe::vUnloadBackDly);

    if(Recipe->Gets(vars, datas))
    {
        if((int)datas[0] < m_ItemName.size())
            ui->btnUse->setText(m_ItemName[(int)datas[0]]);

        ui->wigBack->setEnabled((int)datas[0] > 0);

        QString str;
        str.sprintf(FORMAT_POS, datas[1]);
        ui->tbPitch->setText(str);
        str.sprintf(FORMAT_POS, datas[2]);
        ui->tbPitch_2->setText(str);
        str.sprintf(FORMAT_POS, datas[3]);
        ui->tbPitch_3->setText(str);
        str.sprintf(FORMAT_SPEED, datas[4]);
        ui->tbPitch_4->setText(str);
        str.sprintf(FORMAT_TIME, datas[5]);
        ui->tbPitch_5->setText(str);
    }
}
void dialog_unloadback::onUse()
{
    dialog_mode_select* sel = (dialog_mode_select*)gGetDialog(DIG_MODE_SELECT);
    sel->InitTitle(ui->lbTitle->text());
    sel->InitRecipe((int)vars[0]);
    sel->InitNum(m_ItemName.size());
    for(int i=0;i<sel->GetNum();i++)
        sel->InitString(i,m_ItemName[i]);

    if(sel->exec() == QDialog::Accepted)
    {
        Update();
        bSave = true;
    }
}

void dialog_unloadback::onX()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-99999.999);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(SOSU_POS);
    np->m_numpad->SetTitle(ui->tbName->text());
    np->m_numpad->SetNum(ui->tbPitch->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(HyRecipe::vUnloadBackX, (float)np->m_numpad->GetNumDouble(), false))
            Update();
        bSave = true;
    }

}
void dialog_unloadback::onY()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-99999.999);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(SOSU_POS);
    np->m_numpad->SetTitle(ui->tbName_2->text());
    np->m_numpad->SetNum(ui->tbPitch_2->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(HyRecipe::vUnloadBackY, (float)np->m_numpad->GetNumDouble(), false))
            Update();
        bSave = true;
    }
}
void dialog_unloadback::onZ()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-99999.999);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(SOSU_POS);
    np->m_numpad->SetTitle(ui->tbName_3->text());
    np->m_numpad->SetNum(ui->tbPitch_3->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(HyRecipe::vUnloadBackZ, (float)np->m_numpad->GetNumDouble(), false))
            Update();
        bSave = true;
    }
}

void dialog_unloadback::onSpeed()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);
    np->m_numpad->SetTitle(ui->tbName_4->text());
    np->m_numpad->SetNum(ui->tbPitch_4->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(HyRecipe::vUnloadBackSpd, (float)np->m_numpad->GetNumDouble(), false))
            Update();
        bSave = true;
    }
}

void dialog_unloadback::onDelay()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(MIN_TIME_SET);
    np->m_numpad->SetMaxValue(MAX_TIME_SET);
    np->m_numpad->SetSosuNum(SOSU_TIME);
    np->m_numpad->SetTitle(ui->tbName_5->text());
    np->m_numpad->SetNum(ui->tbPitch_5->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(HyRecipe::vUnloadBackDly, (float)np->m_numpad->GetNumDouble(), false))
            Update();
        bSave = true;
    }
}
