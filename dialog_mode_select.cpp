#include "dialog_mode_select.h"
#include "ui_dialog_mode_select.h"

dialog_mode_select::dialog_mode_select(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_mode_select)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    m_vtText.clear();m_vtPic.clear();
    m_vtText.append(ui->lbSel1);m_vtPic.append(ui->lbSelPic1);
    m_vtText.append(ui->lbSel2);m_vtPic.append(ui->lbSelPic2);
    m_vtText.append(ui->lbSel3);m_vtPic.append(ui->lbSelPic3);
    m_vtText.append(ui->lbSel4);m_vtPic.append(ui->lbSelPic4);
    m_vtText.append(ui->lbSel5);m_vtPic.append(ui->lbSelPic5);
    m_vtText.append(ui->lbSel6);m_vtPic.append(ui->lbSelPic6);

    int i;
    for(i=0;i<m_vtText.count();i++)
    {
        connect(m_vtPic[i],SIGNAL(mouse_release()),m_vtText[i],SIGNAL(mouse_release()));
        connect(m_vtText[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
    }

    // init. variable
    m_nSelNum = 2;
    for(i=0;i<MAX_MODE_SELECT_NUM;i++)
        m_strText[i] = QString("");

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    confirm_x = ui->frConfirm->x();
    confirm_y = ui->frConfirm->y();
    confirm_w = ui->frConfirm->width();
    confirm_h = ui->frConfirm->height();

    setModal(true);

}

dialog_mode_select::~dialog_mode_select()
{
    delete ui;
}

void dialog_mode_select::showEvent(QShowEvent *)
{
    Update();
}
void dialog_mode_select::hideEvent(QHideEvent *){}
void dialog_mode_select::changeEvent(QEvent *){}

void dialog_mode_select::InitNum(int num)
{
    if(num < 2) num = 2;
    if(num > MAX_MODE_SELECT_NUM) num = MAX_MODE_SELECT_NUM;

    m_nSelNum = num;
}

int dialog_mode_select::GetNum()
{
    return m_nSelNum;
}

void dialog_mode_select::InitString(int index, QString str)
{
    if(index < 0) index = 0;
    if(index >= MAX_MODE_SELECT_NUM) index = MAX_MODE_SELECT_NUM-1;

    m_strText[index] = str;
}

void dialog_mode_select::InitRecipe(int recipe_enum)
{
    m_nRecipe = recipe_enum;
}

void dialog_mode_select::InitTitle(QString title)
{
    ui->lbTitle->setText(title);
}

void dialog_mode_select::Update()
{
    QImage img;
    img.load(":/image/image/mode_select2");
    QPixmap no = QPixmap::fromImage(img);
    img.load(":/image/image/mode_select3");
    QPixmap yes = QPixmap::fromImage(img);

    float fdata = 0;
    Recipe->Get((HyRecipe::RECIPE_NUMBER)m_nRecipe, &fdata);
    m_NowSelected = (int)fdata;

    int i;
    for(i=0;i<MAX_MODE_SELECT_NUM;i++)
    {
        if(i < m_nSelNum)
        {
            m_vtText[i]->show();
            m_vtPic[i]->show();

            m_vtText[i]->setText(m_strText[i]);

            if(m_NowSelected == i)
                m_vtPic[i]->setPixmap(yes);
            else
                m_vtPic[i]->setPixmap(no);
        }
        else
        {
            m_vtText[i]->hide();
            m_vtPic[i]->hide();
        }
    }

    int _x = confirm_x;
    int _y = confirm_y + (m_nSelNum * ui->lbSelPic1->height());
    int _w = confirm_w;
    int _h = confirm_h;
    ui->frConfirm->setGeometry(_x,_y,_w,_h);

}

void dialog_mode_select::onSelect()
{
    QString sel = sender()->objectName();

    for(int i=0;i<m_nSelNum;i++)
    {
        if(sel == m_vtPic[i]->objectName() || sel == m_vtText[i]->objectName())
        {
            /*if(i != m_NowSelected)
                Recipe->Set((HyRecipe::RECIPE_NUMBER)m_nRecipe,(float)i);

            this->accept();*/

            // new
            if(i != m_NowSelected)
                m_NowSelected = i;
            DisplayUpdate();
            return;
        }
    }
}

void dialog_mode_select::DisplayUpdate()
{
    QImage img;
    img.load(":/image/image/mode_select2");
    QPixmap no = QPixmap::fromImage(img);
    img.load(":/image/image/mode_select3");
    QPixmap yes = QPixmap::fromImage(img);

    int i;
    for(i=0;i<m_nSelNum;i++)
    {
        if(m_NowSelected == i)
            m_vtPic[i]->setPixmap(yes);
        else
            m_vtPic[i]->setPixmap(no);
    }
}

void dialog_mode_select::onOK()
{
    Recipe->Set((HyRecipe::RECIPE_NUMBER)m_nRecipe,(float)m_NowSelected);
    // for Log
    QString str,str2;
    str.sprintf("=%d", m_NowSelected);
    str2 = Recipe->toName((HyRecipe::RECIPE_NUMBER)m_nRecipe) + str;
    qDebug() << str2;
    Log->Set(HY_SETVAR_LOG, str2);

    accept();
}
void dialog_mode_select::onCancel()
{
    reject();
}
