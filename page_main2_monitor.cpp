#include "page_main2_monitor.h"
#include "ui_page_main2_monitor.h"


page_main2_monitor::page_main2_monitor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_main2_monitor)
{
    ui->setupUi(this);

    connect(ui->btnIOPic,SIGNAL(mouse_release()),this,SLOT(onIO()));
    connect(ui->btnIO,SIGNAL(mouse_press()),ui->btnIOPic,SLOT(press()));
    connect(ui->btnIO,SIGNAL(mouse_release()),ui->btnIOPic,SLOT(release()));
    connect(ui->btnIOIcon,SIGNAL(mouse_press()),ui->btnIOPic,SLOT(press()));
    connect(ui->btnIOIcon,SIGNAL(mouse_release()),ui->btnIOPic,SLOT(release()));

    connect(ui->btnMotorPic,SIGNAL(mouse_release()),this,SLOT(onMotor()));
    connect(ui->btnMotor,SIGNAL(mouse_press()),ui->btnMotorPic,SLOT(press()));
    connect(ui->btnMotor,SIGNAL(mouse_release()),ui->btnMotorPic,SLOT(release()));
    connect(ui->btnMotorIcon,SIGNAL(mouse_press()),ui->btnMotorPic,SLOT(press()));
    connect(ui->btnMotorIcon,SIGNAL(mouse_release()),ui->btnMotorPic,SLOT(release()));

    connect(ui->btnEmPic,SIGNAL(mouse_release()),this,SLOT(onEuromap()));
    connect(ui->btnEm,SIGNAL(mouse_press()),ui->btnEmPic,SLOT(press()));
    connect(ui->btnEm,SIGNAL(mouse_release()),ui->btnEmPic,SLOT(release()));
    connect(ui->btnEmIcon,SIGNAL(mouse_press()),ui->btnEmPic,SLOT(press()));
    connect(ui->btnEmIcon,SIGNAL(mouse_release()),ui->btnEmPic,SLOT(release()));

    connect(ui->btnShmPic,SIGNAL(mouse_release()),this,SLOT(onSData()));
    connect(ui->btnShm,SIGNAL(mouse_press()),ui->btnShmPic,SLOT(press()));
    connect(ui->btnShm,SIGNAL(mouse_release()),ui->btnShmPic,SLOT(release()));
    connect(ui->btnShmIcon,SIGNAL(mouse_press()),ui->btnShmPic,SLOT(press()));
    connect(ui->btnShmIcon,SIGNAL(mouse_release()),ui->btnShmPic,SLOT(release()));

    // dialog pointer clear.
    dig_motor = 0;
    dig_io = 0;
    dig_view_dmcvar = 0;
    dig_euromap = 0;

}
page_main2_monitor::~page_main2_monitor()
{
    delete ui;
}
void page_main2_monitor::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void page_main2_monitor::showEvent(QShowEvent *)
{
    UserLevel();
}

void page_main2_monitor::hideEvent(QHideEvent *){}

void page_main2_monitor::UserLevel()
{
    int user_level = Param->Get(HyParam::USER_LEVEL).toInt();
    if(user_level < USER_LEVEL_HIGH)
    {
        ui->btnShmPic->hide();
        ui->btnShm->hide();
        ui->btnShmIcon->hide();
    }
    else
    {
        ui->btnShmPic->show();
        ui->btnShm->show();
        ui->btnShmIcon->show();
    }
}

void page_main2_monitor::onIO()
{   
    if(dig_io == 0)
        dig_io = new dialog_io();

    dig_io->exec();
}

void page_main2_monitor::onSData()
{
    if(dig_view_dmcvar == 0)
        dig_view_dmcvar = new dialog_view_dmcvar();

    dig_view_dmcvar->exec();
}

void page_main2_monitor::onMotor()
{
    if(dig_motor == 0)
        dig_motor = new dialog_motor();

    dig_motor->exec();
}

void page_main2_monitor::onEuromap()
{
    if(dig_euromap == 0)
        dig_euromap = new dialog_euromap();

    dig_euromap->exec();
}

