#ifndef DIALOG_PROGRAM_MANAGE_H
#define DIALOG_PROGRAM_MANAGE_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_program_manage_edit.h"
#include "dialog_confirm.h"
#include "dialog_message.h"

namespace Ui {
class dialog_program_manage;
}

class dialog_program_manage : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_program_manage(QWidget *parent = 0);
    ~dialog_program_manage();

private: //Database
    // UI
    void Update_Recipe();
    void Display_RecipeList(int recipe_selected_index);
    void Redraw_RecipeProgList(int start_index);
    void Display_Select_RecipeProgList(int select_index);
    void Display_DbListOn(int table_index);

    bool Make_RecipeList(QStringList& recipe_list);
    bool Make_RecipeProgList(QString recipe_name, QStringList& prog_list);

private: //TP
    void Update_Tp();
    bool Make_TpProgList(QStringList& prog_list);
    void Redraw_TpProgList(int start_index);
    void Display_TpListOn(int table_index);

private: //USB
    void Update_Usb();
    bool IsUsbPath();
    void Display_UsbRefresh(bool bUsbPath);
    void Check_UsbOut();


    bool Make_UsbProgList(QStringList& prog_list);
    void Redraw_UsbProgList(int start_index);
    void Display_UsbListOn(int table_index);



public slots:
    void onClose();
    void onTimer();

    // database
    void onRecipe_Up();
    void onRecipe_Down();
    void onDbList();
    void onDbList_Up();
    void onDbList_Down();
    void onDbTool_Edit();
    void onDbTool_Del();

    // db <-> tp
    void onTp2Db();
    void onDb2Tp();

    // tp
    void onTpList();
    void onTpList_Up();
    void onTpList_Down();
    void onTpTool_Edit();
    void onTpTool_Del();

    // tp <-> usb
    void onTp2Usb();
    void onUsb2Tp();

    // Usb
    void onUsbRefresh();

    void onUsbList();
    void onUsbList_Up();
    void onUsbList_Down();
    void onUsbTool_Edit();
    void onUsbTool_Del();


private:
    Ui::dialog_program_manage *ui;

    QTimer* timer;

    dialog_program_manage_edit* dig_program_manage_edit;    // database
    dialog_program_manage_edit* dig_program_manage_edit2;   // tp
    dialog_program_manage_edit* dig_program_manage_edit3;   // usb

    // database
    QVector<QLabel3*> m_vtDbProg;
    QVector<QLabel3*> m_vtDbNo;
    QStringList m_RecipeList;
    QStringList m_RecipeProgList;
    int m_nRecipeSelectedIndex;
    int m_nDbStartIndex, m_nDbSelectedIndex;

    // TP
    QVector<QLabel3*> m_vtTpProg;
    QVector<QLabel3*> m_vtTpNo;
    QStringList m_TpProgList;
    int m_nTpStartIndex, m_nTpSelectedIndex;

    // USB
    QVector<QLabel3*> m_vtUsbProg;
    QVector<QLabel3*> m_vtUsbNo;
    QStringList m_UsbProgList;
    int m_nUsbStartIndex, m_nUsbSelectedIndex;
    QRect m_rectShow, m_rectHide;



protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_PROGRAM_MANAGE_H
