#include "dialog_robotconfig.h"
#include "ui_dialog_robotconfig.h"

dialog_robotconfig::dialog_robotconfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_robotconfig)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onSave()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtBtnPic.clear();
    m_vtBtnPic.append(ui->btnAxisPic);
    m_vtBtnPic.append(ui->btnLimitPic);
    m_vtBtnPic.append(ui->btnMotionPic);
    m_vtBtnTxt.clear();
    m_vtBtnTxt.append(ui->btnAxis);
    m_vtBtnTxt.append(ui->btnLimit);
    m_vtBtnTxt.append(ui->btnMotion);
    int i;
    for(i=0;i<m_vtBtnPic.count();i++)
    {
        connect(m_vtBtnPic[i],SIGNAL(mouse_release()),this,SLOT(onButton()));
        connect(m_vtBtnTxt[i],SIGNAL(mouse_press()),m_vtBtnPic[i],SLOT(press()));
        connect(m_vtBtnTxt[i],SIGNAL(mouse_release()),m_vtBtnPic[i],SLOT(release()));
    }

    connect(ui->btnEditPic,SIGNAL(mouse_release()),this,SLOT(onEdit()));
    connect(ui->btnEdit,SIGNAL(mouse_press()),ui->btnEditPic,SLOT(press()));
    connect(ui->btnEdit,SIGNAL(mouse_release()),ui->btnEditPic,SLOT(release()));
    connect(ui->btnEditIcon,SIGNAL(mouse_press()),ui->btnEditPic,SLOT(press()));
    connect(ui->btnEditIcon,SIGNAL(mouse_release()),ui->btnEditPic,SLOT(release()));

}

dialog_robotconfig::~dialog_robotconfig()
{
    delete ui;
}
void dialog_robotconfig::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_robotconfig::showEvent(QShowEvent *)
{
    RobotConfig->Read();

    Update(TB_AIXS);
    ui->tbTable->setCurrentCell(-1,-1);

    UserLevel();

}
void dialog_robotconfig::hideEvent(QHideEvent *){}

void dialog_robotconfig::onSave()
{
    RobotConfig->Write();
    accept();
}
void dialog_robotconfig::onCancel()
{
    reject();
}

void dialog_robotconfig::UserLevel()
{
    m_nUserLevel = Param->Get(HyParam::USER_LEVEL).toInt();

    if(m_nUserLevel < USER_LEVEL_HIGH)
    {
        ui->btnEditPic->setEnabled(false);
        ui->btnEdit->setEnabled(false);
        ui->btnEditIcon->setEnabled(false);
        ui->btnEditPic->hide();
        ui->btnEdit->hide();
        ui->btnEditIcon->hide();

        ui->btnOKPic->setEnabled(false);
        ui->btnOK->setEnabled(false);
        ui->btnOKIcon->setEnabled(false);
        ui->btnOKPic->hide();
        ui->btnOK->hide();
        ui->btnOKIcon->hide();
    }
    else
    {
        ui->btnEditPic->setEnabled(true);
        ui->btnEdit->setEnabled(true);
        ui->btnEditIcon->setEnabled(true);
        ui->btnEditPic->show();
        ui->btnEdit->show();
        ui->btnEditIcon->show();

        ui->btnOKPic->setEnabled(true);
        ui->btnOK->setEnabled(true);
        ui->btnOKIcon->setEnabled(true);
        ui->btnOKPic->show();
        ui->btnOK->show();
        ui->btnOKIcon->show();
    }
}


void dialog_robotconfig::Update(ENUM_TABLE table)
{

    switch(table)
    {
    case TB_AIXS:
        update_axis();
        break;
    case TB_LIMIT:
        update_limit();
        break;
    case TB_MOTION:
        update_motion();
        break;
    }

    SetButton(table);
    m_nNowTable = table;
}

void dialog_robotconfig::update_axis()
{
    int i,j;
    QStringList value[10];
    QTableWidgetItem* col[10];

    ui->tbTable->clear();
    m_slHeader.clear();
    m_slHeader.append(tr("Type"));
    m_slHeader.append(tr("P/R"));
    m_slHeader.append(tr("Gear"));
    m_slHeader.append(tr("RPM"));
    m_slHeader.append(tr("Acc.Time"));
    m_slHeader.append(tr("Jog Speed"));

    m_slColHeader.clear();
    for(i=0;i<USE_AXIS_NUM;i++)
        m_slColHeader.append(QString(tr("J%1")).arg(i+1));

    ui->tbTable->setColumnCount(m_slHeader.size());
    ui->tbTable->setHorizontalHeaderLabels(m_slHeader);
    ui->tbTable->setRowCount(m_slColHeader.size());
    ui->tbTable->setVerticalHeaderLabels(m_slColHeader);

    ui->tbTable->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tbTable->setSortingEnabled(false);
    ui->tbTable->horizontalHeader()->setMinimumHeight(60);
    ui->tbTable->verticalHeader()->setMinimumWidth(30);

    for(i=0;i<m_slHeader.count();i++)
        ui->tbTable->setColumnWidth(i, (int)(ui->tbTable->width()/m_slHeader.size()));
    ui->tbTable->setColumnWidth(0, 50); // type = 50pixel


    m_slRobotConfigName.clear();
    m_slRobotConfigName.append(QString("AXIS_TYPE"));
    m_slRobotConfigName.append(QString("AXIS_PPR"));
    m_slRobotConfigName.append(QString("AXIS_GEAR"));
    m_slRobotConfigName.append(QString("AXIS_RPM"));
    m_slRobotConfigName.append(QString("AXIS_ACCTIME"));
    m_slRobotConfigName.append(QString("AXIS_MSPEED"));


    for(i=0;i<m_slRobotConfigName.count();i++)
        RobotConfig->Get(m_slRobotConfigName[i],value[i]);

    // i=axis, j=col data.
    for(i=0;i<USE_AXIS_NUM;i++)
    {
        for(j=0;j<m_slHeader.count();j++)
        {
            col[j] = ui->tbTable->item(i,j);
            if(!col[j])
            {
                col[j] = new QTableWidgetItem("");
                ui->tbTable->setItem(i,j,col[j]);
            }

            if(value[j].size() <= i)
                continue;

            ui->tbTable->item(i,j)->setText(value[j].at(i));
            ui->tbTable->item(i,j)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        }
    }
}

void dialog_robotconfig::update_limit()
{
    int i,j;
    QStringList value[10];
    QTableWidgetItem* col[10];

    ui->tbTable->clear();
    m_slHeader.clear();
    m_slHeader.append(tr("Minus Limit"));
    m_slHeader.append(tr("Plus Limit"));

    m_slColHeader.clear();
    for(i=0;i<USE_AXIS_NUM;i++)
        m_slColHeader.append(QString(tr("J%1")).arg(i+1));

    ui->tbTable->setColumnCount(m_slHeader.size());
    ui->tbTable->setHorizontalHeaderLabels(m_slHeader);
    ui->tbTable->setRowCount(m_slColHeader.size());
    ui->tbTable->setVerticalHeaderLabels(m_slColHeader);

    ui->tbTable->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->tbTable->setSortingEnabled(false);
    ui->tbTable->horizontalHeader()->setMinimumHeight(60);
    ui->tbTable->verticalHeader()->setMinimumWidth(30);

    for(i=0;i<m_slHeader.count();i++)
        ui->tbTable->setColumnWidth(i, (int)((ui->tbTable->width()-50)/m_slHeader.size()));
    //ui->tbTable->setColumnWidth(0, 50); // type = 50pixel

    m_slRobotConfigName.clear();
    m_slRobotConfigName.append(QString("AXIS_LIMITM"));
    m_slRobotConfigName.append(QString("AXIS_LIMITP"));


    for(i=0;i<m_slRobotConfigName.count();i++)
        RobotConfig->Get(m_slRobotConfigName[i],value[i]);

    // i=axis, j=col data.
    for(i=0;i<USE_AXIS_NUM;i++)
    {
        for(j=0;j<m_slHeader.count();j++)
        {
            col[j] = ui->tbTable->item(i,j);
            if(!col[j])
            {
                col[j] = new QTableWidgetItem("");
                ui->tbTable->setItem(i,j,col[j]);
            }

            if(value[j].size() <= i)
                continue;

            ui->tbTable->item(i,j)->setText(value[j].at(i));
            ui->tbTable->item(i,j)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        }
    }
}

void dialog_robotconfig::update_motion()
{
    int i,j;
    QStringList value[10];
    QTableWidgetItem* col[10];
    QStringList slName,slUnit;

    ui->tbTable->clear();
    m_slHeader.clear();
    m_slHeader.append(tr("Name"));
    m_slHeader.append(tr("Unit"));
    m_slHeader.append(tr("Value"));

    slName.clear();
    slName.append(tr("Linear Max.Speed"));
    slName.append(tr("Rotation Max.Speed"));
    slName.append(tr("Linear Max.JogSpeed"));
    slName.append(tr("Rotation Max.JogSpeed"));
    slName.append(tr("Acc.Time"));
    slName.append(tr("Min.Accuracy"));

    slUnit.clear();
    slUnit.append(tr("mm/sec"));
    slUnit.append(tr("deg/sec"));
    slUnit.append(tr("mm/sec"));
    slUnit.append(tr("deg/sec"));
    slUnit.append(tr("sec"));
    slUnit.append(tr("mm"));

    ui->tbTable->setColumnCount(m_slHeader.size());
    ui->tbTable->setHorizontalHeaderLabels(m_slHeader);
    ui->tbTable->setRowCount(slName.size());

    ui->tbTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tbTable->setSortingEnabled(false);
    ui->tbTable->horizontalHeader()->setMinimumHeight(60);
    ui->tbTable->verticalHeader()->setMinimumWidth(30);

    //for(i=0;i<m_slHeader.count();i++)
    //    ui->tbTable->setColumnWidth(i, (int)((ui->tbTable->width()-50)/m_slHeader.size()));

    ui->tbTable->setColumnWidth(0, 300);
    ui->tbTable->setColumnWidth(1, 100);
    ui->tbTable->setColumnWidth(2, 100);

    m_slRobotConfigName.clear();
    m_slRobotConfigName.append(QString("W_SPEED"));
    m_slRobotConfigName.append(QString("W_MSPEED"));
    m_slRobotConfigName.append(QString("W_ACCTIME"));
    m_slRobotConfigName.append(QString("W_MINACCURACY"));


    // read data.
    RobotConfig->Get(m_slRobotConfigName[0],value[0]);
    value[1].append(value[0].at(1));
    RobotConfig->Get(m_slRobotConfigName[1],value[2]);
    value[3].append(value[2].at(1));
    RobotConfig->Get(m_slRobotConfigName[2],value[4]);
    RobotConfig->Get(m_slRobotConfigName[3],value[5]);

    // i=axis, j=col data.
    for(i=0;i<slName.count();i++)
    {
        for(j=0;j<m_slHeader.count();j++)
        {
            col[j] = ui->tbTable->item(i,j);
            if(!col[j])
            {
                col[j] = new QTableWidgetItem("");
                ui->tbTable->setItem(i,j,col[j]);
            }
            ui->tbTable->item(i,j)->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        }

        ui->tbTable->item(i,0)->setText(slName[i]);
        ui->tbTable->item(i,1)->setText(slUnit[i]);

        if(value[i].size() <= 0)
            continue;

        ui->tbTable->item(i,2)->setText(value[i].at(0));
    }
}


void dialog_robotconfig::SetButton(ENUM_TABLE table)
{
    for(int i=0;i<m_vtBtnPic.count();i++)
        m_vtBtnTxt[i]->setAutoFillBackground(i==table);
}

void dialog_robotconfig::onButton()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtBtnPic.indexOf(btn);
    if(index < 0) return;

    Update((ENUM_TABLE)index);
}

void dialog_robotconfig::onEdit()
{
    int row = ui->tbTable->currentRow();
    int col = ui->tbTable->currentColumn();

    qDebug() << "row=" << row << "col=" << col;
    if(row < 0 || col < 0) return;

    if(m_nNowTable == TB_MOTION)
    {
        // special case. (per property)
        edit_motion(row, col);
    }
    else if(m_nNowTable == TB_AIXS)
    {
        edit_axis(row, col);
    }
    else if(m_nNowTable == TB_LIMIT)
    {
        edit_limit(row,col);
    }
    else
        return;

    Update(m_nNowTable);
    ui->tbTable->setCurrentCell(row, col);
}

void dialog_robotconfig::edit_axis(int row, int col)
{

    // get data.
    QStringList value;
    RobotConfig->Get(m_slRobotConfigName[col], value);

    dialog_keyboard* dk = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    dk->m_kb->SetTitle(m_slHeader[col]);
    dk->m_kb->SetText(value[row]);

    if(col == 0)    // type
    {
        if(dk->exec() == QDialog::Accepted)
        {
            if(dk->m_kb->GetText() == "T" || dk->m_kb->GetText() == "R")
            {
                value[row] = dk->m_kb->GetText();
            }
            else
            {
                qDebug() << "Input Error !!";
            }
        }
    }
    else
    {
        if(dk->exec() == QDialog::Accepted)
        {
            bool bOk=false;
            dk->m_kb->GetText().toDouble(&bOk);
            if(bOk)
            {
                value[row] = dk->m_kb->GetText();
                qDebug() << "Ok Number" << dk->m_kb->GetText();
            }
            else
            {
                qDebug() << "No Number" << dk->m_kb->GetText();
            }
        }
    }

    RobotConfig->Set(m_slRobotConfigName[col], value);

}

void dialog_robotconfig::edit_limit(int row, int col)
{
    // get data.
    QStringList value;
    RobotConfig->Get(m_slRobotConfigName[col], value);

    dialog_keyboard* dk = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    dk->m_kb->SetTitle(m_slHeader[col]);
    dk->m_kb->SetText(value[row]);

    if(dk->exec() == QDialog::Accepted)
    {
        bool bOk=false;
        dk->m_kb->GetText().toDouble(&bOk);
        if(bOk)
        {
            value[row] = dk->m_kb->GetText();
            qDebug() << "Ok Number" << dk->m_kb->GetText();
        }
        else
        {
            qDebug() << "No Number" << dk->m_kb->GetText();
        }
    }

    RobotConfig->Set(m_slRobotConfigName[col], value);
}

void dialog_robotconfig::edit_motion(int row, int col)
{
    int index=0;
    if(row == 0 || row == 1)
        index = 0;
    else if(row == 2 || row == 3)
        index = 1;
    else if(row == 4)
        index = 2;
    else if(row == 5)
        index = 3;

    col = 0; // no use
    qDebug() << "Edit Motion " <<"row=" << row << "col=" << col;

    // get data.
    QStringList value;
    if(!RobotConfig->Get(m_slRobotConfigName[index], value))
        return;

    dialog_keyboard* dk = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    dk->m_kb->SetTitle(m_slHeader[2]);
    if(value.size() >= 2)
        dk->m_kb->SetText(value[row%2]);
    else
        dk->m_kb->SetText(value[0]);

    if(dk->exec() == QDialog::Accepted)
    {
        bool bOk=false;
        dk->m_kb->GetText().toDouble(&bOk);
        if(bOk)
        {
            if(value.size() >= 2)
                value[row%2] = dk->m_kb->GetText();
            else
                value[0] = dk->m_kb->GetText();

            qDebug() << "Ok Number" << dk->m_kb->GetText();
        }
        else
        {
            qDebug() << "No Number" << dk->m_kb->GetText();
        }
    }

    RobotConfig->Set(m_slRobotConfigName[index], value);
}
