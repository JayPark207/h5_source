#include "form_stepedit_userinput.h"
#include "ui_form_stepedit_userinput.h"

form_stepedit_userinput::form_stepedit_userinput(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form_stepedit_userinput)
{
    ui->setupUi(this);
    // confirm button event
    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    MatchingInput();

    m_vtIcon.clear();m_vtSel.clear();
    m_vtIcon.append(ui->btnIcon);  m_vtSel.append(ui->btnSel);
    m_vtIcon.append(ui->btnIcon_2);m_vtSel.append(ui->btnSel_2);
    m_vtIcon.append(ui->btnIcon_3);m_vtSel.append(ui->btnSel_3);
    m_vtIcon.append(ui->btnIcon_4);m_vtSel.append(ui->btnSel_4);
    m_vtIcon.append(ui->btnIcon_5);m_vtSel.append(ui->btnSel_5);
    m_vtIcon.append(ui->btnIcon_6);m_vtSel.append(ui->btnSel_6);
    m_vtIcon.append(ui->btnIcon_7);m_vtSel.append(ui->btnSel_7);
    m_vtIcon.append(ui->btnIcon_8);m_vtSel.append(ui->btnSel_8);
    m_vtIcon.append(ui->btnIcon_9); m_vtSel.append(ui->btnSel_9);
    m_vtIcon.append(ui->btnIcon_10);m_vtSel.append(ui->btnSel_10);
    m_vtIcon.append(ui->btnIcon_11);m_vtSel.append(ui->btnSel_11);
    m_vtIcon.append(ui->btnIcon_12);m_vtSel.append(ui->btnSel_12);
    m_vtIcon.append(ui->btnIcon_13);m_vtSel.append(ui->btnSel_13);
    m_vtIcon.append(ui->btnIcon_14);m_vtSel.append(ui->btnSel_14);
    m_vtIcon.append(ui->btnIcon_15);m_vtSel.append(ui->btnSel_15);
    m_vtIcon.append(ui->btnIcon_16);m_vtSel.append(ui->btnSel_16);

    int i;
    for(i=0;i<m_vtPic.count();i++)
    {
        connect(m_vtPic[i],SIGNAL(mouse_release()),this,SLOT(onInput()));
        connect(m_vtIcon[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtIcon[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
        connect(m_vtSel[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtSel[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
    }

    connect(ui->tbDelayPic,SIGNAL(mouse_release()),this,SLOT(onDelay()));
    connect(ui->tbDelay,SIGNAL(mouse_press()),ui->tbDelayPic,SLOT(press()));
    connect(ui->tbDelay,SIGNAL(mouse_release()),ui->tbDelayPic,SLOT(release()));

    connect(ui->tbTypeOnPic,SIGNAL(mouse_release()),this,SLOT(onType()));
    connect(ui->tbTypeOnIcon,SIGNAL(mouse_press()),ui->tbTypeOnPic,SLOT(press()));
    connect(ui->tbTypeOnIcon,SIGNAL(mouse_release()),ui->tbTypeOnPic,SLOT(release()));

    connect(ui->tbTypeOffPic,SIGNAL(mouse_release()),this,SLOT(onType()));
    connect(ui->tbTypeOffIcon,SIGNAL(mouse_press()),ui->tbTypeOffPic,SLOT(press()));
    connect(ui->tbTypeOffIcon,SIGNAL(mouse_release()),ui->tbTypeOffPic,SLOT(release()));

    connect(ui->tbCondAndPic,SIGNAL(mouse_release()),this,SLOT(onCond()));
    connect(ui->tbCondAnd,SIGNAL(mouse_press()),ui->tbCondAndPic,SLOT(press()));
    connect(ui->tbCondAnd,SIGNAL(mouse_release()),ui->tbCondAndPic,SLOT(release()));

    connect(ui->tbCondOrPic,SIGNAL(mouse_release()),this,SLOT(onCond()));
    connect(ui->tbCondOr,SIGNAL(mouse_press()),ui->tbCondOrPic,SLOT(press()));
    connect(ui->tbCondOr,SIGNAL(mouse_release()),ui->tbCondOrPic,SLOT(release()));

}

form_stepedit_userinput::~form_stepedit_userinput()
{
    delete ui;
}
void form_stepedit_userinput::showEvent(QShowEvent *)
{
    Update();
}
void form_stepedit_userinput::hideEvent(QHideEvent *)
{

}
void form_stepedit_userinput::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}


void form_stepedit_userinput::onOK()
{
    if(Save())
    {
        emit sigClose();
    }
    else
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Save Error"));
        msg->Message(tr("Save Fail!"),tr("Try again, please!"));
        msg->exec();
    }
}
void form_stepedit_userinput::onCancel()
{
    emit sigClose();
}

void form_stepedit_userinput::MatchingInput()
{
    m_vtPic.clear();
    m_vtInputNum.clear();

    // here "m_vtInputNum in input number" is modify ~~~~!!!!!
    m_vtPic.append(ui->btnPic);    m_vtInputNum.append(HyInput::VAC1   +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_2);  m_vtInputNum.append(HyInput::VAC2   +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_3);  m_vtInputNum.append(HyInput::VAC3   +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_4);  m_vtInputNum.append(HyInput::VAC4   +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_5);  m_vtInputNum.append(HyInput::CHUCK  +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_6);  m_vtInputNum.append(HyInput::GRIP   +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_7);  m_vtInputNum.append(HyInput::NIPPER +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_8);  m_vtInputNum.append(HyInput::INPUT_OFFSET); //[7]extend
    m_vtPic.append(ui->btnPic_9);  m_vtInputNum.append(HyInput::USER1  +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_10); m_vtInputNum.append(HyInput::USER2  +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_11); m_vtInputNum.append(HyInput::USER3  +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_12); m_vtInputNum.append(HyInput::USER4  +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_13); m_vtInputNum.append(HyInput::USER5  +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_14); m_vtInputNum.append(HyInput::USER6  +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_15); m_vtInputNum.append(HyInput::USER7  +HyInput::INPUT_OFFSET);
    m_vtPic.append(ui->btnPic_16); m_vtInputNum.append(HyInput::USER8  +HyInput::INPUT_OFFSET);
}



void form_stepedit_userinput::Init(int index, QString title, int buf_index, bool only_time)
{
    m_nSelectedIndex = index;
    m_nBufIndex = buf_index;
    m_strTitle = title;
    m_bOnlyTime = only_time;
}

void form_stepedit_userinput::Update()
{
    this->SetDisplay(m_bOnlyTime);
    ui->lbTitle->setText(m_strTitle);
    ui->lbCurrIndex->setText(QString().setNum(m_nSelectedIndex+1));
    ui->lbCurrName->setText(StepEdit->MakeDispName(m_nSelectedIndex));

    if(m_bOnlyTime)
    {
        if(UserData->RD(m_nBufIndex, &m_TimeData))
        {
            DisplayDelay();
        }
    }
    else
    {
        if(UserData->RD(m_nBufIndex, &m_Data))
        {
            Set_Extend(HyInput::INPUT_OFFSET); // reset m_vtInputNum[USERIN_EXTEND]
            Init_Extend(m_Data); // for extend

            DisplayInput();
            DisplayDelay();
            SetType((HyUserData::ENUM_TYPE)((int)m_Data.Type));
            SetCond((HyUserData::ENUM_CONDITION)((int)m_Data.Condition));
        }
    }

}

void form_stepedit_userinput::SetDisplay(bool only_time)
{
    if(only_time)
    {
        ui->grpInput->setEnabled(false);
        ui->wigCond->hide();
        ui->wigType->hide();

        for(int i=0;i<m_vtSel.count();i++)
            m_vtSel[i]->hide();
    }
    else
    {
        ui->grpInput->setEnabled(true);
        ui->wigCond->show();
        ui->wigType->show();
    }
}

void form_stepedit_userinput::DisplayInput()
{
    int i;
    for(i=0;i<m_vtSel.count();i++)
        m_vtSel[i]->hide();

    int index;
    for(int i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
    {
        index = m_vtInputNum.indexOf((int)m_Data.Sel[i]);
        if(index >= 0)
            m_vtSel[index]->show();
    }
}

void form_stepedit_userinput::SetInput(int index)
{
    m_Data.Sel[index] = (float)m_vtInputNum[index];
}
void form_stepedit_userinput::SetInput(int pic_index, int sel_index)
{
    m_Data.Sel[sel_index] = (float)m_vtInputNum[pic_index];
}
void form_stepedit_userinput::ResetInput(int index)
{
    m_Data.Sel[index] = 0.0;
}

void form_stepedit_userinput::onInput()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtPic.indexOf(btn);
    if(index < 0) return;

    int sel_index = GetEmptyIndex(m_Data);
    if(sel_index < 0)
    {
        if(m_vtSel[index]->isVisible())
        {
            sel_index = GetSelIndex(index);
            if(sel_index >= 0)
            {
                ResetInput(sel_index);
                if(index == USERIN_EXTEND)
                    Set_Extend(HyInput::INPUT_OFFSET);
            }
        }
    }
    else
    {
        if(m_vtSel[index]->isVisible())
        {
            sel_index = GetSelIndex(index);
            if(sel_index >= 0)
            {
                ResetInput(sel_index);
                if(index == USERIN_EXTEND)
                    Set_Extend(HyInput::INPUT_OFFSET);
            }
        }
        else
        {
            // extend
            if(index == USERIN_EXTEND)
            {
                np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
                np->m_numpad->SetTitle(m_vtIcon[index]->text());
                np->m_numpad->SetNum(0);
                np->m_numpad->SetMinValue(1.0);
                np->m_numpad->SetMaxValue(32.0);
                np->m_numpad->SetSosuNum(0);
                if(np->exec() == QDialog::Accepted)
                {
                    int input_num = (int)np->m_numpad->GetNumDouble();
                    if(!IsOK_Extend(input_num + HyInput::INPUT_OFFSET))
                    {
                        // message
                        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
                        msg->SetColor(dialog_message::RED);
                        msg->Title(tr("Error"));
                        QString str;
                        str.sprintf("X%03d", input_num);
                        msg->Message(tr("This input cannot be selected!"), str);
                        msg->exec();
                        return;
                    }

                    Set_Extend(input_num + HyInput::INPUT_OFFSET);
                }
                else
                    return;
            }

            SetInput(index, sel_index);
        }
    }

    DisplayInput();
}

void form_stepedit_userinput::DisplayDelay()
{
    QString str;

    if(m_bOnlyTime)
        str.sprintf(FORMAT_TIME, m_TimeData.Delay);
    else
        str.sprintf(FORMAT_TIME, m_Data.Delay);

    ui->tbDelay->setText(str);
}

void form_stepedit_userinput::onDelay()
{
    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->tbDelayTitle->text());

    if(m_bOnlyTime)
        np->m_numpad->SetNum((double)m_TimeData.Delay);
    else
        np->m_numpad->SetNum((double)m_Data.Delay);

    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(1000.0);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        if(m_bOnlyTime)
            m_TimeData.Delay = (float)np->m_numpad->GetNumDouble();
        else
            m_Data.Delay = (float)np->m_numpad->GetNumDouble();

        DisplayDelay();
    }
}

void form_stepedit_userinput::onType()
{
    QString btn = sender()->objectName();

    if(btn == ui->tbTypeOnPic->objectName())
        SetType(HyUserData::TYPE_ON);
    else if(btn == ui->tbTypeOffPic->objectName())
        SetType(HyUserData::TYPE_OFF);

}

void form_stepedit_userinput::SetType(HyUserData::ENUM_TYPE type)
{
    if(type == HyUserData::TYPE_PULSE)
        return;

    m_Data.Type = (float)type;

    ui->tbTypeOnPic->setAutoFillBackground(false);
    ui->tbTypeOffPic->setAutoFillBackground(false);
    if(type == HyUserData::TYPE_OFF)
        ui->tbTypeOffPic->setAutoFillBackground(true);
    else
        ui->tbTypeOnPic->setAutoFillBackground(true);

}

void form_stepedit_userinput::onCond()
{
    QString btn = sender()->objectName();

    if(btn == ui->tbCondAndPic->objectName())
        SetCond(HyUserData::COND_AND);
    else if(btn == ui->tbCondOrPic->objectName())
        SetCond(HyUserData::COND_OR);

}

void form_stepedit_userinput::SetCond(HyUserData::ENUM_CONDITION cond)
{
    m_Data.Condition = (float)cond;

    ui->tbCondAndPic->setAutoFillBackground(false);
    ui->tbCondOrPic->setAutoFillBackground(false);
    if(cond == HyUserData::COND_AND)
        ui->tbCondAndPic->setAutoFillBackground(true);
    else
        ui->tbCondOrPic->setAutoFillBackground(true);
}

bool form_stepedit_userinput::Save()
{
    if(m_bOnlyTime)
    {
        return(UserData->WR(m_nBufIndex, m_TimeData));
    }
    else
    {
        return(UserData->WR(m_nBufIndex, m_Data));
    }

}

bool form_stepedit_userinput::IsMaxSelect(ST_USERDATA_INPUT& data)
{
    for(int i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
    {
        if(data.Sel[i] < HyInput::INPUT_OFFSET)
            return false;
    }
    return true;
}

int form_stepedit_userinput::GetEmptyIndex(ST_USERDATA_INPUT &data)
{
    for(int i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
    {
        if(data.Sel[i] < HyInput::INPUT_OFFSET)
            return i;
    }
    return -1;
}

int form_stepedit_userinput::GetSelIndex(int pic_index)
{
    for(int i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
    {
        if((int)m_Data.Sel[i] == m_vtInputNum[pic_index])
            return i;
    }
    return -1;
}


void form_stepedit_userinput::Init_Extend(ST_USERDATA_INPUT &data)
{
    for(int i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
    {
        if(IsOK_Extend((int)data.Sel[i]))
        {
            Set_Extend((int)data.Sel[i]);
            return;
        }
    }
}

void form_stepedit_userinput::Set_Extend(int extend_input_num)
{
    // set extend input num with exception.
    if(extend_input_num < HyInput::INPUT_OFFSET)
        extend_input_num = HyInput::INPUT_OFFSET;

    m_vtInputNum[USERIN_EXTEND] = extend_input_num;

    // display
    QString str;
    str.sprintf("X%03d", m_vtInputNum[USERIN_EXTEND]-HyInput::INPUT_OFFSET);
    m_vtIcon[USERIN_EXTEND]->setText(str);
}
bool form_stepedit_userinput::IsOK_Extend(int input_num)
{
    if(input_num <= HyInput::INPUT_OFFSET)
        return false;

    int index = m_vtInputNum.indexOf(input_num);
    if(index < 0)
    {
        if(input_num < 1033
        && input_num != 1016)
            return true;
    }
    return false;
}
