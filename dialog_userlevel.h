#ifndef DIALOG_USERLEVEL_H
#define DIALOG_USERLEVEL_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_delaying.h"

#define USERLEVEL_WAITTIME 10 //[sec]

namespace Ui {
class dialog_userlevel;
}

class dialog_userlevel : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_userlevel(QWidget *parent = 0);
    ~dialog_userlevel();

    void Update();

public slots:
    void onNumpad();
    void onBack();
    void onClear();
    void onSelect();
    void onTimer();

    void onTimer_Clock();

private:
    Ui::dialog_userlevel *ui;
    int m_nNowLevel;
    QString m_strDefaulPassword;
    QStringList m_strlPassword;

    QVector<QLabel3*> m_vtLed;
    QVector<QLabel4*> m_vtBtnPic;
    QVector<QLabel3*> m_vtBtn;
    QVector<QLabel3*> m_vtBtnIcon;

    QVector<QLabel3*> m_vtPw;

    void SetLed(int level);

    bool CheckPW(int level, QStringList word);
    void SetPW(int word_count);
    void AddWord(int number);
    void DelWord();
    void DelWordAll();
    QStringList m_strlWord;


    QTimer* timer;
    int m_nX;
    int m_nCount;

    QTimer* timer_clock;
    int m_nClockCount;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_USERLEVEL_H
