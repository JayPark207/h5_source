#include "dialog_array_info.h"
#include "ui_dialog_array_info.h"

dialog_array_info::dialog_array_info(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_array_info)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // matrix X-Y
    m_vtXY.clear();
    QVector<QLabel3*> vecY;
    vecY.clear();
    vecY.append(ui->mXY);
    vecY.append(ui->mXY_11);
    vecY.append(ui->mXY_21);
    vecY.append(ui->mXY_31);
    vecY.append(ui->mXY_41);
    vecY.append(ui->mXY_51);
    vecY.append(ui->mXY_61);
    vecY.append(ui->mXY_71);
    vecY.append(ui->mXY_81);
    vecY.append(ui->mXY_91);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_2);
    vecY.append(ui->mXY_12);
    vecY.append(ui->mXY_22);
    vecY.append(ui->mXY_32);
    vecY.append(ui->mXY_42);
    vecY.append(ui->mXY_52);
    vecY.append(ui->mXY_62);
    vecY.append(ui->mXY_72);
    vecY.append(ui->mXY_82);
    vecY.append(ui->mXY_92);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_3);
    vecY.append(ui->mXY_13);
    vecY.append(ui->mXY_23);
    vecY.append(ui->mXY_33);
    vecY.append(ui->mXY_43);
    vecY.append(ui->mXY_53);
    vecY.append(ui->mXY_63);
    vecY.append(ui->mXY_73);
    vecY.append(ui->mXY_83);
    vecY.append(ui->mXY_93);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_4);
    vecY.append(ui->mXY_14);
    vecY.append(ui->mXY_24);
    vecY.append(ui->mXY_34);
    vecY.append(ui->mXY_44);
    vecY.append(ui->mXY_54);
    vecY.append(ui->mXY_64);
    vecY.append(ui->mXY_74);
    vecY.append(ui->mXY_84);
    vecY.append(ui->mXY_94);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_5);
    vecY.append(ui->mXY_15);
    vecY.append(ui->mXY_25);
    vecY.append(ui->mXY_35);
    vecY.append(ui->mXY_45);
    vecY.append(ui->mXY_55);
    vecY.append(ui->mXY_65);
    vecY.append(ui->mXY_75);
    vecY.append(ui->mXY_85);
    vecY.append(ui->mXY_95);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_6);
    vecY.append(ui->mXY_16);
    vecY.append(ui->mXY_26);
    vecY.append(ui->mXY_36);
    vecY.append(ui->mXY_46);
    vecY.append(ui->mXY_56);
    vecY.append(ui->mXY_66);
    vecY.append(ui->mXY_76);
    vecY.append(ui->mXY_86);
    vecY.append(ui->mXY_96);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_7);
    vecY.append(ui->mXY_17);
    vecY.append(ui->mXY_27);
    vecY.append(ui->mXY_37);
    vecY.append(ui->mXY_47);
    vecY.append(ui->mXY_57);
    vecY.append(ui->mXY_67);
    vecY.append(ui->mXY_77);
    vecY.append(ui->mXY_87);
    vecY.append(ui->mXY_97);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_8);
    vecY.append(ui->mXY_18);
    vecY.append(ui->mXY_28);
    vecY.append(ui->mXY_38);
    vecY.append(ui->mXY_48);
    vecY.append(ui->mXY_58);
    vecY.append(ui->mXY_68);
    vecY.append(ui->mXY_78);
    vecY.append(ui->mXY_88);
    vecY.append(ui->mXY_98);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_9);
    vecY.append(ui->mXY_19);
    vecY.append(ui->mXY_29);
    vecY.append(ui->mXY_39);
    vecY.append(ui->mXY_49);
    vecY.append(ui->mXY_59);
    vecY.append(ui->mXY_69);
    vecY.append(ui->mXY_79);
    vecY.append(ui->mXY_89);
    vecY.append(ui->mXY_99);
    m_vtXY.append(vecY);
    vecY.clear();
    vecY.append(ui->mXY_10);
    vecY.append(ui->mXY_20);
    vecY.append(ui->mXY_30);
    vecY.append(ui->mXY_40);
    vecY.append(ui->mXY_50);
    vecY.append(ui->mXY_60);
    vecY.append(ui->mXY_70);
    vecY.append(ui->mXY_80);
    vecY.append(ui->mXY_90);
    vecY.append(ui->mXY_100);
    m_vtXY.append(vecY);

    // matrix Z
    m_vtZ.clear();

    m_vtZ.append(ui->mZ);
    m_vtZ.append(ui->mZ_2);
    m_vtZ.append(ui->mZ_3);
    m_vtZ.append(ui->mZ_4);
    m_vtZ.append(ui->mZ_5);
    m_vtZ.append(ui->mZ_6);
    m_vtZ.append(ui->mZ_7);
    m_vtZ.append(ui->mZ_8);
    m_vtZ.append(ui->mZ_9);
    m_vtZ.append(ui->mZ_10);


    int i,j;

    for(i=0;i<m_vtXY.count();i++)
    {
        for(j=0;j<m_vtXY[i].count();j++)
        {
            connect(m_vtXY[i][j],SIGNAL(mouse_release()),this,SLOT(onSelectXY()));
        }
    }

    connect(ui->btnChangePic,SIGNAL(mouse_release()),this,SLOT(onChange()));
    connect(ui->btnChangeIcon,SIGNAL(mouse_press()),ui->btnChangePic,SLOT(press()));
    connect(ui->btnChangeIcon,SIGNAL(mouse_release()),ui->btnChangePic,SLOT(release()));

    for(i=0;i<m_vtZ.count();i++)
        connect(m_vtZ[i],SIGNAL(mouse_release()),this,SLOT(onSelectZ()));


    timer_Unload = new QTimer(this);
    timer_Unload->setInterval(1000);
    timer_Insert = new QTimer(this);
    timer_Insert->setInterval(1000);

    connect(timer_Unload,SIGNAL(timeout()),this,SLOT(onTimer_Unload()));
    connect(timer_Insert,SIGNAL(timeout()),this,SLOT(onTimer_Insert()));

    connect(ui->btnUpdatePic,SIGNAL(mouse_release()),this,SLOT(onUpdate()));
    connect(ui->btnUpdate,SIGNAL(mouse_press()),ui->btnUpdatePic,SLOT(press()));
    connect(ui->btnUpdate,SIGNAL(mouse_release()),ui->btnUpdatePic,SLOT(release()));
    connect(ui->btnUpdateIcon,SIGNAL(mouse_press()),ui->btnUpdatePic,SLOT(press()));
    connect(ui->btnUpdateIcon,SIGNAL(mouse_release()),ui->btnUpdatePic,SLOT(release()));

    connect(ui->btnClearPic,SIGNAL(mouse_release()),this,SLOT(onClear()));
    connect(ui->btnClear,SIGNAL(mouse_press()),ui->btnClearPic,SLOT(press()));
    connect(ui->btnClear,SIGNAL(mouse_release()),ui->btnClearPic,SLOT(release()));
    connect(ui->btnClearIcon,SIGNAL(mouse_press()),ui->btnClearPic,SLOT(press()));
    connect(ui->btnClearIcon,SIGNAL(mouse_release()),ui->btnClearPic,SLOT(release()));

    connect(ui->btnSetPic,SIGNAL(mouse_release()),this,SLOT(onSet()));
    connect(ui->btnSet,SIGNAL(mouse_press()),ui->btnSetPic,SLOT(press()));
    connect(ui->btnSet,SIGNAL(mouse_release()),ui->btnSetPic,SLOT(release()));
    connect(ui->btnSetIcon,SIGNAL(mouse_press()),ui->btnSetPic,SLOT(press()));
    connect(ui->btnSetIcon,SIGNAL(mouse_release()),ui->btnSetPic,SLOT(release()));

    connect(ui->btnXpPic,SIGNAL(mouse_release()),this,SLOT(onXp()));
    connect(ui->btnXp,SIGNAL(mouse_press()),ui->btnXpPic,SLOT(press()));
    connect(ui->btnXp,SIGNAL(mouse_release()),ui->btnXpPic,SLOT(release()));
    connect(ui->btnXnPic,SIGNAL(mouse_release()),this,SLOT(onXn()));
    connect(ui->btnXn,SIGNAL(mouse_press()),ui->btnXnPic,SLOT(press()));
    connect(ui->btnXn,SIGNAL(mouse_release()),ui->btnXnPic,SLOT(release()));

    connect(ui->btnYpPic,SIGNAL(mouse_release()),this,SLOT(onYp()));
    connect(ui->btnYp,SIGNAL(mouse_press()),ui->btnYpPic,SLOT(press()));
    connect(ui->btnYp,SIGNAL(mouse_release()),ui->btnYpPic,SLOT(release()));
    connect(ui->btnYnPic,SIGNAL(mouse_release()),this,SLOT(onYn()));
    connect(ui->btnYn,SIGNAL(mouse_press()),ui->btnYnPic,SLOT(press()));
    connect(ui->btnYn,SIGNAL(mouse_release()),ui->btnYnPic,SLOT(release()));

    connect(ui->btnZpPic,SIGNAL(mouse_release()),this,SLOT(onZp()));
    connect(ui->btnZp,SIGNAL(mouse_press()),ui->btnZpPic,SLOT(press()));
    connect(ui->btnZp,SIGNAL(mouse_release()),ui->btnZpPic,SLOT(release()));
    connect(ui->btnZnPic,SIGNAL(mouse_release()),this,SLOT(onZn()));
    connect(ui->btnZn,SIGNAL(mouse_press()),ui->btnZnPic,SLOT(press()));
    connect(ui->btnZn,SIGNAL(mouse_release()),ui->btnZnPic,SLOT(release()));

    // variable & pointer clear
    pxmUpdateLed.clear();

}

dialog_array_info::~dialog_array_info()
{
    delete ui;
}
void dialog_array_info::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_array_info::showEvent(QShowEvent *)
{
    // first view
    if(m_bUnload)
        Update_Unload();
    else if(m_bInsert)
        Update_Insert();
    // control enable change button.
    ui->grpChange->setEnabled((m_bUnload && m_bInsert));

}
void dialog_array_info::hideEvent(QHideEvent *)
{
    timer_Unload->stop();
    timer_Insert->stop();
}

void dialog_array_info::Init(bool bUnload, bool bInsert)
{
    m_bUnload = bUnload;
    m_bInsert = bInsert;
}

void dialog_array_info::onChange()
{
    if(ui->grpTitleUnload->isEnabled())
    {
        Update_Insert();
    }
    else
    {
        Update_Unload();
    }
}

void dialog_array_info::onTimer_Unload()
{
    Refresh_Unload();
}
void dialog_array_info::onTimer_Insert()
{
    Refresh_Insert();
}

void dialog_array_info::Update_Unload()
{
    timer_Unload->stop();
    timer_Insert->stop();

    ui->grpTitleUnload->setEnabled(true);
    ui->grpTitleInsert->setEnabled(false);

    QVector<float> datas;
    QVector<HyRecipe::RECIPE_NUMBER> vars;

    vars.append(HyRecipe::vArrXNum);
    vars.append(HyRecipe::vArrYNum);
    vars.append(HyRecipe::vArrZNum);
    vars.append(HyRecipe::vArrXOrder);
    vars.append(HyRecipe::vArrYOrder);
    vars.append(HyRecipe::vArrZOrder);
    vars.append(HyRecipe::vArrXCurr);
    vars.append(HyRecipe::vArrYCurr);
    vars.append(HyRecipe::vArrZCurr);

    if(Recipe->Gets(vars, datas))
    {
        MakeMatrixXYZ(datas[0], datas[1], datas[2]);
        MakeMatrixDataXYZ(datas[3], datas[4], datas[5]);
        Display_OrderNum(datas[3], datas[4], datas[5]);

        // here to data update.
        Display_Matrix(datas[6], datas[7], datas[8]);
        Display_CurrTable(m_nCurr[X], m_nCurr[Y], m_nCurr[Z]);
    }

    timer_Unload->start();
    Display_UpdateLed(true);

    m_nStartIndex[X] = 0;
    m_nStartIndex[Y] = 0;
    m_nStartIndex[Z] = 0;
}
void dialog_array_info::Update_Insert()
{
    timer_Unload->stop();
    timer_Insert->stop();

    ui->grpTitleUnload->setEnabled(false);
    ui->grpTitleInsert->setEnabled(true);

    QVector<float> datas;
    QVector<HyRecipe::RECIPE_NUMBER> vars;

    vars.append(HyRecipe::vIArrXNum);
    vars.append(HyRecipe::vIArrYNum);
    vars.append(HyRecipe::vIArrZNum);
    vars.append(HyRecipe::vIArrXOrder);
    vars.append(HyRecipe::vIArrYOrder);
    vars.append(HyRecipe::vIArrZOrder);
    vars.append(HyRecipe::vIArrXCurr);
    vars.append(HyRecipe::vIArrYCurr);
    vars.append(HyRecipe::vIArrZCurr);

    if(Recipe->Gets(vars, datas))
    {
        MakeMatrixXYZ(datas[0], datas[1], datas[2]);
        MakeMatrixDataXYZ(datas[3], datas[4], datas[5]);
        Display_OrderNum(datas[3], datas[4], datas[5]);
        // here to data update.

        Display_Matrix(datas[6], datas[7], datas[8]);
        Display_CurrTable(m_nCurr[X], m_nCurr[Y], m_nCurr[Z]);
    }

    timer_Insert->start();
    Display_UpdateLed(true);

    m_nStartIndex[X] = 0;
    m_nStartIndex[Y] = 0;
    m_nStartIndex[Z] = 0;
}

void dialog_array_info::Refresh_Unload()
{
    QVector<float> datas;
    QVector<HyRecipe::RECIPE_NUMBER> vars;

    vars.append(HyRecipe::vArrXCurr);
    vars.append(HyRecipe::vArrYCurr);
    vars.append(HyRecipe::vArrZCurr);

    if(Recipe->Gets(vars, datas))
    {
        if(timer_Unload->isActive())
        {
            if(m_nCurr[X] == datas[0] && m_nCurr[Y] == datas[1] && m_nCurr[Z] == datas[2])
                return;
        }

        Display_Matrix(datas[0], datas[1], datas[2]);
    }
}

void dialog_array_info::Refresh_Insert()
{
    QVector<float> datas;
    QVector<HyRecipe::RECIPE_NUMBER> vars;

    vars.append(HyRecipe::vIArrXCurr);
    vars.append(HyRecipe::vIArrYCurr);
    vars.append(HyRecipe::vIArrZCurr);

    if(Recipe->Gets(vars, datas))
    {
        if(timer_Unload->isActive())
        {
            if(m_nCurr[X] == datas[0] && m_nCurr[Y] == datas[1] && m_nCurr[Z] == datas[2])
                return;
        }

        Display_Matrix(datas[0], datas[1], datas[2]);
    }
}


/*
void dialog_array_info::MakeMatrixXYZ(int xnum, int ynum, int znum)
{
    // saturation.
    if(xnum < 1) xnum = 1;
    if(ynum < 1) ynum = 1;
    if(znum < 1) znum = 1;
    if(xnum > MAX_ARR_X_NUM) xnum = MAX_ARR_X_NUM;
    if(ynum > MAX_ARR_Y_NUM) ynum = MAX_ARR_Y_NUM;
    if(znum > MAX_ARR_Z_NUM) znum = MAX_ARR_Z_NUM;

    int total_width = ui->grpXYmat->width();
    int total_height = ui->grpXYmat->height();
    int total_z_width = ui->grpZmat->width();
    int total_z_height = ui->grpZmat->height();


    int each_width = total_width / xnum;
    int each_height = total_height / ynum;
    int each_z_height = total_z_height / znum;


    // view control
    int i,j;
    for(i=0;i<m_vtXY.count();i++)
    {
        for(j=0;j<m_vtXY[i].count();j++)
        {
            if(i < xnum && j < ynum)
            {
                m_vtXY[i][j]->show();
                m_vtXY[i][j]->setGeometry(i*each_width, j*each_height,
                                          each_width, each_height);
            }
            else
                m_vtXY[i][j]->hide();
        }
    }

    for(i=0;i<m_vtZ.count();i++)
    {
        if(i < znum)
        {
            m_vtZ[i]->show();
            m_vtZ[i]->setGeometry(0, i*each_z_height,
                                  total_z_width, each_z_height);
        }
        else
            m_vtZ[i]->hide();
    }

    // make member variable
    m_nNum[X] = xnum;
    m_nNum[Y] = ynum;
    m_nNum[Z] = znum;


}*/

//new
void dialog_array_info::MakeMatrixXYZ(int xnum, int ynum, int znum)
{
    // saturation.
    if(xnum < 1) xnum = 1;
    if(ynum < 1) ynum = 1;
    if(znum < 1) znum = 1;
    if(xnum > MAX_ARR_X_NUM) xnum = MAX_ARR_X_NUM;
    if(ynum > MAX_ARR_Y_NUM) ynum = MAX_ARR_Y_NUM;
    if(znum > MAX_ARR_Z_NUM) znum = MAX_ARR_Z_NUM;

    // x,y,z num save & control button enable
    m_nNum[X] = xnum;m_nNum[Y] = ynum;m_nNum[Z] = znum;

    ui->wigX->setEnabled(m_nNum[X] > MAX_ARR_X_VIEW);
    ui->wigY->setEnabled(m_nNum[Y] > MAX_ARR_Y_VIEW);
    ui->wigZ->setEnabled(m_nNum[Z] > MAX_ARR_Z_VIEW);

    if(xnum > MAX_ARR_X_VIEW) xnum = MAX_ARR_X_VIEW;
    if(ynum > MAX_ARR_X_VIEW) ynum = MAX_ARR_X_VIEW;
    if(znum > MAX_ARR_X_VIEW) znum = MAX_ARR_X_VIEW;

    int total_width = ui->grpXYmat->width();
    int total_height = ui->grpXYmat->height();
    int total_z_width = ui->grpZmat->width();
    int total_z_height = ui->grpZmat->height();


    int each_width = total_width / xnum;
    int each_height = total_height / ynum;
    int each_z_height = total_z_height / znum;


    // view control
    int i,j;
    for(i=0;i<m_vtXY.count();i++)
    {
        for(j=0;j<m_vtXY[i].count();j++)
        {
            if(i < xnum && j < ynum)
            {
                m_vtXY[i][j]->show();
                m_vtXY[i][j]->setGeometry(i*each_width, j*each_height,
                                          each_width, each_height);
            }
            else
                m_vtXY[i][j]->hide();
        }
    }

    for(i=0;i<m_vtZ.count();i++)
    {
        if(i < znum)
        {
            m_vtZ[i]->show();
            m_vtZ[i]->setGeometry(0, i*each_z_height,
                                  total_z_width, each_z_height);
        }
        else
            m_vtZ[i]->hide();
    }

}

void dialog_array_info::MakeMatrixDataXYZ(int xord, int yord, int zord)
{
    if(xord == yord || yord == zord || xord == zord)
    {
        qDebug() << "Matrix Order Data Error!! " << xord << yord << zord;
        xord=0;yord=1;zord=2;
        qDebug() << "Matrix Order Change!! " << xord << yord << zord;
    }

    m_nOrder[X] = xord;
    m_nOrder[Y] = yord;
    m_nOrder[Z] = zord;

    // change num in order.
    int i, j, k;
    int num[3];
    int datX,datY,datZ;
    int count = 1;

    // matrix clear
    for(k=0;k<MAX_ARR_Z_NUM;k++)
        for(j=0;j<MAX_ARR_Y_NUM;j++)
            for(i=0;i<MAX_ARR_X_NUM;i++)
                m_nXYZ[i][j][k] = 0;

    num[xord] = m_nNum[X];
    num[yord] = m_nNum[Y];
    num[zord] = m_nNum[Z];

    for(k=0;k<num[2];k++)
    {
        for(j=0;j<num[1];j++)
        {
            for(i=0;i<num[0];i++)
            {
                // i , j, k => fix
                // parsing. X, Y, Z
                if(m_nOrder[X] == 0)
                {
                    datX = i;
                    if(m_nOrder[Y] == 1)
                    {
                        datY = j;
                        datZ = k;
                    }
                    else
                    {
                        datZ = j;
                        datY = k;
                    }
                }
                else
                {
                    if(m_nOrder[Y] == 0)
                    {
                        datY = i;
                        if(m_nOrder[Z] == 1)
                        {
                            datZ = j;
                            datX = k;
                        }
                        else
                        {
                            datX = j;
                            datZ = k;
                        }
                    }
                    else
                    {
                        datZ = i;
                        if(m_nOrder[X] == 1)
                        {
                            datX = j;
                            datY = k;
                        }
                        else
                        {
                            datY = j;
                            datX = k;
                        }
                    }
                }

                m_nXYZ[datX][datY][datZ] = count;
                count++;
            }
        }
    }
}

void dialog_array_info::Display_Matrix(int xcur, int ycur, int zcur, int start_x, int start_y, int start_z)
{
    // exception.
    if(xcur < 0) return;
    if(xcur >= m_nNum[X]) return;
    if(ycur < 0) return;
    if(ycur >= m_nNum[Y]) return;
    if(zcur < 0) return;
    if(zcur >= m_nNum[Z]) return;

    m_nCurr[X] = xcur;
    m_nCurr[Y] = ycur;
    m_nCurr[Z] = zcur;
    m_nSelectZ = zcur;

    if(start_x == -1 || start_y == -1 || start_z == -1)
    {
        // start index auto cal
        if(xcur >= MAX_ARR_X_VIEW)
            start_x = (xcur - MAX_ARR_X_VIEW) + 1;
        else
            start_x = 0;

        if(ycur >= MAX_ARR_Y_VIEW)
            start_y = (ycur - MAX_ARR_Y_VIEW) + 1;
        else
            start_y = 0;

        if(zcur >= MAX_ARR_Z_VIEW)
            start_z = (zcur - MAX_ARR_Z_VIEW) + 1;
        else
            start_z = 0;
    }
    else
    {
        // start index manual   
        if(start_x < 0) start_x = 0;
        if(m_nNum[X] > MAX_ARR_X_VIEW)
        {
            if(start_x + MAX_ARR_X_VIEW > m_nNum[X])
                start_x = m_nNum[X] - MAX_ARR_X_VIEW;
        }
        else
            start_x = 0;

        if(start_y < 0) start_y = 0;
        if(m_nNum[Y] > MAX_ARR_Y_VIEW)
        {
            if(start_y + MAX_ARR_Y_VIEW > m_nNum[Y])
                start_y = m_nNum[Y] - MAX_ARR_Y_VIEW;
        }
        else
            start_y = 0;

        if(start_z < 0) start_z = 0;
        if(m_nNum[Z] > MAX_ARR_Z_VIEW)
        {
            if(start_z + MAX_ARR_Z_VIEW > m_nNum[Z])
                start_z = m_nNum[Z] - MAX_ARR_Z_VIEW;
        }
        else
            start_z = 0;
    }

    m_nStartIndex[X] = start_x;
    m_nStartIndex[Y] = start_y;
    m_nStartIndex[Z] = start_z;


    int x,y;
    int index_x, index_y;
    for(y=0;y<MAX_ARR_Y_VIEW;y++)
    {
        for(x=0;x<MAX_ARR_X_VIEW;x++)
        {
            index_x = start_x + x;
            index_y = start_y + y;

            m_vtXY[x][y]->setText(QString().setNum(m_nXYZ[index_x][index_y][zcur]));

            if(m_nXYZ[index_x][index_y][zcur] < m_nXYZ[xcur][ycur][zcur])
                ChangeCellWorked(x,y);
            else if(m_nXYZ[index_x][index_y][zcur] > m_nXYZ[xcur][ycur][zcur])
                ChangeCellReady(x,y);
            else
                ChangeCellWorking(x,y);
        }
    }

    int index_z;
    for(int z=0;z<MAX_ARR_Z_VIEW;z++)
    {
        index_z = z + start_z;
        m_vtZ[z]->setText(QString().setNum(index_z + 1));
        m_vtZ[z]->setAutoFillBackground(zcur == index_z);
    }

}

void dialog_array_info::Display_Matrix(int xcur, int ycur, int zcur, int start_x, int start_y, int start_z, int select_z)
{
    // exception.
    if(xcur < 0) return;
    if(xcur >= m_nNum[X]) return;
    if(ycur < 0) return;
    if(ycur >= m_nNum[Y]) return;
    if(zcur < 0) return;
    if(zcur >= m_nNum[Z]) return;

    // start index manual
    if(start_x < 0) start_x = 0;
    if(m_nNum[X] > MAX_ARR_X_VIEW)
    {
        if(start_x + MAX_ARR_X_VIEW > m_nNum[X])
            start_x = m_nNum[X] - MAX_ARR_X_VIEW;
    }
    else
        start_x = 0;

    if(start_y < 0) start_y = 0;
    if(m_nNum[Y] > MAX_ARR_Y_VIEW)
    {
        if(start_y + MAX_ARR_Y_VIEW > m_nNum[Y])
            start_y = m_nNum[Y] - MAX_ARR_Y_VIEW;
    }
    else
        start_y = 0;

    if(start_z < 0) start_z = 0;
    if(m_nNum[Z] > MAX_ARR_Z_VIEW)
    {
        if(start_z + MAX_ARR_Z_VIEW > m_nNum[Z])
            start_z = m_nNum[Z] - MAX_ARR_Z_VIEW;
    }
    else
        start_z = 0;

    m_nStartIndex[X] = start_x;
    m_nStartIndex[Y] = start_y;
    m_nStartIndex[Z] = start_z;


    int x,y;
    int index_x, index_y;
    for(y=0;y<MAX_ARR_Y_VIEW;y++)
    {
        for(x=0;x<MAX_ARR_X_VIEW;x++)
        {
            index_x = start_x + x;
            index_y = start_y + y;

            m_vtXY[x][y]->setText(QString().setNum(m_nXYZ[index_x][index_y][select_z]));

            if(m_nXYZ[index_x][index_y][select_z] < m_nXYZ[xcur][ycur][zcur])
                ChangeCellWorked(x,y);
            else if(m_nXYZ[index_x][index_y][select_z] > m_nXYZ[xcur][ycur][zcur])
                ChangeCellReady(x,y);
            else
                ChangeCellWorking(x,y);
        }
    }

    int index_z;
    for(int z=0;z<MAX_ARR_Z_VIEW;z++)
    {
        index_z = z + start_z;
        m_vtZ[z]->setText(QString().setNum(index_z + 1));
        m_vtZ[z]->setAutoFillBackground(select_z == index_z);
    }

}

void dialog_array_info::View_Matrix(int select_z)
{
    if(select_z < 0) return;
    if(select_z >= m_nNum[Z]) return;

    for(int j=0;j<MAX_ARR_Y_VIEW;j++)
    {
        for(int i=0;i<MAX_ARR_X_VIEW;i++)
        {
            m_vtXY[i][j]->setText(QString().setNum(m_nXYZ[i][j][select_z]));

            if(m_nXYZ[i][j][select_z] < m_nXYZ[m_nCurr[X]][m_nCurr[Y]][m_nCurr[Z]])
                ChangeCellWorked(i,j);
            else if(m_nXYZ[i][j][select_z] > m_nXYZ[m_nCurr[X]][m_nCurr[Y]][m_nCurr[Z]])
                ChangeCellReady(i,j);
            else
                ChangeCellWorking(i,j);
        }
    }
    int index_z;
    for(int z=0;z<MAX_ARR_Z_VIEW;z++)
    {
        index_z = z + m_nStartIndex[Z];
        m_vtZ[z]->setAutoFillBackground(select_z == index_z);
    }

    m_nSelectZ = select_z;
}


void dialog_array_info::ChangeCellWorked(int arr_x, int arr_y)
{
    QPalette pal;
    pal.setBrush(QPalette::Window,QBrush(QColor(255,255,0)));
    m_vtXY[arr_x][arr_y]->setPalette(pal);
}
void dialog_array_info::ChangeCellWorking(int arr_x, int arr_y)
{
    QPalette pal;
    pal.setBrush(QPalette::Window,QBrush(QColor(255,0,0)));
    m_vtXY[arr_x][arr_y]->setPalette(pal);
}
void dialog_array_info::ChangeCellReady(int arr_x, int arr_y)
{
    QPalette pal;
    pal.setBrush(QPalette::Window,QBrush(QColor(255,255,255)));
    m_vtXY[arr_x][arr_y]->setPalette(pal);
}

void dialog_array_info::ChangeFloorWorking(int arr_z)
{
    for(int i=0;i<m_nNum[Z];i++)
    {
        if(arr_z == i)
            m_vtZ[i]->setAutoFillBackground(true);
        else
            m_vtZ[i]->setAutoFillBackground(false);
    }

//    QPalette pal;
//    pal.setBrush(QPalette::Window,QBrush(QColor(255,255,0)));
//    m_vtZ[arr_z]->setPalette(pal);

}

void dialog_array_info::Display_OrderNum(int xord, int yord, int zord)
{
    QString str;
    str = QString(tr("X"));
    str += QString("%1").arg(xord+1);
    ui->mXTitle->setText(str);

    str = QString(tr("Y"));
    str += QString("%1").arg(yord+1);
    ui->mYTitle->setText(str);

    str = QString(tr("Z"));
    str += QString("%1").arg(zord+1);
    ui->mZTitle->setText(str);

}

void dialog_array_info::Display_CurrTable(int xcur, int ycur, int zcur)
{
    ui->tbXval->setText(QString().setNum(xcur+1));
    ui->tbYval->setText(QString().setNum(ycur+1));
    ui->tbZval->setText(QString().setNum(zcur+1));
}

void dialog_array_info::onSelectZ()
{
    if(!ui->grpSetBtn->isEnabled())
        return;

    QLabel3* sel = (QLabel3*)sender();

    int index = m_vtZ.indexOf(sel);
    if(index < 0) return;

    int real_index = index + m_nStartIndex[Z];

    View_Matrix(real_index);
}

void dialog_array_info::onUpdate()
{
    if(ui->grpTitleUnload->isEnabled())
    {
        if(timer_Unload->isActive())
        {
            Display_UpdateLed(false);
            timer_Unload->stop();
        }
        else
        {
            Display_UpdateLed(true);
            timer_Unload->start();
        }
    }
    else
    {
        if(timer_Insert->isActive())
        {
            Display_UpdateLed(false);
            timer_Insert->stop();
        }
        else
        {
            Display_UpdateLed(true);
            timer_Insert->start();
        }
    }

}
void dialog_array_info::onClear()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(QString(tr("CONFIRM")));
    QString str;
    str.sprintf("(%d,%d,%d) No.%d",1,1,1,1);
    conf->Message(QString(tr("Would you want to clear this point?")),str);
    if(conf->exec() == QDialog::Rejected)
        return;

    QVector<float> _datas;
    QVector<HyRecipe::RECIPE_NUMBER> _vars;

    if(ui->grpTitleUnload->isEnabled())
    {
        _vars.append(HyRecipe::vArrXCurr);
        _datas.append(0);
        _vars.append(HyRecipe::vArrYCurr);
        _datas.append(0);
        _vars.append(HyRecipe::vArrZCurr);
        _datas.append(0);

        if(Recipe->Sets(_vars, _datas))
        {
            Refresh_Unload();
            Display_CurrTable(m_nCurr[X], m_nCurr[Y], m_nCurr[Z]);
        }
    }
    else
    {
        _vars.append(HyRecipe::vIArrXCurr);
        _datas.append(0);
        _vars.append(HyRecipe::vIArrYCurr);
        _datas.append(0);
        _vars.append(HyRecipe::vIArrZCurr);
        _datas.append(0);

        if(Recipe->Sets(_vars, _datas))
        {
            Refresh_Insert();
            Display_CurrTable(m_nCurr[X], m_nCurr[Y], m_nCurr[Z]);
        }
    }

    onUpdate();
}

void dialog_array_info::Display_UpdateLed(bool onoff)
{
    if(pxmUpdateLed.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon992-9.png");
        pxmUpdateLed.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon992-10.png");
        pxmUpdateLed.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmUpdateLed.size())
    {
        ui->btnUpdateIcon->setPixmap(pxmUpdateLed[(int)onoff]);
        ui->grpSetBtn->setEnabled(!onoff);

        ui->wigX->setEnabled(!onoff);
        ui->wigY->setEnabled(!onoff);
        ui->wigZ->setEnabled(!onoff);
    }
}

void dialog_array_info::onSet()
{

    int x = ui->tbXval->text().toInt() -1;
    int y = ui->tbYval->text().toInt() -1;
    int z = ui->tbZval->text().toInt() -1;

    int view_x, view_y;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(QString(tr("CONFIRM")));
    QString str;
    str.sprintf("(%d,%d,%d) No.%d",x+1,y+1,z+1,m_nXYZ[x][y][z]);
    conf->Message(QString(tr("Would you want to set this point?")),str);
    if(conf->exec() == QDialog::Rejected)
        return;

    QVector<float> _datas;
    QVector<HyRecipe::RECIPE_NUMBER> _vars;

    if(ui->grpTitleUnload->isEnabled())
    {
        _vars.append(HyRecipe::vArrXCurr);
        _datas.append(x);
        _vars.append(HyRecipe::vArrYCurr);
        _datas.append(y);
        _vars.append(HyRecipe::vArrZCurr);
        _datas.append(z);

        if(Recipe->Sets(_vars, _datas))
        {
            Refresh_Unload();
            Display_CurrTable(m_nCurr[X], m_nCurr[Y], m_nCurr[Z]);

            view_x = m_nCurr[X] - m_nStartIndex[X];
            view_y = m_nCurr[Y] - m_nStartIndex[Y];
            m_palBefore = m_vtXY[view_x][view_y]->palette(); // new color update
        }
    }
    else
    {
        _vars.append(HyRecipe::vIArrXCurr);
        _datas.append(x);
        _vars.append(HyRecipe::vIArrYCurr);
        _datas.append(y);
        _vars.append(HyRecipe::vIArrZCurr);
        _datas.append(z);

        if(Recipe->Sets(_vars, _datas))
        {
            Refresh_Insert();
            Display_CurrTable(m_nCurr[X], m_nCurr[Y], m_nCurr[Z]);

            view_x = m_nCurr[X] - m_nStartIndex[X];
            view_y = m_nCurr[Y] - m_nStartIndex[Y];
            m_palBefore = m_vtXY[view_x][view_y]->palette(); // new color update
        }
    }

    onUpdate();
}

void dialog_array_info::onSelectXY()
{
    if(!ui->grpSetBtn->isEnabled())
        return;

    QLabel3* sel = (QLabel3*)sender();

    int index, i;
    for(i=0;i<m_vtXY.count();i++)
    {
        index = m_vtXY[i].indexOf(sel);
        if(index >= 0)
            break;
    }

    int x = i;
    int y = index;
    int z;


    for(i=0;i<m_vtZ.count();i++)
    {
        if(m_vtZ[i]->autoFillBackground())
        {
            z = i;
            break;
        }
    }

    int real_x = x + m_nStartIndex[X];
    int real_y = y + m_nStartIndex[Y];
    int real_z = z + m_nStartIndex[Z];

    Display_CurrTable(real_x, real_y, real_z);
    Display_SelectXY(x, y, z);
}

void dialog_array_info::Display_SelectXY(int xsel, int ysel, int zsel)
{
    if(zsel == m_nSel[Z])
    {
        m_vtXY[m_nSel[X]][m_nSel[Y]]->setPalette(m_palBefore);
    }

    m_palBefore = m_vtXY[xsel][ysel]->palette();

    QPalette pal;
    pal.setBrush(QPalette::Window,QBrush(QColor(0,0,255)));
    m_vtXY[xsel][ysel]->setPalette(pal);

    m_nSel[X] = xsel;
    m_nSel[Y] = ysel;
    m_nSel[Z] = zsel;
}

void dialog_array_info::onXp()
{
    if(m_nNum[X] <= (m_nStartIndex[X]+MAX_ARR_X_VIEW))
        return;

    m_nStartIndex[X]++;
    Display_Matrix(m_nCurr[X], m_nCurr[Y], m_nCurr[Z], m_nStartIndex[X], m_nStartIndex[Y], m_nStartIndex[Z], m_nSelectZ);
}
void dialog_array_info::onXn()
{
    if(m_nStartIndex[X] <= 0)
        return;

    m_nStartIndex[X]--;
    Display_Matrix(m_nCurr[X], m_nCurr[Y], m_nCurr[Z], m_nStartIndex[X], m_nStartIndex[Y], m_nStartIndex[Z], m_nSelectZ);
}

void dialog_array_info::onYp()
{
    if(m_nNum[Y] <= (m_nStartIndex[Y]+MAX_ARR_Y_VIEW))
        return;

    m_nStartIndex[Y]++;
    Display_Matrix(m_nCurr[X], m_nCurr[Y], m_nCurr[Z], m_nStartIndex[X], m_nStartIndex[Y], m_nStartIndex[Z], m_nSelectZ);
}
void dialog_array_info::onYn()
{
    if(m_nStartIndex[Y] <= 0)
        return;

    m_nStartIndex[Y]--;
    Display_Matrix(m_nCurr[X], m_nCurr[Y], m_nCurr[Z], m_nStartIndex[X], m_nStartIndex[Y], m_nStartIndex[Z], m_nSelectZ);
}

void dialog_array_info::onZp()
{
    if(m_nNum[Z] <= (m_nStartIndex[Z]+MAX_ARR_Z_VIEW))
        return;

    m_nStartIndex[Z]++;
    Display_Matrix(m_nCurr[X], m_nCurr[Y], m_nCurr[Z], m_nStartIndex[X], m_nStartIndex[Y], m_nStartIndex[Z], m_nSelectZ);
}
void dialog_array_info::onZn()
{
    if(m_nStartIndex[Z] <= 0)
        return;

    m_nStartIndex[Z]--;
    Display_Matrix(m_nCurr[X], m_nCurr[Y], m_nCurr[Z], m_nStartIndex[X], m_nStartIndex[Y], m_nStartIndex[Z], m_nSelectZ);
}
