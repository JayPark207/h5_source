#include "page_main2_setting.h"
#include "ui_page_main2_setting.h"

page_main2_setting::page_main2_setting(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_main2_setting)
{
    ui->setupUi(this);

    connect(ui->btnLangPic,SIGNAL(mouse_release()),this,SLOT(onLang()));
    connect(ui->btnLang,SIGNAL(mouse_press()),ui->btnLangPic,SLOT(press()));
    connect(ui->btnLang,SIGNAL(mouse_release()),ui->btnLangPic,SLOT(release()));
    connect(ui->btnLangIcon,SIGNAL(mouse_press()),ui->btnLangPic,SLOT(press()));
    connect(ui->btnLangIcon,SIGNAL(mouse_release()),ui->btnLangPic,SLOT(release()));

    connect(ui->btnTimeSetPic,SIGNAL(mouse_release()),this,SLOT(onTimeSet()));
    connect(ui->btnTimeSet,SIGNAL(mouse_press()),ui->btnTimeSetPic,SLOT(press()));
    connect(ui->btnTimeSet,SIGNAL(mouse_release()),ui->btnTimeSetPic,SLOT(release()));
    connect(ui->btnTimeSetIcon,SIGNAL(mouse_press()),ui->btnTimeSetPic,SLOT(press()));
    connect(ui->btnTimeSetIcon,SIGNAL(mouse_release()),ui->btnTimeSetPic,SLOT(release()));

    connect(ui->btnSpecialPic,SIGNAL(mouse_release()),this,SLOT(onSpecial()));
    connect(ui->btnSpecial,SIGNAL(mouse_press()),ui->btnSpecialPic,SLOT(press()));
    connect(ui->btnSpecial,SIGNAL(mouse_release()),ui->btnSpecialPic,SLOT(release()));
    connect(ui->btnSpecialIcon,SIGNAL(mouse_press()),ui->btnSpecialPic,SLOT(press()));
    connect(ui->btnSpecialIcon,SIGNAL(mouse_release()),ui->btnSpecialPic,SLOT(release()));

    connect(ui->btnVerPic,SIGNAL(mouse_release()),this,SLOT(onVersion()));
    connect(ui->btnVer,SIGNAL(mouse_press()),ui->btnVerPic,SLOT(press()));
    connect(ui->btnVer,SIGNAL(mouse_release()),ui->btnVerPic,SLOT(release()));
    connect(ui->btnVerIcon,SIGNAL(mouse_press()),ui->btnVerPic,SLOT(press()));
    connect(ui->btnVerIcon,SIGNAL(mouse_release()),ui->btnVerPic,SLOT(release()));

    connect(ui->btnUseskipPic,SIGNAL(mouse_release()),this,SLOT(onUseSkip()));
    connect(ui->btnUseskip,SIGNAL(mouse_press()),ui->btnUseskipPic,SLOT(press()));
    connect(ui->btnUseskip,SIGNAL(mouse_release()),ui->btnUseskipPic,SLOT(release()));
    connect(ui->btnUseskipIcon,SIGNAL(mouse_press()),ui->btnUseskipPic,SLOT(press()));
    connect(ui->btnUseskipIcon,SIGNAL(mouse_release()),ui->btnUseskipPic,SLOT(release()));

    connect(ui->btnCommPic,SIGNAL(mouse_release()),this,SLOT(onNetwork()));
    connect(ui->btnComm,SIGNAL(mouse_press()),ui->btnCommPic,SLOT(press()));
    connect(ui->btnComm,SIGNAL(mouse_release()),ui->btnCommPic,SLOT(release()));
    connect(ui->btnCommIcon,SIGNAL(mouse_press()),ui->btnCommPic,SLOT(press()));
    connect(ui->btnCommIcon,SIGNAL(mouse_release()),ui->btnCommPic,SLOT(release()));

    // dialog pointer clear.
    dig_timeset = 0;
    dig_version = 0;
    dig_language = 0;
    dig_useskip = 0;
    dig_network = 0;

}

page_main2_setting::~page_main2_setting()
{
    delete ui;
}
void page_main2_setting::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);

        Display_Flag(Param->Get(HyParam::LANGUAGE).toInt());
    }
}
void page_main2_setting::showEvent(QShowEvent *)
{
    Display_Flag(Param->Get(HyParam::LANGUAGE).toInt());
    UserLevel();
}

void page_main2_setting::hideEvent(QHideEvent *)
{

}

void page_main2_setting::UserLevel()
{
    int user_level = Param->Get(HyParam::USER_LEVEL).toInt();
    if(user_level < USER_LEVEL_HIGH)
    {
        // level 0 or 3
        ui->btnSpecialPic->hide();
        ui->btnSpecial->hide();
        ui->btnSpecialIcon->hide();

        if(user_level < USER_LEVEL_MID)
        {
            // level 0
            ui->btnCommPic->hide();
            ui->btnComm->hide();
            ui->btnCommIcon->hide();
        }
        else
        {
            // level 3
            ui->btnCommPic->show();
            ui->btnComm->show();
            ui->btnCommIcon->show();
        }
    }
    else
    {
        // level 7
        ui->btnSpecialPic->show();
        ui->btnSpecial->show();
        ui->btnSpecialIcon->show();

        ui->btnCommPic->show();
        ui->btnComm->show();
        ui->btnCommIcon->show();
    }
}

void page_main2_setting::onLang()
{
    if(dig_language == 0)
        dig_language = new dialog_language();

    dig_language->exec();
}
void page_main2_setting::onSpecial()
{
    gSetMainPage(SPECIAL_SETTING_FORM);
}

void page_main2_setting::onTimeSet()
{   
    if(dig_timeset == 0)
        dig_timeset = new dialog_timeset();

    dig_timeset->exec();
}
void page_main2_setting::onVersion()
{
    if(dig_version == 0)
        dig_version = new dialog_version();

    dig_version->exec();
}
void page_main2_setting::onUseSkip()
{
    if(dig_useskip == 0)
        dig_useskip = new dialog_useskip();

    dig_useskip->exec();
}

void page_main2_setting::onNetwork()
{
    if(dig_network == 0)
        dig_network = new dialog_network();

    dig_network->exec();
}

/** additional functions. **/
void page_main2_setting::Display_Flag(int lang_index)
{
    if(lang_index < 0) lang_index = 0;
    else if(lang_index >= Language->m_Flag.size())
        lang_index = Language->m_Flag.size()-1;

    ui->btnLangIcon->setPixmap(Language->m_Flag[lang_index]);
}
