#include "dialog_confirm.h"
#include "ui_dialog_confirm.h"

dialog_confirm::dialog_confirm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_confirm)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(reject()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));


    SetColor(DEFAULT);
}

dialog_confirm::~dialog_confirm()
{
    delete ui;
}
void dialog_confirm::showEvent(QShowEvent *)
{

}
void dialog_confirm::hideEvent(QHideEvent *)
{

}
void dialog_confirm::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_confirm::Message(QString msg1, QString msg2)
{
    ui->lbMessage1->setText(msg1);
    ui->lbMessage2->setText(msg2);
}

void dialog_confirm::Title(QString title)
{
    ui->lbTitle->setText(title);
}

void dialog_confirm::SetColor(DIAG_CONFIRM_COLOR color)
{
    QImage img;

    if(color == dialog_confirm::RED)
        img.load(":/image/image/messagebox2");
    else if(color == dialog_confirm::GREEN)
        img.load(":/image/image/messagebox3");
    else if(color == dialog_confirm::SKYBLUE)
        img.load(":/image/image/messagebox4");
    else if(color == dialog_confirm::GRAY)
        img.load(":/image/image/messagebox5");
    else if(color == dialog_confirm::BLUE)
        img.load(":/image/image/messagebox6");
    else
        img.load(":/image/image/messagebox1");

    QPixmap pxm = QPixmap::fromImage(img);
    ui->lbBasePic->setPixmap(pxm);
}
