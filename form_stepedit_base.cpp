#include "form_stepedit_base.h"
#include "ui_form_stepedit_base.h"



form_stepedit_base::form_stepedit_base(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form_stepedit_base)
{
    ui->setupUi(this);

    // confirm button event
    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // reference postion.
    mX = ui->grpWait->x();
    mY = ui->grpWait->y();
    mW = ui->grpWait->width();
    mH = ui->grpWait->height();

    // button set group - match enum
    m_vtGrp.clear();
    m_vtGrp.append(ui->grpWait);
    m_vtGrp.append(ui->grpTakeout);
    m_vtGrp.append(ui->grpUp);
    m_vtGrp.append(ui->grpUnload);
    m_vtGrp.append(ui->grpInsert);
    m_vtGrp.append(ui->grpInsert_unload);
    // add..

    // list item => max. 6ea
    m_vtListItem.clear();
    m_vtListItem.append(ui->lbPosList1);
    m_vtListItem.append(ui->lbPosList2);
    m_vtListItem.append(ui->lbPosList3);
    m_vtListItem.append(ui->lbPosList4);
    m_vtListItem.append(ui->lbPosList5);
    m_vtListItem.append(ui->lbPosList6);

    for(int i=0;i<m_vtListItem.count();i++)
    {
        m_vtListItem[i]->setText("");
        connect(m_vtListItem[i],SIGNAL(mouse_release()),this,SLOT(onList()));
    }

    connect(ui->btnListUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_press()),ui->btnListUpPic,SLOT(press()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_release()),ui->btnListUpPic,SLOT(release()));

    connect(ui->btnListDownPic,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnListDownIcon,SIGNAL(mouse_press()),ui->btnListDownPic,SLOT(press()));
    connect(ui->btnListDownIcon,SIGNAL(mouse_release()),ui->btnListDownPic,SLOT(release()));

    connect(ui->btnWait_TeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnWait_Teach,SIGNAL(mouse_press()),ui->btnWait_TeachPic,SLOT(press()));
    connect(ui->btnWait_Teach,SIGNAL(mouse_release()),ui->btnWait_TeachPic,SLOT(release()));
    connect(ui->btnWait_TeachIcon,SIGNAL(mouse_press()),ui->btnWait_TeachPic,SLOT(press()));
    connect(ui->btnWait_TeachIcon,SIGNAL(mouse_release()),ui->btnWait_TeachPic,SLOT(release()));

    connect(ui->btnTout_TeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnTout_Teach,SIGNAL(mouse_press()),ui->btnTout_TeachPic,SLOT(press()));
    connect(ui->btnTout_Teach,SIGNAL(mouse_release()),ui->btnTout_TeachPic,SLOT(release()));
    connect(ui->btnTout_TeachIcon,SIGNAL(mouse_press()),ui->btnTout_TeachPic,SLOT(press()));
    connect(ui->btnTout_TeachIcon,SIGNAL(mouse_release()),ui->btnTout_TeachPic,SLOT(release()));

    connect(ui->btnTout_MethodPic,SIGNAL(mouse_release()),this,SLOT(onMethod()));
    connect(ui->btnTout_Method,SIGNAL(mouse_press()),ui->btnTout_MethodPic,SLOT(press()));
    connect(ui->btnTout_Method,SIGNAL(mouse_release()),ui->btnTout_MethodPic,SLOT(release()));
    connect(ui->btnTout_MethodIcon,SIGNAL(mouse_press()),ui->btnTout_MethodPic,SLOT(press()));
    connect(ui->btnTout_MethodIcon,SIGNAL(mouse_release()),ui->btnTout_MethodPic,SLOT(release()));

    connect(ui->btnTout_JPic,SIGNAL(mouse_release()),this,SLOT(onJMotion()));
    connect(ui->btnTout_J,SIGNAL(mouse_press()),ui->btnTout_JPic,SLOT(press()));
    connect(ui->btnTout_J,SIGNAL(mouse_release()),ui->btnTout_JPic,SLOT(release()));
    connect(ui->btnTout_JIcon,SIGNAL(mouse_press()),ui->btnTout_JPic,SLOT(press()));
    connect(ui->btnTout_JIcon,SIGNAL(mouse_release()),ui->btnTout_JPic,SLOT(release()));

    connect(ui->btnTout_TempPic,SIGNAL(mouse_release()),this,SLOT(onTemp()));
    connect(ui->btnTout_Temp,SIGNAL(mouse_press()),ui->btnTout_TempPic,SLOT(press()));
    connect(ui->btnTout_Temp,SIGNAL(mouse_release()),ui->btnTout_TempPic,SLOT(release()));
    connect(ui->btnTout_TempIcon,SIGNAL(mouse_press()),ui->btnTout_TempPic,SLOT(press()));
    connect(ui->btnTout_TempIcon,SIGNAL(mouse_release()),ui->btnTout_TempPic,SLOT(release()));

    connect(ui->btnTout_SyncPic,SIGNAL(mouse_release()),this,SLOT(onSyncBack()));
    connect(ui->btnTout_Sync,SIGNAL(mouse_press()),ui->btnTout_SyncPic,SLOT(press()));
    connect(ui->btnTout_Sync,SIGNAL(mouse_release()),ui->btnTout_SyncPic,SLOT(release()));
    connect(ui->btnTout_SyncIcon,SIGNAL(mouse_press()),ui->btnTout_SyncPic,SLOT(press()));
    connect(ui->btnTout_SyncIcon,SIGNAL(mouse_release()),ui->btnTout_SyncPic,SLOT(release()));


    connect(ui->btnUp_TeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnUp_Teach,SIGNAL(mouse_press()),ui->btnUp_TeachPic,SLOT(press()));
    connect(ui->btnUp_Teach,SIGNAL(mouse_release()),ui->btnUp_TeachPic,SLOT(release()));
    connect(ui->btnUp_TeachIcon,SIGNAL(mouse_press()),ui->btnUp_TeachPic,SLOT(press()));
    connect(ui->btnUp_TeachIcon,SIGNAL(mouse_release()),ui->btnUp_TeachPic,SLOT(release()));

    connect(ui->btnUd_TeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnUd_Teach,SIGNAL(mouse_press()),ui->btnUd_TeachPic,SLOT(press()));
    connect(ui->btnUd_Teach,SIGNAL(mouse_release()),ui->btnUd_TeachPic,SLOT(release()));
    connect(ui->btnUd_TeachIcon,SIGNAL(mouse_press()),ui->btnUd_TeachPic,SLOT(press()));
    connect(ui->btnUd_TeachIcon,SIGNAL(mouse_release()),ui->btnUd_TeachPic,SLOT(release()));

    connect(ui->btnUd_ArrPic,SIGNAL(mouse_release()),this,SLOT(onArray()));
    connect(ui->btnUd_Arr,SIGNAL(mouse_press()),ui->btnUd_ArrPic,SLOT(press()));
    connect(ui->btnUd_Arr,SIGNAL(mouse_release()),ui->btnUd_ArrPic,SLOT(release()));
    connect(ui->btnUd_ArrIcon,SIGNAL(mouse_press()),ui->btnUd_ArrPic,SLOT(press()));
    connect(ui->btnUd_ArrIcon,SIGNAL(mouse_release()),ui->btnUd_ArrPic,SLOT(release()));

    connect(ui->btnUd_SeqPic,SIGNAL(mouse_release()),this,SLOT(onSeqeuntial()));
    connect(ui->btnUd_Seq,SIGNAL(mouse_press()),ui->btnUd_SeqPic,SLOT(press()));
    connect(ui->btnUd_Seq,SIGNAL(mouse_release()),ui->btnUd_SeqPic,SLOT(release()));
    connect(ui->btnUd_SeqIcon,SIGNAL(mouse_press()),ui->btnUd_SeqPic,SLOT(press()));
    connect(ui->btnUd_SeqIcon,SIGNAL(mouse_release()),ui->btnUd_SeqPic,SLOT(release()));

    connect(ui->btnUd_WeightPic,SIGNAL(mouse_release()),this,SLOT(onWeight()));
    connect(ui->btnUd_Weight,SIGNAL(mouse_press()),ui->btnUd_WeightPic,SLOT(press()));
    connect(ui->btnUd_Weight,SIGNAL(mouse_release()),ui->btnUd_WeightPic,SLOT(release()));
    connect(ui->btnUd_WeightIcon,SIGNAL(mouse_press()),ui->btnUd_WeightPic,SLOT(press()));
    connect(ui->btnUd_WeightIcon,SIGNAL(mouse_release()),ui->btnUd_WeightPic,SLOT(release()));

    connect(ui->btnUd_2UnloadPic,SIGNAL(mouse_release()),this,SLOT(on2Unload()));
    connect(ui->btnUd_2Unload,SIGNAL(mouse_press()),ui->btnUd_2UnloadPic,SLOT(press()));
    connect(ui->btnUd_2Unload,SIGNAL(mouse_release()),ui->btnUd_2UnloadPic,SLOT(release()));
    connect(ui->btnUd_2UnloadIcon,SIGNAL(mouse_press()),ui->btnUd_2UnloadPic,SLOT(press()));
    connect(ui->btnUd_2UnloadIcon,SIGNAL(mouse_release()),ui->btnUd_2UnloadPic,SLOT(release()));

    connect(ui->btnIns_TeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnIns_Teach,SIGNAL(mouse_press()),ui->btnIns_TeachPic,SLOT(press()));
    connect(ui->btnIns_Teach,SIGNAL(mouse_release()),ui->btnIns_TeachPic,SLOT(release()));
    connect(ui->btnIns_TeachIcon,SIGNAL(mouse_press()),ui->btnIns_TeachPic,SLOT(press()));
    connect(ui->btnIns_TeachIcon,SIGNAL(mouse_release()),ui->btnIns_TeachPic,SLOT(release()));

    connect(ui->btnIns_MainPic,SIGNAL(mouse_release()),this,SLOT(onMain()));
    connect(ui->btnIns_Main,SIGNAL(mouse_press()),ui->btnIns_MainPic,SLOT(press()));
    connect(ui->btnIns_Main,SIGNAL(mouse_release()),ui->btnIns_MainPic,SLOT(release()));
    connect(ui->btnIns_MainIcon,SIGNAL(mouse_press()),ui->btnIns_MainPic,SLOT(press()));
    connect(ui->btnIns_MainIcon,SIGNAL(mouse_release()),ui->btnIns_MainPic,SLOT(release()));

    connect(ui->btnIns_SubPic,SIGNAL(mouse_release()),this,SLOT(onSub()));
    connect(ui->btnIns_Sub,SIGNAL(mouse_press()),ui->btnIns_SubPic,SLOT(press()));
    connect(ui->btnIns_Sub,SIGNAL(mouse_release()),ui->btnIns_SubPic,SLOT(release()));
    connect(ui->btnIns_SubIcon,SIGNAL(mouse_press()),ui->btnIns_SubPic,SLOT(press()));
    connect(ui->btnIns_SubIcon,SIGNAL(mouse_release()),ui->btnIns_SubPic,SLOT(release()));

    connect(ui->btnIns2_TeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnIns2_Teach,SIGNAL(mouse_press()),ui->btnIns2_TeachPic,SLOT(press()));
    connect(ui->btnIns2_Teach,SIGNAL(mouse_release()),ui->btnIns2_TeachPic,SLOT(release()));
    connect(ui->btnIns2_TeachIcon,SIGNAL(mouse_press()),ui->btnIns2_TeachPic,SLOT(press()));
    connect(ui->btnIns2_TeachIcon,SIGNAL(mouse_release()),ui->btnIns2_TeachPic,SLOT(release()));


#ifdef _H6_
    ui->lbJ6->setText(tr("J6"));

    ui->lbJ6->setEnabled(true);
    ui->lbJ6val->setEnabled(true);
#else
    ui->lbJ6->setText("");

    ui->lbJ6->setEnabled(false);
    ui->lbJ6val->setEnabled(false);
#endif

    // pointer clear
    dig_array_set = 0;
    dig_insertmain_set = 0;
    dig_unloadseq_set = 0;
    dig_insertsub_set = 0;
    dig_syncback = 0;
    dig_dualunload_mon = 0;
}

form_stepedit_base::~form_stepedit_base()
{
    delete ui;
}
void form_stepedit_base::showEvent(QShowEvent *)
{
    Update();
}
void form_stepedit_base::hideEvent(QHideEvent *){}
void form_stepedit_base::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void form_stepedit_base::onClose()
{
    emit sigClose();
}

void form_stepedit_base::Init(int index, QString title, HyStepData::ENUM_STEP step_type)
{
    m_nSelectedIndex = index;   // step index (now no use)
    m_strTitle = title;
    m_StepType = step_type;
}

// common setting
void form_stepedit_base::Update()
{
    // change button group visible & postion
    this->SetBtnGroup(m_StepType);
    // title update
    ui->lbTitle->setText(m_strTitle);

    Update(m_StepType);
    EnableControlButton(m_StepType);

    // table select view control.
    ui->btnTableSelectPic->hide();
}

// private setting (modify to mode)
void form_stepedit_base::Update(HyStepData::ENUM_STEP step_type)
{
    m_UsePosi.clear();
    gMakeUsePosList(step_type, m_UsePosi);
    m_nSelectedList = 0;
    m_nStartListIndex = 0;
    Redraw_List(m_nStartListIndex);
    Display_Data(m_nSelectedList);
}

void form_stepedit_base::EnableControlButton(HyStepData::ENUM_STEP step_type)
{
    float data;

    switch(step_type)
    {
    case HyStepData::UNLOAD:
        if(Recipe->Get(HyRecipe::mdProdUnload, &data))
        {
            ui->btnUd_ArrPic->setEnabled((data==2.0) || (data==4.0));
            ui->btnUd_Arr->setEnabled((data==2.0) || (data==4.0));
            ui->btnUd_ArrIcon->setEnabled((data==2.0) || (data==4.0));

            ui->btnUd_SeqPic->setEnabled((data==3.0) || (data==4.0));
            ui->btnUd_Seq->setEnabled((data==3.0) || (data==4.0));
            ui->btnUd_SeqIcon->setEnabled((data==3.0) || (data==4.0));
        }

        if(Recipe->Get(HyRecipe::mdDualUnload, &data))
        {
            ui->btnUd_2UnloadPic->setEnabled(data > 0.5);
            ui->btnUd_2Unload->setEnabled(data > 0.5);
            ui->btnUd_2UnloadIcon->setEnabled(data > 0.5);
        }
        break;


    default:
        return;
    }
}

void form_stepedit_base::Redraw_List(int start_index)
{
    int i,index;

    ListOff();

    for(i=0;i<m_vtListItem.count();i++)
    {
        index  = start_index + i;

        if(index < m_UsePosi.size())
        {
            // have data
            m_vtListItem[i]->setText(Posi->GetName(m_UsePosi[index]));
        }
        else
        {
            // don't have data
            m_vtListItem[i]->setText("");
        }

        if(index == m_nSelectedList)
            ListOn(i);
    }

    ui->lbPosNum->setText(QString().setNum(m_UsePosi.size()));
}

void form_stepedit_base::Display_Data(int list_index)
{
    int i = m_UsePosi[list_index];

    cn_trans pos;
    cn_joint joint;
    float speed, delay;

    if(!Posi->RD(i, pos, joint, speed, delay))
        return;

    Display_SetPos(pos, joint);
    Display_Spd(speed);
    Display_Delay(delay);
}

void form_stepedit_base::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    ui->lbXval->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    ui->lbYval->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    ui->lbZval->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    ui->lbRval->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, j));
    ui->lbSval->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, j));
    ui->lbJ6val->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    ui->lbXval->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    ui->lbYval->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    ui->lbZval->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    ui->lbRval->setText(str);
    str.sprintf(FORMAT_POS, j.joint[JSWV]);
    ui->lbSval->setText(str);

    ui->lbJ6val->setText("");
#endif

}

void form_stepedit_base::Display_Spd(float speed)
{
    QString str;
    str.sprintf(FORMAT_SPEED, speed);
    ui->btnSpd->setText(str);
    ui->lbSpdVal->setText(str);
}
void form_stepedit_base::Display_Delay(float delay)
{
    QString str;
    str.sprintf(FORMAT_TIME, delay);
    ui->btnDelay->setText(str);
    ui->lbDelayVal->setText(str);
}

void form_stepedit_base::SetBtnGroup(HyStepData::ENUM_STEP step_type)
{
    for(int i=0;i<m_vtGrp.count();i++)
    {
        if(i == (int)step_type)
        {
            m_vtGrp[i]->show();
            m_vtGrp[i]->setGeometry(mX,mY,mW,mH);
        }
        else
        {
            m_vtGrp[i]->hide();
        }
    }
}

void form_stepedit_base::onList()
{
    QString sel = sender()->objectName();

    for(int i=0;i<m_vtListItem.count();i++)
    {
        if(sel == m_vtListItem[i]->objectName())
        { 
            int index = m_nStartListIndex + i;
            if(index < m_UsePosi.size())
            {
                m_nSelectedList = m_nStartListIndex + i;
                ListOn(i);
                Display_Data(m_nSelectedList);
            }
        }
    }
}

void form_stepedit_base::onListUp()
{
    if(m_nStartListIndex <= 0)
        return;

    Redraw_List(--m_nStartListIndex);
}
void form_stepedit_base::onListDown()
{
    if(m_nStartListIndex >= (m_UsePosi.size() - m_vtListItem.size()))
        return;

    Redraw_List(++m_nStartListIndex);
}

void form_stepedit_base::ListOn(int i)
{
    ListOff();

    QPalette* pal = new QPalette();
    pal->setBrush(QPalette::Window,QBrush(QColor(255,255,0)));

    m_vtListItem[i]->setPalette(*pal);
    m_vtListItem[i]->setAutoFillBackground(true);

}
void form_stepedit_base::ListOff()
{
    for(int i=0;i<m_vtListItem.count();i++)
        m_vtListItem[i]->setAutoFillBackground(false);
}

void form_stepedit_base::onTeach()
{
    dialog_position_teaching2* dig = (dialog_position_teaching2*)gGetDialog(DIG_POS_TEACH);

    dig->Init(m_UsePosi[m_nSelectedList]);

    if(dig->exec() == QDialog::Accepted)
    {
        Display_Data(m_nSelectedList);
    }
}

void form_stepedit_base::onMethod()
{
    dialog_takeout_method* dig = new dialog_takeout_method();
    dig->exec();
}

void form_stepedit_base::onJMotion()
{
    dialog_jmotion* dig = new dialog_jmotion();
    dig->exec();
}
void form_stepedit_base::onTemp()
{
    dialog_temperature* dig = new dialog_temperature();
    dig->exec();
}

void form_stepedit_base::onArray()
{
    if(dig_array_set == 0)
        dig_array_set = new dialog_array_set();

    dig_array_set->exec();

}
void form_stepedit_base::onSeqeuntial()
{
    if(dig_unloadseq_set == 0)
        dig_unloadseq_set = new dialog_unloadseq_set();

    dig_unloadseq_set->exec();
}
void form_stepedit_base::onWeight()
{
    dialog_weight* dig = new dialog_weight();
    dig->exec();
}

void form_stepedit_base::onMain()
{
    if(dig_insertmain_set == 0)
        dig_insertmain_set = new dialog_insertmain_set();

    dig_insertmain_set->exec();
}
void form_stepedit_base::onSub()
{
    if(dig_insertsub_set == 0)
        dig_insertsub_set = new dialog_insertsub_set();

    dig_insertsub_set->exec();
}
void form_stepedit_base::onSyncBack()
{
    if(dig_syncback == 0)
        dig_syncback = new dialog_syncback();

    dig_syncback->exec();
}
void form_stepedit_base::on2Unload()
{
    if(dig_dualunload_mon == 0)
        dig_dualunload_mon = new dialog_dualunload_mon();

    dig_dualunload_mon->exec();
}
