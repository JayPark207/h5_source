#include "dialog_useskip.h"
#include "ui_dialog_useskip.h"

dialog_useskip::dialog_useskip(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_useskip)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onEnd()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // table
    m_vtNo.clear();            m_vtName.clear();              m_vtUse.clear();
    m_vtNo.append(ui->tbNo);   m_vtName.append(ui->tbName);   m_vtUse.append(ui->tbUse);
    m_vtNo.append(ui->tbNo_2); m_vtName.append(ui->tbName_2); m_vtUse.append(ui->tbUse_2);
    m_vtNo.append(ui->tbNo_3); m_vtName.append(ui->tbName_3); m_vtUse.append(ui->tbUse_3);
    m_vtNo.append(ui->tbNo_4); m_vtName.append(ui->tbName_4); m_vtUse.append(ui->tbUse_4);
    m_vtNo.append(ui->tbNo_5); m_vtName.append(ui->tbName_5); m_vtUse.append(ui->tbUse_5);
    m_vtNo.append(ui->tbNo_6); m_vtName.append(ui->tbName_6); m_vtUse.append(ui->tbUse_6);
    m_vtNo.append(ui->tbNo_7); m_vtName.append(ui->tbName_7); m_vtUse.append(ui->tbUse_7);
    m_vtNo.append(ui->tbNo_8); m_vtName.append(ui->tbName_8); m_vtUse.append(ui->tbUse_8);
    m_vtNo.append(ui->tbNo_9); m_vtName.append(ui->tbName_9); m_vtUse.append(ui->tbUse_9);
    m_vtNo.append(ui->tbNo_10);m_vtName.append(ui->tbName_10);m_vtUse.append(ui->tbUse_10);
    m_vtNo.append(ui->tbNo_11);m_vtName.append(ui->tbName_11);m_vtUse.append(ui->tbUse_11);
    m_vtNo.append(ui->tbNo_12);m_vtName.append(ui->tbName_12);m_vtUse.append(ui->tbUse_12);
    m_vtNo.append(ui->tbNo_13);m_vtName.append(ui->tbName_13);m_vtUse.append(ui->tbUse_13);
    m_vtNo.append(ui->tbNo_14);m_vtName.append(ui->tbName_14);m_vtUse.append(ui->tbUse_14);
    m_vtNo.append(ui->tbNo_15);m_vtName.append(ui->tbName_15);m_vtUse.append(ui->tbUse_15);
    m_vtNo.append(ui->tbNo_16);m_vtName.append(ui->tbName_16);m_vtUse.append(ui->tbUse_16);

    int i;
    for(i=0;i<m_vtNo.count();i++)
    {
        connect(m_vtNo[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtName[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
    }


    // checkbox
    connect(ui->lbBox,SIGNAL(mouse_release()),this,SLOT(onCheckbox()));
    connect(ui->lbCheck,SIGNAL(mouse_release()),ui->lbBox,SIGNAL(mouse_release()));

    // Change
    connect(ui->btnChangePic,SIGNAL(mouse_release()),this,SLOT(onChange()));
    connect(ui->btnChangeIcon,SIGNAL(mouse_press()),ui->btnChangePic,SLOT(press()));
    connect(ui->btnChangeIcon,SIGNAL(mouse_release()),ui->btnChangePic,SLOT(release()));


}
dialog_useskip::~dialog_useskip()
{
    delete ui;
}
void dialog_useskip::showEvent(QShowEvent *)
{
    Update();
}
void dialog_useskip::hideEvent(QHideEvent *)
{

}
void dialog_useskip::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_useskip::onEnd()
{
    emit accept();
}

void dialog_useskip::onSelect()
{
    QLabel3* sel = (QLabel3*)sender();

    if(ui->lbCheck->isHidden()) return;

    int index = m_vtNo.indexOf(sel);
    if(index < 0) return;

    Display_Select(index);

}
void dialog_useskip::onCheckbox()
{
    if(ui->lbCheck->isHidden())
    {
        ui->lbCheck->show();
        Display_Select(0);
        Display_Checkbox(true);
    }
    else
    {
        ui->lbCheck->hide();
        Display_Select(-1);
        Display_Checkbox(false);
    }
}
void dialog_useskip::onChange()
{
    if(ui->lbCheck->isHidden()) return;
    HyUseSkip::ENUM_USESKIP index = (HyUseSkip::ENUM_USESKIP)m_nSelectedIndex;
    bool bNow = UseSkip->Get(index);

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(QString(tr("CONFIRM")));
    QString str;
    if(bNow)
        str = tr("USE") + " >> " + tr("SKIP");
    else
        str = tr("SKIP") + " >> " + tr("USE");

    conf->Message(UseSkip->GetName(index), str);
    if(conf->exec() == QDialog::Accepted)
    {
        UseSkip->Set(index, !bNow);
        if(!UseSkip->Write())
        {
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Use/Skip Data Write Error.")),
                         QString(tr("Plase try again!!")));
            msg->exec();
            UseSkip->Set(index, bNow);
            return;
        }

        Display_Data();
    }
}

void dialog_useskip::Update()
{
    if(UseSkip->Read())
    {
        Display_Name();
        Display_Data();
    }

    Display_Select(-1);
    Display_Checkbox(false);

}

void dialog_useskip::Display_Name()
{
    for(int i=0;i<m_vtName.count();i++)
    {
        m_vtNo[i]->setText(QString("%1").arg(i+1));
        m_vtName[i]->setText(UseSkip->GetName((HyUseSkip::ENUM_USESKIP)i));
    }
}
void dialog_useskip::Display_Data()
{
    for(int i=0;i<m_vtUse.count();i++)
    {
        if(UseSkip->Get((HyUseSkip::ENUM_USESKIP)i))
        {
            m_vtUse[i]->setText(tr("USE"));
            m_vtUse[i]->setEnabled(true);
        }
        else
        {
            m_vtUse[i]->setText(tr("SKIP"));
            m_vtUse[i]->setEnabled(false);
        }
    }
}
void dialog_useskip::Display_Select(int index)
{
    m_nSelectedIndex = index;
    for(int i=0;i<m_vtNo.count();i++)
    {
        m_vtNo[i]->setAutoFillBackground(i==index);
        m_vtName[i]->setAutoFillBackground(i==index);
    }
}
void dialog_useskip::Display_Checkbox(bool bCheck)
{
    if(bCheck)
        ui->lbCheck->show();
    else
        ui->lbCheck->hide();

    ui->grpChange->setEnabled(bCheck);
}
