#include "hysoftsensor.h"

HySoftSensor::HySoftSensor(HyParam* param, HyRecipe* recipe)
{
    m_Param = param;
    m_Recipe = recipe;
}

bool HySoftSensor::Read()
{
    QString str;
    m_Min.clear();
    m_Max.clear();

    str = m_Param->Get(HyParam::SOFT_SENSOR_MIN);
    m_Min = str.split(",");
    str = m_Param->Get(HyParam::SOFT_SENSOR_MAX);
    m_Max = str.split(",");

    if(m_Min.size() != m_Max.size() || m_Min.isEmpty() || m_Max.isEmpty())
    {
        qDebug() << "HySoftSensor Read() Wrong Data Exception!";
        m_Min.clear();
        m_Max.clear();
        for(int i=0;i<SSEN_NUM;i++)
        {
            m_Min.append("0");
            m_Max.append("0");
        }
    }

    return true;
}

bool HySoftSensor::Write(bool save)
{
    if(m_Min.size() != m_Max.size() || m_Min.isEmpty() || m_Max.isEmpty())
    {
        qDebug() << "HySoftSensor Write() Wrong Data Exception!";
        return false;
    }

    QString min = m_Min.join(",");
    QString max = m_Max.join(",");
    m_Param->Set(HyParam::SOFT_SENSOR_MIN, min);
    m_Param->Set(HyParam::SOFT_SENSOR_MAX, max);

    QVector<float> min_datas, max_datas;
    min_datas.clear();max_datas.clear();

    for(int i=0;i<m_Min.count();i++)
    {
        min_datas.append(m_Min[i].toFloat());
        max_datas.append(m_Max[i].toFloat());
    }

    if(!m_Recipe->SetsArr("softsen_min", min_datas, save))
        return false;
    if(!m_Recipe->SetsArr("softsen_max", max_datas, save))
        return false;

    return true;
}

bool HySoftSensor::Sync(bool save)
{
    if(!Read())
        return false;
    if(!Write(save))
        return false;

    return true;
}

