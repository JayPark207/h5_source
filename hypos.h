#ifndef HYPOS
#define HYPOS

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyrecipe.h"
#include "hystepdata.h"

/** position & speed data array
 * number & buffer class. **/
class HyPos : public QObject
{
    Q_OBJECT

public:
    enum ENUM_POS_ARRAY
    {
        ZERO = 0,
        HOME,
        JIG_CHANGE,
        SAMPLE,
        FAULTY,         //unload

        WAIT,           //wait
        EXT_WAIT,       //wait
        TAKE_OUT,       //takeout
        UNDERCUT1,      //takeout
        UNDERCUTN = UNDERCUT1 + (MAX_UNDERCUT_POS-1), // takeout
        UP1ST=24,       //up
        UNLOAD,         //unload
        WEIGHT,         //unload
        INSERT,         //insert
        INSERT_UNLOAD,
        STOPOVER1,
        STOPOVER2,
        UNLOAD_MID,     //31
        UNLOAD2,        //32
        CLOSE_WAIT,     //33
        INSERT_MID,     //34
        STOPOVER3,
        // add here...


        POS_MAX_NUM
    };

public:
    explicit HyPos(HyRecipe* recipe);
    ~HyPos(){}

    void Init();    // name write.

    void AllZero(); // for dev (make 0 datas.)

    // common
    QString GetName(int i);

    // in-direct acces case.
    void        Set_Trans(int i, cn_trans data);
    cn_trans    Get_Trans(int i);
    void        Set_Joint(int i, cn_joint data);
    cn_joint    Get_Joint(int i);
    void        Set_Speed(int i, float percent);
    float       Get_Speed(int i);
    void        Set_Delay(int i, float time);
    float       Get_Delay(int i);

    bool Read();
    bool Read(int i);
    bool Write();
    bool Write(int i);

    // direct access case
    bool WR(int index, cn_trans trans, cn_joint joint, float speed, float delay);
    bool WR_nvs(int index, cn_trans trans, cn_joint joint, float speed, float delay);
    bool WR(int index, cn_trans trans, cn_joint joint);
    bool WR_Pos(int i, cn_trans trans);
    bool WR_Pos(int i, cn_joint joint);
    bool WR_Speed(int i, float speed);
    bool WR_Delay(int i, float delay);

    bool RD(int index, cn_trans& trans, cn_joint& joint, float& speed, float& delay);
    bool RD(int index, cn_trans& trans, cn_joint& joint);
    bool RD_Pos(int i, cn_trans* trans);
    bool RD_Pos(int i, cn_joint* joint);
    bool RD_Speed(int i, float* speed);
    bool RD_Delay(int i, float* delay);

    bool Trans2Joint(cn_trans in_trans, cn_joint old_joint, cn_joint& new_joint);
    bool Joint2Trans(cn_joint in_joint, cn_trans& new_trans);

    QString m_strArrName_TPos;
    QString m_strArrName_JPos;
    QString m_strArrName_Speed;
    QString m_strArrName_Delay;

    bool Set_UndercutSpeed(int start_num, int total_num, float speed, bool save = false); // num (1 ~ MAX_UNDERCUT_POS)

private:
    HyRecipe* m_Recipe;

    QVector<cn_trans>  Trans;
    QVector<cn_joint>  Joint;
    QVector<float>     Speed;      // [%] 0~100
    QVector<float>     Delay;
    QVector<QString>   Name;



};



#endif // HYPOS

