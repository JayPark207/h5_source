#ifndef DIALOG_ZEROING_H
#define DIALOG_ZEROING_H

#include <QDialog>
#include <QTimer>
#include "global.h"

#include "qlabel3.h"

#include "widget_jogmodule3.h"
#include "top_sub.h"
#include "dialog_delaying.h"
#include "dialog_confirm.h"

namespace Ui {
class dialog_zeroing;
}

class dialog_zeroing : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_zeroing(QWidget *parent = 0);
    ~dialog_zeroing();

public slots:
    void onClose();

    void onTimer();
    void onZero();
    void onServo();
    void onReset();
    void onJogChange();

    void onEachZero();
    void onMultiTurnClear();

private:
    Ui::dialog_zeroing *ui;
    QTimer* timer;
    widget_jogmodule3* wmJog;

    QVector<QLabel3*> m_vtTrans;
    QVector<QLabel3*> m_vtJoint;

    void SetServo(bool onoff);
    QVector<QPixmap> pxmServo;

    void Save();
    bool m_bZeroing;

    top_sub* top;
    void SubTop();          // Need SubTop

    void Display_CurPos(float* t, float* j);

    float*  m_TransData;
    float*  m_JointData;

    bool m_bJnT; //J=false(defalut), T=true;
    void SetJog(bool bJnT);

    QVector<QLabel3*> m_vtJ;
    QVector<QLabel4*> m_vtJPic;
    QVector<QLabel4*> m_vtJvalPic;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ZEROING_H
