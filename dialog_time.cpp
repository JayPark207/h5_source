#include "dialog_time.h"
#include "ui_dialog_time.h"

dialog_time::dialog_time(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_time)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onPageUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onPageDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

    m_vtPic.clear();
    m_vtPic.append(ui->tbTimePic);
    m_vtPic.append(ui->tbTimePic_2);
    m_vtPic.append(ui->tbTimePic_3);
    m_vtPic.append(ui->tbTimePic_4);
    m_vtPic.append(ui->tbTimePic_5);
    m_vtPic.append(ui->tbTimePic_6);
    m_vtPic.append(ui->tbTimePic_7);

    m_vtName.clear();
    m_vtName.append(ui->tbName);
    m_vtName.append(ui->tbName_2);
    m_vtName.append(ui->tbName_3);
    m_vtName.append(ui->tbName_4);
    m_vtName.append(ui->tbName_5);
    m_vtName.append(ui->tbName_6);
    m_vtName.append(ui->tbName_7);

    m_vtVal.clear();
    m_vtVal.append(ui->tbTime);
    m_vtVal.append(ui->tbTime_2);
    m_vtVal.append(ui->tbTime_3);
    m_vtVal.append(ui->tbTime_4);
    m_vtVal.append(ui->tbTime_5);
    m_vtVal.append(ui->tbTime_6);
    m_vtVal.append(ui->tbTime_7);

    m_vtNo.clear();
    m_vtNo.append(ui->tbNo);
    m_vtNo.append(ui->tbNo_2);
    m_vtNo.append(ui->tbNo_3);
    m_vtNo.append(ui->tbNo_4);
    m_vtNo.append(ui->tbNo_5);
    m_vtNo.append(ui->tbNo_6);
    m_vtNo.append(ui->tbNo_7);

    int i=0;
    for(i=0;i<m_vtPic.count();i++)
    {
        connect(m_vtPic[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtVal[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtVal[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
    }

    top = 0;
}

dialog_time::~dialog_time()
{
    delete ui;
}

void dialog_time::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_time::showEvent(QShowEvent *)
{
    m_nNowPage = 0;

    int remain = Time->GetNum() % m_vtName.size();
    if(remain > 0)
        m_nMaxPage = (int)(Time->GetNum() / m_vtName.size()) + 1;
    else
        m_nMaxPage = (int)(Time->GetNum() / m_vtName.size());

    Display_PageNum();
    Update(m_nNowPage);

    SubTop();
}

void dialog_time::hideEvent(QHideEvent *){}

void dialog_time::onClose()
{
    emit accept();
}

void dialog_time::Update(int page)
{
    int num=0;
    // last page check
    if((page + 1) >= m_nMaxPage)
    {
        num = Time->GetNum() % m_vtName.size();
        if(num <= 0)
            num = m_vtName.size();
    }
    else
    {
        num = m_vtName.size();
    }

    // all read time data.
    Time->Read();

    int id=0;
    QString str;
    for(int i=0;i<m_vtName.size();i++)
    {
        id = (page*m_vtName.size()) + i;

        if(i < num)
        {
            m_vtNo[i]->setText(QString().setNum(id+1));

            m_vtName[i]->setText(Time->GetName(id));

            str.sprintf(FORMAT_TIME, Time->Get(id));
            m_vtVal[i]->setText(str);

            m_vtName[i]->show();
            m_vtPic[i]->show();
            m_vtVal[i]->show();

            m_vtName[i]->setEnabled(true);
            m_vtPic[i]->setEnabled(true);
            m_vtVal[i]->setEnabled(true);
            m_vtNo[i]->setEnabled(true);
        }
        else
        {
            m_vtName[i]->setText("");
            m_vtVal[i]->setText("");
            m_vtNo[i]->setText("");

            m_vtName[i]->setEnabled(false);
            m_vtPic[i]->setEnabled(false);
            m_vtVal[i]->setEnabled(false);
            m_vtNo[i]->setEnabled(false);
        }

    }

}

void dialog_time::Display_PageNum()
{
    QString str;
    str.sprintf("%d/%d", (m_nNowPage+1), m_nMaxPage);
    ui->lbPage->setText(str);
}



void dialog_time::onPageUp()
{
    if(m_nNowPage <= 0)
        return;

    m_nNowPage--;
    Update(m_nNowPage);
    Display_PageNum();

}
void dialog_time::onPageDown()
{
    if(m_nNowPage >= m_nMaxPage-1)
        return;

    m_nNowPage++;
    Update(m_nNowPage);
    Display_PageNum();

}
void dialog_time::onSelect()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    int list_index = (m_nNowPage*m_vtName.size()) + index;

    np->m_numpad->SetTitle(m_vtName[index]->text());
    np->m_numpad->SetNum(m_vtVal[index]->text().toDouble());
    np->m_numpad->SetMinValue(MIN_TIME_SET);
    np->m_numpad->SetMaxValue(MAX_TIME_SET);
    np->m_numpad->SetSosuNum(SOSU_TIME);
    if(np->exec() == QDialog::Accepted)
    {
        if(Time->WR(list_index, (float)np->m_numpad->GetNumDouble()))
            Update(m_nNowPage);
    }

}

void dialog_time::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
