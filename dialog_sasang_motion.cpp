#include "dialog_sasang_motion.h"
#include "ui_dialog_sasang_motion.h"

dialog_sasang_motion::dialog_sasang_motion(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_sasang_motion)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(500);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // init page
    Page = new QStackedWidget(this);
    Page->setGeometry(ui->wigEdit->x(), ui->wigEdit->y(),
                      ui->wigEdit->width(), ui->wigEdit->height());
    Page->addWidget(ui->wigEdit);
    page_teach = new dialog_sasang_motion_teach(this);
    Page->addWidget(page_teach);
    page_test = new dialog_sasang_motion_test(this);
    Page->addWidget(page_test);
    Page->setCurrentIndex(0);

    // init Panel
    Panel = new QStackedWidget(ui->wigEdit);
    Panel->setGeometry(ui->wigPanel->x(),ui->wigPanel->y()
                      ,ui->wigPanel->width(),ui->wigPanel->height());
    Panel->addWidget(ui->wigPanel);
    Panel->addWidget(ui->wigPanel_2);
    Panel->setCurrentIndex(0);

    // init SubPanel
    SubPanel = new QStackedWidget(ui->wigEdit);
    SubPanel->setGeometry(ui->wigSubPanel->x(),ui->wigSubPanel->y()
                         ,ui->wigSubPanel->width(),ui->wigSubPanel->height());
    SubPanel->addWidget(ui->wigSubPanel);
    SubPanel->addWidget(ui->wigSubPanel_2);
    SubPanel->setCurrentIndex(0);



    connect(page_teach,SIGNAL(sigClose()),this,SLOT(onClose_Page()));
    connect(page_test,SIGNAL(sigClose()),this,SLOT(onClose_Page()));
    connect(page_test,SIGNAL(sigEdit()),this,SLOT(onEditing()));

    connect(ui->btnTeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnTeach,SIGNAL(mouse_press()),ui->btnTeachPic,SLOT(press()));
    connect(ui->btnTeach,SIGNAL(mouse_release()),ui->btnTeachPic,SLOT(release()));
    connect(ui->btnTeachIcon,SIGNAL(mouse_press()),ui->btnTeachPic,SLOT(press()));
    connect(ui->btnTeachIcon,SIGNAL(mouse_release()),ui->btnTeachPic,SLOT(release()));

    connect(ui->btnTestPic,SIGNAL(mouse_release()),this,SLOT(onTest()));
    connect(ui->btnTest,SIGNAL(mouse_press()),ui->btnTestPic,SLOT(press()));
    connect(ui->btnTest,SIGNAL(mouse_release()),ui->btnTestPic,SLOT(release()));
    connect(ui->btnTestIcon,SIGNAL(mouse_press()),ui->btnTestPic,SLOT(press()));
    connect(ui->btnTestIcon,SIGNAL(mouse_release()),ui->btnTestPic,SLOT(release()));

    m_vtTbNo.clear();m_vtTbMot.clear();
    m_vtTbNo.append(ui->tbNo);  m_vtTbMot.append(ui->tbMot);
    m_vtTbNo.append(ui->tbNo_2);m_vtTbMot.append(ui->tbMot_2);
    m_vtTbNo.append(ui->tbNo_3);m_vtTbMot.append(ui->tbMot_3);
    m_vtTbNo.append(ui->tbNo_4);m_vtTbMot.append(ui->tbMot_4);
    m_vtTbNo.append(ui->tbNo_5);m_vtTbMot.append(ui->tbMot_5);
    m_vtTbNo.append(ui->tbNo_6);m_vtTbMot.append(ui->tbMot_6);
    m_vtTbNo.append(ui->tbNo_7);m_vtTbMot.append(ui->tbMot_7);
    m_vtTbNo.append(ui->tbNo_8);m_vtTbMot.append(ui->tbMot_8);

    int i;
    for(i=0;i<m_vtTbNo.count();i++)
    {
        connect(m_vtTbNo[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtTbMot[i],SIGNAL(mouse_release()),m_vtTbNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

    connect(ui->btnListUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_press()),ui->btnListUpPic,SLOT(press()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_release()),ui->btnListUpPic,SLOT(release()));

    connect(ui->btnListDownPic,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnListDownIcon,SIGNAL(mouse_press()),ui->btnListDownPic,SLOT(press()));
    connect(ui->btnListDownIcon,SIGNAL(mouse_release()),ui->btnListDownPic,SLOT(release()));

    connect(ui->btnListDelPic,SIGNAL(mouse_release()),this,SLOT(onListDel()));
    connect(ui->btnListDelIcon,SIGNAL(mouse_press()),ui->btnListDelPic,SLOT(press()));
    connect(ui->btnListDelIcon,SIGNAL(mouse_release()),ui->btnListDelPic,SLOT(release()));

    connect(ui->btnSavePic,SIGNAL(mouse_release()),this,SLOT(onSave()));
    connect(ui->btnSaveIcon,SIGNAL(mouse_press()),ui->btnSavePic,SLOT(press()));
    connect(ui->btnSaveIcon,SIGNAL(mouse_release()),ui->btnSavePic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));


    // Panel Table
    m_vtAxis.clear();
    m_vtAxis.append(ui->tbAxis);
    m_vtAxis.append(ui->tbAxis_2);
    m_vtAxis.append(ui->tbAxis_3);
    m_vtAxis.append(ui->tbAxis_4);
    m_vtAxis.append(ui->tbAxis_5);
    m_vtAxis.append(ui->tbAxis_6);

    m_vtValPic.clear();m_vtVal.clear();
    m_vtValPic.append(ui->tbValPic);  m_vtVal.append(ui->tbVal);
    m_vtValPic.append(ui->tbValPic_2);m_vtVal.append(ui->tbVal_2);
    m_vtValPic.append(ui->tbValPic_3);m_vtVal.append(ui->tbVal_3);
    m_vtValPic.append(ui->tbValPic_4);m_vtVal.append(ui->tbVal_4);
    m_vtValPic.append(ui->tbValPic_5);m_vtVal.append(ui->tbVal_5);
    m_vtValPic.append(ui->tbValPic_6);m_vtVal.append(ui->tbVal_6);
    for(i=0;i<m_vtValPic.count();i++)
    {
        connect(m_vtValPic[i],SIGNAL(mouse_release()),this,SLOT(onPos()));
        connect(m_vtVal[i],SIGNAL(mouse_press()),m_vtValPic[i],SLOT(press()));
        connect(m_vtVal[i],SIGNAL(mouse_release()),m_vtValPic[i],SLOT(release()));
    }

    m_vtVal2Pic.clear();m_vtVal2.clear();
    m_vtVal2Pic.append(ui->tbVal2Pic);  m_vtVal2.append(ui->tbVal2);
    m_vtVal2Pic.append(ui->tbVal2Pic_2);m_vtVal2.append(ui->tbVal2_2);
    m_vtVal2Pic.append(ui->tbVal2Pic_3);m_vtVal2.append(ui->tbVal2_3);
    m_vtVal2Pic.append(ui->tbVal2Pic_4);m_vtVal2.append(ui->tbVal2_4);
    m_vtVal2Pic.append(ui->tbVal2Pic_5);m_vtVal2.append(ui->tbVal2_5);
    m_vtVal2Pic.append(ui->tbVal2Pic_6);m_vtVal2.append(ui->tbVal2_6);
    for(i=0;i<m_vtVal2Pic.count();i++)
    {
        connect(m_vtVal2Pic[i],SIGNAL(mouse_release()),this,SLOT(onPos2()));
        connect(m_vtVal2[i],SIGNAL(mouse_press()),m_vtVal2Pic[i],SLOT(press()));
        connect(m_vtVal2[i],SIGNAL(mouse_release()),m_vtVal2Pic[i],SLOT(release()));
    }

    m_vtType.clear();m_vtTypeIcon.clear();
    m_vtType.append(ui->tbTypeOffPic);  m_vtTypeIcon.append(ui->tbTypeOffIcon);
    m_vtType.append(ui->tbTypeOnPic);   m_vtTypeIcon.append(ui->tbTypeOnIcon);
    m_vtType.append(ui->tbTypePulsePic);m_vtTypeIcon.append(ui->tbTypePulseIcon);
    for(i=0;i<m_vtType.count();i++)
    {
        connect(m_vtType[i],SIGNAL(mouse_release()),this,SLOT(onType()));
        connect(m_vtTypeIcon[i],SIGNAL(mouse_press()),m_vtType[i],SLOT(press()));
        connect(m_vtTypeIcon[i],SIGNAL(mouse_release()),m_vtType[i],SLOT(release()));
    }

    connect(ui->tbSpdPic,SIGNAL(mouse_release()),this,SLOT(onSpeed()));
    connect(ui->tbSpd,SIGNAL(mouse_press()),ui->tbSpdPic,SLOT(press()));
    connect(ui->tbSpd,SIGNAL(mouse_release()),ui->tbSpdPic,SLOT(release()));

    connect(ui->tbAccuPic,SIGNAL(mouse_release()),this,SLOT(onAccuracy()));
    connect(ui->tbAccu,SIGNAL(mouse_press()),ui->tbAccuPic,SLOT(press()));
    connect(ui->tbAccu,SIGNAL(mouse_release()),ui->tbAccuPic,SLOT(release()));

    connect(ui->tbTimePic,SIGNAL(mouse_release()),this,SLOT(onTime()));
    connect(ui->tbTime,SIGNAL(mouse_press()),ui->tbTimePic,SLOT(press()));
    connect(ui->tbTime,SIGNAL(mouse_release()),ui->tbTimePic,SLOT(release()));

    MatchingOutput();
    for(i=0;i<m_vtOutPic.count();i++)
    {
        connect(m_vtOutPic[i],SIGNAL(mouse_release()),this,SLOT(onOutput()));
        connect(m_vtOutIcon[i],SIGNAL(mouse_press()),m_vtOutPic[i],SLOT(press()));
        connect(m_vtOutIcon[i],SIGNAL(mouse_release()),m_vtOutPic[i],SLOT(release()));
        connect(m_vtOutSel[i],SIGNAL(mouse_press()),m_vtOutPic[i],SLOT(press()));
        connect(m_vtOutSel[i],SIGNAL(mouse_release()),m_vtOutPic[i],SLOT(release()));
    }

    connect(ui->btnListEditPic,SIGNAL(mouse_release()),this,SLOT(onListEdit()));
    connect(ui->btnListEditIcon,SIGNAL(mouse_press()),ui->btnListEditPic,SLOT(press()));
    connect(ui->btnListEditIcon,SIGNAL(mouse_release()),ui->btnListEditPic,SLOT(release()));

    connect(ui->btnShiftPic,SIGNAL(mouse_release()),this,SLOT(onShift()));
    connect(ui->btnShiftIcon,SIGNAL(mouse_press()),ui->btnShiftPic,SLOT(press()));
    connect(ui->btnShiftIcon,SIGNAL(mouse_release()),ui->btnShiftPic,SLOT(release()));

    connect(ui->btnEditBackPic,SIGNAL(mouse_release()),this,SLOT(onEditBack()));
    connect(ui->btnEditBackIcon,SIGNAL(mouse_press()),ui->btnEditBackPic,SLOT(press()));
    connect(ui->btnEditBackIcon,SIGNAL(mouse_release()),ui->btnEditBackPic,SLOT(release()));

    //ui->wigEditBack->setGeometry(695, 345, ui->wigEditBack->width(), ui->wigEditBack->height());
    ui->wigEditBack->setGeometry(ui->wigEditBack->x(), ui->wigEditBack->y(),
                                 ui->wigEditBack->width(), ui->wigEditBack->height());


#ifdef _H6_
    ui->tbAxis_6->setText(tr("J6"));
    ui->tbAxis_6->setEnabled(true);

    ui->tbVal_6->setEnabled(false);
    ui->tbValPic_6->setEnabled(false);
    ui->tbVal2_6->setEnabled(true);
    ui->tbVal2Pic_6->setEnabled(false);

    ui->tbVal_4->setEnabled(false);
    ui->tbValPic_4->setEnabled(false);
    ui->tbVal2_4->setEnabled(true);
    ui->tbVal2Pic_4->setEnabled(false);

    ui->tbVal_5->setEnabled(false);
    ui->tbValPic_5->setEnabled(false);
    ui->tbVal2_5->setEnabled(true);
    ui->tbVal2Pic_5->setEnabled(false);
#else
    ui->tbAxis_6->setText("");
    ui->tbAxis_6->setEnabled(false);

    ui->tbVal_6->setEnabled(false);
    ui->tbValPic_6->setEnabled(false);
    ui->tbVal2_6->setEnabled(false);
    ui->tbVal2Pic_6->setEnabled(false);

    ui->tbVal_4->setEnabled(true);
    ui->tbValPic_4->setEnabled(true);
    ui->tbVal2_4->setEnabled(true);
    ui->tbVal2Pic_4->setEnabled(true);

    ui->tbVal_5->setEnabled(true);
    ui->tbValPic_5->setEnabled(true);
    ui->tbVal2_5->setEnabled(true);
    ui->tbVal2Pic_5->setEnabled(true);
#endif


    // initial
    pxmListEditIcon.clear();
    dig_shift = 0;
    m_bEditing = false;
    m_GrpName.clear();
    m_PatName.clear();
}

dialog_sasang_motion::~dialog_sasang_motion()
{
    delete ui;
}

void dialog_sasang_motion::showEvent(QShowEvent *)
{
    //Test_Make_List(); // for test
    //Test_Make_RawProg(); // for test

    Update();

    timer->start();
}
void dialog_sasang_motion::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_sasang_motion::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_sasang_motion::onClose()
{
    if(ui->wigSave->isEnabled())
    {
        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::GREEN);
        conf->Title(tr("Confirm"));
        conf->Message(QString(tr("Would you want to exit?")), tr("Changed data will not be saved."));
        if(conf->exec() != QDialog::Accepted)
            return;
    }

    emit accept();
}

void dialog_sasang_motion::onClose_Page()
{
    Page->setCurrentIndex(0);
    Redraw_List(m_nStartIndex);
}

void dialog_sasang_motion::onEditing()
{
    Page->setCurrentIndex(0);
    Redraw_List(m_nStartIndex);
    m_bEditing = true;

    Control_ListEdit(false);
}

void dialog_sasang_motion::Display_Editing(bool bEditing)
{
    ui->wigTest->setVisible(!bEditing);
    ui->wigEditBack->setVisible(bEditing);
    ui->grpEnd->setVisible(!bEditing);
    ui->wigListEditControl->setEnabled(!bEditing);
}

void dialog_sasang_motion::onEditBack()
{
    m_bEditing = false;
    Page->setCurrentIndex(2);

    //gReadyMacro(true);
}

void dialog_sasang_motion::onTimer()
{
    Display_SaveEnable();
    Display_Editing(m_bEditing);
}


void dialog_sasang_motion::Init(QString group_name, QString pattern_name)
{
    m_GrpName = group_name;
    m_PatName = pattern_name;

    ui->lbGrp->setText(m_GrpName);
    ui->lbPat->setText(m_PatName);
}

void dialog_sasang_motion::Update()
{
    m_nStartIndex=0;m_nSelectedIndex=0;
    Redraw_List(m_nStartIndex);

    Control_ListEdit(false);
}

void dialog_sasang_motion::Redraw_List(int start_index)
{
    int cal_size = Sasang->Motion->MotionList.size() - start_index;
    int index;
    QString no,mot;

    Display_ListOn(-1); // all off

    for(int i=0;i<m_vtTbNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            mot = Sasang->Motion->GetName(index);
            no = QString().setNum(index + 1);
        }
        else
        {
            // no data.
            mot.clear();
            no.clear();
        }

        m_vtTbNo[i]->setText(no);
        m_vtTbMot[i]->setText(mot);

        if((start_index + i) == m_nSelectedIndex)
            Display_ListOn(i);
    }

    Display_Data(m_nSelectedIndex);

    // display list size.
    ui->lbListSize->setText(QString().setNum(Sasang->Motion->GetCount()));
}

void dialog_sasang_motion::Display_ListOn(int table_index)
{
    for(int i=0;i<m_vtTbNo.count();i++)
    {
        m_vtTbNo[i]->setAutoFillBackground(i==table_index);
        m_vtTbMot[i]->setAutoFillBackground(i==table_index);
    }
}

void dialog_sasang_motion::Display_Data(int list_index)
{
    if(list_index < 0
    || list_index >= Sasang->Motion->MotionList.size())
    {
        // no have data.
        Panel->hide();
        SubPanel->hide();
        return;
    }

    //0. data save
    m_DataOld = Sasang->Motion->MotionList[list_index];
    m_DataNew = m_DataOld;

    //1. Change Panel & SubPanel / ...
    int command = Sasang->Motion->IndexOf(m_DataNew.command);
    if(command < 0)
    {
        // no have command
        Panel->hide();
        SubPanel->hide();
        return;
    }

    int panel_index=0, subpanel_index=0;
    bool bPos=false,bPos2=false,bSpd=false,bAccu=false,bOut=false,bType=false;

    switch(command)
    {
    case HySasang_Motion::p2p:
        panel_index = 0;
        subpanel_index = 0;
        bPos=true;
        bSpd=true;
        break;
    case HySasang_Motion::con:
        panel_index = 0;
        subpanel_index = 0;
        bPos=true;
        bSpd=true;
        bAccu=true;
        break;
    case HySasang_Motion::cir:
        panel_index = 0;
        subpanel_index = 0;
        bPos=true;bPos2=true;
        bSpd=true;
        break;
    case HySasang_Motion::out:
        panel_index = 1;
        subpanel_index = 1;
        bOut=true;
        bType=true;
        break;
    case HySasang_Motion::wtt:
        panel_index = 1;
        subpanel_index = 1;
        break;
    }

    Display_Data_Write(m_DataNew);
    Display_PosTitle(m_DataNew.command);

    Panel->show();
    Panel->setCurrentIndex(panel_index);
    SubPanel->show();
    SubPanel->setCurrentIndex(subpanel_index);

    // enable control (choice) - except time
    ui->wigPosSet->setEnabled(bPos);
    ui->wigPosSet_2->setEnabled(bPos2);
    ui->wigSpeed->setEnabled(bSpd);
    ui->wigAccu->setEnabled(bAccu);
    ui->wigPanel_2->setEnabled(bOut);
    ui->wigType->setEnabled(bType);

    /*// visible control (choice)
    ui->wigPosSet->setVisible(bPos);
    ui->wigPosSet_2->setVisible(bPos2);
    ui->wigSpeed->setVisible(bSpd);
    ui->wigAccu->setVisible(bAccu);
    ui->wigPanel_2->setVisible(bOut);
    ui->wigType->setVisible(bType);*/

}

bool dialog_sasang_motion::Display_Data_Write(ST_SSMOTION_DATA data)
{
    int command = Sasang->Motion->IndexOf(data.command);
    if(command < 0) return false;

    switch(command)
    {
    case HySasang_Motion::p2p:
    case HySasang_Motion::con:
    case HySasang_Motion::cir:

        Display_SetPos(data.trans[0], data.joint[0]);
        Display_SetPos2(data.trans[1], data.joint[1]);
        Display_Speed(data.speed_output);
        Display_Accuracy(data.accuracy_input);
        break;

    case HySasang_Motion::out:
        Display_Output(data.speed_output);
        Display_Time(data.time);
        Display_Type(data.type);
        break;

    case HySasang_Motion::wtt:
        Display_Time(data.time);
        break;
    }

    return true;
}

void dialog_sasang_motion::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtVal[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtVal[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtVal[2]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    m_vtVal[3]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, j));
    m_vtVal[4]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, j));
    m_vtVal[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtVal[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtVal[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtVal[2]->setText(str);
    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    m_vtVal[3]->setText(str);
    str.sprintf(FORMAT_POS, j.joint[JSWV]);
    m_vtVal[4]->setText(str);
    str.clear();
    m_vtVal[5]->setText(str);
#endif

}
void dialog_sasang_motion::Display_SetPos2(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtVal2[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtVal2[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtVal2[2]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    m_vtVal2[3]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, j));
    m_vtVal2[4]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, j));
    m_vtVal2[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtVal2[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtVal2[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtVal2[2]->setText(str);
    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    m_vtVal2[3]->setText(str);
    str.sprintf(FORMAT_POS, j.joint[JSWV]);
    m_vtVal2[4]->setText(str);
    str.clear();
    m_vtVal2[5]->setText(str);
#endif

}
void dialog_sasang_motion::Display_Speed(float speed)
{
    QString str;
    str.sprintf(FORMAT_SPEED, speed);
    ui->tbSpd->setText(str);
    ui->tbSpdUnit->setText(tr("%"));
}
void dialog_sasang_motion::Display_Accuracy(float accuracy)
{
    QString str;
    str.sprintf(FORMAT_POS, accuracy);
    ui->tbAccu->setText(str);
    ui->tbAccuUnit->setText(tr("mm"));
}
void dialog_sasang_motion::Display_Type(float type)
{
    for(int i=0;i<m_vtType.count();i++)
        m_vtType[i]->setAutoFillBackground((int)type == i);

    ui->wigTime->setEnabled((int)type == HyUserData::TYPE_PULSE);
}
void dialog_sasang_motion::Display_Time(float time)
{
    QString str;
    str.sprintf(FORMAT_TIME, time);
    ui->tbTime->setText(str);
    ui->tbTimeUnit->setText(tr("sec"));

    ui->wigTime->setEnabled(true);
}

void dialog_sasang_motion::MatchingOutput()
{
    m_vtOutPic.clear();m_vtOutputNum.clear();
    // here "m_vtOutputNum in output number" is modify ~~~~!!!!!
    m_vtOutPic.append(ui->btnPic);    m_vtOutputNum.append(HyOutput::VAC1); // ouput num 1~128 not 0
    m_vtOutPic.append(ui->btnPic_2);  m_vtOutputNum.append(HyOutput::VAC2);
    m_vtOutPic.append(ui->btnPic_3);  m_vtOutputNum.append(HyOutput::VAC3);
    m_vtOutPic.append(ui->btnPic_4);  m_vtOutputNum.append(HyOutput::VAC4);
    m_vtOutPic.append(ui->btnPic_5);  m_vtOutputNum.append(HyOutput::CHUCK);
    m_vtOutPic.append(ui->btnPic_6);  m_vtOutputNum.append(HyOutput::GRIP);
    m_vtOutPic.append(ui->btnPic_7);  m_vtOutputNum.append(HyOutput::NIPPER);
    m_vtOutPic.append(ui->btnPic_8);  m_vtOutputNum.append(HyOutput::RGRIP);
    m_vtOutPic.append(ui->btnPic_9);  m_vtOutputNum.append(HyOutput::USER1);
    m_vtOutPic.append(ui->btnPic_10); m_vtOutputNum.append(HyOutput::USER2);
    m_vtOutPic.append(ui->btnPic_11); m_vtOutputNum.append(HyOutput::USER3);
    m_vtOutPic.append(ui->btnPic_12); m_vtOutputNum.append(HyOutput::USER4);
    m_vtOutPic.append(ui->btnPic_13); m_vtOutputNum.append(HyOutput::USER5);
    m_vtOutPic.append(ui->btnPic_14); m_vtOutputNum.append(HyOutput::USER6);
    m_vtOutPic.append(ui->btnPic_15); m_vtOutputNum.append(HyOutput::USER7);
    m_vtOutPic.append(ui->btnPic_16); m_vtOutputNum.append(HyOutput::USER8);


    // etc...
    m_vtOutIcon.clear();m_vtOutSel.clear();
    m_vtOutIcon.append(ui->btnIcon);   m_vtOutSel.append(ui->btnSel);
    m_vtOutIcon.append(ui->btnIcon_2); m_vtOutSel.append(ui->btnSel_2);
    m_vtOutIcon.append(ui->btnIcon_3); m_vtOutSel.append(ui->btnSel_3);
    m_vtOutIcon.append(ui->btnIcon_4); m_vtOutSel.append(ui->btnSel_4);
    m_vtOutIcon.append(ui->btnIcon_5); m_vtOutSel.append(ui->btnSel_5);
    m_vtOutIcon.append(ui->btnIcon_6); m_vtOutSel.append(ui->btnSel_6);
    m_vtOutIcon.append(ui->btnIcon_7); m_vtOutSel.append(ui->btnSel_7);
    m_vtOutIcon.append(ui->btnIcon_8); m_vtOutSel.append(ui->btnSel_8);
    m_vtOutIcon.append(ui->btnIcon_9); m_vtOutSel.append(ui->btnSel_9);
    m_vtOutIcon.append(ui->btnIcon_10);m_vtOutSel.append(ui->btnSel_10);
    m_vtOutIcon.append(ui->btnIcon_11);m_vtOutSel.append(ui->btnSel_11);
    m_vtOutIcon.append(ui->btnIcon_12);m_vtOutSel.append(ui->btnSel_12);
    m_vtOutIcon.append(ui->btnIcon_13);m_vtOutSel.append(ui->btnSel_13);
    m_vtOutIcon.append(ui->btnIcon_14);m_vtOutSel.append(ui->btnSel_14);
    m_vtOutIcon.append(ui->btnIcon_15);m_vtOutSel.append(ui->btnSel_15);
    m_vtOutIcon.append(ui->btnIcon_16);m_vtOutSel.append(ui->btnSel_16);
}

void dialog_sasang_motion::Display_Output(float output)
{
    for(int i=0;i<m_vtOutSel.count();i++)
    {
        if(m_vtOutputNum[i] == (int)output)
            m_vtOutSel[i]->show();
        else
            m_vtOutSel[i]->hide();
    }
}

void dialog_sasang_motion::Display_SaveEnable()
{
    bool bSame = IsSame(m_DataOld, m_DataNew);

    ui->wigSave->setEnabled(!bSame);
    ui->wigReset->setEnabled(!bSame);
}

void dialog_sasang_motion::Display_PosTitle(QString command)
{
    if(command == Sasang->Motion->toStr(HySasang_Motion::cir))
    {
        ui->tbValTitle->setText(tr("Middle"));
        ui->tbVal2Title->setText(tr("End"));
    }
    else
    {
        ui->tbValTitle->setText(tr("Setting"));
        ui->tbVal2Title->setText(tr(""));
    }
}


bool dialog_sasang_motion::IsSame(ST_SSMOTION_DATA old_data, ST_SSMOTION_DATA new_data)
{
    int i;

    if(old_data.command != new_data.command)
        return false;
    for(i=0;i<3;i++)
    {
        if(old_data.trans[0].p[i] != new_data.trans[0].p[i])
            return false;
        if(old_data.trans[1].p[i] != new_data.trans[1].p[i])
            return false;

        if(old_data.trans[0].eu[i] != new_data.trans[0].eu[i])
            return false;
        if(old_data.trans[1].eu[i] != new_data.trans[1].eu[i])
            return false;
    }

    for(i=0;i<USE_AXIS_NUM;i++)
    {
        if(old_data.joint[0].joint[i] != new_data.joint[0].joint[i])
            return false;
        if(old_data.joint[1].joint[i] != new_data.joint[1].joint[i])
            return false;
    }

    if(old_data.speed_output != new_data.speed_output)
        return false;
    if(old_data.accuracy_input != new_data.accuracy_input)
        return false;
    if(old_data.type != new_data.type)
        return false;
    if(old_data.time != new_data.time)
        return false;

    return true;
}

bool dialog_sasang_motion::Save()
{
    if(Sasang->Motion->Data2Raw(m_DataNew)
    && Sasang->Motion->Data2Prog(m_DataNew))
    {
        Sasang->Motion->MotionList[m_nSelectedIndex] = m_DataNew;
        m_DataOld = m_DataNew;
        return true;
    }

    return false;
}
bool dialog_sasang_motion::Reset()
{
    m_DataNew = m_DataOld;
    return true;
}

void dialog_sasang_motion::Control_ListEdit(bool bOn)
{
    ui->wigListEdit->setEnabled(bOn);

    if(pxmListEditIcon.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon992-1.png");   // off
        pxmListEditIcon.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon992-10.png");   // on
        pxmListEditIcon.append(QPixmap::fromImage(img));
    }

    ui->btnListEditIcon->setPixmap(pxmListEditIcon[(int)bOn]);
}



void dialog_sasang_motion::onList()
{
    QLabel3* sel = (QLabel3*)sender();

    int index = m_vtTbNo.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nStartIndex + index;
    if(list_index == m_nSelectedIndex) return;


    if(ui->wigSave->isEnabled())
    {
        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::GREEN);
        conf->Title(tr("Confirm"));
        conf->Message(tr("Would you want to view another data?"), tr("Changed data will not be saved."));
        if(conf->exec() != QDialog::Accepted)
            return;
    }

    if(list_index < Sasang->Motion->MotionList.size())
    {
        m_nSelectedIndex = list_index;
        Redraw_List(m_nStartIndex);

        Test_View_RawProg(Sasang->Motion->MotionList[m_nSelectedIndex]);

        // change panel...
    }
}
void dialog_sasang_motion::onUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_sasang_motion::onDown()
{
    if(m_nStartIndex >= (Sasang->Motion->GetCount() - m_vtTbNo.size()))
        return;

    Redraw_List(++m_nStartIndex);
}
void dialog_sasang_motion::onListUp()
{
    if(Sasang->Motion->Up(m_nSelectedIndex))
    {
        m_nSelectedIndex--;
        if(m_nStartIndex > m_nSelectedIndex)
            m_nStartIndex--;

        Redraw_List(m_nStartIndex);
    }
}
void dialog_sasang_motion::onListDown()
{
    if(Sasang->Motion->Down(m_nSelectedIndex))
    {
        m_nSelectedIndex++;
        if((m_nStartIndex + m_vtTbNo.size()) <= m_nSelectedIndex)
            m_nStartIndex++;

        Redraw_List(m_nStartIndex);
    }
}
void dialog_sasang_motion::onListDel()
{
    if(Sasang->Motion->GetCount() <= 0)
        return;
    if(m_nSelectedIndex <= 0)
        return;
    if(m_nSelectedIndex == (Sasang->Motion->GetCount()-1))
        return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(QString(tr("CONFIRM")));
    conf->Message(QString(tr("Would you want to DELETE?"))
                  ,Sasang->Motion->GetName(m_nSelectedIndex));
    if(conf->exec() == QDialog::Rejected)
        return;

    if(Sasang->Motion->Del(m_nSelectedIndex))
    {
        if(m_nSelectedIndex >= Sasang->Motion->GetCount())
            m_nSelectedIndex = Sasang->Motion->GetCount() - 1;

        if((m_nStartIndex + m_vtTbNo.size()) > Sasang->Motion->GetCount())
        {
            if(m_nStartIndex > 0)
                m_nStartIndex--;
        }

        Redraw_List(m_nStartIndex);
    }
    else
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("DELETE Error!"),
                     tr("Please, Try again!"));
        msg->exec();
        return;
    }
}

void dialog_sasang_motion::onTeach()
{
    page_teach->m_bEditing = m_bEditing;
    Page->setCurrentIndex(1);
}
void dialog_sasang_motion::onTest()
{
    Page->setCurrentIndex(2);
}

void dialog_sasang_motion::onSave()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(QString(tr("CONFIRM")));
    conf->Message(QString(tr("Would you want to SAVE?")));
    if(conf->exec() == QDialog::Rejected)
        return;

    if(!Save())
    {
        // error case
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Save Error!"),
                     tr("Please, Try again!"));
        msg->exec();
        return;
    }

    Display_Data_Write(m_DataNew);

}
void dialog_sasang_motion::onReset()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title(QString(tr("CONFIRM")));
    conf->Message(QString(tr("Would you want to RESET?")));
    if(conf->exec() == QDialog::Rejected)
        return;

    Reset();
    Display_Data_Write(m_DataNew);
}

void dialog_sasang_motion::onPos()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtValPic.indexOf(sel);
    if(index < 0) return;


    float now_value, value;
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);


    np->m_numpad->SetTitle(m_vtAxis[index]->text());
    now_value = m_vtVal[index]->text().toFloat();
    np->m_numpad->SetNum((double)now_value);
    value = now_value - POS_VALUE_SET_RANGE;
    np->m_numpad->SetMinValue((double)value);
    value = now_value + POS_VALUE_SET_RANGE;
    np->m_numpad->SetMaxValue((double)value);
    np->m_numpad->SetSosuNum(SOSU_POS);

    cn_trans _trans;
    cn_joint _joint;
    if(np->exec() == QDialog::Accepted)
    {
        if(index < JROT)
        {
            _trans = m_DataNew.trans[0];
            _trans.p[index] = (float)np->m_numpad->GetNumDouble();
            if(Posi->Trans2Joint(_trans, m_DataNew.joint[0],_joint))
            {
                m_DataNew.trans[0] = _trans;
                m_DataNew.joint[0] = _joint;
            }
            else
            {
                // error
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
                return;
            }
        }
        else
        {
            _joint = m_DataNew.joint[0];

#ifdef _H6_
            //_joint.joint[index] = (float)np->m_numpad->GetNumDouble();
            _joint.joint[index] = gGetDisp2Joint(index, (float)np->m_numpad->GetNumDouble(), _joint);
#else
            if(index == JROT)
            {
                float curr_wrist_angle = gGetWristAngle(_joint);
                float curr_input_angel = (float)np->m_numpad->GetNumDouble();
                float diff_angle = curr_input_angel - curr_wrist_angle;

                _joint.joint[index] += diff_angle;
            }
            else
                _joint.joint[index] = (float)np->m_numpad->GetNumDouble();
#endif

            if(Posi->Joint2Trans(_joint, _trans))
            {
                m_DataNew.joint[0] = _joint;
                m_DataNew.trans[0] = _trans;
            }
            else
            {
                //Error Message
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
                return;
            }
        }

        Display_Data_Write(m_DataNew);
    }

}
void dialog_sasang_motion::onPos2()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtVal2Pic.indexOf(sel);
    if(index < 0) return;

#ifdef _H6_
    if(index >= JROT) return;
#endif

    float now_value, value;
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);


    np->m_numpad->SetTitle(m_vtAxis[index]->text());
    now_value = m_vtVal2[index]->text().toFloat();
    np->m_numpad->SetNum((double)now_value);
    value = now_value - POS_VALUE_SET_RANGE;
    np->m_numpad->SetMinValue((double)value);
    value = now_value + POS_VALUE_SET_RANGE;
    np->m_numpad->SetMaxValue((double)value);
    np->m_numpad->SetSosuNum(SOSU_POS);

    cn_trans _trans;
    cn_joint _joint;
    if(np->exec() == QDialog::Accepted)
    {
        if(index < JROT)
        {
            _trans = m_DataNew.trans[1];
            _trans.p[index] = (float)np->m_numpad->GetNumDouble();
            if(Posi->Trans2Joint(_trans, m_DataNew.joint[1],_joint))
            {
                m_DataNew.trans[1] = _trans;
                m_DataNew.joint[1] = _joint;
            }
            else
            {
                // error
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
                return;
            }
        }
        else
        {
            _joint = m_DataNew.joint[1];

#ifdef _H6_
            //_joint.joint[index] = (float)np->m_numpad->GetNumDouble();
            _joint.joint[index] = gGetDisp2Joint(index, (float)np->m_numpad->GetNumDouble(), _joint);
#else
            if(index == JROT)
            {
                float curr_wrist_angle = gGetWristAngle(_joint);
                float curr_input_angel = (float)np->m_numpad->GetNumDouble();
                float diff_angle = curr_input_angel - curr_wrist_angle;

                _joint.joint[index] += diff_angle;
            }
            else
                _joint.joint[index] = (float)np->m_numpad->GetNumDouble();
#endif

            if(Posi->Joint2Trans(_joint, _trans))
            {
                m_DataNew.joint[1] = _joint;
                m_DataNew.trans[1] = _trans;
            }
            else
            {
                //Error Message
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
                return;
            }
        }

        Display_Data_Write(m_DataNew);
    }
}
void dialog_sasang_motion::onSpeed()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbSpdTitle->text());
    np->m_numpad->SetNum(m_DataNew.speed_output);
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);

    if(np->exec() == QDialog::Accepted)
    {
        m_DataNew.speed_output = (float)np->m_numpad->GetNumDouble();
        Display_Data_Write(m_DataNew);
    }
}
void dialog_sasang_motion::onAccuracy()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbAccuTitle->text());
    np->m_numpad->SetNum(m_DataNew.accuracy_input);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(MAX_POS_VALUE);
    np->m_numpad->SetSosuNum(SOSU_POS);

    if(np->exec() == QDialog::Accepted)
    {
        m_DataNew.accuracy_input = (float)np->m_numpad->GetNumDouble();
        Display_Data_Write(m_DataNew);
    }
}
void dialog_sasang_motion::onOutput()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtOutPic.indexOf(sel);
    if(index < 0) return;

    m_DataNew.speed_output = m_vtOutputNum[index];
    Display_Data_Write(m_DataNew);
}

void dialog_sasang_motion::onTime()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbTimeTitle->text());
    np->m_numpad->SetNum(m_DataNew.time);
    np->m_numpad->SetMinValue(MIN_TIME_SET);
    np->m_numpad->SetMaxValue(MAX_TIME_SET);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        m_DataNew.time = (float)np->m_numpad->GetNumDouble();
        Display_Data_Write(m_DataNew);
    }
}
void dialog_sasang_motion::onType()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtType.indexOf(sel);
    if(index < 0) return;

    m_DataNew.type = (float)index;
    Display_Data_Write(m_DataNew);
}

void dialog_sasang_motion::onShift()
{
    if(dig_shift == 0)
        dig_shift = new dialog_sasang_shift();

    if(dig_shift->exec() == QDialog::Accepted)
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        QString str;
        int i;
        int ret;

        if(dig_shift->IsShift())
        {
            ST_SSSHIFT_VALUE val;
            dig_shift->Get(val);

            QList<ST_SSMOTION_DATA> list;
            list = Sasang->Motion->MotionList;

            for(i=0;i<list.count();i++)
            {
                ret = Sasang->Motion->DataShift(list[i], val);
                if(ret < 0)
                {
                    msg->SetColor(dialog_message::RED);
                    msg->Title(QString(tr("Error")));
                    msg->Message(tr("Position Shift Error!"),
                                 tr("Please, Try again!"));
                    msg->exec();
                    return;
                }
                else if(ret >= 1)
                {
                    if(!Sasang->Motion->Data2Raw(list[i])
                    || !Sasang->Motion->Data2Prog(list[i]))
                    {
                        msg->SetColor(dialog_message::RED);
                        msg->Title(QString(tr("Error")));
                        msg->Message(tr("Position Shift Error!"),
                                     tr("Please, Try again!"));
                        msg->exec();
                        return;
                    }
                }
            }

            Sasang->Motion->MotionList = list;
            Redraw_List(m_nStartIndex);

            msg->SetColor(dialog_message::GREEN);
            msg->Title(QString(tr("Success")));

#ifdef _H6_
            str.sprintf("%.2f,%.2f,%.2f,%.2f,%.2f,%.2f",val.x,val.y,val.z,val.r,val.s,val.j6);
#else
            str.sprintf("%.2f,%.2f,%.2f,%.2f,%.2f",val.x,val.y,val.z,val.r,val.s);
#endif
            msg->Message(tr("Position Shift Success!"),
                         str);
            msg->exec();
        }

        if(dig_shift->IsSpeed())
        {
            for(i=0;i<Sasang->Motion->GetCount();i++)
            {
                ret = Sasang->Motion->SetSpeed(Sasang->Motion->MotionList[i], dig_shift->GetSpeed());
                if(ret < 0)
                {
                    msg->SetColor(dialog_message::RED);
                    msg->Title(QString(tr("Error")));
                    msg->Message(tr("Change All Speed Value Error!"),
                                 tr("Please, Try again!"));
                    msg->exec();
                    return;
                }
            }

            msg->SetColor(dialog_message::GREEN);
            msg->Title(QString(tr("Success")));
            str.sprintf(FORMAT_SPEED, dig_shift->GetSpeed());
            msg->Message(tr("Change All Speed Value Success!"),
                         str + ui->tbSpdUnit->text());
            msg->exec();
        }

        if(dig_shift->IsAccuracy())
        {
            for(i=0;i<Sasang->Motion->GetCount();i++)
            {
                ret = Sasang->Motion->SetAccuracy(Sasang->Motion->MotionList[i], dig_shift->GetAccuracy());
                if(ret < 0)
                {
                    msg->SetColor(dialog_message::RED);
                    msg->Title(QString(tr("Error")));
                    msg->Message(tr("Change Accuracy Value Error!"),
                                 tr("Please, Try again!"));
                    msg->exec();
                    return;
                }
            }

            msg->SetColor(dialog_message::GREEN);
            msg->Title(QString(tr("Success")));
            str.sprintf(FORMAT_POS, dig_shift->GetAccuracy());
            msg->Message(tr("Change Accuracy Value Success!"),
                         str + ui->tbAccuUnit->text());
            msg->exec();
        }

        Redraw_List(m_nStartIndex);
    }
}

void dialog_sasang_motion::onListEdit()
{
    bool bOn = ui->wigListEdit->isEnabled();
    Control_ListEdit(!bOn);
}


/** for Test **/
void dialog_sasang_motion::Test_Make_List()
{
    CNRobo* pCon = CNRobo::getInstance();

    Sasang->Motion->ClearList();

    ST_SSMOTION_DATA data;

    // start
    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::p2p);
    data.joint[0].joint[0] = 1;
    data.joint[0].joint[1] = 0;
    data.joint[0].joint[2] = 0;
    data.joint[0].joint[3] = 0;
    data.joint[0].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    data.speed_output = 50.0;
    Sasang->Motion->MotionList.append(data);

    // end
    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::p2p);
    data.joint[0].joint[0] = 486.20;
    data.joint[0].joint[1] = -8.91;
    data.joint[0].joint[2] = 17.69;
    data.joint[0].joint[3] = -8.80;
    data.joint[0].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    data.speed_output = 50.0;
    Sasang->Motion->MotionList.append(data);

    //=======

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
    data.joint[0].joint[0] = 50.0;
    data.joint[0].joint[1] = 0;
    data.joint[0].joint[2] = 0;
    data.joint[0].joint[3] = 0;
    data.joint[0].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    data.speed_output = 50.0;
    data.accuracy_input = 1.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
    data.joint[0].joint[0] = 100.0;
    data.joint[0].joint[1] = 0;
    data.joint[0].joint[2] = 0;
    data.joint[0].joint[3] = 0;
    data.joint[0].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    data.speed_output = 50.0;
    data.accuracy_input = 1.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
    data.joint[0].joint[0] = 150.0;
    data.joint[0].joint[1] = 0;
    data.joint[0].joint[2] = 0;
    data.joint[0].joint[3] = 0;
    data.joint[0].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    data.speed_output = 50.0;
    data.accuracy_input = 1.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::out);
    data.speed_output = 1;
    data.type = HyUserData::TYPE_ON;
    data.time = 0.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::wtt);
    data.time = 2.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::out);
    data.speed_output = 1;
    data.type = HyUserData::TYPE_OFF;
    data.time = 0.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
    data.joint[0].joint[0] = 100.0;
    data.joint[0].joint[1] = 0;
    data.joint[0].joint[2] = 0;
    data.joint[0].joint[3] = 0;
    data.joint[0].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    data.speed_output = 50.0;
    data.accuracy_input = 1.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::out);
    data.speed_output = 2;
    data.type = HyUserData::TYPE_PULSE;
    data.time = 2.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
    data.joint[0].joint[0] = 50.0;
    data.joint[0].joint[1] = 0;
    data.joint[0].joint[2] = 0;
    data.joint[0].joint[3] = 0;
    data.joint[0].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    data.speed_output = 50.0;
    data.accuracy_input = 1.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::wtt);
    data.time = 3.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::p2p);
    data.joint[0].joint[0] = 200.0;
    data.joint[0].joint[1] = -9.74;
    data.joint[0].joint[2] = 34.29;
    data.joint[0].joint[3] = -24.55;
    data.joint[0].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    data.speed_output = 50.0;
    Sasang->Motion->Add(data);

    Sasang->Motion->Default(data);
    data.command = Sasang->Motion->toStr(HySasang_Motion::cir);
    data.joint[0].joint[0] = 328.79;
    data.joint[0].joint[1] = -9.82;
    data.joint[0].joint[2] = 45.27;
    data.joint[0].joint[3] = -35.54;
    data.joint[0].joint[4] = 0;

    data.joint[1].joint[0] = 486.20;
    data.joint[1].joint[1] = -9.03;
    data.joint[1].joint[2] = 36.03;
    data.joint[1].joint[3] = -27.09;
    data.joint[1].joint[4] = 0;
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    pCon->calcForward(data.joint[1].joint, data.trans[1]);
    data.speed_output = 20;
    Sasang->Motion->Add(data);

}



void dialog_sasang_motion::Test_Make_RawProg()
{
    int i;

    ST_SSMOTION_DATA data;

    for(i=0;i<Sasang->Motion->MotionList.count();i++)
    {
        qDebug() << "Motion " << i+1;
        if(Sasang->Motion->Data2Raw(Sasang->Motion->MotionList[i]))
        {
            Sasang->Motion->Default(data);
            data.raw = Sasang->Motion->MotionList[i].raw;
        }

        Sasang->Motion->Raw2Data(data);

        Sasang->Motion->Data2Prog(Sasang->Motion->MotionList[i]);

        Sasang->Motion->Prog2Macro(data.program, i);
    }

    Sasang->Motion->MakeTestMain();
}

void dialog_sasang_motion::Test_View_RawProg(ST_SSMOTION_DATA data)
{
    qDebug() << data.raw;
    for(int i=0;i<data.program.count();i++)
        qDebug() << data.program[i];
}

