#include "dialog_pictureview.h"
#include "ui_dialog_pictureview.h"

dialog_pictureview::dialog_pictureview(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_pictureview)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->lbPicture,SIGNAL(mouse_release()),this,SLOT(accept()));
}

dialog_pictureview::~dialog_pictureview()
{
    delete ui;
}
void dialog_pictureview::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_pictureview::showEvent(QShowEvent *)
{
    Show();
}
void dialog_pictureview::hideEvent(QHideEvent *)
{

}

void dialog_pictureview::Init(QString recipe_no)
{
    m_strRecipeNo = recipe_no;
}

void dialog_pictureview::Show()
{
    QString path = PICVIEW_BASE_PATH;
    path += "/";
    path += m_strRecipeNo;

    QImage img;
    if(!img.load(path))
    {
        View_DefaultPic();
        return;
    }

    View_Pic(path);
}

void dialog_pictureview::View_DefaultPic()
{
    QString default_pic_path = ":/image/image/Logo_HY.png";
    View_Pic(default_pic_path);
}

bool dialog_pictureview::View_Pic(QString path)
{
    QImage img;
    img.load(path);
    QPixmap pxm = QPixmap::fromImage(img);

    ui->lbPicture->setPixmap(pxm);
    return true;
}
