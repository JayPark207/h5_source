#include "dialog_sasang_motion_teach.h"
#include "ui_dialog_sasang_motion_teach.h"

dialog_sasang_motion_teach::dialog_sasang_motion_teach(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dialog_sasang_motion_teach)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    wig_jog = new widget_jogmodule4(ui->wigJog);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    Panel = new QStackedWidget(ui->wigData);
    Panel->setGeometry(ui->wigPos->x(),ui->wigPos->y(),
                       ui->wigPos->width(),ui->wigPos->height());
    Panel->addWidget(ui->wigPos);
    Panel->addWidget(ui->wigOut);
    Panel->setCurrentIndex(0);

    m_vtTbNo.clear();m_vtTbMot.clear();
    m_vtTbNo.append(ui->tbNo);  m_vtTbMot.append(ui->tbMot);
    m_vtTbNo.append(ui->tbNo_2);m_vtTbMot.append(ui->tbMot_2);
    m_vtTbNo.append(ui->tbNo_3);m_vtTbMot.append(ui->tbMot_3);
    m_vtTbNo.append(ui->tbNo_4);m_vtTbMot.append(ui->tbMot_4);
    m_vtTbNo.append(ui->tbNo_5);m_vtTbMot.append(ui->tbMot_5);
    m_vtTbNo.append(ui->tbNo_6);m_vtTbMot.append(ui->tbMot_6);
    m_vtTbNo.append(ui->tbNo_7);m_vtTbMot.append(ui->tbMot_7);
    m_vtTbNo.append(ui->tbNo_8);m_vtTbMot.append(ui->tbMot_8);

    int i;
    for(i=0;i<m_vtTbNo.count();i++)
    {
        connect(m_vtTbNo[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtTbMot[i],SIGNAL(mouse_release()),m_vtTbNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

    m_vtAxis.clear();
    m_vtAxis.append(ui->tbAxis);
    m_vtAxis.append(ui->tbAxis_2);
    m_vtAxis.append(ui->tbAxis_3);
    m_vtAxis.append(ui->tbAxis_4);
    m_vtAxis.append(ui->tbAxis_5);
    m_vtAxis.append(ui->tbAxis_6);

    m_vtSet.clear();m_vtReal.clear();
    m_vtSet.append(ui->tbSet);  m_vtReal.append(ui->tbReal);
    m_vtSet.append(ui->tbSet_2);m_vtReal.append(ui->tbReal_2);
    m_vtSet.append(ui->tbSet_3);m_vtReal.append(ui->tbReal_3);
    m_vtSet.append(ui->tbSet_4);m_vtReal.append(ui->tbReal_4);
    m_vtSet.append(ui->tbSet_5);m_vtReal.append(ui->tbReal_5);
    m_vtSet.append(ui->tbSet_6);m_vtReal.append(ui->tbReal_6);

    connect(ui->tbSetTitlePic,SIGNAL(mouse_release()),this,SLOT(onSetTitle()));
    connect(ui->tbSetTitle,SIGNAL(mouse_press()),ui->tbSetTitlePic,SLOT(press()));
    connect(ui->tbSetTitle,SIGNAL(mouse_release()),ui->tbSetTitlePic,SLOT(release()));

    connect(ui->tbSpdPic,SIGNAL(mouse_release()),this,SLOT(onSpeed()));
    connect(ui->tbSpd,SIGNAL(mouse_press()),ui->tbSpdPic,SLOT(press()));
    connect(ui->tbSpd,SIGNAL(mouse_release()),ui->tbSpdPic,SLOT(release()));

    connect(ui->tbAccuPic,SIGNAL(mouse_release()),this,SLOT(onAccuracy()));
    connect(ui->tbAccu,SIGNAL(mouse_press()),ui->tbAccuPic,SLOT(press()));
    connect(ui->tbAccu,SIGNAL(mouse_release()),ui->tbAccuPic,SLOT(release()));

    connect(ui->tbRealTitlePic,SIGNAL(mouse_release()),this,SLOT(onCurrent()));
    connect(ui->tbRealTitle,SIGNAL(mouse_press()),ui->tbRealTitlePic,SLOT(press()));
    connect(ui->tbRealTitle,SIGNAL(mouse_release()),ui->tbRealTitlePic,SLOT(release()));

    connect(ui->btnModPic,SIGNAL(mouse_release()),this,SLOT(onOutput()));
    connect(ui->btnModIcon,SIGNAL(mouse_press()),ui->btnModPic,SLOT(press()));
    connect(ui->btnModIcon,SIGNAL(mouse_release()),ui->btnModPic,SLOT(release()));

    connect(ui->tbAddSpdPic,SIGNAL(mouse_release()),this,SLOT(onAddSpeed()));
    connect(ui->tbAddSpd,SIGNAL(mouse_press()),ui->tbAddSpdPic,SLOT(press()));
    connect(ui->tbAddSpd,SIGNAL(mouse_release()),ui->tbAddSpdPic,SLOT(release()));

    connect(ui->tbAddAccuPic,SIGNAL(mouse_release()),this,SLOT(onAddAccuracy()));
    connect(ui->tbAddAccu,SIGNAL(mouse_press()),ui->tbAddAccuPic,SLOT(press()));
    connect(ui->tbAddAccu,SIGNAL(mouse_release()),ui->tbAddAccuPic,SLOT(release()));

    connect(ui->btnSavePic,SIGNAL(mouse_release()),this,SLOT(onSave()));
    connect(ui->btnSaveIcon,SIGNAL(mouse_press()),ui->btnSavePic,SLOT(press()));
    connect(ui->btnSaveIcon,SIGNAL(mouse_release()),ui->btnSavePic,SLOT(release()));

    m_vtAddPic.clear();m_vtAdd.clear();
    m_vtAddPic.append(ui->btnP2pPic);m_vtAdd.append(ui->btnP2p);
    m_vtAddPic.append(ui->btnConPic);m_vtAdd.append(ui->btnCon);
    m_vtAddPic.append(ui->btnCirPic);m_vtAdd.append(ui->btnCir);
    m_vtAddPic.append(ui->btnOutPic);m_vtAdd.append(ui->btnOut);
    m_vtAddPic.append(ui->btnWttPic);m_vtAdd.append(ui->btnWtt);

    for(i=0;i<m_vtAddPic.count();i++)
    {
        connect(m_vtAddPic[i],SIGNAL(mouse_release()),this,SLOT(onAdd()));
        connect(m_vtAdd[i],SIGNAL(mouse_press()),m_vtAddPic[i],SLOT(press()));
        connect(m_vtAdd[i],SIGNAL(mouse_release()),m_vtAddPic[i],SLOT(release()));
    }

    connect(ui->btnDelPic,SIGNAL(mouse_release()),this,SLOT(onDel()));
    connect(ui->btnDel,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDel,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));
    connect(ui->btnDelIcon,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDelIcon,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));

#ifdef _H6_
    ui->tbAxis_6->setText(tr("J6"));

    ui->tbAxis_6->setEnabled(true);
    ui->tbSet_6->setEnabled(true);
    ui->tbReal_6->setEnabled(true);
#else
    ui->tbAxis_6->setText("");

    ui->tbAxis_6->setEnabled(false);
    ui->tbSet_6->setEnabled(false);
    ui->tbReal_6->setEnabled(false);
#endif


    // initial clear
    pxmType.clear();
    pxmOutput.clear();

    m_AddSpeed = 50.0;      // default value
    m_AddAccuracy = 1.0;    // defalut value

    dig_sasang_output = 0;
    m_bAddCirEnd = false;

    m_bEditing = false;

}

dialog_sasang_motion_teach::~dialog_sasang_motion_teach()
{
    delete ui;
}
void dialog_sasang_motion_teach::showEvent(QShowEvent *)
{
    Update();

    timer->start();

    gReadyJog(true);

}
void dialog_sasang_motion_teach::hideEvent(QHideEvent *)
{
    timer->stop();

    gReadyJog(false);
}
void dialog_sasang_motion_teach::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_sasang_motion_teach::Update()
{
    m_nStartIndex=0;m_nSelectedIndex=0;
    Redraw_List(m_nStartIndex);

    m_bAddCirEnd = false;
}

void dialog_sasang_motion_teach::onClose()
{
    emit sigClose();
}

void dialog_sasang_motion_teach::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    float* trans = pCon->getCurTrans();
    float* joint = pCon->getCurJoint();

    Display_RealPos(trans, joint);
    Display_AddSpeed(m_AddSpeed);
    Display_AddAccuracy(m_AddAccuracy);

    Display_SaveEnable();
    Enable_CirEnd(m_bAddCirEnd);

    Display_Editing(m_bEditing);
}

void dialog_sasang_motion_teach::onList()
{
    QLabel3* sel = (QLabel3*)sender();

    int index = m_vtTbNo.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nStartIndex + index;
    if(list_index == m_nSelectedIndex) return;

    if(list_index < Sasang->Motion->MotionList.size())
    {
        m_nSelectedIndex = list_index;
        Redraw_List(m_nStartIndex);
    }
}
void dialog_sasang_motion_teach::onUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_sasang_motion_teach::onDown()
{
    if(m_nStartIndex >= (Sasang->Motion->GetCount() - m_vtTbNo.size()))
        return;

    Redraw_List(++m_nStartIndex);
}

void dialog_sasang_motion_teach::Redraw_List(int start_index)
{
    int cal_size = Sasang->Motion->MotionList.size() - start_index;
    int index;
    QString no,mot;

    Display_ListOn(-1); // all off

    for(int i=0;i<m_vtTbNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            mot = Sasang->Motion->GetName(index);
            no = QString().setNum(index + 1);
        }
        else
        {
            // no data.
            mot.clear();
            no.clear();
        }

        m_vtTbNo[i]->setText(no);
        m_vtTbMot[i]->setText(mot);

        if((start_index + i) == m_nSelectedIndex)
            Display_ListOn(i);
    }

    Display_Data(m_nSelectedIndex);

    // display list size.
    ui->lbListSize->setText(QString().setNum(Sasang->Motion->GetCount()));
}
void dialog_sasang_motion_teach::Display_ListOn(int table_index)
{
    for(int i=0;i<m_vtTbNo.count();i++)
    {
        m_vtTbNo[i]->setAutoFillBackground(i==table_index);
        m_vtTbMot[i]->setAutoFillBackground(i==table_index);
    }
}
void dialog_sasang_motion_teach::Display_Data(int list_index)
{
    if(list_index < 0
    || list_index >= Sasang->Motion->MotionList.size())
    {
        // no have data.
        return;
    }

    //0. data save
    m_DataOld = Sasang->Motion->MotionList[list_index];
    m_DataNew = m_DataOld;

    //1. Change Panel & SubPanel / ...
    int command = Sasang->Motion->IndexOf(m_DataNew.command);
    if(command < 0)
    {
        // no have command
        return;
    }

    int panel_index=0;
    bool bSpd=false,bAccu=false,bOut=false,bType=false,bCurr=false;

    switch(command)
    {
    case HySasang_Motion::p2p:
        panel_index = 0;
        bSpd=true;
        bCurr=true;
        break;
    case HySasang_Motion::con:
        panel_index = 0;
        bSpd=true;
        bAccu=true;
        bCurr=true;
        break;
    case HySasang_Motion::cir:
        panel_index = 0;
        bSpd=true;
        bCurr=true;
        break;
    case HySasang_Motion::out:
        panel_index = 1;
        bOut=true;
        bType=true;
        break;
    case HySasang_Motion::wtt:
        panel_index = 1;
        break;
    }

    Display_Data_Write(m_DataNew);
    Display_SetTitle(m_DataNew.command);

    Panel->show();
    Panel->setCurrentIndex(panel_index);

    // enable control (choice) - except time
    ui->wigSpd->setEnabled(bSpd);
    ui->wigAccu->setEnabled(bAccu);
    ui->wigOutput->setEnabled(bOut);
    ui->wigType->setEnabled(bType);
    ui->wigCurr->setEnabled(bCurr);

}

void dialog_sasang_motion_teach::Display_Data_Write(ST_SSMOTION_DATA data)
{
    int command = Sasang->Motion->IndexOf(data.command);
    if(command < 0) return;

    switch(command)
    {
    case HySasang_Motion::p2p:
    case HySasang_Motion::con:
        Display_SetPos(data.trans[0], data.joint[0]);
        Display_Speed(data.speed_output);
        Display_Accuracy(data.accuracy_input);
        break;

    case HySasang_Motion::cir:
        if(!m_bSetTitle)
            Display_SetPos(data.trans[0], data.joint[0]);
        else
            Display_SetPos(data.trans[1], data.joint[1]);

        Display_Speed(data.speed_output);
        break;

    case HySasang_Motion::out:
        Display_Output(data.speed_output);
        Display_Time(data.time);
        Display_Type(data.type);
        break;

    case HySasang_Motion::wtt:
        Display_Time(data.time);
        break;
    }

    return;
}

void dialog_sasang_motion_teach::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtSet[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtSet[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtSet[2]->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    m_vtSet[3]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, j));
    m_vtSet[4]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, j));
    m_vtSet[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtSet[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtSet[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtSet[2]->setText(str);
    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    m_vtSet[3]->setText(str);
    str.sprintf(FORMAT_POS, j.joint[JSWV]);
    m_vtSet[4]->setText(str);
    str.clear();
    m_vtSet[5]->setText(str);
#endif

}
void dialog_sasang_motion_teach::Display_RealPos(float* trans, float* joint)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, trans[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, trans[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, trans[UPDN]);
    m_vtReal[2]->setText(str);

    cn_joint _joint = gMakeCnJoint(joint);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, _joint));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, _joint));
    m_vtReal[4]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, _joint));
    m_vtReal[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, trans[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, trans[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, trans[UPDN]);
    m_vtReal[2]->setText(str);
    str.sprintf(FORMAT_POS, gGetWristAngle(joint[J2], joint[J3], joint[J4]));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, joint[JSWV]);
    m_vtReal[4]->setText(str);
    str.clear();
    m_vtReal[5]->setText(str);
#endif

    // make cn_trans & cn_joint
    int i;
    for(i=0;i<3;i++)
    {
        m_CurrTrans.p[i] = trans[i];
        m_CurrTrans.eu[i] = trans[3+i];
    }

    for(i=0;i<MAX_AXIS_NUM;i++)
    {
        if(i < USE_AXIS_NUM)
            m_CurrJoint.joint[i] = joint[i];
        else
            m_CurrJoint.joint[i] = 0.0;
    }


}
void dialog_sasang_motion_teach::Display_Speed(float speed)
{
    QString str;
    str.sprintf(FORMAT_SPEED, speed);
    ui->tbSpd->setText(str);
}
void dialog_sasang_motion_teach::Display_Accuracy(float accuracy)
{
    QString str;
    str.sprintf(FORMAT_POS, accuracy);
    ui->tbAccu->setText(str);
}
void dialog_sasang_motion_teach::Display_Output(float output)
{
    if(pxmOutput.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon5-12.png");   // vac.
        pxmOutput.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon6-10.png");   // chuck.
        pxmOutput.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon6-9.png");   // grip.
        pxmOutput.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon6-1.png");   // nipper.
        pxmOutput.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon6-3.png");   // r grip.
        pxmOutput.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon6-5.png");   // user.
        pxmOutput.append(QPixmap::fromImage(img));
    }

    int index;

    switch((int)output)
    {
    case HyOutput::VAC1:
    case HyOutput::VAC2:
    case HyOutput::VAC3:
    case HyOutput::VAC4:
        index = 0;
        break;
    case HyOutput::CHUCK:
        index = 1;
        break;
    case HyOutput::GRIP:
        index = 2;
        break;
    case HyOutput::NIPPER:
        index = 3;
        break;
    case HyOutput::RGRIP:
        index = 4;
        break;
    case HyOutput::USER1:
    case HyOutput::USER2:
    case HyOutput::USER3:
    case HyOutput::USER4:
    case HyOutput::USER5:
    case HyOutput::USER6:
    case HyOutput::USER7:
    case HyOutput::USER8:
        index = 5;
        break;

    default:
        index = 0;
        break;
    }

    ui->tbOutIcon->setPixmap(pxmOutput[index]);
    ui->tbOut->setText(Output->GetUiName((int)output - 1));
}
void dialog_sasang_motion_teach::Display_Type(float type)
{
    if(pxmType.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon2-1.png");   // off
        pxmType.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon2-2.png");   // on
        pxmType.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon2-3.png");   // pulse
        pxmType.append(QPixmap::fromImage(img));
    }

    ui->tbTypeIcon->setPixmap(pxmType[(int)type]);

    QString str;
    if((int)type == HyUserData::TYPE_ON)
        str = tr("ON");
    else if((int)type == HyUserData::TYPE_PULSE)
        str = tr("PULSE");
    else
        str = tr("OFF");

    ui->wigTime->setEnabled((int)type == HyUserData::TYPE_PULSE);
    ui->tbType->setText(str);
}
void dialog_sasang_motion_teach::Display_Time(float time)
{
    QString str;
    str.sprintf(FORMAT_TIME, time);
    ui->tbTime->setText(str);

    ui->wigTime->setEnabled(true);
}
void dialog_sasang_motion_teach::Display_SetTitle(QString command)
{
    if(command == Sasang->Motion->toStr(HySasang_Motion::cir))
        ui->tbSetTitle->setText(tr("Middle"));
    else
        ui->tbSetTitle->setText(tr("Set"));

    m_bSetTitle = false;
}

void dialog_sasang_motion_teach::Display_AddSpeed(float speed)
{
    QString str;
    str.sprintf(FORMAT_SPEED, speed);
    ui->tbAddSpd->setText(str);
}
void dialog_sasang_motion_teach::Display_AddAccuracy(float accuracy)
{
    QString str;
    str.sprintf(FORMAT_POS, accuracy);
    ui->tbAddAccu->setText(str);
}


void dialog_sasang_motion_teach::onSetTitle()
{
    if(m_DataNew.command != Sasang->Motion->toStr(HySasang_Motion::cir))
        return;

    if(!m_bSetTitle)
    {
        m_bSetTitle = true;
        Display_SetPos(m_DataNew.trans[1], m_DataNew.joint[1]);
        ui->tbSetTitle->setText(tr("End"));
    }
    else
    {
        m_bSetTitle = false;
        Display_SetPos(m_DataNew.trans[0], m_DataNew.joint[0]);
        ui->tbSetTitle->setText(tr("Middle"));
    }

}

void dialog_sasang_motion_teach::onAddSpeed()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbAddSpdTitle->text());
    np->m_numpad->SetNum((double)m_AddSpeed);
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);

    if(np->exec() == QDialog::Accepted)
        m_AddSpeed = (float)np->m_numpad->GetNumDouble();
}
void dialog_sasang_motion_teach::onAddAccuracy()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbAddAccuTitle->text());
    np->m_numpad->SetNum((double)m_AddAccuracy);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(MAX_POS_VALUE);
    np->m_numpad->SetSosuNum(SOSU_POS);

    if(np->exec() == QDialog::Accepted)
        m_AddAccuracy = (float)np->m_numpad->GetNumDouble();
}

void dialog_sasang_motion_teach::onSpeed()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbSpdTitle->text());
    np->m_numpad->SetNum(m_DataNew.speed_output);
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);

    if(np->exec() == QDialog::Accepted)
    {
        m_DataNew.speed_output = (float)np->m_numpad->GetNumDouble();
        Display_Data_Write(m_DataNew);
    }
}
void dialog_sasang_motion_teach::onAccuracy()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbAccuTitle->text());
    np->m_numpad->SetNum(m_DataNew.accuracy_input);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(MAX_POS_VALUE);
    np->m_numpad->SetSosuNum(SOSU_POS);

    if(np->exec() == QDialog::Accepted)
    {
        m_DataNew.accuracy_input = (float)np->m_numpad->GetNumDouble();
        Display_Data_Write(m_DataNew);
    }
}
void dialog_sasang_motion_teach::onCurrent()
{
    if(!Sasang->Motion->IsMoveCommand(m_DataNew.command))
        return;

    int buf_index = 0;
    if(m_DataNew.command == Sasang->Motion->toStr(HySasang_Motion::cir))
    {
        if(m_bSetTitle)
            buf_index = 1;
    }

    m_DataNew.trans[buf_index] = m_CurrTrans;
    m_DataNew.joint[buf_index] = m_CurrJoint;

    // for start & end  setting.
    if(m_nSelectedIndex == 0
    || m_nSelectedIndex == (Sasang->Motion->GetCount()-1))
        m_DataNew.speed_output = m_AddSpeed;

    Display_Data_Write(m_DataNew);
}
void dialog_sasang_motion_teach::onOutput()
{
    if(dig_sasang_output == 0)
        dig_sasang_output = new dialog_sasang_output();

    dig_sasang_output->Init(m_DataNew);
    if(dig_sasang_output->exec() == QDialog::Accepted)
    {
        dig_sasang_output->Get(m_DataNew);

        if(!Save())
        {
            // error case
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("Error")));
            msg->Message(tr("Save Error!"),
                         tr("Please, Try again!"));
            msg->exec();
            return;
        }

        Display_Data_Write(m_DataNew);
    }
}

bool dialog_sasang_motion_teach::IsSame(ST_SSMOTION_DATA old_data, ST_SSMOTION_DATA new_data)
{
    int i;

    if(old_data.command != new_data.command)
        return false;
    for(i=0;i<3;i++)
    {
        if(old_data.trans[0].p[i] != new_data.trans[0].p[i])
            return false;
        if(old_data.trans[1].p[i] != new_data.trans[1].p[i])
            return false;

        if(old_data.trans[0].eu[i] != new_data.trans[0].eu[i])
            return false;
        if(old_data.trans[1].eu[i] != new_data.trans[1].eu[i])
            return false;
    }

    for(i=0;i<USE_AXIS_NUM;i++)
    {
        if(old_data.joint[0].joint[i] != new_data.joint[0].joint[i])
            return false;
        if(old_data.joint[1].joint[i] != new_data.joint[1].joint[i])
            return false;
    }

    if(old_data.speed_output != new_data.speed_output)
        return false;
    if(old_data.accuracy_input != new_data.accuracy_input)
        return false;
    if(old_data.type != new_data.type)
        return false;
    if(old_data.time != new_data.time)
        return false;

    return true;
}

void dialog_sasang_motion_teach::Display_SaveEnable()
{
    bool bSame = IsSame(m_DataOld, m_DataNew);

    ui->wigSave->setEnabled(!bSame);
}

bool dialog_sasang_motion_teach::Save()
{
    if(Sasang->Motion->Data2Raw(m_DataNew)
    && Sasang->Motion->Data2Prog(m_DataNew))
    {
        Sasang->Motion->MotionList[m_nSelectedIndex] = m_DataNew;
        m_DataOld = m_DataNew;
        return true;
    }

    return false;
}
void dialog_sasang_motion_teach::onSave()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(QString(tr("CONFIRM")));
    conf->Message(QString(tr("Would you want to SAVE?")),
                  Sasang->Motion->GetName(m_nSelectedIndex));
    if(conf->exec() == QDialog::Rejected)
        return;

    if(!Save())
    {
        // error case
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Save Error!"),
                     tr("Please, Try again!"));
        msg->exec();
        return;
    }

    Display_Data_Write(m_DataNew);
}

void dialog_sasang_motion_teach::onAdd()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtAddPic.indexOf(btn);
    if(index < 0) return;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    // make data.
    ST_SSMOTION_DATA data;
    Sasang->Motion->Default(data);

    switch(index)
    {
    case HySasang_Motion::p2p:
        data.command = Sasang->Motion->toStr(HySasang_Motion::p2p);
        data.trans[0] = m_CurrTrans;
        data.joint[0] = m_CurrJoint;
        data.speed_output = m_AddSpeed;
        break;

    case HySasang_Motion::con:
        data.command = Sasang->Motion->toStr(HySasang_Motion::con);
        data.trans[0] = m_CurrTrans;
        data.joint[0] = m_CurrJoint;
        data.speed_output = m_AddSpeed;
        data.accuracy_input = m_AddAccuracy;
        break;

    case HySasang_Motion::cir:
        // another method
        if(!m_bAddCirEnd)
        {
            msg->SetColor(dialog_message::SKYBLUE);
            msg->Title(QString(tr("Circle Set")));
            msg->Message(tr("Seting Circle Middle Point"),
                         tr("Next, Setting Circle End Point"));
            msg->exec();

            data.command = Sasang->Motion->toStr(HySasang_Motion::cir);
            data.trans[0] = m_CurrTrans;
            data.joint[0] = m_CurrJoint;
            data.speed_output = m_AddSpeed;

            m_CirData = data;
            m_bAddCirEnd = true;
            return;
        }
        else
        {
            msg->SetColor(dialog_message::SKYBLUE);
            msg->Title(QString(tr("Circle Set")));
            msg->Message(tr("Finish Setting Circle!"),
                         tr("Seting Circle End Point"));
            msg->exec();

            m_CirData.trans[1] = m_CurrTrans;
            m_CirData.joint[1] = m_CurrJoint;

            data = m_CirData;
            m_bAddCirEnd = false;
        }
        break;

    case HySasang_Motion::out:
        data.command = Sasang->Motion->toStr(HySasang_Motion::out);

        if(dig_sasang_output == 0)
            dig_sasang_output = new dialog_sasang_output();

        dig_sasang_output->Init(data);
        if(dig_sasang_output->exec() == QDialog::Rejected)
            return;

        dig_sasang_output->Get(data);
        break;

    case HySasang_Motion::wtt:
        data.command = Sasang->Motion->toStr(HySasang_Motion::wtt);

        if(dig_sasang_output == 0)
            dig_sasang_output = new dialog_sasang_output();

        dig_sasang_output->Init(data);
        if(dig_sasang_output->exec() == QDialog::Rejected)
            return;

        dig_sasang_output->Get(data);
        break;
    }

    // Data2Raw, Data2Prog
    if(Sasang->Motion->Data2Raw(data)
    && Sasang->Motion->Data2Prog(data))
    {
        Sasang->Motion->Add(data);

        m_nSelectedIndex = Sasang->Motion->GetCount() - 2;
        if(Sasang->Motion->GetCount() > (m_nStartIndex + m_vtTbNo.count()))
            m_nStartIndex = Sasang->Motion->GetCount() - m_vtTbNo.count();

        Redraw_List(m_nStartIndex);
    }
    else
    {
        // error case
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Add Error!"),
                     tr("Please, Try again!"));
        msg->exec();
    }

    if(index == HySasang_Motion::p2p
    || index == HySasang_Motion::con)
    {
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(m_vtAdd[index]->text());
        msg->Message(tr("Add this motion!"),
                     m_vtAdd[index]->text());
        msg->CloseTime(1000);
        msg->exec();
    }

}

void dialog_sasang_motion_teach::onDel()
{
    if(Sasang->Motion->GetCount() <= 0)
        return;
    if(m_nSelectedIndex <= 0)
        return;
    if(m_nSelectedIndex == (Sasang->Motion->GetCount()-1))
        return;


    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(QString(tr("CONFIRM")));
    conf->Message(QString(tr("Would you want to DELETE?"))
                  ,Sasang->Motion->GetName(m_nSelectedIndex));
    if(conf->exec() == QDialog::Rejected)
        return;

    if(!Sasang->Motion->Del(m_nSelectedIndex))
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("DELETE Error!"),
                     tr("Please, Try again!"));
        msg->exec();
        return;
    }

    if(m_nSelectedIndex >= Sasang->Motion->GetCount())
        m_nSelectedIndex = Sasang->Motion->GetCount() - 1;

    if(m_nStartIndex + m_vtTbNo.size() > Sasang->Motion->GetCount())
    {
        if(m_nStartIndex > 0)
            m_nStartIndex--;
    }

    Redraw_List(m_nStartIndex);
}

void dialog_sasang_motion_teach::Enable_CirEnd(bool bEnd)
{
    int i;
    if(bEnd)
    {
        for(i=0;i<m_vtAddPic.count();i++)
        {
            if(i != HySasang_Motion::cir)
            {
                m_vtAddPic[i]->setEnabled(false);
                m_vtAdd[i]->setEnabled(false);
            }
            else
            {
                m_vtAddPic[i]->setEnabled(true);
                m_vtAdd[i]->setEnabled(true);
            }

        }

        ui->btnDelPic->setEnabled(false);
        ui->btnDel->setEnabled(false);
        ui->btnDelIcon->setEnabled(false);
    }
    else
    {
        for(i=0;i<m_vtAddPic.count();i++)
        {
            m_vtAddPic[i]->setEnabled(true);
            m_vtAdd[i]->setEnabled(true);
        }

        ui->btnDelPic->setEnabled(true);
        ui->btnDel->setEnabled(true);
        ui->btnDelIcon->setEnabled(true);
    }
}

void dialog_sasang_motion_teach::Display_Editing(bool bEditing)
{
    ui->wigAdd->setEnabled(!bEditing);
}
