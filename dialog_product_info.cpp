#include "dialog_product_info.h"
#include "ui_dialog_product_info.h"

dialog_product_info::dialog_product_info(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_product_info)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->tbTargetPic,SIGNAL(mouse_release()),this,SLOT(onTarget()));
    connect(ui->tbTarget,SIGNAL(mouse_press()),ui->tbTargetPic,SLOT(press()));
    connect(ui->tbTarget,SIGNAL(mouse_release()),ui->tbTargetPic,SLOT(release()));

    connect(ui->tbCavityPic,SIGNAL(mouse_release()),this,SLOT(onCavity()));
    connect(ui->tbCavity,SIGNAL(mouse_press()),ui->tbCavityPic,SLOT(press()));
    connect(ui->tbCavity,SIGNAL(mouse_release()),ui->tbCavityPic,SLOT(release()));

    connect(ui->btnCntHoldPic,SIGNAL(mouse_release()),this,SLOT(onCntHold()));
    connect(ui->btnCntHold,SIGNAL(mouse_press()),ui->btnCntHoldPic,SLOT(press()));
    connect(ui->btnCntHold,SIGNAL(mouse_release()),ui->btnCntHoldPic,SLOT(release()));

    connect(ui->btnCntClearPic,SIGNAL(mouse_release()),this,SLOT(onClear()));
    connect(ui->btnCntClear,SIGNAL(mouse_press()),ui->btnCntClearPic,SLOT(press()));
    connect(ui->btnCntClear,SIGNAL(mouse_release()),ui->btnCntClearPic,SLOT(release()));
    connect(ui->btnCntClearIcon,SIGNAL(mouse_press()),ui->btnCntClearPic,SLOT(press()));
    connect(ui->btnCntClearIcon,SIGNAL(mouse_release()),ui->btnCntClearPic,SLOT(release()));



}

dialog_product_info::~dialog_product_info()
{
    delete ui;
}
void dialog_product_info::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_product_info::showEvent(QShowEvent *)
{
    Update();
    timer->start();
}
void dialog_product_info::hideEvent(QHideEvent *)
{
    timer->stop();
}

void dialog_product_info::onTimer()
{
    Refresh_Data();
}

void dialog_product_info::Update()
{
    float data;
    QString str;

    vars.append(HyRecipe::vTargetProd);
    vars.append(HyRecipe::vCavity);
    vars.append(HyRecipe::vCurrProd);
    vars.append(HyRecipe::vDischarge);
    vars.append(HyRecipe::vSample);
    vars.append(HyRecipe::vCountHold);

    if(Recipe->Gets(vars, datas))
    {
        // Target Product Qty.
        str.sprintf("%d", (int)datas[0]);
        ui->tbTarget->setText(str);

        // Cavity
        str.sprintf("%d", (int)datas[1]);
        ui->tbCavity->setText(str);

        // Total Cycle (calculated value)
        if(datas[1] > 0.0)
        {
            data = datas[0] / datas[1];
            str.sprintf("%.1f", data);
        }
        else
            str = QString("N/A");

        ui->tbTotalCycle->setText(str);

        // Current Product Qty.
        str.sprintf("%d", (int)datas[2]);
        ui->tbProdQty->setText(str);

        // Current Discharge Qty.
        str.sprintf("%d", (int)datas[3]);
        ui->tbDischargeQty->setText(str);

        // Current Sample Qty.
        str.sprintf("%d", (int)datas[4]);
        ui->tbSampleQty->setText(str);

        // Count Hold
        Display_CountHold(((int)datas[5] > 0));

    }

}

void dialog_product_info::Refresh_Data()
{
    QString str;
    QVector<float> _datas;
    QVector<HyRecipe::RECIPE_NUMBER> _vars;

    _vars.append(HyRecipe::vCurrProd);
    _vars.append(HyRecipe::vDischarge);
    _vars.append(HyRecipe::vSample);

    if(Recipe->Gets(_vars, _datas))
    {
        // Current Product Qty.
        str.sprintf("%d", (int)_datas[0]);
        ui->tbProdQty->setText(str);

        // Current Discharge Qty.
        str.sprintf("%d", (int)_datas[1]);
        ui->tbDischargeQty->setText(str);

        // Current Sample Qty.
        str.sprintf("%d", (int)_datas[2]);
        ui->tbSampleQty->setText(str);
    }
}

void dialog_product_info::Display_CountHold(bool bHold)
{
    ui->btnCntHoldIcon->setEnabled(bHold);
}

void dialog_product_info::onTarget()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbTargetName->text());
    np->m_numpad->SetNum((double)datas[0]);
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(9999.0);
    np->m_numpad->SetSosuNum(0);

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(HyRecipe::vTargetProd, (float)np->m_numpad->GetNumDouble()))
            Update();
    }

}
void dialog_product_info::onCavity()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbCavityName->text());
    np->m_numpad->SetNum((double)datas[1]);
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(99.0);
    np->m_numpad->SetSosuNum(0);

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(HyRecipe::vCavity, (float)np->m_numpad->GetNumDouble()))
            Update();
    }
}

void dialog_product_info::onCntHold()
{
    if(ui->btnCntHoldIcon->isEnabled())
    {
        if(Recipe->Set(HyRecipe::vCountHold, 0))
            Display_CountHold(false);
    }
    else
    {
        if(Recipe->Set(HyRecipe::vCountHold, 1))
            Display_CountHold(true);
    }
}

void dialog_product_info::onClear()
{
//    QVector<float> _datas;
//    QVector<HyRecipe::RECIPE_NUMBER> _vars;

//    _vars.append(HyRecipe::vCurrProd);
//    _datas.append(1);
//    _vars.append(HyRecipe::vDischarge);
//    _datas.append(2);
//    _vars.append(HyRecipe::vSample);
//    _datas.append(3);

//    if(Recipe->Sets(_vars, _datas))
//       Refresh_Data();

    Recipe->Set(HyRecipe::vCurrProd, 0);
    Recipe->Set(HyRecipe::vDischarge, 0);
    Recipe->Set(HyRecipe::vSample, 0);

    Refresh_Data();

}
