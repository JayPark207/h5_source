#include "hyeuromap.h"

HyEuromap::HyEuromap(HyRecipe* recipe, HyInput* input, HyOutput* output)
{
    m_Recipe = recipe;
    m_Input = input;
    m_Output = output;

    Init();
}

void HyEuromap::Init()
{
    Init_Module();
    Init_Signal();
}


void HyEuromap::Init_Module()
{
    ST_EUROMAP_MODULE mod_data;
    Module.clear();
    for(int i=0;i<MODULE_NUM;i++)
    {
        mod_data.index = i;
        Module.append(mod_data);
    }

    // COMMON
    Module[COMMON].Name = tr("Common");
    Module[COMMON].DataName.clear();
    Module[COMMON].DataName.append("emType");
    Module[COMMON].DataName.append("emUse");
    Module[COMMON].DataName.append("emSimRun");

    // OPERATION
    Module[OPERATION].Name = tr("Operation");
    Module[OPERATION].DataName.clear();
    Module[OPERATION].DataName.append("emAutoMode");
    Module[OPERATION].DataName.append("emCmdAuto");
    Module[OPERATION].DataName.append("emStatAuto");

    // SAFETY
    Module[SAFETY].Name = tr("Safety");
    Module[SAFETY].DataName.clear();
    Module[SAFETY].DataName.append("emStatImmEmo");
    Module[SAFETY].DataName.append("emEmoStop");
    Module[SAFETY].DataName.append("emSdMode");
    Module[SAFETY].DataName.append("emStatSd");
    Module[SAFETY].DataName.append("emSdStop");
    Module[SAFETY].DataName.append("sTakeout");
    Module[SAFETY].DataName.append("emSdSwitch");

    // MOLD
    Module[MOLD].Name = tr("Mold");
    Module[MOLD].DataName.clear();
    Module[MOLD].DataName.append("emMoldMode");
    Module[MOLD].DataName.append("emCmdTakeout");
    Module[MOLD].DataName.append("emCmdInMold");
    Module[MOLD].DataName.append("emStatMold");

    // EJECTOR
    Module[EJECTOR].Name = tr("Ejector");
    Module[EJECTOR].DataName.clear();
    Module[EJECTOR].DataName.append("emEjectMode");
    Module[EJECTOR].DataName.append("emSigEject");
    Module[EJECTOR].DataName.append("emStatEject");

    // CORE1
    Module[CORE1].Name = tr("Core 1");
    Module[CORE1].DataName.clear();
    Module[CORE1].DataName.append("emCoreMode1");
    Module[CORE1].DataName.append("emSigCore1");

    // CORE2
    Module[CORE2].Name = tr("Core 2");
    Module[CORE2].DataName.clear();
    Module[CORE2].DataName.append("emCoreMode2");
    Module[CORE2].DataName.append("emSigCore2");

    // add here...

}

void HyEuromap::Init_Signal()
{
    ST_EUROMAP_SIGNAL sig;
    OutRob.clear();

    // 만약 아이오 이름과 다르게 사용하고 싶을경우. sig.Name 에 다른 이름 입력.
    sig.IONum = 33;
    sig.Name = tr("Emergency");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 34;
    sig.Name = tr("Reserved");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 35;
    sig.Name = tr("Reserved");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 36;
    sig.Name = tr("Cycle Start");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0";
    OutRob.append(sig);

    sig.IONum = 37;
    sig.Name = tr("Robot Auto");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "1,2";
    OutRob.append(sig);

    sig.IONum = 38;
    sig.Name = tr("Enable Mold Close");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 39;
    sig.Name = tr("Enable Mold Open");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 40;
    sig.Name = tr("Enable Ejector Bwd");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 41;
    sig.Name = tr("Enable Ejector Fwd");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 42;
    sig.Name = tr("Enable Core1 to Pos1");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 43;
    sig.Name = tr("Enable Core1 to Pos2");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 44;
    sig.Name = tr("Enable Core2 to Pos1");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "1";
    OutRob.append(sig);

    sig.IONum = 45;
    sig.Name = tr("Enable Core2 to Pos2");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "1";
    OutRob.append(sig);

    sig.IONum = 46;
    sig.Name = tr("Enable Mold Full Open");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "1,2";
    OutRob.append(sig);

    sig.IONum = 47;
    sig.Name = tr("Reserved");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);

    sig.IONum = 48;
    sig.Name = tr("Reserved");
    //sig.Name = m_Output->GetName(sig.IONum);
    sig.Type = "0,1,2";
    OutRob.append(sig);


    //==========================================================//
    InImm.clear();

    sig.IONum = 33;
    sig.Name = tr("Emergency 1");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 34;
    sig.Name = tr("Emergency 2");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "1";
    InImm.append(sig);

    sig.IONum = 35;
    sig.Name = tr("Safety Device 1");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 36;
    sig.Name = tr("Safety Device 2");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "1";
    InImm.append(sig);

    sig.IONum = 37;
    sig.Name = tr("Enable Auto");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 38;
    sig.Name = tr("Mold Closed");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 39;
    sig.Name = tr("Mold Opened");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 40;
    sig.Name = tr("Ejector Bwd.");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 41;
    sig.Name = tr("Ejector Fwd.");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 42;
    sig.Name = tr("Core1 In Pos1");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 43;
    sig.Name = tr("Core1 In Pos2");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 44;
    sig.Name = tr("Core2 In Pos1");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "1";
    InImm.append(sig);

    sig.IONum = 45;
    sig.Name = tr("Core2 In Pos2");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "1";
    InImm.append(sig);

    sig.IONum = 46;
    sig.Name = tr("Mold Opening");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

    sig.IONum = 47;
    sig.Name = tr("Reject");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "1,2";
    InImm.append(sig);

    sig.IONum = 48;
    sig.Name = tr("Reserved");
    //sig.Name = m_Input->GetName(sig.IONum);
    sig.Type = "0,1,2";
    InImm.append(sig);

}

void HyEuromap::Get_SimRun(bool &run)
{
    float data;
    if(m_Recipe->Get(HyRecipe::emSimRun, &data))
    {
        if(data > 0.5)
            run = true;
        else
            run = false;
    }
}
void HyEuromap::Set_SimRun(bool run)
{
    m_Recipe->Set(HyRecipe::emSimRun, (float)run);
}
