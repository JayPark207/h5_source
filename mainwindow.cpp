#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QElapsedTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    timer.start();
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    //WatchDog Function.
    WatchDog = new QTimer(this);
    connect(WatchDog,SIGNAL(timeout()),this,SLOT(onWatchDog()));
    ReConnect = new QTimer(this);
    connect(ReConnect,SIGNAL(timeout()),this,SLOT(onReConnect()));
    m_StateReConnect = 0;

    tErrCheck = new QTimer(this);
    connect(tErrCheck,SIGNAL(timeout()),this,SLOT(onErrCheck()));
    m_bError=false;m_bErrorOld=false;

    tFirstRun = new QTimer(this);
    tFirstRun->setInterval(500);
    connect(tFirstRun,SIGNAL(timeout()),this,SLOT(onFirstRun()));

    tPendent = new QTimer(this);
    tPendent->setInterval(100);
    connect(tPendent,SIGNAL(timeout()),this,SLOT(onTimer_Pendent()));


    /** ====================================== **/
    // init classes
    Param           = new HyParam();
    Recipe          = new HyRecipe();
    Posi            = new HyPos(Recipe);
    Time            = new HyTime(Recipe);
    Input           = new HyInput();
    Output          = new HyOutput();
    UserData        = new HyUserData(Recipe);
    Error           = new HyError(Param);
    Status          = new HyStatus();
    RobotConfig     = new HyRobotConfig();
    TPonly          = new HyTPonly();
    Log             = new HyLog();
    BR              = new HyBackupRecovery();
    UseSkip         = new HyUseSkip(Param, Recipe);
    Mode            = new HyMode(Recipe);
    Language        = new HyLanguage();
    SoftSensor      = new HySoftSensor(Param, Recipe);
    Motors          = new HyEcatMotor();

    RM              = new HyRecipeManage2();
    Pendent         = new HyPendent();
    StepEdit        = new HyStepEdit();
    Sasang          = new HySasang();
    Euromap         = new HyEuromap(Recipe, Input, Output);
    Network         = new HyNetwork(Recipe);
    BigMode         = new HyBigMode(Mode);


    // Initial above class.
    Init_Values();
    Init_Forms();
    Init_Dialogs();

    Init_HomeBroken_ErrorCode();

    qDebug() << "mainwindow create end time =" << timer.elapsed() << "mesc";
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::showEvent(QShowEvent *)
{
    return;
}
void MainWindow::hideEvent(QHideEvent *)
{
    WatchDog->stop();
    ReConnect->stop();
    tErrCheck->stop();
    tPendent->stop();
}
void MainWindow::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);

        // change language.
        Param->Init_String();
        Posi->Init();
        Time->Init();
        Input->Init();
        Output->Init();
        Error->Init();
        Log->Init();
        UseSkip->Init();
        Mode->Init();
        StepEdit->m_StepData.Init();
        Sasang->Init_String();
        Euromap->Init();
        Network->Init();
        BigMode->Init();
    }
    QMainWindow::changeEvent(event);
}

void MainWindow::Init()
{
    // variable auto save mode => off. (because macro)
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setVariableAutoSave(false);
    pCon->setServoOnMode(SON_MODE_NORMAL);

    if(!Param->Read())
        return;

    if(RM->Initial(Param->Get(HyParam::RECIPE)))
    {
        Param->Set(HyParam::RECIPE, RM->GetNowRecipeNo());
        RM->GetNowRecipeName(m_strRecipeName); // go to init_variable
    }

    if(!UseSkip->Sync())
        qDebug() << "UseSkip Sync() Fail!!";

    if(!SoftSensor->Sync())
        qDebug() << "SoftSensor Sync() Fail!!";

    Error->Sync();

    // booting sequence....

    // sub_main start
    Init_Variable();
    Init_SubMain();

    if(Param->Get(HyParam::FIRST_SETTING).toInt() == 0)
    {
         StackedForm->setCurrentIndex(SPECIAL_SETTING_FORM);
    }
    else
    {

    }

    // default io update. only input
    gStop_IOUpdate();

    // pendent led all off
    Pendent->Led_L1();
    Pendent->Led_L2();
    Pendent->Led_L3();
    Pendent->Led_R1();
    Pendent->Led_R2();
    Pendent->Led_R3();

    // load errorcode
    if(!Error->Load_ErrorCode())
        qDebug() << "Fail to load errorcode!";

    tFirstRun->start();
    tUserLevel.start();
}

void MainWindow::Init_SubMain()
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    QStringList autorun, macro;
    QString str;
    int i;
    str = Param->Get(HyParam::SUBTASK_AUTORUN);
    autorun = str.split(",");
    if(autorun.count() != SUB_TASK_NUM)
    {
        autorun.clear();
        for(i=0;i<SUB_TASK_NUM;i++)
            autorun.append("0");
    }
    autorun[0] = "1"; // perfect fix.

    str = Param->Get(HyParam::SUBTASK_MACRO);
    macro = str.split(",");
    if(macro.count() != SUB_TASK_NUM)
    {
        macro.clear();
        for(i=0;i<SUB_TASK_NUM;i++)
            autorun.append("");
    }
    macro[0] = SUBMAIN_MACRO; // perfect fix

    for(i=0;i<SUB_TASK_NUM;i++)
    {
        if(autorun[i].toInt() > 0)
        {
            if(!pCon->findProgram(macro[i])) continue;

            ret = pCon->clearCurProgram(SUB_TASK + i);
            if(ret < 0)
                qDebug() << "fail to clearCurProgram! ret =" << ret << SUB_TASK+i << macro[i];
            ret = pCon->executeProgram(SUB_TASK + i, macro[i], -1);
            if(ret < 0)
                qDebug() << "fail to executeProgram! ret =" << ret << SUB_TASK+i << macro[i];
        }
    }

}

void MainWindow::Init_Variable()
{
    QStringList names;
    QList<cn_variant> datas;
    cn_variant var;

    names.clear(); datas.clear();

    names.append("bSubInit");
    var.type = CNVAR_FLOAT;
    var.val.f32 = 1.0;
    datas.append(var);

    names.append("sAutoRun");
    var.type = CNVAR_FLOAT;
    var.val.f32 = 0;
    datas.append(var);

    names.append("sAutoMode");
    var.type = CNVAR_FLOAT;
    var.val.f32 = 0;
    datas.append(var);

    names.append("$s_RecipeName");
    var.type = CNVAR_STRING;
    sprintf(var.val.str,"%s",m_strRecipeName.toAscii().data());
    datas.append(var);

    names.append("bTestMode");
    var.type = CNVAR_FLOAT;
    var.val.f32 = 0;
    datas.append(var);

    names.append("sNotUse");
    var.type = CNVAR_FLOAT;
    var.val.f32 = 0;
    datas.append(var);

    names.append("bRecipeChange");
    var.type = CNVAR_FLOAT;
    var.val.f32 = 0;
    datas.append(var);

    names.append("iNowRecipeNo");
    var.type = CNVAR_FLOAT;
    var.val.f32 = RM->GetNowRecipeNo().toFloat();
    datas.append(var);

    Recipe->SetVars(names, datas, false);

}

void MainWindow::onWatchDog()
{
    CNRobo* pCon = CNRobo::getInstance();
    if(!pCon->isConnected())
    {
        if(m_StateReConnect == 0)
        {
            m_StateReConnect = 10;
            qDebug() << "Detect disconnect, Run Re-connection!!";
        }
    }
    else // connected true.
    {
        Status->Update_Time();
    }
}

void MainWindow::onReConnect()
{
    CNRobo* pCon = CNRobo::getInstance();
    char pw[] = "corerobot";
    bool bok;

    switch(m_StateReConnect)
    {
    case 0:
        break;

    case 10:
        if(!pCon->isConnected()) break;

        qDebug() << "Success to Re-connect coreServer!";
        m_StateReConnect = 11;
        break;

    case 11:
        pCon->login(pw);
        qDebug() << "Re-Try to log-in coreServer!";
        m_StateReConnect = 12;
        break;

    case 12:
        pCon->getLoginStatus(&bok);
        if(!bok) break;

        qDebug() << "Re-Success to log-in coreServer!";
        m_StateReConnect = 13;
        break;

    case 13:
        pCon->lock(0);
        qDebug() << "Re-Try to get control ticket!";
        m_StateReConnect = 14;
        break;

    case 14:
        pCon->getLockStatus(&bok);
        if(!bok) break;

        qDebug() << "Re-Success to get control ticket!";
        m_StateReConnect = 15;
        break;

    case 15:
        pCon->resetError();
        pCon->resetEcatError();
        qDebug() << "Reset Error!";
        m_StateReConnect = 100;
        break;

    case 100:
        qDebug() << "Success to Init Connection!";
        m_StateReConnect = 0;
        break;
    }

}

void MainWindow::Init_Forms()
{
    QElapsedTimer ti;
    // Initial Top StackedWidget
    StackedTop = new QStackedWidget(this);
    StackedTop->setGeometry(0,0,MAX_WIDTH,TOP_HEIGHT);

    ti.start();
    topMain = new top_main(this);
    StackedTop->addWidget(topMain);
    qDebug() << "top_main =" << ti.elapsed() << "mesc";

    ti.start();
    topSpeed = new top_speed(this);
    StackedTop->addWidget(topSpeed);
    qDebug() << "top_speed =" << ti.elapsed() << "mesc";

    StackedTop->setCurrentIndex(TOP_MAIN);

    // Initial MainPage StackedWidget
    StackedForm = new QStackedWidget(this);
    StackedForm->setGeometry(0,TOP_HEIGHT,MAX_WIDTH,MAX_HEIGHT-TOP_HEIGHT);

    connect(StackedForm,SIGNAL(currentChanged(int)),this,SLOT(OnStatckedForm_CurrentChanged(int)));

    //0
    ti.start();
    pageMain2 = new page_main2(this);
    StackedForm->addWidget(pageMain2);
    qDebug() << "page_main2 =" << ti.elapsed() << "mesc";

    //1
    ti.start();
    frmAutoRun = new form_autorun(this);
    StackedForm->addWidget(frmAutoRun);
    qDebug() << "form_autorun =" << ti.elapsed() << "mesc";

    //2
    ti.start();
    frmSpecialSetting = new form_specialsetting(this);
    StackedForm->addWidget(frmSpecialSetting);
    qDebug() << "form_specialsetting =" << ti.elapsed() << "mesc";


    // setting start page.
    g_PreMainPage = MAIN2_PAGE;
    StackedForm->setCurrentIndex(MAIN2_PAGE);
}

void MainWindow::Init_Dialogs()
{
    QElapsedTimer ti;

    Dialogs.clear();

    // 0
    ti.start();
    digModeSelect = new dialog_mode_select();
    Dialogs.append(digModeSelect);
    qDebug() << "dialog_mode_select =" << ti.elapsed() << "mesc";

    // 1
    ti.start();
    digNumpad = new dialog_numpad();
    Dialogs.append(digNumpad);
    qDebug() << "dialog_numpad =" << ti.elapsed() << "mesc";

    // 2
    ti.start();
    digPosTeach = new dialog_position_teaching2();
    Dialogs.append(digPosTeach);
    qDebug() << "dialog_position_teaching2 =" << ti.elapsed() << "mesc";

    // 3
    ti.start();
    digConfirm = new dialog_confirm();
    Dialogs.append(digConfirm);
    qDebug() << "dialog_confirm =" << ti.elapsed() << "mesc";

    // 4
    ti.start();
    digMsg = new dialog_message();
    Dialogs.append(digMsg);
    qDebug() << "dialog_message =" << ti.elapsed() << "mesc";

    // 5
    ti.start();
    digKeyboard = new dialog_keyboard();
    Dialogs.append(digKeyboard);
    qDebug() << "dialog_keyboard =" << ti.elapsed() << "mesc";

    // 6
    ti.start();
    digHome = new dialog_home();
    Dialogs.append(digHome);
    qDebug() << "dialog_home =" << ti.elapsed() << "mesc";

    // 7
    ti.start();
    digError = new dialog_error();
    Dialogs.append(digError);
    qDebug() << "dialog_error =" << ti.elapsed() << "mesc";

    // 8
    ti.start();
    digUserLevel = new dialog_userlevel();
    Dialogs.append(digUserLevel);
    qDebug() << "dialog_userlevel =" << ti.elapsed() << "mesc";


}

void MainWindow::Init_Values()
{
    QElapsedTimer ti;
    ti.start();

    // init button image
    QImage img;
    QPixmap pxm;
    g_vtImgNoPress.clear();
    g_vtPxmNoPress.clear();
    g_vtPxmPress.clear();
    g_vtImgPress.clear();

    img.load(":/image/image/button_circle_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_circle_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_longrect_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_longrect_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_longrect2_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_longrect2_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_rect1_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_rect1_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_rect2_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_rect2_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_midrect_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_midrect_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_midrect2_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_midrect_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_midrect3_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_midrect_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_tallrect_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_tallrect_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_rect3_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_rect3_4.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_circle2_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_circle2_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_midrect4_0.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_midrect4_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    img.load(":/image/image/button_midrect4_1.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmNoPress.append(pxm);
    g_vtImgNoPress.append(pxm.toImage());
    img.load(":/image/image/button_midrect4_2.png");
    pxm = QPixmap::fromImage(img);
    g_vtPxmPress.append(pxm);
    g_vtImgPress.append(pxm.toImage());

    qDebug() << "MainWindow::Init_Values() =" << ti.elapsed() << "mesc";

    gLabelWidget = 0;
    g_nKeyCommand_Press = KEYCMD_NONE;
    g_nKeyCommand_Release = KEYCMD_NONE;
    dig_home2 = 0;
    dig_jog = 0;
    m_bLed1=false;m_bLed1_old=true;
    m_bLed2=false;m_bLed2_old=true;
    m_bLed3=false;m_bLed3_old=true;

}

void MainWindow::OnStatckedForm_CurrentChanged(int index)
{
    switch(index)
    {
    case MAIN2_PAGE:
    case AUTORUN_FORM:
        StackedForm->setGeometry(0,TOP_HEIGHT,MAX_WIDTH,MAX_HEIGHT-TOP_HEIGHT);
        StackedTop->show();
        break;

    default:
        StackedForm->setGeometry(0,0,MAX_WIDTH,MAX_HEIGHT);
        StackedTop->hide();
        break;
    }
}

void MainWindow::Init_HomeBroken_ErrorCode()
{
    m_vtHomeBroken_ErrorCode.clear();

    m_vtHomeBroken_ErrorCode.append(-1010);
    m_vtHomeBroken_ErrorCode.append(-1505); // motor error

    //m_vtHomeBroken_ErrorCode.append(-9010); // not use homebroken
    //m_vtHomeBroken_ErrorCode.append(-9020); // not use homebroken
    //m_vtHomeBroken_ErrorCode.append(-9030); // safety device error, not use homebroken

    m_vtHomeBroken_ErrorCode.append(-9050);
    m_vtHomeBroken_ErrorCode.append(-9060);
    m_vtHomeBroken_ErrorCode.append(-9061);

    // add here...
}

bool MainWindow::IsHomeBroken(int err_code)
{
    if(m_vtHomeBroken_ErrorCode.indexOf(err_code) < 0)
        return false;
    return true;
}

void MainWindow::onErrCheck()
{
    m_bError = Status->bError;

    if((m_bError != m_bErrorOld))
    {
        if(m_bError)
        {
            digError->m_bNewError = true;
            digError->exec();

            if(IsHomeBroken(Error->GetCode(0)))
                frmAutoRun->BrokenHome();
        }
        else
        {
            Status->SetErrCode(0);
        }
    }
    m_bErrorOld = m_bError;
}

void MainWindow::onFirstRun()
{
    // user log-in first execution.
    if(tUserLevel.hasExpired(200))
    {
       digUserLevel->exec(); // modal
       tFirstRun->stop();

       // pendent left key enable!!
       g_nKeyCommand_Press = KEYCMD_NONE;
       g_nKeyCommand_Release = KEYCMD_NONE;
       tPendent->start();
    }
}


/// About Pendent (Key & Led) Control

void MainWindow::onTimer_Pendent()
{
    // Press
    if(g_nKeyCommand_Press > KEYCMD_NONE)
    {
        switch (g_nKeyCommand_Press)
        {
        default:
            qDebug() << g_nKeyCommand_Press;
            g_nKeyCommand_Press = KEYCMD_NONE;
            break;
        }
    }

    // Release
    if(g_nKeyCommand_Release > KEYCMD_NONE)
    {  
        switch (g_nKeyCommand_Release)
        {
        case KEYCMD_L_LEFT:

            Key_OpenHome();

            qDebug() << "KEYCMD_L_LEFT";
            g_nKeyCommand_Release = KEYCMD_NONE;
            break;

        case KEYCMD_L_UP:

            Key_Speed(true);

            qDebug() << "KEYCMD_L_UP";
            g_nKeyCommand_Release = KEYCMD_NONE;
            break;

        case KEYCMD_L_RIGHT:

            Key_OpenAuto();

            qDebug() << "KEYCMD_L_RIGHT";
            g_nKeyCommand_Release = KEYCMD_NONE;
            break;

        case KEYCMD_L_DOWN:

            Key_Speed(false);

            qDebug() << "KEYCMD_L_DOWN";
            g_nKeyCommand_Release = KEYCMD_NONE;
            break;

        case KEYCMD_L_BTN1:

            Key_StartPause();

            qDebug() << "KEYCMD_L_BTN1";
            g_nKeyCommand_Release = KEYCMD_NONE;
            break;

        case KEYCMD_L_BTN2:

            Key_Reset();

            qDebug() << "KEYCMD_L_BTN2";
            g_nKeyCommand_Release = KEYCMD_NONE;
            break;

        case KEYCMD_L_BTN3:

            Key_Jog(!IsOpenDialog("dialog_jog"));

            qDebug() << "KEYCMD_L_BTN3";
            g_nKeyCommand_Release = KEYCMD_NONE;
            break;

        default:
            qDebug() << g_nKeyCommand_Release;
            g_nKeyCommand_Release = KEYCMD_NONE;
            break;
        }
    }

    Process_PendentLED();
}

bool MainWindow::ActiveModalClose(int check_max_num)
{
    QWidget* widget = 0;
    bool bOk = true;

    for(int i=0;i<check_max_num;i++)
    {
        widget = QApplication::activeModalWidget();

        if(widget != 0)
        {
            qDebug() << widget->objectName();
            bOk = widget->close();
        }
        else
            return true;
    }

    return bOk;
}

void MainWindow::Key_Reset()
{
#ifdef _H6_
    if(!Status->bError)
    {
        Capture();
        return;
    }
#else
    //if(IsMacroRun()) return; // ignore for running -> warnning case.
    if(!Status->bError) return;
#endif
    if(IsOpenDialog("dialog_robot_notuse")) return;

    dialog_delaying* dig = new dialog_delaying();
    CNRobo* pCon = CNRobo::getInstance();
    pCon->resetEcatError();
    dig->Init(500);
    dig->exec();
    pCon->resetError();
    pCon->clearWarningCode();
}

void MainWindow::Key_OpenAuto()
{
    if(IsMacroRun()) return;

    if(gGetMainPage() == AUTORUN_FORM) return;

    if(IsOpenDialog("dialog_robot_notuse")) return;

    dialog_delaying* dig = new dialog_delaying();

    ActiveModalClose(10);

    dig->Init(500);
    dig->exec();

    gSetMainPage(AUTORUN_FORM);
}
void MainWindow::Key_OpenHome()
{
    if(IsMacroRun()) return;
    if(IsOpenDialog("dialog_robot_notuse")) return;

    dialog_delaying* dig = new dialog_delaying();

    if(gGetMainPage() == AUTORUN_FORM)
    {
        if(IsOpenDialog("dialog_home2"))
        {
            return;
        }
        else
        {
            ActiveModalClose(10);
            dig->Init(500);
            dig->exec();

            frmAutoRun->m_bKeyHome = true;
        }
    }
    else
    {

        if(IsOpenDialog("dialog_home2"))
        {
            ActiveModalClose(1);
        }
        else
        {
            ActiveModalClose(10);
            //gSetMainPage(MAIN2_PAGE);
            dig->Init(500);
            dig->exec();

            if(dig_home2 == 0)
                dig_home2 = new dialog_home2();
            dig_home2->open();
        }
    }

}

bool MainWindow::IsOpenDialog(QString dialog_name)
{
    QWidget* widget = 0;
    widget = QApplication::activeModalWidget();
    if(widget != 0)
    {
        if(widget->objectName() == dialog_name)
            return true;
    }
    return false;
}

void MainWindow::Key_Jog(bool onoff)
{
    if(IsMacroRun()) return;
    if(IsOpenDialog("dialog_robot_notuse")) return;

    if(onoff && Status->bJog) return;

    if(IsOpenDialog("dialog_home2"))
    {
        if(gGetMainPage() == AUTORUN_FORM)
            return;
    }

    dialog_delaying* dig = new dialog_delaying();

    if(onoff)
    {
        ActiveModalClose(10);
        gSetMainPage(MAIN2_PAGE);

        dig->Init(500);
        dig->exec();

        if(dig_jog == 0)
            dig_jog = new dialog_jog();
        dig_jog->open();
    }
    else
    {
        if(IsOpenDialog("dialog_jog"))
            ActiveModalClose(1);
    }
}

void MainWindow::Key_StartPause()
{
    if(IsOpenDialog("dialog_robot_notuse")) return;

    QWidget* widget = 0;
    widget = QApplication::activeModalWidget();

    if(widget != 0)
    {
        if(widget->objectName() == "dialog_home2")
        {
            dialog_home2* _home2 = (dialog_home2*)widget;
            _home2->onMoveHome();
            return;
        }
        else if(widget->objectName() == "dialog_home")
        {
            dialog_home* _home = (dialog_home*)widget;
            _home->onMoveHome();
            return;
        }
        else if(gGetMainPage() == AUTORUN_FORM)
        {
            frmAutoRun->onRun();
            return;
        }
    }
    else
    {
        if(gGetMainPage() == AUTORUN_FORM)
        {
            frmAutoRun->onRun();
            return;
        }
    }

#ifdef _H6_
    if(Status->bJog)
    {
        // control tool coordinate.
        if(!UseSkip->Get(HyUseSkip::TOOL_COORD))
        {
            qDebug() << "Disabled Tool Coordinate!";
        }
        else
        {
            if(gGetJogType() == JOG_TYPE_HY)
            {
                gSetJogType(JOG_TYPE_HY_TOOL);
                qDebug() << "JOG_TYPE_HY_TOOL";
            }
            else if(gGetJogType() == JOG_TYPE_HY_TOOL)
            {
                gSetJogType(JOG_TYPE_HY);
                qDebug() << "JOG_TYPE_HY";
            }
        }
    }
#endif

}

void MainWindow::Key_Speed(bool updown)
{
    if(IsOpenDialog("dialog_robot_notuse")) return;

    CNRobo* pCon = CNRobo::getInstance();
    float fSpeed;
    int nSpeed;

    if(Status->bJog)
    {
        if(pCon->getJogSpeed(0, fSpeed) < 0)
            return;

        nSpeed = (int)((fSpeed * 100.0)+0.5);
        if(updown)
            nSpeed += JUMP_JOG_SPEED;
        else
            nSpeed -= JUMP_JOG_SPEED;

        if(nSpeed > MAX_JOG_SPEED)
            nSpeed = MAX_JOG_SPEED;
        else if(nSpeed < MIN_JOG_SPEED)
            nSpeed = MIN_JOG_SPEED;

        fSpeed = (float)nSpeed/100.0;
        pCon->setJogSpeed(0, fSpeed);
    }
    else
    {
        if(IsOpenDialog("dialog_stepedit")) // for step test run speed.
            return;
        if(IsOpenDialog("dialog_sasang_pattern"))
            return;
        if(IsOpenDialog("dialog_sasang_motion"))
            return;

        nSpeed = Status->nMainSpeed;

        if(updown)
            nSpeed += JUMP_MAIN_SPEED;
        else
            nSpeed -= JUMP_MAIN_SPEED;

        if(nSpeed > MAX_MAIN_SPEED)
            nSpeed = MAX_MAIN_SPEED;
        else if(nSpeed < MIN_MAIN_SPEED)
            nSpeed = MIN_MAIN_SPEED;

        Param->Set(HyParam::MAIN_SPEED, nSpeed);
        fSpeed = (float)nSpeed;
        pCon->setSpeed(fSpeed);
    }
}

bool MainWindow::IsMacroRun()
{
    if(Status->MainTask == CNR_TASK_RUNNING)
        return true;
    return false;
}

void MainWindow::Process_PendentLED()
{
    QWidget* widget = 0;

    // LED1 (Start/Pause)
    m_bLed1 = (Status->MainTask == CNR_TASK_RUNNING);
    if(m_bLed1 != m_bLed1_old)
    {
        widget = QApplication::activeModalWidget();

        if(m_bLed1)
        {
            if(widget == 0)
            {
                if(gGetMainPage() == AUTORUN_FORM)
                {
                    Pendent->Led_L1(CNR_LED_COLOR_BLUE);
                    Recipe->Set(HyRecipe::sAutoRun, 1, false);
                    Log->Set(HY_INFO_LOG, "AutoRun Start");
                }
            }
            else
            {
                if(widget->objectName() == "dialog_home"
                || widget->objectName() == "dialog_home2")
                    Pendent->Led_L1(CNR_LED_COLOR_BLUE);
                else if(gGetMainPage() == AUTORUN_FORM)
                {
                    Pendent->Led_L1(CNR_LED_COLOR_BLUE);
                    Recipe->Set(HyRecipe::sAutoRun, 1, false);
                    Log->Set(HY_INFO_LOG, "AutoRun Start");
                }
            }
        }
        else
        {
            Pendent->Led_L1();

            if(widget == 0)
            {
                if(gGetMainPage() == AUTORUN_FORM)
                {
                    Recipe->Set(HyRecipe::sAutoRun, 0, false);
                    Log->Set(HY_INFO_LOG, "AutoRun Stop");
                }
            }
            else
            {
                if(widget->objectName() != "dialog_home"
                && widget->objectName() != "dialog_home2"
                && gGetMainPage() == AUTORUN_FORM)
                {
                    Recipe->Set(HyRecipe::sAutoRun, 0, false);
                    Log->Set(HY_INFO_LOG, "AutoRun Stop");
                }
            }
        }
    }
    m_bLed1_old = m_bLed1;


    // LED2 (Reset -> Error)
    m_bLed2 = Status->bError;
    if(m_bLed2 != m_bLed2_old)
    {
        if(m_bLed2)
            Pendent->Led_L2(CNR_LED_COLOR_RED);
        else
        {
            Pendent->Led_L2();
            if(IsOpenDialog("dialog_error"))
                ActiveModalClose(1);
            else if(IsOpenDialog("dialog_error_list"))
                ActiveModalClose(2);
        }
    }
    m_bLed2_old = m_bLed2;

    // LED3 (Jog)
    m_bLed3 = Status->bJog;
    if(m_bLed3 != m_bLed3_old)
    {
        if(m_bLed3)
            Pendent->Led_L3(CNR_LED_COLOR_MAGENTA);
        else
            Pendent->Led_L3();
    }
    m_bLed3_old = m_bLed3;

}


