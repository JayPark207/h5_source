#include "dialog_mode_entry.h"
#include "ui_dialog_mode_entry.h"

dialog_mode_entry::dialog_mode_entry(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_mode_entry)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtEntryPic.clear();m_vtEntry.clear();m_vtEntryIcon.clear();
    m_vtEntryPic.append(ui->btnEntryPic);  m_vtEntry.append(ui->btnEntry);  m_vtEntryIcon.append(ui->btnEntryIcon);
    m_vtEntryPic.append(ui->btnEntryPic_2);m_vtEntry.append(ui->btnEntry_2);m_vtEntryIcon.append(ui->btnEntryIcon_2);

    int i;
    for(i=0;i<m_vtEntryPic.count();i++)
    {
        connect(m_vtEntryPic[i],SIGNAL(mouse_release()),this,SLOT(onEntry()));
        connect(m_vtEntry[i],SIGNAL(mouse_press()),m_vtEntryPic[i],SLOT(press()));
        connect(m_vtEntry[i],SIGNAL(mouse_release()),m_vtEntryPic[i],SLOT(release()));
        connect(m_vtEntryIcon[i],SIGNAL(mouse_press()),m_vtEntryPic[i],SLOT(press()));
        connect(m_vtEntryIcon[i],SIGNAL(mouse_release()),m_vtEntryPic[i],SLOT(release()));
    }

    // init
    dig_mode_detail = 0;
    dig_mode_easy = 0;
}

dialog_mode_entry::~dialog_mode_entry()
{
    delete ui;
}
void dialog_mode_entry::showEvent(QShowEvent *)
{

}
void dialog_mode_entry::hideEvent(QHideEvent *)
{

}
void dialog_mode_entry::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_mode_entry::onClose()
{
    emit accept();
}

void dialog_mode_entry::onEntry()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtEntryPic.indexOf(btn);
    if(index < 0) return;

    switch (index) {
    case 0:
        if(dig_mode_easy == 0)
            dig_mode_easy = new dialog_mode_easy();
        dig_mode_easy->exec();
        break;
    case 1:
        if(dig_mode_detail == 0)
            dig_mode_detail = new dialog_mode_detail();

        dig_mode_detail->Set_Readonly(false);
        dig_mode_detail->exec();
        break;
    }

    onClose();
}
