#ifndef DIALOG_TASK_CONTROL_H
#define DIALOG_TASK_CONTROL_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_keyboard.h"
#include "dialog_message.h"
#include "dialog_confirm.h"

#include "dialog_varmonitor.h"

namespace Ui {
class dialog_task_control;
}

class dialog_task_control : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_task_control(QWidget *parent = 0);
    ~dialog_task_control();

    void Update();

private:
    Ui::dialog_task_control *ui;

    QTimer* timer;

    QVector<QLabel3*> m_vtTask;
    QVector<QLabel3*> m_vtStatus;
    QVector<QLabel4*> m_vtContPic;
    QVector<QLabel3*> m_vtCont;
    QVector<QLabel3*> m_vtAutoBox;
    QVector<QLabel3*> m_vtAutoChk;
    QVector<QLabel4*> m_vtAutoPic;
    QVector<QLabel3*> m_vtAuto;
    QVector<QLabel4*> m_vtMacroPic;
    QVector<QLabel3*> m_vtMacroIcon;
    QVector<QLabel3*> m_vtMacro;

    QStringList m_Macro;
    QStringList m_AutoRun;

    QVector<QPixmap> pxmRunIcon;
    void Display_RunPause(int table_index, CNR_TASK_STATUS status);
    void Display_AutoRun(int table_index, QString autorun);
    void Display_Macro(int table_index, QString macro);
    void Display_EnableControl(int table_index, CNR_TASK_STATUS status);
    void Display_MacroChanged(int table_index, bool changed);

    QVector<bool> m_bChangeMacro;

    dialog_varmonitor* dig_varmonitor;

public slots:
    void onClose();
    void onTimer();

    void onCont();
    void onAuto();
    void onMacro();

    void onVarInit();
    void onDataView();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_TASK_CONTROL_H
