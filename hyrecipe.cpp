#include "hyrecipe.h"

HyRecipe::HyRecipe()
{

}

bool HyRecipe::MakeDefault()
{
    if(!IsCanWrite()) return false;
    //CNRobo* pCon = CNRobo::getInstance();

    // For test
    cn_variant value;

    value.val.joint.joint[0] = 0;
    value.val.joint.joint[1] = 1;
    value.val.joint.joint[2] = 2;
    value.val.joint.joint[3] = 3;
    value.val.joint.joint[4] = 4;
    Set(j_data1, value.val.joint);

    cn_joint joint;
    Get(j_data1, &joint);

    value.val.trans.n[0] = 0;
    value.val.trans.n[1] = 1;
    value.val.trans.n[2] = 2;
    value.val.trans.o[0] = 3;
    value.val.trans.o[1] = 4;
    value.val.trans.o[2] = 5;

    Set(t_data1, value.val.trans);

    cn_trans trans;
    Get(t_data1, &trans);


    value.val.f32 = 123.456;
    Set(n_data1, value.val.f32);
    float num;
    Get(n_data1, &num);

    QString string;
    Set(s_data1, QString("Hello!! AR"));
    Get(s_data1, &string);
    qDebug() << string;

    return true;
}

bool HyRecipe::AllZero()
{
    int i;
    cn_variant value;
    memset(value.val.str,0,sizeof(CN_MAX_CHARS));

    /*for(i=0;i<j_num;i++)
        Set((RECIPE_JOINT)i, value.val.joint);

    for(i=0;i<t_num;i++)
        Set((RECIPE_TRANS)i, value.val.trans);*/

    for(i=0;i<n_num;i++)
        Set((RECIPE_NUMBER)i, 0.0);

    for(i=0;i<s_num;i++)
        Set((RECIPE_STRING)i, QString(""));

    return true;
}

bool HyRecipe::Clear()
{
    if(!IsCanWrite()) return false;
    CNRobo* pCon = CNRobo::getInstance();

    pCon->deleteVariableAll();


    /*Joint_Name.clear();Joint_Value.clear();
    pCon->getVariableList(CN_ROBOT_VARIABLE_JOINT, Joint_Name, Joint_Value);
    for(int i=0;i<Joint_Name.count();i++)
        pCon->deleteVariable(CN_ROBOT_VARIABLE_JOINT,Joint_Name[i]);

    Trans_Name.clear();Trans_Value.clear();
    pCon->getVariableList(CN_ROBOT_VARIABLE_TRANS, Trans_Name, Trans_Value);
    for(int i=0;i<Trans_Name.count();i++)
        pCon->deleteVariable(CN_ROBOT_VARIABLE_TRANS,Trans_Name[i]);

    Number_Name.clear();Number_Value.clear();
    pCon->getVariableList(CN_ROBOT_VARIABLE_NUMBER, Number_Name, Number_Value);
    for(int i=0;i<Number_Name.count();i++)
        pCon->deleteVariable(CN_ROBOT_VARIABLE_NUMBER,Number_Name[i]);

    String_Name.clear();String_Value.clear();
    pCon->getVariableList(CN_ROBOT_VARIABLE_STRING, String_Name, String_Value);
    for(int i=0;i<String_Name.count();i++)
        pCon->deleteVariable(CN_ROBOT_VARIABLE_STRING,String_Name[i]);*/

    return true;
}

bool HyRecipe::Clear(int type)
{
    if(!IsCanWrite()) return false;
    CNRobo* pCon = CNRobo::getInstance();

    switch(type)
    {
    case CN_ROBOT_VARIABLE_JOINT:
        /*Joint_Name.clear();Joint_Value.clear();
        pCon->getVariableList(CN_ROBOT_VARIABLE_JOINT, Joint_Name, Joint_Value);
        for(int i=0;i<Joint_Name.count();i++)
            pCon->deleteVariable(CN_ROBOT_VARIABLE_JOINT,Joint_Name[i]);*/
        break;

    case CN_ROBOT_VARIABLE_TRANS:
        /*Trans_Name.clear();Trans_Value.clear();
        pCon->getVariableList(CN_ROBOT_VARIABLE_TRANS, Trans_Name, Trans_Value);
        for(int i=0;i<Trans_Name.count();i++)
            pCon->deleteVariable(CN_ROBOT_VARIABLE_TRANS,Trans_Name[i]);*/

        break;

    case CN_ROBOT_VARIABLE_NUMBER:
        Number_Name.clear();Number_Value.clear();
        pCon->getVariableList(CN_ROBOT_VARIABLE_NUMBER, Number_Name, Number_Value);
        for(int i=0;i<Number_Name.count();i++)
            pCon->deleteVariable(CN_ROBOT_VARIABLE_NUMBER,Number_Name[i]);
        break;

    case CN_ROBOT_VARIABLE_STRING:
        String_Name.clear();String_Value.clear();
        pCon->getVariableList(CN_ROBOT_VARIABLE_STRING, String_Name, String_Value);
        for(int i=0;i<String_Name.count();i++)
            pCon->deleteVariable(CN_ROBOT_VARIABLE_STRING,String_Name[i]);
        break;

    default:
        return false;
    }

    return true;

}

bool HyRecipe::Read()
{
    if(!IsCanRead()) return false;
    CNRobo* pCon = CNRobo::getInstance();

    Joint_Name.clear();Joint_Value.clear();
    pCon->getVariableList(CN_ROBOT_VARIABLE_JOINT, Joint_Name, Joint_Value);
    Trans_Name.clear();Trans_Value.clear();
    pCon->getVariableList(CN_ROBOT_VARIABLE_TRANS, Trans_Name, Trans_Value);
    Number_Name.clear();Number_Value.clear();
    pCon->getVariableList(CN_ROBOT_VARIABLE_NUMBER, Number_Name, Number_Value);
    String_Name.clear();String_Value.clear();
    pCon->getVariableList(CN_ROBOT_VARIABLE_STRING, String_Name, String_Value);

    return true;
}

bool HyRecipe::Read(int type)
{
    if(!IsCanRead()) return false;
    CNRobo* pCon = CNRobo::getInstance();

    switch(type)
    {
    case CN_ROBOT_VARIABLE_JOINT:
        Joint_Name.clear();Joint_Value.clear();
        pCon->getVariableList(CN_ROBOT_VARIABLE_JOINT, Joint_Name, Joint_Value);
        break;

    case CN_ROBOT_VARIABLE_TRANS:
        Trans_Name.clear();Trans_Value.clear();
        pCon->getVariableList(CN_ROBOT_VARIABLE_TRANS, Trans_Name, Trans_Value);
        break;

    case CN_ROBOT_VARIABLE_NUMBER:
        Number_Name.clear();Number_Value.clear();
        pCon->getVariableList(CN_ROBOT_VARIABLE_NUMBER, Number_Name, Number_Value);
        break;

    case CN_ROBOT_VARIABLE_STRING:
        String_Name.clear();String_Value.clear();
        pCon->getVariableList(CN_ROBOT_VARIABLE_STRING, String_Name, String_Value);
        break;

    default:
        return false;
    }

    return true;
}

bool HyRecipe::Write()
{
    if(!IsCanWrite()) return false;
    CNRobo* pCon = CNRobo::getInstance();

    int i;

    for(i=0;i<Joint_Name.count();i++)
        pCon->setVariableData(CN_ROBOT_VARIABLE_JOINT,Joint_Name[i],Joint_Value[i],true);

    for(i=0;i<Trans_Name.count();i++)
        pCon->setVariableData(CN_ROBOT_VARIABLE_TRANS,Trans_Name[i],Trans_Value[i],true);

    for(i=0;i<Number_Name.count();i++)
        pCon->setVariableData(CN_ROBOT_VARIABLE_NUMBER,Number_Name[i],Number_Value[i],true);

    for(i=0;i<String_Name.count();i++)
        pCon->setVariableData(CN_ROBOT_VARIABLE_STRING,String_Name[i],String_Value[i],true);

    pCon->saveVariable();
    return true;
}

bool HyRecipe::Write(int type)
{
    if(!IsCanWrite()) return false;
    CNRobo* pCon = CNRobo::getInstance();

    int i;

    switch(type)
    {
    case CN_ROBOT_VARIABLE_JOINT:
        for(i=0;i<Joint_Name.count();i++)
            pCon->setVariableData(CN_ROBOT_VARIABLE_JOINT,Joint_Name[i],Joint_Value[i],true);
        break;

    case CN_ROBOT_VARIABLE_TRANS:
        for(i=0;i<Trans_Name.count();i++)
            pCon->setVariableData(CN_ROBOT_VARIABLE_TRANS,Trans_Name[i],Trans_Value[i],true);
        break;

    case CN_ROBOT_VARIABLE_NUMBER:
        for(i=0;i<Number_Name.count();i++)
            pCon->setVariableData(CN_ROBOT_VARIABLE_NUMBER,Number_Name[i],Number_Value[i],true);
        break;

    case CN_ROBOT_VARIABLE_STRING:
        for(i=0;i<String_Name.count();i++)
            pCon->setVariableData(CN_ROBOT_VARIABLE_STRING,String_Name[i],String_Value[i],true);
        break;

    default:
        return false;
    }

    pCon->saveVariable();
    return true;
}

bool HyRecipe::Set(HyRecipe::RECIPE_JOINT id, cn_joint data, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    QString name = toName(id);
    if(name == "") return false;
    name = "#" + name;
    qDebug() << name;

    cn_variant value;
    value.val.joint = data;

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_JOINT, name, value, true);
    if(ret != 0)
        return false;

    if(save)
        pCon->saveVariable();

    return true;
}

void HyRecipe::SaveVariable()
{
    CNRobo* pCon = CNRobo::getInstance();
    pCon->saveVariable();
}

bool HyRecipe::Set(HyRecipe::RECIPE_TRANS id, cn_trans data, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    QString name = toName(id);
    if(name == "") return false;
    qDebug() << name;

    cn_variant value;
    value.val.trans = data;

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_TRANS, name, value, true);
    if(ret != 0)
        return false;

    if(save)
        pCon->saveVariable();

    return true;
}

bool HyRecipe::Set(HyRecipe::RECIPE_NUMBER id, float data, bool save)
{
    if(!IsCanWrite()) return false;

    timer.start();

    CNRobo* pCon = CNRobo::getInstance();

    QString name = toName(id);
    if(name == "") return false;
    qDebug() << name;

    cn_variant value;
    value.val.f32 = data;

    qDebug() << "before var.set time = " << timer.elapsed() << "msec";

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_NUMBER, name, value, true);
    if(ret != 0)
    {
        qDebug() << "setVariableData ret=" << ret;
        return false;
    }

    if(save)
        pCon->saveVariable();

    qDebug() << "after var.set time = " << timer.elapsed() << "msec";
    return true;
}

bool HyRecipe::Set(QString var, float data, bool save)
{
    if(!IsCanWrite()) return false;

    timer.start();

    CNRobo* pCon = CNRobo::getInstance();

    QString name = var;
    if(name == "") return false;
    qDebug() << name;

    cn_variant value;
    value.val.f32 = data;

    qDebug() << "before var.set time = " << timer.elapsed() << "msec";

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_NUMBER, name, value, true);
    if(ret != 0)
    {
        qDebug() << "setVariableData ret=" << ret;
        return false;
    }

    if(save)
        pCon->saveVariable();

    qDebug() << "after var.set time = " << timer.elapsed() << "msec";
    return true;
}

bool HyRecipe::Set(HyRecipe::RECIPE_STRING id, QString data, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    QString name = toName(id);
    if(name == "") return false;
    name = "$"+name;
    qDebug() << name;

    cn_variant value;
    sprintf(value.val.str,"%s",data.toAscii().data());

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_STRING, name, value, true);
    if(ret != 0)
        return false;

    if(save)
        pCon->saveVariable();

    return true;
}

bool HyRecipe::Set(QString var, QString data, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    QString name = var;
    if(name == "") return false;
    if(name.indexOf("$") < 0)
    {
        qDebug() << "HyRecipe::Set String no $";
        return false;
    }
    qDebug() << name;

    cn_variant value;
    sprintf(value.val.str,"%s",data.toAscii().data());

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_STRING, name, value, true);
    if(ret != 0)
        return false;

    if(save)
        pCon->saveVariable();

    return true;
}

bool HyRecipe::Get(HyRecipe::RECIPE_JOINT id, cn_joint *data)
{
    if(!IsCanRead()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    QString name = toName(id);
    if(name == "") return false;
    name = "#" + name;

    qDebug() << name;

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_JOINT, name, &value);
    if(ret != 0)
        return false;

    *data = value.val.joint;
    return true;
}

bool HyRecipe::Get(HyRecipe::RECIPE_TRANS id, cn_trans* data)
{
    if(!IsCanRead()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    QString name = toName(id);
    if(name == "") return false;

    qDebug() << name;

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_TRANS, name, &value);
    if(ret != 0)
        return false;

    *data = value.val.trans;
    return true;
}

bool HyRecipe::Get(HyRecipe::RECIPE_NUMBER id, float* data)
{
    if(!IsCanRead()) return false;

    timer.start();

    CNRobo* pCon = CNRobo::getInstance();

    QString name = toName(id);
    if(name == "") return false;

    //qDebug() << name;

    //qDebug() << "before var.get time = " << timer.elapsed() << "msec";

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_NUMBER, name, &value);
    if(ret != 0)
        return false;

    //qDebug() << "after var.get time = " << timer.elapsed() << "msec";

    *data = value.val.f32;
    return true;
}

bool HyRecipe::Get(QString var, float& data)
{
    if(!IsCanRead()) return false;

    timer.start();

    CNRobo* pCon = CNRobo::getInstance();

    QString name = var;
    if(name == "") return false;

    //qDebug() << name;

    //qDebug() << "before var.get time = " << timer.elapsed() << "msec";

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_NUMBER, name, &value);
    if(ret != 0)
        return false;

    //qDebug() << "after var.get time = " << timer.elapsed() << "msec";

    data = value.val.f32;
    return true;
}

bool HyRecipe::Get(HyRecipe::RECIPE_STRING id, QString* data)
{
    if(!IsCanRead()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    QString name = toName(id);
    if(name == "") return false;
    name = "$"+name;
    qDebug() << name;

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_STRING, name, &value);
    if(ret != 0)
        return false;

    *data = QString("%1").arg(value.val.str);
    return true;
}

bool HyRecipe::Get(QString var, QString& data)
{
    if(!IsCanRead()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    QString name = var;
    if(name == "") return false;
    if(name.indexOf("$") < 0)
    {
        qDebug() << "HyRecipe::Get String no $";
        return false;
    }
    //qDebug() << name;

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_STRING, name, &value);
    if(ret != 0)
        return false;

    data = QString("%1").arg(value.val.str);
    return true;
}

bool HyRecipe::IsCanRead()
{
    CNRobo* pCon = CNRobo::getInstance();
    if(!pCon->isConnected()) return false;
    bool bOk;
    pCon->getLoginStatus(&bOk);
    if(!bOk) return false;

    return true;
}

bool HyRecipe::IsCanWrite()
{
    CNRobo* pCon = CNRobo::getInstance();
    if(!pCon->isConnected()) return false;
    bool bOk;
    pCon->getLoginStatus(&bOk);
    if(!bOk) return false;
    pCon->getLockStatus(&bOk);
    if(!bOk) return false;

    return true;
}

QString HyRecipe::toName(HyRecipe::RECIPE_JOINT enums)
{
    const QMetaObject mo = staticMetaObject;
    int index = mo.indexOfEnumerator("RECIPE_JOINT");
    if(index == -1)
        return "";
    QMetaEnum metaenum = mo.enumerator(index);
    return QString(metaenum.valueToKey(enums));
}

QString HyRecipe::toName(HyRecipe::RECIPE_TRANS enums)
{
    const QMetaObject mo = staticMetaObject;
    int index = mo.indexOfEnumerator("RECIPE_TRANS");
    if(index == -1)
        return "";
    QMetaEnum metaenum = mo.enumerator(index);
    return QString(metaenum.valueToKey(enums));
}

QString HyRecipe::toName(HyRecipe::RECIPE_NUMBER enums)
{
    const QMetaObject mo = staticMetaObject;
    int index = mo.indexOfEnumerator("RECIPE_NUMBER");
    if(index == -1)
        return "";
    QMetaEnum metaenum = mo.enumerator(index);
    return QString(metaenum.valueToKey(enums));
}

QString HyRecipe::toName(HyRecipe::RECIPE_STRING enums)
{
    const QMetaObject mo = staticMetaObject;
    int index = mo.indexOfEnumerator("RECIPE_STRING");
    if(index == -1)
        return "";
    QMetaEnum metaenum = mo.enumerator(index);
    return QString(metaenum.valueToKey(enums));
}

bool HyRecipe::SetArr(QString name, int i, cn_joint data, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    name += QString("[%1]").arg(i);
    name.push_front("#");
    qDebug() << name;

    cn_variant value;
    value.val.joint = data;

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_JOINT, name, value, true);
    if(ret != 0)
        return false;

    if(save)
        pCon->saveVariable();

    return true;
}
bool HyRecipe::GetArr(QString name, int i, cn_joint* data)
{
    if(!IsCanRead()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    name += QString("[%1]").arg(i);
    name.push_front("#");
    qDebug() << name;

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_JOINT, name, &value);
    if(ret != 0)
        return false;

    *data = value.val.joint;
    return true;
}

bool HyRecipe::SetArr(QString name, int i, cn_trans data, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    name += QString("[%1]").arg(i);
    qDebug() << name;

    cn_variant value;
    value.val.trans = data;

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_TRANS, name, value, true);
    if(ret != 0)
        return false;

    if(save)
        pCon->saveVariable();

    return true;
}
bool HyRecipe::GetArr(QString name, int i, cn_trans* data)
{
    if(!IsCanRead()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    name += QString("[%1]").arg(i);
    qDebug() << name;

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_TRANS, name, &value);
    if(ret != 0)
        return false;

    for(int j=0;j<3;j++)
    {
        qDebug("p[%d]=%f",j,value.val.trans.p[j]);
        qDebug("eu[%d]=%f",j,value.val.trans.eu[j]);
        qDebug("n[%d]=%f",j,value.val.trans.n[j]);
        qDebug("o[%d]=%f",j,value.val.trans.o[j]);
        qDebug("a[%d]=%f",j,value.val.trans.a[j]);

        data->p[j] = value.val.trans.p[j];
        data->eu[j] = value.val.trans.eu[j];
    }

    *data = value.val.trans;
    return true;
}

bool HyRecipe::SetArr(QString name, int i, float data, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    name += QString("[%1]").arg(i);
    qDebug() << name;

    cn_variant value;
    value.val.f32 = data;

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_NUMBER, name, value, true);
    if(ret != 0)
        return false;

    if(save)
        pCon->saveVariable();

    return true;
}
bool HyRecipe::GetArr(QString name, int i, float* data)
{
    if(!IsCanRead()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    name += QString("[%1]").arg(i);
    qDebug() << name;

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_NUMBER, name, &value);
    if(ret != 0)
        return false;

    *data = value.val.f32;
    return true;
}

bool HyRecipe::SetArr(QString name, int i, QString data, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    name += QString("[%1]").arg(i);
    name.push_front("$");
    qDebug() << name;

    cn_variant value;
    sprintf(value.val.str,"%s",data.toAscii().data());

    int ret = pCon->setVariableData(CN_ROBOT_VARIABLE_STRING, name, value, true);
    if(ret != 0)
        return false;

    if(save)
        pCon->saveVariable();

    return true;
}
bool HyRecipe::GetArr(QString name, int i, QString* data)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    name += QString("[%1]").arg(i);
    name.push_front("$");
    qDebug() << name;

    cn_variant value;
    int ret = pCon->getVariableData(CN_ROBOT_VARIABLE_STRING, name, &value);
    if(ret != 0)
        return false;

    *data = QString("%1").arg(value.val.str);
    return true;
}

bool HyRecipe::Gets(QVector<RECIPE_NUMBER> ids, QVector<float> &datas)
{
    if(!IsCanRead()) return false;

    if(ids.isEmpty()) return false;

    //timer.start();

    CNRobo* pCon = CNRobo::getInstance();

    // make variable name
    QStringList var_names;
    int i;
    for(i=0;i<ids.count();i++)
        var_names.append(toName(ids[i]));


    QList<cn_variant> values;
    int ret = pCon->getVariables(var_names, values);
    if(ret != 0)
    {
        qDebug() << "getVariables ret=" << ret;
        return false;
    }

    datas.clear();
    for(i=0;i<values.count();i++)
    {
        if(values[i].type < 0) // error case
            datas.append(0);
        else
            datas.append(values[i].val.f32);
    }

    //qDebug() << "HyRecipe Gets() time = " << timer.elapsed() << "msec";

    return true;
}

bool HyRecipe::Sets(QVector<RECIPE_NUMBER> ids, QVector<float> &datas, bool save)
{
    if(!IsCanRead()) return false;

    if(ids.isEmpty()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    // make variable name
    QStringList var_names;
    int i;
    for(i=0;i<ids.count();i++)
        var_names.append(toName(ids[i]));


    // make cn_variant list
    QList<cn_variant> values;
    cn_variant temp;
    for(i=0;i<datas.count();i++)
    {
        temp.type = CNVAR_FLOAT;
        temp.val.f32 = datas[i];
        values.append(temp);
    }

    int ret = pCon->setVariables(var_names, values);
    if(ret != 0)
    {
        qDebug() << "setVariables ret=" << ret;
        return false;
    }

    for(i=0;i<values.count();i++)
    {
        if(values[i].type < 0)
        {
            qDebug() << "setVariables error [" << i << "]";
        }
    }

    if(save)
        pCon->saveVariable();

    return true;
}

bool HyRecipe::SetsArr(QString name, QVector<float> &datas, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    // make variable name
    QStringList var_names;
    QString str;
    int i;
    for(i=0;i<datas.count();i++)
    {
        str = name;
        str += QString("[%1]").arg(i);
        var_names.append(str);
    }

    // make cn_variant list
    QList<cn_variant> values;
    cn_variant temp;
    for(i=0;i<datas.count();i++)
    {
        temp.type = CNVAR_FLOAT;
        temp.val.f32 = datas[i];
        values.append(temp);
    }

    int ret = pCon->setVariables(var_names, values);
    if(ret != 0)
    {
        qDebug() << "setVariables ret=" << ret;
        return false;
    }

    for(i=0;i<values.count();i++)
    {
        if(values[i].type < 0)
        {
            qDebug() << "setVariables error [" << i << "]";
        }
    }

    if(save)
        pCon->saveVariable();

    return true;
}
bool HyRecipe::GetsArr(QString name, int count, QVector<float> &datas)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    // make variable name
    QStringList var_names;
    QString str;
    int i;
    for(i=0;i<count;i++)
    {
        str = name;
        str += QString("[%1]").arg(i);
        var_names.append(str);
    }

    QList<cn_variant> values;
    int ret = pCon->getVariables(var_names, values);
    if(ret != 0)
    {
        qDebug() << "getVariables ret=" << ret;
        return false;
    }

    datas.clear();
    for(i=0;i<values.count();i++)
    {
        if(values[i].type < 0) // error case
            datas.append(0);
        else
            datas.append(values[i].val.f32);
    }

    return true;
}

/** global name type **/
bool HyRecipe::SetVars(QStringList names, QList<cn_variant>& datas, bool save)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->setVariables(names, datas);
    if(ret < 0)
    {
        qDebug() << "setVariables ret=" << ret;
        return false;
    }

    for(int i=0;i<datas.count();i++)
    {
        if(datas[i].type < 0)
            qDebug() << "setVariables error [" << i << "]";
    }

    if(save)
        pCon->saveVariable();

    return true;
}

bool HyRecipe::GetVars(QStringList names, QList<cn_variant>& datas)
{
    if(!IsCanWrite()) return false;

    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->getVariables(names, datas);
    if(ret < 0)
    {
        qDebug() << "getVariables ret=" << ret;
        return false;
    }

    return true;
}

