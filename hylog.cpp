#include "hylog.h"

HyLog::HyLog()
{
    Init();
}

void HyLog::Init()
{
    m_slCode.clear();m_slMsg.clear();   // must.

    // example.
    RegLog(7777, tr("AR Test Log"));
    RegLog(500, tr("Started logging"));
    RegLog(501, tr("Varialble data is saved"));
    // add here

}
void HyLog::RegLog(int code, QString message)
{
    // if code have, change only message.
    // if code don't have, register code & message.

    int index = GetIndex(code);
    if(index < 0)
    {
        // no have code
        m_slCode.append(QString().setNum(code));
        m_slMsg.append(message);
    }
    else
    {
        // have code
        m_slMsg[index] = message;
    }
}

void HyLog::Set(int code)
{
    // if code don't have, write default message.

    QString msg;
    int index = GetIndex(code);
    if(index < 0)
        msg = QString(tr("User Log : %1")).arg(code);
    else
        msg = m_slMsg.at(index);

    CNRobo* pCon = CNRobo::getInstance();
    pCon->setUserLog(code, msg);
}

void HyLog::Set(int code, QString message)
{
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setUserLog(code, message);
}

bool HyLog::IsExist(int code)
{
    return m_slCode.contains(QString().setNum(code));
}
QString HyLog::Get(int code)
{
    QString msg;
    int index = GetIndex(code);

    if(index < 0)
        msg.clear();
    else
        msg = m_slMsg[index];

    return msg;
}

int HyLog::GetIndex(int code)
{
    return m_slCode.indexOf(QString().setNum(code));
}




