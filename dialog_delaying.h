#ifndef DIALOG_DELAYING_H
#define DIALOG_DELAYING_H

#include <QDialog>
#include <QTimer>
#include <QElapsedTimer>

#include "global.h"


namespace Ui {
class dialog_delaying;
}

class dialog_delaying : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_delaying(QWidget *parent = 0);
    ~dialog_delaying();

    void Init(int delay_msec);

public slots:
    void onTimer();

private:
    Ui::dialog_delaying *ui;

    QTimer* timer;
    QElapsedTimer watch;

    int m_nDelayMsec;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_DELAYING_H
