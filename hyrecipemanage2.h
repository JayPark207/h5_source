#ifndef HYRECIPEMANAGE2_H
#define HYRECIPEMANAGE2_H

#include <QObject>
#include <QStringList>
#include <QDebug>
#include <QDateTime>
#include <QElapsedTimer>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"


/** no : 1~ N (but, list start 0)
    so, new recipe always match [0]=1, [1]=2 ....
    so, list[n] = "n+1"(m_No string)
**/

class HyRecipeManage2 : public QObject
{
    Q_OBJECT
public:
    enum INFO_CONTENTS
    {
        INFO_NO=0,
        INFO_NAME,
        INFO_DATE
    };


public: // functions
    explicit HyRecipeManage2(QObject *parent = 0);

    // init (not yet => this function go to bakup class)
    //bool MakeDirStructure(QString target_base_dir);  // postpone
    //bool MakeFirstRecipe(); // no have one recipe(default -> 1 new) // postpone

    // default data maker functions.
    bool MakeDefualt(); // from now to default. (for development)
    // default macro program list = default dir. programs list.
    bool SetDefault_Info(QString ver, QDateTime date);
    bool SetDefault_Var();
    bool SetDefault_Program(QString prgName); // only base macro program.
    bool SetDefault_Program();
    bool IsDefaultProgram(QString prgName);


    // Initailization (using ui booting)
    bool Initial(QString now_no);
    QString GetNowRecipeNo();
    QString GetNowRecipeNoName();
    bool GetNowRecipeName(QString& name);
    void SetNowRecipeNo(QString no);

    // data update. (read dirlist & info files)
    bool ReadDatum();          // both. (this is boss)
    bool ReadDatum_Inner();    // only inner.
    bool ReadDatum_SD();       // only SD.
    bool ReadInfo();           // "m_No" use read info data.

    // mainly functions.
    bool Sync(int list_index); // save from now datas to db recipe.
    bool Load(int old_list_index, int new_list_index); // include sync.
    bool Load_Force(int list_index); // no sync, only load
    bool New(QString name);
    bool New(int list_index, QString name);
    bool Copy(int source_list_index, int target_list_index, bool bVar=true, bool bProg=true);
    bool Delete(int list_index);
    bool Rename(int list_index, QString name); // info file change.

    // advanced mainly functions. (macro program management upgrade)
    bool ASync(int list_index); // save from now datas to db recipe.
    bool ALoad(int old_list_index, int new_list_index); // include sync.
    bool ALoad_Force(int list_index); // no sync, only load
    bool ANew(QString name);
    bool ANew(int list_index, QString name);
    bool ACopy(int source_list_index, int target_list_index, bool bVar=true, bool bProg=true);
    bool ADelete(int list_index);
    bool ARename(int list_index, QString name); // info file change.

    // check.
    bool IsExist(QString path);
    bool IsTPRecipe();
    bool IsSDRecipe();
    bool IsSD();
    bool IsUSB();
    int Find_NewListIndex(); // find empty index (this+1 is no!!!)
    int ListIndexOf(QString no); // same indexof return value.

    // handling list contents.
    int     GetCount();
    QString GetNo(int list_index);
    QString GetName(int list_index);
    QString GetDate(int list_index);
    // for read current name & date
    QString GetName(QString no);
    QString GetDate(QString no);

    QString GetDisplayName(QString no);
    QString GetDisplayName(int list_index);

    // "(...)" delete
    bool CallLine2MacroName(QString& str);


private: // functions

    bool SetInfo(QString no, QString name);
    bool SetInfo(QString no, QString name, QDateTime date);
    bool GetInfo(QString no, QString& name);
    bool GetInfo(QString no, QString& name, QDateTime& date);

    // index & list manager.
    void AddRecipeList(int list_index, QString no, QString name, QDateTime date); // find empty number & adding data.
    void DelRecipeList(int list_index);

    QString GetRecipePath(int list_index);  // must have list
    QString GetRecipePath(QString no);      // don't have list(using new, copy ...)

private: // variable
    // 3 lists is one. important buffer for maintain list & display.
    QStringList m_No;     // *reference (=folder name)
    QStringList m_Name;
    QStringList m_Date;

    QString m_NowNo;    // now loaded no.

    QElapsedTimer ti;

};

#endif // HYRECIPEMANAGE2_H
