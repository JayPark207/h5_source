#ifndef DIALOG_ERROR_LIST_H
#define DIALOG_ERROR_LIST_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"

namespace Ui {
class dialog_error_list;
}

class dialog_error_list : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_error_list(QWidget *parent = 0);
    ~dialog_error_list();

    void Update();

public slots:
    void onClose();

private:
    Ui::dialog_error_list *ui;

    QVector<QLabel3*> m_vtNo;
    QVector<QLabel3*> m_vtCode;
    QVector<QLabel3*> m_vtName;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ERROR_LIST_H
