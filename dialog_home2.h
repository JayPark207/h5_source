#ifndef DIALOG_HOME2_H
#define DIALOG_HOME2_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "dialog_delaying.h"

namespace Ui {
class dialog_home2;
}

class dialog_home2 : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_home2(QWidget *parent = 0);
    ~dialog_home2();

    void Update();
    bool m_bNoMacroOff;
    void Enable_Check();

    void Init(bool enable_teach = true, bool no_macro_off = false);

private:
    Ui::dialog_home2 *ui;

    QTimer* timer;

    QString m_strDelayUnit, m_strSpdUnit;
    void SetMoving(bool onoff);
    QImage img;
    QVector<QPixmap> pxmMoving;
    bool m_bMoving, m_bHolding;

    void Enable_btns(bool enable);

    bool m_bTryMoving;

    void Display_SetPos(cn_trans trans, cn_joint joint);
    void Display_RealPos(float *trans, float *joint);

    bool m_bTeach;


    // home end check
    QTimer* fast_timer;
    int m_nState_EndCheck;
    void Process_EndCheck();


public slots:
    void onClose();

    void onMoveHome();
    void onTimer();

    void onFastTimer();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_HOME2_H
