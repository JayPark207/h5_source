#ifndef DIALOG_MESSAGE_H
#define DIALOG_MESSAGE_H

#include <QDialog>
#include <QTimer>
#include <QElapsedTimer>

namespace Ui {
class dialog_message;
}

class dialog_message : public QDialog
{
    Q_OBJECT
public:
    enum DIAG_MSG_COLOR
    {
        DEFAULT,
        RED,
        GREEN,
        SKYBLUE,
        GRAY,
        BLUE,
    };
    
public:
    explicit dialog_message(QWidget *parent = 0);
    ~dialog_message();

    void Message(QString msg1, QString msg2 = "");
    void Title(QString title);
    void SetColor(DIAG_MSG_COLOR color);
    void CloseTime(int msec);

private:
    Ui::dialog_message *ui;
    QTimer* timer;
    QElapsedTimer watch;

    int m_nCloseTime_msec;

public slots:
    void onTimer();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // DIALOG_MESSAGE_H
