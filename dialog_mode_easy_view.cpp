#include "dialog_mode_easy_view.h"
#include "ui_dialog_mode_easy_view.h"

dialog_mode_easy_view::dialog_mode_easy_view(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_mode_easy_view)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtBtnGrpPic.clear();m_vtBtnGrp.clear();m_vtLedGrp.clear();
    m_vtBtnGrpPic.append(ui->btnGPic);  m_vtBtnGrp.append(ui->btnG);  m_vtLedGrp.append(ui->ledG);
    m_vtBtnGrpPic.append(ui->btnGPic_2);m_vtBtnGrp.append(ui->btnG_2);m_vtLedGrp.append(ui->ledG_2);
    m_vtBtnGrpPic.append(ui->btnGPic_3);m_vtBtnGrp.append(ui->btnG_3);m_vtLedGrp.append(ui->ledG_3);
    m_vtBtnGrpPic.append(ui->btnGPic_4);m_vtBtnGrp.append(ui->btnG_4);m_vtLedGrp.append(ui->ledG_4);
    m_vtBtnGrpPic.append(ui->btnGPic_5);m_vtBtnGrp.append(ui->btnG_5);m_vtLedGrp.append(ui->ledG_5);

    int i;
    for(i=0;i<m_vtBtnGrpPic.count();i++)
    {
        connect(m_vtBtnGrpPic[i],SIGNAL(mouse_release()),this,SLOT(onChangeGrp()));
        connect(m_vtBtnGrp[i],SIGNAL(mouse_press()),m_vtBtnGrpPic[i],SLOT(press()));
        connect(m_vtBtnGrp[i],SIGNAL(mouse_release()),m_vtBtnGrpPic[i],SLOT(release()));
    }

    m_vtTbNo.clear();m_vtTbName.clear();m_vtTbData.clear();
    m_vtTbNo.append(ui->tbNo);  m_vtTbName.append(ui->tbName);  m_vtTbData.append(ui->tbData);
    m_vtTbNo.append(ui->tbNo_2);m_vtTbName.append(ui->tbName_2);m_vtTbData.append(ui->tbData_2);
    m_vtTbNo.append(ui->tbNo_3);m_vtTbName.append(ui->tbName_3);m_vtTbData.append(ui->tbData_3);
    m_vtTbNo.append(ui->tbNo_4);m_vtTbName.append(ui->tbName_4);m_vtTbData.append(ui->tbData_4);
    m_vtTbNo.append(ui->tbNo_5);m_vtTbName.append(ui->tbName_5);m_vtTbData.append(ui->tbData_5);
    m_vtTbNo.append(ui->tbNo_6);m_vtTbName.append(ui->tbName_6);m_vtTbData.append(ui->tbData_6);
    m_vtTbNo.append(ui->tbNo_7);m_vtTbName.append(ui->tbName_7);m_vtTbData.append(ui->tbData_7);

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));


    // var. init.
    m_nIndex = -1;
}

dialog_mode_easy_view::~dialog_mode_easy_view()
{
    delete ui;
}
void dialog_mode_easy_view::showEvent(QShowEvent *)
{
    Update();
}
void dialog_mode_easy_view::hideEvent(QHideEvent *)
{

}
void dialog_mode_easy_view::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_mode_easy_view::onClose()
{
    m_nIndex = -1;
    emit accept();
}

void dialog_mode_easy_view::Init(int index)
{
    m_nIndex = index;

    HyBigMode::ST_BMODE_INFO info;
    BigMode->Get_Info(m_nIndex, info);

    m_strName = info.name;
}

void dialog_mode_easy_view::Update()
{
    if(m_nIndex < 0) return;
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!BigMode->Read_FileData(m_nIndex, BigMode->m_FileData))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail to file read."),
                     ui->lbSelName->text());
        msg->exec();
        onClose();
        return;
    }

    if(!BigMode->Parsing_FileData(BigMode->m_FileData))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail to file read."),
                     ui->lbSelName->text());
        msg->exec();
        onClose();
        return;
    }

    SelectGrp(0);
    ui->lbSelName->setText(m_strName);
}

void dialog_mode_easy_view::SelectGrp(int grp)
{
    m_nSelectedGrp = grp;
    m_nStartIndex = 0;
    Update(m_nSelectedGrp);
    Display_BtnGrp(m_nSelectedGrp);
    Redraw_List(m_nSelectedGrp, m_nStartIndex);
}

void dialog_mode_easy_view::Update(int grp)
{
    BigMode->Read_ViewData((HyMode::MODE_GROUP)grp);
}

void dialog_mode_easy_view::Display_BtnGrp(int grp)
{
    for(int i=0;i<m_vtBtnGrpPic.count();i++)
    {
        m_vtBtnGrpPic[i]->setAutoFillBackground(i == grp);
        m_vtLedGrp[i]->setAutoFillBackground(i == grp);
    }
}

void dialog_mode_easy_view::Redraw_List(int grp, int start_index)
{
    int list_index;
    QString strNo,strName,strData;

    for(int i=0;i<m_vtTbNo.count();i++)
    {
        list_index = start_index + i;

        if(list_index >= BigMode->m_Mode->m_Data[grp].count())
        {
            // no have data.
            strNo.clear();
            strName.clear();
            strData.clear();

            m_vtTbNo[i]->setEnabled(false);
            m_vtTbName[i]->setEnabled(false);
            m_vtTbData[i]->setEnabled(false);
        }
        else
        {
            strNo = QString().setNum(list_index+1);
            strName = BigMode->m_Mode->m_Data[grp][list_index].Name;
            strData = BigMode->m_Mode->m_Data[grp][list_index].DispResult;

            if(!BigMode->m_Mode->m_Data[grp][list_index].bNA)
            {
                m_vtTbNo[i]->setEnabled(true);
                m_vtTbName[i]->setEnabled(true);
                m_vtTbData[i]->setEnabled(true);
            }
            else
            {
                m_vtTbNo[i]->setEnabled(false);
                m_vtTbName[i]->setEnabled(false);
                m_vtTbData[i]->setEnabled(false);
            }
        }

        m_vtTbNo[i]->setText(strNo);
        m_vtTbName[i]->setText(strName);
        m_vtTbData[i]->setText(strData);
    }

    ui->lbItemNum->setText(QString().setNum(Mode->m_Data[grp].size()));
}

void dialog_mode_easy_view::onChangeGrp()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtBtnGrpPic.indexOf(btn);
    if(index < 0) return;

    SelectGrp(index);
}

void dialog_mode_easy_view::onListUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(m_nSelectedGrp, --m_nStartIndex);
}

void dialog_mode_easy_view::onListDown()
{
    if(m_nStartIndex >= BigMode->m_Mode->m_Data[m_nSelectedGrp].size() - m_vtTbNo.size())
        return;

    Redraw_List(m_nSelectedGrp, ++m_nStartIndex);
}
