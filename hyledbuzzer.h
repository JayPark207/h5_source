#ifndef HYLEDBUZZER_H
#define HYLEDBUZZER_H

#include "dtp7-etc.h"

#include <QObject>
#include <QDebug>
#include <QString>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#define ON  1
#define OFF 0

#define LED_OFF     0x00
#define LED_RED     0x01
#define LED_BLUE    0x02
#define LED_PINK    0x03

class HyLedBuzzer : public QObject
{
    Q_OBJECT

public:
    HyLedBuzzer();
    ~HyLedBuzzer();

    bool Initial();

    void Led_All(bool onoff, unsigned char color = LED_RED);

    void Led_R1(bool onoff, unsigned char color = LED_RED);
    void Led_R2(bool onoff, unsigned char color = LED_RED);
    void Led_R3(bool onoff, unsigned char color = LED_RED);
    void Led_L1(bool onoff, unsigned char color = LED_RED);
    void Led_L2(bool onoff, unsigned char color = LED_RED);
    void Led_L3(bool onoff, unsigned char color = LED_RED);

    int     getLed_R1();
    int     getLed_R2();
    int     getLed_R3();
    int     getLed_L1();
    int     getLed_L2();
    int     getLed_L3();

    bool    IsLed_R1();
    bool    IsLed_R2();
    bool    IsLed_R3();
    bool    IsLed_L1();
    bool    IsLed_L2();
    bool    IsLed_L3();

    void Buzzer(bool onoff);
    bool IsBuzzer();
    void Beep(int time_msec);
    void Beep();

    QString GetErrorMessage();


    // new functions - using coreserver api
    bool Led_L1(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);
    bool Led_L2(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);
    bool Led_L3(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);

    bool Led_R1(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);
    bool Led_R2(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);
    bool Led_R3(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);


private:
    int m_dev;
    QString m_strErrorMsg;

    dtp7_etc_arg m_Args;
    dtp7_etc_arg m_ArgsBuzz;

};

#endif // HYLEDBUZZER_H
