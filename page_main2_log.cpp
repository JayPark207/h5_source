#include "page_main2_log.h"

page_main2_log::page_main2_log(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_main2_log)
{
    ui->setupUi(this);
}

page_main2_log::~page_main2_log()
{
    delete ui;
}

void page_main2_log::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
