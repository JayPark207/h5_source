#ifndef DIALOG_EUROMAP_H
#define DIALOG_EUROMAP_H

#include <QDialog>
#include <QStackedWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_confirm.h"


namespace Ui {
class dialog_euromap;
}

class dialog_euromap : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_euromap(QWidget *parent = 0);
    ~dialog_euromap();

    void Update();

private:
    Ui::dialog_euromap *ui;

    QTimer* timer;

    // Common
    void Display_MainTitle(int curr_type);

    // Tabs
    QVector<QLabel3*> m_vtTabLed;
    QVector<QLabel3*> m_vtTab;
    QVector<QLabel4*> m_vtTabPic;
    QStackedWidget* Panel;
    void Display_Tab(int tab_index);

    // Tab1
    QVector<QLabel3*> m_vtRtLed, m_vtImLed;
    QVector<QLabel3*> m_vtRtSym, m_vtImSym;

    void Display_RtName(int curr_type);
    void Display_ImName(int curr_type);
    void Refresh_RtLed();
    void Refresh_ImLed();

    void Enable_RobotForce(bool enable);
    bool IsRobotForce();
    void Enable_ImmForce(bool enable);
    bool IsImmForce();

    // Tab2
    QVector<QLabel3*> m_vtMdName;
    int m_nSelectedIndex_Mdu;
    int m_nStartIndex_Mdu;
    void Redraw_List_Mdu(int start_index);
    void Display_ListOn_Mdu(int table_index);


    QVector<QLabel3*> m_vtMdData, m_vtMdVal;
    QVector<QLabel4*> m_vtMdValPic;
    int m_nStartIndex_Mdd;
    QStringList m_ModuleData;
    bool Read_ModuleData(int module_index);
    void Redraw_List_Mdd(int start_index, int select_index);

    void Enable_MddEdit(bool enable);
    bool IsMddEdit();

    QVector<QPixmap> pxmRunIcon;
    void Display_SimRun(bool run);
    bool m_bSimRun;
    void ForceOff_SimRun();

    void UserLevel();

public slots:
    void onClose();
    void onTimer();

    void onTab();

    void onRobotForce();
    void onImmForce();
    void onRobotSignal();
    void onImmSignal();

    void onMduName();
    void onMduUp();
    void onMduDown();

    void onMddValue();
    void onMddUp();
    void onMddDown();

    void onMddEdit();
    void onSimRun();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_EUROMAP_H
