#ifndef WIDGET_STEPEDIT_PROGRAM_H
#define WIDGET_STEPEDIT_PROGRAM_H

#include <QWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_keyboard.h"
#include "dialog_confirm.h"

namespace Ui {
class widget_stepedit_program;
}

class widget_stepedit_program : public QWidget
{
    Q_OBJECT

public:
    explicit widget_stepedit_program(QWidget *parent = 0);
    ~widget_stepedit_program();

    void Init(QString prg_name, bool use_fullpath = false);
    void Update(); // first.

    void Read(QString prg_name);
    void Write(QString prg_name);
    void Redraw(int start_index);
    void SetModifyIcon(int line_index, bool onoff);
    void SetLineColor(int line_index);
    void SetProgramName(QString prg_name);
    void SetProgramIndex(int select_index, int max_index);

    // for inside/outside.
    void Update_Call(); // use inside/outside change.
    void SetInsideBtn(bool onoff);
    void SetOutsideBtn(bool onoff);

signals:
    void sigClose();

public slots:
    void onClose();
    void onTimer();

    void onList();
    void onUp();
    void onDown();
    void onEdit();
    void onAdd();
    void onDel();
    void onSave();
    void onReset();

    // for inside/outside
    void onInside();
    void onOutside();

private:
    Ui::widget_stepedit_program *ui;

    QTimer* timer;

    // for edit.
    QString m_strPrgName;
    int m_nSelectedIndex;
    int m_nStartIndex;
    QStringList m_Program, m_Program_Old, m_Program_Comp;

    QVector<QLabel3*> m_vtIcon;
    QVector<QLabel3*> m_vtNo;
    QVector<QLabel3*> m_vtText;
    bool m_bModify;

    // for inside/outside
    QStringList m_Stack, m_Stack_SelectedIndex, m_Stack_StartIndex;

    QVector<QPixmap> pxmModifyIcon;

    bool m_bUseFullPath;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // WIDGET_STEPEDIT_PROGRAM_H
