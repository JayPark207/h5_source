#include "widget_backup_recovery2.h"
#include "ui_widget_backup_recovery2.h"

widget_backup_recovery2::widget_backup_recovery2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::widget_backup_recovery2)
{
    ui->setupUi(this);

    connect(ui->btnBackPic,SIGNAL(mouse_release()),this,SIGNAL(sigBack()));
    connect(ui->btnBack,SIGNAL(mouse_press()),ui->btnBackPic,SLOT(press()));
    connect(ui->btnBack,SIGNAL(mouse_release()),ui->btnBackPic,SLOT(release()));
    connect(ui->btnBackIcon,SIGNAL(mouse_press()),ui->btnBackPic,SLOT(press()));
    connect(ui->btnBackIcon,SIGNAL(mouse_release()),ui->btnBackPic,SLOT(release()));

    m_vtChk.clear();m_vtMark.clear();m_vtNo.clear();
    m_vtChk.append(ui->tbCheck);  m_vtMark.append(ui->tbMark);  m_vtNo.append(ui->tbNo);
    m_vtChk.append(ui->tbCheck_2);m_vtMark.append(ui->tbMark_2);m_vtNo.append(ui->tbNo_2);
    m_vtChk.append(ui->tbCheck_3);m_vtMark.append(ui->tbMark_3);m_vtNo.append(ui->tbNo_3);
    m_vtChk.append(ui->tbCheck_4);m_vtMark.append(ui->tbMark_4);m_vtNo.append(ui->tbNo_4);
    m_vtChk.append(ui->tbCheck_5);m_vtMark.append(ui->tbMark_5);m_vtNo.append(ui->tbNo_5);
    m_vtChk.append(ui->tbCheck_6);m_vtMark.append(ui->tbMark_6);m_vtNo.append(ui->tbNo_6);

    m_vtName.clear();m_vtDate.clear();
    m_vtName.append(ui->tbName);  m_vtDate.append(ui->tbDate);
    m_vtName.append(ui->tbName_2);m_vtDate.append(ui->tbDate_2);
    m_vtName.append(ui->tbName_3);m_vtDate.append(ui->tbDate_3);
    m_vtName.append(ui->tbName_4);m_vtDate.append(ui->tbDate_4);
    m_vtName.append(ui->tbName_5);m_vtDate.append(ui->tbDate_5);
    m_vtName.append(ui->tbName_6);m_vtDate.append(ui->tbDate_6);

    int i;
    for(i=0;i<m_vtNo.count();i++)
    {
        connect(m_vtNo[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtChk[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
        connect(m_vtMark[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
        connect(m_vtName[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
        connect(m_vtDate[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

    connect(ui->tbCheckBoxTitle,SIGNAL(mouse_release()),this,SLOT(onCheckAll()));
    connect(ui->tbMarkTitle,SIGNAL(mouse_release()),ui->tbCheckBoxTitle,SIGNAL(mouse_release()));
    connect(ui->tbCheckTitle,SIGNAL(mouse_release()),ui->tbCheckBoxTitle,SIGNAL(mouse_release()));


    connect(ui->lbBox_Param,SIGNAL(mouse_release()),this,SLOT(onParam()));
    connect(ui->lbCBText_Param,SIGNAL(mouse_release()),ui->lbBox_Param,SIGNAL(mouse_release()));
    connect(ui->lbCheck_Param,SIGNAL(mouse_release()),ui->lbBox_Param,SIGNAL(mouse_release()));

    connect(ui->lbBox_Config,SIGNAL(mouse_release()),this,SLOT(onConfig()));
    connect(ui->lbCBText_Config,SIGNAL(mouse_release()),ui->lbBox_Config,SIGNAL(mouse_release()));
    connect(ui->lbCheck_Config,SIGNAL(mouse_release()),ui->lbBox_Config,SIGNAL(mouse_release()));

    connect(ui->btnRecoveryPic,SIGNAL(mouse_release()),this,SLOT(onRecovery()));
    connect(ui->btnRecovery,SIGNAL(mouse_press()),ui->btnRecoveryPic,SLOT(press()));
    connect(ui->btnRecovery,SIGNAL(mouse_release()),ui->btnRecoveryPic,SLOT(release()));
    connect(ui->btnRecoveryIcon,SIGNAL(mouse_press()),ui->btnRecoveryPic,SLOT(press()));
    connect(ui->btnRecoveryIcon,SIGNAL(mouse_release()),ui->btnRecoveryPic,SLOT(release()));


    m_nBackupListIndex = 0;
}

widget_backup_recovery2::~widget_backup_recovery2()
{
    delete ui;
}
void widget_backup_recovery2::showEvent(QShowEvent *)
{
    Update();
}
void widget_backup_recovery2::hideEvent(QHideEvent *)
{

}
void widget_backup_recovery2::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void widget_backup_recovery2::Display_USB(bool bExist)
{
    ui->lbUSB->setEnabled(bExist);
}
void widget_backup_recovery2::Init(int backup_list_index)
{
    m_nBackupListIndex = backup_list_index;
}

void widget_backup_recovery2::Update()
{
    QString backup_list_data = BR->m_BackupList[m_nBackupListIndex];
    BR->Read_Recipe(backup_list_data);

    // make mark flag.
    m_bMark.clear();
    m_bMark.fill(true, BR->m_RecipeNo.size());

    m_nStartIndex = 0;
    Redraw_List(m_nStartIndex);

    Display_SelectedBackupInfo();

    // check exist param & config file.
    ui->chkboxParam->setEnabled(BR->HasParam(backup_list_data));
    if(ui->chkboxParam->isEnabled())
        ui->lbCheck_Param->show();
    else
        ui->lbCheck_Param->hide();
    ui->chkboxConfig->setEnabled(BR->HasConfig(backup_list_data));
    if(ui->chkboxConfig->isEnabled())
        ui->lbCheck_Config->show();
    else
        ui->lbCheck_Config->hide();

}

void widget_backup_recovery2::Redraw_List(int start_index)
{
    int list_index;
    QString no, name, date;
    bool mark;

    for(int i=0;i<m_vtNo.count();i++)
    {
        list_index = start_index + i;

        if(list_index >= BR->m_RecipeNo.size())
        {
            // no have data.
            no.clear();
            name.clear();
            date.clear();
            mark = false;
        }
        else
        {
            // have data.
            no = BR->m_RecipeNo[list_index];
            name = BR->m_RecipeName[list_index];
            date = BR->m_RecipeDate[list_index];
            mark = m_bMark[list_index];
        }

        m_vtNo[i]->setText(no);
        m_vtName[i]->setText(name);
        m_vtDate[i]->setText(date);

        if(mark)
            m_vtMark[i]->show();
        else
            m_vtMark[i]->hide();

    }

    ui->lbTotal->setText(QString().setNum(BR->m_RecipeNo.size()));
}

void widget_backup_recovery2::onList()
{
    QLabel3* sel = (QLabel3*)sender();

    int row_index = m_vtNo.indexOf(sel);
    if(row_index < 0) return;

    if(m_vtNo[row_index]->text().isEmpty())
        return;

    int list_index = m_nStartIndex + row_index;

    m_bMark[list_index] = !m_bMark[list_index];

    ConfirmCheck();
    Redraw_List(m_nStartIndex);
}

void widget_backup_recovery2::onUp()
{
    if(m_nStartIndex <= 0)
        return;

    m_nStartIndex--;

    Redraw_List(m_nStartIndex);
}

void widget_backup_recovery2::onDown()
{
    if(BR->m_RecipeNo.size() <= (m_nStartIndex+m_vtNo.size()))
        return;

    m_nStartIndex++;

    Redraw_List(m_nStartIndex);
}

void widget_backup_recovery2::CheckAll(bool bCheck)
{
    m_bMark.clear();
    m_bMark.fill(bCheck, BR->m_RecipeNo.size());

    ConfirmCheck();
}
void widget_backup_recovery2::ConfirmCheck()
{
    int index = m_bMark.indexOf(false);
    if(index < 0)
    {
        // all true
        ui->tbMarkTitle->show();
    }
    else
    {
        // not all true.
        ui->tbMarkTitle->hide();
    }
}
void widget_backup_recovery2::onCheckAll()
{
    if(ui->tbMarkTitle->isHidden())
        CheckAll(true);
    else
        CheckAll(false);

    Redraw_List(m_nStartIndex);
}

void widget_backup_recovery2::Display_SelectedBackupInfo()
{
    QString date, time, name;

    BR->Parsing_BackupDir(BR->m_BackupList[m_nBackupListIndex],date,time,name);

    ui->lbSelectedDate->setText(date);
    ui->lbSelectedTime->setText(time);
    ui->lbSelectedName->setText(name);
}

void widget_backup_recovery2::onParam()
{
    if(ui->lbCheck_Param->isHidden())
        ui->lbCheck_Param->show();
    else
        ui->lbCheck_Param->hide();
}
void widget_backup_recovery2::onConfig()
{
    if(ui->lbCheck_Config->isHidden())
        ui->lbCheck_Config->show();
    else
        ui->lbCheck_Config->hide();
}

void widget_backup_recovery2::onRecovery()
{
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    // check selected items.
    bool bRecipe,bParam,bConfig;
    int index = m_bMark.indexOf(true);
    if(index < 0) bRecipe = false;
    else bRecipe = true;
    bParam = ui->lbCheck_Param->isVisible();
    bConfig = ui->lbCheck_Config->isVisible();

    if(!bRecipe && !bParam && !bConfig)
    {
        // error msg
        msg->SetColor(dialog_message::RED);
        msg->Title(ui->btnRecovery->text());
        msg->Message(tr("No Selected item!"));
        msg->exec();
        return;
    }

    // make massage 2
    QString msg2;
    msg2.clear();
    if(bRecipe)
    {
        msg2 += QString(tr("Mold Recipes"));
        msg2 += " ";
    }
    if(bParam)
    {
        msg2 += QString(tr("Robot Parameter"));
        msg2 += " ";
    }
    if(bConfig)
        msg2 += QString(tr("Robot Configuration"));

    // confirm sure
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnRecovery->text());
    conf->Message(tr("Would you want to Recovery?"), msg2);
    if(conf->exec() != QDialog::Accepted)
        return;

    // recipe recovery
    QString backup_list_data = BR->m_BackupList[m_nBackupListIndex];

    if(bRecipe)
    {
        for(int i=0;i<BR->m_RecipeNo.count();i++)
        {
            if(!m_bMark[i]) continue;

            if(!BR->Recovery_Recipe(backup_list_data, BR->m_RecipeNo[i]))
            {
                msg->SetColor(dialog_message::RED);
                msg->Title(ui->btnRecovery->text());
                msg->Message(tr("Fail to recovery!"));
                msg->exec();
                return;
            }
        }

        // force load [0]
        RM->ReadDatum();
        if(RM->ALoad_Force(0))
        {
            Param->Set(HyParam::RECIPE, RM->GetNowRecipeNo());
            QString recipe_name;
            if(RM->GetNowRecipeName(recipe_name))
                Recipe->Set(HyRecipe::s_RecipeName, recipe_name, false);
            Recipe->Set(HyRecipe::iNowRecipeNo, RM->GetNowRecipeNo().toFloat(), false);

            gSync_CommonPos();
        }

    }

    // param recovery
    if(bParam)
    {
        if(!BR->Recovery_Param(backup_list_data))
        {
            msg->SetColor(dialog_message::RED);
            msg->Title(ui->btnRecovery->text());
            msg->Message(tr("Fail to recovery!"));
            msg->exec();
            return;
        }
    }

    // config recovery
    if(bConfig)
    {
        if(!BR->Recovery_Config(backup_list_data))
        {
            msg->SetColor(dialog_message::RED);
            msg->Title(ui->btnRecovery->text());
            msg->Message(tr("Fail to recovery!"));
            msg->exec();
            return;
        }
    }

    msg->SetColor(dialog_message::GREEN);
    msg->Title(ui->btnRecovery->text());
    msg->Message(tr("Success to recovery!"),tr("Execute System Reboot!"));
    msg->exec();

    emit sigRecovery();
}
