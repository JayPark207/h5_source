#include "dialog_common_position.h"
#include "ui_dialog_common_position.h"

dialog_common_position::dialog_common_position(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_common_position)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtPosIndex.clear();
    m_vtPosIndex.append(HyPos::JIG_CHANGE);
    m_vtPosIndex.append(HyPos::SAMPLE);
    m_vtPosIndex.append(HyPos::FAULTY);

    m_slProgName.clear();
    m_slProgName.append(QString("move_jigchange"));
    m_slProgName.append(QString("move_sample"));
    m_slProgName.append(QString("move_faulty"));

    m_vtPosPic.clear();
    m_vtPosPic.append(ui->btnJigPic);
    m_vtPosPic.append(ui->btnSamplePic);
    m_vtPosPic.append(ui->btnFaultyPic);

    m_vtPosTxt.clear();
    m_vtPosTxt.append(ui->btnJig);
    m_vtPosTxt.append(ui->btnSample);
    m_vtPosTxt.append(ui->btnFaulty);

    m_vtPosIcon.clear();
    m_vtPosIcon.append(ui->btnJigIcon);
    m_vtPosIcon.append(ui->btnSampleIcon);
    m_vtPosIcon.append(ui->btnFaultyIcon);

    int i;
    for(i=0;i<m_vtPosPic.count();i++)
    {
        connect(m_vtPosPic[i],SIGNAL(mouse_release()),this,SLOT(onPosSelect()));
        connect(m_vtPosTxt[i],SIGNAL(mouse_press()),m_vtPosPic[i],SLOT(press()));
        connect(m_vtPosTxt[i],SIGNAL(mouse_release()),m_vtPosPic[i],SLOT(release()));
        connect(m_vtPosIcon[i],SIGNAL(mouse_press()),m_vtPosPic[i],SLOT(press()));
        connect(m_vtPosIcon[i],SIGNAL(mouse_release()),m_vtPosPic[i],SLOT(release()));
    }

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnTeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnTeach,SIGNAL(mouse_press()),ui->btnTeachPic,SLOT(press()));
    connect(ui->btnTeach,SIGNAL(mouse_release()),ui->btnTeachPic,SLOT(release()));
    connect(ui->btnTeachIcon,SIGNAL(mouse_press()),ui->btnTeachPic,SLOT(press()));
    connect(ui->btnTeachIcon,SIGNAL(mouse_release()),ui->btnTeachPic,SLOT(release()));

    connect(ui->btnMovePic,SIGNAL(mouse_release()),this,SLOT(onMove()));
    connect(ui->btnMove,SIGNAL(mouse_press()),ui->btnMovePic,SLOT(press()));
    connect(ui->btnMove,SIGNAL(mouse_release()),ui->btnMovePic,SLOT(release()));
    connect(ui->btnMoveIcon,SIGNAL(mouse_press()),ui->btnMovePic,SLOT(press()));
    connect(ui->btnMoveIcon,SIGNAL(mouse_release()),ui->btnMovePic,SLOT(release()));

#ifdef _H6_
    ui->btnJ6->setText(tr("J6"));

    ui->btnJ6->setEnabled(true);
    ui->btnJ6val->setEnabled(true);
    ui->btnJ6real->setEnabled(true);
#else
    ui->btnJ6->setText("");

    ui->btnJ6->setEnabled(false);
    ui->btnJ6val->setEnabled(false);
    ui->btnJ6real->setEnabled(false);
#endif

    // clear
    pxmMoving.clear();
}

dialog_common_position::~dialog_common_position()
{
    delete ui;
}
void dialog_common_position::showEvent(QShowEvent *)
{
    if(m_UsePosIndex == POS_ALL)
        m_nSelectedIndex = 0;
    else
        m_nSelectedIndex = m_UsePosIndex;

    Update(m_nSelectedIndex);

    timer->start();

    m_bHolding = false;
    gReadyMacro(true);
}
void dialog_common_position::hideEvent(QHideEvent *)
{
    gReadyMacro(false);
    timer->stop();
}
void dialog_common_position::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_common_position::Init(COMMON_POS_INDEX use_pos_index, bool bTeachMode)
{
    m_UsePosIndex = use_pos_index;
    m_bTeachMode = bTeachMode;
}

void dialog_common_position::ModeChange()
{
    // teach mode.
    if(m_bTeachMode)
    {
        ui->btnTeachPic->show();
        ui->btnTeach->show();
        ui->btnTeachIcon->show();
    }
    else
    {
        ui->btnTeachPic->hide();
        ui->btnTeach->hide();
        ui->btnTeachIcon->hide();
    }

    // use pos.
    switch(m_UsePosIndex)
    {
    case JIG_CHANGE:
    case SAMPLE:
    case FAULTY:
        ui->grpPosSelect->hide();
        break;

    default:
        ui->grpPosSelect->show();
        break;
    }

}

void dialog_common_position::Update(int btn_index)
{
    QString name;
    cn_trans trans;
    cn_joint joint;
    float speed,delay;

    HyPos::ENUM_POS_ARRAY pos_index = m_vtPosIndex[btn_index];
    Display_SelectBtn(btn_index);

    name = Posi->GetName(pos_index);

    if(!Posi->RD(pos_index, trans, joint, speed, delay))
        return;

    ui->lbNowPos->setText(name);

    Display_SetPos(trans, joint);

    QString str;
    str.sprintf(FORMAT_SPEED, speed);
    ui->btnSpd->setText(str);

    str.sprintf(FORMAT_TIME, delay);
    ui->btnDelay->setText(str);

    ui->btnSelectPic->hide();

    ModeChange();
}

void dialog_common_position::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    float* joint = pCon->getCurJoint();
    float* trans = pCon->getCurTrans();

    Display_RealPos(trans, joint);

    SetMoving(!pCon->getHoldRun());

    Enable_SelectBtn(!m_bMoving);
}

void dialog_common_position::onPosSelect()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtPosPic.indexOf(btn);
    if(index < 0) return;

    m_nSelectedIndex = index;
    Update(m_nSelectedIndex);

    if(m_bHolding)
        m_bHolding = false;
}

void dialog_common_position::Display_SelectBtn(int btn_index)
{ 
    for(int i=0;i<m_vtPosPic.count();i++)
    {
        m_vtPosTxt[i]->setAutoFillBackground(btn_index == i);
        m_vtPosIcon[i]->setAutoFillBackground(btn_index == i);
    }
}

void dialog_common_position::onTeach()
{
    dialog_position_teaching2* dig = (dialog_position_teaching2*)gGetDialog(DIG_POS_TEACH);
    dig->Init(m_vtPosIndex[m_nSelectedIndex]);

    timer->stop();

    if(dig->exec() == QDialog::Accepted)
        Update(m_nSelectedIndex);

    timer->start();

    gReadyMacro(true);
}
void dialog_common_position::onMove()
{
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setCoordinate(CNR_TC_BASE);

    QString prg_name = m_slProgName[m_nSelectedIndex];
    qDebug() << prg_name;

    int ret=0;

    if(!m_bMoving)
    {
        if(m_bHolding)
        {
            m_bHolding = false;
            ret = pCon->continueProgram(0);
            qDebug() << "continueProgram ret =" << ret;
        }
        else
        {
            ret = pCon->executeProgram(0, prg_name, 1);
            qDebug() << "executeProgram ret =" << ret;
        }
    }
    else
    {
        ret = pCon->holdProgram(0);
        qDebug() << "holdProgram ret =" << ret;

        m_bHolding = true;
    }
}

void dialog_common_position::SetMoving(bool bMoving)
{

    if(pxmMoving.isEmpty())
    {
        QImage img;
        img.load(":/image/image/play.png");
        pxmMoving.append(QPixmap::fromImage(img));
        img.load(":/image/image/stop.png");
        pxmMoving.append(QPixmap::fromImage(img));
    }

    if((int)bMoving < pxmMoving.size())
    {
        ui->btnMoveIcon->setPixmap(pxmMoving[(int)bMoving]);
        if(bMoving)
            ui->btnMove->setText(tr("Stop"));
        else
            ui->btnMove->setText(tr("Move"));

        m_bMoving = bMoving;
    }
}

void dialog_common_position::Enable_SelectBtn(bool enable)
{
    ui->grpPosSelect->setEnabled(enable);

    ui->btnTeachPic->setEnabled(enable);
    ui->btnTeach->setEnabled(enable);
    ui->btnTeachIcon->setEnabled(enable);

    ui->grpEnd->setEnabled(enable);
}

void dialog_common_position::Display_SetPos(cn_trans trans, cn_joint joint)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, trans.p[TRAV]);
    ui->btnXval->setText(str);
    str.sprintf(FORMAT_POS, trans.p[FWDBWD]);
    ui->btnYval->setText(str);
    str.sprintf(FORMAT_POS, trans.p[UPDN]);
    ui->btnZval->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, joint));
    ui->btnRval->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, joint));
    ui->btnSval->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, joint));
    ui->btnJ6val->setText(str);
#else
    str.sprintf(FORMAT_POS, trans.p[TRAV]);
    ui->btnXval->setText(str);
    str.sprintf(FORMAT_POS, trans.p[FWDBWD]);
    ui->btnYval->setText(str);
    str.sprintf(FORMAT_POS, trans.p[UPDN]);
    ui->btnZval->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(joint));
    ui->btnRval->setText(str);
    str.sprintf(FORMAT_POS, joint.joint[JSWV]);
    ui->btnSval->setText(str);

    ui->btnJ6val->setText("");
#endif

}

void dialog_common_position::Display_RealPos(float *trans, float *joint)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, trans[TRAV]);
    ui->btnXreal->setText(str);
    str.sprintf(FORMAT_POS, trans[FWDBWD]);
    ui->btnYreal->setText(str);
    str.sprintf(FORMAT_POS, trans[UPDN]);
    ui->btnZreal->setText(str);

    cn_joint _joint = gMakeCnJoint(joint);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, _joint));
    ui->btnRreal->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, _joint));
    ui->btnSreal->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, _joint));
    ui->btnJ6real->setText(str);
#else
    str.sprintf(FORMAT_POS, trans[TRAV]);
    ui->btnXreal->setText(str);
    str.sprintf(FORMAT_POS, trans[FWDBWD]);
    ui->btnYreal->setText(str);
    str.sprintf(FORMAT_POS, trans[UPDN]);
    ui->btnZreal->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(joint[J2],joint[J3],joint[J4]));
    ui->btnRreal->setText(str);
    str.sprintf(FORMAT_POS, joint[JSWV]);
    ui->btnSreal->setText(str);

    ui->btnJ6real->setText("");
#endif

}
