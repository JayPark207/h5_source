#include "dialog_io.h"
#include "ui_dialog_io.h"

dialog_io::dialog_io(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_io)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

//    timer = new QTimer(this);
//    connect(timer,SIGNAL(timeout()),this,SLOT(onIOTimer()));
//    timer->setInterval(200);
    timer_display = new QTimer(this);
    connect(timer_display,SIGNAL(timeout()),this,SLOT(onDispTimer()));

    connect(ui->btnPUpPic,SIGNAL(mouse_release()),this,SLOT(onUpIn()));
    connect(ui->btnPUpIcon,SIGNAL(mouse_press()),ui->btnPUpPic,SLOT(press()));
    connect(ui->btnPUpIcon,SIGNAL(mouse_release()),ui->btnPUpPic,SLOT(release()));

    connect(ui->btnPDownPic,SIGNAL(mouse_release()),this,SLOT(onDownIn()));
    connect(ui->btnPDownIcon,SIGNAL(mouse_press()),ui->btnPDownPic,SLOT(press()));
    connect(ui->btnPDownIcon,SIGNAL(mouse_release()),ui->btnPDownPic,SLOT(release()));

    connect(ui->btnPUpPic_2,SIGNAL(mouse_release()),this,SLOT(onUpOut()));
    connect(ui->btnPUpIcon_2,SIGNAL(mouse_press()),ui->btnPUpPic_2,SLOT(press()));
    connect(ui->btnPUpIcon_2,SIGNAL(mouse_release()),ui->btnPUpPic_2,SLOT(release()));

    connect(ui->btnPDownPic_2,SIGNAL(mouse_release()),this,SLOT(onDownOut()));
    connect(ui->btnPDownIcon_2,SIGNAL(mouse_press()),ui->btnPDownPic_2,SLOT(press()));
    connect(ui->btnPDownIcon_2,SIGNAL(mouse_release()),ui->btnPDownPic_2,SLOT(release()));

    m_vtISym.clear();m_vtIName.clear();m_vtIPic.clear();
    m_vtISym.append(ui->lbISym);  m_vtIName.append(ui->lbIName);  m_vtIPic.append(ui->lbIPic);
    m_vtISym.append(ui->lbISym_2);m_vtIName.append(ui->lbIName_2);m_vtIPic.append(ui->lbIPic_2);
    m_vtISym.append(ui->lbISym_3);m_vtIName.append(ui->lbIName_3);m_vtIPic.append(ui->lbIPic_3);
    m_vtISym.append(ui->lbISym_4);m_vtIName.append(ui->lbIName_4);m_vtIPic.append(ui->lbIPic_4);
    m_vtISym.append(ui->lbISym_5);m_vtIName.append(ui->lbIName_5);m_vtIPic.append(ui->lbIPic_5);
    m_vtISym.append(ui->lbISym_6);m_vtIName.append(ui->lbIName_6);m_vtIPic.append(ui->lbIPic_6);
    m_vtISym.append(ui->lbISym_7);m_vtIName.append(ui->lbIName_7);m_vtIPic.append(ui->lbIPic_7);
    m_vtISym.append(ui->lbISym_8);m_vtIName.append(ui->lbIName_8);m_vtIPic.append(ui->lbIPic_8);

    m_vtOSym.clear();m_vtOName.clear();m_vtOPic.clear();
    m_vtOSym.append(ui->lbOSym);  m_vtOName.append(ui->lbOName);  m_vtOPic.append(ui->lbOPic);
    m_vtOSym.append(ui->lbOSym_2);m_vtOName.append(ui->lbOName_2);m_vtOPic.append(ui->lbOPic_2);
    m_vtOSym.append(ui->lbOSym_3);m_vtOName.append(ui->lbOName_3);m_vtOPic.append(ui->lbOPic_3);
    m_vtOSym.append(ui->lbOSym_4);m_vtOName.append(ui->lbOName_4);m_vtOPic.append(ui->lbOPic_4);
    m_vtOSym.append(ui->lbOSym_5);m_vtOName.append(ui->lbOName_5);m_vtOPic.append(ui->lbOPic_5);
    m_vtOSym.append(ui->lbOSym_6);m_vtOName.append(ui->lbOName_6);m_vtOPic.append(ui->lbOPic_6);
    m_vtOSym.append(ui->lbOSym_7);m_vtOName.append(ui->lbOName_7);m_vtOPic.append(ui->lbOPic_7);
    m_vtOSym.append(ui->lbOSym_8);m_vtOName.append(ui->lbOName_8);m_vtOPic.append(ui->lbOPic_8);

    int i;
    for(i=0;i<m_vtIPic.count();i++)
    {
        connect(m_vtIPic[i],SIGNAL(mouse_release()),this,SLOT(onSelectIn()));
        connect(m_vtISym[i],SIGNAL(mouse_release()),m_vtIPic[i],SIGNAL(mouse_release()));

        connect(m_vtOPic[i],SIGNAL(mouse_release()),this,SLOT(onSelectOut()));
        connect(m_vtOSym[i],SIGNAL(mouse_release()),m_vtOPic[i],SIGNAL(mouse_release()));
        //connect(m_vtOName[i],SIGNAL(mouse_release()),m_vtOPic[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnOEnablePic,SIGNAL(mouse_release()),this,SLOT(onEnable()));
    connect(ui->btnOEnableIcon,SIGNAL(mouse_press()),ui->btnOEnablePic,SLOT(press()));
    connect(ui->btnOEnableIcon,SIGNAL(mouse_release()),ui->btnOEnablePic,SLOT(release()));

    connect(ui->btnIEnablePic,SIGNAL(mouse_release()),this,SLOT(onForceInput()));
    connect(ui->btnIEnableIcon,SIGNAL(mouse_press()),ui->btnIEnablePic,SLOT(press()));
    connect(ui->btnIEnableIcon,SIGNAL(mouse_release()),ui->btnIEnablePic,SLOT(release()));


    m_nMaxInputPage = USE_INPUT_NUM / 8;
    m_nMaxOutputPage = USE_OUTPUT_NUM / 8;
    m_nInputPage = 0;
    m_nOutputPage = 0;

    top = 0;
    pxmIn.clear();
    pxmOut.clear();
    m_bForceOutput = true;
}

dialog_io::~dialog_io()
{
    delete ui;
}
void dialog_io::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_io::showEvent(QShowEvent *)
{
    //timer->start();
    gStart_IOUpdate();
    timer_display->start(200);

    Display_InputPage(m_nInputPage);
    Display_OutputPage(m_nOutputPage);
    SetEnable(false);
    SetForceInput(false);

    m_bDualSolOff = false;

    SubTop();
}

void dialog_io::hideEvent(QHideEvent *)
{
    //timer->stop();
    gStop_IOUpdate();
    timer_display->stop();
    SetForceInput(false);

    if(m_bDualSolOff)
        Recipe->Set(HyRecipe::bDualSolOff, 1, false);

    m_bForceOutput = true;
}

void dialog_io::onClose()
{
    //gSetMainPage(gGetPreMainPage());

    emit accept();
}

//void dialog_io::onIOTimer()
//{
//    CNRobo* pCon = CNRobo::getInstance();
//    cn_ui32 _in[USE_INPUT_BUFNUM];
//    cn_ui32 _out[USE_OUTPUT_BUFNUM];
//    bool _result;
//    cn_ui32 _mask;
//    int i;

//    pCon->getDI(_in, USE_INPUT_BUFNUM);
//    pCon->getDO(_out, USE_OUTPUT_BUFNUM);

//    for(i=0;i<USE_INPUT_NUM; i++)
//    {
//        if((i % (sizeof(cn_ui32)*8)) == 0)
//            _mask = 1;

//        _result = ((_in[(int)(i/(sizeof(cn_ui32)*8))] & _mask) > 0);
//        _mask <<= 1;

//        Input->SetOn(i, _result);
//    }

//    for(i=0;i<USE_OUTPUT_NUM; i++)
//    {
//        if((i % (sizeof(cn_ui32)*8)) == 0)
//            _mask = 1;

//        _result = ((_out[(int)(i/(sizeof(cn_ui32)*8))] & _mask) > 0);
//        _mask <<= 1;

//        Output->SetOn(i, _result);
//    }

//}

void dialog_io::ForceOutput(bool enable)
{
    m_bForceOutput = enable;
}
void dialog_io::Display_ForceOutput()
{
    ui->wigFO->setEnabled(m_bForceOutput);
}

void dialog_io::onDispTimer()
{
    Display_InputData(m_nInputPage);
    Display_OutputData(m_nOutputPage);
    Display_ForceOutput();
}

void dialog_io::onUpIn()
{
    if(m_nInputPage <= 0)
        return;

    Display_InputPage(--m_nInputPage);

}
void dialog_io::onDownIn()
{
    if(m_nInputPage >= (m_nMaxInputPage-1))
        return;

    Display_InputPage(++m_nInputPage);

}
void dialog_io::onUpOut()
{
    if(m_nOutputPage <= 0)
        return;

    Display_OutputPage(--m_nOutputPage);
}
void dialog_io::onDownOut()
{
    if(m_nOutputPage >= (m_nMaxOutputPage-1))
        return;

    Display_OutputPage(++m_nOutputPage);
}
void dialog_io::onSelectIn()
{
    if(!m_bForceInput) return;

    QString btn = sender()->objectName();
    CNRobo* pCon = CNRobo::getInstance();

    int _in=0, _index;
    int ret;

    for(int i=0;i<m_vtIPic.count();i++)
    {
        if(btn == m_vtIPic[i]->objectName())
        {
            _index = i + (m_nInputPage * m_vtIPic.size());

            if(Input->IsOn(_index))
                _in = (-1)*((_index+1));
            else
                _in = (_index+1);

            ret = pCon->setDI(_in);
            if(ret < 0)
                qDebug() << _in <<" setDI() ret = " << ret;
            return;
        }
    }

}
void dialog_io::onSelectOut()
{
    if(!m_bEnable) return;

    QString btn = sender()->objectName();
    CNRobo* pCon = CNRobo::getInstance();

    int _out=0, _index;
    int ret;

    for(int i=0;i<m_vtOPic.count();i++)
    {
        if(btn == m_vtOPic[i]->objectName())
        {
            _index = i + (m_nOutputPage * m_vtOPic.size());

            if(Output->IsOn(_index))
                _out = (-1)*(_index + 1);
            else
                _out = _index + 1;

            ret = pCon->setDO(&_out, 1);
            if(ret < 0)
                qDebug() << "setDO() ret = " << ret;
            return;
        }
    }

}
void dialog_io::onEnable()
{
    SetEnable(!m_bEnable);
}
void dialog_io::SetEnable(bool enable)
{
    m_bEnable = enable;
    ui->btnOEnableIcon->setAutoFillBackground(m_bEnable);

    if(m_bEnable)
        m_bDualSolOff = true;
}

void dialog_io::onForceInput()
{
    SetForceInput(!m_bForceInput);
}

void dialog_io::SetForceInput(bool enable)
{
    m_bForceInput = enable;
    ui->btnIEnableIcon->setAutoFillBackground(m_bForceInput);

    // virtual input
    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setOverrideDIN(m_bForceInput);
    qDebug() << "setOverrideDIN ret=" << ret << m_bForceInput;
    bool bResult=false;
    pCon->getOverrideDIN(bResult);
    qDebug() << bResult;
}

void dialog_io::Display_InputData(int page)
{
    if(pxmIn.isEmpty())
    {
         QImage img;
         img.load(":/icon/icon/icon992-9");
         pxmIn.append(QPixmap::fromImage(img));
         img.load(":/icon/icon/icon992-10");
         pxmIn.append(QPixmap::fromImage(img));
    }

    if(pxmIn.size() < 2)
        return;

    for(int i=0;i<m_vtIPic.count();i++)
    {
        if(Input->IsOn(i + (page * m_vtIPic.size())))
            m_vtIPic[i]->setPixmap(pxmIn[1]);
        else
            m_vtIPic[i]->setPixmap(pxmIn[0]);
    }

}
void dialog_io::Display_InputPage(int page)
{
    int index;
    for(int i=0;i<m_vtISym.count();i++)
    {
        index = i + (page * m_vtISym.size());
        m_vtISym[i]->setText(Input->GetSym(index));
        m_vtIName[i]->setText(Input->GetName(index));
    }

    ui->lbPageNo->setText(QString().setNum(page + 1));
    ui->lbPageNoMax->setText(QString().setNum(m_nMaxInputPage));
}
void dialog_io::Display_OutputData(int page)
{
    if(pxmOut.isEmpty())
    {
         QImage img;
         img.load(":/icon/icon/icon992-7");
         pxmOut.append(QPixmap::fromImage(img));
         img.load(":/icon/icon/icon992-4");
         pxmOut.append(QPixmap::fromImage(img));
    }

    if(pxmOut.size() < 2)
        return;

    for(int i=0;i<m_vtOPic.count();i++)
    {
        if(Output->IsOn(i + (page * m_vtOPic.size())))
            m_vtOPic[i]->setPixmap(pxmOut[1]);
        else
            m_vtOPic[i]->setPixmap(pxmOut[0]);
    }

}
void dialog_io::Display_OutputPage(int page)
{
    int index;
    for(int i=0;i<m_vtOSym.count();i++)
    {
        index = i + (page * m_vtOSym.size());
        m_vtOSym[i]->setText(Output->GetSym(index));
        m_vtOName[i]->setText(Output->GetName(index));
    }

    ui->lbPageNo_2->setText(QString().setNum(page + 1));
    ui->lbPageNoMax_2->setText(QString().setNum(m_nMaxOutputPage));
}

void dialog_io::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
