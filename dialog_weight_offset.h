#ifndef DIALOG_WEIGHT_OFFSET_H
#define DIALOG_WEIGHT_OFFSET_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_weight_offset;
}

class dialog_weight_offset : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_weight_offset(QWidget *parent = 0);
    ~dialog_weight_offset();

    void Update();


private:
    Ui::dialog_weight_offset *ui;

    QVector<QLabel4*> m_vtValPic;
    QVector<QLabel3*> m_vtVal;
    QVector<QLabel3*> m_vtTitle;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;

    bool m_bSave;

public slots:
    void onClose();

    void onOffset();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_WEIGHT_OFFSET_H
