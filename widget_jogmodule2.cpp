#include "widget_jogmodule2.h"
#include "ui_widget_jogmodule2.h"

widget_jogmodule2::widget_jogmodule2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::widget_jogmodule2)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(300);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnJogSelPic,SIGNAL(mouse_release()),this,SLOT(onJogType()));
    connect(ui->btnJogSel,SIGNAL(mouse_press()),ui->btnJogSelPic,SLOT(press()));
    connect(ui->btnJogSel,SIGNAL(mouse_release()),ui->btnJogSelPic,SLOT(release()));
    connect(ui->btnInchSelPic,SIGNAL(mouse_release()),this,SLOT(onJogType()));
    connect(ui->btnInchSel,SIGNAL(mouse_press()),ui->btnInchSelPic,SLOT(press()));
    connect(ui->btnInchSel,SIGNAL(mouse_release()),ui->btnInchSelPic,SLOT(release()));

    m_vtInch.clear();
    m_vtInch.append(ui->btn10);
    m_vtInch.append(ui->btn1);
    m_vtInch.append(ui->btn0p1);
    m_vtInch.append(ui->btn0p01);

    m_vtInchPic.clear();
    m_vtInchPic.append(ui->btn10Pic);
    m_vtInchPic.append(ui->btn1Pic);
    m_vtInchPic.append(ui->btn0p1Pic);
    m_vtInchPic.append(ui->btn0p01Pic);

    for(int i=0;i<m_vtInchPic.count();i++)
    {
        connect(m_vtInchPic[i],SIGNAL(mouse_release()),this,SLOT(onJogInch()));
        connect(m_vtInch[i],SIGNAL(mouse_press()),m_vtInchPic[i],SLOT(press()));
        connect(m_vtInch[i],SIGNAL(mouse_release()),m_vtInchPic[i],SLOT(release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onSpeedUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onSpeedDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

    m_nJogSpeed = MIN_JOG_SPEED;
    ui->progJogSpd->setMaximum(MAX_JOG_SPEED);
    ui->progJogSpd->setMinimum(0);
}

widget_jogmodule2::~widget_jogmodule2()
{
    delete ui;
}

void widget_jogmodule2::showEvent(QShowEvent *)
{
    SetJogInch(false);

    SetInch(0,false);
    SetInch(1,false);
    SetInch(2,false);
    SetInch(3,true);

    SetSpeed(m_nJogSpeed);

    // Confirm Jog Type
    gSetJogType(JOG_TYPE_HY);

    // Confirm Jog Speed
    CNRobo* pCon = CNRobo::getInstance();
    float data = float(m_nJogSpeed)/100.0;
    pCon->setJogSpeed(0, data);

    timer->start();

}
void widget_jogmodule2::hideEvent(QHideEvent *)
{
    timer->stop();
}
void widget_jogmodule2::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void widget_jogmodule2::onJogType()
{
    QString btn = sender()->objectName();

    if(btn == ui->btnJogSelPic->objectName())
        SetJogInch(false);
    else if(btn == ui->btnInchSelPic->objectName())
        SetJogInch(true);
}
void widget_jogmodule2::onJogInch()
{
    QString btn = sender()->objectName();

    for(int i=0;i<m_vtInchPic.count();i++)
    {
        if(btn == m_vtInchPic[i]->objectName())
            SetInch(i, true);
        else
            SetInch(i, false);
    }
}

void widget_jogmodule2::onSpeedUp()
{
    m_nJogSpeed += JUMP_JOG_SPEED;
    if(m_nJogSpeed >= MAX_JOG_SPEED)
        m_nJogSpeed = MAX_JOG_SPEED;

    SetSpeed(m_nJogSpeed);
}
void widget_jogmodule2::onSpeedDown()
{
    m_nJogSpeed -= JUMP_JOG_SPEED;
    if(m_nJogSpeed <= MIN_JOG_SPEED)
        m_nJogSpeed = MIN_JOG_SPEED;

    SetSpeed(m_nJogSpeed);
}

void widget_jogmodule2::SetJogInch(bool on)
{
    QImage img;
    img.load(":/image/image/button_midrect_4.png");
    QPixmap pxmon = QPixmap::fromImage(img);
    img.load(":/image/image/button_midrect_0.png");
    QPixmap pxmoff = QPixmap::fromImage(img);

    if(on)
    {
        ui->btnJogSelPic->setPixmap(pxmoff);
        ui->btnInchSelPic->setPixmap(pxmon);
        ui->frJogSpd->setEnabled(false);
        ui->frInch->setEnabled(true);
    }
    else
    {
        ui->btnJogSelPic->setPixmap(pxmon);
        ui->btnInchSelPic->setPixmap(pxmoff);
        ui->frJogSpd->setEnabled(true);
        ui->frInch->setEnabled(false);
    }

    CNRobo* pCon = CNRobo::getInstance();
    pCon->setInchingOn(on);
}
void widget_jogmodule2::SetInch(int i, bool on)
{
    QImage img;
    img.load(":/image/image/button_rect2_2.png");
    QPixmap pxmon = QPixmap::fromImage(img);
    img.load(":/image/image/button_rect2_0.png");
    QPixmap pxmoff = QPixmap::fromImage(img);

    CNRobo* pCon = CNRobo::getInstance();

    if(on)
    {
        m_vtInchPic[i]->setPixmap(pxmon);
        pCon->setInchingStep(0, m_vtInch[i]->text().toFloat());
        pCon->setInchingStepAngular(0, m_vtInch[i]->text().toFloat());
    }
    else
    {
        m_vtInchPic[i]->setPixmap(pxmoff);
    }
}

void widget_jogmodule2::SetSpeed(int speed)
{
    CNRobo* pCon = CNRobo::getInstance();
    float data = (float)speed/100.0;
    pCon->setJogSpeed(0, data);
}
void widget_jogmodule2::GetSpeed(int &speed)
{
    CNRobo* pCon = CNRobo::getInstance();
    float data;
    if(pCon->getJogSpeed(0, data) < 0)
        return;

    speed = (int)((data * 100.0) + 0.5);
}
void widget_jogmodule2::onTimer()
{
    GetSpeed(m_nJogSpeed);
    ui->progJogSpd->setValue(m_nJogSpeed);
}
