#include "hytponly.h"

HyTPonly::HyTPonly()
{

}

bool HyTPonly::IsDirExist(QString dir_path)
{
    QDir dir(dir_path);
    return dir.exists();
}

bool HyTPonly::IsFileExist(QString file_path)
{
    QFile file(file_path);
    return file.exists();
}

bool HyTPonly::SaveTxtFile(QString file_path, QStringList file_data)
{
    QFile* file = new QFile(file_path);

    if(file->exists())
        return false;

    if(!file->open(QIODevice::WriteOnly))
        return false;

    QTextStream out(file);
    for(int i=0;i<file_data.size();i++)
    {
        out << file_data[i] << endl;
    }

    file->close();
    delete file;

    return true;
}
