#include "keyboard.h"
#include "ui_keyboard.h"


keyboard::keyboard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::keyboard)
{
    ui->setupUi(this);

    connect(ui->btn0,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn1,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn2,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn3,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn4,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn5,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn6,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn7,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn8,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btn9,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));

    connect(ui->btnQ,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnW,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnE,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnR,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnT,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnY,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnU,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnI,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnO,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnP,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));

    connect(ui->btnA,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnS,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnD,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnF,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnG,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnH,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnJ,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnK,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnL,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));

    connect(ui->btnZ,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnX,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnC,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnV,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnB,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnN,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnM,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));

    connect(ui->btnSpace,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));

    connect(ui->btnChar1,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar2,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar3,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar4,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar5,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar6,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar7,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar8,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar9,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar10,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));
    connect(ui->btnChar11,SIGNAL(clicked()),this,SLOT(KeyboardHandler()));


    SetText("");
    ui->txtView->setFocus();

    // option
    ui->btnFn->hide();

}

keyboard::~keyboard()
{
    delete ui;
}

void keyboard::SetText(QString text)
{
    m_strOutputText = text;
    ui->txtView->setText(m_strOutputText);
}

void keyboard::SetTitle(QString text)
{
    ui->lbTitle->setText(text);
}

void keyboard::KeyboardHandler()
{
    QPushButton* btn = (QPushButton*)sender();

    QString strTemp = btn->text();
    int cursorpos = ui->txtView->cursorPosition();

    if(strTemp == "Space")
    {
        //m_strOutputText += " ";
        m_strOutputText.insert(cursorpos, " ");
    }
    else if(strTemp == "&&")
    {
        m_strOutputText.insert(cursorpos, "&");
    }
    else
    {
        //m_strOutputText += strTemp;
        m_strOutputText.insert(cursorpos, strTemp);
    }

    ui->txtView->setText(m_strOutputText);
    ui->txtView->setCursorPosition(cursorpos+1);
}


void keyboard::on_btnBackspace_clicked()
{
    int cursorpos = ui->txtView->cursorPosition();

    if(cursorpos <= 0)
    {
        ui->txtView->setCursorPosition(cursorpos);
        ui->txtView->setFocus();
        return;
    }

    m_strOutputText.remove(cursorpos-1,1);
    SetText(m_strOutputText);
    ui->txtView->setCursorPosition(cursorpos-1);
}

void keyboard::on_txtView_textChanged(const QString &arg1)
{
    if(m_strOutputText != arg1)
        ui->txtView->setText(m_strOutputText);

    ui->txtView->setFocus();
}

void keyboard::on_btnEsc_clicked()
{
    emit Cancel();
}

void keyboard::on_btnEnter_clicked()
{
    emit Enter();
}

void keyboard::on_txtView_lostFocus()
{
}

void keyboard::on_btnDel_clicked()
{
    int cursorpos = ui->txtView->cursorPosition();

    if(cursorpos >= m_strOutputText.length())
    {
        ui->txtView->setCursorPosition(cursorpos);
        ui->txtView->setFocus();
        return;
    }

    m_strOutputText.remove(cursorpos,1);
    SetText(m_strOutputText);
    ui->txtView->setCursorPosition(cursorpos);
}

void keyboard::on_btnShift_toggled(bool checked)
{
    /*if(ui->btnFn->isChecked())
    {
        ui->btnFn->setChecked(false);
        ui->btnShift->setChecked(!checked);
        return;
    }*/

    QPalette* pal = new QPalette();

    if(checked)
    {
        pal->setBrush(QPalette::Button,QBrush(QColor(255,255,255)));
        ui->btnShift->setPalette(*pal);
        pal->setBrush(QPalette::ButtonText,QBrush(QColor(0,0,0)));
        ui->btnShift->setPalette(*pal);
        ui->btnShift->setText("Shift");


        ui->btnQ->setText("Q");
        ui->btnW->setText("W");
        ui->btnE->setText("E");
        ui->btnR->setText("R");
        ui->btnT->setText("T");
        ui->btnY->setText("Y");
        ui->btnU->setText("U");
        ui->btnI->setText("I");
        ui->btnO->setText("O");
        ui->btnP->setText("P");

        ui->btnA->setText("A");
        ui->btnS->setText("S");
        ui->btnD->setText("D");
        ui->btnF->setText("F");
        ui->btnG->setText("G");
        ui->btnH->setText("H");
        ui->btnJ->setText("J");
        ui->btnK->setText("K");
        ui->btnL->setText("L");

        ui->btnZ->setText("Z");
        ui->btnX->setText("X");
        ui->btnC->setText("C");
        ui->btnV->setText("V");
        ui->btnB->setText("B");
        ui->btnN->setText("N");
        ui->btnM->setText("M");

        ui->btn1->setText("!");
        ui->btn2->setText("@");
        ui->btn3->setText("#");
        ui->btn4->setText("$");
        ui->btn5->setText("%");
        ui->btn6->setText("^");
        ui->btn7->setText("&&");
        ui->btn8->setText("*");
        ui->btn9->setText("(");
        ui->btn0->setText(")");

        ui->btnChar1->setText("~");
        ui->btnChar2->setText("_");
        ui->btnChar3->setText("{");
        ui->btnChar4->setText("}");
        ui->btnChar5->setText("+");
        ui->btnChar6->setText(":");
        ui->btnChar7->setText(QString(QChar('"')));
        ui->btnChar8->setText("|");
        ui->btnChar9->setText("<");
        ui->btnChar10->setText(">");
        ui->btnChar11->setText("?");

    }
    else
    {
        pal->setBrush(QPalette::Button,QBrush(QColor(0,0,0)));
        ui->btnShift->setPalette(*pal);
        pal->setBrush(QPalette::ButtonText,QBrush(QColor(255,255,255)));
        ui->btnShift->setPalette(*pal);
        ui->btnShift->setText("Shift");

        ui->btnQ->setText("q");
        ui->btnW->setText("w");
        ui->btnE->setText("e");
        ui->btnR->setText("r");
        ui->btnT->setText("t");
        ui->btnY->setText("y");
        ui->btnU->setText("u");
        ui->btnI->setText("i");
        ui->btnO->setText("o");
        ui->btnP->setText("p");

        ui->btnA->setText("a");
        ui->btnS->setText("s");
        ui->btnD->setText("d");
        ui->btnF->setText("f");
        ui->btnG->setText("g");
        ui->btnH->setText("h");
        ui->btnJ->setText("j");
        ui->btnK->setText("k");
        ui->btnL->setText("l");

        ui->btnZ->setText("z");
        ui->btnX->setText("x");
        ui->btnC->setText("c");
        ui->btnV->setText("v");
        ui->btnB->setText("b");
        ui->btnN->setText("n");
        ui->btnM->setText("m");

        ui->btn1->setText("1");
        ui->btn2->setText("2");
        ui->btn3->setText("3");
        ui->btn4->setText("4");
        ui->btn5->setText("5");
        ui->btn6->setText("6");
        ui->btn7->setText("7");
        ui->btn8->setText("8");
        ui->btn9->setText("9");
        ui->btn0->setText("0");

        ui->btnChar1->setText("`");
        ui->btnChar2->setText("-");
        ui->btnChar3->setText("[");
        ui->btnChar4->setText("]");
        ui->btnChar5->setText("=");
        ui->btnChar6->setText(";");
        ui->btnChar7->setText("'");
        ui->btnChar8->setText("\\");
        ui->btnChar9->setText(",");
        ui->btnChar10->setText(".");
        ui->btnChar11->setText("/");
    }

    delete pal;

}

void keyboard::on_btnFn_toggled(bool checked)
{
    QPalette* pal = new QPalette();

    if(checked)
    {
        pal->setBrush(QPalette::Button,QBrush(QColor(255,255,255)));
        ui->btnFn->setPalette(*pal);
        pal->setBrush(QPalette::ButtonText,QBrush(QColor(0,0,0)));
        ui->btnFn->setPalette(*pal);
        ui->btnFn->setText("Kor");

        /*ui->btnQ->setText("!");
        ui->btnW->setText("@");
        ui->btnE->setText("#");
        ui->btnR->setText("$");
        ui->btnT->setText("%");
        ui->btnY->setText("^");
        ui->btnU->setText("&&");
        ui->btnI->setText("*");
        ui->btnO->setText("(");
        ui->btnP->setText(")");

        ui->btnA->setText("+");
        ui->btnS->setText("-");
        ui->btnD->setText("/");
        ui->btnF->setText("=");
        ui->btnG->setText("_");
        ui->btnH->setText("[");
        ui->btnJ->setText("]");
        ui->btnK->setText("{");
        ui->btnL->setText("}");

        ui->btnZ->setText(";");
        ui->btnX->setText(":");
        ui->btnC->setText("~");
        ui->btnV->setText(",");
        ui->btnB->setText(".");
        ui->btnN->setText("\\");
        ui->btnM->setText("?");*/

    }
    else
    {
        /*on_btnShift_toggled(ui->btnShift->isChecked());*/

        pal->setBrush(QPalette::Button,QBrush(QColor(0,0,0)));
        ui->btnFn->setPalette(*pal);
        pal->setBrush(QPalette::ButtonText,QBrush(QColor(255,255,255)));
        ui->btnFn->setPalette(*pal);
        ui->btnFn->setText("Eng");
    }

    delete pal;
}

void keyboard::on_btnLeft_clicked()
{
    ui->txtView->cursorBackward(false);
    ui->txtView->setFocus();
}


void keyboard::on_btnRight_clicked()
{
    ui->txtView->cursorForward(false);
    ui->txtView->setFocus();
}

QString keyboard::GetText()
{
    return m_strOutputText;
}

void keyboard::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
