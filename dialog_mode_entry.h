#ifndef DIALOG_MODE_ENTRY_H
#define DIALOG_MODE_ENTRY_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_mode_detail.h"
#include "dialog_mode_easy.h"

namespace Ui {
class dialog_mode_entry;
}

class dialog_mode_entry : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_mode_entry(QWidget *parent = 0);
    ~dialog_mode_entry();

private:
    Ui::dialog_mode_entry *ui;

    dialog_mode_detail* dig_mode_detail;
    dialog_mode_easy* dig_mode_easy;

    QVector<QLabel4*> m_vtEntryPic;
    QVector<QLabel3*> m_vtEntry, m_vtEntryIcon;

public slots:
    void onClose();

    void onEntry();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_MODE_ENTRY_H
