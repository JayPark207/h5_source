#ifndef HYTIME
#define HYTIME

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyrecipe.h"

/** time data array
 * number & buffer class. **/
class HyTime : public QObject
{
    Q_OBJECT

public:
    enum ENUM_TIME_ARRAY
    {
        DELAY = 0,
        CHUCK_LIMIT=0,
        VAC1_LIMIT,
        VAC2_LIMIT,
        VAC3_LIMIT,
        VAC4_LIMIT,
        GRIP_LIMIT,   
        CHUCK_DELAY,
        TAKEOUT_DELAY,
        TEMP_DELAY,
        WEIGHT_DELAY,
        CONV_ON,
        PROD_UNLOAD_DELAY,
        INSERT_GRIP_DELAY,
        INSERT_GRIP_AFTER_DELAY,
        PROD_UNLOAD_AFTER_DELAY,
        NIPPER_LIMIT,
        RGRIP_LIMIT,
        WEIGHT_RESET_DELAY,
        // add here

        TIME_MAX_NUM,
    };

public:
    explicit HyTime(HyRecipe* recipe);

    void Init();
    void AllZero(); // for dev (make 0 datas.)
    QString GetName(int i);

    // direct access case
    bool    WR(int i, float time);
    bool    RD(int i, float* time);
    int     GetNum();

    // in-direct access case
    bool    Read();        // all data read -> save Time vector.
    bool    Write();       // all data write from Time vector.
    void    Set(int i, float time); // write only Time vector. you need Write()
    float   Get(int i);             // read only Time vecotor.

private:
    HyRecipe* m_Recipe;

    QVector<QString>   Name;
    QVector<float>     Time;

    QString m_strArrName;
};

#endif // HYTIME

