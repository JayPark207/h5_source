#include "hylanguage.h"

HyLanguage::HyLanguage()
{
    Init();
}

void HyLanguage::Init()
{
    QImage img;

    m_Flag.clear();
    m_LangName.clear();
    m_Locale.clear();
    m_QmFile.clear();

    img.load(":/image/image/flag_en2");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("EN"));
    m_Locale.append(QLocale(QLocale::English,QLocale::UnitedStates));
    m_QmFile.append(":/lang/lang/lang_en");

    img.load(":/image/image/flag_ko");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("KO"));
    m_Locale.append(QLocale(QLocale::Korean,QLocale::RepublicOfKorea));
    m_QmFile.append(":/lang/lang/lang_ko");

    img.load(":/image/image/flag_zh");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("ZH"));
    m_Locale.append(QLocale(QLocale::Chinese,QLocale::China));
    m_QmFile.append(":/lang/lang/lang_zh");

    img.load(":/image/image/flag_ja");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("JA"));
    m_Locale.append(QLocale(QLocale::Japanese,QLocale::Japan));
    m_QmFile.append(":/lang/lang/lang_ja");

    img.load(":/image/image/flag_de");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("DE"));
    m_Locale.append(QLocale(QLocale::German,QLocale::Germany));
    m_QmFile.append(":/lang/lang/lang_de");

    img.load(":/image/image/flag_ru");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("RU"));
    m_Locale.append(QLocale(QLocale::Russian,QLocale::RussianFederation));
    m_QmFile.append(":/lang/lang/lang_ru");

    img.load(":/image/image/flag_cs");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("CS"));
    m_Locale.append(QLocale(QLocale::Czech,QLocale::CzechRepublic));
    m_QmFile.append(":/lang/lang/lang_cs");

    img.load(":/image/image/flag_fr");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("FR"));
    m_Locale.append(QLocale(QLocale::French,QLocale::France));
    m_QmFile.append(":/lang/lang/lang_fr");

    img.load(":/image/image/flag_es");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("ES"));
    m_Locale.append(QLocale(QLocale::Spanish,QLocale::Spain));
    m_QmFile.append(":/lang/lang/lang_es");

    img.load(":/image/image/flag_it");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("IT"));
    m_Locale.append(QLocale(QLocale::Italian,QLocale::Italy));
    m_QmFile.append(":/lang/lang/lang_it");

    img.load(":/image/image/flag_th");
    m_Flag.append(QPixmap::fromImage(img));
    m_LangName.append(tr("TH"));
    m_Locale.append(QLocale(QLocale::Thai,QLocale::Thailand));
    m_QmFile.append(":/lang/lang/lang_th");
}

void HyLanguage::Init(int lang_index, QTranslator& translator, QLocale& locale)
{
    if(lang_index < 0) lang_index = 0;
    if(lang_index >= m_Locale.size()) lang_index = (m_Locale.size()-1);

    translator.load(m_QmFile[lang_index].toAscii().constData());
    QApplication::installTranslator(&translator);
    locale = m_Locale[lang_index];
}
