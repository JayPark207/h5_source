#ifndef DIALOG_CALL_POSITION_H
#define DIALOG_CALL_POSITION_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"

namespace Ui {
class dialog_call_position;
}

class dialog_call_position : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_call_position(QWidget *parent = 0);
    ~dialog_call_position();

    void Update_List();
    void Redraw_List(int start_index);
    void Display_Data(int list_index);
    QVector<int> m_UsePos;

    cn_trans GetTrans();
    cn_joint GetJoint();

public slots:
    void onOK();

    void onList();
    void onListUp();
    void onListDown();

private:
    Ui::dialog_call_position *ui;

    QVector<QLabel3*> m_vtList;

    int m_nStartIndex;
    int m_nSelectedIndex;

    void ListOn(int table_index);
    void ListOff();

    void Display_SetPos(cn_trans t, cn_joint j);

    cn_trans m_Trans;
    cn_joint m_Joint;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_CALL_POSITION_H
