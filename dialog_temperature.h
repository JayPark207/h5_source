#ifndef DIALOG_TEMPERATURE_H
#define DIALOG_TEMPERATURE_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_mode_select.h"
#include "dialog_message.h"

namespace Ui {
class dialog_temperature;
}

class dialog_temperature : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_temperature(QWidget *parent = 0);
    ~dialog_temperature();

    void Update();

public slots:
    void onEnd();
    void onUse();
    void onMin();
    void onMax();
    void onDelay();

private:
    Ui::dialog_temperature *ui;

    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtUse;
    QVector<QLabel4*> m_vtUsePic;
    QVector<QLabel3*> m_vtMin;
    QVector<QLabel4*> m_vtMinPic;
    QVector<QLabel3*> m_vtMax;
    QVector<QLabel4*> m_vtMaxPic;

    int m_nEnableNum;
    void Enable_Table(int num);

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;

    QStringList m_ItemName;

    dialog_mode_select* sel;
    dialog_numpad* np;
    dialog_message* msg;

    bool Check_Values(QString& msg);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_TEMPERATURE_H
