#include "dialog_home.h"
#include "ui_dialog_home.h"

dialog_home::dialog_home(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_home)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    //this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnHomePic,SIGNAL(mouse_release()),this,SLOT(onMoveHome()));
    connect(ui->btnHome,SIGNAL(mouse_press()),ui->btnHomePic,SLOT(press()));
    connect(ui->btnHome,SIGNAL(mouse_release()),ui->btnHomePic,SLOT(release()));
    connect(ui->btnHomeIcon,SIGNAL(mouse_press()),ui->btnHomePic,SLOT(press()));
    connect(ui->btnHomeIcon,SIGNAL(mouse_release()),ui->btnHomePic,SLOT(release()));

    connect(ui->btnTeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnTeach,SIGNAL(mouse_press()),ui->btnTeachPic,SLOT(press()));
    connect(ui->btnTeach,SIGNAL(mouse_release()),ui->btnTeachPic,SLOT(release()));
    connect(ui->btnTeachIcon,SIGNAL(mouse_press()),ui->btnTeachPic,SLOT(press()));
    connect(ui->btnTeachIcon,SIGNAL(mouse_release()),ui->btnTeachPic,SLOT(release()));

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    fast_timer = new QTimer(this);
    fast_timer->setInterval(50);
    connect(fast_timer,SIGNAL(timeout()),this,SLOT(onFastTimer()));

    // init. variable & pointer.
    top = 0;
    pxmMoving.clear();
    m_bNoMacroOff = false;
    m_bTeach = true;
}

dialog_home::~dialog_home()
{
    delete ui;
}
void dialog_home::showEvent(QShowEvent *)
{
    /*// for exhibition case
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setWZCheckFlag(false);*/
#ifdef _H6_
    ui->tbAxis_6->setText(tr("J6"));
    ui->tbAxis_6->setEnabled(true);
    ui->tbSet_6->setEnabled(true);
    ui->tbReal_6->setEnabled(true);

    /*ui->tbAxis_6->setVisible(true);
    ui->tbSet_6->setVisible(true);
    ui->tbReal_6->setVisible(true);*/
#else
    ui->tbAxis_6->setText("");
    ui->tbAxis_6->setEnabled(false);
    ui->tbSet_6->setEnabled(false);
    ui->tbReal_6->setEnabled(false);

    /*ui->tbAxis_6->setVisible(false);
    ui->tbSet_6->setVisible(false);
    ui->tbReal_6->setVisible(false);*/
#endif

    posteach = (dialog_position_teaching2*)gGetDialog(DIG_POS_TEACH);

    gReadyMacro(true);

    timer->start();

    Update();

    m_bHolding = false;

    m_bTryMoving = false;

    ui->wigTeach->setVisible(m_bTeach);

    SubTop();

    //end check
    m_nState_EndCheck = 0;
    fast_timer->start();

    UserLevel();
}
void dialog_home::hideEvent(QHideEvent *)
{
    onClose();

    if(!m_bNoMacroOff)
        gReadyMacro(false);

    m_bNoMacroOff = false;

    timer->stop();
    fast_timer->stop();

    /*// for exhibition case
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setWZCheckFlag(true);
    pCon->WZSaveAll();*/
}
void dialog_home::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_home::UserLevel()
{
    int user_level = Param->Get(HyParam::USER_LEVEL).toInt();
    if(user_level < USER_LEVEL_MID)
    {
        // 0 level
        ui->wigTeach->hide();
    }
    else
    {
        // 3,7 level
        ui->wigTeach->show();
    }
}

void dialog_home::onClose()
{
    if(m_bTryMoving)
    {
        CNRobo* pCon = CNRobo::getInstance();
        int ret = pCon->clearCurProgram(MAIN_TASK);
        if(ret < 0)
        {
            qDebug() << "dialog_home::onClose() clearCurProgram ret = " << ret;
        }
    }

    float isHome;
    if(Recipe->Get(HyRecipe::sHome, &isHome))
    {
        if(isHome > 0.5)
            emit accept();
        else
            emit reject();
    }
    else
    {
        emit reject();
    }
}

void dialog_home::Init(bool enable_teach, bool no_macro_off)
{
    m_bTeach = enable_teach;
    m_bNoMacroOff = no_macro_off;
}

void dialog_home::Enable_Check()
{
    Recipe->Set(HyRecipe::sHome, 0.0, false);
}

void dialog_home::Update()
{
    QString _name;
    cn_trans _pos;
    cn_joint _joint;
    float _speed,_delay;

    _name = Posi->GetName(HyPos::HOME);

    if(!Posi->RD(HyPos::HOME, _pos, _joint, _speed, _delay))
        return;

    Display_SetPos(_pos, _joint);

    QString str;

    str.sprintf(FORMAT_SPEED, _speed);
    ui->btnSpd->setText(str);

    str.sprintf(FORMAT_TIME, _delay);
    ui->btnDelay->setText(str);

}

void dialog_home::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    float* joint = pCon->getCurJoint();
    float* trans = pCon->getCurTrans();

    Display_RealPos(trans, joint);

    SetMoving(!pCon->getHoldRun()); // hold = 1, run = 0
    Enable_btns(!m_bMoving);
}

void dialog_home::Display_SetPos(cn_trans trans, cn_joint joint)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, trans.p[TRAV]);
    ui->tbSet->setText(str);
    str.sprintf(FORMAT_POS, trans.p[FWDBWD]);
    ui->tbSet_2->setText(str);
    str.sprintf(FORMAT_POS, trans.p[UPDN]);
    ui->tbSet_3->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, joint));
    ui->tbSet_4->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, joint));
    ui->tbSet_5->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, joint));
    ui->tbSet_6->setText(str);
#else
    str.sprintf(FORMAT_POS, trans.p[TRAV]);
    ui->tbSet->setText(str);
    str.sprintf(FORMAT_POS, trans.p[FWDBWD]);
    ui->tbSet_2->setText(str);
    str.sprintf(FORMAT_POS, trans.p[UPDN]);
    ui->tbSet_3->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(joint));
    ui->tbSet_4->setText(str);
    str.sprintf(FORMAT_POS, joint.joint[JSWV]);
    ui->tbSet_5->setText(str);

    ui->tbSet_6->setText("");
#endif

}

void dialog_home::Display_RealPos(float *trans, float *joint)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, trans[0]);
    ui->tbReal->setText(str);
    str.sprintf(FORMAT_POS, trans[1]);
    ui->tbReal_2->setText(str);
    str.sprintf(FORMAT_POS, trans[2]);
    ui->tbReal_3->setText(str);

    cn_joint _joint = gMakeCnJoint(joint);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, _joint));
    ui->tbReal_4->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, _joint));
    ui->tbReal_5->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, _joint));
    ui->tbReal_6->setText(str);
#else
    str.sprintf(FORMAT_POS, trans[0]);
    ui->tbReal->setText(str);
    str.sprintf(FORMAT_POS, trans[1]);
    ui->tbReal_2->setText(str);
    str.sprintf(FORMAT_POS, trans[2]);
    ui->tbReal_3->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(joint[J2],joint[J3],joint[J4]));
    ui->tbReal_4->setText(str);
    str.sprintf(FORMAT_POS, joint[JSWV]);
    ui->tbReal_5->setText(str);

    ui->tbReal_6->setText("");
#endif
}

void dialog_home::onMoveHome()
{
    // testing...

    CNRobo* pCon = CNRobo::getInstance();
    pCon->setCoordinate(CNR_TC_BASE);
    QString prg_name = QString("homemove");
    int ret=0;

    if(!m_bMoving)
    {
        m_nState_EndCheck = 10;

        if(m_bHolding)
        {
            m_bHolding = false;
            ret = pCon->continueProgram(0);
            qDebug() << "continueProgram ret =" << ret;
        }
        else
        {
            //ret = pCon->executeProgram(CNHelper::toMultiByteCode(prg_name), 1);
            ret = pCon->executeProgram(0, prg_name, 1);
            qDebug() << "executeProgram ret =" << ret;
        }

        m_bTryMoving = true; // for clear program.
    }
    else
    {
        m_nState_EndCheck = 20;

        ret = pCon->holdProgram(0);
        //ret = pCon->holdProgram(1);
        qDebug() << "holdProgram ret =" << ret;

        m_bHolding = true;
    }

}

void dialog_home::onTeach()
{
    posteach->Init(HyPos::HOME);

    timer->stop();
    top->timer->stop();
    fast_timer->stop();

    if(posteach->exec() == QDialog::Accepted)
    {
        Update();
    }

    timer->start();
    top->timer->start();
    fast_timer->start();

    gReadyMacro(true);
}

void dialog_home::SetMoving(bool onoff)
{
    if(pxmMoving.isEmpty())
    {
        QImage img;
        img.load(":/image/image/play.png");
        pxmMoving.append(QPixmap::fromImage(img));
        img.load(":/image/image/stop.png");
        pxmMoving.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmMoving.size())
    {
        ui->btnHomeIcon->setPixmap(pxmMoving[(int)onoff]);
        if(onoff)
            ui->btnHome->setText(tr("Stop"));
        else
            ui->btnHome->setText(tr("Home Move"));

        m_bMoving = onoff;
    }
}

void dialog_home::Enable_btns(bool enable)
{
    ui->wigTeach->setEnabled(enable);
    ui->grpEnd->setEnabled(enable);
}


void dialog_home::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}

void dialog_home::onFastTimer()
{
    Process_EndCheck();
}
void dialog_home::Process_EndCheck()
{
    CNRobo* pCon = CNRobo::getInstance();

    switch(m_nState_EndCheck)
    {
    case 0:
        break;

    case 10:
        // check real moving..
        if(m_bMoving)
            m_nState_EndCheck = 11;
        break;

    case 11:
        if(!m_bMoving)
        {
            if(pCon->getTaskStatus(0) == CNR_TASK_NOTUSED)
            {
                dialog_delaying* dig = new dialog_delaying();
                dig->Init(300);
                dig->exec();
                onClose();
            }
            else
                m_nState_EndCheck = 0;
        }
        break;

    case 20:
        m_nState_EndCheck = 0;
        break;

    }

}
