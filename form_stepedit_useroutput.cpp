#include "form_stepedit_useroutput.h"
#include "ui_form_stepedit_useroutput.h"

form_stepedit_useroutput::form_stepedit_useroutput(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form_stepedit_useroutput)
{
    ui->setupUi(this);

    MatchingOutput();

    m_vtIcon.clear();m_vtSel.clear();
    m_vtIcon.append(ui->btnIcon);   m_vtSel.append(ui->btnSel);
    m_vtIcon.append(ui->btnIcon_2); m_vtSel.append(ui->btnSel_2);
    m_vtIcon.append(ui->btnIcon_3); m_vtSel.append(ui->btnSel_3);
    m_vtIcon.append(ui->btnIcon_4); m_vtSel.append(ui->btnSel_4);
    m_vtIcon.append(ui->btnIcon_5); m_vtSel.append(ui->btnSel_5);
    m_vtIcon.append(ui->btnIcon_6); m_vtSel.append(ui->btnSel_6);
    m_vtIcon.append(ui->btnIcon_7); m_vtSel.append(ui->btnSel_7);
    m_vtIcon.append(ui->btnIcon_8); m_vtSel.append(ui->btnSel_8);
    m_vtIcon.append(ui->btnIcon_9); m_vtSel.append(ui->btnSel_9);
    m_vtIcon.append(ui->btnIcon_10);m_vtSel.append(ui->btnSel_10);
    m_vtIcon.append(ui->btnIcon_11);m_vtSel.append(ui->btnSel_11);
    m_vtIcon.append(ui->btnIcon_12);m_vtSel.append(ui->btnSel_12);
    m_vtIcon.append(ui->btnIcon_13);m_vtSel.append(ui->btnSel_13);
    m_vtIcon.append(ui->btnIcon_14);m_vtSel.append(ui->btnSel_14);
    m_vtIcon.append(ui->btnIcon_15);m_vtSel.append(ui->btnSel_15);
    m_vtIcon.append(ui->btnIcon_16);m_vtSel.append(ui->btnSel_16);

    // confirm button event
    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    int i;
    for(i=0;i<m_vtPic.count();i++)
    {
        connect(m_vtPic[i],SIGNAL(mouse_release()),this,SLOT(onOutput()));
        connect(m_vtIcon[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtIcon[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
    }

    m_vtTypePic.clear();m_vtTypeIcon.clear();
    m_vtTypePic.append(ui->tbTypeOffPic);  m_vtTypeIcon.append(ui->tbTypeOffIcon);
    m_vtTypePic.append(ui->tbTypeOnPic);   m_vtTypeIcon.append(ui->tbTypeOnIcon);
    m_vtTypePic.append(ui->tbTypePulsePic);m_vtTypeIcon.append(ui->tbTypePulseIcon);

    for(i=0;i<m_vtTypePic.count();i++)
    {
        connect(m_vtTypePic[i],SIGNAL(mouse_release()),this,SLOT(onType()));
        connect(m_vtTypeIcon[i],SIGNAL(mouse_press()),m_vtTypePic[i],SLOT(press()));
        connect(m_vtTypeIcon[i],SIGNAL(mouse_release()),m_vtTypePic[i],SLOT(release()));
    }

    connect(ui->tbDelayPic,SIGNAL(mouse_release()),this,SLOT(onDelay()));
    connect(ui->tbDelay,SIGNAL(mouse_press()),ui->tbDelayPic,SLOT(press()));
    connect(ui->tbDelay,SIGNAL(mouse_release()),ui->tbDelayPic,SLOT(release()));

    connect(ui->tbPulsePic,SIGNAL(mouse_release()),this,SLOT(onPulse()));
    connect(ui->tbPulse,SIGNAL(mouse_press()),ui->tbPulsePic,SLOT(press()));
    connect(ui->tbPulse,SIGNAL(mouse_release()),ui->tbPulsePic,SLOT(release()));

}

form_stepedit_useroutput::~form_stepedit_useroutput()
{
    delete ui;
}
void form_stepedit_useroutput::showEvent(QShowEvent *)
{
    Update();
}
void form_stepedit_useroutput::hideEvent(QHideEvent *)
{

}
void form_stepedit_useroutput::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void form_stepedit_useroutput::onOK()
{
    if(Save())
    {
        emit sigClose();
    }
    else
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Save Error"));
        msg->Message(tr("Save Fail!"),tr("Try again, please!"));
        msg->exec();
    }
}
void form_stepedit_useroutput::onCancel()
{
    emit sigClose();
}

void form_stepedit_useroutput::MatchingOutput()
{
    m_vtPic.clear();
    m_vtOutputNum.clear();

    // here "m_vtOutputNum in output number" is modify ~~~~!!!!!
    m_vtPic.append(ui->btnPic);    m_vtOutputNum.append(HyOutput::VAC1); // ouput num 1~128 not 0
    m_vtPic.append(ui->btnPic_2);  m_vtOutputNum.append(HyOutput::VAC2);
    m_vtPic.append(ui->btnPic_3);  m_vtOutputNum.append(HyOutput::VAC3);
    m_vtPic.append(ui->btnPic_4);  m_vtOutputNum.append(HyOutput::VAC4);
    m_vtPic.append(ui->btnPic_5);  m_vtOutputNum.append(HyOutput::CHUCK);
    m_vtPic.append(ui->btnPic_6);  m_vtOutputNum.append(HyOutput::GRIP);
    m_vtPic.append(ui->btnPic_7);  m_vtOutputNum.append(HyOutput::NIPPER);
    m_vtPic.append(ui->btnPic_8);  m_vtOutputNum.append(0); // [7]extend
    m_vtPic.append(ui->btnPic_9);  m_vtOutputNum.append(HyOutput::USER1);
    m_vtPic.append(ui->btnPic_10); m_vtOutputNum.append(HyOutput::USER2);
    m_vtPic.append(ui->btnPic_11); m_vtOutputNum.append(HyOutput::USER3);
    m_vtPic.append(ui->btnPic_12); m_vtOutputNum.append(HyOutput::USER4);
    m_vtPic.append(ui->btnPic_13); m_vtOutputNum.append(HyOutput::USER5);
    m_vtPic.append(ui->btnPic_14); m_vtOutputNum.append(HyOutput::USER6);
    m_vtPic.append(ui->btnPic_15); m_vtOutputNum.append(HyOutput::USER7);
    m_vtPic.append(ui->btnPic_16); m_vtOutputNum.append(HyOutput::USER8);

}

void form_stepedit_useroutput::Init(int index, QString title, int buf_index)
{
    m_nSelectedIndex = index;   // stepedit index
    m_strTitle = title;
    m_nBufIndex = buf_index;    // user output buf index. (0~max.user output num-1)
}

void form_stepedit_useroutput::Update()
{
    ui->lbTitle->setText(m_strTitle);
    ui->lbCurrIndex->setText(QString().setNum(m_nSelectedIndex+1));
    ui->lbCurrName->setText(StepEdit->MakeDispName(m_nSelectedIndex));

    if(UserData->RD(m_nBufIndex, &m_Data))
    {
        Set_Extend(0); // reset
        Init_Extend(m_Data);

        DisplayOutput();
        DisplayDelay();
        DisplayPulse();
        SetType((HyUserData::ENUM_TYPE)((int)m_Data.Type));
    }
}

void form_stepedit_useroutput::onOutput()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtPic.indexOf(btn);
    if(index < 0) return;

    if(m_vtSel[index]->isVisible())
    {
        ResetOutput(index);
        if(index == USEROUT_EXTEND)
            Set_Extend(0);
    }
    else
    {
        if(index == USEROUT_EXTEND)
        {
            np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
            np->m_numpad->SetTitle(m_vtIcon[index]->text());
            np->m_numpad->SetNum(0);
            np->m_numpad->SetMinValue(1.0);
            np->m_numpad->SetMaxValue(32.0);
            np->m_numpad->SetSosuNum(0);
            if(np->exec() == QDialog::Accepted)
            {
                int output_num = (int)np->m_numpad->GetNumDouble();
                if(!IsOK_Extend(output_num))
                {
                    // message
                    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
                    msg->SetColor(dialog_message::RED);
                    msg->Title(tr("Error"));
                    QString str;
                    str.sprintf("Y%03d", output_num);
                    msg->Message(tr("This output cannot be selected!"), str);
                    msg->exec();
                    return;
                }

                Set_Extend(output_num);
            }
            else
                return;
        }
        else
        {
            Set_Extend(0);
        }

        SetOutput(index);
    }

    DisplayOutput();
}

void form_stepedit_useroutput::DisplayOutput()
{
    int i;
    for(i=0;i<m_vtSel.count();i++)
        m_vtSel[i]->hide();

    int index;
    for(i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++)
    {
        if(m_Data.Sel[i] < 1) continue;

        index = m_vtOutputNum.indexOf((int)m_Data.Sel[i]);
        if(index >= 0) // on
            m_vtSel[index]->show();
    }
}

void form_stepedit_useroutput::SetOutput(int index)
{
    // 현재중복선택안되도록...
    for(int i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++)
        m_Data.Sel[i] = 0.0;

    m_Data.Sel[0] = (float)m_vtOutputNum[index];
}
void form_stepedit_useroutput::ResetOutput(int index)
{
    // 현재중복선택안되도록...
    int i = index; // index is now nouse.
    for(i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++)
        m_Data.Sel[i] = 0.0;
}

void form_stepedit_useroutput::DisplayDelay()
{
    QString str;
    str.sprintf(FORMAT_TIME, m_Data.Delay);
    ui->tbDelay->setText(str);
}
void form_stepedit_useroutput::DisplayPulse()
{
    QString str;
    str.sprintf(FORMAT_TIME, m_Data.OnTime);
    ui->tbPulse->setText(str);
}

void form_stepedit_useroutput::onDelay()
{
    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->tbDelayTitle->text());
    np->m_numpad->SetNum((double)m_Data.Delay);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(1000.0);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        m_Data.Delay = (float)np->m_numpad->GetNumDouble();
        DisplayDelay();
    }
}

void form_stepedit_useroutput::onPulse()
{
    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->tbPulseTitle->text());
    np->m_numpad->SetNum((double)m_Data.OnTime);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(1000.0);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        m_Data.OnTime = (float)np->m_numpad->GetNumDouble();
        DisplayPulse();
    }
}

void form_stepedit_useroutput::onType()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtTypePic.indexOf(btn);
    if(index < 0) return;

    SetType((HyUserData::ENUM_TYPE)index);
}

void form_stepedit_useroutput::SetType(HyUserData::ENUM_TYPE type)
{
    m_Data.Type = (float)type;

    for(int i=0;i<m_vtTypePic.count();i++)
        m_vtTypePic[i]->setAutoFillBackground(i == (int)type);

    ui->wigPulse->setEnabled(type == HyUserData::TYPE_PULSE);
}

bool form_stepedit_useroutput::Save()
{
    return(UserData->WR(m_nBufIndex, m_Data));
}

void form_stepedit_useroutput::Init_Extend(ST_USERDATA_OUTPUT &data)
{
    int i=0;
    //for(int i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++) // 현재중복안되도록...
    {
        if(IsOK_Extend((int)data.Sel[i]))
        {
            Set_Extend((int)data.Sel[i]);
            return;
        }
    }
}

void form_stepedit_useroutput::Set_Extend(int extend_output_num)
{
    if(extend_output_num < 0)
        extend_output_num = 0;
    if(extend_output_num > 32)
        extend_output_num = 32;

    m_vtOutputNum[USEROUT_EXTEND] = extend_output_num;

    //display
    QString str;
    str.sprintf("Y%03d", m_vtOutputNum[USEROUT_EXTEND]);
    m_vtIcon[USEROUT_EXTEND]->setText(str);
}
bool form_stepedit_useroutput::IsOK_Extend(int output_num)
{
    if(output_num <= 0)
        return false;

    int index = m_vtOutputNum.indexOf(output_num);
    if(index < 0)
    {
        if(output_num < 33
        && output_num != 2 && output_num != 4 && output_num != 6 && output_num != 8 && output_num != 10
        && output_num != 15 && output_num != 16
        && output_num != 17
        && output_num != 18
        && output_num != 19)
            return true;
    }
    return false;
}

