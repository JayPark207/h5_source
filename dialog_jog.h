#ifndef DIALOG_JOG_H
#define DIALOG_JOG_H

#include <QDialog>
#include <QTimer>

#include "qlabel3.h"

#include "widget_jogmodule.h"
#include "widget_jogmodule3.h"
#include "widget_jogmodule3_p.h"

namespace Ui {
class dialog_jog;
}

class dialog_jog : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_jog(QWidget *parent = 0);
    ~dialog_jog();

    void Init_String();

private:
    Ui::dialog_jog *ui;
#ifdef _H6_
    //widget_jogmodule3_p* wig_jogmodule;
    widget_jogmodule3* wig_jogmodule;
#else
    widget_jogmodule3* wig_jogmodule;
#endif

    QTimer* timer;

    QVector<QLabel3*> m_vtAxis;
    QVector<QLabel3*> m_vtReal;

    void Display_Real(float* trans, float* joint);

public slots:
    void onClose();
    void onTimer();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_JOG_H
