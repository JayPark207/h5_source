#ifndef DIALOG_DUALUNLOAD_MON_H
#define DIALOG_DUALUNLOAD_MON_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_confirm.h"
#include "dialog_message.h"

namespace Ui {
class dialog_dualunload_mon;
}

class dialog_dualunload_mon : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_dualunload_mon(QWidget *parent = 0);
    ~dialog_dualunload_mon();

    void Update();

private:
    Ui::dialog_dualunload_mon *ui;
    QTimer* timer;

    QVector<QLabel4*> m_vtCurrPic;
    QVector<QLabel3*> m_vtCurr;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;
    QStringList m_ItemName;

    void Display_Use(float data);
    void Display_Curr(float data);

    bool bSave;

public slots:
    void onClose();

    void onTimer();

    void onCurr();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_DUALUNLOAD_MON_H
