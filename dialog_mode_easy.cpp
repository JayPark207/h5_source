#include "dialog_mode_easy.h"
#include "ui_dialog_mode_easy.h"

dialog_mode_easy::dialog_mode_easy(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_mode_easy)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtList.clear();
    m_vtList.append(ui->lbName);
    m_vtList.append(ui->lbName_2);
    m_vtList.append(ui->lbName_3);
    m_vtList.append(ui->lbName_4);

    int i;
    for(i=0;i<m_vtList.count();i++)
        connect(m_vtList[i],SIGNAL(mouse_release()),this,SLOT(onList()));

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

    connect(ui->btnDetailPic,SIGNAL(mouse_release()),this,SLOT(onDetail()));
    connect(ui->btnDetail,SIGNAL(mouse_press()),ui->btnDetailPic,SLOT(press()));
    connect(ui->btnDetail,SIGNAL(mouse_release()),ui->btnDetailPic,SLOT(release()));

    connect(ui->btnAddPic,SIGNAL(mouse_release()),this,SLOT(onAdd()));
    connect(ui->btnAdd,SIGNAL(mouse_press()),ui->btnAddPic,SLOT(press()));
    connect(ui->btnAdd,SIGNAL(mouse_release()),ui->btnAddPic,SLOT(release()));

    connect(ui->btnDelPic,SIGNAL(mouse_release()),this,SLOT(onDel()));
    connect(ui->btnDel,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDel,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));

    connect(ui->btnSetPic,SIGNAL(mouse_release()),this,SLOT(onSet()));
    connect(ui->btnSet,SIGNAL(mouse_press()),ui->btnSetPic,SLOT(press()));
    connect(ui->btnSet,SIGNAL(mouse_release()),ui->btnSetPic,SLOT(release()));

    connect(ui->btnPreviewPic,SIGNAL(mouse_release()),this,SLOT(onPreview()));
    connect(ui->btnPreview,SIGNAL(mouse_press()),ui->btnPreviewPic,SLOT(press()));
    connect(ui->btnPreview,SIGNAL(mouse_release()),ui->btnPreviewPic,SLOT(release()));


    // init var
    dig_mode_detail = 0;
    dig_mode_easy_view = 0;

    top = 0;
}

dialog_mode_easy::~dialog_mode_easy()
{
    delete ui;
}
void dialog_mode_easy::showEvent(QShowEvent *)
{
    Update();

    SubTop();
}
void dialog_mode_easy::hideEvent(QHideEvent *)
{

}
void dialog_mode_easy::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_mode_easy::onClose()
{
    emit accept();
}

void dialog_mode_easy::Update()
{
    BigMode->Read_List(BigMode->m_List);
    m_nStartIndex=0;m_nSelectedIndex=0;
    Redraw_List(m_nStartIndex);
    Display_Info(m_nSelectedIndex);

}

void dialog_mode_easy::Display_ListOn(int table_index)
{
    for(int i=0;i<m_vtList.count();i++)
        m_vtList[i]->setAutoFillBackground(i == table_index);
}

void dialog_mode_easy::Redraw_List(int start_index)
{
    int cal_size = BigMode->m_List.size() - start_index;
    int index;
    QString no, name;
    HyBigMode::ST_BMODE_INFO info;

    Display_ListOn(-1); // all off

    for(int i=0;i<m_vtList.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            no = BigMode->m_List[index]; // 001,00N...
            BigMode->Get_Info(no.toInt()-1, info);
            name = info.name;
        }
        else
        {
            // no data.
            name.clear();
        }

        m_vtList[i]->setText(name);

        if((start_index + i) == m_nSelectedIndex)
            Display_ListOn(i);
    }
}

void dialog_mode_easy::onList()
{
    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtList.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nStartIndex + index;
    if(list_index == m_nSelectedIndex) return;

    if(list_index < BigMode->m_List.size())
    {
        // list selection process...
        m_nSelectedIndex = list_index;
        Redraw_List(m_nStartIndex);

        Display_Info(m_nSelectedIndex);
    }
}
void dialog_mode_easy::onUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_mode_easy::onDown()
{
    if(m_nStartIndex >= (BigMode->m_List.size() - m_vtList.size()))
        return;

    Redraw_List(++m_nStartIndex);
}

void dialog_mode_easy::onAdd()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnAdd->text());
    conf->Message("Do you want to start easy-mode making?", "Next, Let's Input the index.");
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(1);
    np->m_numpad->SetMaxValue(999);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle("Index");
    np->m_numpad->SetNum(1);

    if(np->exec() != QDialog::Accepted)
        return;

    int input_index = (int)np->m_numpad->GetNumDouble() - 1;

    QString file_name = BigMode->Make_FileName(input_index);
    if(BigMode->m_List.indexOf(file_name) < 0)
    {
        // new
        conf->SetColor(dialog_confirm::GREEN);
        conf->Title(ui->btnAdd->text());
        conf->Message("Do you want to add new?", BigMode->m_DefaultName+file_name);
        if(conf->exec() != QDialog::Accepted)
            return;
    }
    else
    {
        // overwirte.
        conf->SetColor(dialog_confirm::GREEN);
        conf->Title(ui->btnAdd->text());
        conf->Message("Do you want to overwrite?", BigMode->m_DefaultName+file_name);
        if(conf->exec() != QDialog::Accepted)
            return;
    }

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    // 1. read now variable data.
    if(!BigMode->Read_ModeData())
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail"),
                     tr("Read Detail Mode Data"));
        msg->exec();
        return;
    }

    // 2. modedata2filedata
    if(!BigMode->ModeData2FileData(BigMode->m_FileData))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail"),
                     tr("ModeData2FileData"));
        msg->exec();
        return;
    }

    // 3. save file data
    if(!BigMode->Save_FileData(input_index, BigMode->m_FileData))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail"),
                     tr("Save file data"));
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::GREEN);
    msg->Title(QString(tr("Success")));
    msg->Message(tr("Success!!"),
                 tr("Save file data"));
    msg->exec();


    Update();

}

void dialog_mode_easy::onDel()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= BigMode->m_List.size()) return;

    QString select_list = BigMode->m_List[m_nSelectedIndex];

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDel->text());
    conf->Message("Do you want to delete?", BigMode->m_DefaultName+select_list);
    if(conf->exec() != QDialog::Accepted)
        return;

    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDel->text());
    conf->Message("Really, Do you want to delete?", BigMode->m_DefaultName+select_list);
    if(conf->exec() != QDialog::Accepted)
        return;

    int select_index = select_list.toInt() - 1;
    if(select_index < 0) return;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!BigMode->Delete(select_index))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail to delete!"),
                     ui->lbSelName->text());
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::GREEN);
    msg->Title(QString(tr("Success")));
    msg->Message(tr("Success to delete!"),
                 ui->lbSelName->text());
    msg->exec();

    Update();

}
void dialog_mode_easy::onDetail()
{
    if(dig_mode_detail == 0)
        dig_mode_detail = new dialog_mode_detail();

    dig_mode_detail->Set_Readonly(false);
    dig_mode_detail->exec();
}

void dialog_mode_easy::Display_Info(int select_index)
{
    if(select_index < 0) return;
    if(select_index >= BigMode->m_List.size()) return;

    int index = BigMode->m_List[select_index].toInt() - 1;

    HyBigMode::ST_BMODE_INFO info;
    BigMode->Get_Info(index, info);

    ui->lbSelName->setText(info.name);
    ui->lbSelSummary->setText(info.summary);

    QPixmap pxm;
    BigMode->Read_Image(index, pxm);
    ui->lbDescPic->setPixmap(pxm);
}

void dialog_mode_easy::onSet()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= BigMode->m_List.size()) return;

    int index = BigMode->m_List[m_nSelectedIndex].toInt() - 1;
    if(index < 0) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDel->text());
    conf->Message("Do you want to set?", ui->lbSelName->text());
    if(conf->exec() != QDialog::Accepted)
        return;

    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDel->text());
    conf->Message("Really, Do you want to set?", ui->lbSelName->text());
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!BigMode->Read_FileData(index, BigMode->m_FileData))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail to set!"),
                     ui->lbSelName->text());
        msg->exec();
        return;
    }

    if(!BigMode->Write_AllVar(BigMode->m_FileData))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail to set!"),
                     ui->lbSelName->text());
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::GREEN);
    msg->Title(QString(tr("Success")));
    msg->Message(tr("Success to set!"),
                 ui->lbSelName->text());
    msg->exec();

    // postprocess...
}

void dialog_mode_easy::onPreview()
{
    if(dig_mode_easy_view == 0)
        dig_mode_easy_view = new dialog_mode_easy_view();

    int index = BigMode->m_List[m_nSelectedIndex].toInt() - 1;
    dig_mode_easy_view->Init(index);
    dig_mode_easy_view->exec();
}



void dialog_mode_easy::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
