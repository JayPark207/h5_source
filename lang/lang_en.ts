<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>HyBigMode</name>
    <message>
        <location filename="../hybigmode.cpp" line="13"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hybigmode.cpp" line="23"/>
        <source>Mode 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hybigmode.cpp" line="24"/>
        <source>Basic Operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hybigmode.cpp" line="28"/>
        <source>Mode 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hybigmode.cpp" line="29"/>
        <source>Basic Operation &amp; Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hybigmode.cpp" line="48"/>
        <source>N/A</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyError</name>
    <message>
        <location filename="../hyerror.cpp" line="18"/>
        <source>Normal Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="18"/>
        <source>It&apos;s OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="19"/>
        <source>Servo Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="19"/>
        <source>Need to Servo On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="20"/>
        <source>User Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="20"/>
        <source>This is error by users.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="26"/>
        <source>Wait Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="27"/>
        <source>Input wait timeout. Please, Check Input.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="30"/>
        <source>Motor Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="32"/>
        <location filename="../hyerror.cpp" line="33"/>
        <location filename="../hyerror.cpp" line="40"/>
        <source>Interlock Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="32"/>
        <source>Mold Open Sensor OFF or Close Sensor ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="33"/>
        <source>Mold Open &amp; Close Sensor ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="34"/>
        <source>Safety Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="34"/>
        <source>Safety Device enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="36"/>
        <location filename="../hyerror.cpp" line="37"/>
        <location filename="../hyerror.cpp" line="38"/>
        <source>Emergency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="36"/>
        <source>Emergency Robot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="37"/>
        <source>Emergency IMM 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="38"/>
        <source>Emergency IMM 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="40"/>
        <source>Mold is not open.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="42"/>
        <source>Product Takeout Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="42"/>
        <source>Failed to takeout product.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="43"/>
        <source>Product Drop Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="43"/>
        <source>Droped product on the move.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="44"/>
        <source>Product Unload Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="44"/>
        <source>Failed to unload product.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="45"/>
        <source>Product Remove Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="45"/>
        <source>The product remains in the wait position.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="47"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="47"/>
        <source>Temperature measurement timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="48"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="48"/>
        <source>Weight measurement timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="49"/>
        <source>Weight Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="49"/>
        <source>Weight Reset timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="51"/>
        <source>Target Production</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="51"/>
        <source>Target product quantity exceeded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="52"/>
        <source>Maximum Production</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="52"/>
        <source>Reset to zero over maximum.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="54"/>
        <source>Ext.Machine Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="54"/>
        <source>Wait time over.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="55"/>
        <source>Jig Foward Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="55"/>
        <source>Check Jig forward sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="56"/>
        <source>Jig Backward Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="56"/>
        <source>Check Jig backward sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="57"/>
        <source>A-Runner1 Check Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="57"/>
        <source>Check A-runner1 check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="58"/>
        <source>A-Runner2 Check Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="58"/>
        <source>Check A-runner2 check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="59"/>
        <source>A-Runner3 Check Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="59"/>
        <source>Check A-runner3 check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="60"/>
        <source>B-Runner1 Check Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="60"/>
        <source>Check B-runner1 check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="61"/>
        <source>B-Runner2 Check Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="61"/>
        <source>Check B-runner2 check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="62"/>
        <source>B-Runner3 Check Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="62"/>
        <source>Check B-runner3 check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="63"/>
        <source>Jig 2nd-Grip1 Check Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="63"/>
        <source>Check Jig 2nd-Grip1 check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="64"/>
        <source>Jig 2nd-Grip2 Check Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="64"/>
        <source>Check Jig 2nd-Grip2 check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="65"/>
        <source>A-AlignJig Up Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="65"/>
        <location filename="../hyerror.cpp" line="67"/>
        <source>Check A-AlignJig Up check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="66"/>
        <source>B-AlignJig Up Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="66"/>
        <location filename="../hyerror.cpp" line="68"/>
        <source>Check B-AlignJig Up check sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="67"/>
        <source>A-AlignJig Down Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyerror.cpp" line="68"/>
        <source>B-AlignJig Down Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyEuromap</name>
    <message>
        <location filename="../hyeuromap.cpp" line="30"/>
        <source>Common</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="37"/>
        <source>Operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="44"/>
        <source>Safety</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="55"/>
        <source>Mold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="63"/>
        <source>Ejector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="70"/>
        <source>Core 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="76"/>
        <source>Core 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="92"/>
        <source>Emergency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="98"/>
        <location filename="../hyeuromap.cpp" line="104"/>
        <location filename="../hyeuromap.cpp" line="176"/>
        <location filename="../hyeuromap.cpp" line="182"/>
        <location filename="../hyeuromap.cpp" line="282"/>
        <source>Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="110"/>
        <source>Cycle Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="116"/>
        <source>Robot Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="122"/>
        <source>Enable Mold Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="128"/>
        <source>Enable Mold Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="134"/>
        <source>Enable Ejector Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="140"/>
        <source>Enable Ejector Fwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="146"/>
        <source>Enable Core1 to Pos1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="152"/>
        <source>Enable Core1 to Pos2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="158"/>
        <source>Enable Core2 to Pos1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="164"/>
        <source>Enable Core2 to Pos2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="170"/>
        <source>Enable Mold Full Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="192"/>
        <source>Emergency 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="198"/>
        <source>Emergency 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="204"/>
        <source>Safety Device 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="210"/>
        <source>Safety Device 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="216"/>
        <source>Enable Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="222"/>
        <source>Mold Closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="228"/>
        <source>Mold Opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="234"/>
        <source>Ejector Bwd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="240"/>
        <source>Ejector Fwd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="246"/>
        <source>Core1 In Pos1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="252"/>
        <source>Core1 In Pos2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="258"/>
        <source>Core2 In Pos1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="264"/>
        <source>Core2 In Pos2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="270"/>
        <source>Mold Opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyeuromap.cpp" line="276"/>
        <source>Reject</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyInput</name>
    <message>
        <location filename="../hyinput.h" line="77"/>
        <source>Vac.1 Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="79"/>
        <source>Vac.2 Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="81"/>
        <source>Vac.3 Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="83"/>
        <source>Vac.4 Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="85"/>
        <source>Chuck Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="87"/>
        <source>Gripper Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="88"/>
        <source>Nipper Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="92"/>
        <source>Emergency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="102"/>
        <source>User Input 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="103"/>
        <source>User Input 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="104"/>
        <source>User Input 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="105"/>
        <source>User Input 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="106"/>
        <source>User Input 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="107"/>
        <source>User Input 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="108"/>
        <source>User Input 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="109"/>
        <source>User Input 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="111"/>
        <source>Emergency IMM 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="112"/>
        <source>Emergency IMM 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="113"/>
        <source>Safety Device IMM 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="114"/>
        <source>Safety Device IMM 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="115"/>
        <source>Enable Operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="116"/>
        <source>Mold Closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="117"/>
        <source>Mold Opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="118"/>
        <source>Ejector Bwd. Pos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="119"/>
        <source>Ejector Fwd. Pos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="120"/>
        <source>Core 1 in pos.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="121"/>
        <source>Core 1 in pos.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="122"/>
        <source>Core 2 in pos.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="123"/>
        <source>Core 2 in pos.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="124"/>
        <source>Mold Opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="125"/>
        <source>Reject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="131"/>
        <source>Vac.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="132"/>
        <source>Vac.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="133"/>
        <source>Vac.3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="134"/>
        <source>Vac.4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="135"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="136"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="137"/>
        <source>Nipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="138"/>
        <source>R Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="139"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="140"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="141"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="142"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="143"/>
        <source>User 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="144"/>
        <source>User 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="145"/>
        <source>User 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyinput.h" line="146"/>
        <source>User 8</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyLanguage</name>
    <message>
        <location filename="../hylanguage.cpp" line="19"/>
        <source>EN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="25"/>
        <source>KO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="31"/>
        <source>ZH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="37"/>
        <source>JA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="43"/>
        <source>DE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="49"/>
        <source>RU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="55"/>
        <source>CS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="61"/>
        <source>FR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="67"/>
        <source>ES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="73"/>
        <source>IT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylanguage.cpp" line="79"/>
        <source>TH</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyLedBuzzer</name>
    <message>
        <location filename="../hyledbuzzer.cpp" line="24"/>
        <source>DTP7 Device Driver Open Fail!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyLog</name>
    <message>
        <location filename="../hylog.cpp" line="13"/>
        <source>AR Test Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylog.cpp" line="14"/>
        <source>Started logging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylog.cpp" line="15"/>
        <source>Varialble data is saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hylog.cpp" line="45"/>
        <source>User Log : %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyMode</name>
    <message>
        <location filename="../hymode.cpp" line="98"/>
        <source>Jig Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="99"/>
        <source>After Travel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="100"/>
        <source>Before Travel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="101"/>
        <source>During Travel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="117"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="118"/>
        <location filename="../hymode.cpp" line="126"/>
        <location filename="../hymode.cpp" line="142"/>
        <location filename="../hymode.cpp" line="192"/>
        <location filename="../hymode.cpp" line="208"/>
        <location filename="../hymode.cpp" line="231"/>
        <location filename="../hymode.cpp" line="331"/>
        <location filename="../hymode.cpp" line="361"/>
        <location filename="../hymode.cpp" line="369"/>
        <location filename="../hymode.cpp" line="377"/>
        <location filename="../hymode.cpp" line="628"/>
        <location filename="../hymode.cpp" line="648"/>
        <location filename="../hymode.cpp" line="663"/>
        <location filename="../hymode.cpp" line="678"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="119"/>
        <location filename="../hymode.cpp" line="127"/>
        <location filename="../hymode.cpp" line="143"/>
        <location filename="../hymode.cpp" line="193"/>
        <location filename="../hymode.cpp" line="232"/>
        <location filename="../hymode.cpp" line="332"/>
        <location filename="../hymode.cpp" line="362"/>
        <location filename="../hymode.cpp" line="370"/>
        <location filename="../hymode.cpp" line="378"/>
        <location filename="../hymode.cpp" line="629"/>
        <location filename="../hymode.cpp" line="679"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="125"/>
        <source>External Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="141"/>
        <source>Close Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="165"/>
        <source>Takeout Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="166"/>
        <location filename="../hymode.cpp" line="295"/>
        <source>Vac1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="167"/>
        <location filename="../hymode.cpp" line="296"/>
        <source>Vac2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="168"/>
        <location filename="../hymode.cpp" line="297"/>
        <source>Vac3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="169"/>
        <location filename="../hymode.cpp" line="298"/>
        <source>Vac4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="170"/>
        <location filename="../hymode.cpp" line="299"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="171"/>
        <location filename="../hymode.cpp" line="300"/>
        <source>Gripper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="172"/>
        <location filename="../hymode.cpp" line="301"/>
        <source>User1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="173"/>
        <location filename="../hymode.cpp" line="302"/>
        <source>User2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="174"/>
        <location filename="../hymode.cpp" line="303"/>
        <source>User3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="175"/>
        <location filename="../hymode.cpp" line="304"/>
        <source>User4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="191"/>
        <source>Takeout Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="207"/>
        <source>Undercut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="213"/>
        <source>Times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="230"/>
        <source>I-Stop on Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="294"/>
        <source>Unload Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="360"/>
        <source>Unload Middle Pos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="368"/>
        <source>Unload Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="376"/>
        <source>Dual Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="559"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="568"/>
        <source>Safety Device Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="569"/>
        <source>No check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="570"/>
        <source>Check before takeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="571"/>
        <source>Check during takeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="572"/>
        <source>Check always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="578"/>
        <source>Operation Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="579"/>
        <source>Use IMM auto signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="580"/>
        <source>Ignore IMM auto signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="586"/>
        <source>Mold Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="588"/>
        <source>Use to mold opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="594"/>
        <source>Ejector Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="595"/>
        <location filename="../hymode.cpp" line="605"/>
        <location filename="../hymode.cpp" line="616"/>
        <source>Not Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="596"/>
        <source>Fwd ON, Bwd OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="597"/>
        <source>Fwd ON, Bwd ON(no check)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="598"/>
        <source>Fwd ON, Bwd ON(check)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="604"/>
        <source>Core 1 Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="606"/>
        <location filename="../hymode.cpp" line="617"/>
        <source>one-direction pos.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="607"/>
        <location filename="../hymode.cpp" line="618"/>
        <source>one-direction pos.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="608"/>
        <location filename="../hymode.cpp" line="619"/>
        <source>two-direction(no check)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="609"/>
        <location filename="../hymode.cpp" line="620"/>
        <source>two-direction(check)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="615"/>
        <source>Core 2 Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="627"/>
        <source>Interlock Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="283"/>
        <source>Product Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="284"/>
        <location filename="../hymode.cpp" line="587"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="285"/>
        <source>In mold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="286"/>
        <source>Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="287"/>
        <source>Sequential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="288"/>
        <source>Array &amp; Sequential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="330"/>
        <source>Conveyor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="395"/>
        <location filename="../hymode.cpp" line="408"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="560"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="561"/>
        <source>Euromap 67</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="562"/>
        <source>Euromap 12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="647"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="649"/>
        <source>Weight1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="650"/>
        <source>Weight2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="651"/>
        <source>Weight3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="652"/>
        <source>Weight4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="662"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="664"/>
        <source>Temp1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="665"/>
        <source>Temp2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="666"/>
        <source>Temp3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="667"/>
        <source>Temp4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hymode.cpp" line="677"/>
        <source>Ionizer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyNetwork</name>
    <message>
        <location filename="../hynetwork.cpp" line="397"/>
        <location filename="../hynetwork.cpp" line="683"/>
        <source>Company Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="402"/>
        <location filename="../hynetwork.cpp" line="688"/>
        <source>Factory Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="407"/>
        <location filename="../hynetwork.cpp" line="698"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="412"/>
        <location filename="../hynetwork.cpp" line="708"/>
        <source>Error Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="417"/>
        <location filename="../hynetwork.cpp" line="713"/>
        <source>Mold Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="422"/>
        <location filename="../hynetwork.cpp" line="718"/>
        <source>Mold Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="427"/>
        <location filename="../hynetwork.cpp" line="723"/>
        <source>Target Qty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="432"/>
        <location filename="../hynetwork.cpp" line="728"/>
        <source>Cavity Qty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="437"/>
        <location filename="../hynetwork.cpp" line="733"/>
        <source>Product Qty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="442"/>
        <location filename="../hynetwork.cpp" line="738"/>
        <source>Run Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="447"/>
        <location filename="../hynetwork.cpp" line="743"/>
        <source>Machine Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="452"/>
        <location filename="../hynetwork.cpp" line="748"/>
        <source>Cycle Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="457"/>
        <location filename="../hynetwork.cpp" line="753"/>
        <source>Takeout Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="462"/>
        <source>S/W Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="467"/>
        <source>H/W Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="472"/>
        <location filename="../hynetwork.cpp" line="773"/>
        <source>Weight1 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="477"/>
        <location filename="../hynetwork.cpp" line="778"/>
        <source>Weight1 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="482"/>
        <location filename="../hynetwork.cpp" line="783"/>
        <source>Weight1 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="487"/>
        <source>Weight1 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="492"/>
        <location filename="../hynetwork.cpp" line="788"/>
        <source>Weight2 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="497"/>
        <location filename="../hynetwork.cpp" line="793"/>
        <source>Weight2 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="502"/>
        <location filename="../hynetwork.cpp" line="798"/>
        <source>Weight2 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="507"/>
        <source>Weight2 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="512"/>
        <source>Weight3 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="517"/>
        <source>Weight3 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="522"/>
        <source>Weight3 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="527"/>
        <source>Weight3 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="532"/>
        <source>Weight4 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="537"/>
        <source>Weight4 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="542"/>
        <source>Weight4 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="547"/>
        <source>Weight4 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="552"/>
        <location filename="../hynetwork.cpp" line="805"/>
        <source>Temp1 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="557"/>
        <location filename="../hynetwork.cpp" line="810"/>
        <source>Temp1 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="562"/>
        <location filename="../hynetwork.cpp" line="815"/>
        <source>Temp1 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="567"/>
        <source>Temp1 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="572"/>
        <location filename="../hynetwork.cpp" line="820"/>
        <source>Temp2 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="577"/>
        <location filename="../hynetwork.cpp" line="825"/>
        <source>Temp2 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="582"/>
        <location filename="../hynetwork.cpp" line="830"/>
        <source>Temp2 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="587"/>
        <source>Temp2 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="592"/>
        <location filename="../hynetwork.cpp" line="835"/>
        <source>Temp3 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="597"/>
        <location filename="../hynetwork.cpp" line="840"/>
        <source>Temp3 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="602"/>
        <location filename="../hynetwork.cpp" line="845"/>
        <source>Temp3 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="607"/>
        <source>Temp3 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="613"/>
        <location filename="../hynetwork.cpp" line="850"/>
        <source>Temp4 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="618"/>
        <location filename="../hynetwork.cpp" line="855"/>
        <source>Temp4 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="623"/>
        <location filename="../hynetwork.cpp" line="860"/>
        <source>Temp4 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="628"/>
        <source>Temp4 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="634"/>
        <location filename="../hynetwork.cpp" line="865"/>
        <source>E-Sensor1 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="639"/>
        <location filename="../hynetwork.cpp" line="870"/>
        <source>E-Sensor1 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="644"/>
        <location filename="../hynetwork.cpp" line="875"/>
        <source>E-Sensor1 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="649"/>
        <source>E-Sensor1 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="655"/>
        <location filename="../hynetwork.cpp" line="880"/>
        <source>E-Sensor2 Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="660"/>
        <location filename="../hynetwork.cpp" line="885"/>
        <source>E-Sensor2 Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="665"/>
        <location filename="../hynetwork.cpp" line="890"/>
        <source>E-Sensor2 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="670"/>
        <source>E-Sensor2 Judge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="693"/>
        <source>Room Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="703"/>
        <source>Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="758"/>
        <source>TP Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="763"/>
        <source>SC Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hynetwork.cpp" line="768"/>
        <source>NAND Version</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyOutput</name>
    <message>
        <location filename="../hyoutput.h" line="85"/>
        <source>Vac.1 ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="86"/>
        <source>Vac.1 OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="87"/>
        <source>Vac.2 ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="88"/>
        <source>Vac.2 OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="89"/>
        <source>Vac.3 ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="90"/>
        <source>Vac.3 OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="91"/>
        <source>Vac.4 ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="92"/>
        <source>Vac.4 OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="93"/>
        <source>Chuck ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="94"/>
        <source>Chuck OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="95"/>
        <source>Gripper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="96"/>
        <location filename="../hyoutput.h" line="145"/>
        <source>Nipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="99"/>
        <source>Laser ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="100"/>
        <source>Ionizer ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="102"/>
        <source>Buzzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="103"/>
        <source>Main Air Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="104"/>
        <source>Conveyor On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="110"/>
        <source>User Output 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="111"/>
        <source>User Output 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="112"/>
        <source>User Output 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="113"/>
        <source>User Output 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="114"/>
        <source>User Output 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="115"/>
        <source>User Output 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="116"/>
        <source>User Output 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="117"/>
        <source>User Output 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="119"/>
        <source>Emergency Robot 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="120"/>
        <source>Emergency Robot 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="122"/>
        <source>Cycle Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="123"/>
        <source>Robot Operation Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="124"/>
        <source>Enable Mold Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="125"/>
        <source>Mold Area Free</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="126"/>
        <source>Enable Ejector Bwd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="127"/>
        <source>Enable Ejector Fwd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="128"/>
        <source>Enable Core 1 in pos.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="129"/>
        <source>Enable Core 1 in pos.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="130"/>
        <source>Enable Core 2 in pos.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="131"/>
        <source>Enable Core 2 in pos.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="132"/>
        <source>Enable Full Mold Opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="139"/>
        <source>Vac.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="140"/>
        <source>Vac.2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="141"/>
        <source>Vac.3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="142"/>
        <source>Vac.4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="143"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="144"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="146"/>
        <source>R Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="147"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="148"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="149"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="150"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="151"/>
        <source>User 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="152"/>
        <source>User 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="153"/>
        <source>User 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyoutput.h" line="154"/>
        <source>User 8</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyParam</name>
    <message>
        <location filename="../hyparam.cpp" line="20"/>
        <source>First Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="21"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="22"/>
        <source>Recipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="24"/>
        <source>Main Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="25"/>
        <source>User Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="27"/>
        <source>Password Level %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="28"/>
        <source>Factory-In Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="29"/>
        <source>Don&apos;t Touch(Jig Pos.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="30"/>
        <source>Don&apos;t Touch(Sample Pos.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="31"/>
        <source>Don&apos;t Touch(Faulty Pos.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="32"/>
        <source>Don&apos;t Touch(Tune Data)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="33"/>
        <source>Use/Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="34"/>
        <source>Soft Sensor Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="35"/>
        <source>Soft Sensor Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="36"/>
        <source>Data Monitor Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="37"/>
        <source>Recently 10 Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="38"/>
        <source>Sub Task Auto Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyparam.cpp" line="39"/>
        <source>Sub Task Macro Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyPos</name>
    <message>
        <location filename="../hypos.cpp" line="37"/>
        <source>Mechanic Zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="38"/>
        <source>Home1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="39"/>
        <source>Jig Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="40"/>
        <source>Sampling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="41"/>
        <source>Faulty Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="43"/>
        <source>Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="44"/>
        <source>Extenal Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="45"/>
        <source>Take-out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="47"/>
        <source>Under-Cut %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="48"/>
        <source>Up Comp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="49"/>
        <source>Unload Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="50"/>
        <source>Weight Measure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="51"/>
        <source>Insert Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="52"/>
        <source>Insert Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="53"/>
        <source>StopOver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="54"/>
        <source>StopOver2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="55"/>
        <source>Unload Middle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="56"/>
        <source>Unload Start 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="57"/>
        <source>Close Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hypos.cpp" line="58"/>
        <source>Insert Middle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyRecipeManage2</name>
    <message>
        <location filename="../hyrecipemanage2.cpp" line="400"/>
        <source>Please Mold New &amp; Load!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyrecipemanage2.cpp" line="402"/>
        <source>Please Mold Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyrecipemanage2.cpp" line="408"/>
        <source>[%1]Mold Error! Insert SD!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyrecipemanage2.cpp" line="410"/>
        <source>[%1]Mold Error! Check!!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HySasang_Motion</name>
    <message>
        <location filename="../hysasang_motion.cpp" line="14"/>
        <source>P2P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hysasang_motion.cpp" line="15"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hysasang_motion.cpp" line="16"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hysasang_motion.cpp" line="17"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hysasang_motion.cpp" line="18"/>
        <source>Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hysasang_motion.cpp" line="19"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hysasang_motion.cpp" line="21"/>
        <source>#START#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hysasang_motion.cpp" line="22"/>
        <source>#END#</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyStepData</name>
    <message>
        <location filename="../hystepdata.h" line="73"/>
        <source>Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="76"/>
        <source>Take-Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="79"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="82"/>
        <source>Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="85"/>
        <source>External Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="88"/>
        <source>Insert Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="92"/>
        <source>Insert Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="95"/>
        <source>Deburring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="102"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="109"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="116"/>
        <source>Wait Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="123"/>
        <source>Wait Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hystepdata.h" line="130"/>
        <source>S-Work</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyTime</name>
    <message>
        <location filename="../hytime.cpp" line="17"/>
        <source>Before WAIT delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="18"/>
        <source>Chuck Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="19"/>
        <source>Vac.1 Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="20"/>
        <source>Vac.2 Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="21"/>
        <source>Vac.3 Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="22"/>
        <source>Vac.4 Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="23"/>
        <source>Gripper Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="30"/>
        <source>Insert Grip Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="31"/>
        <source>Insert Grip After Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="32"/>
        <source>Prod. After Unload Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="33"/>
        <source>Nipper Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="34"/>
        <source>Gripper2 Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="24"/>
        <source>Chuck Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="25"/>
        <source>Take-out Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="26"/>
        <source>Temp. Measure Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="27"/>
        <source>Weight Measure Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="28"/>
        <source>Conveyor On Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hytime.cpp" line="29"/>
        <source>Prod. Unload Delay</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HyUseSkip</name>
    <message>
        <location filename="../hyuseskip.cpp" line="15"/>
        <source>Button Beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyuseskip.cpp" line="16"/>
        <source>Display Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyuseskip.cpp" line="17"/>
        <source>Tool Coordinate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyuseskip.cpp" line="18"/>
        <source>SoftSensor Monitoring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyuseskip.cpp" line="19"/>
        <source>Skip Ext.Machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../hyuseskip.cpp" line="20"/>
        <source>Skip Flame</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="78"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_anti_vibration</name>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="410"/>
        <source>Anti-Vibration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="612"/>
        <source>Smooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="1625"/>
        <source>Payload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="764"/>
        <source>Smooth Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="904"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="964"/>
        <location filename="../dialog_anti_vibration.cpp" line="129"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="1096"/>
        <source>SEC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="1189"/>
        <source>Filter Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="1249"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.ui" line="1432"/>
        <source>Payload Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_anti_vibration.cpp" line="127"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_array_info</name>
    <message>
        <location filename="../dialog_array_info.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="200"/>
        <location filename="../dialog_array_info.ui" line="2611"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="296"/>
        <location filename="../dialog_array_info.ui" line="2707"/>
        <location filename="../dialog_array_info.ui" line="3571"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="392"/>
        <location filename="../dialog_array_info.ui" line="2803"/>
        <location filename="../dialog_array_info.ui" line="4531"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="488"/>
        <location filename="../dialog_array_info.ui" line="2899"/>
        <location filename="../dialog_array_info.ui" line="5491"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="584"/>
        <location filename="../dialog_array_info.ui" line="2995"/>
        <location filename="../dialog_array_info.ui" line="6451"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="680"/>
        <location filename="../dialog_array_info.ui" line="3091"/>
        <location filename="../dialog_array_info.ui" line="7411"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="776"/>
        <location filename="../dialog_array_info.ui" line="3187"/>
        <location filename="../dialog_array_info.ui" line="8371"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="872"/>
        <location filename="../dialog_array_info.ui" line="3283"/>
        <location filename="../dialog_array_info.ui" line="9331"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="968"/>
        <location filename="../dialog_array_info.ui" line="3379"/>
        <location filename="../dialog_array_info.ui" line="10291"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="1064"/>
        <location filename="../dialog_array_info.ui" line="3475"/>
        <location filename="../dialog_array_info.ui" line="11251"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="1220"/>
        <source>Unload Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="1281"/>
        <source>Insert Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="1406"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="1529"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="1714"/>
        <location filename="../dialog_array_info.ui" line="2503"/>
        <location filename="../dialog_array_info.cpp" line="856"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="1810"/>
        <location filename="../dialog_array_info.ui" line="2029"/>
        <location filename="../dialog_array_info.ui" line="2248"/>
        <location filename="../dialog_array_info.ui" line="12362"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="1933"/>
        <location filename="../dialog_array_info.ui" line="2380"/>
        <location filename="../dialog_array_info.cpp" line="860"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="2152"/>
        <location filename="../dialog_array_info.ui" line="12239"/>
        <location filename="../dialog_array_info.cpp" line="864"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="3667"/>
        <source>1111</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="12615"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="12708"/>
        <source>Z-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="12769"/>
        <source>Z+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="12840"/>
        <source>X-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="12901"/>
        <source>X+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="12972"/>
        <source>Y-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.ui" line="13033"/>
        <source>Y+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.cpp" line="926"/>
        <location filename="../dialog_array_info.cpp" line="1003"/>
        <source>CONFIRM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.cpp" line="929"/>
        <source>Would you want to clear this point?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_info.cpp" line="1006"/>
        <source>Would you want to set this point?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_array_set</name>
    <message>
        <location filename="../dialog_array_set.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="121"/>
        <source>Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="359"/>
        <location filename="../dialog_array_set.ui" line="2274"/>
        <location filename="../dialog_array_set.ui" line="3286"/>
        <location filename="../dialog_array_set.ui" line="3434"/>
        <location filename="../dialog_array_set.ui" line="3809"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="382"/>
        <location filename="../dialog_array_set.ui" line="1432"/>
        <location filename="../dialog_array_set.ui" line="2297"/>
        <location filename="../dialog_array_set.ui" line="3036"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="475"/>
        <location filename="../dialog_array_set.ui" line="2390"/>
        <location filename="../dialog_array_set.ui" line="3161"/>
        <location filename="../dialog_array_set.ui" line="3559"/>
        <location filename="../dialog_array_set.ui" line="3934"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="498"/>
        <location filename="../dialog_array_set.ui" line="1217"/>
        <location filename="../dialog_array_set.ui" line="2413"/>
        <location filename="../dialog_array_set.ui" line="2943"/>
        <location filename="../dialog_array_set.ui" line="4235"/>
        <location filename="../dialog_array_set.ui" line="4627"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="591"/>
        <location filename="../dialog_array_set.ui" line="871"/>
        <location filename="../dialog_array_set.ui" line="1240"/>
        <location filename="../dialog_array_set.ui" line="1455"/>
        <location filename="../dialog_array_set.ui" line="2506"/>
        <location filename="../dialog_array_set.ui" line="2850"/>
        <location filename="../dialog_array_set.ui" line="3411"/>
        <location filename="../dialog_array_set.ui" line="3786"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="614"/>
        <location filename="../dialog_array_set.ui" line="848"/>
        <location filename="../dialog_array_set.ui" line="2529"/>
        <location filename="../dialog_array_set.ui" line="2827"/>
        <location filename="../dialog_array_set.ui" line="4258"/>
        <location filename="../dialog_array_set.ui" line="4380"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="755"/>
        <source>Num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="1002"/>
        <source>Pitch (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="4714"/>
        <location filename="../dialog_array_set.ui" line="4740"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="1025"/>
        <location filename="../dialog_array_set.ui" line="1124"/>
        <location filename="../dialog_array_set.ui" line="1339"/>
        <location filename="../dialog_array_set.ui" line="4534"/>
        <location filename="../dialog_array_set.ui" line="4650"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="1492"/>
        <source>Array Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="1898"/>
        <source>Order Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="1921"/>
        <source>Floor Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="2734"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_array_set.ui" line="4511"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_backup</name>
    <message>
        <location filename="../dialog_backup.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_backup.ui" line="287"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_backup.ui" line="495"/>
        <source>Recovery</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_call_position</name>
    <message>
        <location filename="../dialog_call_position.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1544"/>
        <source>Call Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="188"/>
        <source>Wait Pos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="255"/>
        <source>Pos2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="290"/>
        <source>Pos3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="325"/>
        <source>Pos4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="360"/>
        <source>Pos5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="459"/>
        <source>Pos6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="625"/>
        <source>99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="714"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="807"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="881"/>
        <location filename="../dialog_call_position.ui" line="910"/>
        <location filename="../dialog_call_position.ui" line="939"/>
        <location filename="../dialog_call_position.ui" line="968"/>
        <source>999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1088"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1117"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1146"/>
        <source>Fwd Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1175"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1204"/>
        <source>99999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1233"/>
        <source>Up Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1353"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_call_position.ui" line="1382"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_common_position</name>
    <message>
        <location filename="../dialog_common_position.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="100"/>
        <source>Common Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1542"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1638"/>
        <source>Fwd Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="2231"/>
        <source>Up Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1761"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1884"/>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="2519"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="2423"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="435"/>
        <source>Teaching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="464"/>
        <location filename="../dialog_common_position.cpp" line="295"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="739"/>
        <source>Jig Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1039"/>
        <source>Sample</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1135"/>
        <source>Faulty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1916"/>
        <location filename="../dialog_common_position.ui" line="1948"/>
        <location filename="../dialog_common_position.ui" line="2135"/>
        <location filename="../dialog_common_position.ui" line="2263"/>
        <location filename="../dialog_common_position.ui" line="2327"/>
        <location filename="../dialog_common_position.ui" line="2551"/>
        <location filename="../dialog_common_position.ui" line="2583"/>
        <location filename="../dialog_common_position.ui" line="2615"/>
        <source>999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="2071"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="2103"/>
        <source>99999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="2295"/>
        <source>125.02</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="2739"/>
        <source>Wait Pos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1282"/>
        <source>100.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1381"/>
        <source>0.23</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1404"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.ui" line="1430"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_common_position.cpp" line="293"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_confirm</name>
    <message>
        <location filename="../dialog_confirm.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_confirm.ui" line="193"/>
        <source>Message 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_confirm.ui" line="36"/>
        <source>TITLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_confirm.ui" line="170"/>
        <source>Message 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_confirm.ui" line="359"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_confirm.ui" line="266"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_delaying</name>
    <message>
        <location filename="../dialog_delaying.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_delaying.ui" line="69"/>
        <source>%p%</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_dualunload</name>
    <message>
        <location filename="../dialog_dualunload.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload.ui" line="235"/>
        <source>Dual Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload.ui" line="302"/>
        <location filename="../dialog_dualunload.cpp" line="85"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload.ui" line="699"/>
        <source>Up Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload.ui" line="1071"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload.ui" line="1443"/>
        <source>Fwd Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload.ui" line="1622"/>
        <source>Pitch Offset Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload.cpp" line="86"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_dualunload_mon</name>
    <message>
        <location filename="../dialog_dualunload_mon.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.ui" line="235"/>
        <source>Dual Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.ui" line="414"/>
        <source>USE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.ui" line="512"/>
        <source>1ST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.ui" line="581"/>
        <source>2ND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="109"/>
        <source>Change Unload Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="116"/>
        <source>Do you want to change unload position?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="130"/>
        <location filename="../dialog_dualunload_mon.cpp" line="145"/>
        <location filename="../dialog_dualunload_mon.cpp" line="178"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="131"/>
        <location filename="../dialog_dualunload_mon.cpp" line="146"/>
        <source>Fail to change unload position!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="132"/>
        <location filename="../dialog_dualunload_mon.cpp" line="147"/>
        <location filename="../dialog_dualunload_mon.cpp" line="180"/>
        <source>Plase try again!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="158"/>
        <location filename="../dialog_dualunload_mon.cpp" line="190"/>
        <source>Clear Array Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="159"/>
        <source>Do you want to clear array data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="179"/>
        <source>Fail to clear array data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_dualunload_mon.cpp" line="191"/>
        <source>Success to clear array data!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_e_sensor</name>
    <message>
        <location filename="../dialog_e_sensor.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="140"/>
        <location filename="../dialog_e_sensor.cpp" line="112"/>
        <source>E-Sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="191"/>
        <location filename="../dialog_e_sensor.ui" line="630"/>
        <location filename="../dialog_e_sensor.ui" line="929"/>
        <location filename="../dialog_e_sensor.ui" line="952"/>
        <location filename="../dialog_e_sensor.ui" line="975"/>
        <location filename="../dialog_e_sensor.ui" line="1112"/>
        <location filename="../dialog_e_sensor.ui" line="1135"/>
        <location filename="../dialog_e_sensor.ui" line="1158"/>
        <source>331.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="284"/>
        <location filename="../dialog_e_sensor.cpp" line="151"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="307"/>
        <location filename="../dialog_e_sensor.ui" line="746"/>
        <location filename="../dialog_e_sensor.ui" line="769"/>
        <location filename="../dialog_e_sensor.ui" line="792"/>
        <location filename="../dialog_e_sensor.cpp" line="150"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="607"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="723"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="1644"/>
        <source>0.50 sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="1737"/>
        <source>Delay Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.ui" line="1763"/>
        <source>Unit : Volt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.cpp" line="96"/>
        <source>Value Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.cpp" line="98"/>
        <source>Please input Min &lt; Max.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.cpp" line="187"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_e_sensor.cpp" line="283"/>
        <source>Min Value &gt; Max Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_easy_setting</name>
    <message>
        <location filename="../dialog_easy_setting.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="321"/>
        <source>Easy Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="634"/>
        <source>Under Cut 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="700"/>
        <source>isMoving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="766"/>
        <source>Under Cut 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="832"/>
        <source>Under Cut 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="1241"/>
        <source>Under Cut 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="1419"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="1448"/>
        <location filename="../dialog_easy_setting.ui" line="1789"/>
        <location filename="../dialog_easy_setting.ui" line="1917"/>
        <location filename="../dialog_easy_setting.ui" line="2045"/>
        <location filename="../dialog_easy_setting.ui" line="2173"/>
        <location filename="../dialog_easy_setting.ui" line="2208"/>
        <location filename="../dialog_easy_setting.ui" line="2243"/>
        <location filename="../dialog_easy_setting.ui" line="2398"/>
        <location filename="../dialog_easy_setting.ui" line="2433"/>
        <location filename="../dialog_easy_setting.ui" line="2468"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="1547"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="1667"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="1760"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="1888"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="2016"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="2144"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="2369"/>
        <source>Real</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="2606"/>
        <source>SET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="2711"/>
        <source>JOG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="2907"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="3000"/>
        <source>Moving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="3093"/>
        <source>Arrival</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="3408"/>
        <source>IO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="3523"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="4034"/>
        <source>Fwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="4127"/>
        <source>Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="4672"/>
        <source>FWD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.ui" line="4732"/>
        <source>BWD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="186"/>
        <location filename="../dialog_easy_setting.cpp" line="572"/>
        <source>Notice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="187"/>
        <location filename="../dialog_easy_setting.cpp" line="573"/>
        <source>Home not completed!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="188"/>
        <location filename="../dialog_easy_setting.cpp" line="574"/>
        <source>Please make sure home.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="553"/>
        <source>Do you want to homing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="1247"/>
        <source>Do you want to set this position?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="1256"/>
        <location filename="../dialog_easy_setting.cpp" line="1273"/>
        <location filename="../dialog_easy_setting.cpp" line="1292"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="1257"/>
        <location filename="../dialog_easy_setting.cpp" line="1293"/>
        <source>Fail to save position!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="1274"/>
        <source>Fail to auto save position!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting.cpp" line="1286"/>
        <source>Do you want to set it to the same position?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_easy_setting_io</name>
    <message>
        <location filename="../dialog_easy_setting_io.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_easy_setting_io.ui" line="414"/>
        <location filename="../dialog_easy_setting_io.ui" line="506"/>
        <location filename="../dialog_easy_setting_io.ui" line="840"/>
        <location filename="../dialog_easy_setting_io.ui" line="1037"/>
        <location filename="../dialog_easy_setting_io.ui" line="1097"/>
        <location filename="../dialog_easy_setting_io.ui" line="1294"/>
        <location filename="../dialog_easy_setting_io.ui" line="1491"/>
        <location filename="../dialog_easy_setting_io.ui" line="1688"/>
        <location filename="../dialog_easy_setting_io.ui" line="1885"/>
        <location filename="../dialog_easy_setting_io.ui" line="2082"/>
        <location filename="../dialog_easy_setting_io.ui" line="2279"/>
        <location filename="../dialog_easy_setting_io.ui" line="2476"/>
        <location filename="../dialog_easy_setting_io.ui" line="2673"/>
        <location filename="../dialog_easy_setting_io.ui" line="2870"/>
        <location filename="../dialog_easy_setting_io.ui" line="3067"/>
        <location filename="../dialog_easy_setting_io.ui" line="3531"/>
        <source>Vac. 1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_error</name>
    <message>
        <location filename="../dialog_error.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.ui" line="102"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.ui" line="210"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Need to Servo On&lt;/p&gt;&lt;p&gt;1&lt;/p&gt;&lt;p&gt;2&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.ui" line="245"/>
        <source>-30803</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.ui" line="321"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.ui" line="389"/>
        <source>Servo Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.ui" line="482"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.ui" line="578"/>
        <source>Recently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.cpp" line="145"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error.cpp" line="160"/>
        <source>Error Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_error_list</name>
    <message>
        <location filename="../dialog_error_list.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error_list.ui" line="221"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error_list.ui" line="370"/>
        <source>Error Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_error_list.ui" line="519"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_euromap</name>
    <message>
        <location filename="../dialog_euromap.ui" line="81"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="100"/>
        <source>Interlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="527"/>
        <source>Robot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="596"/>
        <location filename="../dialog_euromap.ui" line="665"/>
        <location filename="../dialog_euromap.ui" line="734"/>
        <location filename="../dialog_euromap.ui" line="803"/>
        <location filename="../dialog_euromap.ui" line="872"/>
        <location filename="../dialog_euromap.ui" line="941"/>
        <location filename="../dialog_euromap.ui" line="1010"/>
        <location filename="../dialog_euromap.ui" line="1079"/>
        <location filename="../dialog_euromap.ui" line="1148"/>
        <location filename="../dialog_euromap.ui" line="1217"/>
        <location filename="../dialog_euromap.ui" line="1286"/>
        <location filename="../dialog_euromap.ui" line="1355"/>
        <location filename="../dialog_euromap.ui" line="1424"/>
        <location filename="../dialog_euromap.ui" line="1493"/>
        <location filename="../dialog_euromap.ui" line="1562"/>
        <location filename="../dialog_euromap.ui" line="1631"/>
        <source>robot signal name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="3870"/>
        <source>IMM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="3939"/>
        <location filename="../dialog_euromap.ui" line="4008"/>
        <location filename="../dialog_euromap.ui" line="4077"/>
        <location filename="../dialog_euromap.ui" line="4146"/>
        <location filename="../dialog_euromap.ui" line="4215"/>
        <location filename="../dialog_euromap.ui" line="4284"/>
        <location filename="../dialog_euromap.ui" line="4353"/>
        <location filename="../dialog_euromap.ui" line="4422"/>
        <location filename="../dialog_euromap.ui" line="4491"/>
        <location filename="../dialog_euromap.ui" line="4560"/>
        <location filename="../dialog_euromap.ui" line="4629"/>
        <location filename="../dialog_euromap.ui" line="4698"/>
        <location filename="../dialog_euromap.ui" line="4767"/>
        <location filename="../dialog_euromap.ui" line="4836"/>
        <location filename="../dialog_euromap.ui" line="4905"/>
        <location filename="../dialog_euromap.ui" line="4974"/>
        <source>imm signal name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="7155"/>
        <source>Signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="7349"/>
        <location filename="../dialog_euromap.ui" line="9109"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="7543"/>
        <source>Simulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="7647"/>
        <source>Mode1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="7740"/>
        <location filename="../dialog_euromap.ui" line="7926"/>
        <source>Info..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="7833"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="8044"/>
        <source>Module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="8164"/>
        <location filename="../dialog_euromap.ui" line="8284"/>
        <location filename="../dialog_euromap.ui" line="8404"/>
        <location filename="../dialog_euromap.ui" line="8524"/>
        <location filename="../dialog_euromap.ui" line="8644"/>
        <location filename="../dialog_euromap.ui" line="8764"/>
        <source>module name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="8959"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="9175"/>
        <location filename="../dialog_euromap.ui" line="9410"/>
        <location filename="../dialog_euromap.ui" line="9514"/>
        <location filename="../dialog_euromap.ui" line="9618"/>
        <location filename="../dialog_euromap.ui" line="9722"/>
        <location filename="../dialog_euromap.ui" line="9826"/>
        <source>data name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="9306"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="10154"/>
        <location filename="../dialog_euromap.ui" line="10214"/>
        <location filename="../dialog_euromap.ui" line="10274"/>
        <location filename="../dialog_euromap.ui" line="10334"/>
        <location filename="../dialog_euromap.ui" line="10394"/>
        <location filename="../dialog_euromap.ui" line="10454"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="10659"/>
        <source>IMM Simulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="10685"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.ui" line="10715"/>
        <source>Panel_3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.cpp" line="324"/>
        <source>EUROMAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.cpp" line="330"/>
        <source>STANDARD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.cpp" line="336"/>
        <source>EUROMAP 67</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.cpp" line="342"/>
        <source>EUROMAP 12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.cpp" line="400"/>
        <location filename="../dialog_euromap.cpp" line="437"/>
        <source>ON -&gt; OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.cpp" line="402"/>
        <location filename="../dialog_euromap.cpp" line="439"/>
        <source>OFF -&gt; ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.cpp" line="691"/>
        <source>STOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_euromap.cpp" line="696"/>
        <source>RUN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_home</name>
    <message>
        <location filename="../dialog_home.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="336"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="508"/>
        <source>Teaching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="371"/>
        <source>Move Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="590"/>
        <source>100.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="689"/>
        <source>0.23</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="712"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="738"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="982"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="1110"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="1230"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="1323"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="1451"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="1932"/>
        <source>Real</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="1011"/>
        <location filename="../dialog_home.ui" line="1352"/>
        <location filename="../dialog_home.ui" line="1480"/>
        <location filename="../dialog_home.ui" line="1608"/>
        <location filename="../dialog_home.ui" line="1736"/>
        <location filename="../dialog_home.ui" line="1771"/>
        <location filename="../dialog_home.ui" line="1806"/>
        <location filename="../dialog_home.ui" line="1961"/>
        <location filename="../dialog_home.ui" line="1996"/>
        <location filename="../dialog_home.ui" line="2031"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="1579"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.ui" line="1707"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.cpp" line="298"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home.cpp" line="300"/>
        <source>Home Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_home2</name>
    <message>
        <location filename="../dialog_home2.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="258"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="287"/>
        <location filename="../dialog_home2.ui" line="628"/>
        <location filename="../dialog_home2.ui" line="756"/>
        <location filename="../dialog_home2.ui" line="884"/>
        <location filename="../dialog_home2.ui" line="1012"/>
        <location filename="../dialog_home2.ui" line="1047"/>
        <location filename="../dialog_home2.ui" line="1082"/>
        <location filename="../dialog_home2.ui" line="1237"/>
        <location filename="../dialog_home2.ui" line="1272"/>
        <location filename="../dialog_home2.ui" line="1307"/>
        <location filename="../dialog_home2.ui" line="1472"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="386"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="506"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="599"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="727"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="855"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="983"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="1208"/>
        <source>Real</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="1373"/>
        <source>100.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="1495"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="1521"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.ui" line="1627"/>
        <source>Move Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.cpp" line="252"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_home2.cpp" line="254"/>
        <source>Home Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_insertmain_set</name>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="118"/>
        <source>Insert Main</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="356"/>
        <location filename="../dialog_insertmain_set.ui" line="2962"/>
        <location filename="../dialog_insertmain_set.ui" line="3974"/>
        <location filename="../dialog_insertmain_set.ui" line="4122"/>
        <location filename="../dialog_insertmain_set.ui" line="4497"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="379"/>
        <location filename="../dialog_insertmain_set.ui" line="1429"/>
        <location filename="../dialog_insertmain_set.ui" line="2985"/>
        <location filename="../dialog_insertmain_set.ui" line="3724"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="472"/>
        <location filename="../dialog_insertmain_set.ui" line="3078"/>
        <location filename="../dialog_insertmain_set.ui" line="3849"/>
        <location filename="../dialog_insertmain_set.ui" line="4247"/>
        <location filename="../dialog_insertmain_set.ui" line="4622"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="495"/>
        <location filename="../dialog_insertmain_set.ui" line="1214"/>
        <location filename="../dialog_insertmain_set.ui" line="2182"/>
        <location filename="../dialog_insertmain_set.ui" line="2574"/>
        <location filename="../dialog_insertmain_set.ui" line="3101"/>
        <location filename="../dialog_insertmain_set.ui" line="3631"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="588"/>
        <location filename="../dialog_insertmain_set.ui" line="868"/>
        <location filename="../dialog_insertmain_set.ui" line="1237"/>
        <location filename="../dialog_insertmain_set.ui" line="1452"/>
        <location filename="../dialog_insertmain_set.ui" line="3194"/>
        <location filename="../dialog_insertmain_set.ui" line="3538"/>
        <location filename="../dialog_insertmain_set.ui" line="4099"/>
        <location filename="../dialog_insertmain_set.ui" line="4474"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="611"/>
        <location filename="../dialog_insertmain_set.ui" line="845"/>
        <location filename="../dialog_insertmain_set.ui" line="2205"/>
        <location filename="../dialog_insertmain_set.ui" line="2327"/>
        <location filename="../dialog_insertmain_set.ui" line="3217"/>
        <location filename="../dialog_insertmain_set.ui" line="3515"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="752"/>
        <source>Num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="999"/>
        <source>Pitch (mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="1022"/>
        <location filename="../dialog_insertmain_set.ui" line="1121"/>
        <location filename="../dialog_insertmain_set.ui" line="1336"/>
        <location filename="../dialog_insertmain_set.ui" line="2481"/>
        <location filename="../dialog_insertmain_set.ui" line="2597"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="1489"/>
        <source>Array Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="1895"/>
        <source>Order Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="1918"/>
        <source>Floor Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="2458"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="2661"/>
        <location filename="../dialog_insertmain_set.ui" line="2687"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertmain_set.ui" line="3422"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_insertsub_set</name>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="112"/>
        <location filename="../dialog_insertsub_set.ui" line="720"/>
        <location filename="../dialog_insertsub_set.ui" line="2422"/>
        <location filename="../dialog_insertsub_set.ui" line="2870"/>
        <location filename="../dialog_insertsub_set.ui" line="3027"/>
        <location filename="../dialog_insertsub_set.ui" line="3248"/>
        <location filename="../dialog_insertsub_set.ui" line="3376"/>
        <location filename="../dialog_insertsub_set.ui" line="3658"/>
        <location filename="../dialog_insertsub_set.ui" line="3786"/>
        <location filename="../dialog_insertsub_set.ui" line="3882"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="173"/>
        <source>User 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="387"/>
        <source>Insert Sub</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="536"/>
        <location filename="../dialog_insertsub_set.ui" line="1203"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="559"/>
        <location filename="../dialog_insertsub_set.ui" line="1110"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="582"/>
        <location filename="../dialog_insertsub_set.ui" line="863"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="605"/>
        <source>Unload Pos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="743"/>
        <source>Heigth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="994"/>
        <source>Offset [mm]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="1017"/>
        <location filename="../dialog_insertsub_set.ui" line="1226"/>
        <location filename="../dialog_insertsub_set.ui" line="1287"/>
        <location filename="../dialog_insertsub_set.ui" line="4306"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="1351"/>
        <location filename="../dialog_insertsub_set.ui" line="1377"/>
        <location filename="../dialog_insertsub_set.ui" line="1403"/>
        <location filename="../dialog_insertsub_set.ui" line="4332"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="1453"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="1680"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="1798"/>
        <source>Pos 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="2102"/>
        <source>Pos 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="2227"/>
        <source>Pos 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="2352"/>
        <source>Pos 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="2515"/>
        <source>Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="2623"/>
        <source>Vac1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="2742"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="2931"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="3120"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="3437"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="3562"/>
        <source>User 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="3911"/>
        <source>User 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="4036"/>
        <source>User 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_insertsub_set.ui" line="4245"/>
        <source>Shift Height</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_io</name>
    <message>
        <location filename="../dialog_io.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="265"/>
        <source>OUTPUT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="294"/>
        <location filename="../dialog_io.ui" line="381"/>
        <location filename="../dialog_io.ui" line="439"/>
        <location filename="../dialog_io.ui" line="497"/>
        <location filename="../dialog_io.ui" line="555"/>
        <location filename="../dialog_io.ui" line="613"/>
        <location filename="../dialog_io.ui" line="671"/>
        <location filename="../dialog_io.ui" line="729"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="323"/>
        <location filename="../dialog_io.ui" line="352"/>
        <location filename="../dialog_io.ui" line="410"/>
        <location filename="../dialog_io.ui" line="468"/>
        <location filename="../dialog_io.ui" line="526"/>
        <location filename="../dialog_io.ui" line="584"/>
        <location filename="../dialog_io.ui" line="642"/>
        <location filename="../dialog_io.ui" line="700"/>
        <source>Y001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="1098"/>
        <location filename="../dialog_io.ui" line="2124"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="1121"/>
        <location filename="../dialog_io.ui" line="2170"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="1156"/>
        <location filename="../dialog_io.ui" line="2147"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="1319"/>
        <source>INPUT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="1348"/>
        <location filename="../dialog_io.ui" line="1406"/>
        <location filename="../dialog_io.ui" line="1464"/>
        <location filename="../dialog_io.ui" line="1522"/>
        <location filename="../dialog_io.ui" line="1580"/>
        <location filename="../dialog_io.ui" line="1638"/>
        <location filename="../dialog_io.ui" line="1696"/>
        <location filename="../dialog_io.ui" line="1783"/>
        <source>Input x001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="1377"/>
        <location filename="../dialog_io.ui" line="1435"/>
        <location filename="../dialog_io.ui" line="1493"/>
        <location filename="../dialog_io.ui" line="1551"/>
        <location filename="../dialog_io.ui" line="1609"/>
        <location filename="../dialog_io.ui" line="1667"/>
        <location filename="../dialog_io.ui" line="1725"/>
        <location filename="../dialog_io.ui" line="1754"/>
        <source>X001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="2379"/>
        <source>Force In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="2536"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_io.ui" line="2197"/>
        <source>I/O</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_jmotion</name>
    <message>
        <location filename="../dialog_jmotion.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="156"/>
        <source>J-Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="639"/>
        <location filename="../dialog_jmotion.ui" line="1043"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="767"/>
        <source>P1-&gt;P2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="953"/>
        <source>P2-&gt;P1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="226"/>
        <source>Not Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="319"/>
        <source>USE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="445"/>
        <source>P1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="570"/>
        <source>P2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="352"/>
        <location filename="../dialog_jmotion.ui" line="860"/>
        <source>Straight Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.ui" line="610"/>
        <location filename="../dialog_jmotion.ui" line="979"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.cpp" line="60"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jmotion.cpp" line="61"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_jog</name>
    <message>
        <location filename="../dialog_jog.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jog.ui" line="127"/>
        <location filename="../dialog_jog.cpp" line="92"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jog.ui" line="156"/>
        <location filename="../dialog_jog.cpp" line="94"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jog.ui" line="185"/>
        <location filename="../dialog_jog.cpp" line="91"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jog.ui" line="305"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jog.ui" line="334"/>
        <location filename="../dialog_jog.cpp" line="95"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jog.ui" line="363"/>
        <location filename="../dialog_jog.cpp" line="93"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jog.ui" line="483"/>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_jog.ui" line="512"/>
        <location filename="../dialog_jog.ui" line="541"/>
        <location filename="../dialog_jog.ui" line="570"/>
        <location filename="../dialog_jog.ui" line="599"/>
        <location filename="../dialog_jog.ui" line="628"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_keyboard</name>
    <message>
        <location filename="../dialog_keyboard.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_language</name>
    <message>
        <location filename="../dialog_language.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_language.ui" line="186"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_language.ui" line="460"/>
        <source>EN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_log</name>
    <message>
        <location filename="../dialog_log.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="200"/>
        <location filename="../dialog_log.ui" line="3568"/>
        <source>~</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="299"/>
        <location filename="../dialog_log.ui" line="3600"/>
        <source>2016-99-99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="398"/>
        <location filename="../dialog_log.ui" line="3455"/>
        <source>2016-00-00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="1643"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="1736"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="1829"/>
        <source>Date/Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2014"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2114"/>
        <source>10000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2140"/>
        <source>Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2210"/>
        <source>Error Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2617"/>
        <source>2016</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2649"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2680"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2703"/>
        <source>2016-08-01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="2896"/>
        <source>12:55:55</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="3202"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="3244"/>
        <source>1 Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.ui" line="3324"/>
        <source>Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="553"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="554"/>
        <source>Not find USB memory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="555"/>
        <source>Plase insert USB and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="577"/>
        <source>CONFIRM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="578"/>
        <source>Would you want to save log in USB?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="627"/>
        <source>SUCCESS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="628"/>
        <source>Success to log file export!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="620"/>
        <source>FAIL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_log.cpp" line="621"/>
        <source>Fail to log file export! Try again!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_manual</name>
    <message>
        <location filename="../dialog_manual.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="250"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="475"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="504"/>
        <location filename="../dialog_manual.cpp" line="275"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="624"/>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="653"/>
        <location filename="../dialog_manual.ui" line="711"/>
        <location filename="../dialog_manual.ui" line="769"/>
        <location filename="../dialog_manual.ui" line="827"/>
        <location filename="../dialog_manual.ui" line="885"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="682"/>
        <location filename="../dialog_manual.cpp" line="276"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="740"/>
        <location filename="../dialog_manual.cpp" line="277"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="798"/>
        <location filename="../dialog_manual.cpp" line="278"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="856"/>
        <location filename="../dialog_manual.cpp" line="279"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="1065"/>
        <source>Trans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="1161"/>
        <source>HY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="1257"/>
        <source>Joint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="1477"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="1544"/>
        <location filename="../dialog_manual.ui" line="2154"/>
        <location filename="../dialog_manual.ui" line="2183"/>
        <location filename="../dialog_manual.ui" line="2212"/>
        <location filename="../dialog_manual.ui" line="2241"/>
        <location filename="../dialog_manual.ui" line="2270"/>
        <location filename="../dialog_manual.ui" line="2299"/>
        <location filename="../dialog_manual.ui" line="2328"/>
        <source>X001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="1573"/>
        <location filename="../dialog_manual.ui" line="2623"/>
        <location filename="../dialog_manual.ui" line="2652"/>
        <location filename="../dialog_manual.ui" line="2681"/>
        <location filename="../dialog_manual.ui" line="2710"/>
        <location filename="../dialog_manual.ui" line="2739"/>
        <location filename="../dialog_manual.ui" line="2768"/>
        <location filename="../dialog_manual.ui" line="2797"/>
        <source>Y001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="1731"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="2981"/>
        <location filename="../dialog_manual.ui" line="3101"/>
        <source>1/6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="3381"/>
        <source>Vac. 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="3410"/>
        <source>Vac. 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="3809"/>
        <source>Vac. 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="4004"/>
        <source>Vac. 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="4033"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="4228"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="6217"/>
        <source>R Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="4729"/>
        <source>Nipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="4758"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="4953"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="5014"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="5381"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="5410"/>
        <source>User 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="5653"/>
        <source>User 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="5950"/>
        <source>User 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_manual.ui" line="5979"/>
        <source>User 8</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_message</name>
    <message>
        <location filename="../dialog_message.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_message.ui" line="199"/>
        <source>Message 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_message.ui" line="42"/>
        <source>TITLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_message.ui" line="176"/>
        <source>Message 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_message.ui" line="272"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_mode_detail</name>
    <message>
        <location filename="../dialog_mode_detail.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="100"/>
        <source>MODE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="218"/>
        <source>Common</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="751"/>
        <source>Takeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="774"/>
        <source>Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="797"/>
        <source>Interlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="820"/>
        <source>Sensors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="1236"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="1329"/>
        <location filename="../dialog_mode_detail.ui" line="1544"/>
        <location filename="../dialog_mode_detail.ui" line="1567"/>
        <location filename="../dialog_mode_detail.ui" line="1718"/>
        <location filename="../dialog_mode_detail.ui" line="1840"/>
        <location filename="../dialog_mode_detail.ui" line="1962"/>
        <location filename="../dialog_mode_detail.ui" line="2084"/>
        <location filename="../dialog_mode_detail.ui" line="2206"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="1422"/>
        <location filename="../dialog_mode_detail.ui" line="1518"/>
        <location filename="../dialog_mode_detail.ui" line="1625"/>
        <location filename="../dialog_mode_detail.ui" line="1785"/>
        <location filename="../dialog_mode_detail.ui" line="1907"/>
        <location filename="../dialog_mode_detail.ui" line="2029"/>
        <location filename="../dialog_mode_detail.ui" line="2151"/>
        <location filename="../dialog_mode_detail.ui" line="2273"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_detail.ui" line="1489"/>
        <location filename="../dialog_mode_detail.ui" line="1596"/>
        <location filename="../dialog_mode_detail.ui" line="1695"/>
        <location filename="../dialog_mode_detail.ui" line="1817"/>
        <location filename="../dialog_mode_detail.ui" line="1939"/>
        <location filename="../dialog_mode_detail.ui" line="2061"/>
        <location filename="../dialog_mode_detail.ui" line="2183"/>
        <location filename="../dialog_mode_detail.ui" line="2480"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_mode_easy</name>
    <message>
        <location filename="../dialog_mode_easy.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="410"/>
        <source>Easy Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="515"/>
        <location filename="../dialog_mode_easy.ui" line="1178"/>
        <source>MODE 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="635"/>
        <source>Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="1207"/>
        <source>summary
summary2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="1370"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="1475"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="1580"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="1685"/>
        <source>Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.ui" line="1790"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="214"/>
        <location filename="../dialog_mode_easy.cpp" line="225"/>
        <location filename="../dialog_mode_easy.cpp" line="236"/>
        <location filename="../dialog_mode_easy.cpp" line="282"/>
        <location filename="../dialog_mode_easy.cpp" line="351"/>
        <location filename="../dialog_mode_easy.cpp" line="361"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="215"/>
        <location filename="../dialog_mode_easy.cpp" line="226"/>
        <location filename="../dialog_mode_easy.cpp" line="237"/>
        <source>Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="216"/>
        <source>Read Detail Mode Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="227"/>
        <source>ModeData2FileData</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="238"/>
        <location filename="../dialog_mode_easy.cpp" line="246"/>
        <source>Save file data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="244"/>
        <location filename="../dialog_mode_easy.cpp" line="290"/>
        <location filename="../dialog_mode_easy.cpp" line="369"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="245"/>
        <source>Success!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="283"/>
        <source>Fail to delete!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="291"/>
        <source>Success to delete!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="352"/>
        <location filename="../dialog_mode_easy.cpp" line="362"/>
        <source>Fail to set!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy.cpp" line="370"/>
        <source>Success to set!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_mode_easy_view</name>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="270"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="363"/>
        <location filename="../dialog_mode_easy_view.ui" line="546"/>
        <location filename="../dialog_mode_easy_view.ui" line="575"/>
        <location filename="../dialog_mode_easy_view.ui" line="694"/>
        <location filename="../dialog_mode_easy_view.ui" line="784"/>
        <location filename="../dialog_mode_easy_view.ui" line="874"/>
        <location filename="../dialog_mode_easy_view.ui" line="964"/>
        <location filename="../dialog_mode_easy_view.ui" line="1054"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="456"/>
        <location filename="../dialog_mode_easy_view.ui" line="514"/>
        <location filename="../dialog_mode_easy_view.ui" line="633"/>
        <location filename="../dialog_mode_easy_view.ui" line="723"/>
        <location filename="../dialog_mode_easy_view.ui" line="813"/>
        <location filename="../dialog_mode_easy_view.ui" line="903"/>
        <location filename="../dialog_mode_easy_view.ui" line="993"/>
        <location filename="../dialog_mode_easy_view.ui" line="1083"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="485"/>
        <location filename="../dialog_mode_easy_view.ui" line="604"/>
        <location filename="../dialog_mode_easy_view.ui" line="665"/>
        <location filename="../dialog_mode_easy_view.ui" line="755"/>
        <location filename="../dialog_mode_easy_view.ui" line="845"/>
        <location filename="../dialog_mode_easy_view.ui" line="935"/>
        <location filename="../dialog_mode_easy_view.ui" line="1025"/>
        <location filename="../dialog_mode_easy_view.ui" line="1259"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="1298"/>
        <source>Common</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="1831"/>
        <source>Takeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="1854"/>
        <source>Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="1877"/>
        <source>Interlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="1900"/>
        <source>Sensors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.ui" line="2419"/>
        <source>MODE 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.cpp" line="96"/>
        <location filename="../dialog_mode_easy_view.cpp" line="107"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_easy_view.cpp" line="97"/>
        <location filename="../dialog_mode_easy_view.cpp" line="108"/>
        <source>Fail to file read.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_mode_entry</name>
    <message>
        <location filename="../dialog_mode_entry.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_entry.ui" line="175"/>
        <source>Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_entry.ui" line="417"/>
        <source>Easy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_mode_select</name>
    <message>
        <location filename="../dialog_mode_select.ui" line="81"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_select.ui" line="103"/>
        <location filename="../dialog_mode_select.ui" line="126"/>
        <location filename="../dialog_mode_select.ui" line="187"/>
        <location filename="../dialog_mode_select.ui" line="229"/>
        <location filename="../dialog_mode_select.ui" line="271"/>
        <location filename="../dialog_mode_select.ui" line="294"/>
        <location filename="../dialog_mode_select.ui" line="336"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_select.ui" line="447"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_mode_select.ui" line="540"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_moldclean</name>
    <message>
        <location filename="../dialog_moldclean.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="893"/>
        <location filename="../dialog_moldclean.cpp" line="75"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="284"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="140"/>
        <source>Mold Cleaner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="261"/>
        <source>ON Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="415"/>
        <source>ON Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="531"/>
        <location filename="../dialog_moldclean.ui" line="592"/>
        <source>0.05</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="659"/>
        <source>Cycles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="688"/>
        <location filename="../dialog_moldclean.ui" line="717"/>
        <source>Sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="832"/>
        <source>USE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.ui" line="508"/>
        <source>ON Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_moldclean.cpp" line="76"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_motor</name>
    <message>
        <location filename="../dialog_motor.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="186"/>
        <source>Motor &amp; Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="352"/>
        <source>Test1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="365"/>
        <source>Test2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="378"/>
        <source>Test3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="477"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="570"/>
        <source>Driver Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="599"/>
        <location filename="../dialog_motor.ui" line="1226"/>
        <location filename="../dialog_motor.ui" line="1429"/>
        <location filename="../dialog_motor.ui" line="1632"/>
        <location filename="../dialog_motor.ui" line="1835"/>
        <location filename="../dialog_motor.ui" line="2038"/>
        <location filename="../dialog_motor.ui" line="2241"/>
        <location filename="../dialog_motor.ui" line="2415"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="628"/>
        <location filename="../dialog_motor.ui" line="1168"/>
        <location filename="../dialog_motor.ui" line="1371"/>
        <location filename="../dialog_motor.ui" line="1574"/>
        <location filename="../dialog_motor.ui" line="1777"/>
        <location filename="../dialog_motor.ui" line="1980"/>
        <location filename="../dialog_motor.ui" line="2183"/>
        <location filename="../dialog_motor.ui" line="2386"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="657"/>
        <location filename="../dialog_motor.ui" line="1081"/>
        <location filename="../dialog_motor.ui" line="1284"/>
        <location filename="../dialog_motor.ui" line="1487"/>
        <location filename="../dialog_motor.ui" line="1690"/>
        <location filename="../dialog_motor.ui" line="1893"/>
        <location filename="../dialog_motor.ui" line="2096"/>
        <location filename="../dialog_motor.ui" line="2444"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="750"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="843"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="936"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="2566"/>
        <source>Torque</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="2895"/>
        <source>SERVO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="2988"/>
        <location filename="../dialog_motor.ui" line="3323"/>
        <source>RESET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="3061"/>
        <source>ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor.ui" line="3108"/>
        <source>Control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_motor_test</name>
    <message>
        <location filename="../dialog_motor_test.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="186"/>
        <source>Motor Test Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="658"/>
        <source>Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="751"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="873"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="2536"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="2662"/>
        <location filename="../dialog_motor_test.ui" line="2784"/>
        <location filename="../dialog_motor_test.ui" line="2999"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="2755"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="2877"/>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="2970"/>
        <source>Decel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="3129"/>
        <source>Accel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="3354"/>
        <location filename="../dialog_motor_test.ui" line="3690"/>
        <source>Real Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="3846"/>
        <location filename="../dialog_motor_test.cpp" line="525"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.ui" line="3881"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_motor_test.cpp" line="517"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_network</name>
    <message>
        <location filename="../dialog_network.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="186"/>
        <source>NETWORK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="615"/>
        <location filename="../dialog_network.ui" line="2216"/>
        <location filename="../dialog_network.ui" line="9009"/>
        <source>POP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="809"/>
        <location filename="../dialog_network.ui" line="6390"/>
        <location filename="../dialog_network.ui" line="9203"/>
        <source>HY Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="1003"/>
        <source>Serial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="1197"/>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="1322"/>
        <location filename="../dialog_network.ui" line="7925"/>
        <source>Company Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="1382"/>
        <location filename="../dialog_network.ui" line="1586"/>
        <location filename="../dialog_network.ui" line="1790"/>
        <location filename="../dialog_network.ui" line="1994"/>
        <location filename="../dialog_network.ui" line="7985"/>
        <location filename="../dialog_network.ui" line="8189"/>
        <location filename="../dialog_network.ui" line="8393"/>
        <location filename="../dialog_network.ui" line="8597"/>
        <source>000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="1526"/>
        <location filename="../dialog_network.ui" line="8129"/>
        <source>Factory Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="1730"/>
        <location filename="../dialog_network.ui" line="8333"/>
        <source>Machine Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="1934"/>
        <location filename="../dialog_network.ui" line="8537"/>
        <source>Room Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="2314"/>
        <location filename="../dialog_network.ui" line="6488"/>
        <location filename="../dialog_network.cpp" line="852"/>
        <location filename="../dialog_network.cpp" line="1082"/>
        <source>NOT USE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="2491"/>
        <location filename="../dialog_network.ui" line="6665"/>
        <source>IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="2611"/>
        <location filename="../dialog_network.ui" line="6785"/>
        <source>PORT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="2671"/>
        <location filename="../dialog_network.ui" line="6845"/>
        <source>2000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="2769"/>
        <location filename="../dialog_network.ui" line="6943"/>
        <source>192</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="2829"/>
        <location filename="../dialog_network.ui" line="2965"/>
        <location filename="../dialog_network.ui" line="7003"/>
        <location filename="../dialog_network.ui" line="7139"/>
        <source>100</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="3025"/>
        <location filename="../dialog_network.ui" line="7199"/>
        <source>168</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="3126"/>
        <location filename="../dialog_network.ui" line="3189"/>
        <location filename="../dialog_network.ui" line="3252"/>
        <location filename="../dialog_network.ui" line="7300"/>
        <location filename="../dialog_network.ui" line="7363"/>
        <location filename="../dialog_network.ui" line="7426"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="3483"/>
        <location filename="../dialog_network.ui" line="4104"/>
        <location filename="../dialog_network.ui" line="7657"/>
        <source>Connetion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="3578"/>
        <location filename="../dialog_network.ui" line="3913"/>
        <location filename="../dialog_network.ui" line="5688"/>
        <location filename="../dialog_network.ui" line="7752"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="3638"/>
        <location filename="../dialog_network.ui" line="4008"/>
        <location filename="../dialog_network.ui" line="7812"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4224"/>
        <location filename="../dialog_network.ui" line="4344"/>
        <location filename="../dialog_network.ui" line="5154"/>
        <location filename="../dialog_network.ui" line="10075"/>
        <location filename="../dialog_network.ui" line="10135"/>
        <location filename="../dialog_network.ui" line="10195"/>
        <location filename="../dialog_network.ui" line="10255"/>
        <location filename="../dialog_network.ui" line="10315"/>
        <location filename="../dialog_network.ui" line="10375"/>
        <location filename="../dialog_network.ui" line="10404"/>
        <location filename="../dialog_network.ui" line="10526"/>
        <location filename="../dialog_network.ui" line="10555"/>
        <location filename="../dialog_network.ui" line="10584"/>
        <location filename="../dialog_network.ui" line="10613"/>
        <location filename="../dialog_network.ui" line="10642"/>
        <location filename="../dialog_network.ui" line="21289"/>
        <location filename="../dialog_network.ui" line="21318"/>
        <location filename="../dialog_network.ui" line="21602"/>
        <location filename="../dialog_network.ui" line="21660"/>
        <location filename="../dialog_network.ui" line="21758"/>
        <location filename="../dialog_network.ui" line="21816"/>
        <location filename="../dialog_network.ui" line="21914"/>
        <location filename="../dialog_network.ui" line="21972"/>
        <location filename="../dialog_network.ui" line="22070"/>
        <location filename="../dialog_network.ui" line="22128"/>
        <location filename="../dialog_network.ui" line="22226"/>
        <location filename="../dialog_network.ui" line="22284"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4454"/>
        <source>9600 8N1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4547"/>
        <source>Spec.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4650"/>
        <source>Counter Display Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4676"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4778"/>
        <source>Current Prod.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4842"/>
        <source>Target Prod.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4906"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4970"/>
        <source>Hold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="4996"/>
        <source>Reserved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="5271"/>
        <source>Erorr Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="5297"/>
        <source>999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="5361"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="5882"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="6076"/>
        <location filename="../dialog_network.ui" line="15237"/>
        <source>Temp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="6270"/>
        <location filename="../dialog_network.ui" line="18256"/>
        <source>E-Sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="9386"/>
        <location filename="../dialog_network.ui" line="20935"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="9415"/>
        <location filename="../dialog_network.ui" line="9613"/>
        <location filename="../dialog_network.ui" line="9680"/>
        <location filename="../dialog_network.ui" line="9747"/>
        <location filename="../dialog_network.ui" line="9814"/>
        <location filename="../dialog_network.ui" line="9881"/>
        <location filename="../dialog_network.ui" line="20964"/>
        <location filename="../dialog_network.ui" line="21631"/>
        <location filename="../dialog_network.ui" line="21787"/>
        <location filename="../dialog_network.ui" line="21943"/>
        <location filename="../dialog_network.ui" line="22099"/>
        <location filename="../dialog_network.ui" line="22255"/>
        <source>data name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="9546"/>
        <location filename="../dialog_network.ui" line="21095"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="10497"/>
        <location filename="../dialog_network.ui" line="21411"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="10735"/>
        <location filename="../dialog_network.ui" line="21504"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="10872"/>
        <location filename="../dialog_network.ui" line="22421"/>
        <source>SEND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="11169"/>
        <location filename="../dialog_network.ui" line="14115"/>
        <location filename="../dialog_network.ui" line="14804"/>
        <location filename="../dialog_network.ui" line="17928"/>
        <location filename="../dialog_network.ui" line="22823"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="11274"/>
        <location filename="../dialog_network.ui" line="22600"/>
        <source>RESET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="11482"/>
        <location filename="../dialog_network.ui" line="11897"/>
        <location filename="../dialog_network.ui" line="12334"/>
        <location filename="../dialog_network.ui" line="12530"/>
        <location filename="../dialog_network.ui" line="12683"/>
        <location filename="../dialog_network.ui" line="12841"/>
        <location filename="../dialog_network.ui" line="13037"/>
        <location filename="../dialog_network.ui" line="13190"/>
        <location filename="../dialog_network.ui" line="13348"/>
        <location filename="../dialog_network.ui" line="13544"/>
        <location filename="../dialog_network.ui" line="13697"/>
        <location filename="../dialog_network.ui" line="13855"/>
        <location filename="../dialog_network.ui" line="15144"/>
        <location filename="../dialog_network.ui" line="15559"/>
        <location filename="../dialog_network.ui" line="15996"/>
        <location filename="../dialog_network.ui" line="16192"/>
        <location filename="../dialog_network.ui" line="16345"/>
        <location filename="../dialog_network.ui" line="16503"/>
        <location filename="../dialog_network.ui" line="16699"/>
        <location filename="../dialog_network.ui" line="16852"/>
        <location filename="../dialog_network.ui" line="17010"/>
        <location filename="../dialog_network.ui" line="17206"/>
        <location filename="../dialog_network.ui" line="17359"/>
        <location filename="../dialog_network.ui" line="17517"/>
        <location filename="../dialog_network.ui" line="18163"/>
        <location filename="../dialog_network.ui" line="18578"/>
        <location filename="../dialog_network.ui" line="19015"/>
        <location filename="../dialog_network.ui" line="19211"/>
        <location filename="../dialog_network.ui" line="19364"/>
        <location filename="../dialog_network.ui" line="19522"/>
        <location filename="../dialog_network.ui" line="19718"/>
        <location filename="../dialog_network.ui" line="19871"/>
        <location filename="../dialog_network.ui" line="20029"/>
        <location filename="../dialog_network.ui" line="20225"/>
        <location filename="../dialog_network.ui" line="20378"/>
        <location filename="../dialog_network.ui" line="20536"/>
        <source>0.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="11575"/>
        <source>weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="11744"/>
        <source>W1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="11837"/>
        <location filename="../dialog_network.ui" line="15499"/>
        <location filename="../dialog_network.ui" line="18518"/>
        <source>Min.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="11990"/>
        <location filename="../dialog_network.ui" line="15652"/>
        <location filename="../dialog_network.ui" line="18671"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="12083"/>
        <location filename="../dialog_network.ui" line="15745"/>
        <location filename="../dialog_network.ui" line="18764"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="12181"/>
        <location filename="../dialog_network.ui" line="12781"/>
        <location filename="../dialog_network.ui" line="13288"/>
        <location filename="../dialog_network.ui" line="13795"/>
        <location filename="../dialog_network.ui" line="15843"/>
        <location filename="../dialog_network.ui" line="16443"/>
        <location filename="../dialog_network.ui" line="16950"/>
        <location filename="../dialog_network.ui" line="17457"/>
        <location filename="../dialog_network.ui" line="18862"/>
        <location filename="../dialog_network.ui" line="19462"/>
        <location filename="../dialog_network.ui" line="19969"/>
        <location filename="../dialog_network.ui" line="20476"/>
        <location filename="../dialog_network.cpp" line="1450"/>
        <location filename="../dialog_network.cpp" line="1649"/>
        <location filename="../dialog_network.cpp" line="1833"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="12274"/>
        <location filename="../dialog_network.ui" line="15936"/>
        <location filename="../dialog_network.ui" line="18955"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="12623"/>
        <source>W2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="13130"/>
        <source>W3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="13637"/>
        <source>W4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="13995"/>
        <location filename="../dialog_network.ui" line="14684"/>
        <location filename="../dialog_network.ui" line="17808"/>
        <source>21</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="14220"/>
        <location filename="../dialog_network.ui" line="14909"/>
        <location filename="../dialog_network.ui" line="18033"/>
        <source>Measure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="14325"/>
        <location filename="../dialog_network.ui" line="15014"/>
        <location filename="../dialog_network.ui" line="20678"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="14504"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="15406"/>
        <source>T1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="16285"/>
        <source>T2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="16792"/>
        <source>T3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="17299"/>
        <source>T4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="18425"/>
        <source>E1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="19304"/>
        <source>E2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="19811"/>
        <source>E3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="20318"/>
        <source>E4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.ui" line="22933"/>
        <location filename="../dialog_network.ui" line="23042"/>
        <location filename="../dialog_network.ui" line="23151"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.cpp" line="854"/>
        <location filename="../dialog_network.cpp" line="1084"/>
        <source>USE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_network.cpp" line="1452"/>
        <location filename="../dialog_network.cpp" line="1651"/>
        <location filename="../dialog_network.cpp" line="1835"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_numpad</name>
    <message>
        <location filename="../dialog_numpad.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_numpad.cpp" line="46"/>
        <source>MIN Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_numpad.cpp" line="47"/>
        <source>Less than MIN Value!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_numpad.cpp" line="54"/>
        <source>MAX Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_numpad.cpp" line="55"/>
        <source>Greater than MAX Value!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_pictureview</name>
    <message>
        <location filename="../dialog_pictureview.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_position</name>
    <message>
        <location filename="../dialog_position.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="262"/>
        <source>Teaching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="307"/>
        <location filename="../dialog_position.ui" line="2675"/>
        <source>100.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="374"/>
        <location filename="../dialog_position.ui" line="2643"/>
        <source>0.23</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="397"/>
        <location filename="../dialog_position.ui" line="2862"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="423"/>
        <location filename="../dialog_position.ui" line="2830"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="482"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="616"/>
        <source>Up Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="739"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="771"/>
        <source>99999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1086"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1374"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="2611"/>
        <source>Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="2798"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="520"/>
        <location filename="../dialog_position.ui" line="1118"/>
        <location filename="../dialog_position.ui" line="1246"/>
        <location filename="../dialog_position.ui" line="1278"/>
        <source>999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="990"/>
        <source>Fwd Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="894"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1214"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1507"/>
        <source>Wait Pos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1614"/>
        <source>Pos2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1686"/>
        <source>Pos3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1758"/>
        <source>Pos4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1830"/>
        <source>Pos5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="1966"/>
        <source>Pos6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="2038"/>
        <source>Pos7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position.ui" line="2177"/>
        <source>99</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_position_auto</name>
    <message>
        <location filename="../dialog_position_auto.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="127"/>
        <location filename="../dialog_position_auto.ui" line="406"/>
        <location filename="../dialog_position_auto.ui" line="499"/>
        <location filename="../dialog_position_auto.ui" line="592"/>
        <location filename="../dialog_position_auto.ui" line="685"/>
        <location filename="../dialog_position_auto.ui" line="962"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="220"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="287"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="380"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="435"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="528"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="621"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="714"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="872"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="936"/>
        <source>100.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="1093"/>
        <source>Delay </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="1122"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="1151"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="1401"/>
        <source>User Pos 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.ui" line="1634"/>
        <source>99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.cpp" line="385"/>
        <location filename="../dialog_position_auto.cpp" line="415"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.cpp" line="386"/>
        <location filename="../dialog_position_auto.cpp" line="416"/>
        <source>Value Transfer Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.cpp" line="387"/>
        <location filename="../dialog_position_auto.cpp" line="417"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.cpp" line="432"/>
        <source>Speed(%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.cpp" line="450"/>
        <source>Delay(sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.cpp" line="469"/>
        <source>Would you want to reset?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_auto.cpp" line="482"/>
        <source>Would you want to save?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_position_teaching</name>
    <message>
        <location filename="../dialog_position_teaching.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="336"/>
        <source>Position Teaching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="377"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="406"/>
        <source>99999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="435"/>
        <source>125.02</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="464"/>
        <source>Fwd Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="493"/>
        <source>Up Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="522"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="551"/>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="580"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="609"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="670"/>
        <location filename="../dialog_position_teaching.ui" line="763"/>
        <location filename="../dialog_position_teaching.ui" line="824"/>
        <location filename="../dialog_position_teaching.ui" line="885"/>
        <location filename="../dialog_position_teaching.ui" line="914"/>
        <location filename="../dialog_position_teaching.ui" line="943"/>
        <location filename="../dialog_position_teaching.ui" line="972"/>
        <location filename="../dialog_position_teaching.ui" line="1001"/>
        <source>999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="1094"/>
        <source>Product</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="1460"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="1553"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="1647"/>
        <source>Wait Pos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="1749"/>
        <source>1.0 sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="1843"/>
        <source>RESET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="2093"/>
        <source>30.0%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.ui" line="2186"/>
        <source>Call</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="171"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="182"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="183"/>
        <source>Save Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="184"/>
        <source>Please, try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="365"/>
        <location filename="../dialog_position_teaching.cpp" line="395"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="366"/>
        <location filename="../dialog_position_teaching.cpp" line="396"/>
        <source>Value Transfer Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="367"/>
        <location filename="../dialog_position_teaching.cpp" line="397"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="460"/>
        <source>Speed[%]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching.cpp" line="479"/>
        <source>Delay[sec]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_position_teaching2</name>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="283"/>
        <source>Wait Pos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="567"/>
        <source>1.0 sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="660"/>
        <source>30.0%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="721"/>
        <source>Call</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1077"/>
        <source>Position Teaching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1185"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1281"/>
        <source>Fwd Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1404"/>
        <location filename="../dialog_position_teaching2.cpp" line="461"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1433"/>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1608"/>
        <location filename="../dialog_position_teaching2.ui" line="1640"/>
        <location filename="../dialog_position_teaching2.ui" line="1862"/>
        <location filename="../dialog_position_teaching2.ui" line="1990"/>
        <location filename="../dialog_position_teaching2.ui" line="2051"/>
        <location filename="../dialog_position_teaching2.ui" line="2313"/>
        <location filename="../dialog_position_teaching2.ui" line="2427"/>
        <location filename="../dialog_position_teaching2.ui" line="2459"/>
        <source>999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1804"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1833"/>
        <source>99999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="1958"/>
        <source>Up Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="2022"/>
        <source>125.02</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="2147"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="2243"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="2560"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="2653"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.ui" line="2825"/>
        <source>RESET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="171"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="179"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="180"/>
        <source>Do you want to save?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="189"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="190"/>
        <source>Save Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="191"/>
        <source>Please, try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="372"/>
        <location filename="../dialog_position_teaching2.cpp" line="402"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="373"/>
        <location filename="../dialog_position_teaching2.cpp" line="403"/>
        <source>Value Transfer Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="374"/>
        <location filename="../dialog_position_teaching2.cpp" line="404"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="463"/>
        <source>Set this position!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="474"/>
        <source>Speed[%]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_position_teaching2.cpp" line="493"/>
        <source>Delay[sec]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_product_info</name>
    <message>
        <location filename="../dialog_product_info.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="238"/>
        <source>Product Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="276"/>
        <location filename="../dialog_product_info.ui" line="454"/>
        <location filename="../dialog_product_info.ui" line="764"/>
        <location filename="../dialog_product_info.ui" line="793"/>
        <location filename="../dialog_product_info.ui" line="1101"/>
        <source>1000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="431"/>
        <source>Total Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="547"/>
        <source>Cavity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="632"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="725"/>
        <source>Target Qty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="886"/>
        <source>Discharge Qty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="979"/>
        <source>Sample Qty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="1072"/>
        <source>Product Qty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="1203"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_product_info.ui" line="1226"/>
        <source>Count Hold</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_program_manage</name>
    <message>
        <location filename="../dialog_program_manage.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.ui" line="100"/>
        <source>Program Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.ui" line="512"/>
        <source>Database Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.ui" line="2135"/>
        <source>TP Programs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.ui" line="4276"/>
        <source>USB Programs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.ui" line="6254"/>
        <source>1/7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.ui" line="6324"/>
        <source>default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="414"/>
        <location filename="../dialog_program_manage.cpp" line="587"/>
        <location filename="../dialog_program_manage.cpp" line="968"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="415"/>
        <location filename="../dialog_program_manage.cpp" line="588"/>
        <location filename="../dialog_program_manage.cpp" line="969"/>
        <source>Don&apos;t exist this file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="423"/>
        <location filename="../dialog_program_manage.cpp" line="596"/>
        <location filename="../dialog_program_manage.cpp" line="977"/>
        <source>Would you want to Delete this file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="428"/>
        <location filename="../dialog_program_manage.cpp" line="601"/>
        <location filename="../dialog_program_manage.cpp" line="982"/>
        <source>Really, Would you want to Delete this file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="437"/>
        <location filename="../dialog_program_manage.cpp" line="610"/>
        <location filename="../dialog_program_manage.cpp" line="991"/>
        <source>File Delete Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="438"/>
        <location filename="../dialog_program_manage.cpp" line="611"/>
        <location filename="../dialog_program_manage.cpp" line="992"/>
        <source>Fail to delete this file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="444"/>
        <location filename="../dialog_program_manage.cpp" line="617"/>
        <location filename="../dialog_program_manage.cpp" line="998"/>
        <source>File Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="445"/>
        <location filename="../dialog_program_manage.cpp" line="618"/>
        <location filename="../dialog_program_manage.cpp" line="999"/>
        <source>Success to delete this file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="653"/>
        <source>Export File! (TP &gt;&gt; DB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="672"/>
        <location filename="../dialog_program_manage.cpp" line="685"/>
        <location filename="../dialog_program_manage.cpp" line="1053"/>
        <location filename="../dialog_program_manage.cpp" line="1066"/>
        <source>Export Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="673"/>
        <location filename="../dialog_program_manage.cpp" line="686"/>
        <location filename="../dialog_program_manage.cpp" line="1054"/>
        <location filename="../dialog_program_manage.cpp" line="1067"/>
        <source>Fail to Export!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="692"/>
        <location filename="../dialog_program_manage.cpp" line="1073"/>
        <source>Export Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="693"/>
        <location filename="../dialog_program_manage.cpp" line="1074"/>
        <source>Success to Export!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="731"/>
        <source>Import File! (DB &gt;&gt; TP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="750"/>
        <location filename="../dialog_program_manage.cpp" line="763"/>
        <location filename="../dialog_program_manage.cpp" line="776"/>
        <location filename="../dialog_program_manage.cpp" line="1128"/>
        <location filename="../dialog_program_manage.cpp" line="1141"/>
        <location filename="../dialog_program_manage.cpp" line="1154"/>
        <source>Import Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="751"/>
        <location filename="../dialog_program_manage.cpp" line="764"/>
        <location filename="../dialog_program_manage.cpp" line="777"/>
        <location filename="../dialog_program_manage.cpp" line="1129"/>
        <location filename="../dialog_program_manage.cpp" line="1142"/>
        <location filename="../dialog_program_manage.cpp" line="1155"/>
        <source>Fail to Import!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="783"/>
        <location filename="../dialog_program_manage.cpp" line="1161"/>
        <source>Import Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="784"/>
        <location filename="../dialog_program_manage.cpp" line="1162"/>
        <source>Success to Import!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="1034"/>
        <source>Export File! (TP &gt;&gt; USB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_program_manage.cpp" line="1109"/>
        <source>Import File! (USB &gt;&gt; TP)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_program_manage_edit</name>
    <message>
        <location filename="../dialog_program_manage_edit.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_recipe</name>
    <message>
        <location filename="../dialog_recipe.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="128"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="189"/>
        <source>Picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="250"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="330"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="391"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="430"/>
        <source>MOLD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="562"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="685"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="808"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="901"/>
        <location filename="../dialog_recipe.ui" line="1180"/>
        <location filename="../dialog_recipe.ui" line="1270"/>
        <location filename="../dialog_recipe.ui" line="1360"/>
        <location filename="../dialog_recipe.ui" line="1450"/>
        <location filename="../dialog_recipe.ui" line="1540"/>
        <source>2017.01.03</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="994"/>
        <source>DEV.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1090"/>
        <location filename="../dialog_recipe.ui" line="1601"/>
        <source>9999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1119"/>
        <location filename="../dialog_recipe.ui" line="1209"/>
        <location filename="../dialog_recipe.ui" line="1328"/>
        <location filename="../dialog_recipe.ui" line="1418"/>
        <location filename="../dialog_recipe.ui" line="1508"/>
        <source>DEVELOP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1151"/>
        <source>99999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1241"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1299"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1389"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1479"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1681"/>
        <source>/9999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1704"/>
        <source>Total :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="1978"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="2039"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="2100"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="2199"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.ui" line="2358"/>
        <source>Make Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="396"/>
        <location filename="../dialog_recipe.cpp" line="573"/>
        <source>INPUT ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="397"/>
        <location filename="../dialog_recipe.cpp" line="574"/>
        <source>Invalid Name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="410"/>
        <source>Would you want to new mold?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="419"/>
        <source>New Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="420"/>
        <source>New Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="432"/>
        <source>New Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="470"/>
        <location filename="../dialog_recipe.cpp" line="489"/>
        <source>Delete Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="471"/>
        <source>Can not delete!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="481"/>
        <source>Would you want to delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="490"/>
        <source>Delete Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="500"/>
        <source>Delete Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="518"/>
        <source>Would you want to copy?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="541"/>
        <source>Copy Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="542"/>
        <source>Copy Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="553"/>
        <source>Copy Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="586"/>
        <source>Would you want to rename?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="593"/>
        <source>Rename Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="594"/>
        <source>Rename Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="604"/>
        <source>Rename Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="624"/>
        <source>Would you want to Fored Load?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="637"/>
        <source>Force Load Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="638"/>
        <source>Force Load Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="659"/>
        <source>Force Load Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="675"/>
        <source>Would you want to Load?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="692"/>
        <source>Load Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="693"/>
        <source>Load Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="716"/>
        <source>Load Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="754"/>
        <source>No SD Memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="755"/>
        <source>Not found SD Memory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="755"/>
        <source>Please insert SD Memory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="791"/>
        <source>Would you want to Refresh &amp; Sync?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="804"/>
        <source>Not found Now Data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="804"/>
        <source>Please insert SD Memory. Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="531"/>
        <location filename="../dialog_recipe.cpp" line="814"/>
        <source>Sync Data Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="345"/>
        <source>SD Memory inserted!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="347"/>
        <source>SD Memory removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="531"/>
        <location filename="../dialog_recipe.cpp" line="814"/>
        <source>Please try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_recipe.cpp" line="822"/>
        <source>Refresh List &amp; Sync data Success!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_robot_notuse</name>
    <message>
        <location filename="../dialog_robot_notuse.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robot_notuse.ui" line="74"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robot_notuse.ui" line="386"/>
        <source>Robot Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robot_notuse.cpp" line="82"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robot_notuse.cpp" line="83"/>
        <source>Do you want to enable this robot?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_robotconfig</name>
    <message>
        <location filename="../dialog_robotconfig.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.ui" line="100"/>
        <source>Robot Motions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.ui" line="470"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.ui" line="560"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.ui" line="688"/>
        <source>Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.ui" line="813"/>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.ui" line="839"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.ui" line="932"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="148"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="149"/>
        <source>P/R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="150"/>
        <source>Gear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="151"/>
        <source>RPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="152"/>
        <location filename="../dialog_robotconfig.cpp" line="283"/>
        <source>Acc.Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="153"/>
        <source>Jog Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="157"/>
        <location filename="../dialog_robotconfig.cpp" line="220"/>
        <source>J%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="215"/>
        <source>Minus Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="216"/>
        <source>Plus Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="274"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="275"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="276"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="279"/>
        <source>Linear Max.Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="280"/>
        <source>Rotation Max.Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="281"/>
        <source>Linear Max.JogSpeed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="282"/>
        <source>Rotation Max.JogSpeed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="284"/>
        <source>Min.Accuracy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="287"/>
        <location filename="../dialog_robotconfig.cpp" line="289"/>
        <source>mm/sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="288"/>
        <location filename="../dialog_robotconfig.cpp" line="290"/>
        <source>deg/sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="291"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotconfig.cpp" line="292"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_robotdimension</name>
    <message>
        <location filename="../dialog_robotdimension.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="250"/>
        <source>Robot Dimension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="409"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="502"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="605"/>
        <source>(1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="663"/>
        <location filename="../dialog_robotdimension.ui" line="799"/>
        <location filename="../dialog_robotdimension.ui" line="909"/>
        <location filename="../dialog_robotdimension.ui" line="1019"/>
        <source>600</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="689"/>
        <location filename="../dialog_robotdimension.ui" line="776"/>
        <location filename="../dialog_robotdimension.ui" line="886"/>
        <location filename="../dialog_robotdimension.ui" line="996"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="715"/>
        <source>(2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="825"/>
        <source>(3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="935"/>
        <source>(4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_robotdimension.ui" line="1043"/>
        <source>HYR600</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_rotation</name>
    <message>
        <location filename="../dialog_rotation.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.ui" line="223"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.ui" line="316"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.ui" line="419"/>
        <location filename="../dialog_rotation.ui" line="894"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.ui" line="448"/>
        <location filename="../dialog_rotation.ui" line="477"/>
        <location filename="../dialog_rotation.ui" line="567"/>
        <location filename="../dialog_rotation.ui" line="596"/>
        <location filename="../dialog_rotation.ui" line="657"/>
        <location filename="../dialog_rotation.ui" line="686"/>
        <location filename="../dialog_rotation.ui" line="923"/>
        <location filename="../dialog_rotation.ui" line="952"/>
        <location filename="../dialog_rotation.ui" line="1042"/>
        <location filename="../dialog_rotation.ui" line="1071"/>
        <location filename="../dialog_rotation.ui" line="1132"/>
        <location filename="../dialog_rotation.ui" line="1161"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.ui" line="506"/>
        <location filename="../dialog_rotation.ui" line="981"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.ui" line="747"/>
        <location filename="../dialog_rotation.ui" line="1222"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.ui" line="776"/>
        <location filename="../dialog_rotation.ui" line="1251"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="109"/>
        <source>Rotation Easy Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="110"/>
        <source>Swivel Easy Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="112"/>
        <location filename="../dialog_rotation.cpp" line="118"/>
        <source>Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="113"/>
        <source>Rotation1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="114"/>
        <source>Rotation2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="115"/>
        <source>Rotation3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="116"/>
        <source>Rotation4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="119"/>
        <source>Swivel1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="120"/>
        <source>Swivel2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="121"/>
        <source>Swivel3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_rotation.cpp" line="122"/>
        <source>Swivel4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_safetyzone</name>
    <message>
        <location filename="../dialog_safetyzone.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="132"/>
        <source>Safety Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="356"/>
        <source>Point 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="449"/>
        <location filename="../dialog_safetyzone.ui" line="1371"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="569"/>
        <source>Point 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="592"/>
        <location filename="../dialog_safetyzone.ui" line="898"/>
        <location filename="../dialog_safetyzone.ui" line="956"/>
        <location filename="../dialog_safetyzone.ui" line="1072"/>
        <location filename="../dialog_safetyzone.ui" line="1130"/>
        <location filename="../dialog_safetyzone.ui" line="1223"/>
        <location filename="../dialog_safetyzone.ui" line="1640"/>
        <location filename="../dialog_safetyzone.ui" line="1669"/>
        <location filename="../dialog_safetyzone.ui" line="1698"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="805"/>
        <location filename="../dialog_safetyzone.ui" line="1491"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="1049"/>
        <location filename="../dialog_safetyzone.ui" line="1611"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="1895"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="2014"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="2117"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="2188"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="2285"/>
        <source>1/3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="2449"/>
        <location filename="../dialog_safetyzone.ui" line="2833"/>
        <location filename="../dialog_safetyzone.ui" line="3249"/>
        <location filename="../dialog_safetyzone.ui" line="3822"/>
        <source>P1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="2572"/>
        <location filename="../dialog_safetyzone.ui" line="2956"/>
        <location filename="../dialog_safetyzone.ui" line="3372"/>
        <source>P2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="2668"/>
        <source>IMM Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="3052"/>
        <source>Takeout Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="3468"/>
        <source>Mold Close Depth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="3564"/>
        <source>Mold Open Depth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="3626"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.ui" line="3918"/>
        <source>Height Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.cpp" line="124"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.cpp" line="125"/>
        <source>Safety Zone Setting is wrong!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.cpp" line="126"/>
        <source>Plase Check Setting Data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.cpp" line="497"/>
        <location filename="../dialog_safetyzone.cpp" line="518"/>
        <source>CONFIRM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.cpp" line="500"/>
        <location filename="../dialog_safetyzone.cpp" line="521"/>
        <source>Would you want to apply this value?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_safetyzone.cpp" line="563"/>
        <location filename="../dialog_safetyzone.cpp" line="569"/>
        <location filename="../dialog_safetyzone.cpp" line="572"/>
        <location filename="../dialog_safetyzone.cpp" line="578"/>
        <location filename="../dialog_safetyzone.cpp" line="581"/>
        <location filename="../dialog_safetyzone.cpp" line="585"/>
        <location filename="../dialog_safetyzone.cpp" line="588"/>
        <location filename="../dialog_safetyzone.cpp" line="594"/>
        <location filename="../dialog_safetyzone.cpp" line="597"/>
        <location filename="../dialog_safetyzone.cpp" line="601"/>
        <location filename="../dialog_safetyzone.cpp" line="604"/>
        <location filename="../dialog_safetyzone.cpp" line="607"/>
        <source>N/A</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sampling</name>
    <message>
        <location filename="../dialog_sampling.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sampling.ui" line="235"/>
        <source>Sampling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sampling.ui" line="302"/>
        <location filename="../dialog_sampling.cpp" line="73"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sampling.ui" line="494"/>
        <source>Sample Qty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sampling.ui" line="517"/>
        <location filename="../dialog_sampling.ui" line="732"/>
        <location filename="../dialog_sampling.ui" line="947"/>
        <source>1000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sampling.ui" line="709"/>
        <source>Sample per Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sampling.ui" line="924"/>
        <source>Cycle Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sampling.cpp" line="74"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sampling.cpp" line="137"/>
        <source>Do you want to clear?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_addstep</name>
    <message>
        <location filename="../dialog_sasang_addstep.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.ui" line="224"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.ui" line="344"/>
        <source>Group Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.ui" line="1417"/>
        <location filename="../dialog_sasang_addstep.ui" line="3253"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.ui" line="1636"/>
        <source>Making Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.ui" line="3009"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.ui" line="3147"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.ui" line="3373"/>
        <source>Patterns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.cpp" line="89"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.cpp" line="90"/>
        <source>Read Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_addstep.cpp" line="91"/>
        <source>Please, try again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_group</name>
    <message>
        <location filename="../dialog_sasang_group.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="218"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="350"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="470"/>
        <source>Group Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="1543"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="1642"/>
        <source>2017.09.27 88:88:88</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="1762"/>
        <source>Making Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="3187"/>
        <source>INSIDE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="3279"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="3393"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="3507"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.ui" line="3696"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="110"/>
        <location filename="../dialog_sasang_group.cpp" line="218"/>
        <location filename="../dialog_sasang_group.cpp" line="286"/>
        <location filename="../dialog_sasang_group.cpp" line="297"/>
        <location filename="../dialog_sasang_group.cpp" line="309"/>
        <location filename="../dialog_sasang_group.cpp" line="361"/>
        <location filename="../dialog_sasang_group.cpp" line="372"/>
        <location filename="../dialog_sasang_group.cpp" line="422"/>
        <location filename="../dialog_sasang_group.cpp" line="433"/>
        <location filename="../dialog_sasang_group.cpp" line="479"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="111"/>
        <source>Read Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="112"/>
        <location filename="../dialog_sasang_group.cpp" line="220"/>
        <location filename="../dialog_sasang_group.cpp" line="299"/>
        <location filename="../dialog_sasang_group.cpp" line="311"/>
        <location filename="../dialog_sasang_group.cpp" line="374"/>
        <location filename="../dialog_sasang_group.cpp" line="435"/>
        <location filename="../dialog_sasang_group.cpp" line="481"/>
        <source>Please, try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="219"/>
        <source>Fail to Inside!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="267"/>
        <source>Would you want to new group?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="287"/>
        <location filename="../dialog_sasang_group.cpp" line="362"/>
        <location filename="../dialog_sasang_group.cpp" line="423"/>
        <source>Wrong Name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="288"/>
        <location filename="../dialog_sasang_group.cpp" line="363"/>
        <location filename="../dialog_sasang_group.cpp" line="424"/>
        <source>Please, Check name &amp; try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="298"/>
        <location filename="../dialog_sasang_group.cpp" line="310"/>
        <source>Making New Group Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="326"/>
        <source>Success to New!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="342"/>
        <source>Would you want to copy?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="373"/>
        <source>Fail to Copy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="389"/>
        <source>Success to Copy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="402"/>
        <source>Would you want to rename?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="434"/>
        <source>Fail to Rename!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="449"/>
        <source>Success to Rename!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="461"/>
        <source>Would you want to Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="467"/>
        <source>Really, Would you want to Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="480"/>
        <source>Fail to Delete!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_group.cpp" line="500"/>
        <source>Success to Delete!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_motion</name>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="81"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="256"/>
        <location filename="../dialog_sasang_motion.ui" line="603"/>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="483"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="2327"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="2635"/>
        <source>Teach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="2706"/>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="3115"/>
        <location filename="../dialog_sasang_motion.ui" line="3448"/>
        <location filename="../dialog_sasang_motion.ui" line="3541"/>
        <location filename="../dialog_sasang_motion.ui" line="3634"/>
        <location filename="../dialog_sasang_motion.ui" line="3727"/>
        <location filename="../dialog_sasang_motion.ui" line="4021"/>
        <location filename="../dialog_sasang_motion.ui" line="4085"/>
        <location filename="../dialog_sasang_motion.ui" line="4111"/>
        <location filename="../dialog_sasang_motion.ui" line="4137"/>
        <location filename="../dialog_sasang_motion.ui" line="4163"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="3235"/>
        <location filename="../dialog_sasang_motion.cpp" line="586"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="3302"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="3422"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="3477"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="3570"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="3663"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="3756"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="4283"/>
        <location filename="../dialog_sasang_motion.cpp" line="582"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="4388"/>
        <location filename="../dialog_sasang_motion.cpp" line="490"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="4452"/>
        <source>100.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="4572"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="4648"/>
        <location filename="../dialog_sasang_motion.cpp" line="497"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="4712"/>
        <source>100.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="4832"/>
        <source>Accuracy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="5076"/>
        <location filename="../dialog_sasang_motion.cpp" line="511"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="5140"/>
        <source>10.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="5260"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="5695"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6004"/>
        <source>Vac. 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6065"/>
        <source>Vac. 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6158"/>
        <source>Vac. 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6251"/>
        <source>Vac. 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6376"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6501"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6530"/>
        <source>Nipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6623"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6748"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6841"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="6934"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="7027"/>
        <source>User 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="7120"/>
        <source>User 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="7213"/>
        <source>User 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="7306"/>
        <source>User 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="8785"/>
        <source>Pattern_name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="8869"/>
        <source>Group_name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.ui" line="8896"/>
        <source>R Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="229"/>
        <location filename="../dialog_sasang_motion.cpp" line="682"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="230"/>
        <source>Would you want to exit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="230"/>
        <location filename="../dialog_sasang_motion.cpp" line="683"/>
        <source>Changed data will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="581"/>
        <source>Middle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="683"/>
        <source>Would you want to view another data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="745"/>
        <location filename="../dialog_sasang_motion.cpp" line="790"/>
        <location filename="../dialog_sasang_motion.cpp" line="814"/>
        <source>CONFIRM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="746"/>
        <source>Would you want to DELETE?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="768"/>
        <location filename="../dialog_sasang_motion.cpp" line="800"/>
        <location filename="../dialog_sasang_motion.cpp" line="861"/>
        <location filename="../dialog_sasang_motion.cpp" line="891"/>
        <location filename="../dialog_sasang_motion.cpp" line="941"/>
        <location filename="../dialog_sasang_motion.cpp" line="971"/>
        <location filename="../dialog_sasang_motion.cpp" line="1076"/>
        <location filename="../dialog_sasang_motion.cpp" line="1088"/>
        <location filename="../dialog_sasang_motion.cpp" line="1117"/>
        <location filename="../dialog_sasang_motion.cpp" line="1141"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="769"/>
        <source>DELETE Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="770"/>
        <location filename="../dialog_sasang_motion.cpp" line="802"/>
        <location filename="../dialog_sasang_motion.cpp" line="863"/>
        <location filename="../dialog_sasang_motion.cpp" line="893"/>
        <location filename="../dialog_sasang_motion.cpp" line="943"/>
        <location filename="../dialog_sasang_motion.cpp" line="973"/>
        <location filename="../dialog_sasang_motion.cpp" line="1078"/>
        <location filename="../dialog_sasang_motion.cpp" line="1090"/>
        <location filename="../dialog_sasang_motion.cpp" line="1119"/>
        <location filename="../dialog_sasang_motion.cpp" line="1143"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="791"/>
        <source>Would you want to SAVE?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="801"/>
        <source>Save Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="815"/>
        <source>Would you want to RESET?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="862"/>
        <location filename="../dialog_sasang_motion.cpp" line="892"/>
        <location filename="../dialog_sasang_motion.cpp" line="942"/>
        <location filename="../dialog_sasang_motion.cpp" line="972"/>
        <source>Value Transfer Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="1077"/>
        <location filename="../dialog_sasang_motion.cpp" line="1089"/>
        <source>Position Shift Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="1101"/>
        <location filename="../dialog_sasang_motion.cpp" line="1126"/>
        <location filename="../dialog_sasang_motion.cpp" line="1150"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="1104"/>
        <source>Position Shift Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="1118"/>
        <source>Change All Speed Value Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="1128"/>
        <source>Change All Speed Value Success!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="1142"/>
        <source>Change Accuracy Value Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion.cpp" line="1152"/>
        <source>Change Accuracy Value Success!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_motion_teach</name>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="283"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="403"/>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="2127"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="2320"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="4956"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="2346"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2540"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2579"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2608"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2637"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2692"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2721"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2869"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2898"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="2927"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="3355"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="3384"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="3799"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="4982"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="5176"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="2514"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="5150"/>
        <source>Accuracy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="2663"/>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="2956"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="2985"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3014"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3134"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3163"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3297"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="3515"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="526"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3326"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3693"/>
        <source>Vac.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3722"/>
        <location filename="../dialog_sasang_motion_teach.ui" line="4698"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3828"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3867"/>
        <source>Pulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="3896"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="4334"/>
        <source>P2P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="4425"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="4516"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="4638"/>
        <source>Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.ui" line="4789"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="504"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="506"/>
        <source>PULSE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="508"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="524"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="560"/>
        <source>Middle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="554"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="661"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="745"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="863"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="904"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="662"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="746"/>
        <source>Save Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="663"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="747"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="865"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="906"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="734"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="894"/>
        <source>CONFIRM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="735"/>
        <source>Would you want to SAVE?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="790"/>
        <location filename="../dialog_sasang_motion_teach.cpp" line="807"/>
        <source>Circle Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="791"/>
        <source>Seting Circle Middle Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="792"/>
        <source>Next, Setting Circle End Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="808"/>
        <source>Finish Setting Circle!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="809"/>
        <source>Seting Circle End Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="864"/>
        <source>Add Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="874"/>
        <source>Add this motion!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="895"/>
        <source>Would you want to DELETE?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_teach.cpp" line="905"/>
        <source>DELETE Error!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_motion_test</name>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="283"/>
        <source>Motion List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="309"/>
        <location filename="../dialog_sasang_motion_test.ui" line="1251"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="335"/>
        <location filename="../dialog_sasang_motion_test.ui" line="1225"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="1106"/>
        <source>99999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="2023"/>
        <source>Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="2137"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="2251"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="2366"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.ui" line="2480"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.cpp" line="186"/>
        <location filename="../dialog_sasang_motion_test.cpp" line="196"/>
        <location filename="../dialog_sasang_motion_test.cpp" line="317"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.cpp" line="187"/>
        <location filename="../dialog_sasang_motion_test.cpp" line="318"/>
        <source>Test Run Ready Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.cpp" line="188"/>
        <location filename="../dialog_sasang_motion_test.cpp" line="319"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.cpp" line="197"/>
        <source>Change Motion List Size!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_motion_test.cpp" line="198"/>
        <source>Please, Moving Safety Zone and Click Ready!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_output</name>
    <message>
        <location filename="../dialog_sasang_output.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="258"/>
        <source>Vac. 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="319"/>
        <source>Vac. 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="412"/>
        <source>Vac. 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="505"/>
        <source>Vac. 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="630"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="755"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="784"/>
        <source>Nipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="877"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="1002"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="1095"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="1188"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="1281"/>
        <source>User 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="1374"/>
        <source>User 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="1467"/>
        <source>User 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="1560"/>
        <source>User 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="3438"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="3655"/>
        <location filename="../dialog_sasang_output.cpp" line="241"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="3719"/>
        <source>10.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="3839"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="3913"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="3983"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_output.ui" line="4162"/>
        <source>R Grip</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_pattern</name>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="336"/>
        <source>Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="477"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="597"/>
        <source>Pattern Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="1670"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="1889"/>
        <source>Motions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="3314"/>
        <source>INSIDE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="3406"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="3520"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="3634"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="3770"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="3884"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="3976"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="4079"/>
        <source>TEST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="4257"/>
        <location filename="../dialog_sasang_pattern.cpp" line="155"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="4471"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.ui" line="4400"/>
        <source>Group_name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="204"/>
        <location filename="../dialog_sasang_pattern.cpp" line="219"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="205"/>
        <location filename="../dialog_sasang_pattern.cpp" line="220"/>
        <source>Would you want to cancel?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="205"/>
        <location filename="../dialog_sasang_pattern.cpp" line="220"/>
        <source>Changed data will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="355"/>
        <source>Would you want to new pattern?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="166"/>
        <location filename="../dialog_sasang_pattern.cpp" line="178"/>
        <location filename="../dialog_sasang_pattern.cpp" line="374"/>
        <location filename="../dialog_sasang_pattern.cpp" line="385"/>
        <location filename="../dialog_sasang_pattern.cpp" line="396"/>
        <location filename="../dialog_sasang_pattern.cpp" line="438"/>
        <location filename="../dialog_sasang_pattern.cpp" line="486"/>
        <location filename="../dialog_sasang_pattern.cpp" line="497"/>
        <location filename="../dialog_sasang_pattern.cpp" line="542"/>
        <location filename="../dialog_sasang_pattern.cpp" line="609"/>
        <location filename="../dialog_sasang_pattern.cpp" line="623"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="375"/>
        <location filename="../dialog_sasang_pattern.cpp" line="487"/>
        <source>Wrong Name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="376"/>
        <location filename="../dialog_sasang_pattern.cpp" line="488"/>
        <source>Please, Check name &amp; try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="386"/>
        <location filename="../dialog_sasang_pattern.cpp" line="397"/>
        <source>Making New Pattern Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="168"/>
        <location filename="../dialog_sasang_pattern.cpp" line="180"/>
        <location filename="../dialog_sasang_pattern.cpp" line="387"/>
        <location filename="../dialog_sasang_pattern.cpp" line="398"/>
        <location filename="../dialog_sasang_pattern.cpp" line="440"/>
        <location filename="../dialog_sasang_pattern.cpp" line="499"/>
        <location filename="../dialog_sasang_pattern.cpp" line="544"/>
        <location filename="../dialog_sasang_pattern.cpp" line="611"/>
        <location filename="../dialog_sasang_pattern.cpp" line="625"/>
        <source>Please, try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="156"/>
        <source>Would you want to save?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="167"/>
        <location filename="../dialog_sasang_pattern.cpp" line="179"/>
        <source>Fail to save group data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="187"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="188"/>
        <source>Success to Save!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="413"/>
        <source>Success to New!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="428"/>
        <source>Would you want to copy?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="439"/>
        <source>Fail to Copy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="455"/>
        <source>Success to Copy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="467"/>
        <source>Would you want to rename?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="498"/>
        <source>Fail to Rename!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="513"/>
        <source>Success to Rename!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="524"/>
        <source>Would you want to Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="530"/>
        <source>Really, Would you want to Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="543"/>
        <source>Fail to Delete!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="563"/>
        <source>Success to Delete!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="610"/>
        <source>Fail to Inside!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="624"/>
        <source>Fail to Save pattern data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="660"/>
        <location filename="../dialog_sasang_pattern.cpp" line="670"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="661"/>
        <location filename="../dialog_sasang_pattern.cpp" line="671"/>
        <source>All Change Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="662"/>
        <location filename="../dialog_sasang_pattern.cpp" line="672"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="685"/>
        <location filename="../dialog_sasang_pattern.cpp" line="694"/>
        <location filename="../dialog_sasang_pattern.cpp" line="703"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="687"/>
        <source>Success Position Shift!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="696"/>
        <source>Success Change All Speed Value!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern.cpp" line="705"/>
        <source>Success Change Accuracy Value!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_pattern_test</name>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="283"/>
        <source>Pattern List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="309"/>
        <source>Pattern_test_1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="335"/>
        <source>0001</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="1106"/>
        <source>99999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="1225"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="1251"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="2014"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="2137"/>
        <source>Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="2251"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.ui" line="2365"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.cpp" line="271"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.cpp" line="272"/>
        <source>Test Run Ready Error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_pattern_test.cpp" line="273"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_sasang_shift</name>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="134"/>
        <source>All Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="225"/>
        <location filename="../dialog_sasang_shift.ui" line="558"/>
        <location filename="../dialog_sasang_shift.ui" line="651"/>
        <location filename="../dialog_sasang_shift.ui" line="744"/>
        <location filename="../dialog_sasang_shift.ui" line="837"/>
        <location filename="../dialog_sasang_shift.ui" line="1771"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="345"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="412"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="532"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="587"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="680"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="773"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="866"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="1024"/>
        <source>Position Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="1116"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="1186"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="1429"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="1467"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="1493"/>
        <source>1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="1707"/>
        <source>Accuracy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_sasang_shift.ui" line="1745"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_softsensor</name>
    <message>
        <location filename="../dialog_softsensor.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="186"/>
        <source>Soft Sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="291"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="384"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="522"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="545"/>
        <location filename="../dialog_softsensor.ui" line="568"/>
        <location filename="../dialog_softsensor.ui" line="1132"/>
        <location filename="../dialog_softsensor.ui" line="1193"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="699"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="830"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="923"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="1016"/>
        <source>Takeout Zone Sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.ui" line="1109"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.cpp" line="74"/>
        <source>Save Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_softsensor.cpp" line="75"/>
        <source>Fail to save data!!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_stepedit</name>
    <message>
        <location filename="../dialog_stepedit.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="118"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="339"/>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="3328"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="509"/>
        <source>Total :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="195"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="532"/>
        <source>Steps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="555"/>
        <source>999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="656"/>
        <location filename="../dialog_stepedit.ui" line="966"/>
        <location filename="../dialog_stepedit.ui" line="1118"/>
        <location filename="../dialog_stepedit.ui" line="1270"/>
        <location filename="../dialog_stepedit.ui" line="1422"/>
        <location filename="../dialog_stepedit.ui" line="1574"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="679"/>
        <location filename="../dialog_stepedit.ui" line="834"/>
        <location filename="../dialog_stepedit.ui" line="989"/>
        <location filename="../dialog_stepedit.ui" line="1141"/>
        <location filename="../dialog_stepedit.ui" line="1293"/>
        <location filename="../dialog_stepedit.ui" line="1445"/>
        <location filename="../dialog_stepedit.ui" line="1597"/>
        <source>Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="811"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="1757"/>
        <location filename="../dialog_stepedit.ui" line="1783"/>
        <location filename="../dialog_stepedit.ui" line="1832"/>
        <location filename="../dialog_stepedit.ui" line="1922"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="1806"/>
        <source>STEP EDIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="1855"/>
        <source>1 Cylcle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="1973"/>
        <source>Step Fwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="3277"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="2074"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="2234"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="2335"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="3176"/>
        <source>Step Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="2684"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="2707"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="2730"/>
        <source>Step Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="2753"/>
        <source>Step Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.ui" line="2798"/>
        <source>Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="306"/>
        <source>Step Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="474"/>
        <location filename="../dialog_stepedit.cpp" line="488"/>
        <location filename="../dialog_stepedit.cpp" line="502"/>
        <location filename="../dialog_stepedit.cpp" line="516"/>
        <location filename="../dialog_stepedit.cpp" line="552"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="475"/>
        <source>Fail to Add User Position Step!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="476"/>
        <location filename="../dialog_stepedit.cpp" line="490"/>
        <location filename="../dialog_stepedit.cpp" line="504"/>
        <location filename="../dialog_stepedit.cpp" line="518"/>
        <location filename="../dialog_stepedit.cpp" line="554"/>
        <source>Over Max. Number or Fail to write file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="489"/>
        <source>Fail to Add User Output Step!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="503"/>
        <source>Fail to Add User InputWait!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="517"/>
        <source>Fail to Add User TimeWait!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="553"/>
        <source>Fail to Add User Work!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="582"/>
        <source>Would you want to Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="630"/>
        <source>User Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="638"/>
        <source>S-Work</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="644"/>
        <source>Wait Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="650"/>
        <source>Take-out Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="656"/>
        <source>Up Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="662"/>
        <source>Unload Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="668"/>
        <source>Grip Step for Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="674"/>
        <source>Unload Step for Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="682"/>
        <source>User Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="690"/>
        <source>User Wait Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="698"/>
        <source>User Wait Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="960"/>
        <location filename="../dialog_stepedit.cpp" line="983"/>
        <location filename="../dialog_stepedit.cpp" line="1338"/>
        <location filename="../dialog_stepedit.cpp" line="1515"/>
        <source>Notice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="961"/>
        <source>Do you want to finish test mode?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="984"/>
        <source>Do you want to entry test mode?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="1339"/>
        <source>Do you want to homing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="1516"/>
        <source>Home not completed!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit.cpp" line="1517"/>
        <source>Please make sure home.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_stepedit_add</name>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="159"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="252"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="1705"/>
        <source>Add User Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="454"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="646"/>
        <source>Time Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="707"/>
        <source>Input Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="768"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="902"/>
        <source>S-Work</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="1197"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_stepedit_add.ui" line="1492"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_syncback</name>
    <message>
        <location filename="../dialog_syncback.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="194"/>
        <source>USE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="255"/>
        <location filename="../dialog_syncback.cpp" line="71"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="282"/>
        <source>Ejector Sync Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="477"/>
        <source>Back Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="500"/>
        <location filename="../dialog_syncback.ui" line="872"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="631"/>
        <source>Back Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="654"/>
        <source>0.05</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="721"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="750"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="843"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.ui" line="907"/>
        <source>mm/s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_syncback.cpp" line="72"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_takeout_method</name>
    <message>
        <location filename="../dialog_takeout_method.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="118"/>
        <source>Takeout Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="191"/>
        <source>Vac 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="301"/>
        <source>Vac 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="411"/>
        <source>Vac 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="521"/>
        <source>Vac 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="631"/>
        <source>Chunck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="741"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="885"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="995"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="1105"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_method.ui" line="1215"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_takeout_timing</name>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="118"/>
        <source>Takeout Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="378"/>
        <source>Before Takeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="567"/>
        <location filename="../dialog_takeout_timing.ui" line="1444"/>
        <location filename="../dialog_takeout_timing.ui" line="1630"/>
        <location filename="../dialog_takeout_timing.ui" line="1816"/>
        <location filename="../dialog_takeout_timing.ui" line="1888"/>
        <location filename="../dialog_takeout_timing.ui" line="2018"/>
        <location filename="../dialog_takeout_timing.ui" line="2172"/>
        <location filename="../dialog_takeout_timing.ui" line="2326"/>
        <location filename="../dialog_takeout_timing.ui" line="2480"/>
        <location filename="../dialog_takeout_timing.ui" line="2660"/>
        <location filename="../dialog_takeout_timing.ui" line="2814"/>
        <location filename="../dialog_takeout_timing.ui" line="3075"/>
        <location filename="../dialog_takeout_timing.ui" line="3172"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="756"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="876"/>
        <source>During Takeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="963"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="1083"/>
        <source>After Takeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="1262"/>
        <location filename="../dialog_takeout_timing.cpp" line="111"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="1530"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="1684"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="1943"/>
        <source>Vac 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="2097"/>
        <source>Vac 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="2251"/>
        <source>Vac 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="2405"/>
        <source>Vac 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="2585"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="2739"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="2893"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.ui" line="2925"/>
        <source>Chunck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_takeout_timing.cpp" line="112"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_task_control</name>
    <message>
        <location filename="../dialog_task_control.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="250"/>
        <source>Task Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="465"/>
        <source>Task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="558"/>
        <source>Sub 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="716"/>
        <source>Run/Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="742"/>
        <location filename="../dialog_task_control.ui" line="2276"/>
        <location filename="../dialog_task_control.cpp" line="185"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="900"/>
        <source>Auto Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="1076"/>
        <location filename="../dialog_task_control.ui" line="1902"/>
        <location filename="../dialog_task_control.ui" line="2478"/>
        <location filename="../dialog_task_control.ui" line="3051"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="1228"/>
        <source>Macro Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="1257"/>
        <location filename="../dialog_task_control.ui" line="1543"/>
        <location filename="../dialog_task_control.ui" line="2119"/>
        <location filename="../dialog_task_control.ui" line="2692"/>
        <source>AAAAAAAAAAAAAAA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="1636"/>
        <source>Sub 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="1700"/>
        <location filename="../dialog_task_control.ui" line="2849"/>
        <location filename="../dialog_task_control.cpp" line="183"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="2212"/>
        <source>Sub 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="2785"/>
        <source>Sub 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="3210"/>
        <source>Var. Init</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.ui" line="3316"/>
        <source>Data View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.cpp" line="130"/>
        <source>N/A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.cpp" line="193"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.cpp" line="198"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.cpp" line="312"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_task_control.cpp" line="313"/>
        <source>Not Found!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_temperature</name>
    <message>
        <location filename="../dialog_temperature.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="140"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="607"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="723"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="1644"/>
        <source>0.50 sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="1737"/>
        <source>Delay Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="1763"/>
        <source>Unit : Celsius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="307"/>
        <location filename="../dialog_temperature.ui" line="746"/>
        <location filename="../dialog_temperature.ui" line="769"/>
        <location filename="../dialog_temperature.ui" line="792"/>
        <location filename="../dialog_temperature.cpp" line="146"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="191"/>
        <location filename="../dialog_temperature.ui" line="630"/>
        <location filename="../dialog_temperature.ui" line="929"/>
        <location filename="../dialog_temperature.ui" line="952"/>
        <location filename="../dialog_temperature.ui" line="975"/>
        <location filename="../dialog_temperature.ui" line="1112"/>
        <location filename="../dialog_temperature.ui" line="1135"/>
        <location filename="../dialog_temperature.ui" line="1158"/>
        <source>331.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.ui" line="284"/>
        <location filename="../dialog_temperature.cpp" line="147"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.cpp" line="92"/>
        <source>Value Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.cpp" line="94"/>
        <source>Please input Min &lt; Max.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.cpp" line="108"/>
        <source>Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.cpp" line="183"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_temperature.cpp" line="279"/>
        <source>Min Value &gt; Max Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_time</name>
    <message>
        <location filename="../dialog_time.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_time.ui" line="336"/>
        <location filename="../dialog_time.ui" line="685"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_time.ui" line="438"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_time.ui" line="499"/>
        <location filename="../dialog_time.ui" line="862"/>
        <location filename="../dialog_time.ui" line="981"/>
        <location filename="../dialog_time.ui" line="1100"/>
        <location filename="../dialog_time.ui" line="1219"/>
        <location filename="../dialog_time.ui" line="1338"/>
        <location filename="../dialog_time.ui" line="1428"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_time.ui" line="592"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_time.ui" line="1627"/>
        <source>1/3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_timeset</name>
    <message>
        <location filename="../dialog_timeset.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="186"/>
        <source>Time Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="409"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="502"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="561"/>
        <source>12:55:55</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="584"/>
        <source>2016-08-01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="738"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="770"/>
        <source>2017</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="1332"/>
        <source>Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="1364"/>
        <source>04</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="1719"/>
        <source>Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="1751"/>
        <source>19</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="2106"/>
        <source>Hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="2138"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="2493"/>
        <source>Minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="2525"/>
        <source>03</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="2880"/>
        <source>Second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_timeset.ui" line="2912"/>
        <source>55</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_tp_ipgw</name>
    <message>
        <location filename="../dialog_tp_ipgw.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.ui" line="186"/>
        <source>IP &amp; Gateway Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.ui" line="347"/>
        <source>IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.ui" line="407"/>
        <location filename="../dialog_tp_ipgw.ui" line="565"/>
        <location filename="../dialog_tp_ipgw.ui" line="723"/>
        <location filename="../dialog_tp_ipgw.ui" line="881"/>
        <location filename="../dialog_tp_ipgw.ui" line="1007"/>
        <location filename="../dialog_tp_ipgw.ui" line="1133"/>
        <location filename="../dialog_tp_ipgw.ui" line="1319"/>
        <location filename="../dialog_tp_ipgw.ui" line="1505"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.ui" line="505"/>
        <location filename="../dialog_tp_ipgw.ui" line="663"/>
        <location filename="../dialog_tp_ipgw.ui" line="821"/>
        <location filename="../dialog_tp_ipgw.ui" line="1067"/>
        <location filename="../dialog_tp_ipgw.ui" line="1193"/>
        <location filename="../dialog_tp_ipgw.ui" line="1253"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.ui" line="1439"/>
        <source>Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.ui" line="1573"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.ui" line="1676"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.cpp" line="76"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.cpp" line="77"/>
        <source>Do you want to System Rebooting?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.cpp" line="78"/>
        <source>OK : now reboot, Cancel : later reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.cpp" line="87"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.cpp" line="88"/>
        <source>Fail to ip &amp; gateway setting!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_tp_ipgw.cpp" line="89"/>
        <source>Please try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_unload_method</name>
    <message>
        <location filename="../dialog_unload_method.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="118"/>
        <source>Unload Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="191"/>
        <source>Vac 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="301"/>
        <source>Vac 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="411"/>
        <source>Vac 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="521"/>
        <source>Vac 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="631"/>
        <source>Chunck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="741"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="885"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="995"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="1105"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unload_method.ui" line="1215"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_unloadback</name>
    <message>
        <location filename="../dialog_unloadback.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="235"/>
        <source>Unload Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="302"/>
        <location filename="../dialog_unloadback.cpp" line="71"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="506"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="664"/>
        <source> Back Pitch [mm]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="687"/>
        <location filename="../dialog_unloadback.ui" line="748"/>
        <location filename="../dialog_unloadback.ui" line="929"/>
        <location filename="../dialog_unloadback.ui" line="1230"/>
        <location filename="../dialog_unloadback.ui" line="1411"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="868"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="1049"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="1207"/>
        <source>Speed [%]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.ui" line="1388"/>
        <source>Delay [s]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadback.cpp" line="72"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_unloadseq_set</name>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="118"/>
        <source>Sequential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="267"/>
        <location filename="../dialog_unloadseq_set.ui" line="934"/>
        <source>UpDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="290"/>
        <location filename="../dialog_unloadseq_set.ui" line="841"/>
        <source>FwdBwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="313"/>
        <location filename="../dialog_unloadseq_set.ui" line="594"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="336"/>
        <source>Unload Pos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="451"/>
        <location filename="../dialog_unloadseq_set.ui" line="2153"/>
        <location filename="../dialog_unloadseq_set.ui" line="2601"/>
        <location filename="../dialog_unloadseq_set.ui" line="2758"/>
        <location filename="../dialog_unloadseq_set.ui" line="2979"/>
        <location filename="../dialog_unloadseq_set.ui" line="3107"/>
        <location filename="../dialog_unloadseq_set.ui" line="3357"/>
        <location filename="../dialog_unloadseq_set.ui" line="3578"/>
        <location filename="../dialog_unloadseq_set.ui" line="3835"/>
        <location filename="../dialog_unloadseq_set.ui" line="3992"/>
        <location filename="../dialog_unloadseq_set.ui" line="4213"/>
        <location filename="../dialog_unloadseq_set.ui" line="4341"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="748"/>
        <location filename="../dialog_unloadseq_set.ui" line="957"/>
        <location filename="../dialog_unloadseq_set.ui" line="1018"/>
        <location filename="../dialog_unloadseq_set.ui" line="4692"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="725"/>
        <location filename="../dialog_unloadseq_set.ui" line="1184"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="474"/>
        <source>Heigth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="1082"/>
        <location filename="../dialog_unloadseq_set.ui" line="1108"/>
        <location filename="../dialog_unloadseq_set.ui" line="1134"/>
        <location filename="../dialog_unloadseq_set.ui" line="4718"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="1411"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="1529"/>
        <source>Pos 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="1833"/>
        <source>Pos 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="1958"/>
        <source>Pos 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="2083"/>
        <source>Pos 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="2246"/>
        <source>Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="2354"/>
        <source>Vac1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="2473"/>
        <source>Vac. 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="2662"/>
        <source>Vac. 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="2851"/>
        <source>Vac. 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="3168"/>
        <source>Vac. 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="3229"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="3482"/>
        <source>Gripper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="3707"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="3896"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="4085"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="4402"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_unloadseq_set.ui" line="4631"/>
        <source>Shift Height</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_userlevel</name>
    <message>
        <location filename="../dialog_userlevel.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="130"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="207"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="284"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="361"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="438"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="515"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="592"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="669"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="746"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="823"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="900"/>
        <location filename="../dialog_userlevel.ui" line="2263"/>
        <location filename="../dialog_userlevel.ui" line="2380"/>
        <location filename="../dialog_userlevel.ui" line="2520"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="977"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="2100"/>
        <source>User Log-in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="2123"/>
        <source>Operator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="2146"/>
        <source>Engineer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_userlevel.ui" line="2403"/>
        <source>Pro.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_useskip</name>
    <message>
        <location filename="../dialog_useskip.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="135"/>
        <source>Use / Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="326"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="419"/>
        <location filename="../dialog_useskip.ui" line="3090"/>
        <source>NO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="512"/>
        <location filename="../dialog_useskip.ui" line="3183"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="605"/>
        <location filename="../dialog_useskip.ui" line="821"/>
        <location filename="../dialog_useskip.ui" line="944"/>
        <location filename="../dialog_useskip.ui" line="1253"/>
        <location filename="../dialog_useskip.ui" line="1562"/>
        <location filename="../dialog_useskip.ui" line="1871"/>
        <location filename="../dialog_useskip.ui" line="2180"/>
        <location filename="../dialog_useskip.ui" line="2489"/>
        <location filename="../dialog_useskip.ui" line="2798"/>
        <location filename="../dialog_useskip.ui" line="3276"/>
        <location filename="../dialog_useskip.ui" line="3399"/>
        <location filename="../dialog_useskip.ui" line="3708"/>
        <location filename="../dialog_useskip.ui" line="4017"/>
        <location filename="../dialog_useskip.ui" line="4326"/>
        <location filename="../dialog_useskip.ui" line="4635"/>
        <location filename="../dialog_useskip.ui" line="4944"/>
        <location filename="../dialog_useskip.ui" line="5253"/>
        <location filename="../dialog_useskip.ui" line="5562"/>
        <location filename="../dialog_useskip.cpp" line="115"/>
        <location filename="../dialog_useskip.cpp" line="117"/>
        <location filename="../dialog_useskip.cpp" line="166"/>
        <source>USE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="1130"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="1439"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="1748"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="2057"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="2366"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="2675"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="2984"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="3585"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="3894"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="4203"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="4512"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="4821"/>
        <source>13</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="5130"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="5439"/>
        <source>15</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.ui" line="5748"/>
        <source>16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.cpp" line="112"/>
        <source>CONFIRM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.cpp" line="115"/>
        <location filename="../dialog_useskip.cpp" line="117"/>
        <location filename="../dialog_useskip.cpp" line="171"/>
        <source>SKIP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.cpp" line="127"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.cpp" line="128"/>
        <source>Use/Skip Data Write Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_useskip.cpp" line="129"/>
        <source>Plase try again!!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_varmonitor</name>
    <message>
        <location filename="../dialog_varmonitor.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_varmonitor.ui" line="566"/>
        <location filename="../dialog_varmonitor.ui" line="2891"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_varmonitor.ui" line="1124"/>
        <location filename="../dialog_varmonitor.ui" line="2705"/>
        <source>Var Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_varmonitor.ui" line="3569"/>
        <source>Data Monitor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_varmonitor.ui" line="3592"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_varmonitor.ui" line="3662"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_varmonitor.cpp" line="150"/>
        <source>N/A</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_version</name>
    <message>
        <location filename="../dialog_version.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="336"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="438"/>
        <source>GUI ver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="534"/>
        <source>SEQ ver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="630"/>
        <source>Server ver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="726"/>
        <source>API ver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="822"/>
        <source>Start Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="854"/>
        <location filename="../dialog_version.ui" line="886"/>
        <location filename="../dialog_version.ui" line="918"/>
        <location filename="../dialog_version.ui" line="950"/>
        <location filename="../dialog_version.ui" line="1110"/>
        <location filename="../dialog_version.ui" line="1237"/>
        <location filename="../dialog_version.ui" line="1269"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="982"/>
        <source>2016-09-01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="1078"/>
        <source>Master ver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="1206"/>
        <source>Kernel ver.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="1365"/>
        <source>IP Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="1465"/>
        <source>http://www.hyrobot.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="1494"/>
        <source>+82-505-300-5040</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="1523"/>
        <source>138-17, Bongsin-ro, Hongbuk-eup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="1552"/>
        <source>Hongseong-gun, Chungcheongnam-do, Republic of Korea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_version.ui" line="1611"/>
        <location filename="../dialog_version.ui" line="1624"/>
        <location filename="../dialog_version.ui" line="1637"/>
        <location filename="../dialog_version.ui" line="1650"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_view_dmcvar</name>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="189"/>
        <location filename="../dialog_view_dmcvar.cpp" line="340"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="215"/>
        <source>Zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="251"/>
        <source>  Trans  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="350"/>
        <source>  Joint  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="449"/>
        <source>  Number  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="548"/>
        <source>  String  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="695"/>
        <source>Shared Variable Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.ui" line="1012"/>
        <location filename="../dialog_view_dmcvar.cpp" line="410"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.cpp" line="43"/>
        <location filename="../dialog_view_dmcvar.cpp" line="63"/>
        <location filename="../dialog_view_dmcvar.cpp" line="83"/>
        <location filename="../dialog_view_dmcvar.cpp" line="96"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.cpp" line="341"/>
        <source>Would you want to delete data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.cpp" line="394"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.cpp" line="395"/>
        <source>Would you want to make zero?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_view_dmcvar.cpp" line="411"/>
        <source>Would you want to Save?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_weight</name>
    <message>
        <location filename="../dialog_weight.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="140"/>
        <location filename="../dialog_weight.cpp" line="110"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="191"/>
        <location filename="../dialog_weight.ui" line="711"/>
        <location filename="../dialog_weight.ui" line="1037"/>
        <location filename="../dialog_weight.ui" line="1060"/>
        <location filename="../dialog_weight.ui" line="1083"/>
        <location filename="../dialog_weight.ui" line="1220"/>
        <location filename="../dialog_weight.ui" line="1243"/>
        <location filename="../dialog_weight.ui" line="1266"/>
        <source>0.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="688"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="831"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="1833"/>
        <source>0.50 sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="1979"/>
        <source>Unit : Gram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="2123"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="334"/>
        <location filename="../dialog_weight.ui" line="854"/>
        <location filename="../dialog_weight.ui" line="877"/>
        <location filename="../dialog_weight.ui" line="900"/>
        <location filename="../dialog_weight.cpp" line="148"/>
        <source>No Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="1953"/>
        <source>Delay Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.ui" line="311"/>
        <location filename="../dialog_weight.cpp" line="149"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.cpp" line="94"/>
        <source>Value Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.cpp" line="96"/>
        <source>Please input Min &lt; Max.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.cpp" line="185"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight.cpp" line="282"/>
        <source>Min Value &gt; Max Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_weight_offset</name>
    <message>
        <location filename="../dialog_weight_offset.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight_offset.ui" line="140"/>
        <source>Weight Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight_offset.ui" line="299"/>
        <source>Weight 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight_offset.ui" line="322"/>
        <location filename="../dialog_weight_offset.ui" line="345"/>
        <location filename="../dialog_weight_offset.ui" line="368"/>
        <location filename="../dialog_weight_offset.ui" line="391"/>
        <source>0.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight_offset.ui" line="598"/>
        <source>Weight 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight_offset.ui" line="691"/>
        <source>Weight 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight_offset.ui" line="784"/>
        <source>Weight 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_weight_offset.ui" line="823"/>
        <source>Unit : Gram</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dialog_zeroing</name>
    <message>
        <location filename="../dialog_zeroing.ui" line="78"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="100"/>
        <source>Mechanical Zero Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="403"/>
        <location filename="../dialog_zeroing.cpp" line="142"/>
        <source>Zeroing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="473"/>
        <source>Product</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="735"/>
        <location filename="../dialog_zeroing.cpp" line="254"/>
        <source>J</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="889"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="921"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="953"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1017"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1204"/>
        <source>Trans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1233"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1265"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1540"/>
        <location filename="../dialog_zeroing.ui" line="1721"/>
        <location filename="../dialog_zeroing.ui" line="1849"/>
        <location filename="../dialog_zeroing.ui" line="1878"/>
        <location filename="../dialog_zeroing.ui" line="2088"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1569"/>
        <source>J1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1371"/>
        <source>Joint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1791"/>
        <source>J4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1820"/>
        <source>J2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="1511"/>
        <source>J5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.ui" line="2059"/>
        <source>J3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.cpp" line="143"/>
        <source>Would you want to zeroing all joint?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.cpp" line="249"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.cpp" line="270"/>
        <source>Each Zeroing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.cpp" line="271"/>
        <source>Would you want to zeroing this joint?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.cpp" line="296"/>
        <source>Multi-Turn Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog_zeroing.cpp" line="297"/>
        <source>Would you want to clear multi-turn data?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>form_autorun</name>
    <message>
        <location filename="../form_autorun.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1005"/>
        <source>Product Qty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="187"/>
        <location filename="../form_autorun.cpp" line="709"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="288"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="373"/>
        <source>1 Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="452"/>
        <source>Discharge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="649"/>
        <source>Sample</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="765"/>
        <source>1000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="885"/>
        <source>Target Qty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1034"/>
        <source>111</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1154"/>
        <source>1 Cycle Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1183"/>
        <source>15.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1303"/>
        <source>Takeout Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1332"/>
        <source>5.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1452"/>
        <source>1 Day Qty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1481"/>
        <source>5760</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1581"/>
        <location filename="../form_autorun.ui" line="1682"/>
        <location filename="../form_autorun.ui" line="1728"/>
        <location filename="../form_autorun.ui" line="1806"/>
        <location filename="../form_autorun.ui" line="1881"/>
        <location filename="../form_autorun.ui" line="1956"/>
        <location filename="../form_autorun.ui" line="2031"/>
        <location filename="../form_autorun.ui" line="2083"/>
        <source>Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1604"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1627"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1705"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1783"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1858"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="1933"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="2008"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="2135"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4090"/>
        <source>Homing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4113"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4212"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4254"/>
        <source>I/O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4296"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4395"/>
        <source>DataView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4437"/>
        <source>Motor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4517"/>
        <source>Euromap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4559"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4620"/>
        <source>2 Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4678"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4736"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4794"/>
        <source>13</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4852"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4910"/>
        <source>15</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="4968"/>
        <source>16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="2265"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="2385"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="2478"/>
        <source>Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="2571"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="2637"/>
        <location filename="../form_autorun.ui" line="2889"/>
        <location filename="../form_autorun.ui" line="3048"/>
        <location filename="../form_autorun.ui" line="3114"/>
        <location filename="../form_autorun.ui" line="3579"/>
        <location filename="../form_autorun.ui" line="3645"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="2730"/>
        <location filename="../form_autorun.ui" line="2823"/>
        <location filename="../form_autorun.ui" line="2982"/>
        <location filename="../form_autorun.ui" line="3207"/>
        <location filename="../form_autorun.ui" line="3300"/>
        <location filename="../form_autorun.ui" line="3393"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="3513"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="3765"/>
        <source>Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.ui" line="3859"/>
        <source>Message Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.cpp" line="227"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.cpp" line="228"/>
        <source>Do you want to close?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.cpp" line="311"/>
        <location filename="../form_autorun.cpp" line="360"/>
        <source>N/A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.cpp" line="707"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.cpp" line="1093"/>
        <location filename="../form_autorun.cpp" line="1112"/>
        <location filename="../form_autorun.cpp" line="1122"/>
        <location filename="../form_autorun.cpp" line="1142"/>
        <source>Notice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.cpp" line="1094"/>
        <location filename="../form_autorun.cpp" line="1123"/>
        <location filename="../form_autorun.cpp" line="1143"/>
        <source>Home not completed!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.cpp" line="1095"/>
        <location filename="../form_autorun.cpp" line="1124"/>
        <location filename="../form_autorun.cpp" line="1144"/>
        <source>Please make sure home.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_autorun.cpp" line="1113"/>
        <source>Do you want to homing?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>form_specialsetting</name>
    <message>
        <location filename="../form_specialsetting.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="295"/>
        <source>Special Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="406"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="467"/>
        <source>Motion Spec.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="606"/>
        <source>Zeroing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="855"/>
        <source>Robot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="884"/>
        <source>Motor Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="1023"/>
        <source>Safety Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="1272"/>
        <source>Soft Sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="1379"/>
        <source>Program Manage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="1518"/>
        <source>IP &amp; Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="1657"/>
        <source>Anti Vibration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="1796"/>
        <source>Task Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="2079"/>
        <location filename="../form_specialsetting.cpp" line="188"/>
        <source>Reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="2249"/>
        <source>First View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.ui" line="2394"/>
        <location filename="../form_specialsetting.cpp" line="250"/>
        <location filename="../form_specialsetting.cpp" line="299"/>
        <source>Data Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.cpp" line="189"/>
        <source>Do you want to reboot system?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.cpp" line="251"/>
        <source>Do you want to update data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_specialsetting.cpp" line="301"/>
        <source>Success to update data!!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>form_stepedit_base</name>
    <message>
        <location filename="../form_stepedit_base.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1063"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1149"/>
        <location filename="../form_stepedit_base.ui" line="3958"/>
        <source>100.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1178"/>
        <location filename="../form_stepedit_base.ui" line="4145"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1242"/>
        <source>99.34</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1271"/>
        <location filename="../form_stepedit_base.ui" line="4113"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1374"/>
        <location filename="../form_stepedit_base.ui" line="1477"/>
        <location filename="../form_stepedit_base.ui" line="1775"/>
        <location filename="../form_stepedit_base.ui" line="1878"/>
        <location filename="../form_stepedit_base.ui" line="2368"/>
        <location filename="../form_stepedit_base.ui" line="2666"/>
        <source>Teaching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1570"/>
        <source>Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="4207"/>
        <source>J-Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1631"/>
        <source>Temp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="2186"/>
        <source>Dual Unload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="2760"/>
        <source>Sync Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3894"/>
        <source>Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3926"/>
        <source>0.23</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="4081"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="1971"/>
        <source>Array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="2064"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="2093"/>
        <source>Sequential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3003"/>
        <source>Up Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3126"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3158"/>
        <source>99999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3473"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3761"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="2907"/>
        <location filename="../form_stepedit_base.ui" line="3505"/>
        <location filename="../form_stepedit_base.ui" line="3633"/>
        <location filename="../form_stepedit_base.ui" line="3665"/>
        <source>999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3377"/>
        <source>Fwd Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3281"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="3601"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="2461"/>
        <source>Main Pos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_base.ui" line="2554"/>
        <source>Sub Pos.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>form_stepedit_userinput</name>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="250"/>
        <source>User Input Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="1787"/>
        <source>R Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="1965"/>
        <source>Vac. 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2026"/>
        <source>Vac. 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2119"/>
        <source>Vac. 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2212"/>
        <source>Vac. 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2337"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2462"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2491"/>
        <source>Nipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2584"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2709"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2802"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2895"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="2988"/>
        <source>User 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="3081"/>
        <source>User 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="3174"/>
        <source>User 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="3267"/>
        <source>User 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="409"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="502"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="576"/>
        <source>Position 1 (nickname)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="672"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="775"/>
        <source>Delay Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="798"/>
        <source>0.05</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="865"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="975"/>
        <source>On / Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="1354"/>
        <source>Condition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="1581"/>
        <source>AND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.ui" line="1604"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.cpp" line="104"/>
        <source>Save Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.cpp" line="105"/>
        <source>Save Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userinput.cpp" line="105"/>
        <source>Try again, please!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>form_stepedit_useroutput</name>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="336"/>
        <source>User Ouput</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="511"/>
        <source>Vac. 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="572"/>
        <source>Vac. 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="665"/>
        <source>Vac. 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="758"/>
        <source>Vac. 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="4526"/>
        <source>R Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="883"/>
        <source>Chuck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1008"/>
        <source>Grip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1037"/>
        <source>Nipper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1130"/>
        <source>User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1255"/>
        <source>User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1348"/>
        <source>User 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1441"/>
        <source>User 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1534"/>
        <source>User 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1627"/>
        <source>User 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1720"/>
        <source>User 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="1813"/>
        <source>User 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="3330"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="3423"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="3561"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="3590"/>
        <source>Position 1 (nickname)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="3693"/>
        <source>Delay Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="3716"/>
        <location filename="../form_stepedit_useroutput.ui" line="4428"/>
        <source>0.05</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="3783"/>
        <location filename="../form_stepedit_useroutput.ui" line="4495"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="3893"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.ui" line="4405"/>
        <source>Pulse Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.cpp" line="102"/>
        <source>Save Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.cpp" line="103"/>
        <source>Save Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_useroutput.cpp" line="103"/>
        <source>Try again, please!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>form_stepedit_userposition</name>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="176"/>
        <location filename="../form_stepedit_userposition.ui" line="1040"/>
        <source>Teaching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="770"/>
        <location filename="../form_stepedit_userposition.ui" line="2783"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="802"/>
        <location filename="../form_stepedit_userposition.ui" line="2596"/>
        <source>100.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="873"/>
        <location filename="../form_stepedit_userposition.ui" line="2751"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="905"/>
        <source>99.34</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1133"/>
        <source>Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1162"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1277"/>
        <source>User Position 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1400"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1545"/>
        <location filename="../form_stepedit_userposition.ui" line="2143"/>
        <location filename="../form_stepedit_userposition.ui" line="2271"/>
        <location filename="../form_stepedit_userposition.ui" line="2303"/>
        <source>999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1641"/>
        <source>Up Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1764"/>
        <source>Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1796"/>
        <source>99999.99</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="1919"/>
        <source>Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="2015"/>
        <source>Fwd Bwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="2111"/>
        <source>Swivel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="2239"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="2399"/>
        <source>Traverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="2532"/>
        <source>Delay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="2564"/>
        <source>0.23</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.ui" line="2719"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="229"/>
        <source>Would you want to change type?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="282"/>
        <location filename="../form_stepedit_userposition.cpp" line="294"/>
        <location filename="../form_stepedit_userposition.cpp" line="343"/>
        <location filename="../form_stepedit_userposition.cpp" line="378"/>
        <location filename="../form_stepedit_userposition.cpp" line="393"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="283"/>
        <location filename="../form_stepedit_userposition.cpp" line="379"/>
        <source>Fail to Read Group Data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="284"/>
        <location filename="../form_stepedit_userposition.cpp" line="380"/>
        <source>Please, Check Nickname and Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="295"/>
        <source>Fail to Transfer Pattern Data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="296"/>
        <location filename="../form_stepedit_userposition.cpp" line="345"/>
        <location filename="../form_stepedit_userposition.cpp" line="395"/>
        <source>Please, Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="344"/>
        <source>Fail to Update!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="359"/>
        <source>Would you want to Refresh This Work?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="394"/>
        <source>Fail to Refresh!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="402"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../form_stepedit_userposition.cpp" line="403"/>
        <source>Success to Refresh!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>keyboard</name>
    <message>
        <location filename="../keyboard.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="243"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="320"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="397"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="474"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="551"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="628"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="705"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="782"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="859"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="936"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1013"/>
        <source>q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1090"/>
        <source>w</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1167"/>
        <source>e</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1244"/>
        <source>r</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1321"/>
        <source>t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1398"/>
        <source>y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1475"/>
        <source>u</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1552"/>
        <source>i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1629"/>
        <source>o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1706"/>
        <source>p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1783"/>
        <source>a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1860"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="1937"/>
        <source>d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2014"/>
        <source>f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2091"/>
        <source>g</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2168"/>
        <source>h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2245"/>
        <source>j</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2322"/>
        <source>k</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2399"/>
        <source>l</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2476"/>
        <source>z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2553"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2630"/>
        <source>c</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2707"/>
        <source>v</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2784"/>
        <source>b</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2861"/>
        <source>n</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="2938"/>
        <source>m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3015"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3092"/>
        <source>BS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3169"/>
        <source>Eng</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3249"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3326"/>
        <source>Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3406"/>
        <source>Enter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3483"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3560"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3637"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3714"/>
        <source>`</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3791"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3868"/>
        <source>=</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="3945"/>
        <source>\</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="4022"/>
        <source>[</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="4099"/>
        <source>]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="4176"/>
        <source>;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="4253"/>
        <source>&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="4330"/>
        <source>,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="4407"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="4484"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../keyboard.ui" line="4522"/>
        <source>TITLE</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>numpad</name>
    <message>
        <location filename="../numpad.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="729"/>
        <location filename="../numpad.ui" line="1763"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="752"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="810"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="868"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="926"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="984"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1042"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1100"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1158"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1332"/>
        <source>+-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1425"/>
        <source>&lt;-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1448"/>
        <source>AC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1541"/>
        <source>ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1216"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1309"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1564"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="126"/>
        <source>TITLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="237"/>
        <source>0.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="330"/>
        <source>MIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="423"/>
        <source>10.00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="516"/>
        <source>MAX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="569"/>
        <source>NOW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="662"/>
        <source>NEW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="1984"/>
        <source>0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="2007"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../numpad.ui" line="2030"/>
        <source>0.01</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>page_main2</name>
    <message>
        <location filename="../page_main2.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="100"/>
        <source>TEACHING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="126"/>
        <source>MOLD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="302"/>
        <source>AUTO RUN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="628"/>
        <source>MONITOR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="804"/>
        <source>SETTING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="980"/>
        <source>LOG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="1156"/>
        <source>SPECIAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="1482"/>
        <source>HOMING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.ui" line="1799"/>
        <source>MANUAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="169"/>
        <source>Interlock Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="170"/>
        <source>Interlock is curruntly disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="170"/>
        <source>This is the test mode. Do you want it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="255"/>
        <source>Safety Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="257"/>
        <source>Safety Zone OFF?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="257"/>
        <source>Be careful!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="259"/>
        <source>Safety Zone ON?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="319"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="320"/>
        <source>Do you want to disable this robot?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="339"/>
        <source>Fail Robot Not Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="340"/>
        <source>The robot is in the mold!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2.cpp" line="341"/>
        <source>Plase move to the out of mold and Try again!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>page_main2_log</name>
    <message>
        <location filename="../page_main2_log.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>page_main2_mold</name>
    <message>
        <location filename="../page_main2_mold.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>page_main2_monitor</name>
    <message>
        <location filename="../page_main2_monitor.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_monitor.ui" line="237"/>
        <source>Motor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_monitor.ui" line="464"/>
        <source>SData</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_monitor.ui" line="787"/>
        <source>Interlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_monitor.ui" line="202"/>
        <source>I/O</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>page_main2_setting</name>
    <message>
        <location filename="../page_main2_setting.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_setting.ui" line="106"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_setting.ui" line="141"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_setting.ui" line="595"/>
        <source>Special</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_setting.ui" line="726"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_setting.ui" line="1436"/>
        <source>Use/Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_setting.ui" line="464"/>
        <source>Date/Time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>page_main2_special</name>
    <message>
        <location filename="../page_main2_special.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_special.ui" line="202"/>
        <source>S-Work</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_special.ui" line="333"/>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_special.ui" line="467"/>
        <source>Task</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>page_main2_teach</name>
    <message>
        <location filename="../page_main2_teach.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_teach.ui" line="106"/>
        <source>MODE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_teach.ui" line="333"/>
        <source>POSITION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_teach.ui" line="467"/>
        <source>TIMER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_teach.ui" line="598"/>
        <source>STEP EDIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_teach.ui" line="1209"/>
        <source>COMMON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../page_main2_teach.ui" line="1340"/>
        <source>EASY SET</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>top_main</name>
    <message>
        <location filename="../top_main.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_main.ui" line="235"/>
        <source>SELECTED MOLD NAME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_main.ui" line="148"/>
        <source>12:55:55</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_main.ui" line="544"/>
        <source>%p%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_main.ui" line="125"/>
        <source>2016-08-01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_main.ui" line="1115"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>top_speed</name>
    <message>
        <location filename="../top_speed.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_speed.ui" line="79"/>
        <source>%p%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_speed.ui" line="318"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_speed.ui" line="366"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_speed.ui" line="414"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>top_sub</name>
    <message>
        <location filename="../top_sub.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_sub.ui" line="106"/>
        <source>Selected Mold Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_sub.ui" line="415"/>
        <source>%p%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../top_sub.ui" line="1088"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>view_param</name>
    <message>
        <location filename="../view_param.ui" line="105"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view_param.ui" line="277"/>
        <source>Machine Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view_param.ui" line="607"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view_param.ui" line="700"/>
        <source>SAVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view_param.ui" line="835"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view_param.ui" line="864"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view_param.cpp" line="23"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view_param.cpp" line="24"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_backup</name>
    <message>
        <location filename="../widget_backup.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.ui" line="109"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.ui" line="313"/>
        <source>ALL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.ui" line="519"/>
        <source>Backup Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.ui" line="642"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="72"/>
        <source>Mold Recipe Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="73"/>
        <source>Robot Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="74"/>
        <source>Robot Configulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="185"/>
        <source>Not found Selected Item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="185"/>
        <source>Please Select Item. Try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="194"/>
        <source>Would you want to Backup Process?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="194"/>
        <source>Next Step is input name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="199"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="208"/>
        <source>INPUT ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="209"/>
        <source>Invalid Name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="220"/>
        <source>Would you want to Backup this name?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="233"/>
        <source>Make Directory Fail!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="245"/>
        <source>Fail to Make Directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="258"/>
        <source>Fail to Bakcup Molde Recipe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="271"/>
        <source>Fail to Bakcup Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="285"/>
        <source>Fail to Bakcup Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup.cpp" line="293"/>
        <source>Backup Success!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_backup_recovery</name>
    <message>
        <location filename="../widget_backup_recovery.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery.ui" line="215"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery.ui" line="338"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery.ui" line="461"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery.ui" line="612"/>
        <source>9999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery.ui" line="692"/>
        <source>Total :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery.ui" line="1229"/>
        <source>Selection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_backup_recovery2</name>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="215"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="338"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="461"/>
        <location filename="../widget_backup_recovery2.ui" line="2332"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="612"/>
        <source>9999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="692"/>
        <source>Total :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="1800"/>
        <source>Recovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="1998"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="2062"/>
        <source>2017.01.25</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="2094"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="2200"/>
        <source>Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.ui" line="2303"/>
        <source>12:12:12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.cpp" line="282"/>
        <source>No Selected item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.cpp" line="292"/>
        <source>Mold Recipes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.cpp" line="297"/>
        <source>Robot Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.cpp" line="301"/>
        <source>Robot Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.cpp" line="307"/>
        <source>Would you want to Recovery?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.cpp" line="324"/>
        <location filename="../widget_backup_recovery2.cpp" line="351"/>
        <location filename="../widget_backup_recovery2.cpp" line="364"/>
        <source>Fail to recovery!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.cpp" line="372"/>
        <source>Success to recovery!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_backup_recovery2.cpp" line="372"/>
        <source>Execute System Reboot!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_jogmodule</name>
    <message>
        <location filename="../widget_jogmodule.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule.ui" line="124"/>
        <source>%v%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule.ui" line="232"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule.ui" line="261"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule.ui" line="386"/>
        <source>0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule.ui" line="415"/>
        <source>0.01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule.ui" line="532"/>
        <source>Jogging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule.ui" line="561"/>
        <source>Inching</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_jogmodule2</name>
    <message>
        <location filename="../widget_jogmodule2.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule2.ui" line="92"/>
        <source>Jogging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule2.ui" line="121"/>
        <source>Inching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule2.ui" line="272"/>
        <source>%v%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule2.ui" line="380"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule2.ui" line="409"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule2.ui" line="534"/>
        <source>0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule2.ui" line="563"/>
        <source>0.01</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_jogmodule3</name>
    <message>
        <location filename="../widget_jogmodule3.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule3.ui" line="150"/>
        <source>Jogging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule3.ui" line="246"/>
        <source>Inching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule3.ui" line="430"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule3.ui" line="526"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule3.ui" line="718"/>
        <source>0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule3.ui" line="814"/>
        <source>0.01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule3.ui" line="1012"/>
        <source>%v%</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_jogmodule4</name>
    <message>
        <location filename="../widget_jogmodule4.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule4.ui" line="159"/>
        <source>Jogging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule4.ui" line="255"/>
        <source>Inching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule4.ui" line="479"/>
        <source>%v%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule4.ui" line="654"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule4.ui" line="750"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule4.ui" line="942"/>
        <source>0.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_jogmodule4.ui" line="1038"/>
        <source>0.01</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_stepedit_program</name>
    <message>
        <location filename="../widget_stepedit_program.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.ui" line="178"/>
        <location filename="../widget_stepedit_program.ui" line="641"/>
        <location filename="../widget_stepedit_program.ui" line="701"/>
        <location filename="../widget_stepedit_program.ui" line="961"/>
        <location filename="../widget_stepedit_program.ui" line="1221"/>
        <location filename="../widget_stepedit_program.ui" line="1481"/>
        <location filename="../widget_stepedit_program.ui" line="1741"/>
        <location filename="../widget_stepedit_program.ui" line="2001"/>
        <location filename="../widget_stepedit_program.ui" line="2261"/>
        <location filename="../widget_stepedit_program.ui" line="2521"/>
        <location filename="../widget_stepedit_program.ui" line="2781"/>
        <location filename="../widget_stepedit_program.ui" line="3041"/>
        <location filename="../widget_stepedit_program.ui" line="4036"/>
        <location filename="../widget_stepedit_program.ui" line="4059"/>
        <location filename="../widget_stepedit_program.ui" line="4082"/>
        <source>Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.ui" line="295"/>
        <source>9999</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.ui" line="581"/>
        <location filename="../widget_stepedit_program.ui" line="901"/>
        <location filename="../widget_stepedit_program.ui" line="1161"/>
        <location filename="../widget_stepedit_program.ui" line="1421"/>
        <location filename="../widget_stepedit_program.ui" line="1681"/>
        <location filename="../widget_stepedit_program.ui" line="1941"/>
        <location filename="../widget_stepedit_program.ui" line="2201"/>
        <location filename="../widget_stepedit_program.ui" line="2461"/>
        <location filename="../widget_stepedit_program.ui" line="2721"/>
        <location filename="../widget_stepedit_program.ui" line="2981"/>
        <location filename="../widget_stepedit_program.ui" line="3241"/>
        <location filename="../widget_stepedit_program.ui" line="3585"/>
        <location filename="../widget_stepedit_program.ui" line="4105"/>
        <location filename="../widget_stepedit_program.ui" line="4128"/>
        <location filename="../widget_stepedit_program.ui" line="4151"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.ui" line="3358"/>
        <source>wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.ui" line="3608"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.ui" line="3631"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.cpp" line="114"/>
        <location filename="../widget_stepedit_program.cpp" line="456"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.cpp" line="116"/>
        <source>Do you want save and close?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.cpp" line="116"/>
        <source>Program are modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.cpp" line="172"/>
        <location filename="../widget_stepedit_program.cpp" line="192"/>
        <source>Don&apos;t have program!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.cpp" line="432"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.cpp" line="458"/>
        <source>Do you want delete this line?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.cpp" line="458"/>
        <source>You can reset for recoverly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_program.cpp" line="473"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>widget_stepedit_programrun</name>
    <message>
        <location filename="../widget_stepedit_programrun.ui" line="78"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_programrun.ui" line="233"/>
        <location filename="../widget_stepedit_programrun.ui" line="401"/>
        <location filename="../widget_stepedit_programrun.ui" line="509"/>
        <location filename="../widget_stepedit_programrun.ui" line="617"/>
        <location filename="../widget_stepedit_programrun.ui" line="725"/>
        <location filename="../widget_stepedit_programrun.ui" line="833"/>
        <location filename="../widget_stepedit_programrun.ui" line="941"/>
        <location filename="../widget_stepedit_programrun.ui" line="1049"/>
        <location filename="../widget_stepedit_programrun.ui" line="1157"/>
        <location filename="../widget_stepedit_programrun.ui" line="1265"/>
        <location filename="../widget_stepedit_programrun.ui" line="1373"/>
        <location filename="../widget_stepedit_programrun.ui" line="1481"/>
        <source>1000</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_programrun.ui" line="293"/>
        <location filename="../widget_stepedit_programrun.ui" line="353"/>
        <location filename="../widget_stepedit_programrun.ui" line="461"/>
        <location filename="../widget_stepedit_programrun.ui" line="569"/>
        <location filename="../widget_stepedit_programrun.ui" line="677"/>
        <location filename="../widget_stepedit_programrun.ui" line="785"/>
        <location filename="../widget_stepedit_programrun.ui" line="893"/>
        <location filename="../widget_stepedit_programrun.ui" line="1001"/>
        <location filename="../widget_stepedit_programrun.ui" line="1109"/>
        <location filename="../widget_stepedit_programrun.ui" line="1217"/>
        <location filename="../widget_stepedit_programrun.ui" line="1325"/>
        <location filename="../widget_stepedit_programrun.ui" line="1433"/>
        <source>Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_programrun.ui" line="1536"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_programrun.ui" line="1559"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_programrun.ui" line="1582"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget_stepedit_programrun.ui" line="1705"/>
        <source>wait</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
