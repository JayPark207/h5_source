#include "hymode.h"

HyMode::HyMode(HyRecipe* recipe)
{
    m_Recipe = recipe;

    Init();

}

void HyMode::Init()
{
   Init_Common();
   Init_Takeout();
   Init_Unload();
   Init_Intlock();
   Init_Sensor();
}


bool HyMode::Read(MODE_GROUP grp)
{
    QVector<HyRecipe::RECIPE_NUMBER> _var;
    QVector<float> _data;

    _var.clear();
    _data.clear();

    int i,j;
    for(i=0;i<m_Data[grp].count();i++)
    {
        for(j=0;j<m_Data[grp][i].Vars.count();j++)
        {
            _var.append((HyRecipe::RECIPE_NUMBER)m_Data[grp][i].Vars[j]);
        }
    }

    if(m_Recipe->Gets(_var, _data))
    {
        // save data
        for(i=0;i<m_Data[grp].count();i++)
        {
            m_Data[grp][i].Datas.clear();
            for(j=0;j<m_Data[grp][i].Vars.count();j++)
            {
                 m_Data[grp][i].Datas.append(_data.first());
                 _data.pop_front();
            }
        }
        if(!_data.isEmpty())
            qDebug() << "data number error!!" << grp;

        // make Result.
        MakeResult(grp);

        return true;
    }

    return false;
}

void HyMode::MakeResult(MODE_GROUP grp)
{
    for(int i=0;i<m_Data[grp].count();i++)
        (this->*m_Data[grp][i].Func)(grp,i);
}



/** MGRP_COMMON **/

void HyMode::Init_Common()
{
    int grp = MGRP_COMMON;
    ST_MODE_DATA data;

    // all data clear.
    this->m_Data[grp].clear();

    /*Clear(data);
    data.Name = tr("Use Arm");
    data.ItemName.append(tr("Product Arm"));
    data.ItemName.append(tr("Product & Runner Arm"));
    data.ItemName.append(tr("Runner Arm"));
    data.Vars.append(HyRecipe::mdUseArm);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);*/

    /*Clear(data);
    data.Name = tr("Product Takeout");
    data.ItemName.append(tr("Moving Axis"));
    data.ItemName.append(tr("Fix Axis"));
    data.Vars.append(HyRecipe::mdProdTakeout);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);*/

    Clear(data);
    data.Name = tr("Jig Rotation");
    data.ItemName.append(tr("After Travel"));
    data.ItemName.append(tr("Before Travel"));
    data.ItemName.append(tr("During Travel"));
    data.Vars.append(HyRecipe::mdRotation);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    /*Clear(data);
    data.Name = tr("Jig Swivel");
    data.ItemName.append(tr("No Swivel"));
    data.ItemName.append(tr("In Mold"));
    data.ItemName.append(tr("After Travel"));
    data.ItemName.append(tr("In Mold & After Travel"));
    data.Vars.append(HyRecipe::mdSwivel);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);*/

    Clear(data);
    data.Name = tr("Insert");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdInsert);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("External Wait");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdExternWait);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    /*Clear(data);
    data.Name = tr("Mold Cleaning");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdMoldClean);
    data.Type = MODE_MOLDCLEAN;
    this->m_Data[grp].append(data);*/

    Clear(data);
    data.Name = tr("Close Wait");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdCloseWait);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    // add here...

}

/** =========== **/

/** MGRP_TAKEOUT **/

void HyMode::Init_Takeout()
{
    int grp = MGRP_TAKEOUT;
    ST_MODE_DATA data;

    // all data clear.
    this->m_Data[grp].clear();

    Clear(data);
    data.Name = tr("Takeout Method");
    data.ItemName.append(tr("Vac1"));
    data.ItemName.append(tr("Vac2"));
    data.ItemName.append(tr("Vac3"));
    data.ItemName.append(tr("Vac4"));
    data.ItemName.append(tr("Chuck"));
    data.ItemName.append(tr("Gripper"));
    data.ItemName.append(tr("User1"));
    data.ItemName.append(tr("User2"));
    data.ItemName.append(tr("User3"));
    data.ItemName.append(tr("User4"));
    data.Vars.append(HyRecipe::mdToutVac1);
    data.Vars.append(HyRecipe::mdToutVac2);
    data.Vars.append(HyRecipe::mdToutVac3);
    data.Vars.append(HyRecipe::mdToutVac4);
    data.Vars.append(HyRecipe::mdToutChuck);
    data.Vars.append(HyRecipe::mdToutGrip);
    data.Vars.append(HyRecipe::mdToutUser1);
    data.Vars.append(HyRecipe::mdToutUser2);
    data.Vars.append(HyRecipe::mdToutUser3);
    data.Vars.append(HyRecipe::mdToutUser4);
    data.Type = MODE_TAKEOUTMETHOD;
    data.Func = &HyMode::MakeResult_TakeoutMethod;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Takeout Timing");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdToutTiming);
    data.Type = MODE_TAKEOUTTIMING;
    this->m_Data[grp].append(data);

    /*Clear(data);
    data.Name = tr("J-Motion");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdJMotion);
    data.Type = MODE_JMOTION;
    this->m_Data[grp].append(data);*/

    Clear(data);
    data.Name = tr("Undercut");
    data.ItemName.append(tr("No Use"));
    QString str;
    for(int i=0;i<MAX_UNDERCUT_POS;i++)
    {
        str = QString("%1 ").arg(i+1);
        str += QString(tr("Times"));
        data.ItemName.append(str);
    }
    data.Vars.append(HyRecipe::mdUndercut);
    data.Type = MODE_UNDERCUT;
    this->m_Data[grp].append(data);

    /*Clear(data);
    data.Name = tr("Sync Back");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    //data.Vars.append(HyRecipe::mdSoftTorque);
    data.Vars.append(HyRecipe::mdSyncBack);
    data.Type = MODE_SYNCBACK;
    this->m_Data[grp].append(data);*/

    Clear(data);
    data.Name = tr("I-Stop on Error");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdIStopTErr);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    // add here..

}

void HyMode::MakeResult_TakeoutMethod(int grp, int index)
{
    ST_MODE_DATA dat = m_Data[grp][index];

    if(dat.Datas.isEmpty())
    {
        dat.DispResult.clear();
        return;
    }
    if(dat.Datas.count() != dat.ItemName.count())
    {
        dat.DispResult.clear();
        return;
    }

    QString str;
    str.clear();
    for(int i=0;i<dat.Datas.count();i++)
    {
        if((int)dat.Datas[i] != 0)
        {
            str += dat.ItemName[i];
            str += QString(",");
        }
    }
    if(!str.isEmpty())
        str.remove(str.length()-1,1);

    m_Data[grp][index].DispResult = str;
}
/** ================ **/

/** MGRP_UNLOAD **/
void HyMode::Init_Unload()
{
    int grp = MGRP_UNLOAD;
    ST_MODE_DATA data;

    // all data clear.
    this->m_Data[grp].clear();

    Clear(data);
    data.Name = tr("Product Unload");
    data.ItemName.append(tr("Normal"));
    data.ItemName.append(tr("In mold"));
    data.ItemName.append(tr("Array"));
    data.ItemName.append(tr("Sequential"));
    data.ItemName.append(tr("Array & Sequential"));
    data.Vars.append(HyRecipe::mdProdUnload);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Unload Method");
    data.ItemName.append(tr("Vac1"));
    data.ItemName.append(tr("Vac2"));
    data.ItemName.append(tr("Vac3"));
    data.ItemName.append(tr("Vac4"));
    data.ItemName.append(tr("Chuck"));
    data.ItemName.append(tr("Gripper"));
    data.ItemName.append(tr("User1"));
    data.ItemName.append(tr("User2"));
    data.ItemName.append(tr("User3"));
    data.ItemName.append(tr("User4"));
    data.Vars.append(HyRecipe::mdToutVac1);
    data.Vars.append(HyRecipe::mdToutVac2);
    data.Vars.append(HyRecipe::mdToutVac3);
    data.Vars.append(HyRecipe::mdToutVac4);
    data.Vars.append(HyRecipe::mdToutChuck);
    data.Vars.append(HyRecipe::mdToutGrip);
    data.Vars.append(HyRecipe::mdToutUser1);
    data.Vars.append(HyRecipe::mdToutUser2);
    data.Vars.append(HyRecipe::mdToutUser3);
    data.Vars.append(HyRecipe::mdToutUser4);
    data.Type = MODE_UNLOADMETHOD;
    data.Func = &HyMode::MakeResult_UnloadMethod;
    this->m_Data[grp].append(data);

    /*Clear(data);
    data.Name = tr("Runner Unload");
    data.ItemName.append(tr("Normal"));
    data.ItemName.append(tr("During Traverse"));
    data.ItemName.append(tr("During Return"));
    data.ItemName.append(tr("In mold"));
    data.Vars.append(HyRecipe::mdRunUnload);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);*/

    Clear(data);
    data.Name = tr("Conveyor");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdConv);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    /*Clear(data);
    data.Name = tr("Conveyor Time");
    data.Vars.append(HyRecipe::tConvTime);
    data.Type = MODE_CONVTIME;
    data.Func = &HyMode::MakeResult_ConvTime;
    this->m_Data[grp].append(data);*/

    /*Clear(data);
    data.Name = tr("Grab Complete");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdGrabComp);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Grab Complete Time");
    data.Vars.append(HyRecipe::tGrabCompTime);
    data.Type = MODE_GRABCOMPTIME;
    data.Func = &HyMode::MakeResult_GrapCompTime;
    this->m_Data[grp].append(data);*/

    Clear(data);
    data.Name = tr("Unload Middle Pos");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdUnloadMidPos);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Unload Back");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdUnloadBack);
    data.Type = MODE_UNLOADBACK; // after make page.
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Dual Unload");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdDualUnload);
    data.Type = MODE_DUALUNLOAD; // after make page.
    this->m_Data[grp].append(data);

    // add here...
}

void HyMode::MakeResult_ConvTime(int grp, int index)
{
    float data = 0;
    if(!m_Data[grp][index].Datas.isEmpty())
        data = m_Data[grp][index].Datas.first();

    QString str;
    str.sprintf(FORMAT_TIME, data);
    str += " ";
    str += tr("sec");

    m_Data[grp][index].DispResult = str;
}
void HyMode::MakeResult_GrapCompTime(int grp, int index)
{
    float data = 0;
    if(!m_Data[grp][index].Datas.isEmpty())
        data = m_Data[grp][index].Datas.first();

    QString str;
    str.sprintf(FORMAT_TIME, data);
    str += " ";
    str += tr("sec");

    m_Data[grp][index].DispResult = str;
}

void HyMode::MakeResult_UnloadMethod(int grp, int index)
{
    ST_MODE_DATA dat = m_Data[grp][index];

    if(dat.Datas.isEmpty())
    {
        dat.DispResult.clear();
        return;
    }
    if(dat.Datas.count() != dat.ItemName.count())
    {
        dat.DispResult.clear();
        return;
    }

    QString str;
    str.clear();
    for(int i=0;i<dat.Datas.count();i++)
    {
        if((int)dat.Datas[i] > 0)
        {
            str += dat.ItemName[i];
            str += QString(",");
        }
    }
    if(!str.isEmpty())
        str.remove(str.length()-1,1);

    m_Data[grp][index].DispResult = str;
}

/** =========== **/

/** MGRP_INTLOCK **/
void HyMode::Init_Intlock()
{
    int grp = MGRP_INTLOCK;
    ST_MODE_DATA data;

    // all data clear.
    this->m_Data[grp].clear();

    /*
    Clear(data);
    data.Name = tr("Interlock Mode");
    data.ItemName.append(tr("Standard"));
    data.ItemName.append(tr("Euromap 67"));
    data.ItemName.append(tr("Euromap 12"));
    data.Vars.append(HyRecipe::mdEuromap);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Mold Close Start");
    data.ItemName.append(tr("After Up"));
    data.ItemName.append(tr("After Rotation"));
    data.Vars.append(HyRecipe::mdMoldClose);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Mold Opening");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdMoldOpening);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Ejector Fwd");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdEjectFwd);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Ejector Fwd Complete");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdEjectFwdComp);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Ejector Bwd");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdEjectBwd);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Ejector Bwd Complete");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdEjectBwdComp);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Core Fwd");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdCoreFwd);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Core Fwd Complete");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdCoreFwdComp);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Core Bwd");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdCoreBwd);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Core Bwd Complete");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdCoreBwdComp);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);
    */
    // add here...

    /*
    // new euromap system.
    Clear(data);
    data.Name = tr("Euromap Use");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::emUse);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);
    */

    Clear(data);
    data.Name = tr("Type");
    data.ItemName.append(tr("Standard"));
    data.ItemName.append(tr("Euromap 67"));
    data.ItemName.append(tr("Euromap 12"));
    data.Vars.append(HyRecipe::emType);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Safety Device Mode");
    data.ItemName.append(tr("No check"));
    data.ItemName.append(tr("Check before takeout"));
    data.ItemName.append(tr("Check during takeout"));
    data.ItemName.append(tr("Check always"));
    data.Vars.append(HyRecipe::emSdMode);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Operation Mode");
    data.ItemName.append(tr("Use IMM auto signal"));
    data.ItemName.append(tr("Ignore IMM auto signal"));
    data.Vars.append(HyRecipe::emAutoMode);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Mold Mode");
    data.ItemName.append(tr("Normal"));
    data.ItemName.append(tr("Use to mold opening"));
    data.Vars.append(HyRecipe::emMoldMode);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Ejector Mode");
    data.ItemName.append(tr("Not Use"));
    data.ItemName.append(tr("Fwd ON, Bwd OFF"));
    data.ItemName.append(tr("Fwd ON, Bwd ON(no check)"));
    data.ItemName.append(tr("Fwd ON, Bwd ON(check)"));
    data.Vars.append(HyRecipe::emEjectMode);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    //new
    Clear(data);
    data.Name = tr("Fast Ejector Fwd");
    data.ItemName.append(tr("Not Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::emFastEject);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Core 1 Mode");
    data.ItemName.append(tr("Not Use"));
    data.ItemName.append(tr("one-direction pos.1"));
    data.ItemName.append(tr("one-direction pos.2"));
    data.ItemName.append(tr("two-direction(no check)"));
    data.ItemName.append(tr("two-direction(check)"));
    data.Vars.append(HyRecipe::emCoreMode1);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Core 2 Mode");
    data.ItemName.append(tr("Not Use"));
    data.ItemName.append(tr("one-direction pos.1"));
    data.ItemName.append(tr("one-direction pos.2"));
    data.ItemName.append(tr("two-direction(no check)"));
    data.ItemName.append(tr("two-direction(check)"));
    data.Vars.append(HyRecipe::emCoreMode2);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    // euromap not use mode.
    Clear(data);
    data.Name = tr("Interlock Use");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::emUse);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

}
/** =========== **/

/** MGRP_SENSOR **/
void HyMode::Init_Sensor()
{
    int grp = MGRP_SENSOR;
    ST_MODE_DATA data;

    // all data clear.
    this->m_Data[grp].clear();

    Clear(data);
    data.Name = tr("Weight");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Weight1"));
    data.ItemName.append(tr("Weight2"));
    data.ItemName.append(tr("Weight3"));
    data.ItemName.append(tr("Weight4"));
    data.Vars.append(HyRecipe::mdWeight1);
    data.Vars.append(HyRecipe::mdWeight2);
    data.Vars.append(HyRecipe::mdWeight3);
    data.Vars.append(HyRecipe::mdWeight4);
    data.Type = MODE_WEIGHT;
    data.Func = &HyMode::MakeResult_Weight;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Temperature");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Temp1"));
    data.ItemName.append(tr("Temp2"));
    data.ItemName.append(tr("Temp3"));
    data.ItemName.append(tr("Temp4"));
    data.Vars.append(HyRecipe::mdTemp1);
    data.Vars.append(HyRecipe::mdTemp2);
    data.Vars.append(HyRecipe::mdTemp3);
    data.Vars.append(HyRecipe::mdTemp4);
    data.Type = MODE_TEMP;
    data.Func = &HyMode::MakeResult_Temp;
    this->m_Data[grp].append(data);

    Clear(data);
    data.Name = tr("Ionizer");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("Use"));
    data.Vars.append(HyRecipe::mdIonizer);
    data.Type = MODE_SELITEM;
    this->m_Data[grp].append(data);

    /*Clear(data);
    data.Name = tr("E-Sensor");
    data.ItemName.append(tr("No Use"));
    data.ItemName.append(tr("E-Sensor1"));
    data.ItemName.append(tr("E-Sensor2"));
    data.ItemName.append(tr("E-Sensor3"));
    data.ItemName.append(tr("E-Sensor4"));
    data.Vars.append(HyRecipe::mdESen1);
    data.Vars.append(HyRecipe::mdESen2);
    data.Vars.append(HyRecipe::mdESen3);
    data.Vars.append(HyRecipe::mdESen4);
    data.Type = MODE_ESENSOR;
    data.Func = &HyMode::MakeResult_ESensor;
    this->m_Data[grp].append(data);*/

    // add here...

}

void HyMode::MakeResult_Weight(int grp, int index)
{
    ST_MODE_DATA dat = m_Data[grp][index];

    if(dat.Datas.isEmpty()
    || dat.Vars.size() != dat.Datas.size())
    {
        m_Data[grp][index].DispResult.clear();
        return;
    }

    QString str;
    str.clear();
    for(int i=0;i<dat.Datas.count();i++)
    {
        if((int)dat.Datas[i] > 0)
        {
            if(!str.isEmpty())
                str += ",";
            str += dat.ItemName[i+1];
        }
    }

    if(str.isEmpty())
        str = dat.ItemName[0];

    m_Data[grp][index].DispResult = str;
}
void HyMode::MakeResult_Temp(int grp, int index)
{
    ST_MODE_DATA dat = m_Data[grp][index];

    if(dat.Datas.isEmpty()
    || dat.Vars.size() != dat.Datas.size())
    {
        m_Data[grp][index].DispResult.clear();
        return;
    }

    QString str;
    str.clear();

    for(int i=0;i<dat.Datas.count();i++)
    {
        if((int)dat.Datas[i] > 0)
        {
            if(!str.isEmpty())
                str += ",";
            str += dat.ItemName[i+1];
        }
    }

    if(str.isEmpty())
        str = dat.ItemName[0];

    m_Data[grp][index].DispResult = str;
}
void HyMode::MakeResult_ESensor(int grp, int index)
{
    ST_MODE_DATA dat = m_Data[grp][index];

    if(dat.Datas.isEmpty()
    || dat.Vars.size() != dat.Datas.size())
    {
        m_Data[grp][index].DispResult.clear();
        return;
    }

    QString str;
    str.clear();

    for(int i=0;i<dat.Datas.count();i++)
    {
        if((int)dat.Datas[i] > 0)
        {
            if(!str.isEmpty())
                str += ",";
            str += dat.ItemName[i+1];
        }
    }

    if(str.isEmpty())
        str = dat.ItemName[0];

    m_Data[grp][index].DispResult = str;
}

/** =========== **/

/** Private Functions **/

void HyMode::MakeResult_Normal(int grp, int index)
{
    ST_MODE_DATA dat = m_Data[grp][index];
    int value = 0;
    if(!dat.Datas.isEmpty())
        value = dat.Datas[0];

    QString str;
    if(value < dat.ItemName.count())
        str = dat.ItemName[value];
    else
        str.clear();

    m_Data[grp][index].DispResult = str;
}

void HyMode::Clear(ST_MODE_DATA &dat)
{
    dat.Name.clear();
    dat.ItemName.clear();
    dat.Vars.clear();
    dat.Datas.clear();
    dat.Type = MODE_SELITEM;
    dat.bNA = false;
    dat.bDiff = false;
    dat.DispResult.clear();
    dat.Func = &HyMode::MakeResult_Normal;
}
