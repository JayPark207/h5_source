#include "dialog_euromap.h"
#include "ui_dialog_euromap.h"

dialog_euromap::dialog_euromap(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_euromap)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // Tabs & Panel
    Panel = new QStackedWidget(this);
    //Panel->setGeometry(0, 60, 800, 335);
    Panel->setGeometry(ui->wigPanel->x(),
                       ui->wigPanel->y(),
                       ui->wigPanel->width(),
                       ui->wigPanel->height());
    Panel->addWidget(ui->wigPanel); // ref.panel
    Panel->addWidget(ui->wigPanel_2);
    Panel->addWidget(ui->wigPanel_3);
    Panel->setCurrentIndex(0);

    m_vtTab.clear();m_vtTabPic.clear();m_vtTabLed.clear();
    m_vtTab.append(ui->btnTab);  m_vtTabPic.append(ui->btnTabPic);  m_vtTabLed.append(ui->lbTabLed);
    m_vtTab.append(ui->btnTab_2);m_vtTabPic.append(ui->btnTabPic_2);m_vtTabLed.append(ui->lbTabLed_2);
    m_vtTab.append(ui->btnTab_3);m_vtTabPic.append(ui->btnTabPic_3);m_vtTabLed.append(ui->lbTabLed_3);

    int i;
    for(i=0;i<m_vtTabPic.count();i++)
    {
        connect(m_vtTabPic[i],SIGNAL(mouse_release()),this,SLOT(onTab()));
        connect(m_vtTab[i],SIGNAL(mouse_press()),m_vtTabPic[i],SLOT(press()));
        connect(m_vtTab[i],SIGNAL(mouse_release()),m_vtTabPic[i],SLOT(release()));
    }

    // current 3rd Tab not use.
    ui->wigTab_3->setVisible(false);


    // Tab1
    m_vtRtLed.clear();m_vtRtSym.clear();
    m_vtRtLed.append(ui->lbRtLed);   m_vtRtSym.append(ui->lbRtSym);
    m_vtRtLed.append(ui->lbRtLed_2); m_vtRtSym.append(ui->lbRtSym_2);
    m_vtRtLed.append(ui->lbRtLed_3); m_vtRtSym.append(ui->lbRtSym_3);
    m_vtRtLed.append(ui->lbRtLed_4); m_vtRtSym.append(ui->lbRtSym_4);
    m_vtRtLed.append(ui->lbRtLed_5); m_vtRtSym.append(ui->lbRtSym_5);
    m_vtRtLed.append(ui->lbRtLed_6); m_vtRtSym.append(ui->lbRtSym_6);
    m_vtRtLed.append(ui->lbRtLed_7); m_vtRtSym.append(ui->lbRtSym_7);
    m_vtRtLed.append(ui->lbRtLed_8); m_vtRtSym.append(ui->lbRtSym_8);
    m_vtRtLed.append(ui->lbRtLed_9); m_vtRtSym.append(ui->lbRtSym_9);
    m_vtRtLed.append(ui->lbRtLed_10);m_vtRtSym.append(ui->lbRtSym_10);
    m_vtRtLed.append(ui->lbRtLed_11);m_vtRtSym.append(ui->lbRtSym_11);
    m_vtRtLed.append(ui->lbRtLed_12);m_vtRtSym.append(ui->lbRtSym_12);
    m_vtRtLed.append(ui->lbRtLed_13);m_vtRtSym.append(ui->lbRtSym_13);
    m_vtRtLed.append(ui->lbRtLed_14);m_vtRtSym.append(ui->lbRtSym_14);
    m_vtRtLed.append(ui->lbRtLed_15);m_vtRtSym.append(ui->lbRtSym_15);
    m_vtRtLed.append(ui->lbRtLed_16);m_vtRtSym.append(ui->lbRtSym_16);

    for(i=0;i<m_vtRtSym.count();i++)
    {
        connect(m_vtRtSym[i],SIGNAL(mouse_release()),this,SLOT(onRobotSignal()));
    }

    m_vtImLed.clear();m_vtImSym.clear();
    m_vtImLed.append(ui->lbImLed);   m_vtImSym.append(ui->lbImSym);
    m_vtImLed.append(ui->lbImLed_2); m_vtImSym.append(ui->lbImSym_2);
    m_vtImLed.append(ui->lbImLed_3); m_vtImSym.append(ui->lbImSym_3);
    m_vtImLed.append(ui->lbImLed_4); m_vtImSym.append(ui->lbImSym_4);
    m_vtImLed.append(ui->lbImLed_5); m_vtImSym.append(ui->lbImSym_5);
    m_vtImLed.append(ui->lbImLed_6); m_vtImSym.append(ui->lbImSym_6);
    m_vtImLed.append(ui->lbImLed_7); m_vtImSym.append(ui->lbImSym_7);
    m_vtImLed.append(ui->lbImLed_8); m_vtImSym.append(ui->lbImSym_8);
    m_vtImLed.append(ui->lbImLed_9); m_vtImSym.append(ui->lbImSym_9);
    m_vtImLed.append(ui->lbImLed_10);m_vtImSym.append(ui->lbImSym_10);
    m_vtImLed.append(ui->lbImLed_11);m_vtImSym.append(ui->lbImSym_11);
    m_vtImLed.append(ui->lbImLed_12);m_vtImSym.append(ui->lbImSym_12);
    m_vtImLed.append(ui->lbImLed_13);m_vtImSym.append(ui->lbImSym_13);
    m_vtImLed.append(ui->lbImLed_14);m_vtImSym.append(ui->lbImSym_14);
    m_vtImLed.append(ui->lbImLed_15);m_vtImSym.append(ui->lbImSym_15);
    m_vtImLed.append(ui->lbImLed_16);m_vtImSym.append(ui->lbImSym_16);

    for(i=0;i<m_vtImSym.count();i++)
    {
        connect(m_vtImSym[i],SIGNAL(mouse_release()),this,SLOT(onImmSignal()));
    }

    connect(ui->btnRtForcePic,SIGNAL(mouse_release()),this,SLOT(onRobotForce()));
    connect(ui->btnRtForceIcon,SIGNAL(mouse_press()),ui->btnRtForcePic,SLOT(press()));
    connect(ui->btnRtForceIcon,SIGNAL(mouse_release()),ui->btnRtForcePic,SLOT(release()));

    connect(ui->btnImForcePic,SIGNAL(mouse_release()),this,SLOT(onImmForce()));
    connect(ui->btnImForceIcon,SIGNAL(mouse_press()),ui->btnImForcePic,SLOT(press()));
    connect(ui->btnImForceIcon,SIGNAL(mouse_release()),ui->btnImForcePic,SLOT(release()));



    // Tab2
    // Module Name table
    m_vtMdName.clear();
    m_vtMdName.append(ui->lbMdName);
    m_vtMdName.append(ui->lbMdName_2);
    m_vtMdName.append(ui->lbMdName_3);
    m_vtMdName.append(ui->lbMdName_4);
    m_vtMdName.append(ui->lbMdName_5);
    m_vtMdName.append(ui->lbMdName_6);

    for(i=0;i<m_vtMdName.count();i++)
    {
        connect(m_vtMdName[i],SIGNAL(mouse_release()),this,SLOT(onMduName()));
    }

    connect(ui->btnMdUpPic,SIGNAL(mouse_release()),this,SLOT(onMduUp()));
    connect(ui->btnMdUpIcon,SIGNAL(mouse_press()),ui->btnMdUpPic,SLOT(press()));
    connect(ui->btnMdUpIcon,SIGNAL(mouse_release()),ui->btnMdUpPic,SLOT(release()));

    connect(ui->btnMdDownPic,SIGNAL(mouse_release()),this,SLOT(onMduDown()));
    connect(ui->btnMdDownIcon,SIGNAL(mouse_press()),ui->btnMdDownPic,SLOT(press()));
    connect(ui->btnMdDownIcon,SIGNAL(mouse_release()),ui->btnMdDownPic,SLOT(release()));

    // Module Data table
    m_vtMdData.clear();m_vtMdVal.clear();m_vtMdValPic.clear();
    m_vtMdData.append(ui->lbMdData);  m_vtMdVal.append(ui->lbMdVal);  m_vtMdValPic.append(ui->lbMdValPic);
    m_vtMdData.append(ui->lbMdData_2);m_vtMdVal.append(ui->lbMdVal_2);m_vtMdValPic.append(ui->lbMdValPic_2);
    m_vtMdData.append(ui->lbMdData_3);m_vtMdVal.append(ui->lbMdVal_3);m_vtMdValPic.append(ui->lbMdValPic_3);
    m_vtMdData.append(ui->lbMdData_4);m_vtMdVal.append(ui->lbMdVal_4);m_vtMdValPic.append(ui->lbMdValPic_4);
    m_vtMdData.append(ui->lbMdData_5);m_vtMdVal.append(ui->lbMdVal_5);m_vtMdValPic.append(ui->lbMdValPic_5);
    m_vtMdData.append(ui->lbMdData_6);m_vtMdVal.append(ui->lbMdVal_6);m_vtMdValPic.append(ui->lbMdValPic_6);

    for(i=0;i<m_vtMdValPic.count();i++)
    {
        connect(m_vtMdValPic[i],SIGNAL(mouse_release()),this,SLOT(onMddValue()));
        connect(m_vtMdVal[i],SIGNAL(mouse_press()),m_vtMdValPic[i],SLOT(press()));
        connect(m_vtMdVal[i],SIGNAL(mouse_release()),m_vtMdValPic[i],SLOT(release()));
    }

    connect(ui->btnMddUpPic,SIGNAL(mouse_release()),this,SLOT(onMddUp()));
    connect(ui->btnMddUpIcon,SIGNAL(mouse_press()),ui->btnMddUpPic,SLOT(press()));
    connect(ui->btnMddUpIcon,SIGNAL(mouse_release()),ui->btnMddUpPic,SLOT(release()));

    connect(ui->btnMddDownPic,SIGNAL(mouse_release()),this,SLOT(onMddDown()));
    connect(ui->btnMddDownIcon,SIGNAL(mouse_press()),ui->btnMddDownPic,SLOT(press()));
    connect(ui->btnMddDownIcon,SIGNAL(mouse_release()),ui->btnMddDownPic,SLOT(release()));

    connect(ui->btnMddEditPic,SIGNAL(mouse_release()),this,SLOT(onMddEdit()));
    connect(ui->btnMddEditIcon,SIGNAL(mouse_press()),ui->btnMddEditPic,SLOT(press()));
    connect(ui->btnMddEditIcon,SIGNAL(mouse_release()),ui->btnMddEditPic,SLOT(release()));

    connect(ui->btnSimPic,SIGNAL(mouse_release()),this,SLOT(onSimRun()));
    connect(ui->btnSim,SIGNAL(mouse_press()),ui->btnSimPic,SLOT(press()));
    connect(ui->btnSim,SIGNAL(mouse_release()),ui->btnSimPic,SLOT(release()));

    // init
    pxmRunIcon.clear();

}

dialog_euromap::~dialog_euromap()
{
    delete ui;
}
void dialog_euromap::showEvent(QShowEvent *)
{
    gStart_IOUpdate();
    Update();
    timer->start();

    UserLevel();
}
void dialog_euromap::hideEvent(QHideEvent *)
{
    gStop_IOUpdate();
    timer->stop();

    Enable_RobotForce(false);
    Enable_ImmForce(false);

    ForceOff_SimRun(); // if out page for simrun on, forced off simrun.
}
void dialog_euromap::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_euromap::onClose()
{
    emit accept();
}

void dialog_euromap::UserLevel()
{
    int user_level = Param->Get(HyParam::USER_LEVEL).toInt();
    if(user_level < USER_LEVEL_HIGH)
    {
        ui->wigTab_2->setVisible(false);
        ui->wigRtForce->setVisible(false);
        ui->wigImForce->setVisible(false);
    }
    else
    {
        ui->wigTab_2->setVisible(true);
        ui->wigRtForce->setVisible(true);
        ui->wigImForce->setVisible(true);
    }
}

void dialog_euromap::Update()
{
    // common
    Display_Tab(0);

    // Tab1
    Enable_RobotForce(false);
    Enable_ImmForce(false);


    float data;
    int current_type = 0;
    if(Recipe->Get(HyRecipe::emType, &data))
        current_type = (int)data;

    Display_MainTitle(current_type);
    Display_RtName(current_type);
    Display_ImName(current_type);

    // Tab2
    m_nStartIndex_Mdu=0;m_nSelectedIndex_Mdu=0;
    Redraw_List_Mdu(m_nStartIndex_Mdu);
    m_nStartIndex_Mdd = 0;
    Read_ModuleData(m_nSelectedIndex_Mdu);
    Redraw_List_Mdd(m_nStartIndex_Mdd, m_nSelectedIndex_Mdu);
    Enable_MddEdit(false);

    Euromap->Get_SimRun(m_bSimRun);
    Display_SimRun(m_bSimRun);

}

void dialog_euromap::onTimer()
{
    switch(Panel->currentIndex())
    {
    case 0:
        Refresh_RtLed();
        Refresh_ImLed();
        break;
    case 1:
        break;
    case 2:
        break;
    }
}

void dialog_euromap::onTab()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtTabPic.indexOf(btn);
    if(index < 0) return;

    Display_Tab(index);
}

void dialog_euromap::Display_Tab(int tab_index)
{
    Panel->setCurrentIndex(tab_index);

    for(int i=0;i<m_vtTabLed.count();i++)
        m_vtTabLed[i]->setEnabled(i == tab_index);
}

void dialog_euromap::Display_RtName(int curr_type)
{
    QStringList types;
    bool enable;
    for(int i=0;i<m_vtRtSym.count();i++)
    {
        types = Euromap->OutRob[i].Type.split(",");
        enable = (types.indexOf(QString().setNum(curr_type)) >= 0);
        m_vtRtSym[i]->setEnabled(enable);
        m_vtRtSym[i]->setText(Euromap->OutRob[i].Name);
    }
}
void dialog_euromap::Display_ImName(int curr_type)
{
    QStringList types;
    bool enable;
    for(int i=0;i<m_vtImSym.count();i++)
    {
        types = Euromap->InImm[i].Type.split(",");
        enable = (types.indexOf(QString().setNum(curr_type)) >= 0);
        m_vtImSym[i]->setEnabled(enable);
        m_vtImSym[i]->setText(Euromap->InImm[i].Name);
    }
}

void dialog_euromap::Refresh_RtLed()
{
    bool bLed;
    for(int i=0;i<m_vtRtLed.count();i++)
    {
         bLed = Output->IsOn(Euromap->OutRob[i].IONum - 1);
         m_vtRtLed[i]->setEnabled(bLed);
    }
}
void dialog_euromap::Refresh_ImLed()
{
    bool bLed;
    for(int i=0;i<m_vtImLed.count();i++)
    {
         bLed = Input->IsOn(Euromap->InImm[i].IONum - 1);
         m_vtImLed[i]->setEnabled(bLed);
    }
}

void dialog_euromap::Display_MainTitle(int curr_type)
{
    QString title = tr("EUROMAP");

    switch(curr_type)
    {
    case 0:
        title += " (";
        title += tr("STANDARD");
        title += ")";
        break;

    case 1:
        title += " (";
        title += tr("EUROMAP 67");
        title += ")";
        break;

    case 2:
        title += " (";
        title += tr("EUROMAP 12");
        title += ")";
        break;
    }

    ui->lbTitle->setText(title);
}

void dialog_euromap::onRobotForce()
{
    bool bon = IsRobotForce();
    Enable_RobotForce(!bon);
}
void dialog_euromap::onImmForce()
{
    bool bon = IsImmForce();
    Enable_ImmForce(!bon);
}

void dialog_euromap::Enable_RobotForce(bool enable)
{
    ui->btnRtForcePic->setAutoFillBackground(enable);
}
bool dialog_euromap::IsRobotForce()
{
    return ui->btnRtForcePic->autoFillBackground();
}

void dialog_euromap::Enable_ImmForce(bool enable)
{
    ui->btnImForcePic->setAutoFillBackground(enable);

    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->setOverrideDIN(enable);
    if(ret < 0)
        qDebug() << "dialog_euromap / setOverrideDIN() ret=" << ret;
}
bool dialog_euromap::IsImmForce()
{
    return ui->btnImForcePic->autoFillBackground();
}

void dialog_euromap::onRobotSignal()
{
    if(!IsRobotForce())
        return;

    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtRtSym.indexOf(sel);
    if(index < 0) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->lbRtTitle->text());

    QString str;
    if(m_vtRtLed[index]->isEnabled())
        str = tr("ON -> OFF");
    else
        str = tr("OFF -> ON");

    conf->Message(Euromap->OutRob[index].Name, str);
    if(conf->exec() != QDialog::Accepted)
        return;


    int io_num = Euromap->OutRob[index].IONum;
    int out=0;
    if(Output->IsOn(io_num - 1))
        out = (-1)*io_num;
    else
        out = io_num;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setDO(&out, 1);
    if(ret < 0)
        qDebug() << "dialog_euromap / setDO() ret=" << ret;
}

void dialog_euromap::onImmSignal()
{
    if(!IsImmForce())
        return;

    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtImSym.indexOf(sel);
    if(index < 0) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->lbImTitle->text());

    QString str;
    if(m_vtImLed[index]->isEnabled())
        str = tr("ON -> OFF");
    else
        str = tr("OFF -> ON");

    conf->Message(Euromap->InImm[index].Name, str);
    if(conf->exec() != QDialog::Accepted)
        return;

    int io_num = Euromap->InImm[index].IONum;
    int in=0;
    if(Input->IsOn(io_num - 1))
        in = (-1)*io_num;
    else
        in = io_num;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setDI(in);
    if(ret < 0)
        qDebug() << "dialog_euromap / setDI() ret=" << ret;
}


/**
 * PANEL 2
**/

void dialog_euromap::Display_ListOn_Mdu(int table_index)
{
    for(int i=0;i<m_vtMdName.count();i++)
        m_vtMdName[i]->setAutoFillBackground(i == table_index);
}

void dialog_euromap::Redraw_List_Mdu(int start_index)
{
    int cal_size = Euromap->Module.size() - start_index;
    int index;
    QString name;

    Display_ListOn_Mdu(-1); // all off

    for(int i=0;i<m_vtMdName.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            name = Euromap->Module[index].Name;
        }
        else
        {
            // no data.
            name.clear();
        }

        m_vtMdName[i]->setText(name);

        if((start_index + i) == m_nSelectedIndex_Mdu)
            Display_ListOn_Mdu(i);
    }

    // display list size.
    ui->lbMdListSize->setText(QString().setNum(Euromap->Module.size()));
}

void dialog_euromap::onMduName()
{
    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtMdName.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nStartIndex_Mdu + index;
    if(list_index == m_nSelectedIndex_Mdu) return;

    if(list_index < Euromap->Module.size())
    {
        m_nSelectedIndex_Mdu = list_index;
        Redraw_List_Mdu(m_nStartIndex_Mdu);
        // Redraw Module Data Table
        // here...
        Read_ModuleData(m_nSelectedIndex_Mdu);
        m_nStartIndex_Mdd=0;
        Redraw_List_Mdd(m_nStartIndex_Mdd, m_nSelectedIndex_Mdu);
    }
}
void dialog_euromap::onMduUp()
{
    if(m_nStartIndex_Mdu <= 0)
        return;

    Redraw_List_Mdu(--m_nStartIndex_Mdu);
}
void dialog_euromap::onMduDown()
{
    if(m_nStartIndex_Mdu >= (Euromap->Module.size()- m_vtMdName.size()))
        return;

    Redraw_List_Mdu(++m_nStartIndex_Mdu);
}

bool dialog_euromap::Read_ModuleData(int module_index)
{
    if(module_index >= Euromap->Module.size())
        return false;

    // here...
    QStringList name = Euromap->Module[module_index].DataName;
    QList<cn_variant> data;
    int i;
    QString str;

    if(Recipe->GetVars(name, data))
    {
        m_ModuleData.clear();
        for(i=0;i<data.count();i++)
        {
            if(data[i].type < 0)
                m_ModuleData.append("");
            else
            {
                str.sprintf("%.2f", data[i].val.f32);
                m_ModuleData.append(str);
            }
        }
    }
    else
    {
        m_ModuleData.clear();
        for(i=0;i<name.count();i++)
            m_ModuleData.append("");
    }

    return true;
}

void dialog_euromap::Redraw_List_Mdd(int start_index, int select_index)
{
    if(select_index >= Euromap->Module.size())
        return;

    int cal_size = Euromap->Module[select_index].DataName.size() - start_index;
    int index;
    QString name, val;
    bool enable;

    for(int i=0;i<m_vtMdData.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            name = Euromap->Module[select_index].DataName[index];
            val = m_ModuleData[index];
            enable = true;
        }
        else
        {
            // no data.
            name.clear();
            val.clear();
            enable = false;
        }

        m_vtMdData[i]->setText(name);
        m_vtMdVal[i]->setText(val);

        m_vtMdValPic[i]->setEnabled(enable);
        m_vtMdVal[i]->setEnabled(enable);
    }

}

void dialog_euromap::onMddUp()
{
    if(m_nStartIndex_Mdd <= 0)
        return;

    Redraw_List_Mdd(--m_nStartIndex_Mdd, m_nSelectedIndex_Mdu);
}
void dialog_euromap::onMddDown()
{
    if(m_nStartIndex_Mdd >= (Euromap->Module[m_nSelectedIndex_Mdu].DataName.size()- m_vtMdData.size()))
        return;

    Redraw_List_Mdd(++m_nStartIndex_Mdd, m_nSelectedIndex_Mdu);
}

void dialog_euromap::onMddValue()
{
    if(!IsMddEdit())
        return;

    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtMdValPic.indexOf(btn);
    if(index < 0) return;

    QString data_name = m_vtMdData[index]->text();
    // add here...
    qDebug() << "click";

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-999999.9);
    np->m_numpad->SetMaxValue(999999.9);
    np->m_numpad->SetSosuNum(2);
    np->m_numpad->SetTitle(data_name);
    np->m_numpad->SetNum(m_vtMdVal[index]->text());

    if(np->exec() == QDialog::Accepted)
    {
        QStringList names;
        names.append(data_name);

        QList<cn_variant> datas;
        cn_variant data;
        data.type = CNVAR_FLOAT;
        data.val.f32 = (float)np->m_numpad->GetNumDouble();
        datas.append(data);

        if(Recipe->SetVars(names, datas))
        {
            Read_ModuleData(m_nSelectedIndex_Mdu);
            Redraw_List_Mdd(m_nStartIndex_Mdd, m_nSelectedIndex_Mdu);
        }
    }

}

void dialog_euromap::Enable_MddEdit(bool enable)
{
    ui->btnMddEditPic->setAutoFillBackground(enable);
}
bool dialog_euromap::IsMddEdit()
{
    return ui->btnMddEditPic->autoFillBackground();
}

void dialog_euromap::onMddEdit()
{
    bool enable = IsMddEdit();
    Enable_MddEdit(!enable);
}

void dialog_euromap::Display_SimRun(bool run)
{
    if(pxmRunIcon.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon99-5.png");   // STOP
        pxmRunIcon.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon99-3.png");   // RUN
        pxmRunIcon.append(QPixmap::fromImage(img));
    }

    if(run)
    {
        ui->btnSim->setText(tr("STOP"));
        ui->btnSimIcon->setPixmap(pxmRunIcon[1]);
    }
    else
    {
        ui->btnSim->setText(tr("RUN"));
        ui->btnSimIcon->setPixmap(pxmRunIcon[0]);
    }
}

void dialog_euromap::onSimRun()
{
    Euromap->Set_SimRun(!m_bSimRun);
    Euromap->Get_SimRun(m_bSimRun);
    Display_SimRun(m_bSimRun);
}

void dialog_euromap::ForceOff_SimRun()
{
    if(m_bSimRun)
        Euromap->Set_SimRun(false);
}

