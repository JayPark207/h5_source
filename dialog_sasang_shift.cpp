#include "dialog_sasang_shift.h"
#include "ui_dialog_sasang_shift.h"

dialog_sasang_shift::dialog_sasang_shift(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_sasang_shift)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtAxis.clear();
    m_vtAxis.append(ui->tbAxis);
    m_vtAxis.append(ui->tbAxis_2);
    m_vtAxis.append(ui->tbAxis_3);
    m_vtAxis.append(ui->tbAxis_4);
    m_vtAxis.append(ui->tbAxis_5);
    m_vtAxis.append(ui->tbAxis_6);

    m_vtValPic.clear();m_vtVal.clear();
    m_vtValPic.append(ui->tbValPic);  m_vtVal.append(ui->tbVal);
    m_vtValPic.append(ui->tbValPic_2);m_vtVal.append(ui->tbVal_2);
    m_vtValPic.append(ui->tbValPic_3);m_vtVal.append(ui->tbVal_3);
    m_vtValPic.append(ui->tbValPic_4);m_vtVal.append(ui->tbVal_4);
    m_vtValPic.append(ui->tbValPic_5);m_vtVal.append(ui->tbVal_5);
    m_vtValPic.append(ui->tbValPic_6);m_vtVal.append(ui->tbVal_6);

    int i;
    for(i=0;i<m_vtValPic.count();i++)
    {
        connect(m_vtValPic[i],SIGNAL(mouse_release()),this,SLOT(onValue()));
        connect(m_vtVal[i],SIGNAL(mouse_press()),m_vtValPic[i],SLOT(press()));
        connect(m_vtVal[i],SIGNAL(mouse_release()),m_vtValPic[i],SLOT(release()));
    }

    connect(ui->tbSpdTitlePic,SIGNAL(mouse_release()),this,SLOT(onSpd()));
    connect(ui->tbSpdTitle,SIGNAL(mouse_press()),ui->tbSpdTitlePic,SLOT(press()));
    connect(ui->tbSpdTitle,SIGNAL(mouse_release()),ui->tbSpdTitlePic,SLOT(release()));

    connect(ui->tbSpdPic,SIGNAL(mouse_release()),this,SLOT(onSpdVal()));
    connect(ui->tbSpd,SIGNAL(mouse_press()),ui->tbSpdPic,SLOT(press()));
    connect(ui->tbSpd,SIGNAL(mouse_release()),ui->tbSpdPic,SLOT(release()));

    connect(ui->tbAccuTitlePic,SIGNAL(mouse_release()),this,SLOT(onAccu()));
    connect(ui->tbAccuTitle,SIGNAL(mouse_press()),ui->tbAccuTitlePic,SLOT(press()));
    connect(ui->tbAccuTitle,SIGNAL(mouse_release()),ui->tbAccuTitlePic,SLOT(release()));

    connect(ui->tbAccuPic,SIGNAL(mouse_release()),this,SLOT(onAccuVal()));
    connect(ui->tbAccu,SIGNAL(mouse_press()),ui->tbAccuPic,SLOT(press()));
    connect(ui->tbAccu,SIGNAL(mouse_release()),ui->tbAccuPic,SLOT(release()));

#ifdef _H6_
    ui->tbAxis_6->setText(tr("J6"));
    ui->tbAxis_6->setEnabled(true);

    ui->tbVal_6->setEnabled(false);
    ui->tbValPic_6->setEnabled(false);

    ui->tbVal_4->setEnabled(false);
    ui->tbValPic_4->setEnabled(false);
    ui->tbVal_5->setEnabled(false);
    ui->tbValPic_5->setEnabled(false);
#else
    ui->tbAxis_6->setText("");
    ui->tbAxis_6->setEnabled(false);

    ui->tbVal_6->setEnabled(false);
    ui->tbValPic_6->setEnabled(false);

    ui->tbVal_4->setEnabled(true);
    ui->tbValPic_4->setEnabled(true);
    ui->tbVal_5->setEnabled(true);
    ui->tbValPic_5->setEnabled(true);
#endif

}

dialog_sasang_shift::~dialog_sasang_shift()
{
    delete ui;
}
void dialog_sasang_shift::showEvent(QShowEvent *)
{
    Update();

    timer->start();
}
void dialog_sasang_shift::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_sasang_shift::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_sasang_shift::onOK()
{
    emit accept();
}
void dialog_sasang_shift::onCancel()
{
    emit reject();
}
void dialog_sasang_shift::onTimer()
{
    ui->wigOK->setEnabled(IsModify());

    ui->wigSpdVal->setEnabled(m_bSpeed);
    ui->wigAccuVal->setEnabled(m_bAccuracy);
}

void dialog_sasang_shift::Update()
{
    m_Value.x = 0;
    m_Value.y = 0;
    m_Value.z = 0;
    m_Value.r = 0;
    m_Value.s = 0;
    m_Value.j6 = 0;

    Display_Value(m_Value);

    m_Speed = 1.0;
    m_bSpeed = false;
    m_Accuracy = 0;
    m_bAccuracy = false;

    Display_Speed(m_Speed);
    Display_Accuracy(m_Accuracy);
}

void dialog_sasang_shift::Display_Value(ST_SSSHIFT_VALUE value)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, value.x);
    m_vtVal[0]->setText(str);
    str.sprintf(FORMAT_POS, value.y);
    m_vtVal[1]->setText(str);
    str.sprintf(FORMAT_POS, value.z);
    m_vtVal[2]->setText(str);
    str.sprintf(FORMAT_POS, value.r);
    m_vtVal[3]->setText(str);
    str.sprintf(FORMAT_POS, value.s);
    m_vtVal[4]->setText(str);
    str.sprintf(FORMAT_POS, value.j6);
    m_vtVal[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, value.x);
    m_vtVal[0]->setText(str);
    str.sprintf(FORMAT_POS, value.y);
    m_vtVal[1]->setText(str);
    str.sprintf(FORMAT_POS, value.z);
    m_vtVal[2]->setText(str);
    str.sprintf(FORMAT_POS, value.r);
    m_vtVal[3]->setText(str);
    str.sprintf(FORMAT_POS, value.s);
    m_vtVal[4]->setText(str);
    str.clear();
    m_vtVal[5]->setText(str);
#endif

}

void dialog_sasang_shift::onValue()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtValPic.indexOf(sel);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(m_vtAxis[index]->text());
    np->m_numpad->SetNum(m_vtVal[index]->text().toDouble());
    np->m_numpad->SetMinValue(MIN_POS_VALUE);
    np->m_numpad->SetMaxValue(MAX_POS_VALUE);
    np->m_numpad->SetSosuNum(SOSU_POS);

    if(np->exec() == QDialog::Accepted)
    {
        SetValue(index, (float)np->m_numpad->GetNumDouble());
        Display_Value(m_Value);
    }
}

void dialog_sasang_shift::SetValue(int index, float value)
{
    switch(index)
    {
    case 0:
        m_Value.x = value;
        break;
    case 1:
        m_Value.y = value;
        break;
    case 2:
        m_Value.z = value;
        break;
    case 3:
        m_Value.r = value;
        break;
    case 4:
        m_Value.s = value;
        break;
    case 5:
        m_Value.j6 = value;
        break;
    }
}

void dialog_sasang_shift::Get(ST_SSSHIFT_VALUE& value)
{
#ifndef _H6_
    m_Value.j6 = 0;
#endif
    value = m_Value;
}

bool dialog_sasang_shift::IsModify()
{
    if(m_Value.x != 0)
        return true;
    if(m_Value.y != 0)
        return true;
    if(m_Value.z != 0)
        return true;
    if(m_Value.r != 0)
        return true;
    if(m_Value.s != 0)
        return true;
#ifdef _H6_
    if(m_Value.j6 != 0)
        return true;
#endif

    if(m_bSpeed)
        return true;
    if(m_bAccuracy)
        return true;

    return false;
}

bool dialog_sasang_shift::IsShift()
{
    if(m_Value.x != 0)
        return true;
    if(m_Value.y != 0)
        return true;
    if(m_Value.z != 0)
        return true;
    if(m_Value.r != 0)
        return true;
    if(m_Value.s != 0)
        return true;
#ifdef _H6_
    if(m_Value.j6 != 0)
        return true;
#endif

    return false;
}

bool dialog_sasang_shift::IsSpeed()
{
    return m_bSpeed;
}
float dialog_sasang_shift::GetSpeed()
{
    return m_Speed;
}

bool dialog_sasang_shift::IsAccuracy()
{
    return m_bAccuracy;
}
float dialog_sasang_shift::GetAccuracy()
{
    return m_Accuracy;
}

void dialog_sasang_shift::Display_Speed(float speed)
{
    QString str;
    str.sprintf(FORMAT_SPEED, speed);
    ui->tbSpd->setText(str);
}
void dialog_sasang_shift::Display_Accuracy(float accuracy)
{
    QString str;
    str.sprintf(FORMAT_POS, accuracy);
    ui->tbAccu->setText(str);
}

void dialog_sasang_shift::onSpd()
{
    m_bSpeed = !m_bSpeed;
}
void dialog_sasang_shift::onSpdVal()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbSpdTitle->text());
    np->m_numpad->SetNum(ui->tbSpd->text().toDouble());
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);

    if(np->exec() == QDialog::Accepted)
    {
        m_Speed = (float)np->m_numpad->GetNumDouble();
        Display_Speed(m_Speed);
    }
}
void dialog_sasang_shift::onAccu()
{
    m_bAccuracy = !m_bAccuracy;
}
void dialog_sasang_shift::onAccuVal()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbAccuTitle->text());
    np->m_numpad->SetNum(ui->tbAccu->text().toDouble());
    np->m_numpad->SetMinValue(MIN_POS_VALUE);
    np->m_numpad->SetMaxValue(MAX_POS_VALUE);
    np->m_numpad->SetSosuNum(SOSU_POS);

    if(np->exec() == QDialog::Accepted)
    {
        m_Accuracy = (float)np->m_numpad->GetNumDouble();
        Display_Accuracy(m_Accuracy);
    }
}
