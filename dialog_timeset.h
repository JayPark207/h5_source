#ifndef DIALOG_TIMESET_H
#define DIALOG_TIMESET_H

#include <QDialog>
#include <QTimer>
#include <QDateTime>
#include "global.h"

#include "qlabel4.h"
#include "qlabel3.h"

namespace Ui {
class dialog_timeset;
}

class dialog_timeset : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_timeset(QWidget *parent = 0);
    ~dialog_timeset();

    void Update();
    void Display();
    void Save();

public slots:
    void onOK();
    void onTimer();

    void onUp();
    void onDown();

private:
    Ui::dialog_timeset *ui;

    QTimer* timer;
    QString m_strDateTime;
    QDateTime m_DateTime;

    QVector<QLabel4*> m_vtUp;
    QVector<QLabel4*> m_vtDn;
    QVector<QLabel3*> m_vtUpIcon;
    QVector<QLabel3*> m_vtDnIcon;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_TIMESET_H
