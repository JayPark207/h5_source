#ifndef HYTPONLY_H
#define HYTPONLY_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QDir>

class HyTPonly : public QObject
{
    Q_OBJECT

public:
    HyTPonly();

    // related file handling.
    bool IsDirExist(QString dir_path);
    bool IsFileExist(QString file_path);
    bool SaveTxtFile(QString file_path, QStringList file_data);

private:

};

#endif // HYTPONLY_H
