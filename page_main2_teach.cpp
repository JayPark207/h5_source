#include "page_main2_teach.h"
#include "ui_page_main2_teach.h"


page_main2_teach::page_main2_teach(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_main2_teach)
{
    ui->setupUi(this);

    connect(ui->lbModePic,SIGNAL(mouse_release()),this,SLOT(onMode()));
    connect(ui->lbMode,SIGNAL(mouse_press()),ui->lbModePic,SLOT(press()));
    connect(ui->lbMode,SIGNAL(mouse_release()),ui->lbModePic,SLOT(release()));
    connect(ui->lbModeIcon,SIGNAL(mouse_press()),ui->lbModePic,SLOT(press()));
    connect(ui->lbModeIcon,SIGNAL(mouse_release()),ui->lbModePic,SLOT(release()));

    connect(ui->lbStepEditPic,SIGNAL(mouse_release()),this,SLOT(onStepEdit()));
    connect(ui->lbStepEdit,SIGNAL(mouse_press()),ui->lbStepEditPic,SLOT(press()));
    connect(ui->lbStepEdit,SIGNAL(mouse_release()),ui->lbStepEditPic,SLOT(release()));
    connect(ui->lbStepEditIcon,SIGNAL(mouse_press()),ui->lbStepEditPic,SLOT(press()));
    connect(ui->lbStepEditIcon,SIGNAL(mouse_release()),ui->lbStepEditPic,SLOT(release()));

    connect(ui->lbPosPic,SIGNAL(mouse_release()),this,SLOT(onPos()));
    connect(ui->lbPos,SIGNAL(mouse_press()),ui->lbPosPic,SLOT(press()));
    connect(ui->lbPos,SIGNAL(mouse_release()),ui->lbPosPic,SLOT(release()));
    connect(ui->lbPosIcon,SIGNAL(mouse_press()),ui->lbPosPic,SLOT(press()));
    connect(ui->lbPosIcon,SIGNAL(mouse_release()),ui->lbPosPic,SLOT(release()));

    connect(ui->lbTimerPic,SIGNAL(mouse_release()),this,SLOT(onTimer()));
    connect(ui->lbTimer,SIGNAL(mouse_press()),ui->lbTimerPic,SLOT(press()));
    connect(ui->lbTimer,SIGNAL(mouse_release()),ui->lbTimerPic,SLOT(release()));
    connect(ui->lbTimerIcon,SIGNAL(mouse_press()),ui->lbTimerPic,SLOT(press()));
    connect(ui->lbTimerIcon,SIGNAL(mouse_release()),ui->lbTimerPic,SLOT(release()));

    connect(ui->lbComPosPic,SIGNAL(mouse_release()),this,SLOT(onComPos()));
    connect(ui->lbComPos,SIGNAL(mouse_press()),ui->lbComPosPic,SLOT(press()));
    connect(ui->lbComPos,SIGNAL(mouse_release()),ui->lbComPosPic,SLOT(release()));
    connect(ui->lbComPosIcon,SIGNAL(mouse_press()),ui->lbComPosPic,SLOT(press()));
    connect(ui->lbComPosIcon,SIGNAL(mouse_release()),ui->lbComPosPic,SLOT(release()));

    connect(ui->lbEasySetPic,SIGNAL(mouse_release()),this,SLOT(onEasySet()));
    connect(ui->lbEasySet,SIGNAL(mouse_press()),ui->lbEasySetPic,SLOT(press()));
    connect(ui->lbEasySet,SIGNAL(mouse_release()),ui->lbEasySetPic,SLOT(release()));
    connect(ui->lbEasySetIcon,SIGNAL(mouse_press()),ui->lbEasySetPic,SLOT(press()));
    connect(ui->lbEasySetIcon,SIGNAL(mouse_release()),ui->lbEasySetPic,SLOT(release()));

    // dialog pointer clear
    dig_common_position = 0;
    dig_position = 0;
    dig_time = 0;
    dig_stepedit = 0;
    dig_mode_entry = 0;
    dig_easy_setting = 0;
    dig_mode_detail = 0;
}

page_main2_teach::~page_main2_teach()
{
    delete ui;
}
void page_main2_teach::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void page_main2_teach::showEvent(QShowEvent *)
{
    UserLevel();
}

void page_main2_teach::hideEvent(QHideEvent *){}

void page_main2_teach::UserLevel()
{
    int user_level = Param->Get(HyParam::USER_LEVEL).toInt();
    if(user_level < USER_LEVEL_HIGH)
    {
        // level 0 or 3

        if(user_level < USER_LEVEL_MID)
        {
            // level 0
            ui->lbModePic->setEnabled(false);
            ui->lbMode->setEnabled(false);
            ui->lbModeIcon->setEnabled(false);

            ui->lbStepEditPic->setEnabled(false);
            ui->lbStepEdit->setEnabled(false);
            ui->lbStepEditIcon->setEnabled(false);

            ui->lbEasySetPic->setEnabled(false);
            ui->lbEasySet->setEnabled(false);
            ui->lbEasySetIcon->setEnabled(false);

            ui->lbPosPic->setEnabled(false);
            ui->lbPos->setEnabled(false);
            ui->lbPosIcon->setEnabled(false);

            ui->lbTimerPic->setEnabled(false);
            ui->lbTimer->setEnabled(false);
            ui->lbTimerIcon->setEnabled(false);

            ui->lbComPosPic->setEnabled(false);
            ui->lbComPos->setEnabled(false);
            ui->lbComPosIcon->setEnabled(false);
        }
        else
        {
            // level 3
            ui->lbModePic->setEnabled(true);
            ui->lbMode->setEnabled(true);
            ui->lbModeIcon->setEnabled(true);

            ui->lbStepEditPic->setEnabled(true);
            ui->lbStepEdit->setEnabled(true);
            ui->lbStepEditIcon->setEnabled(true);

            ui->lbEasySetPic->setEnabled(true);
            ui->lbEasySet->setEnabled(true);
            ui->lbEasySetIcon->setEnabled(true);

            ui->lbPosPic->setEnabled(true);
            ui->lbPos->setEnabled(true);
            ui->lbPosIcon->setEnabled(true);

            ui->lbTimerPic->setEnabled(true);
            ui->lbTimer->setEnabled(true);
            ui->lbTimerIcon->setEnabled(true);

            ui->lbComPosPic->setEnabled(true);
            ui->lbComPos->setEnabled(true);
            ui->lbComPosIcon->setEnabled(true);


        }
    }
    else
    {
        // level 7
        ui->lbModePic->setEnabled(true);
        ui->lbMode->setEnabled(true);
        ui->lbModeIcon->setEnabled(true);

        ui->lbStepEditPic->setEnabled(true);
        ui->lbStepEdit->setEnabled(true);
        ui->lbStepEditIcon->setEnabled(true);

        ui->lbEasySetPic->setEnabled(true);
        ui->lbEasySet->setEnabled(true);
        ui->lbEasySetIcon->setEnabled(true);

        ui->lbPosPic->setEnabled(true);
        ui->lbPos->setEnabled(true);
        ui->lbPosIcon->setEnabled(true);

        ui->lbTimerPic->setEnabled(true);
        ui->lbTimer->setEnabled(true);
        ui->lbTimerIcon->setEnabled(true);

        ui->lbComPosPic->setEnabled(true);
        ui->lbComPos->setEnabled(true);
        ui->lbComPosIcon->setEnabled(true);
    }
}


void page_main2_teach::onMode()
{
    /*
    // use easy mode
    if(dig_mode_entry == 0)
        dig_mode_entry = new dialog_mode_entry();

    dig_mode_entry->exec();*/

    // not use easy mode
    if(dig_mode_detail == 0)
        dig_mode_detail = new dialog_mode_detail();

    dig_mode_detail->Set_Readonly(false);
    dig_mode_detail->exec();
}

void page_main2_teach::onStepEdit()
{
    if(dig_stepedit == 0)
        dig_stepedit = new dialog_stepedit();

    dig_stepedit->exec();
}

void page_main2_teach::onPos()
{
    if(dig_position == 0)
        dig_position = new dialog_position();

    dig_position->exec();
}

void page_main2_teach::onTimer()
{
    if(dig_time == 0)
        dig_time = new dialog_time();

    dig_time->exec();
}

void page_main2_teach::onComPos()
{   
    if(dig_common_position == 0)
        dig_common_position = new dialog_common_position();

    dig_common_position->Init();
    dig_common_position->exec();
}

void page_main2_teach::onEasySet()
{
    if(dig_easy_setting == 0)
        dig_easy_setting = new dialog_easy_setting();

    dig_easy_setting->exec();
}
