#ifndef DIALOG_SAMPLING_H
#define DIALOG_SAMPLING_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_mode_select.h"
#include "dialog_confirm.h"


namespace Ui {
class dialog_sampling;
}

class dialog_sampling : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_sampling(QWidget *parent = 0);
    ~dialog_sampling();

    void Update();
    bool IsUse();

private:
    Ui::dialog_sampling *ui;
    QTimer* timer;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;
    QStringList m_ItemName;

public slots:
    void onClose();

    void onTimer();

    void onUse();
    void onSQty();
    void onSCyc();
    void onSCnt();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SAMPLING_H
