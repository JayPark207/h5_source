#ifndef HYSASANG_H
#define HYSASANG_H

#include <QObject>

#include "hysasang_motion.h"
#include "hysasang_pattern.h"
#include "hysasang_group.h"

class HySasang : public QObject
{
    Q_OBJECT

/** variables **/
public:
    HySasang_Motion*    Motion;
    HySasang_Pattern*   Pattern;
    HySasang_Group*     Group;

private:


/** functions **/
public:
    HySasang();
    void Init_String();

    // include below...
    // transfer functions (motion <-> pattern <-> group)
    // global & common functions

    bool Pattern2Motions(ST_SSPATTERN_DATA pattern, QList<ST_SSMOTION_DATA>& motions);
    bool Motions2Pattern(QList<ST_SSMOTION_DATA> motions, ST_SSPATTERN_DATA& pattern);

    bool Group2Patterns(ST_SSGROUP_DATA group, QList<ST_SSPATTERN_DATA>& patterns);
    bool Patterns2Group(QList<ST_SSPATTERN_DATA> patterns, ST_SSGROUP_DATA& group);


private:

};

#endif // HYSASANG_H
