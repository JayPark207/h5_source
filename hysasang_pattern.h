#ifndef HYSASANG_PATTERN_H
#define HYSASANG_PATTERN_H

#include <QObject>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hysasang_motion.h"

struct ST_SSPATTERN_DATA
{
    // info
    QString name;
    QString nickname;

    // motion list in raw level
    QStringList motions;
    //QStringList program;
    QList<QStringList> program;
};

class HySasang_Pattern : public QObject
{
    Q_OBJECT

/** variables **/
public:
    QList<ST_SSPATTERN_DATA> PatternList;
    QList<ST_SSPATTERN_DATA> PatternList_Comp;

private:
    HySasang_Motion* m_MotionFunc;

/** functions **/
public:
    explicit HySasang_Pattern();

    QString GetName_TestSubMacro() { return("sspat_"); }
    QString GetName_TestMainMacro() { return ("pattern_main"); }

    // edit
    bool Default(ST_SSPATTERN_DATA& data);
    bool Add(ST_SSPATTERN_DATA& data, int index = -1); // -1 rare postion.
    bool Copy(int source_index, int target_index = -1); // target_index = -1 is rare postion.
    bool Del(int index);
    bool Rename(int index, QString name); // not sure
    bool Up(int index);
    bool Down(int index);

    // access data
    int  GetCount();
    bool GetName(int index, QString& name);
    bool SetName(int index, QString& name);
    bool GetNickname(int index, QString& nickname); // not sure
    bool SetNickname(int index, QString& nickname); // not sure
    int  GetMotionCount(int index);

    // test run
    bool MakeTestProgram();  // main & sub macro
    int MakeTestProgram(QList<ST_SSPATTERN_DATA>& comp_pattern_list);

    // utility functions.
    bool Data2Prog(ST_SSPATTERN_DATA& data);
    bool Prog2Macro(QList<QStringList> prog, int list_index);
    QStringList SumProgram(QList<QStringList> prog);
    bool MakeTestMain();
    bool DataAllChange(ST_SSPATTERN_DATA& data, ST_SSSHIFT_VALUE shift, float speed = -1.0, float accuracy = -1.0);


    bool IsAbleName(QString name);

    bool Data2GroupFormat(ST_SSPATTERN_DATA data, QStringList& format);
    bool GroupFormat2Data(QStringList format, ST_SSPATTERN_DATA& data);

    // compare
    bool Update_CompData();
    bool Reset_CompData();
    bool IsEdit();
    bool IsSame(ST_SSPATTERN_DATA& data1, ST_SSPATTERN_DATA& data2);
    bool IsSameData(ST_SSPATTERN_DATA& data1, ST_SSPATTERN_DATA& data2);
    bool IsSameProg(ST_SSPATTERN_DATA& data1, ST_SSPATTERN_DATA& data2);
    bool IsSameName(ST_SSPATTERN_DATA& data1, ST_SSPATTERN_DATA& data2);

private:



};

#endif // HYSASANG_PATTERN_H
