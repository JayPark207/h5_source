#ifndef HYSOFTSENSOR_H
#define HYSOFTSENSOR_H

#include <QObject>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyrecipe.h"
#include "hyparam.h"

/** 엔코더를 활용한 소프트위치센서 관련 클래스.
    데이터: HyParam 을 데이터베이스로하고, Recipe 에 동기화한다.
    구조는 Param -> comma 구분 String(->StringList), Recipe -> SoftSenMin[N], SoftSenMax[N] N=0~N(Sensor ID)
    동기화: 프로그램 첫 시작시 써주고, 레시피교체시 써준다.(읽어보고다르면..)
    사용:sub Task에서 해당데이터 참고하여 Trans 데이터가 해당범위에 조건에의해 특정데이터값 온오프함.
    약속된 내용만을 사용할수있음. 이유는 sub Task 프로그램과 연동되어야하기 때문에이고 이 클래스
    는 오직 범위데이터 만을 전달하는 목적으로 사용된다.
**/

class HySoftSensor : public QObject
{
    Q_OBJECT
public:
    enum ENUM_SOFTSENSOR
    {
        SSEN_TRAV_TAKEOUT_ZONE=0,
        SSEN_UPDN_TAKEOUT_ZONE,

        SSEN_NUM,
    };

public:
    explicit HySoftSensor(HyParam* param, HyRecipe* recipe);

    bool Read(); //read param -> make data.
    bool Write(bool save = true); // write Param & Recipe.
    bool Sync(bool save = true); // Read() & Write();


    // data buffer.
    QStringList m_Min;
    QStringList m_Max;

private:
    HyParam* m_Param;
    HyRecipe* m_Recipe;

};

#endif // HYSOFTSENSOR_H
