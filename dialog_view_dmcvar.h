#ifndef DIALOG_VIEW_DMCVAR_H
#define DIALOG_VIEW_DMCVAR_H

#include <QDialog>
#include <QTableWidget>

#include "global.h"

#include "dialog_confirm.h"

namespace Ui {
class dialog_view_dmcvar;
}

class dialog_view_dmcvar : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_view_dmcvar(QWidget *parent = 0);
    ~dialog_view_dmcvar();

public slots:
    void onClose();
    void onDelete();
    void onZero();

    void onSave();

private:
    Ui::dialog_view_dmcvar *ui;

    QStringList m_TransHeader;
    QStringList m_JointHeader;
    QStringList m_NumHeader;
    QStringList m_StrHeader;

    int m_nNameWidth;
    int m_nColWidth;

    void Update_Trans();
    void Update_Joint();
    void Update_Num();
    void Update_Str();

    QVector<QTableWidget*> m_twTable;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_VIEW_DMCVAR_H
