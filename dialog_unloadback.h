#ifndef DIALOG_UNLOADBACK_H
#define DIALOG_UNLOADBACK_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_mode_select.h"
#include "dialog_numpad.h"

namespace Ui {
class dialog_unloadback;
}

class dialog_unloadback : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_unloadback(QWidget *parent = 0);
    ~dialog_unloadback();

    void Update();

private:
    Ui::dialog_unloadback *ui;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;
    QStringList m_ItemName;

    bool bSave;

public slots:
    void onClose();

    void onUse();
    void onX();
    void onY();
    void onZ();
    void onSpeed();
    void onDelay();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_UNLOADBACK_H
