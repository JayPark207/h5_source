#include "dialog_takeout_timing.h"
#include "ui_dialog_takeout_timing.h"

dialog_takeout_timing::dialog_takeout_timing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_takeout_timing)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnUsePic,SIGNAL(mouse_release()),this,SLOT(onUse()));
    connect(ui->btnUse,SIGNAL(mouse_press()),ui->btnUsePic,SLOT(press()));
    connect(ui->btnUse,SIGNAL(mouse_release()),ui->btnUsePic,SLOT(release()));

    m_vtPic.clear();m_vtIcon.clear();
    m_vtPic.append(ui->btnPic);   m_vtIcon.append(ui->btnIcon);
    m_vtPic.append(ui->btnPic_2); m_vtIcon.append(ui->btnIcon_2);
    m_vtPic.append(ui->btnPic_3); m_vtIcon.append(ui->btnIcon_3);
    m_vtPic.append(ui->btnPic_4); m_vtIcon.append(ui->btnIcon_4);
    m_vtPic.append(ui->btnPic_5); m_vtIcon.append(ui->btnIcon_5);
    m_vtPic.append(ui->btnPic_6); m_vtIcon.append(ui->btnIcon_6);
    m_vtPic.append(ui->btnPic_7); m_vtIcon.append(ui->btnIcon_7);
    m_vtPic.append(ui->btnPic_8); m_vtIcon.append(ui->btnIcon_8);
    m_vtPic.append(ui->btnPic_9); m_vtIcon.append(ui->btnIcon_9);
    m_vtPic.append(ui->btnPic_10);m_vtIcon.append(ui->btnIcon_10);
    m_vtPic.append(ui->btnPic_11);m_vtIcon.append(ui->btnIcon_11);
    m_vtPic.append(ui->btnPic_12);m_vtIcon.append(ui->btnIcon_12);

    m_vtName.clear();m_vtSel.clear();
    m_vtName.append(ui->btnName);   m_vtSel.append(ui->btnSel);
    m_vtName.append(ui->btnName_2); m_vtSel.append(ui->btnSel_2);
    m_vtName.append(ui->btnName_3); m_vtSel.append(ui->btnSel_3);
    m_vtName.append(ui->btnName_4); m_vtSel.append(ui->btnSel_4);
    m_vtName.append(ui->btnName_5); m_vtSel.append(ui->btnSel_5);
    m_vtName.append(ui->btnName_6); m_vtSel.append(ui->btnSel_6);
    m_vtName.append(ui->btnName_7); m_vtSel.append(ui->btnSel_7);
    m_vtName.append(ui->btnName_8); m_vtSel.append(ui->btnSel_8);
    m_vtName.append(ui->btnName_9); m_vtSel.append(ui->btnSel_9);
    m_vtName.append(ui->btnName_10);m_vtSel.append(ui->btnSel_10);
    m_vtName.append(ui->btnName_11);m_vtSel.append(ui->btnSel_11);
    m_vtName.append(ui->btnName_12);m_vtSel.append(ui->btnSel_12);

    int i;
    for(i=0;i<m_vtPic.count();i++)
    {
        connect(m_vtPic[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtIcon[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtIcon[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
        connect(m_vtName[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtName[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
        connect(m_vtSel[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtSel[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
    }

    m_vtTbNoPic.clear();              m_vtTbNo.clear();           m_vtTbName.clear();
    m_vtTbNoPic.append(ui->tbNoPic);  m_vtTbNo.append(ui->tbNo);  m_vtTbName.append(ui->tbName);
    m_vtTbNoPic.append(ui->tbNoPic_2);m_vtTbNo.append(ui->tbNo_2);m_vtTbName.append(ui->tbName_2);
    m_vtTbNoPic.append(ui->tbNoPic_3);m_vtTbNo.append(ui->tbNo_3);m_vtTbName.append(ui->tbName_3);

    for(i=0;i<m_vtTbNoPic.count();i++)
    {
        connect(m_vtTbNoPic[i],SIGNAL(mouse_release()),this,SLOT(onTiming()));
        connect(m_vtTbNo[i],SIGNAL(mouse_press()),m_vtTbNoPic[i],SLOT(press()));
        connect(m_vtTbNo[i],SIGNAL(mouse_release()),m_vtTbNoPic[i],SLOT(release()));
    }

}

dialog_takeout_timing::~dialog_takeout_timing()
{
    delete ui;
}
void dialog_takeout_timing::showEvent(QShowEvent *)
{
    Init();
    Update();
    m_bSave = false;
}
void dialog_takeout_timing::hideEvent(QHideEvent *)
{
    if(m_bSave)
        Recipe->SaveVariable();
}
void dialog_takeout_timing::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_takeout_timing::onClose()
{
    emit accept();
}

void dialog_takeout_timing::Init()
{
    m_nSelectNo = 0;
    Display_SelectNo(m_nSelectNo);
}

void dialog_takeout_timing::Update()
{
    // read mdToutTiming
    m_ItemName.clear();
    m_ItemName.append(tr("No Use"));
    m_ItemName.append(tr("Use"));

    float use = 0.0;
    if(!Recipe->Get(HyRecipe::mdToutTiming, &use))
        use = 0.0;

    if((int)use < m_ItemName.size())
        ui->btnUse->setText(m_ItemName[(int)use]);

    Display_Use((int)use);

    vars.clear();
    vars.append(HyRecipe::tiToutVac1);
    vars.append(HyRecipe::tiToutVac2);
    vars.append(HyRecipe::tiToutVac3);
    vars.append(HyRecipe::tiToutVac4);
    vars.append(HyRecipe::tiToutChuck);
    vars.append(HyRecipe::tiToutGrip);
    vars.append(HyRecipe::tiToutUser1);
    vars.append(HyRecipe::tiToutUser2);
    vars.append(HyRecipe::tiToutUser3);
    vars.append(HyRecipe::tiToutUser4);

    if(Recipe->Gets(vars, datas))
    {
        Display_TimingNo();
    }

}

void dialog_takeout_timing::Display_Use(bool use)
{
    ui->wigBtns->setEnabled(use);
    ui->wigTiming->setEnabled(use);
}

void dialog_takeout_timing::Display_TimingNo()
{
    QString str;
    for(int i=0;i<m_vtSel.count();i++)
    {
        if(i < datas.size())
        {
            // use slot
            m_vtPic[i]->setEnabled(true);
            m_vtIcon[i]->setEnabled(true);
            m_vtName[i]->setEnabled(true);
            m_vtSel[i]->setEnabled(true);

            str.sprintf("%d", (int)datas[i]);
            m_vtSel[i]->setText(str);
        }
        else
        {
            // reserved slot
            m_vtPic[i]->setEnabled(false);
            m_vtIcon[i]->setEnabled(false);
            m_vtName[i]->setEnabled(false);
            m_vtSel[i]->setEnabled(false);

            m_vtSel[i]->setText("");
        }
    }

}

void dialog_takeout_timing::onUse()
{
    dialog_mode_select* sel = (dialog_mode_select*)gGetDialog(DIG_MODE_SELECT);
    sel->InitTitle(ui->lbTitle->text());
    sel->InitRecipe(HyRecipe::mdToutTiming);
    sel->InitNum(m_ItemName.size());
    for(int i=0;i<sel->GetNum();i++)
        sel->InitString(i,m_ItemName[i]);

    if(sel->exec() == QDialog::Accepted)
        Update();
}

void dialog_takeout_timing::onSelect()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtPic.indexOf(sel);
    if(index < 0) return;

    if(m_vtSel[index]->text().toInt() == m_nSelectNo) return;

    Recipe->Set(vars[index], (float)m_nSelectNo, false);

    m_bSave = true;
    Update();
}

void dialog_takeout_timing::Display_SelectNo(int select_no)
{
    for(int i=0;i<m_vtTbNoPic.count();i++)
    {
        m_vtTbNoPic[i]->setAutoFillBackground(i == select_no);
        m_vtTbName[i]->setAutoFillBackground(i == select_no);
    }
}


void dialog_takeout_timing::onTiming()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtTbNoPic.indexOf(sel);
    if(index < 0) return;
    if(index == m_nSelectNo) return;

    m_nSelectNo = index;
    Display_SelectNo(m_nSelectNo);
}
