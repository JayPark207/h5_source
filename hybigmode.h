#ifndef HYBIGMODE_H
#define HYBIGMODE_H

#include <QObject>
#include <defines.h>
#include <QImage>
#include <QPixmap>

#include <hymode.h>

class HyBigMode : public QObject
{
    Q_OBJECT
public:
    struct ST_BMODE_INFO
    {
        QString name;
        QString summary;
    };

public:
    HyBigMode(HyMode* mode);
    HyMode* m_Mode;

    void Init();


    // database list
    QStringList m_List;  // key var.
    QVector<ST_BMODE_INFO> m_Info; // key var.
    QString m_DefaultName;
    void Init_Info();
    bool Get_Info(int index, ST_BMODE_INFO& info);

    // list-up process
    bool Read_List(QStringList& list);

    // one set data.
    QStringList m_FileData; // key var. one line = name,val (file format)
    QStringList m_Name, m_Val; // key var. must match data. name = val

    // save & delete process
    bool Read_ModeData();
    bool ModeData2FileData(QStringList& file_data);
    bool Save_FileData(int index, QStringList file_data);
    bool Delete(int index);

    // write process
    bool Read_FileData(int index, QStringList& file_data);
    bool Write_AllVar(QStringList file_data);

    // view process
    bool FileData2ViewModeData(QStringList file_data); // boss
    bool Parsing_FileData(QStringList file_data); // one line => num/name/value
    bool Read_ViewData(HyMode::MODE_GROUP grp);
    bool Gets_FileData(QStringList names, QStringList& values);
    bool Get_FileData(QString name, QString& value);

    // common.
    QString MakePath_FileData(int index);
    bool IsExistFile(QString file_path);

    QString Make_FileName(int index);

    // description picture view
    QString MakePath_Image(int index);
    bool Read_Image(int index, QPixmap& pxm);


};

#endif // HYBIGMODE_H
