#ifndef DIALOG_ERROR_H
#define DIALOG_ERROR_H

#include <QDialog>
#include <QTimer>
#include "global.h"

#include "dialog_delaying.h"
#include "dialog_error_list.h"

namespace Ui {
class dialog_error;
}

class dialog_error : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_error(QWidget *parent = 0);
    ~dialog_error();

    bool m_bNewError;

public slots:
    void onClose();
    void onTimer();
    void onReset();
    void onRecently();
    void onBzOff();

private:
    Ui::dialog_error *ui;
    QTimer* timer;

    int m_nCode;
    int m_nAxis;
    int m_nSubCode;

    QStringList m_MotorErrorCode;

    dialog_error_list* dig_error_list;

    QString m_ErrorMsg;

    bool m_bNewErrorWrite;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ERROR_H
