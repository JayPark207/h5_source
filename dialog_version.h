#ifndef DIALOG_VERSION_H
#define DIALOG_VERSION_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "qlabel3.h"

namespace Ui {
class dialog_version;
}

class dialog_version : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_version(QWidget *parent = 0);
    ~dialog_version();

    void Update();

public slots:
    void onTimer();

    void onLogoMove();

private:
    Ui::dialog_version *ui;

    QTimer* timer;
    int m_nLogoX,m_nLogoY,m_nLogoW,m_nLogoH;
    float m_fPer;
    void StartLogoMoving();

    QVector<QLabel3*> m_vtTest;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_VERSION_H
