#include "dialog_position_teaching.h"
#include "ui_dialog_position_teaching.h"

dialog_position_teaching::dialog_position_teaching(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_position_teaching)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    wmJog = new widget_jogmodule(ui->grpJog);

    // Realcount update timer.
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    // confirm button event
    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtPos.clear();m_vtPosPic.clear();
    m_vtPos.append(ui->btnXval);m_vtPosPic.append(ui->btnXvalPic);
    m_vtPos.append(ui->btnYval);m_vtPosPic.append(ui->btnYvalPic);
    m_vtPos.append(ui->btnZval);m_vtPosPic.append(ui->btnZvalPic);
    m_vtPos.append(ui->btnRval);m_vtPosPic.append(ui->btnRvalPic);
    m_vtPos.append(ui->btnSval);m_vtPosPic.append(ui->btnSvalPic);

    m_vtReal.clear();m_vtRealPic.clear();
    m_vtReal.append(ui->btnXreal);m_vtRealPic.append(ui->btnXrealPic);
    m_vtReal.append(ui->btnYreal);m_vtRealPic.append(ui->btnYrealPic);
    m_vtReal.append(ui->btnZreal);m_vtRealPic.append(ui->btnZrealPic);
    m_vtReal.append(ui->btnRreal);m_vtRealPic.append(ui->btnRrealPic);
    m_vtReal.append(ui->btnSreal);m_vtRealPic.append(ui->btnSrealPic);

    m_vtPosName.clear();
    m_vtPosName.append(ui->lbX);
    m_vtPosName.append(ui->lbY);
    m_vtPosName.append(ui->lbZ);
    m_vtPosName.append(ui->btnR);
    m_vtPosName.append(ui->btnS);

    int i;
    for(i=0;i<m_vtPos.count();i++)
    {
        connect(m_vtPosPic[i],SIGNAL(mouse_release()),this,SLOT(onPos()));
        connect(m_vtPos[i],SIGNAL(mouse_press()),m_vtPosPic[i],SLOT(press()));
        connect(m_vtPos[i],SIGNAL(mouse_release()),m_vtPosPic[i],SLOT(release()));

        connect(m_vtRealPic[i],SIGNAL(mouse_release()),this,SLOT(onReal()));
        connect(m_vtReal[i],SIGNAL(mouse_press()),m_vtRealPic[i],SLOT(press()));
        connect(m_vtReal[i],SIGNAL(mouse_release()),m_vtRealPic[i],SLOT(release()));
    }

    connect(ui->btnRotPic,SIGNAL(mouse_release()),this,SLOT(onRotation()));
    connect(ui->btnR,SIGNAL(mouse_press()),ui->btnRotPic,SLOT(press()));
    connect(ui->btnR,SIGNAL(mouse_release()),ui->btnRotPic,SLOT(release()));

    connect(ui->btnSwvPic,SIGNAL(mouse_release()),this,SLOT(onSwivel()));
    connect(ui->btnS,SIGNAL(mouse_press()),ui->btnSwvPic,SLOT(press()));
    connect(ui->btnS,SIGNAL(mouse_release()),ui->btnSwvPic,SLOT(release()));


    connect(ui->btnColPic2,SIGNAL(mouse_release()),this,SLOT(onCurr()));
    connect(ui->btnCol2,SIGNAL(mouse_press()),ui->btnColPic2,SLOT(press()));
    connect(ui->btnCol2,SIGNAL(mouse_release()),ui->btnColPic2,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnReset,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnReset,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnSpdPic,SIGNAL(mouse_release()),this,SLOT(onSpeed()));
    connect(ui->btnSpd,SIGNAL(mouse_press()),ui->btnSpdPic,SLOT(press()));
    connect(ui->btnSpd,SIGNAL(mouse_release()),ui->btnSpdPic,SLOT(release()));
    connect(ui->btnSpdIcon,SIGNAL(mouse_press()),ui->btnSpdPic,SLOT(press()));
    connect(ui->btnSpdIcon,SIGNAL(mouse_release()),ui->btnSpdPic,SLOT(release()));

    connect(ui->btnDelayPic,SIGNAL(mouse_release()),this,SLOT(onDelay()));
    connect(ui->btnDelay,SIGNAL(mouse_press()),ui->btnDelayPic,SLOT(press()));
    connect(ui->btnDelay,SIGNAL(mouse_release()),ui->btnDelayPic,SLOT(release()));
    connect(ui->btnDelayIcon,SIGNAL(mouse_press()),ui->btnDelayPic,SLOT(press()));
    connect(ui->btnDelayIcon,SIGNAL(mouse_release()),ui->btnDelayPic,SLOT(release()));

    connect(ui->btnMovePic1,SIGNAL(mouse_release()),this,SLOT(onMoveType()));
    connect(ui->btnMoveIcon1,SIGNAL(mouse_press()),ui->btnMovePic1,SLOT(press()));
    connect(ui->btnMoveIcon1,SIGNAL(mouse_release()),ui->btnMovePic1,SLOT(release()));

    connect(ui->btnMovePic2,SIGNAL(mouse_release()),this,SLOT(onMoveType()));
    connect(ui->btnMoveIcon2,SIGNAL(mouse_press()),ui->btnMovePic2,SLOT(press()));
    connect(ui->btnMoveIcon2,SIGNAL(mouse_release()),ui->btnMovePic2,SLOT(release()));

    connect(ui->btnCallPic,SIGNAL(mouse_release()),this,SLOT(onCall()));
    connect(ui->btnCall,SIGNAL(mouse_press()),ui->btnCallPic,SLOT(press()));
    connect(ui->btnCall,SIGNAL(mouse_release()),ui->btnCallPic,SLOT(release()));
    connect(ui->btnCallIcon,SIGNAL(mouse_press()),ui->btnCallPic,SLOT(press()));
    connect(ui->btnCallIcon,SIGNAL(mouse_release()),ui->btnCallPic,SLOT(release()));


    Init_String();
    // thinking.... later change or not.
    for(i=0;i<m_vtRealPic.count();i++)
    {
        m_vtRealPic[i]->hide();
        m_vtRealPic[i]->setEnabled(false);
    }

    /*
    m_vtPosPic[0]->hide();
    m_vtPosPic[0]->setEnabled(false);
    m_vtPosPic[1]->hide();
    m_vtPosPic[1]->setEnabled(false);
    m_vtPosPic[2]->hide();
    m_vtPosPic[2]->setEnabled(false);
    m_vtPosPic[3]->hide();
    m_vtPosPic[3]->setEnabled(false);
    m_vtPosPic[4]->hide();
    m_vtPosPic[4]->setEnabled(false);
    */
    ui->btnRotPic->hide();
    ui->btnRotPic->setEnabled(false);
    ui->btnSwvPic->hide();
    ui->btnSwvPic->setEnabled(false);
    ui->grpMoveType->hide();

    // pointer clear
    top = 0;
    dig_call_position = 0;

}

dialog_position_teaching::~dialog_position_teaching()
{
    delete ui;
}
void dialog_position_teaching::showEvent(QShowEvent *)
{
    Update();

    timer->start(200);

    gReadyJog(true);

    SubTop();
}
void dialog_position_teaching::hideEvent(QHideEvent *)
{
    timer->stop();

    gReadyJog(false);
}
void dialog_position_teaching::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
        Init_String();
    }
}

void dialog_position_teaching::Init_String()
{
    m_strDelayUnit = QString(tr("sec"));
    m_strSpdUnit = QString("%");
}

void dialog_position_teaching::onOK()
{
    if(!Save())
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Save Error!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    emit accept();
}
void dialog_position_teaching::onCancel()
{
    emit reject();
}

void dialog_position_teaching::Init(int pos_index)
{
    m_nPosIndex = pos_index;
    m_nType = BASE;
}
void dialog_position_teaching::Init(ENUM_TYPE type, int pos_index)
{
    m_nType = type;
    m_nPosIndex = pos_index;
}

void dialog_position_teaching::Update()
{

    QString _name;
    cn_trans _pos;

    float _speed,_delay,_type;
    int _user_pos_index;
    cn_joint _joint;

    if(m_nType == USER)
    {
        ST_USERDATA_POS _userpos;
        // for display
        _user_pos_index = HyStepData::ADD_POS0 + m_nPosIndex;
        _name = StepEdit->m_StepData.Step[_user_pos_index].DispName;

        if(!StepEdit->m_StepData.Step[_user_pos_index].NickName.isEmpty())
            _name += " ("+ StepEdit->m_StepData.Step[_user_pos_index].NickName + ")";

        if(!UserData->RD(m_nPosIndex, &_userpos))
            return;

        _pos = _userpos.Trans;
        _joint = _userpos.Joint;
        _speed = _userpos.Speed;
        _delay = _userpos.Delay;
        _type = _userpos.Type;
    }
    else if(m_nType == WORK)
    {
        ST_USERDATA_WORK _userwork;
        // for display
        _user_pos_index = HyStepData::ADD_WORK0 + m_nPosIndex;
        _name = StepEdit->m_StepData.Step[_user_pos_index].DispName;

        if(!StepEdit->m_StepData.Step[_user_pos_index].NickName.isEmpty())
            _name += " ("+ StepEdit->m_StepData.Step[_user_pos_index].NickName + ")";

        if(!UserData->RD(m_nPosIndex, &_userwork))
            return;

        _pos = _userwork.Trans;
        _joint = _userwork.Joint;
        _speed = _userwork.Speed;
        _delay = _userwork.Delay;
        _type = _userwork.Type;
    }
    else // BASE
    {
        _name = Posi->GetName(m_nPosIndex);

        if(!Posi->RD(m_nPosIndex, _pos, _joint, _speed, _delay))
            return;

        _type = 0;
    }

    // type setting visible control
    ui->grpMoveType->setVisible(m_nType != BASE);

    m_TransDataOld = _pos;
    m_TransData = _pos;

    m_JointDataOld = _joint;
    m_JointData = _joint;

    m_SpdDataOld = _speed;
    m_DelayDataOld = _delay;
    m_TypeDataOld = _type;


    QString str;

    ui->lbNowPos->setText(_name);

    Display_SetPos(m_TransData, m_JointData);

    str.sprintf(FORMAT_SPEED, _speed);
    str.append(m_strSpdUnit);
    ui->btnSpd->setText(str);

    str.sprintf(FORMAT_TIME, _delay);
    str.append(m_strDelayUnit);
    ui->btnDelay->setText(str);

    // here user pos only - move type control
    SetMoveType((ENUM_MOVE_TYPE)((int)_type));

    // table select enable control
    ui->btnSelectPic->hide();


    qDebug() << "Trans Value";
    qDebug() << "X=" <<m_TransData.p[0] << "Y=" << m_TransData.p[1] << "Z=" << m_TransData.p[2];
    qDebug() << "A=" << m_TransData.eu[0] << "B=" << m_TransData.eu[1] << "C=" << m_TransData.eu[2];

    for(int i=0;i<USE_AXIS_NUM;i++)
        qDebug("Axis[%d] = %.2f", i, m_JointData.joint[i]);
}

void dialog_position_teaching::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    float* trans = pCon->getCurTrans();
    float* joint = pCon->getCurJoint();

    Display_CurPos(trans, joint);

    for(int i=0;i<3;i++)
    {
        m_RealTransData.p[i] = trans[i];
        m_RealTransData.eu[i] = trans[3+i];
    }
    memcpy(m_RealJointData.joint,joint,sizeof(float) * USE_AXIS_NUM);

}

void dialog_position_teaching::onPos()
{

    QLabel4* sel = (QLabel4*)sender();
    int i = m_vtPosPic.indexOf(sel);
    if(i < 0) return;

    cn_trans _trans;
    cn_joint _joint;
    float value;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    np->m_numpad->SetTitle(m_vtPosName[i]->text());
    np->m_numpad->SetNum(m_vtPos[i]->text().toDouble());
    value = m_vtPos[i]->text().toFloat() - POS_VALUE_SET_RANGE;
    np->m_numpad->SetMinValue((double)value);
    value = m_vtPos[i]->text().toFloat() + POS_VALUE_SET_RANGE;
    np->m_numpad->SetMaxValue((double)value);
    np->m_numpad->SetSosuNum(SOSU_POS);

    if(np->exec() == QDialog::Accepted)
    {
        //@@@rot
        if(i<JROT)
        {
            _trans = m_TransData;
            _trans.p[i] = (float)np->m_numpad->GetNumDouble();
            if(Posi->Trans2Joint(_trans, m_JointData, _joint))
            {
                //OK, result value insert variable.
                m_TransData = _trans;
                m_JointData = _joint;
            }
            else
            {
                //Error Message
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
            }
        }
        else
        {
            _joint = m_JointData;
            if(i == JROT)
            {
                float curr_wrist_angle = gGetWristAngle(_joint);
                float curr_input_angel = (float)np->m_numpad->GetNumDouble();
                float diff_angle = curr_input_angel - curr_wrist_angle;

                _joint.joint[i] += diff_angle;
            }
            else
                _joint.joint[i] = (float)np->m_numpad->GetNumDouble();

            if(Posi->Joint2Trans(_joint, _trans))
            {
                // OK, result value inser variable
                m_JointData = _joint;
                m_TransData = _trans;
            }
            else
            {
                //Error Message
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
            }
        }

        Display_SetPos(m_TransData, m_JointData);
    }
}
void dialog_position_teaching::onReal()
{
    // no use
    QLabel4* sel = (QLabel4*)sender();
    if(sel->isHidden())
        return;

    QString btn = sender()->objectName();

    for(int i=0;i<m_vtRealPic.count();i++)
    {
        if(btn == m_vtRealPic[i]->objectName())
        {
            // only display write. no save database.
            m_vtPos[i]->setText(m_vtReal[i]->text());

            //@@@rot (no use)
            if(i<JROT)
                m_TransData.p[i] = m_vtReal[i]->text().toFloat();
            else
                m_JointData.joint[i] = m_vtReal[i]->text().toFloat();
        }
    }
}
void dialog_position_teaching::onCurr()
{
    QLabel4* sel = (QLabel4*)sender();
    if(sel->isHidden())
        return;

    for(int i=0;i<m_vtPos.count();i++)
    {
        // only display write. no save database.
        m_vtPos[i]->setText(m_vtReal[i]->text());
    }

    m_TransData = m_RealTransData;
    m_JointData = m_RealJointData;


    qDebug() << "Trans Value";
    qDebug() << "X=" <<m_TransData.p[0] << "Y=" << m_TransData.p[1] << "Z=" << m_TransData.p[2];
    qDebug() << "A=" << m_TransData.eu[0] << "B=" << m_TransData.eu[1] << "C=" << m_TransData.eu[2];

    for(int i=0;i<USE_AXIS_NUM;i++)
        qDebug("Axis[%d] = %.2f", i, m_JointData.joint[i]);

}

void dialog_position_teaching::onSpeed()
{
    QString str;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(tr("Speed[%]"));
    np->m_numpad->SetNum(ui->btnSpd->text().remove(m_strSpdUnit).toDouble());
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);

    if(np->exec() == QDialog::Accepted)
    {
        str.sprintf(FORMAT_SPEED, (float)np->m_numpad->GetNumDouble());
        str.append(m_strSpdUnit);
        ui->btnSpd->setText(str);
    }
}
void dialog_position_teaching::onDelay()
{
    QString str;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(tr("Delay[sec]"));
    np->m_numpad->SetNum(ui->btnDelay->text().remove(m_strDelayUnit).toDouble());
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(1000.0);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        str.sprintf(FORMAT_TIME, (float)np->m_numpad->GetNumDouble());
        str.append(m_strDelayUnit);
        ui->btnDelay->setText(str);
    }
}

bool dialog_position_teaching::Save()
{
    float speed;
    float delay;

    speed       = ui->btnSpd->text().remove(m_strSpdUnit).toFloat();
    delay       = ui->btnDelay->text().remove(m_strDelayUnit).toFloat();

    if(m_nType == USER)
    {
        ST_USERDATA_POS _userpos;
        UserData->SetBuf_Default(&_userpos);
        _userpos.Trans = m_TransData;
        _userpos.Joint = m_JointData;
        _userpos.Speed = speed;
        _userpos.Delay = delay;
        // add move type
        _userpos.Type = (float)m_nMoveType;

        if(!UserData->WR(m_nPosIndex, _userpos))
            return false;
    }
    else if(m_nType == WORK)
    {
        ST_USERDATA_WORK _userwork;
        UserData->SetBuf_Default(&_userwork);
        _userwork.Trans = m_TransData;
        _userwork.Joint = m_JointData;
        _userwork.Speed = speed;
        _userwork.Delay = delay;
        // add move type
        _userwork.Type = (float)m_nMoveType;

        if(!UserData->WR(m_nPosIndex, _userwork))
            return false;
    }
    else
    {
        if(!Posi->WR(m_nPosIndex, m_TransData, m_JointData, speed, delay))
            return false;

        // working at only common postion index.
        Save_CommonPos(m_TransData, m_JointData, speed, delay);
    }

    return true;
}

void dialog_position_teaching::Save_CommonPos(cn_trans trans, cn_joint joint, float speed, float delay)
{

    if(m_nPosIndex == HyPos::JIG_CHANGE)
        Param->Set(HyParam::JIG_POS,trans,joint,speed,delay);
    else if(m_nPosIndex == HyPos::SAMPLE)
        Param->Set(HyParam::SAMPLE_POS,trans,joint,speed,delay);
    else if(m_nPosIndex == HyPos::FAULTY)
        Param->Set(HyParam::FAULTY_POS,trans,joint,speed,delay);
}

void dialog_position_teaching::Reset()
{
    m_TransData = m_TransDataOld;
    m_JointData = m_JointDataOld;
    Display_SetPos(m_TransData, m_JointData);

    QString str;

    str.sprintf(FORMAT_SPEED, m_SpdDataOld);
    str.append(m_strSpdUnit);
    ui->btnSpd->setText(str);

    str.sprintf(FORMAT_TIME, m_DelayDataOld);
    str.append(m_strDelayUnit);
    ui->btnDelay->setText(str);

    // add move type;
    SetMoveType((ENUM_MOVE_TYPE)((int)m_TypeDataOld));

}

void dialog_position_teaching::onReset()
{
    Reset();
}

void dialog_position_teaching::onRotation()
{
    QLabel4* sel = (QLabel4*)sender();
    if(sel->isHidden())
        return;

    dialog_rotation* dig = new dialog_rotation();

    dig->Init(dialog_rotation::ROTATION, m_vtPos[3]->text());
    if(dig->exec() == QDialog::Accepted)
    {
        // only display write. no save database.
        m_vtPos[3]->setText(dig->GetValue());
    }
}
void dialog_position_teaching::onSwivel()
{
    QLabel4* sel = (QLabel4*)sender();
    if(sel->isHidden())
        return;

    dialog_rotation* dig = new dialog_rotation();

    dig->Init(dialog_rotation::SWIVEL, m_vtPos[4]->text());
    if(dig->exec() == QDialog::Accepted)
    {
        // only display write. no save database.
        m_vtPos[4]->setText(dig->GetValue());
    }
}

// for only USER type
void dialog_position_teaching::onMoveType()
{
    QString btn = sender()->objectName();

    if(btn == ui->btnMovePic1->objectName())
        SetMoveType(TYPE_SEQ);
    else if(btn == ui->btnMovePic2->objectName())
        SetMoveType(TYPE_ALL);
}

void dialog_position_teaching::SetMoveType(ENUM_MOVE_TYPE move_type)
{

    m_nMoveType = move_type;

    QImage img;
    img.load(":/image/image/button_rect2_3.png");
    QPixmap pxmon = QPixmap::fromImage(img);
    img.load(":/image/image/button_rect2_0.png");
    QPixmap pxmoff = QPixmap::fromImage(img);

    if(move_type == TYPE_SEQ)
    {
        ui->btnMovePic1->setPixmap(pxmon);
        ui->btnMovePic2->setPixmap(pxmoff);
    }
    else
    {
        ui->btnMovePic1->setPixmap(pxmoff);
        ui->btnMovePic2->setPixmap(pxmon);
    }
}

void dialog_position_teaching::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}

void dialog_position_teaching::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtPos[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtPos[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtPos[2]->setText(str);
    //@@@rot
    //str.sprintf("%.2f",j.joint[JROT]);
    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    m_vtPos[3]->setText(str);
    str.sprintf(FORMAT_POS ,j.joint[JSWV]);
    m_vtPos[4]->setText(str);

}
void dialog_position_teaching::Display_CurPos(float* t, float* j)
{
    QString str;

    str.sprintf(FORMAT_POS, t[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, t[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, t[UPDN]);
    m_vtReal[2]->setText(str);
    //@@@rot
    //str.sprintf("%.2f", j[JROT]);
    str.sprintf(FORMAT_POS, gGetWristAngle(j[J2], j[J3], j[J4]));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, j[JSWV]);
    m_vtReal[4]->setText(str);

}

void dialog_position_teaching::onCall()
{
    if(dig_call_position == 0)
        dig_call_position = new dialog_call_position();

    if(dig_call_position->exec() == QDialog::Accepted)
    {
        m_TransData = dig_call_position->GetTrans();
        m_JointData = dig_call_position->GetJoint();
        Display_SetPos(m_TransData, m_JointData);
    }
}

/*
bool dialog_position_teaching::Trans2Joint(cn_trans in_trans, cn_joint old_joint, cn_joint& new_joint)
{
    // display before important value.
    qDebug() << "before Trans Value";
    qDebug() << "X=" <<m_TransData.p[0] << "Y=" << m_TransData.p[1] << "Z=" << m_TransData.p[2];
    qDebug() << "A=" << m_TransData.eu[0] << "B=" << m_TransData.eu[1] << "C=" << m_TransData.eu[2];

    qDebug() << "Input Trans Value";
    qDebug() << "X=" <<in_trans.p[0] << "Y=" << in_trans.p[1] << "Z=" << in_trans.p[2];
    qDebug() << "A=" << in_trans.eu[0] << "B=" << in_trans.eu[1] << "C=" << in_trans.eu[2];

    qDebug() << "before Joint Value";
    qDebug() << old_joint.joint[0] << old_joint.joint[1] << old_joint.joint[2]
             << old_joint.joint[3] << old_joint.joint[4];


    CNRobo* pCon = CNRobo::getInstance();
    cn_conf confIn;
    confIn.cfx = 0x00;
    memset(confIn.cfang,0,sizeof(unsigned char)*3);
    memset(confIn.cfmaster,0,sizeof(unsigned char)*4);

    int ret = pCon->calcInverse(in_trans, old_joint.joint, new_joint.joint, confIn);
    if(ret < 0)
    {
        qDebug() << "Trans2Joint >> calcInverse() ret = " << ret;
        return false;
    }

    qDebug() << "Input Trans Value";
    qDebug() << "X=" <<in_trans.p[0] << "Y=" << in_trans.p[1] << "Z=" << in_trans.p[2];
    qDebug() << "A=" << in_trans.eu[0] << "B=" << in_trans.eu[1] << "C=" << in_trans.eu[2];

    qDebug() << "calc Joint Value";
    qDebug() << new_joint.joint[0] << new_joint.joint[1] << new_joint.joint[2]
             << new_joint.joint[3] << new_joint.joint[4];

    return true;
}

bool dialog_position_teaching::Joint2Trans(cn_joint in_joint, cn_trans& new_trans)
{
    qDebug() << "Joint2Trans() Log";
    qDebug() << "before joint value";
    qDebug() << m_JointData.joint[0] << m_JointData.joint[1] << m_JointData.joint[2]
             << m_JointData.joint[3] << m_JointData.joint[4];

    qDebug() << "Input joint value";
    qDebug() << in_joint.joint[0] << in_joint.joint[1] << in_joint.joint[2]
             << in_joint.joint[3] << in_joint.joint[4];

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->calcForward(in_joint.joint, new_trans);
    if(ret < 0)
    {
        qDebug() << "Joint2Trans() >> calcForward() ret = " << ret;
        return false;
    }

    qDebug() << "before Trans Value";
    qDebug() << "X=" << m_TransData.p[0] << "Y=" << m_TransData.p[1] << "Z=" << m_TransData.p[2];
    qDebug() << "A=" << m_TransData.eu[0] << "B=" << m_TransData.eu[1] << "C=" << m_TransData.eu[2];

    qDebug() << "calc Trans Value";
    qDebug() << "X=" << new_trans.p[0] << "Y=" << new_trans.p[1] << "Z=" << new_trans.p[2];
    qDebug() << "A=" << new_trans.eu[0] << "B=" << new_trans.eu[1] << "C=" << new_trans.eu[2];

    return true;
}
*/
