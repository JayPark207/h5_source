#ifndef HYPENDENT_H
#define HYPENDENT_H

#include <QApplication>
#include <QObject>
#include <QDebug>
#include <QWidget>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

class HyPendent : public QObject
{
    Q_OBJECT

public:
    HyPendent();

    bool Led_L1(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);
    bool Led_L2(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);
    bool Led_L3(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);

    bool Led_R1(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);
    bool Led_R2(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);
    bool Led_R3(CNR_LED_COLOR color = CNR_LED_COLOR_OFF);

    bool Beep();

    bool Alert(bool onoff);

private:
};

#endif // HYPENDENT_H
