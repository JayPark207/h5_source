#include "hystatus.h"

HyStatus::HyStatus()
{
    nRobotStatus = RBS_RDY;
    bConnect = false;
    bServo = false;
    bTeach = false;
    bTPEnable = false;
    bLogin =false;
    bLock = false;
    bError = false;
    bJog = false;
    bLed1 = false;
    bLed2 = false;
    bLed3 = false;
    bEmo = false;
    bFlicker = false;

    m_nErrCode = 0;
}

void HyStatus::Update()
{
    CNRobo* pCon = CNRobo::getInstance();

    // raw status.
    bConnect = pCon->isConnected();
    bServo = pCon->getServoOn();
    bTeach = pCon->isTeach();
    bTPEnable = pCon->isTPEnable();
    bSafetyZone = pCon->getWZCheckFlag();
    //pCon->getLoginStatus(&bLogin);    // about spend 25msec
    bLogin = true;
    //pCon->getLockStatus(&bLock);      // about spend 25msec
    bLock = true;
    bWarn = pCon->isWarning();
    bErr = pCon->isError();
    bError = bErr || bWarn;
    MainTask = pCon->getTaskStatus(MAIN_TASK);
    SubTask = pCon->getTaskStatus(SUB_TASK);

    // combination status.
    bJog = (bTeach && bTPEnable);
    bLed1 = bConnect && bLogin && bLock;
    bLed2 = (SubTask == CNR_TASK_RUNNING);
    //bLed3 = false;

    if(bError)
        nRobotStatus = RBS_ERROR;
    else if(!bServo)
        nRobotStatus = RBS_NOT_RDY;
    else
        nRobotStatus = RBS_RDY;


}

void HyStatus::Update_Time()
{
    CNRobo* pCon = CNRobo::getInstance();

    // time.
    QString str;
    pCon->getDateTime(str);
    qDateTime = QDateTime::fromString(str, Qt::ISODate);
    strDate = qDateTime.date().toString(Qt::ISODate);
    strTime = qDateTime.time().toString(Qt::ISODate);
}


void HyStatus::SetErrCode(int code)
{
    m_nErrCode = code;
}

int HyStatus::GetErrCode()
{
    return m_nErrCode;
}
