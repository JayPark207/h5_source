#ifndef DIALOG_SASANG_MOTION_TEACH_H
#define DIALOG_SASANG_MOTION_TEACH_H

#include <QWidget>
#include <QStackedWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_message.h"
#include "dialog_confirm.h"
#include "dialog_numpad.h"

#include "widget_jogmodule4.h"
#include "dialog_sasang_output.h"

namespace Ui {
class dialog_sasang_motion_teach;
}

class dialog_sasang_motion_teach : public QWidget
{
    Q_OBJECT

public:
    explicit dialog_sasang_motion_teach(QWidget *parent = 0);
    ~dialog_sasang_motion_teach();

    void Update();

    bool m_bEditing;

private:
    Ui::dialog_sasang_motion_teach *ui;

    QTimer* timer;

    widget_jogmodule4* wig_jog;

    QStackedWidget* Panel;

    QVector<QLabel3*> m_vtTbNo;
    QVector<QLabel3*> m_vtTbMot;

    int m_nSelectedIndex;
    int m_nStartIndex;

    void Redraw_List(int start_index);      // boss
    void Display_ListOn(int table_index);   // boss

    QVector<QLabel3*> m_vtAxis;
    QVector<QLabel3*> m_vtSet;
    QVector<QLabel3*> m_vtReal;

    ST_SSMOTION_DATA m_DataOld;
    ST_SSMOTION_DATA m_DataNew;

    void Display_Data(int list_index);      // boss
    void Display_Data_Write(ST_SSMOTION_DATA data); // semi-boss

    void Display_SetPos(cn_trans t, cn_joint j);
    void Display_RealPos(float* trans, float* joint);
    void Display_Speed(float speed);
    void Display_Accuracy(float accuracy);
    void Display_Output(float output);
    void Display_Type(float type);
    void Display_Time(float time);
    void Display_SetTitle(QString command);

    void Display_AddSpeed(float speed);
    void Display_AddAccuracy(float accuracy);

    QVector<QPixmap> pxmType;
    QVector<QPixmap> pxmOutput;

    cn_trans m_CurrTrans;
    cn_joint m_CurrJoint;

    bool m_bSetTitle;

    float m_AddSpeed;
    float m_AddAccuracy;

    bool IsSame(ST_SSMOTION_DATA old_data, ST_SSMOTION_DATA new_data);
    void Display_SaveEnable();

    bool Save();

    dialog_sasang_output* dig_sasang_output;

    QVector<QLabel4*> m_vtAddPic;
    QVector<QLabel3*> m_vtAdd;

    void Enable_CirEnd(bool bEnd);
    bool m_bAddCirEnd;
    ST_SSMOTION_DATA m_CirData;

    void Display_Editing(bool bEditing);

public slots:
    void onClose();

    void onTimer();

    void onList();
    void onUp();
    void onDown();

    void onSetTitle();

    void onAddSpeed();
    void onAddAccuracy();

    void onSpeed();
    void onAccuracy();
    void onCurrent();
    void onOutput();

    void onSave();

    void onAdd();
    void onDel();

signals:
    void sigClose();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SASANG_MOTION_TEACH_H
