#ifndef DIALOG_ARRAY_INFO_H
#define DIALOG_ARRAY_INFO_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "dialog_confirm.h"

#define MAX_ARR_X_VIEW 10
#define MAX_ARR_Y_VIEW 10
#define MAX_ARR_Z_VIEW 10

namespace Ui {
class dialog_array_info;
}

class dialog_array_info : public QDialog
{
    Q_OBJECT
private:
    enum MATRIX_COORD
    {
        X=0,
        Y,
        Z,
        MATRIX_COORD_NUM,
    };

public:
    explicit dialog_array_info(QWidget *parent = 0);
    ~dialog_array_info();

    void Init(bool bUnload = true, bool bInsert = true);

private slots:
    void onChange();
    void onTimer_Unload();
    void onTimer_Insert();

    void onSelectXY();
    void onSelectZ();

    void onUpdate();
    void onClear();
    void onSet();

    void onZp();
    void onZn();
    void onXp();
    void onXn();
    void onYp();
    void onYn();

private:
    // make matrix.
    void Update_Unload();
    void Update_Insert();

    // data update (change color & floor view)
    void Refresh_Unload();
    void Refresh_Insert();

private:
    Ui::dialog_array_info *ui;
    QTimer* timer_Unload;
    QTimer* timer_Insert;

    bool m_bUnload, m_bInsert;

    QVector< QVector<QLabel3*> > m_vtXY;
    QVector<QLabel3*> m_vtZ;

    void MakeMatrixXYZ(int xnum, int ynum, int znum);
    int m_nNum[MATRIX_COORD_NUM];
    int m_nCurr[MATRIX_COORD_NUM];
    int m_nOrder[MATRIX_COORD_NUM];
    int m_nStartIndex[MATRIX_COORD_NUM];
    int m_nSelectZ;

    void MakeMatrixDataXYZ(int xord, int yord ,int zord);
    int m_nXYZ[MAX_ARR_X_NUM][MAX_ARR_Y_NUM][MAX_ARR_Z_NUM]; // in order number. 1~1000 0=no use.

    void Display_Matrix(int xcur, int ycur, int zcur, int start_x=-1, int start_y=-1, int start_z=-1); // new
    void Display_Matrix(int xcur, int ycur, int zcur, int start_x, int start_y, int start_z, int select_z);
    void View_Matrix(int select_z);

    void ChangeCellWorked(int arr_x, int arr_y);
    void ChangeCellWorking(int arr_x, int arr_y);
    void ChangeCellReady(int arr_x, int arr_y);
    void ChangeFloorWorking(int arr_z);

    void Display_OrderNum(int xord, int yord ,int zord);
    void Display_CurrTable(int xcur, int ycur, int zcur);

    void Display_UpdateLed(bool onoff);
    QVector<QPixmap> pxmUpdateLed;

    void Display_SelectXY(int xsel, int ysel, int zsel);
    QPalette m_palBefore;
    int m_nSel[MATRIX_COORD_NUM];

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ARRAY_INFO_H
