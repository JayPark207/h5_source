#ifndef DIALOG_POSITION_AUTO_H
#define DIALOG_POSITION_AUTO_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_message.h"
#include "dialog_confirm.h"

namespace Ui {
class dialog_position_auto;
}

class dialog_position_auto : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_position_auto(QWidget *parent = 0);
    ~dialog_position_auto();

    void Update_List();
    void Redraw_List(int start_index);
    void Display_Data(int list_index);


public slots:
    void onClose();
    void onTimer();

    void onList();
    void onListUp();
    void onListDown();

    void onValue();
    void onSpeed();
    void onDelay();

    void onReset();
    void onSave();

private:
    Ui::dialog_position_auto *ui;

    QTimer* timer;

    QVector<int> m_UsePos; // important var.

    QVector<QLabel3*> m_vtList;

    QVector<QLabel4*> m_vtValPic;
    QVector<QLabel3*> m_vtVal;
    QVector<QLabel3*> m_vtAxis;

    int m_nStartIndex;
    int m_nSelectedIndex;

    void Display_ListOn(int ui_list_index);
    void Display_ListOff();

    cn_trans m_Trans, m_TransNew;
    cn_joint m_Joint, m_JointNew;
    ST_USERDATA_POS m_UserData; //this is only buffer for move type.
    ST_USERDATA_WORK m_UserWork;
    float m_Speed, m_SpeedNew;
    float m_Delay, m_DelayNew;

    void Display_SetPos(cn_trans t, cn_joint j);

    void Save(int list_index);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_POSITION_AUTO_H
