#include "page_main2.h"


page_main2::page_main2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_main2)
{
    ui->setupUi(this);

    stackedPage = new QStackedWidget(this);
    //stackedPage->setGeometry(405,20,380,280);
    stackedPage->setGeometry(ui->wigSubMenu->x(),ui->wigSubMenu->y(),ui->wigSubMenu->width(),ui->wigSubMenu->height());

    pageTeach = 0;
    pageMonitor = 0;
    pageSetting = 0;
    pageSpecial = 0;

    ChangePage(MAIN2_PAGE_NONE);
    SetHighlightOff();

    connect(ui->lbMoldPic,SIGNAL(mouse_release()),this,SLOT(onMold()));
    connect(ui->lbMoldIcon,SIGNAL(mouse_press()),ui->lbMoldPic,SLOT(press()));
    connect(ui->lbMoldIcon,SIGNAL(mouse_release()),ui->lbMoldPic,SLOT(release()));

    connect(ui->lbTeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->lbTeachIcon,SIGNAL(mouse_press()),ui->lbTeachPic,SLOT(press()));
    connect(ui->lbTeachIcon,SIGNAL(mouse_release()),ui->lbTeachPic,SLOT(release()));

    connect(ui->lbMonitorPic,SIGNAL(mouse_release()),this,SLOT(onMonitor()));
    connect(ui->lbMonitorIcon,SIGNAL(mouse_press()),ui->lbMonitorPic,SLOT(press()));
    connect(ui->lbMonitorIcon,SIGNAL(mouse_release()),ui->lbMonitorPic,SLOT(release()));

    connect(ui->lbLogPic,SIGNAL(mouse_release()),this,SLOT(onLog()));
    connect(ui->lbLogIcon,SIGNAL(mouse_press()),ui->lbLogPic,SLOT(press()));
    connect(ui->lbLogIcon,SIGNAL(mouse_release()),ui->lbLogPic,SLOT(release()));

    connect(ui->lbSettingPic,SIGNAL(mouse_release()),this,SLOT(onSetting()));
    connect(ui->lbSettingIcon,SIGNAL(mouse_press()),ui->lbSettingPic,SLOT(press()));
    connect(ui->lbSettingIcon,SIGNAL(mouse_release()),ui->lbSettingPic,SLOT(release()));

    connect(ui->lbSpecialPic,SIGNAL(mouse_release()),this,SLOT(onSpecial()));
    connect(ui->lbSpecialIcon,SIGNAL(mouse_press()),ui->lbSpecialPic,SLOT(press()));
    connect(ui->lbSpecialIcon,SIGNAL(mouse_release()),ui->lbSpecialPic,SLOT(release()));

    connect(ui->lbHomingPic,SIGNAL(mouse_release()),this,SLOT(onHoming()));
    connect(ui->lbHomingIcon,SIGNAL(mouse_press()),ui->lbHomingPic,SLOT(press()));
    connect(ui->lbHomingIcon,SIGNAL(mouse_release()),ui->lbHomingPic,SLOT(release()));

    connect(ui->lbAutoPic,SIGNAL(mouse_release()),this,SLOT(onAuto()));
    connect(ui->lbAutoIcon,SIGNAL(mouse_press()),ui->lbAutoPic,SLOT(press()));
    connect(ui->lbAutoIcon,SIGNAL(mouse_release()),ui->lbAutoPic,SLOT(release()));

    connect(ui->btnManualPic,SIGNAL(mouse_release()),this,SLOT(onManual()));
    connect(ui->btnManualIcon,SIGNAL(mouse_press()),ui->btnManualPic,SLOT(press()));
    connect(ui->btnManualIcon,SIGNAL(mouse_release()),ui->btnManualPic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnErrorPic,SIGNAL(mouse_release()),this,SLOT(onErrorView()));
    connect(ui->btnErrorIcon,SIGNAL(mouse_press()),ui->btnErrorPic,SLOT(press()));
    connect(ui->btnErrorIcon,SIGNAL(mouse_release()),ui->btnErrorPic,SLOT(release()));

    connect(ui->btnSafetyzonePic,SIGNAL(mouse_release()),this,SLOT(onSafetyzone()));
    connect(ui->btnSafetyzoneIcon,SIGNAL(mouse_press()),ui->btnSafetyzonePic,SLOT(press()));
    connect(ui->btnSafetyzoneIcon,SIGNAL(mouse_release()),ui->btnSafetyzonePic,SLOT(release()));

    connect(ui->btnNotUsePic,SIGNAL(mouse_release()),this,SLOT(onRobotNotUse()));
    connect(ui->btnNotUseIcon,SIGNAL(mouse_press()),ui->btnNotUsePic,SLOT(press()));
    connect(ui->btnNotUseIcon,SIGNAL(mouse_release()),ui->btnNotUsePic,SLOT(release()));

    // page pointer clear.
    dig_Mauual = 0;
    dig_log = 0;
    dig_recipe = 0;
    dig_robot_notuse = 0;
}

page_main2::~page_main2()
{
    delete ui;
}
void page_main2::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);

        Display_Flag(Param->Get(HyParam::LANGUAGE).toInt());
    }
}
void page_main2::showEvent(QShowEvent *)
{

}

void page_main2::hideEvent(QHideEvent *)
{

}


void page_main2::onMold()
{
    ChangePage(MAIN2_PAGE_NONE);
    SetHighlight(0,0);

    if(dig_recipe == 0)
        dig_recipe = new dialog_recipe();

    dig_recipe->exec();
}

void page_main2::onTeach()
{
    ChangePage(MAIN2_PAGE_TEACH);
    SetHighlight(1,0);
}

void page_main2::onMonitor()
{
    ChangePage(MAIN2_PAGE_MONITOR);
    SetHighlight(2,0);
}

void page_main2::onLog()
{
    ChangePage(MAIN2_PAGE_NONE);
    SetHighlight(0,1);

    if(dig_log == 0)
        dig_log = new dialog_log();

    dig_log->exec();
}

void page_main2::onSetting()
{
    ChangePage(MAIN2_PAGE_SETTING);
    SetHighlight(1,1);
}

void page_main2::onSpecial()
{
    ChangePage(MAIN2_PAGE_SPECIAL);
    SetHighlight(2,1);
}

void page_main2::onHoming()
{
    ChangePage(MAIN2_PAGE_NONE);
    SetHighlight(1,2);

    dialog_home* dig = (dialog_home*)gGetDialog(DIG_HOME);
    dig->exec();
}

void page_main2::onAuto()
{
    float data;
    if(!Recipe->Get(HyRecipe::emUse, &data))
        data = 0;

    if(data < 0.5)
    {
        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::RED);
        conf->Title(tr("Interlock Disable"));
        conf->Message(tr("Interlock is curruntly disabled."), tr("This is the test mode. Do you want it?"));

        if(conf->exec() != QDialog::Accepted)
            return;
    }

    // go auto run page.
    gSetMainPage(AUTORUN_FORM);
    ChangePage(MAIN2_PAGE_NONE);
    SetHighlight(0,2);
}


void page_main2::onManual()
{
    ChangePage(MAIN2_PAGE_NONE);
    SetHighlight(2,2);

    if(dig_Mauual == 0)
        dig_Mauual = new dialog_manual();

    dig_Mauual->exec();
}

void page_main2::SetHighlight(int x, int y)
{
    if(x > 2) x = 2;
    else if(x < 0) x = 0;

    if(y > 2) y = 2;
    else if(y < 0) y = 0;

    int posX, posY;

    posX = HIGHLIGHT_START_X + (HIGHLIGHT_X_PITCH * x);
    posY = HIGHLIGHT_START_Y + (HIGHLIGHT_Y_PITCH * y);

    if(!ui->lbHighlight->isVisible())
        ui->lbHighlight->show();

    ui->lbHighlight->setGeometry(posX,posY
        ,ui->lbHighlight->width(),ui->lbHighlight->height());
}

void page_main2::SetHighlightOff()
{
    ui->lbHighlight->hide();
}


/** additional functions. **/
void page_main2::Display_Flag(int lang_index)
{
    if(lang_index < 0) lang_index = 0;
    else if(lang_index >= Language->m_Flag.size())
        lang_index = Language->m_Flag.size()-1;

    ui->lbFlagIcon->setPixmap(Language->m_Flag[lang_index]);
}

void page_main2::onReset()
{
    //if(!Status->bError) return;

    dialog_delaying* dig = new dialog_delaying();
    CNRobo* pCon = CNRobo::getInstance();
    pCon->resetEcatError();
    dig->Init(500);
    dig->exec();
    pCon->resetError();
    pCon->clearWarningCode();
}

void page_main2::onErrorView()
{
    //if(!Status->bError) return;

    dialog_error* dig = (dialog_error*)gGetDialog(DIG_ERR);
    dig->exec();
}

void page_main2::onSafetyzone()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::DEFAULT);
    conf->Title(tr("Safety Zone"));
    if(Status->bSafetyZone)
        conf->Message(tr("Safety Zone OFF?"), tr("Be careful!"));
    else
        conf->Message(tr("Safety Zone ON?"));

    if(conf->exec() == QDialog::Accepted)
    {
        CNRobo* pCon = CNRobo::getInstance();
        pCon->setWZCheckFlag(!Status->bSafetyZone);
        pCon->WZSaveAll();
    }
}

void page_main2::ChangePage(MAIN2_PAGE_INDEX page_index)
{
    // case 1. none page
    if(page_index >= MAIN2_PAGE_NONE)
    {
        stackedPage->hide();
        return;
    }

    // case 2. exist pages
    // If it have not instance, make new one only one time.
    // Matching to enum MAIN2_PAGE_INDEX
    if(stackedPage->count() <= 0)
    {
        if(pageTeach == 0)
            pageTeach = new page_main2_teach(this);
        stackedPage->addWidget(pageTeach);

        if(pageMonitor == 0)
            pageMonitor = new page_main2_monitor(this);
        stackedPage->addWidget(pageMonitor);

        if(pageSetting == 0)
            pageSetting = new page_main2_setting(this);
        stackedPage->addWidget(pageSetting);

        if(pageSpecial == 0)
            pageSpecial = new page_main2_special(this);
        stackedPage->addWidget(pageSpecial);

        qDebug() << "To make main2 page instance is OK!";
    }

    // show sub menu & change page.
    if(stackedPage->isHidden())
        stackedPage->show();

    stackedPage->setCurrentIndex(page_index);
}


// for robot not use

void page_main2::onRobotNotUse()
{
    if(dig_robot_notuse == 0)
        dig_robot_notuse = new dialog_robot_notuse();

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(tr("Confirm"));
    conf->Message(tr("Do you want to disable this robot?")/*,
                  tr("Next step is homing.")*/);
    if(conf->exec() != QDialog::Accepted)
        return;

    /* // home & robot not use
     * if(!dig_robot_notuse->Home(true))
        return;*/


    // check softsen & robot not use
    float data;
    if(!Recipe->Get(HyRecipe::emCmdInMold, &data))
        data = 1.0;

    if(data > 0.5)
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Fail Robot Not Use")));
        msg->Message(QString(tr("The robot is in the mold!")),
                     QString(tr("Plase move to the out of mold and Try again!")));
        msg->exec();
        return;
    }

    dig_robot_notuse->exec();
}
