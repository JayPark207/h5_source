#include "hyerror.h"

HyError::HyError(HyParam* param)
{
    m_Param = param;

    m_vtCode.clear();m_Name.clear();m_Detail.clear();
    Init();

    m_vtError.clear();
    m_vtError.fill(0,MAX_ERROR_BUFFER);
}

void HyError::Init()
{
    // add here. -> using Set function.
    Set(0,tr("Normal Status"),tr("It's OK"));
    Set(-10308,tr("Servo Off"),tr("Need to Servo On"));
    Set(-9001,tr("User Error"),tr("This is error by users."));

    int i;
    QString str;
    for(i=0;i<MAX_USER_STEP_IN;i++)
    {
        str = tr("Wait Input") + " " + QString().setNum(i+1);
        Set((-1)*(9801+i),str, tr("Input wait timeout. Please, Check Input."));
    }

    Set(-1505,tr("Motor Error"),tr(""));

    Set(-9010,tr("Interlock Error"),tr("Mold Open Sensor OFF or Close Sensor ON"));
    Set(-9020,tr("Interlock Error"),tr("Mold Open & Close Sensor ON"));
    Set(-9030,tr("Safety Device"),tr("Safety Device enabled."));
    Set(-9040,tr("Low WAIT Position"),tr("The WAIT position is set lower than softsensor min. position."));
    Set(-9041,tr("Low UP Position"),tr("The UP position is set lower than softsensor min. position."));
    Set(-9042,tr("Low UP Position"),tr("The UP position is set lower than softsensor min. position."));
    Set(-9043,tr("Low UP Position"),tr("The UP position is set lower than WAIT position.(200mm)"));

    Set(-9050,tr("Emergency"),tr("Emergency Robot"));
    Set(-9060,tr("Emergency"),tr("Emergency IMM 1"));
    Set(-9061,tr("Emergency"),tr("Emergency IMM 2"));

    Set(-9070,tr("Home in Softsensor"),tr("Inside Softsensor, you can't homing."));

    Set(-9130,tr("Interlock Error"),tr("Mold is not open."));

    Set(-9210,tr("Product Takeout Error"),tr("Failed to takeout product."));
    Set(-9220,tr("Product Drop Error"),tr("Droped product on the move."));
    Set(-9230,tr("Product Unload Error"),tr("Failed to unload product."));
    Set(-9240,tr("Product Remove Error"),tr("The product remains in the wait position."));

    Set(-9310,tr("Temperature"),tr("Temperature measurement timed out."));
    Set(-9320,tr("Weight"),tr("Weight measurement timed out."));
    Set(-9330,tr("Weight Reset"),tr("Weight Reset timed out."));

    Set(-9910,tr("Target Production"),tr("Target product quantity exceeded."));
    Set(-9920,tr("Maximum Production"),tr("Reset to zero over maximum."));

    Set(-9410,tr("Ext.Machine Error"),tr("Wait time over."));
    Set(-9420,tr("Jig Foward Error"),tr("Check Jig forward sensor"));
    Set(-9430,tr("Jig Backward Error"),tr("Check Jig backward sensor"));
    Set(-9440,tr("A-Runner1 Check Error"),tr("Check A-runner1 check sensor"));
    Set(-9450,tr("A-Runner2 Check Error"),tr("Check A-runner2 check sensor"));
    Set(-9460,tr("A-Runner3 Check Error"),tr("Check A-runner3 check sensor"));
    Set(-9470,tr("B-Runner1 Check Error"),tr("Check B-runner1 check sensor"));
    Set(-9480,tr("B-Runner2 Check Error"),tr("Check B-runner2 check sensor"));
    Set(-9490,tr("B-Runner3 Check Error"),tr("Check B-runner3 check sensor"));
    Set(-9500,tr("Jig 2nd-Grip1 Check Error"),tr("Check Jig 2nd-Grip1 check sensor"));
    Set(-9510,tr("Jig 2nd-Grip2 Check Error"),tr("Check Jig 2nd-Grip2 check sensor"));
    Set(-9520,tr("A-AlignJig Up Error"),tr("Check A-AlignJig Up check sensor"));
    Set(-9530,tr("B-AlignJig Up Error"),tr("Check B-AlignJig Up check sensor"));
    Set(-9540,tr("A-AlignJig Down Error"),tr("Check A-AlignJig Up check sensor"));
    Set(-9550,tr("B-AlignJig Down Error"),tr("Check B-AlignJig Up check sensor"));

}

void HyError::Set(int code, QString name, QString detail)
{
    int index = m_vtCode.indexOf(code);

    if(index < 0)
    {
        m_vtCode.append(code);
        m_Name.append(name);
        m_Detail.append(detail);
    }
    else
    {
        m_Name[index] = name;
        m_Detail[index] = detail;
    }
}

QString HyError::GetName(int err_code)
{
    int index = m_vtCode.indexOf(err_code);

    if(index < 0)
        return "N/A";

    return m_Name.at(index);
}

QString HyError::GetDetail(int err_code)
{
    int index = m_vtCode.indexOf(err_code);

    if(index < 0)
        return "N/A";

    return m_Detail.at(index);
}

void HyError::Error(int err_code)
{
    m_vtError.push_front(err_code);
    if(m_vtError.size() > MAX_ERROR_BUFFER)
        m_vtError.pop_back();

    //Write();
}

int HyError::GetCode(int index)
{
    if(index >= m_vtError.size())
        return 0;

    return m_vtError[index];
}

void HyError::Sync()
{
    QString str = m_Param->Get(HyParam::RECENTLY_ERROR);
    QStringList str_list = str.split(",");

    if(str_list.size() != MAX_ERROR_BUFFER)
        return;

    for(int i=0;i<MAX_ERROR_BUFFER;i++)
        m_vtError[i] = str_list[i].toInt();
}
void HyError::Write()
{
    QStringList str_list;
    str_list.clear();
    for(int i=0;i<MAX_ERROR_BUFFER;i++)
        str_list.append(QString().setNum(m_vtError[i]));

    QString str = str_list.join(",");
    m_Param->Set(HyParam::RECENTLY_ERROR, str);

    qDebug() << str;
}

bool HyError::Load_ErrorCode()
{
    // check exist file.
    CNRobo* pCon = CNRobo::getInstance();
    QString path = PATH_ERROR_CODE;
    bool exist = false;
    if(pCon->checkDirFileExist(path, exist) < 0)
        return false;
    if(!exist)
        return false;

    // read file
    QStringList datas;
    int ret;
    ret = pCon->getProgramFile(path, datas);
    if(ret < 0)
    {
        qDebug() << "HyError::Load_ErrorCode() getProgramFile ret =" << ret;
        return false;
    }
    qDebug() << datas;

    if(datas.isEmpty())
        return false;

    // parsing & set error code
    QStringList line;
    QString str;
    for(int i=0;i<datas.count();i++)
    {
        str = datas.at(i);
        line = str.split(",");
        qDebug() << line;

        if(line.count() == 3)
            Set(line.at(0).toInt(), line.at(1), line.at(2));
    }

    return true;
}
