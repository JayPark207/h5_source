#ifndef TOP_SPEED_H
#define TOP_SPEED_H

#include <QWidget>
#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

namespace Ui {
class top_speed;
}

class top_speed : public QWidget
{
    Q_OBJECT
    
public:
    explicit top_speed(QWidget *parent = 0);
    ~top_speed();

    void Update();

    
public slots:
    void onOK();
    void onCancel();
    void onUp();
    void onDown();
    void onSelect();

public:
    Ui::top_speed *ui;

    int m_nSpeed;
    int m_nJumpValue;
    QVector<QLabel4*> m_vtPic;
    QVector<QLabel3*> m_vtText;

    void SetJumpNum(int index);


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // TOP_SPEED_H
