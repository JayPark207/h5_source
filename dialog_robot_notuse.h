#ifndef DIALOG_ROBOT_NOTUSE_H
#define DIALOG_ROBOT_NOTUSE_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_home2.h"
#include "dialog_confirm.h"

namespace Ui {
class dialog_robot_notuse;
}

class dialog_robot_notuse : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_robot_notuse(QWidget *parent = 0);
    ~dialog_robot_notuse();

    bool Home(bool bCheck);

private:
    Ui::dialog_robot_notuse *ui;

    dialog_home2* dig_home2;

    QTimer* timer;

    int m_nXpos;

public slots:
    void onClose();
    void onResume();
    void onTimer();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ROBOT_NOTUSE_H
