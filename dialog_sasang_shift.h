#ifndef DIALOG_SASANG_SHIFT_H
#define DIALOG_SASANG_SHIFT_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_sasang_shift;
}

class dialog_sasang_shift : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_sasang_shift(QWidget *parent = 0);
    ~dialog_sasang_shift();

    void Update();

    bool IsShift();
    void Get(ST_SSSHIFT_VALUE& value);

    bool IsSpeed();
    float GetSpeed();
    bool IsAccuracy();
    float GetAccuracy();

private:
    Ui::dialog_sasang_shift *ui;

    QTimer* timer;

    QVector<QLabel3*> m_vtAxis;
    QVector<QLabel4*> m_vtValPic;
    QVector<QLabel3*> m_vtVal;

    ST_SSSHIFT_VALUE m_Value;
    float m_Speed;
    float m_Accuracy;
    bool m_bSpeed;
    bool m_bAccuracy;

    void Display_Value(ST_SSSHIFT_VALUE value);
    void SetValue(int index, float value);

    bool IsModify();

    void Display_Speed(float speed);
    void Display_Accuracy(float accuracy);

public slots:
    void onOK();
    void onCancel();

    void onTimer();

    void onValue();

    void onSpd();
    void onSpdVal();
    void onAccu();
    void onAccuVal();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SASANG_SHIFT_H
