#include "dialog_dualunload.h"
#include "ui_dialog_dualunload.h"

dialog_dualunload::dialog_dualunload(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_dualunload)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnUsePic,SIGNAL(mouse_release()),this,SLOT(onUse()));
    connect(ui->btnUse,SIGNAL(mouse_press()),ui->btnUsePic,SLOT(press()));
    connect(ui->btnUse,SIGNAL(mouse_release()),ui->btnUsePic,SLOT(release()));

    m_vtXPic.clear();                    m_vtX.clear();
    m_vtXPic.append(ui->btnTravPlusPic); m_vtX.append(ui->btnTravPlus);
    m_vtXPic.append(ui->btnTravMinusPic);m_vtX.append(ui->btnTravMinus);

    int i;
    for(i=0;i<m_vtXPic.count();i++)
    {
        connect(m_vtXPic[i],SIGNAL(mouse_release()),this,SLOT(onX()));
        connect(m_vtX[i],SIGNAL(mouse_press()),m_vtXPic[i],SLOT(press()));
        connect(m_vtX[i],SIGNAL(mouse_release()),m_vtXPic[i],SLOT(release()));
    }

    m_vtYPic.clear();                  m_vtY.clear();
    m_vtYPic.append(ui->btnFBPlusPic); m_vtY.append(ui->btnFBPlus);
    m_vtYPic.append(ui->btnFBMinusPic);m_vtY.append(ui->btnFBMinus);

    for(i=0;i<m_vtYPic.count();i++)
    {
        connect(m_vtYPic[i],SIGNAL(mouse_release()),this,SLOT(onY()));
        connect(m_vtY[i],SIGNAL(mouse_press()),m_vtYPic[i],SLOT(press()));
        connect(m_vtY[i],SIGNAL(mouse_release()),m_vtYPic[i],SLOT(release()));
    }

    m_vtZPic.clear();                  m_vtZ.clear();
    m_vtZPic.append(ui->btnUDPlusPic); m_vtZ.append(ui->btnUDPlus);
    m_vtZPic.append(ui->btnUDMinusPic);m_vtZ.append(ui->btnUDMinus);

    for(i=0;i<m_vtZPic.count();i++)
    {
        connect(m_vtZPic[i],SIGNAL(mouse_release()),this,SLOT(onZ()));
        connect(m_vtZ[i],SIGNAL(mouse_press()),m_vtZPic[i],SLOT(press()));
        connect(m_vtZ[i],SIGNAL(mouse_release()),m_vtZPic[i],SLOT(release()));
    }

}

dialog_dualunload::~dialog_dualunload()
{
    delete ui;
}
void dialog_dualunload::showEvent(QShowEvent *)
{
    Update();
    bSave = false;
}
void dialog_dualunload::hideEvent(QHideEvent *)
{
    if(bSave)
        Recipe->SaveVariable();
}
void dialog_dualunload::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_dualunload::onClose()
{
    emit accept();
}

void dialog_dualunload::Update()
{
    m_ItemName.clear();
    m_ItemName.append(tr("No Use"));
    m_ItemName.append(tr("Use"));

    vars.clear();
    vars.append(HyRecipe::mdDualUnload);
    vars.append(HyRecipe::vDualRevX);
    vars.append(HyRecipe::vDualRevY);
    vars.append(HyRecipe::vDualRevZ);

    if(Recipe->Gets(vars, datas))
    {
        if((int)datas[0] < m_ItemName.size())
            ui->btnUse->setText(m_ItemName[(int)datas[0]]);

        Display_X(datas[1]);
        Display_Y(datas[2]);
        Display_Z(datas[3]);

        ui->wigRev->setEnabled((int)datas[0] > 0);
    }
}

void dialog_dualunload::Display_X(float data)
{
    for(int i=0;i<m_vtXPic.count();i++)
        m_vtXPic[i]->setAutoFillBackground(false);

    if((int)data == -1)
        m_vtXPic[DU_MINUS]->setAutoFillBackground(true);
    else if((int)data == 1)
        m_vtXPic[DU_PLUS]->setAutoFillBackground(true);
}
void dialog_dualunload::Display_Y(float data)
{
    for(int i=0;i<m_vtYPic.count();i++)
        m_vtYPic[i]->setAutoFillBackground(false);

    if((int)data == -1)
        m_vtYPic[DU_MINUS]->setAutoFillBackground(true);
    else if((int)data == 1)
        m_vtYPic[DU_PLUS]->setAutoFillBackground(true);
}
void dialog_dualunload::Display_Z(float data)
{
    for(int i=0;i<m_vtZPic.count();i++)
        m_vtZPic[i]->setAutoFillBackground(false);

    if((int)data == -1)
        m_vtZPic[DU_MINUS]->setAutoFillBackground(true);
    else if((int)data == 1)
        m_vtZPic[DU_PLUS]->setAutoFillBackground(true);
}

void dialog_dualunload::onX()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtXPic.indexOf(btn);
    if(index < 0) return;

    if(m_vtXPic[index]->autoFillBackground())
        return;

    bSave = true;

    if(index == DU_PLUS)
    {
        if(Recipe->Set(HyRecipe::vDualRevX, 1, false))
            Update();
    }
    else
    {
        if(Recipe->Set(HyRecipe::vDualRevX, -1, false))
            Update();
    }
}
void dialog_dualunload::onY()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtYPic.indexOf(btn);
    if(index < 0) return;

    if(m_vtYPic[index]->autoFillBackground())
        return;

    bSave = true;

    if(index == DU_PLUS)
    {
        if(Recipe->Set(HyRecipe::vDualRevY, 1, false))
            Update();
    }
    else
    {
        if(Recipe->Set(HyRecipe::vDualRevY, -1, false))
            Update();
    }
}
void dialog_dualunload::onZ()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtZPic.indexOf(btn);
    if(index < 0) return;

    if(m_vtZPic[index]->autoFillBackground())
        return;

    bSave = true;

    if(index == DU_PLUS)
    {
        if(Recipe->Set(HyRecipe::vDualRevZ, 1, false))
            Update();
    }
    else
    {
        if(Recipe->Set(HyRecipe::vDualRevZ, -1, false))
            Update();
    }
}

void dialog_dualunload::onUse()
{
    dialog_mode_select* sel = (dialog_mode_select*)gGetDialog(DIG_MODE_SELECT);
    sel->InitTitle(ui->lbTitle->text());
    sel->InitRecipe((int)vars[0]);
    sel->InitNum(m_ItemName.size());
    for(int i=0;i<sel->GetNum();i++)
        sel->InitString(i,m_ItemName[i]);

    if(sel->exec() == QDialog::Accepted)
        Update();
}
