#ifndef DIALOG_TAKEOUT_TIMING_H
#define DIALOG_TAKEOUT_TIMING_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_mode_select.h"

namespace Ui {
class dialog_takeout_timing;
}

class dialog_takeout_timing : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_takeout_timing(QWidget *parent = 0);
    ~dialog_takeout_timing();

    void Init();

    void Update();

public slots:
    void onClose();
    void onUse();
    void onSelect();
    void onTiming();

private:
    Ui::dialog_takeout_timing *ui;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;
    QStringList m_ItemName;

    QVector<QLabel4*> m_vtPic;
    QVector<QLabel3*> m_vtIcon;
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtSel;

    QVector<QLabel4*> m_vtTbNoPic;
    QVector<QLabel3*> m_vtTbNo;
    QVector<QLabel3*> m_vtTbName;

    void Display_Use(bool use);
    void Display_TimingNo();

    int m_nSelectNo;
    void Display_SelectNo(int select_no);


    bool m_bSave;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // DIALOG_TAKEOUT_TIMING_H
