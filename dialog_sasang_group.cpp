#include "dialog_sasang_group.h"
#include "ui_dialog_sasang_group.h"

dialog_sasang_group::dialog_sasang_group(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_sasang_group)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtTbNo.clear();m_vtTbGrp.clear();m_vtTbDate.clear();
    m_vtTbNo.append(ui->tbNo);  m_vtTbGrp.append(ui->tbGrp);  m_vtTbDate.append(ui->tbDate);
    m_vtTbNo.append(ui->tbNo_2);m_vtTbGrp.append(ui->tbGrp_2);m_vtTbDate.append(ui->tbDate_2);
    m_vtTbNo.append(ui->tbNo_3);m_vtTbGrp.append(ui->tbGrp_3);m_vtTbDate.append(ui->tbDate_3);
    m_vtTbNo.append(ui->tbNo_4);m_vtTbGrp.append(ui->tbGrp_4);m_vtTbDate.append(ui->tbDate_4);
    m_vtTbNo.append(ui->tbNo_5);m_vtTbGrp.append(ui->tbGrp_5);m_vtTbDate.append(ui->tbDate_5);
    m_vtTbNo.append(ui->tbNo_6);m_vtTbGrp.append(ui->tbGrp_6);m_vtTbDate.append(ui->tbDate_6);
    m_vtTbNo.append(ui->tbNo_7);m_vtTbGrp.append(ui->tbGrp_7);m_vtTbDate.append(ui->tbDate_7);
    m_vtTbNo.append(ui->tbNo_8);m_vtTbGrp.append(ui->tbGrp_8);m_vtTbDate.append(ui->tbDate_8);

    int i;
    for(i=0;i<m_vtTbNo.count();i++)
    {
        connect(m_vtTbNo[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtTbGrp[i],SIGNAL(mouse_release()),m_vtTbNo[i],SIGNAL(mouse_release()));
        connect(m_vtTbDate[i],SIGNAL(mouse_release()),m_vtTbNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

    connect(ui->btnNewPic,SIGNAL(mouse_release()),this,SLOT(onNew()));
    connect(ui->btnNew,SIGNAL(mouse_press()),ui->btnNewPic,SLOT(press()));
    connect(ui->btnNew,SIGNAL(mouse_release()),ui->btnNewPic,SLOT(release()));
    connect(ui->btnNewIcon,SIGNAL(mouse_press()),ui->btnNewPic,SLOT(press()));
    connect(ui->btnNewIcon,SIGNAL(mouse_release()),ui->btnNewPic,SLOT(release()));

    connect(ui->btnCopyPic,SIGNAL(mouse_release()),this,SLOT(onCopy()));
    connect(ui->btnCopy,SIGNAL(mouse_press()),ui->btnCopyPic,SLOT(press()));
    connect(ui->btnCopy,SIGNAL(mouse_release()),ui->btnCopyPic,SLOT(release()));
    connect(ui->btnCopyIcon,SIGNAL(mouse_press()),ui->btnCopyPic,SLOT(press()));
    connect(ui->btnCopyIcon,SIGNAL(mouse_release()),ui->btnCopyPic,SLOT(release()));

    connect(ui->btnRenamePic,SIGNAL(mouse_release()),this,SLOT(onRename()));
    connect(ui->btnRename,SIGNAL(mouse_press()),ui->btnRenamePic,SLOT(press()));
    connect(ui->btnRename,SIGNAL(mouse_release()),ui->btnRenamePic,SLOT(release()));
    connect(ui->btnRenameIcon,SIGNAL(mouse_press()),ui->btnRenamePic,SLOT(press()));
    connect(ui->btnRenameIcon,SIGNAL(mouse_release()),ui->btnRenamePic,SLOT(release()));

    connect(ui->btnDelPic,SIGNAL(mouse_release()),this,SLOT(onDelete()));
    connect(ui->btnDel,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDel,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));
    connect(ui->btnDelIcon,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDelIcon,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));

    connect(ui->btnInsidePic,SIGNAL(mouse_release()),this,SLOT(onInside()));
    connect(ui->btnInside,SIGNAL(mouse_press()),ui->btnInsidePic,SLOT(press()));
    connect(ui->btnInside,SIGNAL(mouse_release()),ui->btnInsidePic,SLOT(release()));
    connect(ui->btnInsideIcon,SIGNAL(mouse_press()),ui->btnInsidePic,SLOT(press()));
    connect(ui->btnInsideIcon,SIGNAL(mouse_release()),ui->btnInsidePic,SLOT(release()));


    // initial
    dig_sasang_pattern = 0;
    top = 0;
}

dialog_sasang_group::~dialog_sasang_group()
{
    delete ui;
}
void dialog_sasang_group::showEvent(QShowEvent *)
{
    Update();

    SubTop();
}
void dialog_sasang_group::hideEvent(QHideEvent *)
{

}
void dialog_sasang_group::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_sasang_group::onClose()
{
    emit accept();
}

void dialog_sasang_group::Update()
{
    if(Sasang->Group->ReadAll() < 0)
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Read Error!")),
                     QString(tr("Please, try again")));
        msg->exec();
        //return;
    }

    m_nStartIndex=0;m_nSelectedIndex=0;
    Redraw_List(m_nStartIndex);
}

void dialog_sasang_group::Redraw_List(int start_index)
{
    int cal_size = Sasang->Group->GroupList.size() - start_index;
    int index;
    QString no,grp,date;

    Display_ListOn(-1); // all off

    for(int i=0;i<m_vtTbNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            no = QString().setNum(index + 1);
            grp = Sasang->Group->GroupList[index].name;
            date = Sasang->Group->GroupList[index].datetime.toString("yyyy.MM.dd hh:mm:ss");
        }
        else
        {
            // no data.
            no.clear();
            grp.clear();
            date.clear();
        }

        m_vtTbNo[i]->setText(no);
        m_vtTbGrp[i]->setText(grp);
        m_vtTbDate[i]->setText(date);

        if((start_index + i) == m_nSelectedIndex)
            Display_ListOn(i);
    }

    // display list size.
    ui->lbListSize->setText(QString().setNum(Sasang->Group->GroupList.size()));
}

void dialog_sasang_group::Display_ListOn(int table_index)
{
    for(int i=0;i<m_vtTbNo.count();i++)
    {
        m_vtTbNo[i]->setAutoFillBackground(i==table_index);
        m_vtTbGrp[i]->setAutoFillBackground(i==table_index);
        m_vtTbDate[i]->setAutoFillBackground(i==table_index);
    }
}





void dialog_sasang_group::onList()
{
    QLabel3* sel = (QLabel3*)sender();

    int index = m_vtTbNo.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nStartIndex + index;
    if(list_index == m_nSelectedIndex) return;

    if(list_index < Sasang->Group->GroupList.size())
    {
        m_nSelectedIndex = list_index;
        Redraw_List(m_nStartIndex);
    }
}
void dialog_sasang_group::onUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_sasang_group::onDown()
{
    if(m_nStartIndex >= (Sasang->Group->GroupList.size() - m_vtTbNo.size()))
        return;

    Redraw_List(++m_nStartIndex);
}
void dialog_sasang_group::onInside()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= Sasang->Group->GroupList.size()) return;

    if(dig_sasang_pattern == 0)
        dig_sasang_pattern = new dialog_sasang_pattern();

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!Sasang->Group2Patterns(Sasang->Group->GroupList[m_nSelectedIndex],
                               Sasang->Pattern->PatternList))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Inside!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    //QString group_name = Sasang->Group->GroupList[m_nSelectedIndex].name;
    dig_sasang_pattern->Init(Sasang->Group->GroupList[m_nSelectedIndex]);
    //if(dig_sasang_pattern->exec() != QDialog::Accepted)
    //    return;
    dig_sasang_pattern->exec();
    Sasang->Group->GroupList[m_nSelectedIndex] = dig_sasang_pattern->GetGrpData();

    /*if(!Sasang->Patterns2Group(Sasang->Pattern->PatternList, Sasang->Group->GroupList[m_nSelectedIndex]))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to save group data!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    int ret = Sasang->Group->SaveFile(Sasang->Group->GroupList[m_nSelectedIndex]);
    if(ret < 0)
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to save group data!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(tr("SAVE"));
    msg->Message(tr("Success to Save!"), group_name);
    msg->exec();*/
}

void dialog_sasang_group::onNew()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnNew->text());
    conf->Message(QString(tr("Would you want to new group?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->btnNew->text());
    kb->m_kb->SetText("");

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(kb->exec() == QDialog::Accepted)
    {
        QString name;
        name = kb->m_kb->GetText();

        if(!Sasang->Group->IsAbleName(name))
        {
            msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Wrong Name!")),
                         QString(tr("Please, Check name & try again")));
            msg->exec();
            return;
        }
        ST_SSGROUP_DATA data;
        if(!Sasang->Group->Default(data))
        {
            msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Making New Group Error!")),
                         QString(tr("Please, try again")));
            msg->exec();
            return;
        }

        data.name = name;
        if(!Sasang->Group->New(data))
        {
            msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Making New Group Error!")),
                         QString(tr("Please, try again")));
            msg->exec();
            return;
        }

        // redraw
        m_nSelectedIndex = Sasang->Group->GroupList.size() - 1;
        if(Sasang->Group->GroupList.size() > (m_nStartIndex + m_vtTbNo.count()))
            m_nStartIndex = Sasang->Group->GroupList.size() - m_vtTbNo.count();

        Redraw_List(m_nStartIndex);

        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(QString(ui->btnNew->text()));
        msg->Message(tr("Success to New!"), name);
        msg->exec();
    }

}
void dialog_sasang_group::onCopy()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= Sasang->Group->GroupList.size()) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnCopy->text());
    QString str;
    str.sprintf("[%d] ",(m_nSelectedIndex+1));
    str += Sasang->Group->GroupList[m_nSelectedIndex].name;
    conf->Message(QString(tr("Would you want to copy?")),str);
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->btnNew->text());
    kb->m_kb->SetText("");

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(kb->exec() == QDialog::Accepted)
    {
        QString name;
        name = kb->m_kb->GetText();

        if(!Sasang->Group->IsAbleName(name))
        {
            msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Wrong Name!")),
                         QString(tr("Please, Check name & try again")));
            msg->exec();
            return;
        }

        if(!Sasang->Group->Copy(m_nSelectedIndex, name))
        {
            msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Fail to Copy!")),
                         QString(tr("Please, try again")));
            msg->exec();
            return;
        }

        // redraw
        m_nSelectedIndex = Sasang->Group->GroupList.size() - 1;
        if(Sasang->Group->GroupList.size() > (m_nStartIndex + m_vtTbNo.count()))
            m_nStartIndex = Sasang->Group->GroupList.size() - m_vtTbNo.count();

        Redraw_List(m_nStartIndex);

        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(QString(ui->btnCopy->text()));
        msg->Message(tr("Success to Copy!"), name);
        msg->exec();
    }

}
void dialog_sasang_group::onRename()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= Sasang->Group->GroupList.size()) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnRename->text());
    conf->Message(QString(tr("Would you want to rename?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->btnRename->text());
    QString name_old = Sasang->Group->GroupList[m_nSelectedIndex].name;
    kb->m_kb->SetText(name_old);

    if(kb->exec() != QDialog::Accepted)
        return;

    QString name = kb->m_kb->GetText();

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!Sasang->Group->IsAbleName(name))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Wrong Name!")),
                     QString(tr("Please, Check name & try again")));
        msg->exec();
        return;
    }

    if(!Sasang->Group->Rename(m_nSelectedIndex, name))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Rename!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    Redraw_List(m_nStartIndex);

    msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(QString(ui->btnRename->text()));
    QString str;
    str = name_old;
    str += " -> ";
    str += name;
    msg->Message(tr("Success to Rename!"), str);
    msg->exec();
}

void dialog_sasang_group::onDelete()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= Sasang->Group->GroupList.size()) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDel->text());
    conf->Message(QString(tr("Would you want to Delete?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDel->text());
    conf->Message(QString(tr("Really, Would you want to Delete?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    QString name = Sasang->Group->GroupList[m_nSelectedIndex].name;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!Sasang->Group->Del(m_nSelectedIndex))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Delete!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    if(m_nSelectedIndex >= Sasang->Group->GroupList.size())
        m_nSelectedIndex = Sasang->Group->GroupList.size() - 1;

    if((m_nStartIndex + m_vtTbNo.size()) > Sasang->Group->GroupList.size())
    {
        if(m_nStartIndex > 0)
            m_nStartIndex--;
    }

    Redraw_List(m_nStartIndex);

    msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(QString(ui->btnRename->text()));
    msg->Message(tr("Success to Delete!"), name);
    msg->exec();
}

void dialog_sasang_group::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
