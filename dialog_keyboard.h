#ifndef DIALOG_KEYBOARD_H
#define DIALOG_KEYBOARD_H

#include <QDialog>
#include <QStackedWidget>

#include "global.h"
#include "keyboard.h"


namespace Ui {
class dialog_keyboard;
}

class dialog_keyboard : public QDialog
{
    Q_OBJECT
    
public:
    explicit dialog_keyboard(QWidget *parent = 0);
    ~dialog_keyboard();

    keyboard* m_kb;
    QStackedWidget* sw;
    
private:
    Ui::dialog_keyboard *ui;

protected:
    void changeEvent(QEvent *);

};

#endif // DIALOG_KEYBOARD_H
