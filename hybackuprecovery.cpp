#include "hybackuprecovery.h"

HyBackupRecovery::HyBackupRecovery()
{

}

/** For Backup Functions **/

QString HyBackupRecovery::MakeDirName(QString input_name)
{
    // name rule => datetime_name ex) 2017010112233_inputname
    QString str;
    str = QDateTime::currentDateTime().toString("yyyyMMddhhmmss_");
    str += input_name;

    return str;
}
bool HyBackupRecovery::IsExist(QString path)
{
    CNRobo* pCon = CNRobo::getInstance();
    bool bExist=false;

    if(pCon->checkDirFileExist(path, bExist) < 0)
        return false;

    return bExist;
}
bool HyBackupRecovery::MakeDir(QString dir_path)
{
    if(IsExist(dir_path))
        return true;

    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->makeDatabaseDir(dir_path);
    if(ret < 0)
    {
        qDebug() << "makeDatabaseDir ret=" << ret;
        return false;
    }
    return true;
}

bool HyBackupRecovery::Backup_Recipe(QString tp_recipe_path, QString sd_recipe_path, QString target_path)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    // 1. tp recipe backup
    ret = pCon->copyDatabaseDir(tp_recipe_path, target_path);
    if(ret < 0)
    {
        qDebug() << tp_recipe_path << "=>>" << target_path;
        qDebug() << "copyDatabaseDir ret =" << ret;
        return false;
    }

    // 2. sd recipe backup
    if(IsExist(sd_recipe_path))
    {
        ret = pCon->copyDatabaseDir(sd_recipe_path, target_path);
        if(ret < 0)
        {
            qDebug() << sd_recipe_path << "=>>" << target_path;
            qDebug() << "copyDatabaseDir ret =" << ret;
            return false;
        }
    }

    return true;
}

bool HyBackupRecovery::Backup_Param(QString target_path)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    // 1. read userparam
    QStringList param;
    ret = pCon->getUserParams(param);
    if(ret < 0)
    {
        qDebug() << "getUserParam ret=" << ret;
        return false;
    }

    // 2. write file on target_path
    QString target = target_path + BACKUP_PARAM_FILENAME;
    ret = pCon->createProgramFile(target, param);
    if(ret < 0)
    {
        qDebug() << target;
        qDebug() << "createProgramFile ret =" << ret;
        return false;
    }

    return true;
}

bool HyBackupRecovery::Backup_Config(QString target_path)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    // 1. read robot config.
    QStringList config;
    ret = pCon->getRobotConf(config);
    if(ret < 0)
    {
        qDebug() << "getRobotConf ret =" << ret;
        return false;
    }

    QString target = target_path + BACKUP_CONFIG_FILENAME;
    ret = pCon->createProgramFile(target, config);
    if(ret < 0)
    {
        qDebug() << target;
        qDebug() << "createProgramFile ret =" << ret;
        return false;
    }

    return true;
}
/** ==================== **/

/** For Recovery Functions **/

bool HyBackupRecovery::Read_BackupDir()
{
    QString backup_path = BACKUP_USB_PATH;
    backup_path += BACKUP_DIR_NAME;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    // read backup dir list
    m_BackupList.clear();
    ret = pCon->getDatabaseDirList(backup_path, m_BackupList);
    if(ret < 0)
    {
        qDebug() << backup_path;
        qDebug() << "getDatabaseDirList ret =" << ret;
        return false;
    }

    m_BackupList.removeAll(".");
    m_BackupList.removeAll("..");

    // for recently data front
    qSort(m_BackupList.begin(),m_BackupList.end(),qGreater<QString>());

    return true;
}

bool HyBackupRecovery::Parsing_BackupDir(QString backup_list_data, QString &date, QString &time, QString &name)
{
    QStringList templist;
    QDateTime datetime;
    QString temp = backup_list_data;

    templist = temp.split("_");
    datetime = QDateTime::fromString(templist.first(),"yyyyMMddhhmmss");
    temp.remove(templist.first());
    temp.remove("_");

    date = datetime.toString("yyyy.MM.dd");
    time = datetime.toString("hh:mm:ss");
    name = temp;

    return true;
}


bool HyBackupRecovery::Read_Recipe(QString backup_list_data)
{
    // list clear
    m_RecipeNo.clear();
    m_RecipeName.clear();
    m_RecipeDate.clear();

    CNRobo* pCon = CNRobo::getInstance();
    int ret=0;
    QString path = BACKUP_USB_PATH;
    path += BACKUP_DIR_NAME;
    path += "/";
    path += backup_list_data;
    QString tp_path = path + RM_RECIPE_DIRNAME;
    QString sd_path = path + RM_SD_RECIPE_DIRNAME;

    // (1) get recipe list
    QStringList list;
    if(IsExist(tp_path))
    {
        ret = pCon->getDatabaseDirList(tp_path, list);
        if(ret < 0)
        {
            qDebug() << "getDatabaseDirList ret =" << ret;
            return false;
        }
    }

    if(IsExist(sd_path))
    {
        ret = pCon->getDatabaseDirList(sd_path, list);
        if(ret < 0)
        {
            qDebug() << "getDatabaseDirList ret =" << ret;
            return false;
        }
    }

    // (2) remove don't need datas.
    list.removeAll(".");
    list.removeAll("..");
    list.removeAll("default");

    // (3) casting integer & sorting 1,2,3,~,N
    QList<int> int_list;
    foreach(QString str, list)
    {
        int_list << str.toInt();
    }
    qSort(int_list.begin(),int_list.end());

    // (4) make m_RecipeNo data. (this is a key)
    foreach(int num, int_list)
    {
        m_RecipeNo << QString().setNum(num);
    }

    // (5) get info.
    QString info_path;
    QStringList info_data;
    QDateTime info_date;
    for(int i=0;i<m_RecipeNo.count();i++)
    {

        if(m_RecipeNo[i].toInt() <= RM_MAX_RECIPE_INNER_NUM)
        {
            // tp
            info_path = tp_path + "/" + m_RecipeNo[i];
            info_path += RM_INFO_FILENAME;
        }
        else
        {
            // sd
            info_path = sd_path + "/" + m_RecipeNo[i];
            info_path += RM_INFO_FILENAME;
        }


        if(!IsExist(info_path))
        {
            m_RecipeName.append("");
            m_RecipeDate.append("");
            continue;
        }

        info_data.clear();
        ret = pCon->getProgramFile(info_path, info_data);
        if(ret < 0)
        {
            m_RecipeName.append("");
            m_RecipeDate.append("");
            continue;
        }

        m_RecipeName.append(info_data[1]);
        info_date = QDateTime::fromString(info_data[2], Qt::ISODate);
        m_RecipeDate.append(info_date.toString(RM_DATE_FORMAT));
    }

    return true;
}

bool HyBackupRecovery::HasParam(QString backup_list_data)
{
    QString path = BACKUP_USB_PATH;
    path += BACKUP_DIR_NAME;
    path += "/";
    path += backup_list_data;
    path += BACKUP_PARAM_FILENAME;

    return IsExist(path);
}
bool HyBackupRecovery::HasConfig(QString backup_list_data)
{
    QString path = BACKUP_USB_PATH;
    path += BACKUP_DIR_NAME;
    path += "/";
    path += backup_list_data;
    path += BACKUP_CONFIG_FILENAME;

    return IsExist(path);
}


bool HyBackupRecovery::Recovery_Recipe(QString backup_list_data, QString recipe_no)
{
    // make base path ex) /usb/HYBackup/20170101111111_name
    QString base_path = BACKUP_USB_PATH;
    base_path += BACKUP_DIR_NAME;
    base_path += "/";
    base_path += backup_list_data;

    // make source path & target path (full path)
    QString source_path;
    QString target_path;
    if(recipe_no.toInt() <= RM_MAX_RECIPE_INNER_NUM)
    {
        // tp
        source_path = base_path + RM_RECIPE_DIRNAME;
        target_path = RM_RECIPE_PATH;
    }
    else
    {
        // sd
        source_path = base_path + RM_SD_RECIPE_DIRNAME;
        target_path = RM_SD_RECIPE_PATH;
    }

    source_path += "/";
    source_path += recipe_no;

    // check exist source path.
    //if(!IsExist(source_path))
    //    return false;

    // copy from source to target
    CNRobo* pCon = CNRobo::getInstance();
    int ret=0;

    ret = pCon->copyDatabaseDir(source_path, target_path);
    if(ret < 0)
    {
        qDebug() << source_path << "=>>" << target_path;
        qDebug() << "copyDatabaseDir ret=" << ret;
        return false;
    }

    return true;
}

//bool HyBackupRecovery::Recovery_Recipe(QString backup_list_data, QStringList& reciep_no_list)
//{
//}

bool HyBackupRecovery::Recovery_Param(QString backup_list_data)
{
    // make base path ex) /usb/HYBackup/20170101111111_name
    QString base_path = BACKUP_USB_PATH;
    base_path += BACKUP_DIR_NAME;
    base_path += "/";
    base_path += backup_list_data;

    // make source path
    QString source_path;
    source_path = base_path;
    source_path += BACKUP_PARAM_FILENAME;

    // read file on source path
    CNRobo* pCon = CNRobo::getInstance();
    int ret=0;

    QStringList param;
    param.clear();
    ret = pCon->getProgramFile(source_path, param);
    if(ret < 0)
    {
        qDebug() << source_path;
        qDebug() << "getProgramFile ret =" << ret;
        return false;
    }

    // set userparam
    ret = pCon->setUserParams(param);
    if(ret < 0)
    {
        qDebug() << "setUserParams ret=" << ret;
        return false;
    }

    return true;
}

bool HyBackupRecovery::Recovery_Config(QString backup_list_data)
{
    // make base path ex) /usb/HYBackup/20170101111111_name
    QString base_path = BACKUP_USB_PATH;
    base_path += BACKUP_DIR_NAME;
    base_path += "/";
    base_path += backup_list_data;

    // make source path
    QString source_path;
    source_path = base_path;
    source_path += BACKUP_CONFIG_FILENAME;

    // read file on source path
    CNRobo* pCon = CNRobo::getInstance();
    int ret=0;

    QStringList config;
    config.clear();
    ret = pCon->getProgramFile(source_path, config);
    if(ret < 0)
    {
        qDebug() << source_path;
        qDebug() << "getProgramFile ret =" << ret;
        return false;
    }

    // set robot config
    ret = pCon->setRobotConf(config);
    if(ret < 0)
    {
        qDebug() << "setRobotConf ret=" << ret;
        return false;
    }

    return true;
}

/** ====================== **/


