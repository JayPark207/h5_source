#ifndef HYNETWORK_H
#define HYNETWORK_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyrecipe.h"

#define NETWORK_STATE_DISCONNECT        0
#define NETWORK_STATE_CONNECT           1
#define NETWORK_CONNECT                 2
#define NETWORK_DISCONNECT              3

#define TCP_READY   0
#define TCP_PUSH    1

#define SERIAL_CALL_TEMP 10
#define SERIAL_COMP_TEMP 20
#define SERIAL_CALL_WEIGHT 11
#define SERIAL_COMP_WEIGHT 21
#define SERIAL_CALL_WRESET 12
#define SERIAL_COMP_WRESET 22
#define SERIAL_CALL_ESEN 13
#define SERIAL_COMP_ESEN 23

class HyNetwork : public QObject
{
    Q_OBJECT

public:

    struct ST_NW_VARINFO
    {
        QString         name;
        cn_vartype      type;
        cn_variant      data;

        QString         ui_name;
    };


private:
    HyRecipe* m_Recipe;

public:
    explicit HyNetwork(HyRecipe* recipe);

    void Init();

    bool Read(QVector<ST_NW_VARINFO>& info);
    bool Write(QVector<ST_NW_VARINFO>& info);

    // code part
    QVector<ST_NW_VARINFO> Info_Code;
    void Init_Code();

    // pop set part
    QVector<ST_NW_VARINFO> Info_Pop;
    QVector<ST_NW_VARINFO> Info_PopIp;
    void Init_Pop();

    // hyserver set part
    QVector<ST_NW_VARINFO> Info_Hys;
    QVector<ST_NW_VARINFO> Info_HysIp;
    void Init_Hys();

    // weight part
    QVector<ST_NW_VARINFO> Info_Weight;
    void Init_Weight();

    // temp part
    QVector<ST_NW_VARINFO> Info_Temp;
    void Init_Temp();

    // e-sensor part
    QVector<ST_NW_VARINFO> Info_ESen;
    void Init_ESen();

    // pop protocol part
    QVector<ST_NW_VARINFO> Info_Pop_Protocol;
    void Init_Pop_Protocol();

    // hyserver protocol part
    QVector<ST_NW_VARINFO> Info_Hys_Protocol;
    void Init_Hys_Protocol();



    // pop control
    bool Set_PopUse(float data);
    bool Get_PopUse(float& data);

    bool Set_PopFlag(float data);
    bool Get_PopFlag(float& data);

    bool Set_PopTrig(float data);
    bool Get_PopTrig(float& data);

    bool Set_PopState(float data);
    bool Get_PopState(float& data);

    bool Get_Pop(float& use, float& flag, float& trig, float& state);

    // hyserver control
    bool Set_HysUse(float data);
    bool Get_HysUse(float& data);

    bool Set_HysFlag(float data);
    bool Get_HysFlag(float& data);

    bool Set_HysTrig(float data);
    bool Get_HysTrig(float& data);

    bool Set_HysState(float data);
    bool Get_HysState(float& data);

    bool Get_Hys(float& use, float& flag, float& trig, float& state);

    // serial control
    bool Set_SerialFlag(float data);
    bool Get_SerialFlag(float& data);

    bool Set_SerialCall(float data);
    bool Get_SerialCall(float& data);

    bool Get_Serial(float &flag, float &call, float &cnterr, float &cntcall);

    bool Set_CountError(float data);
    bool Get_CountError(float& data);

    bool Set_CountCall(float data);
    bool Get_CountCall(float& data);


    // TP Main IP address handling functions.
    // with gateway setting.
    // format : 000.000.000.000 (both ip & gw)
    bool Set_IP(QString ip);                    // auto gateway setting
    bool Set_IP(QString ip, QString gateway);   // manual gateway setting.
    bool Get_IP(QString& ip, QString& gateway); // read ip & gw.


};

#endif // HYNETWORK_H
