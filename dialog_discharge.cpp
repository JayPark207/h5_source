#include "dialog_discharge.h"
#include "ui_dialog_discharge.h"

dialog_discharge::dialog_discharge(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_discharge)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnDiscPic,SIGNAL(mouse_release()),this,SLOT(onUse_Disc()));
    connect(ui->btnDisc,SIGNAL(mouse_press()),ui->btnDiscPic,SLOT(press()));
    connect(ui->btnDisc,SIGNAL(mouse_release()),ui->btnDiscPic,SLOT(release()));

    connect(ui->btn1stDiscPic,SIGNAL(mouse_release()),this,SLOT(onUse_1stDisc()));
    connect(ui->btn1stDisc,SIGNAL(mouse_press()),ui->btn1stDiscPic,SLOT(press()));
    connect(ui->btn1stDisc,SIGNAL(mouse_release()),ui->btn1stDiscPic,SLOT(release()));

    connect(ui->btnSetCntPic,SIGNAL(mouse_release()),this,SLOT(onSet_1stDisc()));
    connect(ui->btnSetCnt,SIGNAL(mouse_press()),ui->btnSetCntPic,SLOT(press()));
    connect(ui->btnSetCnt,SIGNAL(mouse_release()),ui->btnSetCntPic,SLOT(release()));

    connect(ui->btnCntPic,SIGNAL(mouse_release()),this,SLOT(onCnt_1stDisc()));
    connect(ui->btnCnt,SIGNAL(mouse_press()),ui->btnCntPic,SLOT(press()));
    connect(ui->btnCnt,SIGNAL(mouse_release()),ui->btnCntPic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset_1stDisc()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));


}

dialog_discharge::~dialog_discharge()
{
    delete ui;
}
void dialog_discharge::showEvent(QShowEvent *)
{
    Update();
}
void dialog_discharge::hideEvent(QHideEvent *)
{
}
void dialog_discharge::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_discharge::onClose()
{
    emit accept();
}

void dialog_discharge::Update()
{
    m_ItemName.clear();
    m_ItemName.append(tr("No Use"));
    m_ItemName.append(tr("Use"));

    vars.clear();
    vars.append(HyRecipe::bDischarge);
    vars.append(HyRecipe::b1stBad);
    vars.append(HyRecipe::v1stBadSet);
    vars.append(HyRecipe::v1stBadCnt);

    if(Recipe->Gets(vars, datas))
    {
        if((int)datas[0] < m_ItemName.size())
            ui->btnDisc->setText(m_ItemName[(int)datas[0]]);

        if((int)datas[1] < m_ItemName.size())
            ui->btn1stDisc->setText(m_ItemName[(int)datas[1]]);

        QString str;
        str.sprintf("%.0f", datas[2]);
        ui->btnSetCnt->setText(str);

        str.sprintf("%.0f", datas[3]);
        ui->btnCnt->setText(str);

        ui->wig1stDiscSet->setEnabled((int)datas[1] > 0);
    }

}
bool dialog_discharge::IsUse_Disc()
{
    return ((int)datas[0] > 0);
}
bool dialog_discharge::IsUse_1stDisc()
{
    return ((int)datas[1] > 0);
}

void dialog_discharge::onUse_Disc()
{
    QVector<HyRecipe::RECIPE_NUMBER> _vars;
    QVector<float> _datas;

    _vars.clear();
    _vars.append(HyRecipe::bDischarge);
    _vars.append(HyRecipe::bNGProd);

    _datas.clear();

    if(IsUse_Disc())
    {
        _datas.append(0.0); //bDischarge
        _datas.append(0.0); //bNGProd
    }
    else
    {
        _datas.append(1.0); //bDischarge
        _datas.append(0.0); //bNGProd
    }

    if(Recipe->Sets(_vars, _datas))
        Update();

}
void dialog_discharge::onUse_1stDisc()
{
    float _use;
    if(IsUse_1stDisc())
        _use = 0.0;
    else
        _use = 1.0;

    if(Recipe->Set(vars[1], _use))
        Update();
}
void dialog_discharge::onSet_1stDisc()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->lbSetCnt->text());
    np->m_numpad->SetNum(ui->btnSetCnt->text().toDouble());
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(9999);
    np->m_numpad->SetSosuNum(0);

    if(np->exec() != QDialog::Accepted)
        return;

    if(Recipe->Set(vars[2],(float)np->m_numpad->GetNumDouble()))
        Update();
}
void dialog_discharge::onCnt_1stDisc()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->lbCnt->text());
    np->m_numpad->SetNum(ui->btnCnt->text().toDouble());
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(ui->btnSetCnt->text().toDouble());
    np->m_numpad->SetSosuNum(0);

    if(np->exec() != QDialog::Accepted)
        return;

    if(Recipe->Set(vars[3],(float)np->m_numpad->GetNumDouble()))
        Update();
}
void dialog_discharge::onReset_1stDisc()
{
    if(Recipe->Set(vars[3], datas[2]))
        Update();
}
