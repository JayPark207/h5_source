#include "dialog_syncback.h"
#include "ui_dialog_syncback.h"

dialog_syncback::dialog_syncback(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_syncback)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->tbUsePic,SIGNAL(mouse_release()),this,SLOT(onUse()));
    connect(ui->tbUse,SIGNAL(mouse_press()),ui->tbUsePic,SLOT(press()));
    connect(ui->tbUse,SIGNAL(mouse_release()),ui->tbUsePic,SLOT(release()));

    m_vtValPic.clear();
    m_vtValPic.append(ui->tbValPic);
    m_vtValPic.append(ui->tbValPic_2);

    m_vtVal.clear();
    m_vtVal.append(ui->tbVal);
    m_vtVal.append(ui->tbVal_2);
    m_vtVal.append(ui->tbVal_3);

    m_vtValName.clear();
    m_vtValName.append(ui->tbValName);
    m_vtValName.append(ui->tbValName_2);
    m_vtValName.append(ui->tbValName_3);

    int i;
    for(i=0;i<m_vtValPic.count();i++)
    {
        connect(m_vtValPic[i],SIGNAL(mouse_release()),this,SLOT(onValue()));
        connect(m_vtVal[i],SIGNAL(mouse_press()),m_vtValPic[i],SLOT(press()));
        connect(m_vtVal[i],SIGNAL(mouse_release()),m_vtValPic[i],SLOT(release()));
    }

}

dialog_syncback::~dialog_syncback()
{
    delete ui;
}
void dialog_syncback::showEvent(QShowEvent *)
{
    Update();
}
void dialog_syncback::hideEvent(QHideEvent *)
{
}
void dialog_syncback::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_syncback::onClose()
{
    emit accept();
}

void dialog_syncback::Update()
{
    m_ItemName.clear();
    m_ItemName.append(tr("No Use"));
    m_ItemName.append(tr("Use"));

    vars.clear();
    vars.append(HyRecipe::mdSyncBack);
    vars.append(HyRecipe::vSyncLength);
    vars.append(HyRecipe::vSyncTime);
    vars.append(HyRecipe::vSyncSpeed);

    if(Recipe->Gets(vars, datas))
    {
        if((int)datas[0] < m_ItemName.size())
            ui->tbUse->setText(m_ItemName[(int)datas[0]]);

        QString str;
        str.sprintf(FORMAT_POS, datas[1]);
        ui->tbVal->setText(str);

        str.sprintf(FORMAT_TIME, datas[2]);
        ui->tbVal_2->setText(str);

        str.sprintf(FORMAT_SPEED, datas[3]);
        ui->tbVal_3->setText(str);

        ui->wigValue->setEnabled((int)datas[0] > 0);
    }
}

void dialog_syncback::onUse()
{
    dialog_mode_select* sel = (dialog_mode_select*)gGetDialog(DIG_MODE_SELECT);
    sel->InitTitle(ui->tbUseTitle->text());
    sel->InitRecipe((int)vars[0]);
    sel->InitNum(m_ItemName.size());
    for(int i=0;i<sel->GetNum();i++)
        sel->InitString(i,m_ItemName[i]);

    if(sel->exec() == QDialog::Accepted)
        Update();
}

void dialog_syncback::onValue()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtValPic.indexOf(sel);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(m_vtValName[index]->text());
    np->m_numpad->SetNum(m_vtVal[index]->text().toDouble());
    np->m_numpad->SetMinValue(0.0);

    if(index == 0)
    {
        np->m_numpad->SetMaxValue(100000);
        np->m_numpad->SetSosuNum(SOSU_POS);
    }
    else
    {
        np->m_numpad->SetMaxValue(1000);
        np->m_numpad->SetSosuNum(SOSU_TIME);
    }

    int vars_index = 1 + index;
    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(vars[vars_index],(float)np->m_numpad->GetNumDouble()))
        {
            Update();
            Update_CalSpeed();
        }
    }
}

void dialog_syncback::Update_CalSpeed()
{
    float length = datas[1];
    float time = datas[2];

    float speed;
    speed = length / time;

    Recipe->Set(HyRecipe::vSyncSpeed, speed);
    Update();
}

