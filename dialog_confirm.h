#ifndef DIALOG_CONFIRM_H
#define DIALOG_CONFIRM_H

#include <QDialog>


namespace Ui {
class dialog_confirm;
}

class dialog_confirm : public QDialog
{
    Q_OBJECT

public:
    enum DIAG_CONFIRM_COLOR
    {
        DEFAULT,
        RED,
        GREEN,
        SKYBLUE,
        GRAY,
        BLUE,
    };

public:
    explicit dialog_confirm(QWidget *parent = 0);
    ~dialog_confirm();

    void Title(QString title);
    void Message(QString msg1, QString msg2 = "");
    void SetColor(DIAG_CONFIRM_COLOR color);

private:
    Ui::dialog_confirm *ui;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_CONFIRM_H
