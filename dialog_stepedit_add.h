#ifndef DIALOG_STEPEDIT_ADD_H
#define DIALOG_STEPEDIT_ADD_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"
#include "dialog_keyboard.h"

namespace Ui {
class dialog_stepedit_add;
}

class dialog_stepedit_add : public QDialog
{
    Q_OBJECT
public:
    enum ADD_ITEM
    {
        ADD_POS=0,
        ADD_OUTPUT,
        ADD_INPUT,
        ADD_TIME,
        ADD_WORK,
    };


public:
    explicit dialog_stepedit_add(QWidget *parent = 0);
    ~dialog_stepedit_add();

    void Init(int CurrIndex);

    void Update();

    ADD_ITEM GetAddItem();
    bool IsBackPos();
    QString GetNickName();

public slots:
    void onAddItem();
    void onSelPos();
    void onAddName();

private:
    Ui::dialog_stepedit_add *ui;

    QVector<QLabel4*> m_vtPic;
    QVector<QLabel3*> m_vtName,m_vtIcon,m_vtSel;

    QVector<QLabel4*> m_vtPosPic;
    QVector<QLabel3*> m_vtPosIcon,m_vtPosLed;


    int m_nCurrIndex;
    ADD_ITEM m_nSelectedItem;
    bool m_bBackPos;    // false=at front,true= at back (default front)
    QString m_strNickName;

    void Select_AddItem(ADD_ITEM item);
    void Select_SelPos(bool bBackPos);
    void Display_Name(QString name);


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_STEPEDIT_ADD_H
