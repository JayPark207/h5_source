#include "hyledbuzzer.h"

HyLedBuzzer::HyLedBuzzer()
{
    m_Args.arg[0] = SET_LED_ONOFF;
    m_Args.arg[1] = 0xFF;
    m_Args.arg[2] = 0xFF;

    m_ArgsBuzz.arg[0] = SET_BUZZER;
    m_ArgsBuzz.arg[1] = 0;
    m_ArgsBuzz.arg[2] = 0;

}
HyLedBuzzer::~HyLedBuzzer()
{
    close(m_dev);
}

bool HyLedBuzzer::Initial()
{
    m_dev = open(DEVICE_FILENAME, O_RDWR|O_NDELAY);
    if(m_dev <= 0)
    {
        m_strErrorMsg = QString(tr("DTP7 Device Driver Open Fail!"));
        //qDebug() << m_strErrorMsg;
        return false;
    }
    else
    {
        Led_All(OFF);
        Buzzer(OFF);
    }

    return true;
}

void HyLedBuzzer::Led_All(bool onoff, unsigned char color)
{
    Led_R1(onoff, color);
    Led_R2(onoff, color);
    Led_R3(onoff, color);
    Led_L1(onoff, color);
    Led_L2(onoff, color);
    Led_L3(onoff, color);
}

void HyLedBuzzer::Led_R1(bool onoff, unsigned char color)
{
    m_Args.arg[0] = SET_LED_ONOFF;
    m_Args.arg[1] |= (0x03 <<(2*0));
    if(onoff)
        m_Args.arg[1] &= ~((color&0x03)<<(2*0));

    ioctl(m_dev,ETC_IOCTL_IO,&m_Args);
}
int HyLedBuzzer::getLed_R1()
{
    return (~(m_Args.arg[1] >> (2*0)) & 0x03);
}
bool HyLedBuzzer::IsLed_R1()
{
    if(getLed_R1() > LED_OFF)
        return true;
    return false;
}

void HyLedBuzzer::Led_R2(bool onoff, unsigned char color)
{
    m_Args.arg[0] = SET_LED_ONOFF;
    m_Args.arg[1] |= (0x03 <<(2*1));
    if(onoff)
        m_Args.arg[1] &= ~((color&0x03)<<(2*1));

    ioctl(m_dev,ETC_IOCTL_IO,&m_Args);
}
int HyLedBuzzer::getLed_R2()
{
    return (~(m_Args.arg[1] >> (2*1)) & 0x03);
}
bool HyLedBuzzer::IsLed_R2()
{
    if(getLed_R2() > LED_OFF)
        return true;
    return false;
}

void HyLedBuzzer::Led_R3(bool onoff, unsigned char color)
{
    m_Args.arg[0] = SET_LED_ONOFF;
    m_Args.arg[1] |= (0x03 <<(2*2));
    if(onoff)
        m_Args.arg[1] &= ~((color&0x03)<<(2*2));

    ioctl(m_dev,ETC_IOCTL_IO,&m_Args);
}
int HyLedBuzzer::getLed_R3()
{
    return (~(m_Args.arg[1] >> (2*2)) & 0x03);
}
bool HyLedBuzzer::IsLed_R3()
{
    if(getLed_R3() > LED_OFF)
        return true;
    return false;
}

void HyLedBuzzer::Led_L1(bool onoff, unsigned char color)
{
    m_Args.arg[0] = SET_LED_ONOFF;
    m_Args.arg[2] |= (0x03 <<(2*0));
    if(onoff)
        m_Args.arg[2] &= ~((color&0x03)<<(2*0));

    ioctl(m_dev,ETC_IOCTL_IO,&m_Args);
}
int HyLedBuzzer::getLed_L1()
{
    return (~(m_Args.arg[2] >> (2*0)) & 0x03);
}
bool HyLedBuzzer::IsLed_L1()
{
    if(getLed_L1() > LED_OFF)
        return true;
    return false;
}

void HyLedBuzzer::Led_L2(bool onoff, unsigned char color)
{
    m_Args.arg[0] = SET_LED_ONOFF;
    m_Args.arg[2] |= (0x03 <<(2*1));
    if(onoff)
        m_Args.arg[2] &= ~((color&0x03)<<(2*1));

    ioctl(m_dev,ETC_IOCTL_IO,&m_Args);
}
int HyLedBuzzer::getLed_L2()
{
    return (~(m_Args.arg[2] >> (2*1)) & 0x03);
}
bool HyLedBuzzer::IsLed_L2()
{
    if(getLed_L2() > LED_OFF)
        return true;
    return false;
}

void HyLedBuzzer::Led_L3(bool onoff, unsigned char color)
{
    m_Args.arg[0] = SET_LED_ONOFF;
    m_Args.arg[2] |= (0x03 <<(2*2));
    if(onoff)
        m_Args.arg[2] &= ~((color&0x03)<<(2*2));

    ioctl(m_dev,ETC_IOCTL_IO,&m_Args);
}
int HyLedBuzzer::getLed_L3()
{
    return (~(m_Args.arg[2] >> (2*2)) & 0x03);
}
bool HyLedBuzzer::IsLed_L3()
{
    if(getLed_L3() > LED_OFF)
        return true;
    return false;
}

void HyLedBuzzer::Buzzer(bool onoff)
{
    m_ArgsBuzz.arg[0] = SET_BUZZER;
    m_ArgsBuzz.arg[1] = (onoff & 0x01);
    m_ArgsBuzz.arg[2] = 0x00;
    ioctl(m_dev,ETC_IOCTL_IO,&m_ArgsBuzz);
}
bool HyLedBuzzer::IsBuzzer()
{
    if(m_ArgsBuzz.arg[1] > 0)
        return true;
    return false;
}
void HyLedBuzzer::Beep(int time_msec)
{
    Buzzer(ON);
    usleep(time_msec * 1000);
    Buzzer(OFF);
}
void HyLedBuzzer::Beep()
{
    Beep(20);
}

QString HyLedBuzzer::GetErrorMessage()
{
    return m_strErrorMsg;
}

/// new functions  ///

bool HyLedBuzzer::Led_L1(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_L1, color) < 0)
        return false;
    return true;
}
bool HyLedBuzzer::Led_L2(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_L2, color) < 0)
        return false;
    return true;
}
bool HyLedBuzzer::Led_L3(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_L3, color) < 0)
        return false;
    return true;
}

bool HyLedBuzzer::Led_R1(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_R1, color) < 0)
        return false;
    return true;
}
bool HyLedBuzzer::Led_R2(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_R2, color) < 0)
        return false;
    return true;
}
bool HyLedBuzzer::Led_R3(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_R3, color) < 0)
        return false;
    return true;
}


