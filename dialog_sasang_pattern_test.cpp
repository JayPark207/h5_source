#include "dialog_sasang_pattern_test.h"
#include "ui_dialog_sasang_pattern_test.h"

dialog_sasang_pattern_test::dialog_sasang_pattern_test(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::dialog_sasang_pattern_test)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtTbIcon.clear();m_vtTbNo.clear();m_vtTbPat.clear();
    m_vtTbIcon.append(ui->tbIcon);   m_vtTbNo.append(ui->tbNo);   m_vtTbPat.append(ui->tbPat);
    m_vtTbIcon.append(ui->tbIcon_2); m_vtTbNo.append(ui->tbNo_2); m_vtTbPat.append(ui->tbPat_2);
    m_vtTbIcon.append(ui->tbIcon_3); m_vtTbNo.append(ui->tbNo_3); m_vtTbPat.append(ui->tbPat_3);
    m_vtTbIcon.append(ui->tbIcon_4); m_vtTbNo.append(ui->tbNo_4); m_vtTbPat.append(ui->tbPat_4);
    m_vtTbIcon.append(ui->tbIcon_5); m_vtTbNo.append(ui->tbNo_5); m_vtTbPat.append(ui->tbPat_5);
    m_vtTbIcon.append(ui->tbIcon_6); m_vtTbNo.append(ui->tbNo_6); m_vtTbPat.append(ui->tbPat_6);
    m_vtTbIcon.append(ui->tbIcon_7); m_vtTbNo.append(ui->tbNo_7); m_vtTbPat.append(ui->tbPat_7);
    m_vtTbIcon.append(ui->tbIcon_8); m_vtTbNo.append(ui->tbNo_8); m_vtTbPat.append(ui->tbPat_8);
    m_vtTbIcon.append(ui->tbIcon_9); m_vtTbNo.append(ui->tbNo_9); m_vtTbPat.append(ui->tbPat_9);
    m_vtTbIcon.append(ui->tbIcon_10);m_vtTbNo.append(ui->tbNo_10);m_vtTbPat.append(ui->tbPat_10);
    m_vtTbIcon.append(ui->tbIcon_11);m_vtTbNo.append(ui->tbNo_11);m_vtTbPat.append(ui->tbPat_11);
    m_vtTbIcon.append(ui->tbIcon_12);m_vtTbNo.append(ui->tbNo_12);m_vtTbPat.append(ui->tbPat_12);
    m_vtTbIcon.append(ui->tbIcon_13);m_vtTbNo.append(ui->tbNo_13);m_vtTbPat.append(ui->tbPat_13);
    m_vtTbIcon.append(ui->tbIcon_14);m_vtTbNo.append(ui->tbNo_14);m_vtTbPat.append(ui->tbPat_14);
    m_vtTbIcon.append(ui->tbIcon_15);m_vtTbNo.append(ui->tbNo_15);m_vtTbPat.append(ui->tbPat_15);
    m_vtTbIcon.append(ui->tbIcon_16);m_vtTbNo.append(ui->tbNo_16);m_vtTbPat.append(ui->tbPat_16);
    m_vtTbIcon.append(ui->tbIcon_17);m_vtTbNo.append(ui->tbNo_17);m_vtTbPat.append(ui->tbPat_17);
    m_vtTbIcon.append(ui->tbIcon_18);m_vtTbNo.append(ui->tbNo_18);m_vtTbPat.append(ui->tbPat_18);

    connect(ui->btnRdyPic,SIGNAL(mouse_release()),this,SLOT(onReady()));
    connect(ui->btnRdy,SIGNAL(mouse_press()),ui->btnRdyPic,SLOT(press()));
    connect(ui->btnRdy,SIGNAL(mouse_release()),ui->btnRdyPic,SLOT(release()));
    connect(ui->btnRdyIcon,SIGNAL(mouse_press()),ui->btnRdyPic,SLOT(press()));
    connect(ui->btnRdyIcon,SIGNAL(mouse_release()),ui->btnRdyPic,SLOT(release()));

    connect(ui->btnStepFwdPic,SIGNAL(mouse_release()),this,SLOT(onStepFwd()));
    connect(ui->btnStepFwd,SIGNAL(mouse_press()),ui->btnStepFwdPic,SLOT(press()));
    connect(ui->btnStepFwd,SIGNAL(mouse_release()),ui->btnStepFwdPic,SLOT(release()));
    connect(ui->btnStepFwdIcon,SIGNAL(mouse_press()),ui->btnStepFwdPic,SLOT(press()));
    connect(ui->btnStepFwdIcon,SIGNAL(mouse_release()),ui->btnStepFwdPic,SLOT(release()));

    connect(ui->btnRunPic,SIGNAL(mouse_release()),this,SLOT(onRun()));
    connect(ui->btnRun,SIGNAL(mouse_press()),ui->btnRunPic,SLOT(press()));
    connect(ui->btnRun,SIGNAL(mouse_release()),ui->btnRunPic,SLOT(release()));
    connect(ui->btnRunIcon,SIGNAL(mouse_press()),ui->btnRunPic,SLOT(press()));
    connect(ui->btnRunIcon,SIGNAL(mouse_release()),ui->btnRunPic,SLOT(release()));

    connect(ui->btnPausePic,SIGNAL(mouse_release()),this,SLOT(onPause()));
    connect(ui->btnPause,SIGNAL(mouse_press()),ui->btnPausePic,SLOT(press()));
    connect(ui->btnPause,SIGNAL(mouse_release()),ui->btnPausePic,SLOT(release()));
    connect(ui->btnPauseIcon,SIGNAL(mouse_press()),ui->btnPausePic,SLOT(press()));
    connect(ui->btnPauseIcon,SIGNAL(mouse_release()),ui->btnPausePic,SLOT(release()));

    connect(ui->btnErrorPic,SIGNAL(mouse_release()),this,SLOT(onError()));
    connect(ui->btnErrorIcon,SIGNAL(mouse_press()),ui->btnErrorPic,SLOT(press()));
    connect(ui->btnErrorIcon,SIGNAL(mouse_release()),ui->btnErrorPic,SLOT(release()));

    connect(ui->btnJogPic,SIGNAL(mouse_release()),this,SLOT(onJog()));
    connect(ui->btnJogIcon,SIGNAL(mouse_press()),ui->btnJogPic,SLOT(press()));
    connect(ui->btnJogIcon,SIGNAL(mouse_release()),ui->btnJogPic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnSpdUpPic,SIGNAL(mouse_release()),this,SLOT(onSpdUp()));
    connect(ui->btnSpdUpIcon,SIGNAL(mouse_press()),ui->btnSpdUpPic,SLOT(press()));
    connect(ui->btnSpdUpIcon,SIGNAL(mouse_release()),ui->btnSpdUpPic,SLOT(release()));

    connect(ui->btnSpdDownPic,SIGNAL(mouse_release()),this,SLOT(onSpdDown()));
    connect(ui->btnSpdDownIcon,SIGNAL(mouse_press()),ui->btnSpdDownPic,SLOT(press()));
    connect(ui->btnSpdDownIcon,SIGNAL(mouse_release()),ui->btnSpdDownPic,SLOT(release()));

    // no use.
    ui->wigNA->setEnabled(false);

    // initial
    m_bReady = false;
    dig_error = 0;
    dig_jog = 0;
    dig_delaying = new dialog_delaying();
    pxmIcon.clear();

}

dialog_sasang_pattern_test::~dialog_sasang_pattern_test()
{
    delete ui;
}

void dialog_sasang_pattern_test::showEvent(QShowEvent *)
{
    Update();

    timer->start();

    m_TestSpeed = SSPTEST_SPEED_INIT;
    SetSpeed(m_TestSpeed);
}
void dialog_sasang_pattern_test::hideEvent(QHideEvent *)
{
    timer->stop();

    gReadyMacro(false);
    ResetProgram();

    ServoOnMode(SON_MODE_NORMAL);

    float main_speed = Param->Get(HyParam::MAIN_SPEED).toFloat();
    SetSpeed(main_speed);
}
void dialog_sasang_pattern_test::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_sasang_pattern_test::onClose()
{
    m_bReady = false;
    emit sigClose();
}

void dialog_sasang_pattern_test::onTimer()
{
    if(!m_bReady) return;

    CNRobo* pCon = CNRobo::getInstance();

    int _movestep=-1,_planstep=-1,_mainstep=0;
    pCon->getRunningStepIndex(MAIN_TASK, &_movestep, &_planstep);
    pCon->getRunningMainStepIndex(&_mainstep);
    m_nTaskStatus =  pCon->getTaskStatus(MAIN_TASK);

    m_nMainRunIndex = _mainstep - 1;
    m_nSubRunIndex = _movestep - 1;
    m_nSubPlanIndex = _planstep - 1;

    Redraw(m_nMainRunIndex, m_nTaskStatus);

    Control_Run(m_nTaskStatus);

    if(m_nTaskStatus == CNR_TASK_NOTUSED
    && m_bReady
    && !ui->wigReady->isEnabled())
    {
        Control_Ready(false);
    }

}

void dialog_sasang_pattern_test::Update()
{
    m_nStartIndex = 0;
    Redraw_List(m_nStartIndex);
    Display_Icon(-1, m_nTaskStatus); // icon clear

    Control_Ready(m_bReady);
}


void dialog_sasang_pattern_test::Redraw(int run_index, CNR_TASK_STATUS task_status)
{
    // run_index = 0~N
    if(run_index < 0) run_index = 0;

    // run_index match list view change
    if(run_index >= (m_nStartIndex + m_vtTbNo.size()))
    {
        //m_nStartIndex = run_index - (m_vtTbNo.size()-1); // each 1 line
        m_nStartIndex = run_index - (m_vtTbNo.size()/2); // each half line (must even)
        Redraw_List(m_nStartIndex);
    }
    else if(run_index < m_nStartIndex)
    {
        m_nStartIndex = run_index;
        Redraw_List(m_nStartIndex);
    }

    int table_index = run_index - m_nStartIndex;
    Display_Icon(table_index, task_status);
}

void dialog_sasang_pattern_test::Redraw_List(int start_index)
{
    int cal_size = Sasang->Pattern->GetCount() - start_index;
    int index;
    QString no,pat;

    for(int i=0;i<m_vtTbNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            Sasang->Pattern->GetName(index, pat);
            no = QString().setNum(index + 1);
        }
        else
        {
            // no data.
            pat.clear();
            no.clear();
        }

        m_vtTbNo[i]->setText(no);
        m_vtTbPat[i]->setText(pat);
    }

    // display list size.
    ui->lbListSize->setText(QString().setNum(Sasang->Pattern->GetCount()));

}

void dialog_sasang_pattern_test::Display_Icon(int table_index, CNR_TASK_STATUS status)
{
    if(pxmIcon.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon99-5.png");
        pxmIcon.append(QPixmap::fromImage(img)); // STOP
        img.load(":/icon/icon/icon99-3.png");
        pxmIcon.append(QPixmap::fromImage(img)); // PLAY
        img.load(":/icon/icon/icon99-4.png");
        pxmIcon.append(QPixmap::fromImage(img)); // PAUSE
    }

    for(int i=0;i<m_vtTbIcon.count();i++)
    {
        if(i == table_index)
            m_vtTbIcon[i]->setPixmap(pxmIcon[status]);
        else
            m_vtTbIcon[i]->setPixmap(0);
    }
}

void dialog_sasang_pattern_test::Control_Ready(bool bReady)
{
    ui->wigList->setEnabled(bReady);
    ui->wigButton->setEnabled(bReady);
    ui->wigReady->setEnabled(!bReady);

    if(bReady)
    {
        Delay(1000);
        ServoOnMode(SON_MODE_SPECIAL);
    }
    else
        ServoOnMode(SON_MODE_NORMAL);
}

void dialog_sasang_pattern_test::onReady()
{
    if(!m_bReady)
    {   
        if(!Sasang->Pattern->MakeTestProgram())
        {
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("Error")));
            msg->Message(tr("Test Run Ready Error!"),
                         tr("Please, Try again!"));
            msg->exec();
            return;
        }
    }

    gReadyMacro(true);
    LoadProgram();

    m_bReady = true;
    Control_Ready(m_bReady);

    SetSpeed(m_TestSpeed);
}

void dialog_sasang_pattern_test::onStepFwd()
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    QString prog_name = pCon->getCurProgramName(MAIN_TASK, true);
    qDebug() << prog_name;
    if(prog_name == Sasang->Pattern->GetName_TestMainMacro())
    {
        qDebug() << "Step > StepOver";
        ret = pCon->runNxtStepOver(MAIN_TASK);
        if(ret < 0)
        {
            qDebug() << "dialog_sasang_pattern_test::onStepFwd() runNxtStepOver ret = " << ret;
            return;
        }
    }
    else
    {
        qDebug() << "Step > Continue";
        ret = pCon->continueProgram2(MAIN_TASK, 4); // option4 : run to until clear stacked program.
        if(ret < 0)
        {
            qDebug() << "dialog_sasang_pattern_test::onStepFwd() continueProgram2(4) ret = " << ret;
            return;
        }
    }

    m_nRunType = 2;
}
void dialog_sasang_pattern_test::onRun()
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    qDebug() << "continueProgram2 option 0";
    ret = pCon->continueProgram2(MAIN_TASK, 0);
    if(ret < 0)
    {
        qDebug() << "dialog_sasang_pattern_test::onRun() continueProgram2(0) ret = " << ret;
        return;
    }

    m_nRunType = 1;
}
void dialog_sasang_pattern_test::onPause()
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->holdProgram(MAIN_TASK);
    if(ret < 0)
    {
        qDebug() << "dialog_sasang_pattern_test::onPause() holdProgram ret = " << ret;
        return;
    }
}

void dialog_sasang_pattern_test::onError()
{
    if(dig_error == 0)
        dig_error = new dialog_error();

    dig_error->exec();
}

void dialog_sasang_pattern_test::onJog()
{
    if(dig_jog == 0)
        dig_jog = new dialog_jog();

    dig_jog->exec();
    gReadyMacro(true);

    SetSpeed(m_TestSpeed);
}

void dialog_sasang_pattern_test::onReset()
{
    CNRobo* pCon = CNRobo::getInstance();
    pCon->resetEcatError();
    dig_delaying->Init(500);
    dig_delaying->exec();
    pCon->resetError();
    pCon->clearWarningCode();
}

bool dialog_sasang_pattern_test::LoadProgram()
{
    CNRobo* pCon = CNRobo::getInstance();

    QString program = Sasang->Pattern->GetName_TestMainMacro();
    int ret;

    /*float _main_speed = Param->Get(HyParam::MAIN_SPEED).toFloat();

    ret = pCon->setSpeed(_main_speed);
    if(ret < 0)
    {
        qDebug() << "dialog_sasang_pattern_test::LoadProgram() setSpeed ret = " << ret;
        return false;
    }*/

    ret = pCon->setCurProgram(MAIN_TASK, program, 1);
    if(ret < 0)
    {
        qDebug() << "dialog_sasang_patttern_test::LoadProgram() setCurProgram ret = " << ret;
        return false;
    }

    dig_delaying->Init(500);
    dig_delaying->exec();

    ret = pCon->resetCurProgram(MAIN_TASK);
    if(ret < 0)
    {
        qDebug() << "dialog_sasang_motion_test::LoadProgram() resetCurProgram ret = " << ret;
        return false;
    }

//    dig_delaying->Init(500);
//    dig_delaying->exec();

//    ret = pCon->resetCurProgram(MAIN_TASK);
//    if(ret < 0)
//    {
//        qDebug() << "dialog_sasang_motion_test::LoadProgram() resetCurProgram ret = " << ret;
//        return false;
//    }

    return true;
}
bool dialog_sasang_pattern_test::ResetProgram()
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

//    ret = pCon->resetCurProgram(MAIN_TASK);
//    if(ret < 0)
//    {
//        qDebug() << "dialog_sasang_motion_test::ResetProgram() resetCurProgram ret = " << ret;
//        return false;
//    }

    ret = pCon->clearCurProgram(MAIN_TASK);
    if(ret < 0)
    {
        qDebug() << "dialog_sasang_motion_test::ResetProgram() clearCurProgram ret = " << ret;
        return false;
    }

    return true;
}

void dialog_sasang_pattern_test::Control_Run(CNR_TASK_STATUS status)
{
    bool bRun = (status == CNR_TASK_RUNNING);

    ui->wigStepFwd->setEnabled(!bRun);
    ui->wigRun->setEnabled(!bRun);
    ui->wigPause->setEnabled(bRun);

    ui->wigError->setEnabled(!bRun);
    ui->wigJog->setEnabled(!bRun);
    ui->wigReset->setEnabled(!bRun);

    ui->grpEnd->setEnabled(!bRun);
}

bool dialog_sasang_pattern_test::ServoOnMode(int mode)
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->setServoOnMode(mode);
    if(ret < 0)
    {
        qDebug() << "setServoOnMode ret=" << ret;
        return false;
    }

    return true;
}

bool dialog_sasang_pattern_test::SetSpeed(float speed)
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->setSpeed(speed);
    if(ret < 0)
    {
        qDebug() << "dialog_sasang_pattern_test::setSpeed() ret=" << ret;
        return false;
    }

    m_TestSpeed = speed;
    ui->pbarSpeed->setValue(m_TestSpeed);
    return true;
}

void dialog_sasang_pattern_test::onSpdUp()
{
    if(m_TestSpeed > SSPTEST_SPEED_MAX)
        return;

    float speed = m_TestSpeed + SSPTEST_SPEED_GAP;
    SetSpeed(speed);
}
void dialog_sasang_pattern_test::onSpdDown()
{
    if(m_TestSpeed <= SSPTEST_SPEED_MIN)
        return;

    float speed = m_TestSpeed - SSPTEST_SPEED_GAP;
    SetSpeed(speed);
}

void dialog_sasang_pattern_test::Delay(int msec)
{
    if(dig_delaying == 0)
        dig_delaying = new dialog_delaying();
    dig_delaying->Init(msec);
    dig_delaying->exec();
}
