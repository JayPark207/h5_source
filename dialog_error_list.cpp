#include "dialog_error_list.h"
#include "ui_dialog_error_list.h"

dialog_error_list::dialog_error_list(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_error_list)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    m_vtNo.clear();m_vtCode.clear();m_vtName.clear();
    m_vtNo.append(ui->tbNo);   m_vtCode.append(ui->tbCode);   m_vtName.append(ui->tbName);
    m_vtNo.append(ui->tbNo_2); m_vtCode.append(ui->tbCode_2); m_vtName.append(ui->tbName_2);
    m_vtNo.append(ui->tbNo_3); m_vtCode.append(ui->tbCode_3); m_vtName.append(ui->tbName_3);
    m_vtNo.append(ui->tbNo_4); m_vtCode.append(ui->tbCode_4); m_vtName.append(ui->tbName_4);
    m_vtNo.append(ui->tbNo_5); m_vtCode.append(ui->tbCode_5); m_vtName.append(ui->tbName_5);
    m_vtNo.append(ui->tbNo_6); m_vtCode.append(ui->tbCode_6); m_vtName.append(ui->tbName_6);
    m_vtNo.append(ui->tbNo_7); m_vtCode.append(ui->tbCode_7); m_vtName.append(ui->tbName_7);
    m_vtNo.append(ui->tbNo_8); m_vtCode.append(ui->tbCode_8); m_vtName.append(ui->tbName_8);
    m_vtNo.append(ui->tbNo_9); m_vtCode.append(ui->tbCode_9); m_vtName.append(ui->tbName_9);
    m_vtNo.append(ui->tbNo_10);m_vtCode.append(ui->tbCode_10);m_vtName.append(ui->tbName_10);


    int i;
    for(i=0;i<m_vtNo.count();i++)
    {
        connect(m_vtNo[i],SIGNAL(mouse_release()),this,SLOT(onClose()));
        connect(m_vtCode[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
        connect(m_vtName[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
    }

}

dialog_error_list::~dialog_error_list()
{
    delete ui;
}
void dialog_error_list::showEvent(QShowEvent *)
{
    Update();
}
void dialog_error_list::hideEvent(QHideEvent *)
{

}
void dialog_error_list::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_error_list::onClose()
{
    emit accept();
}

void dialog_error_list::Update()
{
    int _code;
    QString code,name;

    for(int i=0;i<m_vtNo.count();i++)
    {
        _code = Error->GetCode(i);

        if(_code == 0)
        {
            code.clear();
            name.clear();
        }
        else
        {
            code = QString().setNum(_code);
            name = Error->GetName(_code);
            if(name == "N/A")
            {
                name.clear();
                CNRobo* pCon = CNRobo::getInstance();
                if(pCon->getErrorCodeInfo(_code, &name) < 0)
                    name = "N/A";
            }
        }

        m_vtNo[i]->setText(QString().setNum(i+1));
        m_vtCode[i]->setText(code);
        m_vtName[i]->setText(name);
    }
}
