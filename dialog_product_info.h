#ifndef DIALOG_PRODUCT_INFO_H
#define DIALOG_PRODUCT_INFO_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_product_info;
}

class dialog_product_info : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_product_info(QWidget *parent = 0);
    ~dialog_product_info();

    void Update();       // in showevent
    void Refresh_Data(); // in timer cyclic update.

private slots:
    void onTimer();

    void onTarget();
    void onCavity();

    void onCntHold();
    void onClear();

private:
    Ui::dialog_product_info *ui;
    QTimer* timer;

    QVector<float> datas;
    QVector<HyRecipe::RECIPE_NUMBER> vars;

    void Display_CountHold(bool bHold);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_PRODUCT_INFO_H
