#ifndef QLABEL2_H
#define QLABEL2_H

#include <QLabel>
#include <QVector>
#include <QMouseEvent>

#include "global.h"

class QLabel2 : public QLabel
{
    Q_OBJECT
public:
    explicit QLabel2(QWidget *parent=0):QLabel(parent){}

signals:
    void mouse_press();
    void mouse_release();
    
public slots:

protected:
    void mousePressEvent(QMouseEvent *)
    {
        // need to test
        if(this->hasScaledContents())
        {
            g_vtPxmLabel2.append(*this->pixmap());

            QImage img;
            img.load(":/image/image/empty2.png");
            this->setPixmap(QPixmap::fromImage(img));
        }

        emit mouse_press();
    }

    void mouseReleaseEvent(QMouseEvent *)
    {
        // need to test
        if(this->hasScaledContents())
        {
            this->setPixmap(g_vtPxmLabel2.last());
            g_vtPxmLabel2.pop_back();
        }

        emit mouse_release();
    }

private:
    
};

#endif // QLABEL2_H
