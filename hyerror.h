#ifndef HYERROR_H
#define HYERROR_H

/** 코어서버에 에러코드구조를 기본으로
    에러리스트를 Init() 에 적어줌으로 코드와 내용을 매칭한다.
**/

#include <QObject>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QDateTime>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyparam.h"

#define MAX_ERROR_BUFFER    10

class HyError : public QObject
{
    Q_OBJECT

public:
    explicit HyError(HyParam* param);

    void Init();
    QString GetName(int err_code);
    QString GetDetail(int err_code);

    void Error(int err_code);
    int GetCode(int index); // 0~MAX_ERROR_BUFFER-1

    void Sync(); // hyparam data read & save(sync)
    void Write();// hyparam data write.

    // file name : /mnt/mtd5/hy_errorcode
    // file format: code,name,detail
    bool Load_ErrorCode();

private:
    void Set(int code, QString name, QString detail);   // register error code.
    // code & error match index. so, search code index, access error data this index.
    QVector<int>    m_vtCode;       // for search
    QStringList     m_Name;      // for data buffer (for access)
    QStringList     m_Detail;

    QVector<int>    m_vtError;      // Occured Error Buffer.

    HyParam* m_Param;


};

#endif // HYERROR_H
