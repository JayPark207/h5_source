#include "view_param.h"
#include "ui_view_param.h"

view_param::view_param(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::view_param)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onSave()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(reject()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_Header.clear();
    m_Header.append(tr("Name"));
    m_Header.append(tr("Value"));

    ui->tbParam->setColumnCount(m_Header.count());
    ui->tbParam->setHorizontalHeaderLabels(m_Header);
    ui->tbParam->setSortingEnabled(false);
#ifdef _CG3300_
    ui->tbParam->setColumnWidth(0, 280);
    ui->tbParam->setColumnWidth(1, 100);
#elif _CG4300_
    ui->tbParam->setColumnWidth(0, 280);
    ui->tbParam->setColumnWidth(1, 100);
#endif
    ui->tbParam->setAlternatingRowColors(false);

    connect(ui->btnEditPic,SIGNAL(mouse_release()),this,SLOT(onEdit()));
    connect(ui->btnEdit,SIGNAL(mouse_press()),ui->btnEditPic,SLOT(press()));
    connect(ui->btnEdit,SIGNAL(mouse_release()),ui->btnEditPic,SLOT(release()));
    connect(ui->btnEditIcon,SIGNAL(mouse_press()),ui->btnEditPic,SLOT(press()));
    connect(ui->btnEditIcon,SIGNAL(mouse_release()),ui->btnEditPic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnReset,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnReset,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));


    //for test
    connect(ui->btnTestPic,SIGNAL(mouse_release()),this,SLOT(onTest()));

}

view_param::~view_param()
{
    delete ui;
}
void view_param::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void view_param::showEvent(QShowEvent *)
{
    kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);

    Update();
}

void view_param::hideEvent(QHideEvent *){}
void view_param::onSave()
{
    Param->Write();
    accept();
}

void view_param::Update()
{
    QTableWidget* tb = ui->tbParam;
    tb->clearContents();

    if(!Param->Read())
        return;

    tb->setRowCount(Param->m_Param.count());

    for(int i=0;i<Param->m_Param.count();i++)
    {
        QTableWidgetItem* col1 = tb->item(i,0);
        QTableWidgetItem* col2 = tb->item(i,1);
        if(!col1)
        {
            col1 = new QTableWidgetItem("");
            tb->setItem(i,0,col1);
        }
        if(!col2)
        {
            col2 = new QTableWidgetItem("");
            tb->setItem(i,1,col2);
        }

        tb->item(i,0)->setText(Param->GetName((HyParam::PARAMS)i));
        tb->item(i,1)->setText(Param->Get((HyParam::PARAMS)i));
    }

}

void view_param::onEdit()
{
    int row_index = ui->tbParam->currentRow();
    if(row_index >= Param->m_Param.count())
        return;
    if(row_index < 0)
        return;

    kb->m_kb->SetTitle(Param->GetName((HyParam::PARAMS)row_index));
    kb->m_kb->SetText(Param->Get((HyParam::PARAMS)row_index));
    if(kb->exec() == QDialog::Accepted)
    {
        Param->Set((HyParam::PARAMS)row_index, kb->m_kb->GetText(), false);
        ui->tbParam->item(row_index, 1)->setText(Param->Get((HyParam::PARAMS)row_index));
    }

}
void view_param::onReset()
{
    Update();
}


void view_param::onTest()
{
    CNRobo* pCon = CNRobo::getInstance();
    int cycle_time = pCon->getCommandCycleTime();
    qDebug() << "Cycle Time =" << cycle_time;

    pCon->rebootSystem();
}
