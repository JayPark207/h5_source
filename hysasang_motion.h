#ifndef HYSASANG_MOTION_H
#define HYSASANG_MOTION_H

#include <QObject>
#include <QMetaEnum>
#include <QDebug>
#include <QElapsedTimer>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hypos.h"
#include "hyoutput.h"

struct ST_SSMOTION_DATA
{
    // parsing data
    QString command;
    cn_trans trans[2];
    cn_joint joint[2];
    float speed_output;     // speed or output num.
    float accuracy_input;   // accuracy or input num.
    float type;
    float time;

    // raw data
    QString raw;            // raw data (== ".dat" 1 string line)
    QStringList program;
};

struct ST_SSSHIFT_VALUE
{
    float x;
    float y;
    float z;
    float r;
    float s;
    float j6;
};

class HySasang_Motion : public QObject
{
    Q_GADGET
    Q_ENUMS(COMMAND)

/** variables **/
public:
    enum COMMAND
    {
        p2p=0,
        con,
        cir, // cir,middle_data,,,@end_data,,,
        out,
        wtt,
        win,

        COMMAND_NUM,
    };

    QList<ST_SSMOTION_DATA> MotionList;

private:
    QStringList Command_UiName;
    QStringList Command_Name;
    QString Command_UiName_Start;
    QString Command_UiName_End;

    HyPos* m_Posi;

    ST_SSMOTION_DATA m_DefaultData;

/** functions **/
public:
    HySasang_Motion();

    // initial
    void Init_String();
    QString GetName_TestSubMacro()  {return ("ssmot_");}
    QString GetName_TestMainMacro() {return ("motion_main");}

    // access list data & making data.
    bool Default(ST_SSMOTION_DATA& data);
    bool SetDefault(ST_SSMOTION_DATA data); // Set m_DefaultData
    bool Set(int index, ST_SSMOTION_DATA& data);
    bool Get(int index, ST_SSMOTION_DATA& data);
    QString GetName(int index); // for ui (list command -> ui name)
    int GetCount();
    void ClearList();
    void NewList(); // add default start & end
    int IndexOf(QString command);

    // edit.
    bool Add(ST_SSMOTION_DATA& data, int index = -1); // -1 rare postion. checking command
    bool Append(ST_SSMOTION_DATA& data);
    bool Del(int index);
    bool Up(int index);     // select data list index up. using QList move().
    bool Down(int index);   // select data list index down. using QList move().

    // test run
    bool MakeTestProgram();  // main & sub macro
    int MakeTestProgram(QList<ST_SSMOTION_DATA>& comp_motion_list); // -1 = error, -2 = differ list size. 0= ok

    // utility functions.
    bool Data2Raw(ST_SSMOTION_DATA& data); // data -> raw 1 line QString inside struct.
    bool Raw2Data(ST_SSMOTION_DATA& data); // raw -> data
    bool Data2Prog(ST_SSMOTION_DATA& data); // data -> program(memory)
    bool Prog2Macro(QStringList prog, int list_index); // program(memory) -> macro file.
    bool Raw2Prog(QString raw, QStringList& prog); // using in pattern class
    bool MakeTestMain();
    int  DataShift(ST_SSMOTION_DATA& data, ST_SSSHIFT_VALUE shift); // -1 =err, 0=continue, 1=ok
    int  SetSpeed(ST_SSMOTION_DATA& data, float speed); // only move command
    int  SetAccuracy(ST_SSMOTION_DATA& data, float accuracy); // only move command
    bool IsMoveCommand(QString command);

    QString toStr(COMMAND cmd);
    bool IsSameData(ST_SSMOTION_DATA& data1, ST_SSMOTION_DATA& data2); // true=same, false=different
    bool IsSameRaw(ST_SSMOTION_DATA& data1, ST_SSMOTION_DATA& data2);
    bool IsSameProg(ST_SSMOTION_DATA& data1, ST_SSMOTION_DATA& data2);

private: // transfer program. (seperate per command)
    bool Prog_p2p(ST_SSMOTION_DATA& data);
    bool Prog_con(ST_SSMOTION_DATA& data);
    bool Prog_cir(ST_SSMOTION_DATA& data);
    bool Prog_out(ST_SSMOTION_DATA& data);
    bool Prog_out2(ST_SSMOTION_DATA& data);
    bool Prog_wtt(ST_SSMOTION_DATA& data);

    bool Prog_p2p_J(ST_SSMOTION_DATA& data);
    bool Prog_con_J(ST_SSMOTION_DATA& data);
    bool Prog_cir_J(ST_SSMOTION_DATA& data);

};

#endif // HYSASANG_MOTION_H
