#ifndef DIALOG_MANUAL_H
#define DIALOG_MANUAL_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "widget_jogmodule4.h"
#include "widget_jogmodule4_p.h"
#include "dialog_io.h"
#include "dialog_motor.h"

#include "top_sub.h"

namespace Ui {
class dialog_manual;
}

class dialog_manual : public QDialog
{
    Q_OBJECT

private:
    enum ENUM_POS_SEL
    {
        POS_HY = 0,
        POS_JOINT,
        POS_TRANS,
    };

public:
    explicit dialog_manual(QWidget *parent = 0);
    ~dialog_manual();

    void Update();

public slots:
    void onClose();
    void onTimer();

    void onPosSel();

    void onIOView();
    void onIUp();
    void onIDown();
    void onOUp();
    void onODown();

    void onOutBtnUp();
    void onOutBtnDown();
    void onOutBtn();

    void onMotorPage();


private: // functions

    void MatchingOutput();

    // position
    void Display_PosSel_Axis(ENUM_POS_SEL pos_sel);
    void Display_PosSel_Button(ENUM_POS_SEL pos_sel);
    void Display_PosSel_Curr(ENUM_POS_SEL pos_sel, float* trans, float* joint);

    // io
    void Display_InputPage(int page_index);
    void Display_OutputPage(int page_index);
    void Display_Input(int page_index);
    void Display_Output(int page_index);

    // contoled output.
    void Display_OutBtnPage(int page_index);
    void Display_OutBtn();

private:
    Ui::dialog_manual *ui;
    QTimer* timer;
#ifdef _H6_
    //widget_jogmodule4_p* wigJog;
    widget_jogmodule4* wigJog;
#else
    widget_jogmodule4* wigJog;
#endif
    dialog_io* dig_io;
    dialog_motor* dig_motor;

    // position info.
    QVector<QLabel4*> m_vtPosSelPic;
    QVector<QLabel3*> m_vtPosSel;

    QVector<QLabel3*> m_vtAxis;
    QVector<QLabel3*> m_vtCurr;

    ENUM_POS_SEL m_nPosSel;

    // io info.
    QVector<QLabel3*> m_vtInIcon;
    QVector<QLabel3*> m_vtInSym;

    QVector<QLabel3*> m_vtOutIcon;
    QVector<QLabel3*> m_vtOutSym;

    int m_nMaxInputPage;
    int m_nMaxOutputPage;
    int m_nInputPageIndex;
    int m_nOutputPageIndex;
    QVector<QPixmap> m_pxmIn, m_pxmOut;


    // controled output info.
    QVector<QFrame*> m_vtOutputPage;

    QVector<int> m_vtOutputNum;
    QVector<QLabel4*> m_vtOutputPic;
    QVector<QLabel3*> m_vtOutputIcon;

    int m_nOutBtnPageIndex;

    top_sub* top;
    void SubTop();          // Need SubTop

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_MANUAL_H
