#ifndef FORM_STEPEDIT_USEROUTPUT_H
#define FORM_STEPEDIT_USEROUTPUT_H

#include <QWidget>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_message.h"

#define USEROUT_EXTEND 7 // buffer index

namespace Ui {
class form_stepedit_useroutput;
}

class form_stepedit_useroutput : public QWidget
{
    Q_OBJECT

public:
    explicit form_stepedit_useroutput(QWidget *parent = 0);
    ~form_stepedit_useroutput();

    void Init(int index, QString title, int buf_index);

    void Update();

    void MatchingOutput();               // initial.


signals:
    void sigClose();

public slots:
    void onOK();
    void onCancel();
    void onOutput();
    void onDelay();
    void onType();
    void onPulse();

private:
    Ui::form_stepedit_useroutput *ui;
    dialog_numpad* np;

    int m_nSelectedIndex;
    int m_nBufIndex;
    QString m_strTitle;

    QVector<QLabel3*>   m_vtIcon;
    QVector<QLabel3*>   m_vtSel;
    QVector<QLabel4*>   m_vtPic;     // m_vtOutputNum 1:1 matching.
    QVector<int>        m_vtOutputNum;  // 1~128. (중복안됨)

    ST_USERDATA_OUTPUT m_Data;

    void DisplayOutput();
    void SetOutput(int index);
    void ResetOutput(int index);

    void DisplayDelay();    // delay time.
    void SetType(HyUserData::ENUM_TYPE type);     // on,off,pulse
    void DisplayPulse();    // pulse time.

    QVector<QLabel4*> m_vtTypePic;
    QVector<QLabel3*> m_vtTypeIcon;

    QString m_strSec;
    void Init_String();

    bool Save();

    // for extend
    void Init_Extend(ST_USERDATA_OUTPUT& data);
    void Set_Extend(int extend_output_num);
    bool IsOK_Extend(int output_num);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // FORM_STEPEDIT_USEROUTPUT_H
