#include "page_main2_special.h"
#include "ui_page_main2_special.h"

page_main2_special::page_main2_special(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::page_main2_special)
{
    ui->setupUi(this);

    connect(ui->btnDeburrPic,SIGNAL(mouse_release()),this,SLOT(onDeburr()));
    connect(ui->btnDeburrIcon,SIGNAL(mouse_press()),ui->btnDeburrPic,SLOT(press()));
    connect(ui->btnDeburrIcon,SIGNAL(mouse_release()),ui->btnDeburrPic,SLOT(release()));

    connect(ui->btnTaskPic,SIGNAL(mouse_release()),this,SLOT(onTask()));
    connect(ui->btnTaskIcon,SIGNAL(mouse_press()),ui->btnTaskPic,SLOT(press()));
    connect(ui->btnTaskIcon,SIGNAL(mouse_release()),ui->btnTaskPic,SLOT(release()));

    connect(ui->btnTestPic,SIGNAL(mouse_release()),this,SLOT(onTest()));

    // initial
    dig_sasang_group = 0;

    ui->btnTest->setVisible(false);
    ui->btnTestPic->setVisible(false);

    ui->btnTaskPic->setVisible(false);
    ui->btnTask->setVisible(false);
    ui->btnTaskIcon->setVisible(false);

}

page_main2_special::~page_main2_special()
{
    delete ui;
}

void page_main2_special::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void page_main2_special::showEvent(QShowEvent *)
{
    UserLevel();
}

void page_main2_special::hideEvent(QHideEvent *){}

void page_main2_special::UserLevel()
{
    int user_level = Param->Get(HyParam::USER_LEVEL).toInt();
    if(user_level < USER_LEVEL_HIGH)
    {
        // level 0 or 3

        if(user_level < USER_LEVEL_MID)
        {
            // level 0
            ui->btnDeburrPic->setEnabled(false);
            ui->btnDeburr->setEnabled(false);
            ui->btnDeburrIcon->setEnabled(false);
        }
        else
        {
            // level 3
            ui->btnDeburrPic->setEnabled(true);
            ui->btnDeburr->setEnabled(true);
            ui->btnDeburrIcon->setEnabled(true);
        }
    }
    else
    {
        // level 7
        ui->btnDeburrPic->setEnabled(true);
        ui->btnDeburr->setEnabled(true);
        ui->btnDeburrIcon->setEnabled(true);
    }
}


void page_main2_special::onDeburr()
{
    if(dig_sasang_group == 0)
        dig_sasang_group = new dialog_sasang_group();

    dig_sasang_group->exec();
}

void page_main2_special::onTask()
{
    dialog_task_control* dig = new dialog_task_control();
    dig->exec();
}

void page_main2_special::onTest()
{
    /*// multi-turn reset test
    Motors->Ready_MultiTurnClear(0);
    dialog_delaying* dig = new dialog_delaying();
    dig->Init(500);
    dig->exec();
    Motors->Set_MultiTurnClear(0);
    dig->exec();
    Motors->Reset_MultiTurnClear(0);*/

    /*int data;
    data = 98;
    if(Motors->Set_OverloadLevel(0,data))
        qDebug() << "true set value = " << data;
    else
        qDebug() << "false set value = " << data;

    if(Motors->Get_OverloadLevel(0,data))
    {
        qDebug() << "true return value = " << data;
    }
    else
    {
        qDebug() << "false return value = " << data;
    }

    Motors->Save_EEPROM(0);
    qDebug() << "eeprom save";*/

}
