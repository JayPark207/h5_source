#ifndef DIALOG_MOTOR_TEST_H
#define DIALOG_MOTOR_TEST_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"
#include "widget_jogmodule3.h"
#include "dialog_numpad.h"
#include "top_sub.h"
#include "dialog_delaying.h"

namespace Ui {
class dialog_motor_test;
}

class dialog_motor_test : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_motor_test(QWidget *parent = 0);
    ~dialog_motor_test();


private:
    void Update();

    void Write_Recipe();    // all write
    void Write_Recipe_Dist();
    void Write_Recipe_Speed();
    void Write_Recipe_Accel();
    void Write_Recipe_Decel();
    void Write_Recipe_Interval();

    void Read_Param_Data();
    void Save_Param_Data();

    void Redraw_Display();

    top_sub* top;
    void SubTop();          // Need SubTop

public slots:
    void onClose();
    void onServo();
    void onReset();

    void onDist();
    void onSpd();
    void onAcc();
    void onDec();
    void onInterval();

    void onSel();
    void onReal();
    void onTimer();

    void onMove();
    void onContinue();
    void onMoveMode();

private:
    Ui::dialog_motor_test *ui;
    widget_jogmodule3* wmJog;
    QTimer* timer;

    QVector<QLabel3*> m_vtSel, m_vtSelIcon;
    QVector<QLabel3*> m_vtJNo;
    QVector<QLabel3*> m_vtType;
    QVector<QLabel3*> m_vtDist;
    QVector<QLabel4*> m_vtDistPic;
    QVector<QLabel3*> m_vtReal;

    // important variable.
    cn_joint m_Dist;
    float m_Speed,m_Interval,m_Accel,m_Decel;
    QStringList m_Types;
    QStringList m_PLimits, m_MLimits;

    QVector<QPixmap> pxmServo;
    void SetServo(bool onoff);


    void SetMoving(bool onoff);
    bool m_bMoving;
    QVector<QPixmap> pxmMove;

    void SetContinue(bool on);

    void SetMoveMode(bool on);

    void Delay(int msec);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_MOTOR_TEST_H
