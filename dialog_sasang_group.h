#ifndef DIALOG_SASANG_GROUP_H
#define DIALOG_SASANG_GROUP_H

#include <QDialog>
#include <QStackedWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_sasang_pattern.h"

#include "dialog_message.h"
#include "dialog_confirm.h"
#include "dialog_keyboard.h"

#include "top_sub.h"

namespace Ui {
class dialog_sasang_group;
}

class dialog_sasang_group : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_sasang_group(QWidget *parent = 0);
    ~dialog_sasang_group();

    void Update();

private:
    Ui::dialog_sasang_group *ui;

    QVector<QLabel3*> m_vtTbNo,m_vtTbGrp,m_vtTbDate;

    int m_nSelectedIndex;
    int m_nStartIndex;
    void Redraw_List(int start_index);      // boss
    void Display_ListOn(int table_index);   // boss

    dialog_sasang_pattern* dig_sasang_pattern;

    top_sub* top;
    void SubTop();          // Need SubTop

public slots:
    void onClose();

    void onList();
    void onUp();
    void onDown();

    void onInside();
    void onNew();
    void onCopy();
    void onRename();
    void onDelete();



protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // DIALOG_SASANG_GROUP_H
