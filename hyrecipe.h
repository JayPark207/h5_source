/* 금형데이터(main program & setting data) 클래스. */

#ifndef HYRECIPE_H
#define HYRECIPE_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QVariant>
#include <QFile>
#include <QVector>
#include <QMetaEnum>
#include <QDebug>
#include <QElapsedTimer>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

/** Recipe 클래스는 아무상속없는 순수 클래스여야 한다.**/
class HyRecipe
{
    Q_GADGET
    Q_ENUMS(RECIPE_JOINT)
    Q_ENUMS(RECIPE_TRANS)
    Q_ENUMS(RECIPE_NUMBER)
    Q_ENUMS(RECIPE_STRING)

public:
    HyRecipe();

public:

    // enum 에 선언한 것이 곧 변수명 이면서 인덱스가 되도록 함.
    // 현재 max. 15글자까지만 됨.

    enum RECIPE_JOINT
    {
        j_data1,
        jTuneDist,
        jTarget,

        j_num
    };

    enum RECIPE_TRANS
    {
        t_data1,
        tTarget,

        tTest1,
        tTest2,
        tTest3,

        t_num
    };

    enum RECIPE_NUMBER
    {
        n_data1, // for test

        SeqVer,

        // mode part
        // common
        mdUseArm,           // 0: Product Arm, 1: Product + Runner Arm 2: Runner Arm.
        mdProdTakeout,      // 0: moving axis, 1: fix axis
        mdRotation,         // 0: before travel, 1: traveling, 2: after travel, 3: no use. => 0: after move, 1:moving
        mdSwivel,           // 0: no use, 1: in mold, 2: after travel, 3: 1+2.
        mdInsert,           // 0: no, 1: use.
        mdExternWait,       // 0: no, 1: use.
        mdMoldOpening,      // 0: no, 1: use.
        mdMoldClean,        // 0: no, 1: use.
        vCleanCycle,        // N : one execution per N cylcle
        tCleanTime,         // on time [sec]
        tCleanDelay,        // before on delay time [sec]
        mdCloseWait,        // new 0: no, 1: use

        // takeout 0:no use, outnum: takeout&unload use, -outnum: takeout use, unload not use.
        mdToutVac1,         // 0: no, outnum: use, -outnum:unload not use. (Takeout Method) - out number
        mdToutVac2,         // 0: no, outnum: use, -outnum:unload not use. (Takeout Method)
        mdToutVac3,         // 0: no, outnum: use, -outnum:unload not use. (Takeout Method)
        mdToutVac4,         // 0: no, outnum: use, -outnum:unload not use. (Takeout Method)
        mdToutChuck,        // 0: no, outnum: use, -outnum:unload not use. (Takeout Method)
        mdToutGrip,         // 0: no, outnum: use, -outnum:unload not use. (Takeout Method)
        mdToutRunGrip,      // 0: no, outnum: use, -outnum:unload not use. (Takeout Method)
        mdToutUser1,        // 0: no, outnum: use, -outnum:unload not use. (Takeout Method) - new add
        mdToutUser2,        // 0: no, outnum: use, -outnum:unload not use. (Takeout Method) - new add
        mdToutUser3,        // 0: no, outnum: use, -outnum:unload not use. (Takeout Method) - new add
        mdToutUser4,        // 0: no, outnum: use, -outnum:unload not use. (Takeout Method) - new add

        // (new) takeout output timing 0:defalt 1:start takeout 2:arrive takeout 3: .....
        mdToutTiming,       // 0: no (defalut), 1: use (depend on data)
        tiToutVac1,         // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutVac2,         // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutVac3,         // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutVac4,         // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutChuck,        // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutGrip,         // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutRunGrip,      // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutUser1,        // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutUser2,        // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutUser3,        // 0:before takeout 1:during takeout 2: after takeout 3:...
        tiToutUser4,        // 0:before takeout 1:during takeout 2: after takeout 3:...

        mdJMotion,          // 0: no, 1: use.
        mdUndercut,         // 0: no, 1~3: undercut number.
        mdSoftTorque,       // 0: no, 1: use.

        vJSDist1,           // j-motion straight distance [mm]
        vJSDist2,           // j-motion straight distance [mm]
        vJRate1,            // j-motion j-rate value [%]
        vJRate2,            // j-motion j-rate value [%]

        mdIStopTErr,        // I-Stop on takeout error. 0: no, 1: use.

        // unload
        mdProdUnload,       // 0: normal, 1: in mold, 2: array, 3: Sequential, 4: 2+3
        mdRunUnload,        // 0: normal, 1: during Traverse, 2: during return, 3: in mold
        mdConv,             // 0: no, 1: use.
        tConvTime,          // [sec]
        mdGrabComp,         // 0: no, 1: use.
        tGrabCompTime,      // [sec]
        mdUnloadMidPos,     // new, 0: no, 1: use.
        mdDualUnload,       // new, 0: no, 1: use.
        vDualRevX,          // new, 1: no, -1: reverse
        vDualRevY,          // new, 1: no, -1: reverse
        vDualRevZ,          // new, 1: no, -1: reverse
        vDualCurr,          // new, 0: master, 1: slave
        mdUnloadBack,       // new, 0: no, 1: use.
        vUnloadBackX,       // new, Unload Back X-Value [mm]
        vUnloadBackY,       // new, Unload Back Y-Value [mm]
        vUnloadBackZ,       // new, Unload Back Z-Value [mm]
        vUnloadBackSpd,     // new, Unload Back Speed [%]
        vUnloadBackDly,     // new, Unload Back Delay [sec]

        vArrXNum,           // array x(traverse) number.
        vArrXPitch,         // array x(traverse) pitch [mm].
        vArrXOrder,         // array x(traverse) Order 0~2.
        vArrYNum,           // array y(fwdbwd) number.
        vArrYPitch,         // array y(fwdbwd) pitch [mm].
        vArrYOrder,         // array y(fwdbwd) Order 0~2.
        vArrZNum,           // array z(updown) number.
        vArrZPitch,         // array z(updown) pitch [mm].
        vArrZOrder,         // array z(updown) Order 0~2.
        vArrZOffsetX,       // array z(updown) offset x(traverse) [mm]
        vArrZOffsetY,       // array z(updown) offset y(fwdbwd) [mm]
        vArrXCurr,          // array x current number.
        vArrYCurr,          // array y current number.
        vArrZCurr,          // array z current number.

        vIArrXNum,           // Insert grip array x(traverse) number.
        vIArrXPitch,         // Insert grip array x(traverse) pitch [mm].
        vIArrXOrder,         // Insert grip array x(traverse) Order 0~2.
        vIArrYNum,           // Insert grip array y(fwdbwd) number.
        vIArrYPitch,         // Insert grip array y(fwdbwd) pitch [mm].
        vIArrYOrder,         // Insert grip array y(fwdbwd) Order 0~2.
        vIArrZNum,           // Insert grip array z(updown) number.
        vIArrZPitch,         // Insert grip array z(updown) pitch [mm].
        vIArrZOrder,         // Insert grip array z(updown) Order 0~2.
        vIArrZOffsetX,       // Insert grip array z(updown) offset x(traverse) [mm]
        vIArrZOffsetY,       // Insert grip array z(updown) offset y(fwdbwd) [mm]
        vIArrXCurr,          // array x current number.
        vIArrYCurr,          // array y current number.
        vIArrZCurr,          // array z current number.

        vSeqNum,            // sequential number 2~4.
        vSeqHeight,         // seq shift point z height value(mm)
        vSeqX1,             // sequential point1 x offset(mm)
        vSeqX2,             // sequential point2 x offset(mm)
        vSeqX3,             // sequential point3 x offset(mm)
        vSeqY1,             // sequential point1 y offset(mm)
        vSeqY2,             // sequential point2 y offset(mm)
        vSeqY3,             // sequential point3 y offset(mm)
        vSeqZ1,             // sequential point1 z offset(mm)
        vSeqZ2,             // sequential point2 z offset(mm)
        vSeqZ3,             // sequential point3 z offset(mm)
        vSeqVac1,           // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqVac2,           // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqVac3,           // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqVac4,           // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqChuck,          // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqGrip,           // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqUser1,          // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqUser2,          // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqUser3,          // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vSeqUser4,          // 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.

        vISeqNum,            // Insert sequential number 2~4.
        vISeqHeight,        // Insert seq shift point z height value(mm)
        vISeqX1,             // Insert sequential point1 x offset(mm)
        vISeqX2,             // Insert sequential point2 x offset(mm)
        vISeqX3,             // Insert sequential point3 x offset(mm)
        vISeqY1,             // Insert sequential point1 y offset(mm)
        vISeqY2,             // Insert sequential point2 y offset(mm)
        vISeqY3,             // Insert sequential point3 y offset(mm)
        vISeqZ1,             // Insert sequential point1 z offset(mm)
        vISeqZ2,             // Insert sequential point2 z offset(mm)
        vISeqZ3,             // Insert sequential point3 z offset(mm)
        vISeqUser1,          // Insert 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vISeqUser2,          // Insert 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vISeqUser3,          // Insert 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vISeqUser4,          // Insert 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vISeqUser5,          // Insert 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vISeqUser6,          // Insert 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vISeqUser7,          // Insert 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.
        vISeqUser8,          // Insert 0: no, 1: 1p use, 2: 2p use, 3: 3p use, 4: 4p use.

        // interlock
        mdEjectFwd,         // 0: no, 1: use.
        mdEjectFwdComp,     // 0: no, 1: use.
        mdEjectBwd,         // 0: no, 1: use.
        mdEjectBwdComp,     // 0: no, 1: use.
        mdCoreFwd,          // 0: no, 1: use.
        mdCoreFwdComp,      // 0: no, 1: use.
        mdCoreBwd,          // 0: no, 1: use.
        mdCoreBwdComp,      // 0: no, 1: use.
        // interlock 2
        mdMoldClose,        // 0: after up, 1: after rotation.
        mdEuromap,          // 0: standard, 1: EM67, 2: EM12

        // Fusion
        mdWeight1,          // 0: no, 1: use.
        vWeightMin1,        // value [g]
        vWeightMax1,        // value [g]
        vWeightOffset1,     // offset [g]
        mdWeight2,          // 0: no, 1: use.
        vWeightMin2,        // value [g]
        vWeightMax2,        // value [g]
        vWeightOffset2,     // offset [g]
        mdWeight3,          // 0: no, 1: use.
        vWeightMin3,        // value [g]
        vWeightMax3,        // value [g]
        vWeightOffset3,     // offset [g]
        mdWeight4,          // 0: no, 1: use.
        vWeightMin4,        // value [g]
        vWeightMax4,        // value [g]
        vWeightOffset4,     // offset [g]
        tWeighDelay,        // delay [sec]
        mdTemp1,            // 0: no, 1: use.
        vTempMin1,          // value [도]
        vTempMax1,          // value [도]
        mdTemp2,            // 0: no, 1: use.
        vTempMin2,          // value [도]
        vTempMax2,          // value [도]
        mdTemp3,            // 0: no, 1: use.
        vTempMin3,          // value [도]
        vTempMax3,          // value [도]
        mdTemp4,            // 0: no, 1: use.
        vTempMin4,          // value [도]
        vTempMax4,          // value [도]
        tTempDelay,         // delay [sec]
        mdIonizer,          // 0: no, 1: use.
        tIonizerOn,         // on time [sec]
        mdESen1,
        vESenMin1,
        vESenMax1,
        mdESen2,
        vESenMin2,
        vESenMax2,
        mdESen3,
        vESenMin3,
        vESenMax3,
        mdESen4,
        vESenMin4,
        vESenMax4,
        tESenDelay,

        vRotPos0,           // pos. rotation return
        vRotPos1,           // pos. rot1
        vRotPos2,           // pos. rot2
        vRotPos3,           // pos. rot3
        vRotPos4,           // pos. rot4
        vSwvPos0,           // pos. swivel return
        vSwvPos1,           // pos. swv1
        vSwvPos2,           // pos. swv2
        vSwvPos3,           // pos. swv3
        vSwvPos4,           // pos. swv4

        // motor Tune
        vTuneSpd,           // motor tune speed %
        vTuneAcc,           // motor tune acc %
        vTuneDec,           // motor tune dec %
        vTuneTime,          // motor tune interval time sec.

        // product info.
        vTargetProd,        // Target Product Qty. [ea] - setting.
        vCurrProd,          // Current Product Qty. [ea]
        tCycleTime,         // 1 Cycle Time [sec]
        tTOutTime,          // Takeout Time [sec]
        vCavity,            // Cavity Number. - setting.
        vDischarge,         // Discharge Number.
        vCountHold,         // Count Hold 0=continue, 1=hold.
        bOneCycle,          // 1 Cycle flag
        bDischarge,         // Discharge flag
        bOneCycleEnd,       // 1 Cycle End flag.
        bNGProd,            // Confirm Faulty Product (if 0 is OK, >1 is NG)
        bSampleUse,         // new! Sample use:1, not use:0
        vSample,            // Sample Production Number.
        bSample,            // Sample execution flag
        vSampleCycle,       // new! Sample per Cycle number (setting)
        vSampleCnt,         // new! Sample trigger down count. (in seq. handling.)
        b1stBad,            // 1st Bad Use or not.
        v1stBadSet,         // 1st Bad Setting Value (only use ui)
        v1stBadCnt,         // 1st Bad real count

        // sync back
        mdSyncBack,         // Ejector Sync Back 0:no use, 1:use
        vSyncLength,        // Sync Back Length (= ejector forwarding length)
        vSyncTime,          // Sync Time. (=ejector forwarding elapsed time)
        vSyncSpeed,         // Calculated Sync Back Speed.

        // Seq. variable (i-)
        ipos_curr,          // current pos index
        ipos_plan,          // plan pos index
        bTestMode,          // test mode flag.
        bMainInit,          // main init flag. make 1 => start main init.
        bSubInit,           // sub init flag. make 1 => start sub init.
        bRecipeChange,      // for before recipe change process
        bDualSolOff,        // for Dual sol off

        // Seq. Status Bit
        sHome,              // status Home 0:no home, 1: home complete.
        sWait,              // status wait step 0:end 1:~ing
        sTakeout,           // status takeout step start ~ up step start 0:end 1:~ing (**important)
        sUp,                // status up step 0:end 1:~ing
        sUnload,            // status unload step 0:end 1:~ing
        sUserPos,           // status userpos step 0:end 1~N:~ing (N is arr no. +1)
        sInTZone,           // status in takeout zone 0:no 1:in (check for softsensor Trav&&UpDn)
        sTravSen,           // status Soft Sensor Trav.
        sUpDnSen,           // status Soft Sensor UpDn.
        sMsgId,
        sUserOut,           // status useroutput step 0:end 1~N:~ing (N is arr No. +1)
        sUserIn,            // status userinput step 0:end 1~N:~ing (N is arr No. +1)
        sInsertGrip,        // status insert grip step 0:end 1:~ing
        sInsertUL,          // status insert unload step 0:end 1:~ing
        sAutoRun,           // status autorun 0:stop , 1:runing
        sAutoMode,          // status auto mode 0:manual 1: auto
        sNotUse,            // status robot not use 0:use 1:not use
        sSafetyCheck,       // status takeout start ~ up end for safety device.
        s1stAutoRun,        // status after stop & home, flag 1 mean first autorun.

        // information
        iNowRecipeNo,       // info now recipe number


        // total interlock module
        // common
        emType,
        emUse,

        // safety
        emStatImmEmo,
        emEmoStop,
        emStatSd,
        emSdStop,
        emSdSwitch,
        emSdMode,
        emEmoMode, // ?

        // operation mode
        emCmdAuto,
        emStatAuto,
        emAutoMode,

        // mold
        emCmdTakeout,
        emCmdInMold,
        emStatMold,
        emMoldMode,

        // ejector
        emSigEject,
        emEjectMode,
        emFastEject,

        // core 1&2
        emSigCore1,
        emCoreMode1,
        emSigCore2,
        emCoreMode2,

        // simulator
        emSimRun,

        n_num
    };

    enum RECIPE_STRING
    {
        s_data1,
        s_RecipeName,

        s_num
    };


public:

    bool MakeDefault();     // for dev
    bool AllZero();         // for dev
    bool Clear();           // for dev
    bool Clear(int type);   // for dev

    bool Read();
    bool Read(int type);

    bool Write();
    bool Write(int type);

    void SaveVariable();

    // single
    bool Set(RECIPE_JOINT id, cn_joint data, bool save=true);
    bool Set(RECIPE_TRANS id, cn_trans data, bool save=true);
    bool Set(RECIPE_NUMBER id, float data, bool save=true);
    bool Set(RECIPE_STRING id, QString data, bool save=true);

    bool Set(QString var, float data, bool save=true);
    bool Set(QString var, QString data, bool save=true);

    bool Get(RECIPE_JOINT id, cn_joint* data);
    bool Get(RECIPE_TRANS id, cn_trans* data);
    bool Get(RECIPE_NUMBER id, float* data);
    bool Get(RECIPE_STRING id, QString* data);

    bool Get(QString var, float& data);
    bool Get(QString var, QString& data);

    // recipe name type
    bool Sets(QVector<RECIPE_JOINT> ids, QVector<cn_joint>& datas, bool save=true);
    bool Sets(QVector<RECIPE_TRANS> ids, QVector<cn_trans>& datas, bool save=true);
    bool Sets(QVector<RECIPE_NUMBER> ids, QVector<float>& datas, bool save=true);
    bool Sets(QVector<RECIPE_STRING> ids, QStringList& datas, bool save=true);

    bool Gets(QVector<RECIPE_JOINT> ids, QVector<cn_joint>& datas);
    bool Gets(QVector<RECIPE_TRANS> ids, QVector<cn_trans>& datas);
    bool Gets(QVector<RECIPE_NUMBER> ids, QVector<float>& datas);
    bool Gets(QVector<RECIPE_STRING> ids, QStringList& datas);


    // array type
    bool SetArr(QString name, int i, cn_joint data, bool save=true);
    bool GetArr(QString name, int i, cn_joint* data);

    bool SetArr(QString name, int i, cn_trans data, bool save=true);
    bool GetArr(QString name, int i, cn_trans* data);

    bool SetArr(QString name, int i, float data, bool save=true);
    bool GetArr(QString name, int i, float* data);
    bool SetsArr(QString name, QVector<float>& datas, bool save=true); // 0 ~ datas count.
    bool GetsArr(QString name, int count, QVector<float>& datas); // 0 ~ datas count.

    bool SetArr(QString name, int i, QString data, bool save=true);
    bool GetArr(QString name, int i, QString* data);

    // global name type
    bool SetVars(QStringList names, QList<cn_variant>& datas, bool save=true);
    bool GetVars(QStringList names, QList<cn_variant>& datas);

public:

    //CN_ROBOT_VARIABLE_JOINT (1)
    QStringList Joint_Name;
    QVector<cn_variant> Joint_Value;

    //CN_ROBOT_VARIABLE_TRANS (2)
    QStringList Trans_Name;
    QVector<cn_variant> Trans_Value;

    //CN_ROBOT_VARIABLE_NUMBER (3)
    QStringList Number_Name;
    QVector<cn_variant> Number_Value;

    //CN_ROBOT_VARIABLE_STRING (4)
    QStringList String_Name;
    QVector<cn_variant> String_Value;

public:

    QString toName(RECIPE_JOINT enums);
    QString toName(RECIPE_TRANS enums);
    QString toName(RECIPE_NUMBER enums);
    QString toName(RECIPE_STRING enums);

    bool IsCanRead();
    bool IsCanWrite();

    QElapsedTimer timer;

};

#endif // HYRECIPE_H
