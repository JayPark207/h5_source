#ifndef FORM_STEPEDIT_USERPOSITION_H
#define FORM_STEPEDIT_USERPOSITION_H

#include <QWidget>
#include <QStackedWidget>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_position_teaching2.h"
#include "dialog_message.h"
#include "dialog_confirm.h"

#include "dialog_sasang_pattern.h"

namespace Ui {
class form_stepedit_userposition;
}

class form_stepedit_userposition : public QWidget
{
    Q_OBJECT

public:
    enum PAGE_MODE
    {
        USERPOS = 0,
        USERWORK,
    };

public:
    explicit form_stepedit_userposition(QWidget *parent = 0);
    ~form_stepedit_userposition();

    void Init(int select_index, int step_index, PAGE_MODE mode = USERPOS, QString title = "");

    void Update();

private:
    Ui::form_stepedit_userposition *ui;

    int         m_nSelectedIndex;
    int         m_nStepIndex;
    QString     m_strTitle;
    PAGE_MODE   m_PageMode;

    QStackedWidget* StackedBtn;
    void Display_BtnGroup(PAGE_MODE mode);

    void Display_Data();
    void Display_SetPos(cn_trans t, cn_joint j);
    void Display_Spd(float speed);
    void Display_Delay(float delay);
    void Display_Type(float type);
    void Display_Info();


    QVector<QLabel3*> m_vtType;

    // main data.
    ST_USERDATA_POS     m_UserPos;
    ST_USERDATA_WORK    m_UserWork;

    bool SaveType(float type);

    dialog_sasang_pattern* dig_sasang_pattern;


signals:
    void sigClose();

public slots:
    void onClose();

    void onType();

    // COMMON
    void onTeach();

    // USERPOS
    // USERWORK
    void onPattern();
    void onRefresh();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // FORM_STEPEDIT_USERPOSITION_H
