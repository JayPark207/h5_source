#include "dialog_stepedit.h"
#include "ui_dialog_stepedit.h"

dialog_stepedit::dialog_stepedit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_stepedit)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnListUp,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnListDown,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnListUp,SIGNAL(mouse_press()),this,SLOT(onPressBtm()));
    connect(ui->btnListDown,SIGNAL(mouse_press()),this,SLOT(onPressBtm()));
    connect(ui->btnListUp,SIGNAL(mouse_release()),this,SLOT(onReleaseBtm()));
    connect(ui->btnListDown,SIGNAL(mouse_release()),this,SLOT(onReleaseBtm()));
    /** for long press operation. **/
    uiLongPressUp = new HyUi_LongPress();
    uiLongPressUp->Init(1000,200);
    connect(ui->btnListUp,SIGNAL(mouse_press()),uiLongPressUp,SLOT(onPressed()));
    connect(ui->btnListUp,SIGNAL(mouse_release()),uiLongPressUp,SLOT(onReleased()));
    connect(uiLongPressUp,SIGNAL(sigTrigger()),this,SLOT(onUpLong()));
    uiLongPressDown = new HyUi_LongPress();
    uiLongPressDown->Init(1000,200);
    connect(ui->btnListDown,SIGNAL(mouse_press()),uiLongPressDown,SLOT(onPressed()));
    connect(ui->btnListDown,SIGNAL(mouse_release()),uiLongPressDown,SLOT(onReleased()));
    connect(uiLongPressDown,SIGNAL(sigTrigger()),this,SLOT(onDownLong()));

    connect(ui->btnAddPic,SIGNAL(mouse_release()),this,SLOT(onAdd()));
    connect(ui->btnAdd,SIGNAL(mouse_press()),ui->btnAddPic,SLOT(press()));
    connect(ui->btnAddIcon,SIGNAL(mouse_press()),ui->btnAddPic,SLOT(press()));
    connect(ui->btnAdd,SIGNAL(mouse_release()),ui->btnAddPic,SLOT(release()));
    connect(ui->btnAddIcon,SIGNAL(mouse_release()),ui->btnAddPic,SLOT(release()));

    connect(ui->btnDeletePic,SIGNAL(mouse_release()),this,SLOT(onDelete()));
    connect(ui->btnDelete,SIGNAL(mouse_press()),ui->btnDeletePic,SLOT(press()));
    connect(ui->btnDeleteIcon,SIGNAL(mouse_press()),ui->btnDeletePic,SLOT(press()));
    connect(ui->btnDelete,SIGNAL(mouse_release()),ui->btnDeletePic,SLOT(release()));
    connect(ui->btnDeleteIcon,SIGNAL(mouse_release()),ui->btnDeletePic,SLOT(release()));

    connect(ui->btnDetailPic,SIGNAL(mouse_release()),this,SLOT(onDetail()));
    connect(ui->btnDetail,SIGNAL(mouse_press()),ui->btnDetailPic,SLOT(press()));
    connect(ui->btnDetailIcon,SIGNAL(mouse_press()),ui->btnDetailPic,SLOT(press()));
    connect(ui->btnDetail,SIGNAL(mouse_release()),ui->btnDetailPic,SLOT(release()));
    connect(ui->btnDetailIcon,SIGNAL(mouse_release()),ui->btnDetailPic,SLOT(release()));

    connect(ui->btnStepUpPic,SIGNAL(mouse_release()),this,SLOT(onStepUp()));
    connect(ui->btnStepUp,SIGNAL(mouse_press()),ui->btnStepUpPic,SLOT(press()));
    connect(ui->btnStepUpIcon,SIGNAL(mouse_press()),ui->btnStepUpPic,SLOT(press()));
    connect(ui->btnStepUp,SIGNAL(mouse_release()),ui->btnStepUpPic,SLOT(release()));
    connect(ui->btnStepUpIcon,SIGNAL(mouse_release()),ui->btnStepUpPic,SLOT(release()));

    connect(ui->btnStepDownPic,SIGNAL(mouse_release()),this,SLOT(onStepDown()));
    connect(ui->btnStepDown,SIGNAL(mouse_press()),ui->btnStepDownPic,SLOT(press()));
    connect(ui->btnStepDownIcon,SIGNAL(mouse_press()),ui->btnStepDownPic,SLOT(press()));
    connect(ui->btnStepDown,SIGNAL(mouse_release()),ui->btnStepDownPic,SLOT(release()));
    connect(ui->btnStepDownIcon,SIGNAL(mouse_release()),ui->btnStepDownPic,SLOT(release()));

    // sub button event
    connect(ui->btnRenamePic,SIGNAL(mouse_release()),this,SLOT(onRename()));
    connect(ui->btnRename,SIGNAL(mouse_press()),ui->btnRenamePic,SLOT(press()));
    connect(ui->btnRenameIcon,SIGNAL(mouse_press()),ui->btnRenamePic,SLOT(press()));
    connect(ui->btnRename,SIGNAL(mouse_release()),ui->btnRenamePic,SLOT(release()));
    connect(ui->btnRenameIcon,SIGNAL(mouse_release()),ui->btnRenamePic,SLOT(release()));

    connect(ui->btnTestPic,SIGNAL(mouse_release()),this,SLOT(onTest()));
    connect(ui->btnTest,SIGNAL(mouse_press()),ui->btnTestPic,SLOT(press()));
    connect(ui->btnTest,SIGNAL(mouse_release()),ui->btnTestPic,SLOT(release()));
    connect(ui->btnTestIcon,SIGNAL(mouse_press()),ui->btnTestPic,SLOT(press()));
    connect(ui->btnTestIcon,SIGNAL(mouse_release()),ui->btnTestPic,SLOT(release()));


    // for test
    connect(ui->btnCheckPic,SIGNAL(mouse_release()),this,SLOT(onDebug()));
    connect(ui->btnCheck,SIGNAL(mouse_press()),ui->btnCheckPic,SLOT(press()));
    connect(ui->btnCheck,SIGNAL(mouse_release()),ui->btnCheckPic,SLOT(release()));
    connect(ui->btnCheckIcon,SIGNAL(mouse_press()),ui->btnCheckPic,SLOT(press()));
    connect(ui->btnCheckIcon,SIGNAL(mouse_release()),ui->btnCheckPic,SLOT(release()));

    m_vtIcon.clear();
    m_vtIcon.append(ui->lbIcon);
    m_vtIcon.append(ui->lbIcon_2);
    m_vtIcon.append(ui->lbIcon_3);
    m_vtIcon.append(ui->lbIcon_4);
    m_vtIcon.append(ui->lbIcon_5);
    m_vtIcon.append(ui->lbIcon_6);
    m_vtIcon.append(ui->lbIcon_7);

    m_vtNo.clear();
    m_vtNo.append(ui->lbNo);
    m_vtNo.append(ui->lbNo_2);
    m_vtNo.append(ui->lbNo_3);
    m_vtNo.append(ui->lbNo_4);
    m_vtNo.append(ui->lbNo_5);
    m_vtNo.append(ui->lbNo_6);
    m_vtNo.append(ui->lbNo_7);

    m_vtName.clear();
    m_vtName.append(ui->lbName);
    m_vtName.append(ui->lbName_2);
    m_vtName.append(ui->lbName_3);
    m_vtName.append(ui->lbName_4);
    m_vtName.append(ui->lbName_5);
    m_vtName.append(ui->lbName_6);
    m_vtName.append(ui->lbName_7);

    m_vtLine.clear();
    m_vtLine.append(ui->lbLinePic);
    m_vtLine.append(ui->lbLinePic_2);
    m_vtLine.append(ui->lbLinePic_3);
    m_vtLine.append(ui->lbLinePic_4);
    m_vtLine.append(ui->lbLinePic_5);
    m_vtLine.append(ui->lbLinePic_6);
    m_vtLine.append(ui->lbLinePic_7);

    m_vtCheck.clear();
    m_vtCheck.append(ui->lbCheck);
    m_vtCheck.append(ui->lbCheck_2);
    m_vtCheck.append(ui->lbCheck_3);
    m_vtCheck.append(ui->lbCheck_4);
    m_vtCheck.append(ui->lbCheck_5);
    m_vtCheck.append(ui->lbCheck_6);
    m_vtCheck.append(ui->lbCheck_7);

    int i;
    for(i=0;i<m_vtNo.count();i++)
    {
        connect(m_vtIcon[i],SIGNAL(mouse_release()),this,SLOT(onSelectLine()));
        connect(m_vtNo[i],SIGNAL(mouse_release()),this,SLOT(onSelectLine()));
        connect(m_vtName[i],SIGNAL(mouse_release()),this,SLOT(onSelectLine()));
        connect(m_vtLine[i],SIGNAL(mouse_release()),this,SLOT(onSelectLine()));
        connect(m_vtCheck[i],SIGNAL(mouse_release()),this,SLOT(onSelectLine()));
    }

    // Test Run Part.
    timerRun = new QTimer(this);
    timerRun->setInterval(200);
    connect(timerRun,SIGNAL(timeout()),this,SLOT(onRunTimer()));

    connect(ui->btnStepRunPic,SIGNAL(mouse_release()),this,SLOT(onStepFwd()));
    connect(ui->btnStepRun,SIGNAL(mouse_press()),ui->btnStepRunPic,SLOT(press()));
    connect(ui->btnStepRun,SIGNAL(mouse_release()),ui->btnStepRunPic,SLOT(release()));
    connect(ui->btnStepRunIcon,SIGNAL(mouse_press()),ui->btnStepRunPic,SLOT(press()));
    connect(ui->btnStepRunIcon,SIGNAL(mouse_release()),ui->btnStepRunPic,SLOT(release()));

    connect(ui->btnStepBwdPic,SIGNAL(mouse_release()),this,SLOT(onStepBwd()));
    connect(ui->btnStepBwd,SIGNAL(mouse_press()),ui->btnStepBwdPic,SLOT(press()));
    connect(ui->btnStepBwd,SIGNAL(mouse_release()),ui->btnStepBwdPic,SLOT(release()));
    connect(ui->btnStepBwdIcon,SIGNAL(mouse_press()),ui->btnStepBwdPic,SLOT(press()));
    connect(ui->btnStepBwdIcon,SIGNAL(mouse_release()),ui->btnStepBwdPic,SLOT(release()));

    connect(ui->btnCyclePic,SIGNAL(mouse_release()),this,SLOT(onCycle()));
    connect(ui->btnCycle,SIGNAL(mouse_press()),ui->btnCyclePic,SLOT(press()));
    connect(ui->btnCycle,SIGNAL(mouse_release()),ui->btnCyclePic,SLOT(release()));
    connect(ui->btnCycleIcon,SIGNAL(mouse_press()),ui->btnCyclePic,SLOT(press()));
    connect(ui->btnCycleIcon,SIGNAL(mouse_release()),ui->btnCyclePic,SLOT(release()));

    connect(ui->btnHoldPic,SIGNAL(mouse_release()),this,SLOT(onPause()));
    connect(ui->btnHold,SIGNAL(mouse_press()),ui->btnHoldPic,SLOT(press()));
    connect(ui->btnHold,SIGNAL(mouse_release()),ui->btnHoldPic,SLOT(release()));
    connect(ui->btnHoldIcon,SIGNAL(mouse_press()),ui->btnHoldPic,SLOT(press()));
    connect(ui->btnHoldIcon,SIGNAL(mouse_release()),ui->btnHoldPic,SLOT(release()));

    connect(ui->btnContinuePic,SIGNAL(mouse_release()),this,SLOT(onRun()));
    connect(ui->btnContinue,SIGNAL(mouse_press()),ui->btnContinuePic,SLOT(press()));
    connect(ui->btnContinue,SIGNAL(mouse_release()),ui->btnContinuePic,SLOT(release()));
    connect(ui->btnContinueIcon,SIGNAL(mouse_press()),ui->btnContinuePic,SLOT(press()));
    connect(ui->btnContinueIcon,SIGNAL(mouse_release()),ui->btnContinuePic,SLOT(release()));

    connect(ui->btnAbortPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnAbort,SIGNAL(mouse_press()),ui->btnAbortPic,SLOT(press()));
    connect(ui->btnAbort,SIGNAL(mouse_release()),ui->btnAbortPic,SLOT(release()));
    connect(ui->btnAbortIcon,SIGNAL(mouse_press()),ui->btnAbortPic,SLOT(press()));
    connect(ui->btnAbortIcon,SIGNAL(mouse_release()),ui->btnAbortPic,SLOT(release()));

    connect(ui->btnHomePic,SIGNAL(mouse_release()),this,SLOT(onHome()));
    connect(ui->btnHome,SIGNAL(mouse_press()),ui->btnHomePic,SLOT(press()));
    connect(ui->btnHome,SIGNAL(mouse_release()),ui->btnHomePic,SLOT(release()));
    connect(ui->btnHomeIcon,SIGNAL(mouse_press()),ui->btnHomePic,SLOT(press()));
    connect(ui->btnHomeIcon,SIGNAL(mouse_release()),ui->btnHomePic,SLOT(release()));

    connect(ui->btnCodePic,SIGNAL(mouse_release()),this,SLOT(onProgram()));
    connect(ui->btnCode,SIGNAL(mouse_press()),ui->btnCodePic,SLOT(press()));
    connect(ui->btnCode,SIGNAL(mouse_release()),ui->btnCodePic,SLOT(release()));
    connect(ui->btnCodeIcon,SIGNAL(mouse_press()),ui->btnCodePic,SLOT(press()));
    connect(ui->btnCodeIcon,SIGNAL(mouse_release()),ui->btnCodePic,SLOT(release()));

    connect(ui->btnSpdUpPic,SIGNAL(mouse_release()),this,SLOT(onTestSpdUp()));
    connect(ui->btnSpdUpIcon,SIGNAL(mouse_press()),ui->btnSpdUpPic,SLOT(press()));
    connect(ui->btnSpdUpIcon,SIGNAL(mouse_release()),ui->btnSpdUpPic,SLOT(release()));

    connect(ui->btnSpdDownPic,SIGNAL(mouse_release()),this,SLOT(onTestSpdDown()));
    connect(ui->btnSpdDownIcon,SIGNAL(mouse_press()),ui->btnSpdDownPic,SLOT(press()));
    connect(ui->btnSpdDownIcon,SIGNAL(mouse_release()),ui->btnSpdDownPic,SLOT(release()));


    // important variable.
    m_nStartIndex = 0;
    m_nSelectedIndex = 0;
    m_nPlayIndex = 0;
    m_nPlayStatus = STOP;

    ui->grpTestBtn->setGeometry(ui->grpEditBtn->x(),
                                ui->grpEditBtn->y(),
                                ui->grpEditBtn->width(),
                                ui->grpEditBtn->height());

    ui->pbarSpeed->setMaximum(STEPTEST_SPEED_MAX);


    // initial important pointer. (check null)
    Stacked = 0;
    top = 0;
    pxmSelectLine.clear();
    pxmCheckLine.clear();
    pxmIconLine.clear();

    dig_sasang_addstep = 0;
    dig_home2 = 0;
    m_bHome = 0;
    m_bUpdate = false;
    m_bAddUserStep = false;
}

dialog_stepedit::~dialog_stepedit()
{
    delete ui;
}
void dialog_stepedit::Init_Widget()
{
    QElapsedTimer ti;

    if(Stacked != 0)
        return;

    // for detail
    Stacked = new QStackedWidget(this);
    //Stacked->setGeometry(0,0,800,480);
    Stacked->setGeometry(0,0,MAX_WIDTH,MAX_HEIGHT);

    ti.start();
    frmBase = new form_stepedit_base(this);
    Stacked->addWidget(frmBase);
    qDebug() << "form_stepedit_base time =" << ti.elapsed() << "mesc";

    ti.start();
    frmOutput = new form_stepedit_useroutput(this);
    Stacked->addWidget(frmOutput);
    qDebug() << "form_stepedit_useroutput time =" << ti.elapsed() << "mesc";

    ti.start();
    frmInputTime = new form_stepedit_userinput(this);
    Stacked->addWidget(frmInputTime);
    qDebug() << "form_stepedit_userinput time =" << ti.elapsed() << "mesc";

    frmUserPos = new form_stepedit_userposition(this);
    Stacked->addWidget(frmUserPos);

    Stacked->hide();

    int i;
    for(i=0;i<Stacked->count();i++)
    {
        connect(Stacked->widget(i),SIGNAL(sigClose()),this,SLOT(onOtherClose()));
    }

    ti.start();
    dig_add = new dialog_stepedit_add();
    qDebug() << "dialog_stepedit_add time =" << ti.elapsed() << "mesc";

    // for program view
    wigProgram = new widget_stepedit_program(this);
    //wigProgram->setGeometry(0,55,wigProgram->width(),wigProgram->height());
    wigProgram->setGeometry(MAX_WIDTH - wigProgram->width(),
                            MAX_HEIGHT - wigProgram->height() -5, // 5 is bottom offset
                            wigProgram->width(),
                            wigProgram->height());
    wigProgram->hide();

    // for program run view
    wigProgramRun = new widget_stepedit_programrun(this);
    wigProgramRun->setGeometry(ui->frStepList->x(),
                            ui->frStepList->y(),
                            ui->frStepList->width(),
                            ui->frStepList->height());
    wigProgramRun->hide();

}


void dialog_stepedit::onClose()
{
    emit accept();
}

void dialog_stepedit::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
        StepEdit->m_StepData.Init();
    }
}

void dialog_stepedit::showEvent(QShowEvent *)
{
    Init_Widget();

    Title = QString(tr("Step Edit"));
    ui->lbTitle->setText(Title);

    m_nSelectedIndex = 0;
    m_nStartIndex = 0;

    SetTestMode(false);
    Update();

    UserLevel();    // must

    SubTop();   // view subtop

    m_bAddUserStep = false;
}

void dialog_stepedit::hideEvent(QHideEvent *)
{
    if(m_bAddUserStep)
        Recipe->SaveVariable();

    timerRun->stop();
    ServoOnMode(SON_MODE_NORMAL);
}

void dialog_stepedit::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}

void dialog_stepedit::UserLevel()
{
    // read user level
    m_nUserLevel = Param->Get(HyParam::USER_LEVEL).toInt();

    if(m_nUserLevel < USER_LEVEL_HIGH)
    {
        //ui->wigCode->hide();
        ui->wigCode->setEnabled(false);
    }
    else
    {
        //ui->wigCode->show();
        ui->wigCode->setEnabled(true);
    }
}

void dialog_stepedit::CheckMode_ChangeList()
{
    QVector<HyRecipe::RECIPE_NUMBER> _vars;
    QVector<float> _datas;

    _vars.clear();
    _vars.append(HyRecipe::mdInsert);
    _vars.append(HyRecipe::mdWeight1);
    _vars.append(HyRecipe::mdWeight2);
    _vars.append(HyRecipe::mdWeight3);
    _vars.append(HyRecipe::mdWeight4);

    if(!Recipe->Gets(_vars, _datas))
        return;

    //HyRecipe::mdInsert
    if((int)_datas[0] > 0)
    {
        if(!StepEdit->HasStep(HyStepData::INSERT))
        {
            StepEdit->Insert_forMode(HyStepData::INSERT);
            /* StepEdit->Insert_forMode(HyStepData::INSERT_UNLOAD); // delete insert unload */
            StepEdit->Write_Main();
        }
    }
    else
    {
        if(StepEdit->HasStep(HyStepData::INSERT))
        {
            StepEdit->Delete_forMode(HyStepData::INSERT);
            /* StepEdit->Delete_forMode(HyStepData::INSERT_UNLOAD); // delete insert unload */
            StepEdit->Write_Main();
            m_nSelectedIndex = 0;
        }
    }

    //HyRecipe::mdWeight1~4
    int _sum = (int)_datas[1] + (int)_datas[2] + (int)_datas[3] + (int)_datas[4];
    bool _write = false;
    if(_sum > 0)
    {
        _write = false;
        if(!StepEdit->HasStep(HyStepData::W_MEASURE))
        {
            StepEdit->Insert_forMode(HyStepData::W_MEASURE);
            _write = true;
        }
        if(!StepEdit->HasStep(HyStepData::W_RESET))
        {
            StepEdit->Insert_forMode(HyStepData::W_RESET);
            _write = true;
        }
        if(_write)
            StepEdit->Write_Main();
    }
    else
    {
        _write = false;
        if(StepEdit->HasStep(HyStepData::W_MEASURE))
        {
            StepEdit->Delete_forMode(HyStepData::W_MEASURE);
            _write = true;
        }
        if(StepEdit->HasStep(HyStepData::W_RESET))
        {
            StepEdit->Delete_forMode(HyStepData::W_RESET);
            _write = true;
        }
        if(_write)
        {
            StepEdit->Write_Main();
            m_nSelectedIndex = 0;
        }
    }

}

void dialog_stepedit::onDebug()
{

}

void dialog_stepedit::onUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);

}
void dialog_stepedit::onUpLong()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_stepedit::onDown()
{
    if(m_vtNo.size() < (StepEdit->m_Step.size()-m_nStartIndex) )
    {
        Redraw_List(++m_nStartIndex);
    }
}
void dialog_stepedit::onDownLong()
{
    if(m_vtNo.size() < (StepEdit->m_Step.size()-m_nStartIndex) )
    {
        Redraw_List(++m_nStartIndex);
    }
}

void dialog_stepedit::onStepUp()
{
    // for debug
    if(!StepEdit->IsCanStepMove(m_nSelectedIndex))
        return;

    if(StepEdit->Up(m_nSelectedIndex))
    {
        m_nSelectedIndex--;

        if(m_nSelectedIndex < m_nStartIndex)
            m_nStartIndex--;

        StepEdit->Write_Main();
        Redraw_List(m_nStartIndex);
    }

}
void dialog_stepedit::onStepDown()
{
    // for debug
    if(!StepEdit->IsCanStepMove(m_nSelectedIndex))
        return;

    if(StepEdit->Down(m_nSelectedIndex))
    {
        m_nSelectedIndex++;

        if((m_nSelectedIndex-m_nStartIndex) >= m_vtNo.size())
            m_nStartIndex++;

        StepEdit->Write_Main();
        Redraw_List(m_nStartIndex);
    }

}
void dialog_stepedit::onAdd()
{
    int pos_offset = 0;
    int selected_index_old = 0;
    int index;
    //dialog_stepedit_add* dig = new dialog_stepedit_add();
    dig_add->Init(m_nSelectedIndex);

    if(dig_add->exec() == QDialog::Accepted)
    {
        if(dig_add->IsBackPos())
            pos_offset = 1;
        else
            pos_offset = 0;

        selected_index_old = m_nSelectedIndex;
        m_nSelectedIndex += pos_offset;

        switch(dig_add->GetAddItem())
        {
        case dialog_stepedit_add::ADD_POS:
            if(!StepEdit->Insert_Position(m_nSelectedIndex))
            {
                msg = (dialog_message*)gGetDialog(DIG_MSG);
                msg->SetColor(dialog_message::RED);
                msg->Title(tr("Error"));
                msg->Message(tr("Fail to Add User Position Step!"),
                             tr("Over Max. Number or Fail to write file."));
                msg->exec();
                m_nSelectedIndex = selected_index_old;
                return;
            }
            StepEdit->m_Step[m_nSelectedIndex].NickName = dig_add->GetNickName();
            // write 1st value.
            ST_USERDATA_POS _pos;
            UserData->Set_1stValue(&_pos);
            index = StepEdit->m_Step[m_nSelectedIndex].Id;
            index -= HyStepData::ADD_POS0;
            if(index >= 0)
                UserData->WR(index, _pos, false);
            break;

        case dialog_stepedit_add::ADD_OUTPUT:
            if(!StepEdit->Insert_Output(m_nSelectedIndex))
            {
                msg = (dialog_message*)gGetDialog(DIG_MSG);
                msg->SetColor(dialog_message::RED);
                msg->Title(tr("Error"));
                msg->Message(tr("Fail to Add User Output Step!"),
                             tr("Over Max. Number or Fail to write file."));
                msg->exec();
                m_nSelectedIndex = selected_index_old;
                return;
            }
            StepEdit->m_Step[m_nSelectedIndex].NickName = dig_add->GetNickName();
            // write 1st value.
            ST_USERDATA_OUTPUT _output;
            UserData->Set_1stValue(&_output);
            index = StepEdit->m_Step[m_nSelectedIndex].Id;
            index -= HyStepData::ADD_OUT0;
            if(index >= 0)
                UserData->WR(index, _output, false);
            break;

        case dialog_stepedit_add::ADD_INPUT:
            if(!StepEdit->Insert_WaitInput(m_nSelectedIndex))
            {
                msg = (dialog_message*)gGetDialog(DIG_MSG);
                msg->SetColor(dialog_message::RED);
                msg->Title(tr("Error"));
                msg->Message(tr("Fail to Add User InputWait!"),
                             tr("Over Max. Number or Fail to write file."));
                msg->exec();
                m_nSelectedIndex = selected_index_old;
                return;
            }
            StepEdit->m_Step[m_nSelectedIndex].NickName = dig_add->GetNickName();
            // write 1st value.
            ST_USERDATA_INPUT _input;
            UserData->Set_1stValue(&_input);
            index = StepEdit->m_Step[m_nSelectedIndex].Id;
            index -= HyStepData::ADD_IN0;
            if(index >= 0)
                UserData->WR(index, _input, false);
            break;

        case dialog_stepedit_add::ADD_TIME:
            if(!StepEdit->Insert_WaitTime(m_nSelectedIndex))
            {
                msg = (dialog_message*)gGetDialog(DIG_MSG);
                msg->SetColor(dialog_message::RED);
                msg->Title(tr("Error"));
                msg->Message(tr("Fail to Add User TimeWait!"),
                             tr("Over Max. Number or Fail to write file."));
                msg->exec();
                m_nSelectedIndex = selected_index_old;
                return;
            }
            StepEdit->m_Step[m_nSelectedIndex].NickName = dig_add->GetNickName();
            // write 1st value.
            ST_USERDATA_TIME _time;
            UserData->Set_1stValue(&_time);
            index = StepEdit->m_Step[m_nSelectedIndex].Id;
            index -= HyStepData::ADD_TIME0;
            if(index >= 0)
                UserData->WR(index, _time, false);
            break;

        case dialog_stepedit_add::ADD_WORK:
            if(dig_sasang_addstep == 0)
                dig_sasang_addstep = new dialog_sasang_addstep();

            if(dig_sasang_addstep->exec() != QDialog::Accepted)
            {
                m_nSelectedIndex = selected_index_old;
                return;
            }

            ST_SSGROUP_DATA data;
            if(!dig_sasang_addstep->GetSelected(data))
            {
                m_nSelectedIndex = selected_index_old;
                return;
            }

            // selected sasang group macro. & name(=nickname)
            QString temp_nick = data.name;
            QStringList temp_macro;
            Sasang->Group->MakingOneProgram(data, temp_macro);

            if(!StepEdit->Insert_Work(m_nSelectedIndex, temp_macro))
            {
                msg = (dialog_message*)gGetDialog(DIG_MSG);
                msg->SetColor(dialog_message::RED);
                msg->Title(tr("Error"));
                msg->Message(tr("Fail to Add User Work!"),
                             tr("Over Max. Number or Fail to write file."));
                msg->exec();
                m_nSelectedIndex = selected_index_old;
                return;
            }
            StepEdit->m_Step[m_nSelectedIndex].NickName = temp_nick;
            // write 1st value.
            ST_USERDATA_WORK _work;
            UserData->Set_1stValue(&_work);
            index = StepEdit->m_Step[m_nSelectedIndex].Id;
            index -= HyStepData::ADD_WORK0;
            if(index >= 0)
                UserData->WR(index, _work, false);
            break;

        }

        StepEdit->Write_Main();
        Redraw_List(m_nStartIndex);
        m_bAddUserStep = true;
    }

}
void dialog_stepedit::onDelete()
{
    // for debug
    if(!StepEdit->IsCanDelete(m_nSelectedIndex))
        return;

    QString step_name = QString().setNum(m_nSelectedIndex+1);
    step_name += ". ";
    step_name += StepEdit->MakeDispName(m_nSelectedIndex);

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDelete->text());
    conf->Message(tr("Would you want to Delete?"), step_name);

    if(conf->exec() != QDialog::Accepted)
        return;

    StepEdit->Delete(m_nSelectedIndex);
    if(StepEdit->m_Step.size() <= m_nSelectedIndex)
        m_nSelectedIndex--;

    StepEdit->Write_Main();
    Redraw_List(m_nStartIndex);

}
void dialog_stepedit::onDetail()
{
    /*//for debug
    if(!StepEdit->HasStep(HyStepData::EXT_WAIT))
    {
        StepEdit->Insert_forMode(HyStepData::EXT_WAIT);
        StepEdit->Write_Main();
        Redraw_List(m_nStartIndex);
    }
    else
    {
        StepEdit->Delete_forMode(HyStepData::EXT_WAIT);
        StepEdit->Write_Main();
        m_nSelectedIndex = 0;
        Redraw_List(m_nStartIndex);
    }*/

    int stepid = StepEdit->m_Step[m_nSelectedIndex].Id;

//    if(stepid >= HyStepData::ADD_POS0
//    && stepid < HyStepData::ADD_POS0 + MAX_USER_STEP_POS)
//    {
//        StepEdit->Read_Main();
//        posteach = (dialog_position_teaching*)gGetDialog(DIG_POS_TEACH);
//        posteach->Init(dialog_position_teaching::USER, stepid - HyStepData::ADD_POS0);
//        if(posteach->exec() == QDialog::Accepted) {}
////        frmPosition->Init(m_nSelectedIndex,tr("User Position"));
////        Stacked->setCurrentWidget(frmPosition);
////        Stacked->show();
//    }

    if(stepid >= HyStepData::ADD_POS0
    && stepid < HyStepData::ADD_POS0 + MAX_USER_STEP_POS)
    {
        StepEdit->Read_Main();
        frmUserPos->Init(m_nSelectedIndex, stepid, form_stepedit_userposition::USERPOS, tr("User Position"));
        Stacked->setCurrentWidget(frmUserPos);
        Stacked->show();
    }
    else if(stepid >= HyStepData::ADD_WORK0
    && stepid < HyStepData::ADD_WORK0 + MAX_USER_STEP_WORK)
    {
        StepEdit->Read_Main();
        frmUserPos->Init(m_nSelectedIndex, stepid, form_stepedit_userposition::USERWORK, tr("S-Work"));
        Stacked->setCurrentWidget(frmUserPos);
        Stacked->show();
    }
    else if(stepid == HyStepData::WAIT)
    {
        frmBase->Init(m_nSelectedIndex,tr("Wait Step"),HyStepData::WAIT);
        Stacked->setCurrentWidget(frmBase);
        Stacked->show();
    }
    else if(stepid == HyStepData::TAKEOUT)
    {
        frmBase->Init(m_nSelectedIndex,tr("Take-out Step"), HyStepData::TAKEOUT);
        Stacked->setCurrentWidget(frmBase);
        Stacked->show();
    }
    else if(stepid == HyStepData::UP)
    {
        frmBase->Init(m_nSelectedIndex,tr("Up Step"), HyStepData::UP);
        Stacked->setCurrentWidget(frmBase);
        Stacked->show();
    }
    else if(stepid == HyStepData::UNLOAD)
    {
        frmBase->Init(m_nSelectedIndex,tr("Unload Step"), HyStepData::UNLOAD);
        Stacked->setCurrentWidget(frmBase);
        Stacked->show();
        m_bUpdate = true;
    }
    else if(stepid == HyStepData::INSERT)
    {
        frmBase->Init(m_nSelectedIndex,tr("Grip Step for Insert"), HyStepData::INSERT);
        Stacked->setCurrentWidget(frmBase);
        Stacked->show();
    }
    else if(stepid == HyStepData::INSERT_UNLOAD)
    {
        frmBase->Init(m_nSelectedIndex,tr("Unload Step for Insert"), HyStepData::INSERT_UNLOAD);
        Stacked->setCurrentWidget(frmBase);
        Stacked->show();
    }
    else if(stepid >= HyStepData::ADD_OUT0
    && stepid < HyStepData::ADD_OUT0 + MAX_USER_STEP_OUT)
    {
        StepEdit->Read_Main();
        frmOutput->Init(m_nSelectedIndex,tr("User Output"), stepid - HyStepData::ADD_OUT0);
        Stacked->setCurrentWidget(frmOutput);
        Stacked->show();
    }
    else if(stepid >= HyStepData::ADD_IN0
    && stepid < HyStepData::ADD_IN0 + MAX_USER_STEP_IN)
    {
        StepEdit->Read_Main();
        frmInputTime->Init(m_nSelectedIndex,tr("User Wait Input"), stepid - HyStepData::ADD_IN0, false);
        Stacked->setCurrentWidget(frmInputTime);
        Stacked->show();
    }
    else if(stepid >= HyStepData::ADD_TIME0
    && stepid < HyStepData::ADD_TIME0 + MAX_USER_STEP_TIME)
    {
        StepEdit->Read_Main();
        frmInputTime->Init(m_nSelectedIndex,tr("User Wait Time"), stepid - HyStepData::ADD_TIME0, true);
        Stacked->setCurrentWidget(frmInputTime);
        Stacked->show();
    }

}

void dialog_stepedit::onOtherClose()
{
    if(m_bUpdate)
    {
        m_bUpdate = false;
        Update(); // if weight setting change, it add or delete weight measure & reset step.
    }
    Stacked->hide();
}

void dialog_stepedit::Update()
{
    StepEdit->Read_Main();
    CheckMode_ChangeList();
    Redraw_List(m_nStartIndex);
}

void dialog_stepedit::Redraw_List(int start_index)
{

    if(StepEdit->m_Step.size() <= m_vtNo.size())
        start_index = 0;

    int cal_size = StepEdit->m_Step.size() - start_index;
    QString strNo,strName;

    SelectLine(m_vtNo.size()); // selectline all clear.

    for(int i=0;i<m_vtNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            strNo.setNum((start_index + i) + 1);
            /*strName = StepEdit->m_Step[start_index + i].DispName;
            if(StepEdit->HasNickname(start_index + i))
                strName += " ("+ StepEdit->m_Step[start_index + i].NickName + ")"; //nickname format
            */
            strName = StepEdit->MakeDispName(start_index + i);
            // check display
            if(StepEdit->IsMode(start_index + i))
            {
                if(StepEdit->IsSubMode(start_index + i))
                    CheckLine(i, SUB_MODE);
                else
                    CheckLine(i, MODE);
            }
            else
                CheckLine(i, USER);

        }
        else
        {
            // don't have data.
            strNo.clear();
            strName.clear();

            // check display
            CheckLine(i, NO_CHECK);
        }

        m_vtNo[i]->setText(strNo);
        m_vtName[i]->setText(strName);

        // change select line.
        if((start_index + i) == m_nSelectedIndex)
        {
            SelectLine(i);

        }

        // change icon
        /*if((start_index + i) == m_nPlayIndex)
            IconLine(i,m_nPlayStatus);
        else
            IconLine(i, NO_STATUS);*/

        IconLine(i, NO_STATUS);

    }

    // display total step num
    QString strTotalNum = QString().setNum(StepEdit->m_Step.size());
    ui->lbStepTotalNum->setText(strTotalNum);

    // button enable control
    Control_SideButton(m_nSelectedIndex);
}

void dialog_stepedit::SelectLine(int line)
{
    if(pxmSelectLine.isEmpty())
    {
        QImage img;
        img.load(":/image/image/stepedit_select2.png");
        pxmSelectLine.append(QPixmap::fromImage(img));
    }

    if(pxmSelectLine.isEmpty())
        return;

    for(int i=0;i<m_vtLine.count();i++)
    {
        if(i == line)
            m_vtLine[i]->setPixmap(pxmSelectLine[0]);
        else
            m_vtLine[i]->setPixmap(0);
    }
}

void dialog_stepedit::onSelectLine()
{
    if(m_bTestMode) return;

    QLabel3* btn = (QLabel3*)sender();

    for(int i=0;i<m_vtNo.count();i++)
    {
        if(btn == m_vtIcon[i] || btn == m_vtNo[i] || btn == m_vtName[i]
        || btn == m_vtLine[i] || btn == m_vtCheck[i])
        {
            if(m_vtNo[i]->text().isEmpty())
               return;

            SelectLine(i);
            m_nSelectedIndex = m_vtNo[i]->text().toInt()-1;
            Control_SideButton(m_nSelectedIndex);
            qDebug("Selected Index = %d", m_nSelectedIndex);
            return;
        }
    }
}

void dialog_stepedit::CheckLine(int line, ENUM_CHECK check)
{
    if(pxmCheckLine.isEmpty())
    {
        QImage img;
        pxmCheckLine.append(0); // NO_CHECK
        img.load(":/image/image/stepedit_mode2.png");
        pxmCheckLine.append(QPixmap::fromImage(img));   // MODE
        img.load(":/image/image/stepedit_user2.png");
        pxmCheckLine.append(QPixmap::fromImage(img));   // USER
        img.load(":/image/image/stepedit_mode.png");
        pxmCheckLine.append(QPixmap::fromImage(img));   // SUB_MODE
    }

    if((int)check < pxmCheckLine.size())
        m_vtCheck[line]->setPixmap(pxmCheckLine[check]);
}

void dialog_stepedit::CheckLineOff()
{
    for(int i=0;i<m_vtCheck.count();i++)
        m_vtCheck[i]->setPixmap(0);
}


void dialog_stepedit::onPressBtm()
{
    QLabel3* btn = (QLabel3*)sender();

    QImage img;
    img.load(":/image/image/stepedit_btn4.png");
    QPixmap pxm = QPixmap::fromImage(img);
    if(btn == ui->btnListUp)
    {
        ui->btnListUp->setPixmap(pxm);
    }
    else if(btn == ui->btnListDown)
    {
        ui->btnListDown->setPixmap(pxm);
    }

}
void dialog_stepedit::onReleaseBtm()
{
    QLabel3* btn = (QLabel3*)sender();
    QImage img;
    QPixmap pxm;

    if(btn == ui->btnListUp)
    {
        img.load(":/image/image/stepedit_btnup.png");
        pxm = QPixmap::fromImage(img);
        ui->btnListUp->setPixmap(pxm);
    }
    else if(btn == ui->btnListDown)
    {
        img.load(":/image/image/stepedit_btndown.png");
        pxm = QPixmap::fromImage(img);
        ui->btnListDown->setPixmap(pxm);
    }
}

void dialog_stepedit::Control_SideButton(int list_index)
{
    bool btemp;

    btemp = StepEdit->IsCanDelete(list_index);
    ui->btnDelete->setEnabled(btemp);
    ui->btnDeletePic->setEnabled(btemp);
    ui->btnDeleteIcon->setEnabled(btemp);

    btemp = StepEdit->IsCanStepMove(list_index);
    ui->btnStepUp->setEnabled(btemp);
    ui->btnStepUpPic->setEnabled(btemp);
    ui->btnStepUpIcon->setEnabled(btemp);
    ui->btnStepDown->setEnabled(btemp);
    ui->btnStepDownPic->setEnabled(btemp);
    ui->btnStepDownIcon->setEnabled(btemp);

    ui->wigRename->setEnabled(StepEdit->IsCanRename(list_index));

}

void dialog_stepedit::onRename()
{
    if(!StepEdit->IsCanRename(m_nSelectedIndex)) return;

    dialog_keyboard* kb = new dialog_keyboard(this);
    kb->m_kb->SetTitle(ui->btnRename->text());
    kb->m_kb->SetText(StepEdit->m_Step[m_nSelectedIndex].NickName);
    if(kb->exec() == QDialog::Accepted)
    {
        StepEdit->m_Step[m_nSelectedIndex].NickName = kb->m_kb->GetText();
        StepEdit->Write_Main();
        Redraw_List(m_nStartIndex);
    }
}

void dialog_stepedit::onProgram()
{
    QString prg_name;
    prg_name = StepEdit->m_Step[m_nSelectedIndex].ProgLine;
    prg_name = prg_name.remove("Call").trimmed();
    qDebug() << "Welcome in" << prg_name;

    wigProgram->Init(prg_name);
    wigProgram->show();
}


/** ====================== **/
/** Step Test Running Part **/
/** ====================== **/

void dialog_stepedit::onTest()
{

    if(m_bTestMode)
    {
        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::SKYBLUE);
        conf->Title(tr("Notice"));
        conf->Message(tr("Do you want to finish test mode?"));
        if(conf->exec() != QDialog::Accepted)
            return;

        // make off
        SetTestMode(false);

        gReadyMacro(false);
        Redraw_List(m_nStartIndex);

        ServoOnMode(SON_MODE_NORMAL);

        float main_speed = Param->Get(HyParam::MAIN_SPEED).toFloat();
        SetSpeed(main_speed);

        if(!ui->wigTest->isEnabled())
            ui->wigTest->setEnabled(true);
    }
    else
    {
        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::SKYBLUE);
        conf->Title(tr("Notice"));
        conf->Message(tr("Do you want to entry test mode?"));
        if(conf->exec() != QDialog::Accepted)
            return;

        EntryHome();

        Delaying(1000);

        gReadyMacro(true);
        LoadProgram();

        // make on
        SetTestMode(true);

        m_nTaskStatus_Old = -1; // make no same.
        m_strNowProgName.clear();m_strNowProgName_Old.clear(); // make same.

        ServoOnMode(SON_MODE_SPECIAL);

        m_TestSpeed = STEPTEST_SPEED_INIT;
        SetSpeed(m_TestSpeed);
    }


}

void dialog_stepedit::SetTestMode(bool on)
{
    if(on)
    {
        ui->grpEditBtn->hide();
        ui->grpTestBtn->show();

        timerRun->start();

        SelectLine(-1); // line all off;


    }
    else
    {
        ui->grpEditBtn->show();
        ui->grpTestBtn->hide();

        timerRun->stop();

        // for high level program run view
        if(!wigProgramRun->isHidden())
            wigProgramRun->hide();
    }

    m_bTestMode = on;

    ui->btnTest->setAutoFillBackground(on);

    ui->wigRename->setEnabled(!on);

    ui->wigCheck->setEnabled(!on);

    if(m_nUserLevel < USER_LEVEL_HIGH)
        ui->wigCode->setEnabled(false);
    else
        ui->wigCode->setEnabled(!on);

    ui->grpEnd->setEnabled(!on);

    ui->btnListUp->setEnabled(!on);
    ui->btnListDown->setEnabled(!on);

    // Recipe > bTestMode set.
    Recipe->Set(HyRecipe::bTestMode, (float)on, false);
}


void dialog_stepedit::onRunTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    int _movestep=-1,_planstep=-1,_mainstep=0;
    pCon->getRunningStepIndex(0, &_movestep, &_planstep);
    pCon->getRunningMainStepIndex(&_mainstep);
    m_nTaskStatus =  pCon->getTaskStatus(0);

    m_nMainRunIndex = _mainstep-1;
    m_nSubRunIndex = _movestep-1;
    m_nSubPlanIndex = _planstep-1;

    // test monitoring.
    ui->lbStepIndex->setText(QString().setNum(m_nSubRunIndex));
    ui->lbPlanIndex->setText(QString().setNum(m_nSubPlanIndex));
    ui->lbMainStepIndex->setText(QString().setNum(m_nMainRunIndex));
    ui->lbTaskStatus->setText(QString().setNum((int)m_nTaskStatus));


    DisplayRunIcon(m_nMainRunIndex , m_nTaskStatus);
    Control_PlayButton(m_nTaskStatus, m_bHome);

    // add here....



    // high level user program running structure. (must last)

    if(m_nUserLevel < USER_LEVEL_HIGH)
        return;

    m_strNowProgName = pCon->getCurProgramName(0);

    if(m_strNowProgName != m_strNowProgName_Old)
    {
        qDebug() << m_strNowProgName;

        if(m_strNowProgName == MACRO_MAIN)
        {
            wigProgramRun->hide();
        }
        else
        {
            //wigProgramRun->hide();
            wigProgramRun->Init(m_strNowProgName);
            wigProgramRun->show();

        }
    }
    m_strNowProgName_Old = m_strNowProgName;

    if(!wigProgramRun->isHidden())
        wigProgramRun->CyclicUpdate_Index(m_nSubRunIndex,
                                          m_nSubPlanIndex,
                                          m_nTaskStatus);

}
void dialog_stepedit::onStepFwd()
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret;
    if(m_nUserLevel < USER_LEVEL_HIGH)
    {
        QString prog_name = pCon->getCurProgramName(MAIN_TASK, true);
        if(prog_name == MACRO_MAIN)
        {
            qDebug() << "Step > StepOver";
            ret = pCon->runNxtStepOver(MAIN_TASK);
            if(ret < 0)
            {
                qDebug() << "dialog_stepedit::onStepFwd() runNxtStepOver ret = " << ret;
                return;
            }
        }
        else
        {
            qDebug() << "Step > Continue";
            ret = pCon->continueProgram2(MAIN_TASK, 4); // option4 : run to until clear stacked program.
            if(ret < 0)
            {
                qDebug() << "dialog_stepedit::onStepFwd() continueProgram2(4) ret = " << ret;
                return;
            }
        }
    }
    else
    {
        ret = pCon->runNxtStep(MAIN_TASK);
        if(ret < 0)
        {
            qDebug() << "dialog_stepedit::runNxtStep ret=" << ret;
            return;
        }
    }

}

void dialog_stepedit::onStepBwd() // not use
{
//    CNRobo* pCon = CNRobo::getInstance();

//    CNR_TASK_STATUS taskstatus =  pCon->getTaskStatus(0);

//    if(taskstatus == CNR_TASK_NOTUSED)
//        return;

//    int ret;
//    if(m_nUserLevel < USER_LEVEL_HIGH)
//    {
//        ret = pCon->runBackStepOver(0);
//        qDebug() << "runBackStepOver ret=" << ret;
//    }
//    else
//    {
//        ret = pCon->runBackStep(0);
//        qDebug() << "runBackStep ret=" << ret;
//    }
}

void dialog_stepedit::onCycle() // not use
{
//    CNRobo* pCon = CNRobo::getInstance();
//    CNR_TASK_STATUS taskstatus =  pCon->getTaskStatus(0);
//    if(taskstatus != CNR_TASK_NOTUSED)
//        return;

//    LoadProgram();
//    int ret = pCon->continueProgram(0);
//    qDebug() << "setProgramRun ret=" << ret;
}

void dialog_stepedit::onPause()
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->holdProgram(MAIN_TASK);
    if(ret < 0)
    {
        qDebug() << "dialog_stepedit::onPause holdProgram ret=" << ret;
        return;
    }
}

void dialog_stepedit::onRun()
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->continueProgram2(MAIN_TASK, 0);
    if(ret < 0)
    {
        qDebug() << "dialog_stepedit::onRun() continueProgram2(0) ret=" << ret;
        return;
    }
}

void dialog_stepedit::onReset()
{
    // program reset concept.
    gReadyMacro(true);
    LoadProgram();
}

bool dialog_stepedit::LoadProgram()
{
    CNRobo* pCon = CNRobo::getInstance();

    QString _program = MACRO_MAIN;
    int ret;

    Delaying(1000);

    //ret = pCon->setCurProgram(MAIN_TASK, _program, 1); // one cycle
    //ret = pCon->setCurProgram(MAIN_TASK, _program, 2); // two cycle
    ret = pCon->setCurProgram(MAIN_TASK, _program, -1); // infinite cycle
    if(ret < 0)
    {
        qDebug() << "dailog_stepedit::LoadProgram() setCurProgram ret=" << ret;
        return false;
    }

    Delaying(1000);

    ret = pCon->resetCurProgram(MAIN_TASK);
    if(ret < 0)
    {
        qDebug() << "dailog_stepedit::LoadProgram() resetCurProgram ret=" << ret;
        return false;
    }

    return true;
}
bool dialog_stepedit::CleanProgram()
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->clearCurProgram(MAIN_TASK);
    if(ret < 0)
    {
        qDebug() << "dailog_stepedit::CleanProgram() clearCurProgram ret=" << ret;
        return false;
    }

    return true;
}

bool dialog_stepedit::ResetProgram()
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->resetCurProgram(MAIN_TASK);
    if(ret < 0)
    {
        qDebug() << "dailog_stepedit::ResetProgram() resetCurProgram ret=" << ret;
        return false;
    }

    return true;
}

void dialog_stepedit::IconLine(int line, ENUM_PLAY_STATUS status)
{
    if(pxmIconLine.isEmpty())
    {
        QImage img;
        pxmIconLine.append(0);
        img.load(":/icon/icon/icon99-5.png");
        pxmIconLine.append(QPixmap::fromImage(img)); // STOP
        img.load(":/icon/icon/icon99-4.png");
        pxmIconLine.append(QPixmap::fromImage(img)); // PAUSE
        img.load(":/icon/icon/icon99-3.png");
        pxmIconLine.append(QPixmap::fromImage(img)); // PLAY
    }

    for(int i=0;i<m_vtIcon.count();i++)
        m_vtIcon[i]->setPixmap(0);

    if((int)status < pxmIconLine.size())
        m_vtIcon[line]->setPixmap(pxmIconLine[status]);
}

void dialog_stepedit::DisplayRunIcon(int run_index, CNR_TASK_STATUS task_status)
{
    // run_index = 0~N
    if(run_index < 0) run_index = 0;

    // run_index match list view change
    if(run_index >= (m_nStartIndex + m_vtLine.size()))
    {
        m_nStartIndex = run_index - (m_vtLine.size()-1);
        Redraw_List(m_nStartIndex);
        SetTestMode(true);
    }
    else if(run_index < m_nStartIndex)
    {
        m_nStartIndex = run_index;
        Redraw_List(m_nStartIndex);
        SetTestMode(true);
    }

    // cofirm line_index
    // must run_index >= m_nStartIndex
    int line_index = run_index - m_nStartIndex;

    ENUM_PLAY_STATUS play_status;
    if(task_status == CNR_TASK_RUNNING)
        play_status = PLAY;
    else if(task_status == CNR_TASK_STOPPED)
        play_status = PAUSE;
    else
        play_status = STOP;


    IconLine(line_index, play_status);

}

void dialog_stepedit::onHome()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(tr("Notice"));
    conf->Message(tr("Do you want to homing?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    float main_speed = Param->Get(HyParam::MAIN_SPEED).toFloat();
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setSpeed(main_speed);

    EntryHome();

    gReadyMacro(true);
    LoadProgram();

    SetSpeed(m_TestSpeed);
}

void dialog_stepedit::Control_PlayButton(CNR_TASK_STATUS task_status, bool homed)
{
    //if(m_nTaskStatus_Old == task_status)
    //    return;

    bool bStepRun,bContinue,bHold,bAbort,bHome,bTest;

    if(!homed)
    {
        bStepRun = false;
        bContinue = false;
        bHold = false;
        bAbort = false;
        bHome = true;
        bTest = true;
    }
    else if(task_status <= CNR_TASK_NOTUSED)
    {
        // Only Reset
        bStepRun = false;
        bContinue = false;
        bHold = false;
        bAbort = true;
        bHome = true;
        bTest = true;
    }
    else if(task_status == CNR_TASK_RUNNING)
    {
        bStepRun = false;
        bContinue = false;
        bHold = true;
        bAbort = false;
        bHome = false;
        bTest = false;
    }
    else // CNR_TASK_STOPPED
    {
        bStepRun = true;
        bContinue = true;
        bHold = false;
        bAbort = true;
        bHome = true;
        bTest = true;
    }

    ui->btnStepRun->setEnabled(bStepRun);
    ui->btnStepRunPic->setEnabled(bStepRun);
    ui->btnStepRunIcon->setEnabled(bStepRun);

    ui->btnStepBwd->setEnabled(bStepRun);
    ui->btnStepBwdPic->setEnabled(bStepRun);
    ui->btnStepBwdIcon->setEnabled(bStepRun);

    ui->btnContinue->setEnabled(bContinue);
    ui->btnContinuePic->setEnabled(bContinue);
    ui->btnContinueIcon->setEnabled(bContinue);

    ui->btnHold->setEnabled(bHold);
    ui->btnHoldPic->setEnabled(bHold);
    ui->btnHoldIcon->setEnabled(bHold);

    ui->btnAbort->setEnabled(bAbort);
    ui->btnAbortPic->setEnabled(bAbort);
    ui->btnAbortIcon->setEnabled(bAbort);

    ui->btnHome->setEnabled(bHome);
    ui->btnHomePic->setEnabled(bHome);
    ui->btnHomeIcon->setEnabled(bHome);

    ui->wigTest->setEnabled(bTest);

    /*QImage img;
    img.load(":/image/image/button_midrect4_0.png");
    QPixmap pxm = QPixmap::fromImage(img);

    if(!ui->btnStepRunPic->isEnabled())
    {
        if(ui->btnStepRunPic->pixmap()->toImage() != img)
            ui->btnStepRunPic->setPixmap(pxm);
    }
    if(!ui->btnStepBwdPic->isEnabled())
    {
        if(ui->btnStepBwdPic->pixmap()->toImage() != img)
            ui->btnStepBwdPic->setPixmap(pxm);
    }
    if(!ui->btnContinuePic->isEnabled())
    {
        if(ui->btnContinuePic->pixmap()->toImage() != img)
            ui->btnContinuePic->setPixmap(pxm);
    }
    if(!ui->btnHoldPic->isEnabled())
    {
        if(ui->btnHoldPic->pixmap()->toImage() != img)
            ui->btnHoldPic->setPixmap(pxm);
    }
    if(!ui->btnAbortPic->isEnabled())
    {
        if(ui->btnAbortPic->pixmap()->toImage() != img)
            ui->btnAbortPic->setPixmap(pxm);
    }
    if(!ui->btnHomePic->isEnabled())
    {
        if(ui->btnHomePic->pixmap()->toImage() != img)
            ui->btnHomePic->setPixmap(pxm);
    }*/

    m_nTaskStatus_Old = task_status;
}


void dialog_stepedit::Delaying(int msec)
{
    dialog_delaying* dig = new dialog_delaying();
    dig->Init(msec);
    dig->exec();
}

bool dialog_stepedit::ServoOnMode(int mode)
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->setServoOnMode(mode);
    if(ret < 0)
    {
        qDebug() << "setServoOnMode ret=" << ret;
        return false;
    }
    return true;
}

bool dialog_stepedit::Home(bool bCheck)
{
    timerRun->stop();
    ServoOnMode(SON_MODE_NORMAL);

    if(dig_home2 == 0)
        dig_home2 = new dialog_home2();

    if(bCheck)
        dig_home2->Enable_Check();
    int res = dig_home2->exec();

    timerRun->start();

    Delaying(1000);
    ServoOnMode(SON_MODE_SPECIAL);

    if(res == QDialog::Accepted)
        return true;

    return false;
}

void dialog_stepedit::EntryHome()
{
    if(!Home(true))
    {
        // no home, All control disable...
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Notice")));
        msg->Message(tr("Home not completed!!"),
                     tr("Please make sure home."));
        msg->exec();
        m_bHome = false;
    }
    else
    {
        m_bHome = true;
    }
}

bool dialog_stepedit::SetSpeed(float speed)
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->setSpeed(speed);
    if(ret < 0)
    {
        qDebug() << "dailog_stepedit::setSpeed() ret=" << ret;
        return false;
    }

    m_TestSpeed = speed;
    ui->pbarSpeed->setValue(m_TestSpeed);
    return true;
}

void dialog_stepedit::onTestSpdUp()
{
    if(m_TestSpeed > STEPTEST_SPEED_MAX)
        return;

    float speed = m_TestSpeed + STEPTEST_SPEED_GAP;
    SetSpeed(speed);
}
void dialog_stepedit::onTestSpdDown()
{
    if(m_TestSpeed <= STEPTEST_SPEED_MIN)
        return;

    float speed = m_TestSpeed - STEPTEST_SPEED_GAP;
    SetSpeed(speed);
}
