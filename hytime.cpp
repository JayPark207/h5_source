#include "hytime.h"

HyTime::HyTime(HyRecipe *recipe)
{
    m_Recipe = recipe;

    Time.clear();
    Time.fill(0.0, TIME_MAX_NUM);
    Name.clear();
    Name.fill(QString(""), TIME_MAX_NUM);

    m_strArrName = QString("time");  // fix
}

void HyTime::Init()
{
    Name[DELAY] = QString(tr("Before WAIT delay"));
    Name[CHUCK_LIMIT] = QString(tr("Chuck Limit"));
    Name[VAC1_LIMIT] = QString(tr("Vac.1 Limit"));
    Name[VAC2_LIMIT] = QString(tr("Vac.2 Limit"));
    Name[VAC3_LIMIT] = QString(tr("Vac.3 Limit"));
    Name[VAC4_LIMIT] = QString(tr("Vac.4 Limit"));
    Name[GRIP_LIMIT] = QString(tr("Gripper Limit"));
    Name[CHUCK_DELAY] = QString(tr("Chuck Delay"));
    Name[TAKEOUT_DELAY] = QString(tr("Take-out Delay"));
    Name[TEMP_DELAY] = QString(tr("Temp. Measure Delay"));
    Name[WEIGHT_DELAY] = QString(tr("Weight Measure Delay"));
    Name[CONV_ON] = QString(tr("Conveyor On Time"));
    Name[PROD_UNLOAD_DELAY] = QString(tr("Prod. Unload Delay"));
    Name[INSERT_GRIP_DELAY] = QString(tr("Insert Grip Delay"));
    Name[INSERT_GRIP_AFTER_DELAY] = QString(tr("Insert Grip After Delay"));
    Name[PROD_UNLOAD_AFTER_DELAY] = QString(tr("Prod. After Unload Delay"));
    Name[NIPPER_LIMIT] = QString(tr("Nipper Limit"));
    Name[RGRIP_LIMIT] = QString(tr("Gripper2 Limit"));
    Name[WEIGHT_RESET_DELAY] = QString(tr("Weight Reset Delay"));

    // not use
    //Name[RGRIP_LIMIT] = QString(tr("Runner Gripper Limit"));
    //Name[IONIZER_ON] = QString(tr("Ionizer On Time"));
    //Name[RUN_UNLOAD_DELAY] = QString(tr("Run. Unload Delay"));
    // add here...

}

void HyTime::AllZero()
{
    for(int i=0;i<TIME_MAX_NUM;i++)
    {
        WR(i, 0.0);
    }
}

QString HyTime::GetName(int i)
{
    return Name.at(i);
}

int HyTime::GetNum()
{
    return (int)TIME_MAX_NUM;
}

bool HyTime::WR(int i, float time)
{
    if(!m_Recipe->SetArr(m_strArrName, i, time))
        return false;
    return true;
}
bool HyTime::RD(int i, float *time)
{
    if(!m_Recipe->GetArr(m_strArrName, i, time))
        return false;
    return true;
}

bool HyTime::Read()
{
    if(!m_Recipe->GetsArr(m_strArrName, TIME_MAX_NUM, Time))
        return false;
    return true;
}
bool HyTime::Write()
{
    if(!m_Recipe->SetsArr(m_strArrName, Time))
        return false;
    return true;
}

void HyTime::Set(int i, float time)
{
    Time[i] = time;
}
float HyTime::Get(int i)
{
    return Time.at(i);
}


