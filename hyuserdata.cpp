#include "hyuserdata.h"

HyUserData::HyUserData(HyRecipe* recipe)
{
    m_Recipe = recipe;

    // struct 순서대로.
    m_UserPos_VarsName.clear();
    m_UserPos_VarsName.append("udp_tpos");
    m_UserPos_VarsName.append("#udp_jpos");
    m_UserPos_VarsName.append("udp_spd");
    m_UserPos_VarsName.append("udp_delay");
    m_UserPos_VarsName.append("udp_type");

    m_UserWork_VarsName.clear();
    m_UserWork_VarsName.append("udw_tpos");
    m_UserWork_VarsName.append("#udw_jpos");
    m_UserWork_VarsName.append("udw_spd");
    m_UserWork_VarsName.append("udw_delay");
    m_UserWork_VarsName.append("udw_type");

    int i;

    m_UserOut_VarsName.clear();
    for(i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++)
        m_UserOut_VarsName.append(QString("udo_sel%1").arg(i));

    m_UserOut_VarsName.append("udo_delay");
    m_UserOut_VarsName.append("udo_type");
    m_UserOut_VarsName.append("udo_ontime");

    m_UserIn_VarsName.clear();
    for(i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
       m_UserIn_VarsName.append(QString("udi_sel%1").arg(i));

    m_UserIn_VarsName.append("udi_delay");
    m_UserIn_VarsName.append("udi_type");
    m_UserIn_VarsName.append("udi_cond");

    m_UserTime_VarsName.clear();
    m_UserTime_VarsName.append("udt_delay");

}

void HyUserData::AllZero()
{
    ST_USERDATA_POS         UserPos;
    ST_USERDATA_OUTPUT      UserOut;
    ST_USERDATA_INPUT       UserIn;
    ST_USERDATA_TIME        UserTime;
    ST_USERDATA_WORK        UserWork;

    SetBuf_Default(&UserPos);
    SetBuf_Default(&UserOut);
    SetBuf_Default(&UserIn);
    SetBuf_Default(&UserTime);
    SetBuf_Default(&UserWork);

    int i;
    for(i=0;i<MAX_USER_STEP_POS;i++)
        WR(i, UserPos);
    for(i=0;i<MAX_USER_STEP_OUT;i++)
        WR(i, UserOut);
    for(i=0;i<MAX_USER_STEP_IN;i++)
        WR(i, UserIn);
    for(i=0;i<MAX_USER_STEP_TIME;i++)
        WR(i, UserTime);
    for(i=0;i<MAX_USER_STEP_WORK;i++)
        WR(i, UserWork);
}

bool HyUserData::WR(int index, ST_USERDATA_POS st, bool save)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    QString str;
    for(int i=0;i<m_UserPos_VarsName.count();i++)
    {
        str = m_UserPos_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    values.clear();
    cn_variant var;
    // trans
    var.type = CNVAR_TRANS;
    var.val.trans = st.Trans;
    values.append(var);
    // joint
    var.type = CNVAR_JOINT;
    var.val.joint = st.Joint;
    values.append(var);
    // speed
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Speed;
    values.append(var);
    // delay
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Delay;
    values.append(var);
    // type
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Type;
    values.append(var);

    if(!m_Recipe->SetVars(var_names, values, save))
        return false;

    return true;
}

bool HyUserData::WR(int index, ST_USERDATA_WORK st, bool save)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    QString str;
    for(int i=0;i<m_UserWork_VarsName.count();i++)
    {
        str = m_UserWork_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    values.clear();
    cn_variant var;
    // trans
    var.type = CNVAR_TRANS;
    var.val.trans = st.Trans;
    values.append(var);
    // joint
    var.type = CNVAR_JOINT;
    var.val.joint = st.Joint;
    values.append(var);
    // speed
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Speed;
    values.append(var);
    // delay
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Delay;
    values.append(var);
    // type
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Type;
    values.append(var);

    if(!m_Recipe->SetVars(var_names, values, save))
        return false;

    return true;
}

bool HyUserData::WR(int index, ST_USERDATA_OUTPUT st, bool save)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    QString str;
    int i;
    for(i=0;i<m_UserOut_VarsName.count();i++)
    {
        str = m_UserOut_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    values.clear();
    cn_variant var;
    for(i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++)
    {
        var.type = CNVAR_FLOAT;
        var.val.f32 = st.Sel[i];
        values.append(var);
    }
    // delay
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Delay;
    values.append(var);
    // type
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Type;
    values.append(var);
    // ontime
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.OnTime;
    values.append(var);

    if(!m_Recipe->SetVars(var_names, values, save))
        return false;

    return true;
}

bool HyUserData::WR(int index, ST_USERDATA_INPUT st, bool save)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    QString str;
    int i;
    for(i=0;i<m_UserIn_VarsName.count();i++)
    {
        str = m_UserIn_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    values.clear();
    cn_variant var;
    for(i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
    {
        var.type = CNVAR_FLOAT;
        var.val.f32 = st.Sel[i];
        values.append(var);
    }
    // delay
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Delay;
    values.append(var);
    // type
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Type;
    values.append(var);
    // condition
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Condition;
    values.append(var);

    if(!m_Recipe->SetVars(var_names, values, save))
        return false;

    return true;
}
bool HyUserData::WR(int index, ST_USERDATA_TIME st, bool save)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    QString str;
    int i;
    for(i=0;i<m_UserTime_VarsName.count();i++)
    {
        str = m_UserTime_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    values.clear();
    cn_variant var;
    // delay
    var.type = CNVAR_FLOAT;
    var.val.f32 = st.Delay;
    values.append(var);

    if(!m_Recipe->SetVars(var_names, values, save))
        return false;

    return true;
}

bool HyUserData::RD(int index, ST_USERDATA_POS *st)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();

    QString str;
    int i;
    for(i=0;i<m_UserPos_VarsName.count();i++)
    {
        str = m_UserPos_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    SetBuf_Default(st);

    if(!m_Recipe->GetVars(var_names, values))
        return false;

    if(values[0].type >= 0)
        st->Trans = values[0].val.trans;
    if(values[1].type >= 0)
        st->Joint = values[1].val.joint;
    if(values[2].type >= 0)
        st->Speed = values[2].val.f32;
    if(values[3].type >= 0)
        st->Delay = values[3].val.f32;
    if(values[4].type >= 0)
        st->Type = values[4].val.f32;

    return true;
}

bool HyUserData::RD(int index, ST_USERDATA_WORK *st)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();

    QString str;
    int i;
    for(i=0;i<m_UserWork_VarsName.count();i++)
    {
        str = m_UserWork_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    SetBuf_Default(st);

    if(!m_Recipe->GetVars(var_names, values))
        return false;

    if(values[0].type >= 0)
        st->Trans = values[0].val.trans;
    if(values[1].type >= 0)
        st->Joint = values[1].val.joint;
    if(values[2].type >= 0)
        st->Speed = values[2].val.f32;
    if(values[3].type >= 0)
        st->Delay = values[3].val.f32;
    if(values[4].type >= 0)
        st->Type = values[4].val.f32;

    return true;
}

bool HyUserData::RD(int index, ST_USERDATA_OUTPUT *st)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();

    QString str;
    int i;
    for(i=0;i<m_UserOut_VarsName.count();i++)
    {
        str = m_UserOut_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    SetBuf_Default(st);

    if(!m_Recipe->GetVars(var_names, values))
        return false;

    for(i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++)
    {
        if(values[i].type >= 0)
            st->Sel[i] = values[i].val.f32;
    }

    if(values[i].type >= 0)
        st->Delay = values[i].val.f32;
    i++;
    if(values[i].type >= 0)
        st->Type = values[i].val.f32;
    i++;
    if(values[i].type >= 0)
        st->OnTime = values[i].val.f32;

    return true;
}
bool HyUserData::RD(int index, ST_USERDATA_INPUT *st)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();

    QString str;
    int i;
    for(i=0;i<m_UserIn_VarsName.count();i++)
    {
        str = m_UserIn_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    SetBuf_Default(st);

    if(!m_Recipe->GetVars(var_names, values))
        return false;

    for(i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
    {
        if(values[i].type >= 0)
            st->Sel[i] = values[i].val.f32;
    }

    if(values[i].type >= 0)
        st->Delay = values[i].val.f32;
    i++;
    if(values[i].type >= 0)
        st->Type = values[i].val.f32;
    i++;
    if(values[i].type >= 0)
        st->Condition = values[i].val.f32;

    return true;
}

bool HyUserData::RD(int index, ST_USERDATA_TIME *st)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();

    QString str;
    int i;
    for(i=0;i<m_UserTime_VarsName.count();i++)
    {
        str = m_UserTime_VarsName[i];
        str += QString("[%1]").arg(index);
        var_names.append(str);
    }

    SetBuf_Default(st);

    if(!m_Recipe->GetVars(var_names, values))
        return false;

    if(values[0].type >= 0)
        st->Delay = values[0].val.f32;

    return true;
}

void HyUserData::SetBuf_Default(ST_USERDATA_POS *st)
{
    cn_trans _trans;
    cn_joint _joint;

    memset(_joint.joint,0,sizeof(float)*CN_MAX_JOINT);

    CNRobo* pCon = CNRobo::getInstance();
    pCon->calcForward(_joint.joint, _trans);

    st->Trans = _trans;
    st->Joint = _joint;
    st->Speed = 0.0;
    st->Delay = 0.0;
    st->Type = 0.0;
}

void HyUserData::SetBuf_Default(ST_USERDATA_WORK *st)
{
    cn_trans _trans;
    cn_joint _joint;

    memset(_joint.joint,0,sizeof(float)*CN_MAX_JOINT);

    CNRobo* pCon = CNRobo::getInstance();
    pCon->calcForward(_joint.joint, _trans);

    st->Trans = _trans;
    st->Joint = _joint;
    st->Speed = 0.0;
    st->Delay = 0.0;
    st->Type = 0.0;
}

void HyUserData::SetBuf_Default(ST_USERDATA_OUTPUT *st)
{
    for(int i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++)
        st->Sel[i] = 0.0;

    st->Delay = 0.0;
    st->Type = 0.0;
    st->OnTime = 0.0;
}

void HyUserData::SetBuf_Default(ST_USERDATA_INPUT *st)
{
    for(int i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
        st->Sel[i] = 0.0;

    st->Delay = 0.0;
    st->Type = 0.0;
    st->Condition = 0.0;
}

void HyUserData::SetBuf_Default(ST_USERDATA_TIME *st)
{
    st->Delay = 0.0;
}


void HyUserData::Set_1stValue(ST_USERDATA_POS *st)
{
    cn_trans _trans;
    cn_joint _joint;

    memset(_joint.joint,0,sizeof(float)*CN_MAX_JOINT);

    CNRobo* pCon = CNRobo::getInstance();
    pCon->calcForward(_joint.joint, _trans);

    st->Trans = _trans;
    st->Joint = _joint;
    st->Speed = 0.0;
    st->Delay = 0.0;
    st->Type = 0.0;
}
void HyUserData::Set_1stValue(ST_USERDATA_WORK *st)
{
    cn_trans _trans;
    cn_joint _joint;

    memset(_joint.joint,0,sizeof(float)*CN_MAX_JOINT);

    CNRobo* pCon = CNRobo::getInstance();
    pCon->calcForward(_joint.joint, _trans);

    st->Trans = _trans;
    st->Joint = _joint;
    st->Speed = 0.0;
    st->Delay = 0.0;
    st->Type = 0.0;
}
void HyUserData::Set_1stValue(ST_USERDATA_OUTPUT *st)
{
    for(int i=0;i<MAX_USERDATA_OUT_SELECTNUM;i++)
        st->Sel[i] = 0.0;

    st->Delay = 0.0;
    st->Type = 0.0;
    st->OnTime = 0.0;
}

void HyUserData::Set_1stValue(ST_USERDATA_INPUT *st)
{
    for(int i=0;i<MAX_USERDATA_IN_SELECTNUM;i++)
        st->Sel[i] = 0.0;

    st->Delay = 0.0;
    st->Type = 0.0;
    st->Condition = 0.0;
}

void HyUserData::Set_1stValue(ST_USERDATA_TIME *st)
{
    st->Delay = 0.0;
}

