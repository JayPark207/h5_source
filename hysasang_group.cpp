#include "hysasang_group.h"

HySasang_Group::HySasang_Group()
{

}

int HySasang_Group::ReadAll()
{
    // read list
    int ret;
    ret = ReadList(GroupNameList);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::ReadAll() ReadList ret =" << ret;
        return -1;
    }

    // read file data.
    GroupList.clear();

    ST_SSGROUP_DATA temp_data;
    for(int i=0;i<GroupNameList.count();i++)
    {
        temp_data.name = GroupNameList[i];
        ret = ReadFile(temp_data);
        if(ret < 0)
        {
            qDebug() << "HySasang_Group::ReadAll() ReadFile ret =" << ret;
            return -2;
        }

        GroupList.append(temp_data);
    }

    ret = Sorting(GroupList);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::ReadAll() Sorting ret =" << ret;
        return -2;
    }

    return 0;
}

int HySasang_Group::ReadList(QStringList &list)
{
    CNRobo* pCon = CNRobo::getInstance();

    QString path = PATH_SASANG_BASE;
    QStringList file_list;
    int ret;

    ret = pCon->getDatabaseFileList(path, file_list);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::ReadList() getDatabaseFileList ret =" << ret;
        return -1;
    }

    file_list.removeAll(".");
    file_list.removeAll("..");

    for(int i=0;i<file_list.count();i++)
        file_list[i].remove(".ss");

    list = file_list;
    qDebug() << list;
    return 0;
}

bool HySasang_Group::IsExistFile(QString name)
{
    CNRobo* pCon = CNRobo::getInstance();

    QString path = PATH_SASANG_BASE;
    path += "/" + name + ".ss";
    qDebug() << path;

    bool bExist = false;
    if(pCon->checkDirFileExist(path, bExist) < 0)
        return false;

    return bExist;
}

int HySasang_Group::ReadFile(ST_SSGROUP_DATA& data)
{
    CNRobo* pCon = CNRobo::getInstance();

    QString path = PATH_SASANG_BASE;
    path += "/" + data.name + ".ss";

    if(!IsExistFile(data.name))
        return -5;

    QStringList datafile;
    datafile.clear();

    int ret;
    ret = pCon->getProgramFile(path, datafile);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::ReadDataFile() getProgramFile ret =" << ret;
        return -1;
    }

    if(datafile.isEmpty())
        return -2;

    if(!Format2Data(datafile, data))
        return -3;

    return 0;
}

bool HySasang_Group::Format2Data(QStringList format, ST_SSGROUP_DATA &data)
{
    if(format.isEmpty()) return false;

    ST_SSGROUP_DATA result = data;

    // parsing name, datetime ..
    QString str = format.takeFirst();
    if(!str.contains("~")) return false;

    str.remove("~");
    QStringList temp = str.split(",");
    if(temp.count() < 2) return false;

    if(temp[0] != data.name)
        result.name = data.name;
    else
        result.name = temp[0];

    result.datetime = QDateTime::fromString(temp[1].trimmed(), Qt::ISODate);

    // parsing data & program.

    QList<QStringList> pattern;
    pattern.clear();

    QStringList temp_list;
    bool bFlag = false;
    int i,j;
    for(i=0;i<format.count();i++)
    {
        if(!bFlag)
        {
            if(format[i].contains("<"))
            {
                bFlag = true;
                temp_list.clear();
            }
        }
        else
        {
            if(format[i].contains(">"))
            {
                bFlag = false;
                pattern.append(temp_list);
            }
            else
                temp_list << format[i];
        }
    }

    int seperate_index;
    QStringList data_list,prog_list;
    QList<QStringList> temp_prog;

    result.data.clear();
    result.program.clear();

    for(i=0;i<pattern.count();i++)
    {
        seperate_index = pattern[i].indexOf("^macro^");
        if(seperate_index < 0) return false;

        // parsing data.
        data_list = pattern[i].mid(0, seperate_index);
        data_list.removeAll("^macro^");
        result.data.append(data_list);

        // parsing program.
        prog_list = pattern[i].mid(seperate_index);
        prog_list.removeAll("^macro^");

        temp_prog.clear();
        bFlag = false;
        for(j=0;j<prog_list.count();j++)
        {
            if(!bFlag)
            {
                if(prog_list[j].contains("{"))
                {
                    bFlag = true;
                    temp_list.clear();
                }
            }
            else
            {
                if(prog_list[j].contains("}"))
                {
                    bFlag = false;
                    temp_prog.append(temp_list);
                }
                else
                    temp_list << prog_list[j];
            }
        }

        result.program.append(temp_prog);

    }

    data = result;
    return true;
}

bool HySasang_Group::Data2Format(ST_SSGROUP_DATA data, QStringList &format)
{
    QStringList result;
    result.clear();

    QStringList temp;
    temp.append(data.name);
    temp.append(data.datetime.toString(Qt::ISODate));
    QString str;
    str = temp.join(",");
    str.push_front("~");

    result.push_front(str);

    if(data.data.size() != data.program.size())
        return false;

    int i,j;
    for(i=0;i<data.data.count();i++)
    {
        result << "<";
        result << data.data[i];
        result << "^macro^";
        for(j=0;j<data.program[i].count();j++)
        {
            result << "{";
            result << data.program[i].at(j);
            result << "}";
        }
        result << ">";
    }

    format = result;
    return true;
}



int HySasang_Group::SaveFile(ST_SSGROUP_DATA data)
{
    QStringList datafile;
    datafile.clear();

    if(!Data2Format(data, datafile))
        return -2;

    // save file. (include folder making)
    QString file_path = PATH_SASANG_BASE;
    file_path += "/" + data.name + ".ss";

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->createProgramFile(file_path, datafile);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::SaveFile() createProgramFile ret=" << ret;
        return -1;
    }

    qDebug() << datafile;
    return 0;
}

int HySasang_Group::Sorting(QList<ST_SSGROUP_DATA> &group_list)
{
    qSort(group_list.begin(), group_list.end(), CompareDateTime);
    return 0;
}
bool HySasang_Group::CompareDateTime(const ST_SSGROUP_DATA &data1, const ST_SSGROUP_DATA &data2)
{
    return data1.datetime < data2.datetime;
}

bool HySasang_Group::Default(ST_SSGROUP_DATA &data)
{
    data.name.clear();
    data.datetime.fromString("20000101000000","yyyyMMddhhmmss");
    data.data.clear();
    data.program.clear();
    return true;
}
bool HySasang_Group::New(ST_SSGROUP_DATA &data)
{
    if(!IsAbleName(data.name)) return false;

    data.datetime = QDateTime::currentDateTime();

    int ret = SaveFile(data);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::New() SaveFile ret =" << ret;
        return false;
    }

    GroupList.push_back(data);
    GroupNameList.push_back(data.name);

    qDebug() << "Success New!!" << data.name;
    return true;
}

bool HySasang_Group::Copy(int source_index, QString name)
{
    if(!IsAbleName(name)) return false;

    if(source_index < 0) return false;
    if(source_index >= GroupList.size()) return false;

    ST_SSGROUP_DATA data;

    data.name = name;
    data.data = GroupList[source_index].data;
    data.program = GroupList[source_index].program;

    if(!New(data))
    {
        qDebug() << "HySasang_Group::Copy() New() fail!!";
        return false;
    }

    qDebug() << "Success Copy!!" << data.name;
    return true;
}

bool HySasang_Group::Del(int index)
{
    if(index < 0) return false;
    if(index >= GroupList.size()) return false;

    QString name = GroupList[index].name;
    QString file_path = PATH_SASANG_BASE;
    file_path += "/" + name + ".ss";

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->deleteDatabaseFile(file_path);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::Del() deleteDatabaseFile ret =" << ret;
        return false;
    }

    ST_SSGROUP_DATA data;
    data = GroupList.takeAt(index);
    GroupNameList.removeAll(data.name);

    qDebug() << "Success Delete!!" << data.name;
    return true;
}

bool HySasang_Group::Rename(int index, QString name)
{
    if(index < 0) return false;
    if(index >= GroupList.size()) return false;

    if(!IsAbleName(name)) return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    QString source_path = PATH_SASANG_BASE;
    source_path += "/" + GroupList[index].name + ".ss";

    ST_SSGROUP_DATA data;
    data = GroupList[index];
    data.name = name;

    ret = SaveFile(data);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::New() SaveFile ret =" << ret;
        return false;
    }

    ret = pCon->deleteDatabaseFile(source_path);
    if(ret < 0)
    {
        qDebug() << "HySasang_Group::Rename() deleteDatabaseFile ret =" << ret;
        return false;
    }

    GroupNameList.removeAll(GroupList[index].name);
    GroupNameList.push_back(data.name);

    GroupList[index].name = name;

    qDebug() << "Success Rename!!";
    return true;
}

bool HySasang_Group::IsExist(QString name)
{
    int index = GroupNameList.indexOf(name);
    if(index < 0)
        return false;
    return true;
}

bool HySasang_Group::IsAbleName(QString name)
{
    if(name.isEmpty())
        return false;
    if(IsExist(name))
        return false;

    // add more

    return true;
}


bool HySasang_Group::Update_CompData()
{
    GroupList_Comp = GroupList;
    return true;
}
bool HySasang_Group::Reset_CompData()
{
    GroupList = GroupList_Comp;
    return true;
}

bool HySasang_Group::IsEdit()
{
    if(GroupList.size() != GroupList_Comp.size())
        return true;

    for(int i=0;i<GroupList.count();i++)
    {
        if(IsSame(GroupList[i], GroupList_Comp[i]))
            return true;
    }

    return false;
}
bool HySasang_Group::IsSame(ST_SSGROUP_DATA data1, ST_SSGROUP_DATA data2)
{
    if(data1.name != data2.name)
        return false;
    if(data1.datetime.toString(Qt::ISODate) != data2.datetime.toString(Qt::ISODate))
        return false;
    if(data1.data != data2.data)
        return false;
    if(data1.program != data2.program)
        return false;

    return true;
}


bool HySasang_Group::MakingOneProgram(ST_SSGROUP_DATA data, QStringList &one_program)
{
    QStringList result;
    result.clear();

    int i,j;
    for(i=0;i<data.program.count();i++)
    {
        for(j=0;j<data.program[i].count();j++)
        {
            result << data.program[i].at(j);
        }
    }

    one_program = result;
    return true;
}
