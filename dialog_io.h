#ifndef DIALOG_IO_H
#define DIALOG_IO_H

#include <QDialog>
#include <QTimer>
#include "global.h"

#include "qlabel3.h"

#include "top_sub.h"

namespace Ui {
class dialog_io;
}

class dialog_io : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_io(QWidget *parent = 0);
    ~dialog_io();

    void ForceOutput(bool enable); // default = enable

private slots:

    void onClose();
    //void onIOTimer();
    void onDispTimer();
    void onUpIn();
    void onDownIn();
    void onUpOut();
    void onDownOut();
    void onSelectIn();
    void onSelectOut();
    void onEnable();

    void onForceInput();

private:
    Ui::dialog_io *ui;
    QTimer* timer;
    QTimer* timer_display;

    QVector<QLabel3*> m_vtISym, m_vtIName, m_vtIPic;
    QVector<QLabel3*> m_vtOSym, m_vtOName, m_vtOPic;

    int m_nInputPage;
    int m_nMaxInputPage;
    int m_nOutputPage;
    int m_nMaxOutputPage;

    QVector<QPixmap> pxmIn;
    QVector<QPixmap> pxmOut;
    void Display_InputPage(int page);
    void Display_InputData(int page);

    void Display_OutputPage(int page);
    void Display_OutputData(int page);

    bool m_bEnable;
    void SetEnable(bool enable);
    bool m_bDualSolOff;

    bool m_bForceOutput;
    void Display_ForceOutput();

    bool m_bForceInput;
    void SetForceInput(bool enable);

    top_sub* top;
    void SubTop();          // Need SubTop

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_IO_H
