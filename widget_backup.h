#ifndef WIDGET_BACKUP_H
#define WIDGET_BACKUP_H

#include <QWidget>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_message.h"
#include "dialog_confirm.h"
#include "dialog_keyboard.h"

namespace Ui {
class widget_backup;
}

class widget_backup : public QWidget
{
    Q_OBJECT

public:
    explicit widget_backup(QWidget *parent = 0);
    ~widget_backup();

    void Init();
    void Update();
    void CheckAll(bool bCheck);
    void ConfirmCheck();
    bool CheckNameRule(QString name);
    void Display_USB(bool bExist);

signals:
    void sigClose();

private slots:
    void onClose();

    void onList();
    void onChkAll();
    void onBackup();

private:
    Ui::widget_backup *ui;

    QVector<QLabel3*> m_vtMark;
    QVector<QLabel3*> m_vtCheck;
    QVector<QLabel3*> m_vtItem;
    QStringList m_Items;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // WIDGET_BACKUP_H
