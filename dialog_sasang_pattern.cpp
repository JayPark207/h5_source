#include "dialog_sasang_pattern.h"
#include "ui_dialog_sasang_pattern.h"

dialog_sasang_pattern::dialog_sasang_pattern(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_sasang_pattern)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(500);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onSave()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // init page
    Page = new QStackedWidget(this);
    //Page->setGeometry(0, 60, 800, 420);
    Page->setGeometry(ui->wigEdit->x(),
                      ui->wigEdit->y(),
                      ui->wigEdit->width(),
                      ui->wigEdit->height());
    Page->addWidget(ui->wigEdit);
    page_test = new dialog_sasang_pattern_test(this);
    Page->addWidget(page_test);
    Page->setCurrentIndex(0);

    connect(page_test,SIGNAL(sigClose()),this,SLOT(onClose_Page()));


    connect(ui->btnTestPic,SIGNAL(mouse_release()),this,SLOT(onTest()));
    connect(ui->btnTest,SIGNAL(mouse_press()),ui->btnTestPic,SLOT(press()));
    connect(ui->btnTest,SIGNAL(mouse_release()),ui->btnTestPic,SLOT(release()));

    m_vtTbNo.clear();m_vtTbPat.clear();m_vtTbMot.clear();
    m_vtTbNo.append(ui->tbNo);  m_vtTbPat.append(ui->tbPat);  m_vtTbMot.append(ui->tbMot);
    m_vtTbNo.append(ui->tbNo_2);m_vtTbPat.append(ui->tbPat_2);m_vtTbMot.append(ui->tbMot_2);
    m_vtTbNo.append(ui->tbNo_3);m_vtTbPat.append(ui->tbPat_3);m_vtTbMot.append(ui->tbMot_3);
    m_vtTbNo.append(ui->tbNo_4);m_vtTbPat.append(ui->tbPat_4);m_vtTbMot.append(ui->tbMot_4);
    m_vtTbNo.append(ui->tbNo_5);m_vtTbPat.append(ui->tbPat_5);m_vtTbMot.append(ui->tbMot_5);
    m_vtTbNo.append(ui->tbNo_6);m_vtTbPat.append(ui->tbPat_6);m_vtTbMot.append(ui->tbMot_6);
    m_vtTbNo.append(ui->tbNo_7);m_vtTbPat.append(ui->tbPat_7);m_vtTbMot.append(ui->tbMot_7);
    m_vtTbNo.append(ui->tbNo_8);m_vtTbPat.append(ui->tbPat_8);m_vtTbMot.append(ui->tbMot_8);

    int i;
    for(i=0;i<m_vtTbNo.count();i++)
    {
        connect(m_vtTbNo[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtTbPat[i],SIGNAL(mouse_release()),m_vtTbNo[i],SIGNAL(mouse_release()));
        connect(m_vtTbMot[i],SIGNAL(mouse_release()),m_vtTbNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));


    connect(ui->btnNewPic,SIGNAL(mouse_release()),this,SLOT(onNew()));
    connect(ui->btnNew,SIGNAL(mouse_press()),ui->btnNewPic,SLOT(press()));
    connect(ui->btnNew,SIGNAL(mouse_release()),ui->btnNewPic,SLOT(release()));
    connect(ui->btnNewIcon,SIGNAL(mouse_press()),ui->btnNewPic,SLOT(press()));
    connect(ui->btnNewIcon,SIGNAL(mouse_release()),ui->btnNewPic,SLOT(release()));

    connect(ui->btnCopyPic,SIGNAL(mouse_release()),this,SLOT(onCopy()));
    connect(ui->btnCopy,SIGNAL(mouse_press()),ui->btnCopyPic,SLOT(press()));
    connect(ui->btnCopy,SIGNAL(mouse_release()),ui->btnCopyPic,SLOT(release()));
    connect(ui->btnCopyIcon,SIGNAL(mouse_press()),ui->btnCopyPic,SLOT(press()));
    connect(ui->btnCopyIcon,SIGNAL(mouse_release()),ui->btnCopyPic,SLOT(release()));

    connect(ui->btnRenamePic,SIGNAL(mouse_release()),this,SLOT(onRename()));
    connect(ui->btnRename,SIGNAL(mouse_press()),ui->btnRenamePic,SLOT(press()));
    connect(ui->btnRename,SIGNAL(mouse_release()),ui->btnRenamePic,SLOT(release()));
    connect(ui->btnRenameIcon,SIGNAL(mouse_press()),ui->btnRenamePic,SLOT(press()));
    connect(ui->btnRenameIcon,SIGNAL(mouse_release()),ui->btnRenamePic,SLOT(release()));

    connect(ui->btnDelPic,SIGNAL(mouse_release()),this,SLOT(onDelete()));
    connect(ui->btnDel,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDel,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));
    connect(ui->btnDelIcon,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDelIcon,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));

    connect(ui->btnListUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnListUp,SIGNAL(mouse_press()),ui->btnListUpPic,SLOT(press()));
    connect(ui->btnListUp,SIGNAL(mouse_release()),ui->btnListUpPic,SLOT(release()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_press()),ui->btnListUpPic,SLOT(press()));
    connect(ui->btnListUpIcon,SIGNAL(mouse_release()),ui->btnListUpPic,SLOT(release()));

    connect(ui->btnListDnPic,SIGNAL(mouse_release()),this,SLOT(onListDn()));
    connect(ui->btnListDn,SIGNAL(mouse_press()),ui->btnListDnPic,SLOT(press()));
    connect(ui->btnListDn,SIGNAL(mouse_release()),ui->btnListDnPic,SLOT(release()));
    connect(ui->btnListDnIcon,SIGNAL(mouse_press()),ui->btnListDnPic,SLOT(press()));
    connect(ui->btnListDnIcon,SIGNAL(mouse_release()),ui->btnListDnPic,SLOT(release()));

    connect(ui->btnInsidePic,SIGNAL(mouse_release()),this,SLOT(onInside()));
    connect(ui->btnInside,SIGNAL(mouse_press()),ui->btnInsidePic,SLOT(press()));
    connect(ui->btnInside,SIGNAL(mouse_release()),ui->btnInsidePic,SLOT(release()));
    connect(ui->btnInsideIcon,SIGNAL(mouse_press()),ui->btnInsidePic,SLOT(press()));
    connect(ui->btnInsideIcon,SIGNAL(mouse_release()),ui->btnInsidePic,SLOT(release()));

    connect(ui->btnShiftPic,SIGNAL(mouse_release()),this,SLOT(onShift()));
    connect(ui->btnShiftIcon,SIGNAL(mouse_press()),ui->btnShiftPic,SLOT(press()));
    connect(ui->btnShiftIcon,SIGNAL(mouse_release()),ui->btnShiftPic,SLOT(release()));


    // clear var & pointer
    dig_sasang_motion = 0;
    m_GrpName.clear();
    dig_shift = 0;
}

dialog_sasang_pattern::~dialog_sasang_pattern()
{
    delete ui;
}
void dialog_sasang_pattern::showEvent(QShowEvent *)
{
    //Test_Make_List();

    Update();

    timer->start();
}
void dialog_sasang_pattern::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_sasang_pattern::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_sasang_pattern::onSave()
{
    //emit accept();

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(tr("Save"));
    conf->Message(QString(tr("Would you want to save?")), m_GrpData.name);
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!Sasang->Patterns2Group(Sasang->Pattern->PatternList, m_GrpData))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to save group data!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    int ret = Sasang->Group->SaveFile(m_GrpData);
    if(ret < 0)
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to save group data!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::GREEN);
    msg->Title(tr("SAVE"));
    msg->Message(tr("Success to Save!"), m_GrpData.name);
    msg->exec();

    Update();

    if(m_bSave_Accepted)
        emit accept();

}

void dialog_sasang_pattern::onCancel() // not use
{
    if(ui->wigSave->isEnabled())
    {
        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::GREEN);
        conf->Title(tr("Confirm"));
        conf->Message(QString(tr("Would you want to cancel?")), tr("Changed data will not be saved."));
        if(conf->exec() != QDialog::Accepted)
            return;
    }

    emit reject();
}

void dialog_sasang_pattern::onClose() // use
{
    if(ui->wigSave->isEnabled())
    {
        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::GREEN);
        conf->Title(tr("Confirm"));
        conf->Message(QString(tr("Would you want to cancel?")), tr("Changed data will not be saved."));
        if(conf->exec() != QDialog::Accepted)
            return;
    }

    emit reject();
}

void dialog_sasang_pattern::onClose_Page()
{
    Page->setCurrentIndex(0);
}
void dialog_sasang_pattern::onTest()
{
    Page->setCurrentIndex(1);
}

void dialog_sasang_pattern::onTimer()
{
    Enable_Save(Sasang->Pattern->IsEdit());
}

void dialog_sasang_pattern::Init(QString group_name)
{
    m_GrpName = group_name;

    ui->lbGrp->setText(m_GrpName);
}

void dialog_sasang_pattern::Init(ST_SSGROUP_DATA group_data, bool save_accepted)
{
    m_GrpData = group_data;
    m_GrpName = group_data.name;
    m_bSave_Accepted = save_accepted;

    ui->lbGrp->setText(m_GrpName);
}

ST_SSGROUP_DATA dialog_sasang_pattern::GetGrpData()
{
    return m_GrpData;
}

void dialog_sasang_pattern::Update()
{
    m_nStartIndex=0;m_nSelectedIndex=0;
    Redraw_List(m_nStartIndex);

    Sasang->Pattern->Update_CompData();
}

void dialog_sasang_pattern::Redraw_List(int start_index)
{
    int cal_size = Sasang->Pattern->PatternList.size() - start_index;
    int index;
    QString no,pat,mot;

    Display_ListOn(-1); // all off

    for(int i=0;i<m_vtTbNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            no = QString().setNum(index + 1);
            Sasang->Pattern->GetName(index, pat);
            mot = QString().setNum(Sasang->Pattern->GetMotionCount(index));
        }
        else
        {
            // no data.
            no.clear();
            pat.clear();
            mot.clear();
        }

        m_vtTbNo[i]->setText(no);
        m_vtTbPat[i]->setText(pat);
        m_vtTbMot[i]->setText(mot);

        if((start_index + i) == m_nSelectedIndex)
            Display_ListOn(i);
    }

    // display list size.
    ui->lbListSize->setText(QString().setNum(Sasang->Pattern->GetCount()));
}

void dialog_sasang_pattern::Display_ListOn(int table_index)
{
    for(int i=0;i<m_vtTbNo.count();i++)
    {
        m_vtTbNo[i]->setAutoFillBackground(i==table_index);
        m_vtTbPat[i]->setAutoFillBackground(i==table_index);
        m_vtTbMot[i]->setAutoFillBackground(i==table_index);
    }
}

void dialog_sasang_pattern::onList()
{
    QLabel3* sel = (QLabel3*)sender();

    int index = m_vtTbNo.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nStartIndex + index;
    if(list_index == m_nSelectedIndex) return;

    if(list_index < Sasang->Pattern->GetCount())
    {
        m_nSelectedIndex = list_index;
        Redraw_List(m_nStartIndex);
    }
}
void dialog_sasang_pattern::onUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_sasang_pattern::onDown()
{
    if(m_nStartIndex >= (Sasang->Pattern->GetCount() - m_vtTbNo.size()))
        return;

    Redraw_List(++m_nStartIndex);
}

void dialog_sasang_pattern::onNew()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnNew->text());
    conf->Message(QString(tr("Would you want to new pattern?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->btnNew->text());
    kb->m_kb->SetText("");

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(kb->exec() == QDialog::Accepted)
    {
        QString name;
        name = kb->m_kb->GetText();

        if(!Sasang->Pattern->IsAbleName(name))
        {
            msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Wrong Name!")),
                         QString(tr("Please, Check name & try again")));
            msg->exec();
            return;
        }
        ST_SSPATTERN_DATA data;
        if(!Sasang->Pattern->Default(data))
        {
            msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Making New Pattern Error!")),
                         QString(tr("Please, try again")));
            msg->exec();
            return;
        }
        data.name = name;
        if(!Sasang->Pattern->Add(data))
        {
            msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("ERROR")));
            msg->Message(QString(tr("Making New Pattern Error!")),
                         QString(tr("Please, try again")));
            msg->exec();
            return;
        }

        // redraw
        m_nSelectedIndex = Sasang->Pattern->GetCount() - 1;
        if(Sasang->Pattern->GetCount() > (m_nStartIndex + m_vtTbNo.count()))
            m_nStartIndex = Sasang->Pattern->GetCount() - m_vtTbNo.count();

        Redraw_List(m_nStartIndex);

        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(QString(ui->btnNew->text()));
        msg->Message(tr("Success to New!"), name);
        msg->exec();
    }
}
void dialog_sasang_pattern::onCopy()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= Sasang->Pattern->GetCount()) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnCopy->text());
    QString str;
    str.sprintf("[%d] ",(m_nSelectedIndex+1));
    str += Sasang->Pattern->PatternList[m_nSelectedIndex].name;
    conf->Message(QString(tr("Would you want to copy?")),str);
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!Sasang->Pattern->Copy(m_nSelectedIndex))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Copy!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    // redraw
    m_nSelectedIndex = Sasang->Pattern->GetCount() - 1;
    if(Sasang->Pattern->GetCount() > (m_nStartIndex + m_vtTbNo.count()))
        m_nStartIndex = Sasang->Pattern->GetCount() - m_vtTbNo.count();

    Redraw_List(m_nStartIndex);

    msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(QString(ui->btnCopy->text()));
    msg->Message(tr("Success to Copy!"));
    msg->exec();
}

void dialog_sasang_pattern::onRename()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= Sasang->Pattern->GetCount()) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnRename->text());
    conf->Message(QString(tr("Would you want to rename?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->btnRename->text());
    QString name_old = Sasang->Pattern->PatternList[m_nSelectedIndex].name;
    kb->m_kb->SetText(name_old);

    if(kb->exec() != QDialog::Accepted)
        return;

    QString name = kb->m_kb->GetText();

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    if(!Sasang->Pattern->IsAbleName(name))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Wrong Name!")),
                     QString(tr("Please, Check name & try again")));
        msg->exec();
        return;
    }

    if(!Sasang->Pattern->Rename(m_nSelectedIndex, name))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Rename!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    Redraw_List(m_nStartIndex);

    msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(QString(ui->btnRename->text()));
    QString str;
    str = name_old;
    str += " -> ";
    str += name;
    msg->Message(tr("Success to Rename!"), str);
    msg->exec();
}
void dialog_sasang_pattern::onDelete()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= Sasang->Pattern->GetCount()) return;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDel->text());
    conf->Message(QString(tr("Would you want to Delete?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnDel->text());
    conf->Message(QString(tr("Really, Would you want to Delete?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    QString name;
    Sasang->Pattern->GetName(m_nSelectedIndex, name);

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    if(!Sasang->Pattern->Del(m_nSelectedIndex))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Delete!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    if(m_nSelectedIndex >= Sasang->Pattern->GetCount())
        m_nSelectedIndex = Sasang->Pattern->GetCount() - 1;

    if((m_nStartIndex + m_vtTbNo.size()) > Sasang->Pattern->GetCount())
    {
        if(m_nStartIndex > 0)
            m_nStartIndex--;
    }

    Redraw_List(m_nStartIndex);

    msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(QString(ui->btnRename->text()));
    msg->Message(tr("Success to Delete!"), name);
    msg->exec();

}
void dialog_sasang_pattern::onListUp()
{
    if(Sasang->Pattern->Up(m_nSelectedIndex))
    {
        m_nSelectedIndex--;
        if(m_nStartIndex > m_nSelectedIndex)
            m_nStartIndex--;

        Redraw_List(m_nStartIndex);
    }
}
void dialog_sasang_pattern::onListDn()
{
    if(Sasang->Pattern->Down(m_nSelectedIndex))
    {
        m_nSelectedIndex++;
        if((m_nStartIndex + m_vtTbNo.size()) <= m_nSelectedIndex)
            m_nStartIndex++;

        Redraw_List(m_nStartIndex);
    }
}

void dialog_sasang_pattern::Enable_Save(bool bEdit)
{
    ui->wigSave->setEnabled(bEdit);
}

void dialog_sasang_pattern::onInside()
{
    if(m_nSelectedIndex < 0) return;
    if(m_nSelectedIndex >= Sasang->Pattern->GetCount()) return;

    if(dig_sasang_motion == 0)
        dig_sasang_motion = new dialog_sasang_motion();

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!Sasang->Pattern2Motions(Sasang->Pattern->PatternList[m_nSelectedIndex],Sasang->Motion->MotionList))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Inside!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    dig_sasang_motion->Init(m_GrpName, Sasang->Pattern->PatternList[m_nSelectedIndex].name);
    dig_sasang_motion->exec();

    if(!Sasang->Motions2Pattern(Sasang->Motion->MotionList, Sasang->Pattern->PatternList[m_nSelectedIndex]))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Save pattern data!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    Redraw_List(m_nStartIndex);
}

void dialog_sasang_pattern::onShift()
{
    if(dig_shift == 0)
        dig_shift = new dialog_sasang_shift();

    if(dig_shift->exec() != QDialog::Accepted)
        return;

    ST_SSSHIFT_VALUE val;
    dig_shift->Get(val);
    float speed = -1.0, accuracy = -1.0;
    if(dig_shift->IsSpeed())
        speed = dig_shift->GetSpeed();
    if(dig_shift->IsAccuracy())
        accuracy = dig_shift->GetAccuracy();

    QList<ST_SSPATTERN_DATA> list;
    list = Sasang->Pattern->PatternList;

    int i;
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    for(i=0;i<list.count();i++)
    {
        if(!Sasang->Pattern->DataAllChange(list[i], val, speed, accuracy))
        {
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("Error")));
            msg->Message(tr("All Change Error!"),
                         tr("Please, Try again!"));
            msg->exec();
            return;
        }

        if(!Sasang->Pattern->Data2Prog(list[i]))
        {
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("Error")));
            msg->Message(tr("All Change Error!"),
                         tr("Please, Try again!"));
            msg->exec();
            return;
        }
    }

    Sasang->Pattern->PatternList = list;
    Redraw_List(m_nStartIndex);

    QString str;
    if(dig_shift->IsShift())
    {
        msg->SetColor(dialog_message::GREEN);
        msg->Title(QString(tr("Success")));
        str.sprintf("%.2f,%.2f,%.2f,%.2f,%.2f",val.x,val.y,val.z,val.r,val.s);
        msg->Message(tr("Success Position Shift!"),
                     str);
        msg->exec();
    }
    if(dig_shift->IsSpeed())
    {
        msg->SetColor(dialog_message::GREEN);
        msg->Title(QString(tr("Success")));
        str.sprintf(FORMAT_SPEED, dig_shift->GetSpeed());
        msg->Message(tr("Success Change All Speed Value!"),
                     str + "%");
        msg->exec();
    }
    if(dig_shift->IsAccuracy())
    {
        msg->SetColor(dialog_message::GREEN);
        msg->Title(QString(tr("Success")));
        str.sprintf(FORMAT_POS, dig_shift->GetAccuracy());
        msg->Message(tr("Success Change Accuracy Value!"),
                     str + "mm");
        msg->exec();
    }
}






/** ============================================== **/

//void dialog_sasang_pattern::Test_Make_List()
//{
//    CNRobo* pCon = CNRobo::getInstance();

//    Sasang->Motion->ClearList();

//    ST_SSMOTION_DATA data;

//    // start
//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::p2p);
//    data.joint[0].joint[0] = 1;
//    data.joint[0].joint[1] = 0;
//    data.joint[0].joint[2] = 0;
//    data.joint[0].joint[3] = 0;
//    data.joint[0].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    data.speed_output = 50.0;
//    Sasang->Motion->MotionList.append(data);

//    // end
//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::p2p);
//    data.joint[0].joint[0] = 486.20;
//    data.joint[0].joint[1] = -8.91;
//    data.joint[0].joint[2] = 17.69;
//    data.joint[0].joint[3] = -8.80;
//    data.joint[0].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    data.speed_output = 50.0;
//    Sasang->Motion->MotionList.append(data);

//    //=======

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
//    data.joint[0].joint[0] = 50.0;
//    data.joint[0].joint[1] = 0;
//    data.joint[0].joint[2] = 0;
//    data.joint[0].joint[3] = 0;
//    data.joint[0].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    data.speed_output = 50.0;
//    data.accuracy_input = 1.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
//    data.joint[0].joint[0] = 100.0;
//    data.joint[0].joint[1] = 0;
//    data.joint[0].joint[2] = 0;
//    data.joint[0].joint[3] = 0;
//    data.joint[0].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    data.speed_output = 50.0;
//    data.accuracy_input = 1.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
//    data.joint[0].joint[0] = 150.0;
//    data.joint[0].joint[1] = 0;
//    data.joint[0].joint[2] = 0;
//    data.joint[0].joint[3] = 0;
//    data.joint[0].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    data.speed_output = 50.0;
//    data.accuracy_input = 1.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::out);
//    data.speed_output = 1;
//    data.type = HyUserData::TYPE_ON;
//    data.time = 0.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::wtt);
//    data.time = 2.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::out);
//    data.speed_output = 1;
//    data.type = HyUserData::TYPE_OFF;
//    data.time = 0.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
//    data.joint[0].joint[0] = 100.0;
//    data.joint[0].joint[1] = 0;
//    data.joint[0].joint[2] = 0;
//    data.joint[0].joint[3] = 0;
//    data.joint[0].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    data.speed_output = 50.0;
//    data.accuracy_input = 1.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::out);
//    data.speed_output = 2;
//    data.type = HyUserData::TYPE_PULSE;
//    data.time = 2.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::con);
//    data.joint[0].joint[0] = 50.0;
//    data.joint[0].joint[1] = 0;
//    data.joint[0].joint[2] = 0;
//    data.joint[0].joint[3] = 0;
//    data.joint[0].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    data.speed_output = 50.0;
//    data.accuracy_input = 1.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::wtt);
//    data.time = 3.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::p2p);
//    data.joint[0].joint[0] = 200.0;
//    data.joint[0].joint[1] = -9.74;
//    data.joint[0].joint[2] = 34.29;
//    data.joint[0].joint[3] = -24.55;
//    data.joint[0].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    data.speed_output = 50.0;
//    Sasang->Motion->Add(data);

//    Sasang->Motion->Default(data);
//    data.command = Sasang->Motion->toStr(HySasang_Motion::cir);
//    data.joint[0].joint[0] = 328.79;
//    data.joint[0].joint[1] = -9.82;
//    data.joint[0].joint[2] = 45.27;
//    data.joint[0].joint[3] = -35.54;
//    data.joint[0].joint[4] = 0;

//    data.joint[1].joint[0] = 486.20;
//    data.joint[1].joint[1] = -9.03;
//    data.joint[1].joint[2] = 36.03;
//    data.joint[1].joint[3] = -27.09;
//    data.joint[1].joint[4] = 0;
//    pCon->calcForward(data.joint[0].joint, data.trans[0]);
//    pCon->calcForward(data.joint[1].joint, data.trans[1]);
//    data.speed_output = 20;
//    Sasang->Motion->Add(data);

//    // making motions raw & prog.

//    for(int i=0;i<Sasang->Motion->MotionList.count();i++)
//    {
//        Sasang->Motion->Data2Raw(Sasang->Motion->MotionList[i]);
//        Sasang->Motion->Data2Prog(Sasang->Motion->MotionList[i]);
//    }

//    // making pattern list
//    Sasang->Pattern->PatternList.clear();

//    ST_SSPATTERN_DATA pattern;
//    pattern.name = "Pattern_Test_1";
//    Sasang->Motions2Pattern(Sasang->Motion->MotionList, pattern);
//    Sasang->Pattern->Add(pattern);

//    pattern.name = "Pattern_Test_2";
//    Sasang->Motions2Pattern(Sasang->Motion->MotionList, pattern);
//    Sasang->Pattern->Add(pattern);

//    pattern.name = "Pattern_Test_3";
//    Sasang->Motions2Pattern(Sasang->Motion->MotionList, pattern);
//    Sasang->Pattern->Add(pattern);

//    Sasang->Pattern->Default(pattern);
//    pattern.name = "Pattern_Test_new";
//    Sasang->Pattern->Add(pattern);

//}
