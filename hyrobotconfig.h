#ifndef HYROBOTCONFIG_H
#define HYROBOTCONFIG_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

class HyRobotConfig : public QObject
{
    Q_OBJECT

public:
    HyRobotConfig();

    bool Read();
    bool Write();

    // QStringList를 사용한 이유는 값중에 콤마구분으로 되어있는 것들이 존재하기때문.
    // QStringList 의 사이즈를 확인하여 사용.
    // 오로지 robot.conf 에 있는 내용만 찾아서 수정할 수있다.
    // Get stringlist value -> value modify -> Set stringlist value.
    // robot.conf  XXX(name) = YYYY,YYYY,...(value)
    // ex) AXIS_TYPE = T, R, R, R, R, R
    bool Get(QString name, QStringList& value);
    bool Set(QString name, QStringList value);

private:
    QStringList m_slRawData;

};

#endif // HYROBOTCONFIG_H
