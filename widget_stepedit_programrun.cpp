#include "widget_stepedit_programrun.h"
#include "ui_widget_stepedit_programrun.h"

widget_stepedit_programrun::widget_stepedit_programrun(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::widget_stepedit_programrun)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    m_vtIcon.clear();              m_vtNo.clear();            m_vtText.clear();
    m_vtIcon.append(ui->lbIcon);   m_vtNo.append(ui->lbNo);   m_vtText.append(ui->lbText);
    m_vtIcon.append(ui->lbIcon_2); m_vtNo.append(ui->lbNo_2); m_vtText.append(ui->lbText_2);
    m_vtIcon.append(ui->lbIcon_3); m_vtNo.append(ui->lbNo_3); m_vtText.append(ui->lbText_3);
    m_vtIcon.append(ui->lbIcon_4); m_vtNo.append(ui->lbNo_4); m_vtText.append(ui->lbText_4);
    m_vtIcon.append(ui->lbIcon_5); m_vtNo.append(ui->lbNo_5); m_vtText.append(ui->lbText_5);
    m_vtIcon.append(ui->lbIcon_6); m_vtNo.append(ui->lbNo_6); m_vtText.append(ui->lbText_6);
    m_vtIcon.append(ui->lbIcon_7); m_vtNo.append(ui->lbNo_7); m_vtText.append(ui->lbText_7);
    m_vtIcon.append(ui->lbIcon_8); m_vtNo.append(ui->lbNo_8); m_vtText.append(ui->lbText_8);
    m_vtIcon.append(ui->lbIcon_9); m_vtNo.append(ui->lbNo_9); m_vtText.append(ui->lbText_9);
    m_vtIcon.append(ui->lbIcon_10);m_vtNo.append(ui->lbNo_10);m_vtText.append(ui->lbText_10);
    m_vtIcon.append(ui->lbIcon_11);m_vtNo.append(ui->lbNo_11);m_vtText.append(ui->lbText_11);
    m_vtIcon.append(ui->lbIcon_12);m_vtNo.append(ui->lbNo_12);m_vtText.append(ui->lbText_12);

}

widget_stepedit_programrun::~widget_stepedit_programrun()
{
    delete ui;
}
void widget_stepedit_programrun::showEvent(QShowEvent *)
{
    //Read(m_strPrgName);
    //Redraw(m_nStartIndex);
}
void widget_stepedit_programrun::hideEvent(QHideEvent *)
{
}
void widget_stepedit_programrun::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void widget_stepedit_programrun::Init(QString prg_name)
{
    m_strPrgName = prg_name;
    m_nRunIndex = 0;
    m_nMaxIndex = 0;
    m_nStartIndex = 0;

    Read(m_strPrgName);
    Redraw(m_nStartIndex);

}
void widget_stepedit_programrun::Read(QString prg_name)
{
    CNRobo* pCon = CNRobo::getInstance();
    QStringList temp;

    int ret = pCon->getProgramSteps(prg_name, temp);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps =" << ret;
        return;
    }

    qDebug() << "Read Program :" <<prg_name;
    /*for(int i=0;i<temp.count();i++)
    {
        temp[i] = temp[i].trimmed();
        //qDebug() << temp[i];
    }*/

    m_Program = temp;       // real data

    // program name update
    ui->lbProgName->setText(m_strPrgName);
    // total index update
    m_nMaxIndex = m_Program.size();
    ui->lbTotalIndex->setText(QString().setNum(m_nMaxIndex));
}

void widget_stepedit_programrun::Redraw(int start_index)
{
    int list_index;
    QString strNo, strText;
    for(int i=0;i<m_vtNo.count();i++)
    {
        list_index = start_index + i;

        if(list_index >= m_Program.size())
        {
            strNo.clear();
            strText.clear();
        }
        else
        {
            strNo = QString().setNum(list_index + 1);
            strText = m_Program[list_index];
        }

        m_vtNo[i]->setText(strNo);
        m_vtText[i]->setText(strText);
    }
}

void widget_stepedit_programrun::SetIcon(int line, CNR_TASK_STATUS task_status)
{
    QImage img;
    QPixmap pxm = 0;
    switch(task_status)
    {
    case CNR_TASK_NOTUSED:
        img.load(":/icon/icon/icon99-5.png");
        pxm = QPixmap::fromImage(img);
        break;
    case CNR_TASK_STOPPED:
        img.load(":/icon/icon/icon99-4.png");
        pxm = QPixmap::fromImage(img);
        break;
    case CNR_TASK_RUNNING:
        img.load(":/icon/icon/icon99-3.png");
        pxm = QPixmap::fromImage(img);
        break;

    default:
        pxm = 0;
        break;
    }
    for(int i=0;i<m_vtIcon.count();i++)
        m_vtIcon[i]->setPixmap(0);

    m_vtIcon[line]->setPixmap(pxm);
}

// Need cyclic running .
void widget_stepedit_programrun::CyclicUpdate_Index(int run_index,
                                                    int plan_index,
                                                    CNR_TASK_STATUS task_status)
{
    if(run_index < 0) run_index = 0;
    if(plan_index < 0) plan_index = 0;

    // auto redraw from run_index.
    if(run_index >= (m_nStartIndex + m_vtNo.size()))
    {
        m_nStartIndex = run_index - (m_vtNo.size()-1);
        Redraw(m_nStartIndex);
    }
    else if(run_index < m_nStartIndex)
    {
        m_nStartIndex = run_index;
        Redraw(m_nStartIndex);
    }

    // icon update.
    int line_index = run_index - m_nStartIndex;
    SetIcon(line_index, task_status);

    ui->lbNowIndex->setText(QString().setNum(run_index+1));
}
