#ifndef DIALOG_JMOTION_H
#define DIALOG_JMOTION_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_mode_select.h"

namespace Ui {
class dialog_jmotion;
}

class dialog_jmotion : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_jmotion(QWidget *parent = 0);
    ~dialog_jmotion();

    void Update();

public slots:
    void onUse();
    void onSDist();

private:
    Ui::dialog_jmotion *ui;

    QVector<QLabel3*> m_vtTbTitle;
    QVector<QLabel4*> m_vtSDistPic;
    QVector<QLabel3*> m_vtSDist;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;

    QStringList m_ItemName;


    dialog_mode_select* sel;
    dialog_numpad* np;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_JMOTION_H
