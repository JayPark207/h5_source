#ifndef DIALOG_INSERTSUB_SET_H
#define DIALOG_INSERTSUB_SET_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_insertsub_set;
}

class dialog_insertsub_set : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_insertsub_set(QWidget *parent = 0);
    ~dialog_insertsub_set();

    void Update(int pos_index);
    void Update_Output();

public slots:
    void onClose();

    void onPos();
    void onSel();

    void onNum();
    void onOffset();

    void onOutput();
    void onOutputPage();

    void onHeight();

private:
    Ui::dialog_insertsub_set *ui;
    dialog_numpad* np;

    QStackedWidget* Stacked;

    QVector<QLabel4*> m_vtPosPic;
    QVector<QLabel3*> m_vtPos;

    QVector<QLabel4*> m_vtSelPic;
    QVector<QLabel3*> m_vtSel;

    int m_nSeperate;
    QVector<HyRecipe::RECIPE_NUMBER> Vars;
    QVector<float> Datas, Data_Offset, Data_Output;

    void Display_Pos(int pos_index);
    void Display_Sel(int sel_index);

    void Enable_Pos(int num);
    void Display_Data(int pos_index);

    QVector<QLabel3*> m_vtName;
    QVector<QLabel4*> m_vtOffsetPic;
    QVector<QLabel3*> m_vtOffset;

    int Get_NowPosIndex();

    // related output
    QStringList m_slOutput;
    QVector<QLabel4*> m_vtOPic;
    QVector<QLabel3*> m_vtOName;
    QVector<QLabel3*> m_vtOSel;
    QVector<QLabel3*> m_vtOIcon;

    QVector<QLabel4*> m_vtOPagePic;
    QVector<QLabel3*> m_vtOPageIcon;

    void Clear_Output(int number);


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_INSERTSUB_SET_H
