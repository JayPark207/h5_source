#ifndef PAGE_MAIN2_H
#define PAGE_MAIN2_H

#include <QWidget>
#include <QStackedWidget>

#include "ui_page_main2.h"
#include "global.h"

//#include "page_main2_log.h"
//#include "page_main2_mold.h"
#include "page_main2_monitor.h"
#include "page_main2_setting.h"
#include "page_main2_special.h"
#include "page_main2_teach.h"

#include "dialog_home.h"
#include "dialog_manual.h"
#include "dialog_log.h"
#include "dialog_recipe.h"

#include "dialog_delaying.h"
#include "dialog_error.h"
#include "dialog_confirm.h"
#include "dialog_robot_notuse.h"
#include "dialog_message.h"

#ifdef _CG3300_
#define HIGHLIGHT_START_X       -9
#define HIGHLIGHT_START_Y       -14
#define HIGHLIGHT_X_PITCH       130
#define HIGHLIGHT_Y_PITCH       130
#elif _CG4300_
#define HIGHLIGHT_START_X       -4
#define HIGHLIGHT_START_Y       -14
#define HIGHLIGHT_X_PITCH       160
#define HIGHLIGHT_Y_PITCH       160
#endif


namespace Ui {
class page_main2;
}

class page_main2 : public QWidget
{
    Q_OBJECT
    
public:
    explicit page_main2(QWidget *parent = 0);
    ~page_main2();

    QStackedWidget* stackedPage;
    //page_main2_mold* pageMold;
    page_main2_teach* pageTeach;
    page_main2_monitor* pageMonitor;
    //page_main2_log* pageLog;
    page_main2_setting* pageSetting;
    page_main2_special* pageSpecial;

    dialog_manual* dig_Mauual;
    dialog_log* dig_log;
    dialog_recipe* dig_recipe;

    void SetHighlight(int x, int y);
    void SetHighlightOff();

private slots:

    void onMold();
    void onTeach();
    void onMonitor();
    void onLog();
    void onSetting();
    void onSpecial();
    void onHoming();
    void onAuto();
    void onManual();

    void onReset();
    void onErrorView();
    void onSafetyzone();
    void onRobotNotUse();


private:
    Ui::page_main2 *ui;

    void Display_Flag(int lang_index);

    dialog_delaying* dig_delaying;

    void ChangePage(MAIN2_PAGE_INDEX page_index);

    dialog_robot_notuse* dig_robot_notuse;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // PAGE_MAIN2_H
