#include "dialog_varmonitor.h"
#include "ui_dialog_varmonitor.h"

dialog_varmonitor::dialog_varmonitor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_varmonitor)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtName.clear();m_vtVal.clear();
    m_vtName.append(ui->tbName);   m_vtVal.append(ui->tbVal);
    m_vtName.append(ui->tbName_2); m_vtVal.append(ui->tbVal_2);
    m_vtName.append(ui->tbName_3); m_vtVal.append(ui->tbVal_3);
    m_vtName.append(ui->tbName_4); m_vtVal.append(ui->tbVal_4);
    m_vtName.append(ui->tbName_5); m_vtVal.append(ui->tbVal_5);
    m_vtName.append(ui->tbName_6); m_vtVal.append(ui->tbVal_6);
    m_vtName.append(ui->tbName_7); m_vtVal.append(ui->tbVal_7);
    m_vtName.append(ui->tbName_8); m_vtVal.append(ui->tbVal_8);
    m_vtName.append(ui->tbName_9); m_vtVal.append(ui->tbVal_9);
    m_vtName.append(ui->tbName_10);m_vtVal.append(ui->tbVal_10);
    m_vtName.append(ui->tbName_11);m_vtVal.append(ui->tbVal_11);
    m_vtName.append(ui->tbName_12);m_vtVal.append(ui->tbVal_12);
    m_vtName.append(ui->tbName_13);m_vtVal.append(ui->tbVal_13);
    m_vtName.append(ui->tbName_14);m_vtVal.append(ui->tbVal_14);
    m_vtName.append(ui->tbName_15);m_vtVal.append(ui->tbVal_15);
    m_vtName.append(ui->tbName_16);m_vtVal.append(ui->tbVal_16);

    int i;
    for(i=0;i<m_vtName.count();i++)
    {
        connect(m_vtName[i],SIGNAL(mouse_release()),this,SLOT(onTable()));
        connect(m_vtVal[i],SIGNAL(mouse_release()),m_vtName[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnModePic,SIGNAL(mouse_release()),this,SLOT(onMode()));
    connect(ui->btnMode,SIGNAL(mouse_press()),ui->btnModePic,SLOT(press()));
    connect(ui->btnMode,SIGNAL(mouse_release()),ui->btnModePic,SLOT(release()));

    connect(ui->btnEditPic,SIGNAL(mouse_release()),this,SLOT(onEdit()));
    connect(ui->btnEdit,SIGNAL(mouse_press()),ui->btnEditPic,SLOT(press()));
    connect(ui->btnEdit,SIGNAL(mouse_release()),ui->btnEditPic,SLOT(release()));

}

dialog_varmonitor::~dialog_varmonitor()
{
    delete ui;
}
void dialog_varmonitor::showEvent(QShowEvent *)
{
    Update_Name();
    timer->start();
    SetEditMode(false);
}
void dialog_varmonitor::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_varmonitor::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_varmonitor::Update_Name()
{
    // Param Read. -> make m_Name.(read variable name list)
    QString param_name = Param->Get(HyParam::VAR_MONITOR_NAME); // 0~7 (8ea)
    QString param_name2 = Param->Get(HyParam::VAR_MONITOR_NAME2); // 8~15 (8ea)
    QStringList list_name = param_name.split(",");
    QStringList list_name2 = param_name2.split(",");

    m_Name.clear();
    int i;
    for(i=0;i<m_vtName.count();i++)
    {
        if(i < (list_name.size() + list_name2.size()))
        {
            if(i < list_name2.size())
                m_Name.append(list_name[i]);
            else
                m_Name.append(list_name2[(i-list_name2.size())]);
        }
        else
            m_Name.append("");
    }

    // Display m_Name.
    for(i=0;i<m_vtName.count();i++)
        m_vtName[i]->setText(m_Name[i]);

}

void dialog_varmonitor::Update_Val()
{
    int i;
    QString str;
    QList<cn_variant> data;
    if(Recipe->GetVars(m_Name, data))
    {
        // data parsing
        m_Data.clear();
        for(i=0;i<data.count();i++)
        {
            if(data[i].type < 0) // error case.
                m_Data.append("");
            else
            {
                str = MakeValue(data[i]);
                m_Data.append(str);
            }
        }

        // display m_Data.
        for(i=0;i<m_vtVal.count();i++)
        {
            if(i < m_Data.size())
                m_vtVal[i]->setText(m_Data[i]);
        }
    }
}

QString dialog_varmonitor::MakeValue(cn_variant variant)
{
    QString result;

    switch(variant.type)
    {
    case CNVAR_FLOAT:
        result.sprintf("%.2f", variant.val.f32);
        break;

    case CNVAR_STRING:
        result = QString(variant.val.str);
        break;

    default:
        result = tr("N/A");
        break;
    }

    return result;
}

void dialog_varmonitor::SaveParam(QStringList& names)
{
    QStringList var_names, var_names2;
    var_names.clear();var_names2.clear();

    for(int i=0;i<names.count();i++)
    {
        if(i < (int)(names.count()/2))
            var_names.append(names[i]);
        else
            var_names2.append(names[i]);
    }

    QString param_name = var_names.join(",");
    QString param_name2 = var_names2.join(",");
    qDebug() << "SaveParam : " << param_name;
    qDebug() << "SaveParam2 : " << param_name2;
    Param->Set(HyParam::VAR_MONITOR_NAME, param_name);
    Param->Set(HyParam::VAR_MONITOR_NAME2, param_name2);
}

void dialog_varmonitor::onTimer()
{
    if(m_bEditMode) return;

    Update_Val();
}


void dialog_varmonitor::onMode()
{
    if(m_bEditMode)
        SetEditMode(false);
    else
        SetEditMode(true);
}

void dialog_varmonitor::onTable()
{
    if(!m_bEditMode) return;

    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtName.indexOf(sel);
    if(index < 0) return;

    m_nSelectedIndex = index;
    Display_Select(m_nSelectedIndex);
}
void dialog_varmonitor::onEdit()
{
    if(!m_bEditMode) return;
    if(m_nSelectedIndex < 0) return;

    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->tbNameTitle->text());
    kb->m_kb->SetText(m_vtName[m_nSelectedIndex]->text());
    if(kb->exec() == QDialog::Accepted)
    {
        if(m_nSelectedIndex < m_Name.size())
            m_Name[m_nSelectedIndex] = kb->m_kb->GetText();

        SaveParam(m_Name);
        Update_Name();
    }
}

void dialog_varmonitor::SetEditMode(bool onoff)
{
    m_bEditMode = onoff;
    ui->wigEdit->setEnabled(m_bEditMode);

    if(m_bEditMode)
        m_nSelectedIndex = 0;
    else
        m_nSelectedIndex = -1;

    Display_Select(m_nSelectedIndex);
}

void dialog_varmonitor::Display_Select(int select_index)
{
    for(int i=0;i<m_vtName.count();i++)
    {
        m_vtName[i]->setAutoFillBackground(i==select_index);
        m_vtVal[i]->setAutoFillBackground(i==select_index);
    }
}
