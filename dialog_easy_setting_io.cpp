#include "dialog_easy_setting_io.h"
#include "ui_dialog_easy_setting_io.h"

dialog_easy_setting_io::dialog_easy_setting_io(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_easy_setting_io)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtOutPic.clear();               m_vtOut.clear();            m_vtOutIcon.clear();
    m_vtOutPic.append(ui->btnOPic);   m_vtOut.append(ui->btnO);   m_vtOutIcon.append(ui->btnOIcon);
    m_vtOutPic.append(ui->btnOPic_2); m_vtOut.append(ui->btnO_2); m_vtOutIcon.append(ui->btnOIcon_2);
    m_vtOutPic.append(ui->btnOPic_3); m_vtOut.append(ui->btnO_3); m_vtOutIcon.append(ui->btnOIcon_3);
    m_vtOutPic.append(ui->btnOPic_4); m_vtOut.append(ui->btnO_4); m_vtOutIcon.append(ui->btnOIcon_4);
    m_vtOutPic.append(ui->btnOPic_5); m_vtOut.append(ui->btnO_5); m_vtOutIcon.append(ui->btnOIcon_5);
    m_vtOutPic.append(ui->btnOPic_6); m_vtOut.append(ui->btnO_6); m_vtOutIcon.append(ui->btnOIcon_6);
    m_vtOutPic.append(ui->btnOPic_7); m_vtOut.append(ui->btnO_7); m_vtOutIcon.append(ui->btnOIcon_7);
    m_vtOutPic.append(ui->btnOPic_8); m_vtOut.append(ui->btnO_8); m_vtOutIcon.append(ui->btnOIcon_8);
    m_vtOutPic.append(ui->btnOPic_9); m_vtOut.append(ui->btnO_9); m_vtOutIcon.append(ui->btnOIcon_9);
    m_vtOutPic.append(ui->btnOPic_10);m_vtOut.append(ui->btnO_10);m_vtOutIcon.append(ui->btnOIcon_10);
    m_vtOutPic.append(ui->btnOPic_11);m_vtOut.append(ui->btnO_11);m_vtOutIcon.append(ui->btnOIcon_11);
    m_vtOutPic.append(ui->btnOPic_12);m_vtOut.append(ui->btnO_12);m_vtOutIcon.append(ui->btnOIcon_12);
    m_vtOutPic.append(ui->btnOPic_13);m_vtOut.append(ui->btnO_13);m_vtOutIcon.append(ui->btnOIcon_13);
    m_vtOutPic.append(ui->btnOPic_14);m_vtOut.append(ui->btnO_14);m_vtOutIcon.append(ui->btnOIcon_14);
    m_vtOutPic.append(ui->btnOPic_15);m_vtOut.append(ui->btnO_15);m_vtOutIcon.append(ui->btnOIcon_15);
    m_vtOutPic.append(ui->btnOPic_16);m_vtOut.append(ui->btnO_16);m_vtOutIcon.append(ui->btnOIcon_16);

    for(int i=0;i<m_vtOutPic.count();i++)
    {
        connect(m_vtOutPic[i],SIGNAL(mouse_release()),this,SLOT(onOut()));
        connect(m_vtOut[i],SIGNAL(mouse_press()),m_vtOutPic[i],SLOT(press()));
        connect(m_vtOut[i],SIGNAL(mouse_release()),m_vtOutPic[i],SLOT(release()));
        connect(m_vtOutIcon[i],SIGNAL(mouse_press()),m_vtOutPic[i],SLOT(press()));
        connect(m_vtOutIcon[i],SIGNAL(mouse_release()),m_vtOutPic[i],SLOT(release()));
    }

    connect(ui->btnIOViewPic,SIGNAL(mouse_release()),this,SLOT(onIOView()));
    connect(ui->btnIOViewIcon,SIGNAL(mouse_press()),ui->btnIOViewPic,SLOT(press()));
    connect(ui->btnIOViewIcon,SIGNAL(mouse_release()),ui->btnIOViewPic,SLOT(release()));


    MatchingOutput();
    dig_io = 0;

}

dialog_easy_setting_io::~dialog_easy_setting_io()
{
    delete ui;
}
void dialog_easy_setting_io::showEvent(QShowEvent *)
{
    gStart_IOUpdate();

    timer->start();

    Display_OutName();
}
void dialog_easy_setting_io::hideEvent(QHideEvent *)
{
    gStop_IOUpdate();

    timer->stop();
}
void dialog_easy_setting_io::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_easy_setting_io::onClose()
{
    emit accept();
}

void dialog_easy_setting_io::onTimer()
{
    Display_OutSignal();
}

void dialog_easy_setting_io::MatchingOutput()
{
    m_vtOutNum.clear();
    m_vtOutNum.append(HyOutput::VAC1);
    m_vtOutNum.append(HyOutput::VAC2);
    m_vtOutNum.append(HyOutput::VAC3);
    m_vtOutNum.append(HyOutput::VAC4);
    m_vtOutNum.append(HyOutput::CHUCK);
    m_vtOutNum.append(HyOutput::GRIP);
    m_vtOutNum.append(HyOutput::NIPPER);
    m_vtOutNum.append(HyOutput::RGRIP);
    m_vtOutNum.append(HyOutput::USER1);
    m_vtOutNum.append(HyOutput::USER2);
    m_vtOutNum.append(HyOutput::USER3);
    m_vtOutNum.append(HyOutput::USER4);
    m_vtOutNum.append(HyOutput::USER5);
    m_vtOutNum.append(HyOutput::USER6);
    m_vtOutNum.append(HyOutput::USER7);
    m_vtOutNum.append(HyOutput::USER8);
}

void dialog_easy_setting_io::Display_OutSignal()
{
    for(int i=0;i<m_vtOutIcon.count();i++)
    {
        if(Output->IsOn(m_vtOutNum[i] - 1))
            m_vtOutIcon[i]->setAutoFillBackground(true);
        else
            m_vtOutIcon[i]->setAutoFillBackground(false);
    }
}

void dialog_easy_setting_io::onOut()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtOutPic.indexOf(btn);
    if(index < 0) return;

    // toggle
    int out=0;
    if(Output->IsOn(m_vtOutNum[index] - 1))
        out = (-1) * m_vtOutNum[index];
    else
        out = m_vtOutNum[index];

    CNRobo* pCon = CNRobo::getInstance();
    pCon->setDO(&out, 1);

}

void dialog_easy_setting_io::onIOView()
{
    if(dig_io == 0)
        dig_io = new dialog_io();

    dig_io->exec();

    // because dig_io stop ioupate flag.
    gStart_IOUpdate();

}

void dialog_easy_setting_io::Display_OutName()
{
    for(int i=0;i<m_vtOut.count();i++)
        m_vtOut[i]->setText(Output->GetUiName(m_vtOutNum[i]-1));
}
