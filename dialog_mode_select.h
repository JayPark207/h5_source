#ifndef DIALOG_MODE_SELECT_H
#define DIALOG_MODE_SELECT_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"

namespace Ui {
class dialog_mode_select;
}

#define MAX_MODE_SELECT_NUM 6

class dialog_mode_select : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_mode_select(QWidget *parent = 0);
    ~dialog_mode_select();

    void InitNum(int num); // num = 2~6
    int  GetNum();
    void InitString(int index, QString str);
    void InitRecipe(int recipe_enum);
    void InitTitle(QString title);

private:

    Ui::dialog_mode_select *ui;

    void Update();
    int m_nSelNum;
    QString m_strText[MAX_MODE_SELECT_NUM];
    int m_nRecipe;

    QVector<QLabel3*> m_vtText;
    QVector<QLabel3*> m_vtPic;

    int m_NowSelected;

    void DisplayUpdate();

    int confirm_x,confirm_y,confirm_w,confirm_h;


public slots:
    void onSelect();

    void onOK();
    void onCancel();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_MODE_SELECT_H
