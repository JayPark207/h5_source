#include "dialog_rotation.h"
#include "ui_dialog_rotation.h"

dialog_rotation::dialog_rotation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_rotation)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    m_vtPosName.clear();m_vtPosPic.clear();m_vtPosVal.clear();
    m_vtPosName.append(ui->btnPos0);m_vtPosPic.append(ui->btnPosPic0);m_vtPosVal.append(ui->btnPosVal0);
    m_vtPosName.append(ui->btnPos1);m_vtPosPic.append(ui->btnPosPic1);m_vtPosVal.append(ui->btnPosVal1);
    m_vtPosName.append(ui->btnPos2);m_vtPosPic.append(ui->btnPosPic2);m_vtPosVal.append(ui->btnPosVal2);
    m_vtPosName.append(ui->btnPos3);m_vtPosPic.append(ui->btnPosPic3);m_vtPosVal.append(ui->btnPosVal3);
    m_vtPosName.append(ui->btnPos4);m_vtPosPic.append(ui->btnPosPic4);m_vtPosVal.append(ui->btnPosVal4);
    m_vtSetName.append(ui->btnSet0);m_vtSetPic.append(ui->btnSetPic0);m_vtSetVal.append(ui->btnSetVal0);
    m_vtSetName.append(ui->btnSet1);m_vtSetPic.append(ui->btnSetPic1);m_vtSetVal.append(ui->btnSetVal1);
    m_vtSetName.append(ui->btnSet2);m_vtSetPic.append(ui->btnSetPic2);m_vtSetVal.append(ui->btnSetVal2);
    m_vtSetName.append(ui->btnSet3);m_vtSetPic.append(ui->btnSetPic3);m_vtSetVal.append(ui->btnSetVal3);
    m_vtSetName.append(ui->btnSet4);m_vtSetPic.append(ui->btnSetPic4);m_vtSetVal.append(ui->btnSetVal4);


    ui->grpSet->setGeometry(ui->grpSel->x(),
                            ui->grpSel->y(),
                            ui->grpSel->width(),
                            ui->grpSel->height());


    // confirm button event
    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    int i;
    for(i=0;i<m_vtPosPic.count();i++)
    {
        connect(m_vtPosPic[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtPosName[i],SIGNAL(mouse_press()),m_vtPosPic[i],SLOT(press()));
        connect(m_vtPosName[i],SIGNAL(mouse_release()),m_vtPosPic[i],SLOT(release()));
        connect(m_vtPosVal[i],SIGNAL(mouse_press()),m_vtPosPic[i],SLOT(press()));
        connect(m_vtPosVal[i],SIGNAL(mouse_release()),m_vtPosPic[i],SLOT(release()));

        connect(m_vtSetPic[i],SIGNAL(mouse_release()),this,SLOT(onSetting()));
        connect(m_vtSetVal[i],SIGNAL(mouse_press()),m_vtSetPic[i],SLOT(press()));
        connect(m_vtSetVal[i],SIGNAL(mouse_release()),m_vtSetPic[i],SLOT(release()));
    }

    connect(ui->btnSetModePic,SIGNAL(mouse_release()),this,SLOT(onChange()));
    connect(ui->btnSetModeIcon,SIGNAL(mouse_press()),ui->btnSetModePic,SLOT(press()));
    connect(ui->btnSetModeIcon,SIGNAL(mouse_release()),ui->btnSetModePic,SLOT(release()));


}

dialog_rotation::~dialog_rotation()
{
    delete ui;
}
void dialog_rotation::showEvent(QShowEvent *)
{
    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    Init_String();

    Update();
}
void dialog_rotation::hideEvent(QHideEvent *)
{

}
void dialog_rotation::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
        Init_String();
    }
}
void dialog_rotation::onOK()
{
    emit accept();
}
void dialog_rotation::onCancel()
{
    emit reject();
}

void dialog_rotation::Init(ENUM_TYPE type, QString now_value)
{
    m_Type = type;
    m_strNowValue = now_value;
}

QString dialog_rotation::GetValue()
{
    return m_vtPosVal[m_nSelectedIndex]->text();
}

void dialog_rotation::Init_String()
{
    m_strTitle[ROTATION] = QString(tr("Rotation Easy Setting"));
    m_strTitle[SWIVEL] = QString(tr("Swivel Easy Setting"));

    m_strPosName[ROTATION][0] = QString(tr("Return"));
    m_strPosName[ROTATION][1] = QString(tr("Rotation1"));
    m_strPosName[ROTATION][2] = QString(tr("Rotation2"));
    m_strPosName[ROTATION][3] = QString(tr("Rotation3"));
    m_strPosName[ROTATION][4] = QString(tr("Rotation4"));

    m_strPosName[SWIVEL][0] = QString(tr("Return"));
    m_strPosName[SWIVEL][1] = QString(tr("Swivel1"));
    m_strPosName[SWIVEL][2] = QString(tr("Swivel2"));
    m_strPosName[SWIVEL][3] = QString(tr("Swivel3"));
    m_strPosName[SWIVEL][4] = QString(tr("Swivel4"));

}

void dialog_rotation::Update()
{
    float data;
    QString str;

    Setting(false);

    ui->lbTitle->setText(m_strTitle[m_Type]);

    for(int i=0;i<m_vtPosName.count();i++)
    {
        m_vtPosName[i]->setText(m_strPosName[m_Type][i]);
        m_vtSetName[i]->setText(m_strPosName[m_Type][i]);

        if(m_Type == ROTATION)
        {
            if(Recipe->Get((HyRecipe::RECIPE_NUMBER)(HyRecipe::vRotPos0 + i), &data))
            {
                str.sprintf(FORMAT_POS, data);
                m_vtPosVal[i]->setText(str);
                m_vtSetVal[i]->setText(str);
            }
        }
        else //SWIVEL
        {
            if(Recipe->Get((HyRecipe::RECIPE_NUMBER)(HyRecipe::vSwvPos0 + i), &data))
            {
                str.sprintf(FORMAT_POS, data);
                m_vtPosVal[i]->setText(str);
                m_vtSetVal[i]->setText(str);
            }
        }

        if(m_vtPosVal[i]->text().toFloat() == m_strNowValue.toFloat())
        {
            Select(i, true);
        }
        else
        {
            Select(i, false);
        }

    }

}

void dialog_rotation::Select(int i, bool on)
{
    QImage img;
    QPixmap pxm;

    if(on)
    {
        img.load(":/image/image/button_midrect_2.png");
        pxm = QPixmap::fromImage(img);
        m_vtPosPic[i]->setPixmap(pxm);
        m_nSelectedIndex = i;
    }
    else
    {
        img.load(":/image/image/button_midrect_0.png");
        pxm = QPixmap::fromImage(img);
        m_vtPosPic[i]->setPixmap(pxm);
    }

}

void dialog_rotation::Setting(bool on)
{
    QImage img;
    img.load(":/image/image/button_rect2_2.png");
    QPixmap pxmon = QPixmap::fromImage(img);
    img.load(":/image/image/button_rect2_0.png");
    QPixmap pxmoff = QPixmap::fromImage(img);

    m_bSetting = on;

    if(on)
    {
        ui->btnSetModePic->setPixmap(pxmon);
        ui->grpSel->hide();
        ui->grpSet->show();

        ui->frYesNo->setEnabled(false);
    }
    else
    {
        ui->btnSetModePic->setPixmap(pxmoff);
        ui->grpSel->show();
        ui->grpSet->hide();

        ui->frYesNo->setEnabled(true);
    }
}

void dialog_rotation::onSelect()
{
    QString btn = sender()->objectName();

    for(int i=0;i<m_vtPosPic.count();i++)
    {
        if(btn == m_vtPosPic[i]->objectName())
            Select(i, true);
        else
            Select(i, false);
    }

}
void dialog_rotation::onSetting()
{
    QString btn = sender()->objectName();

    for(int i=0;i<m_vtSetPic.count();i++)
    {
        if(btn == m_vtSetPic[i]->objectName())
        {
            np->m_numpad->SetTitle(m_vtSetName[i]->text());
            np->m_numpad->SetNum(m_vtSetVal[i]->text().toDouble());

            if(i == 0)
            {
                np->m_numpad->SetMinValue(-10.0);
                np->m_numpad->SetMaxValue(10.0);
            }
            else if(i < 3)
            {
                np->m_numpad->SetMinValue(0.0);
                np->m_numpad->SetMaxValue(100.0);
            }
            else
            {
                np->m_numpad->SetMinValue(-100.0);
                np->m_numpad->SetMaxValue(0.0);
            }

            np->m_numpad->SetSosuNum(SOSU_POS);

            if(np->exec() == QDialog::Accepted)
            {
                if(m_Type == ROTATION)
                    Recipe->Set((HyRecipe::RECIPE_NUMBER)(HyRecipe::vRotPos0 + i),
                                (float)np->m_numpad->GetNumDouble());
                else
                    Recipe->Set((HyRecipe::RECIPE_NUMBER)(HyRecipe::vSwvPos0 + i),
                                (float)np->m_numpad->GetNumDouble());

                Update_Value();
            }
        }
    }
}

void dialog_rotation::Update_Value()
{
    float data;
    QString str;

    for(int i=0;i<m_vtPosName.count();i++)
    {
        if(m_Type == ROTATION)
        {
            if(Recipe->Get((HyRecipe::RECIPE_NUMBER)(HyRecipe::vRotPos0 + i), &data))
            {
                str.sprintf(FORMAT_POS, data);
                m_vtPosVal[i]->setText(str);
                m_vtSetVal[i]->setText(str);
            }
        }
        else //SWIVEL
        {
            if(Recipe->Get((HyRecipe::RECIPE_NUMBER)(HyRecipe::vSwvPos0 + i), &data))
            {
                str.sprintf(FORMAT_POS, data);
                m_vtPosVal[i]->setText(str);
                m_vtSetVal[i]->setText(str);
            }
        }

    }
}

void dialog_rotation::onChange()
{
    if(m_bSetting)
        Setting(false);
    else
        Setting(true);
}
