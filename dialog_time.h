#ifndef DIALOG_TIME_H
#define DIALOG_TIME_H

#include <QDialog>

#include "global.h"
#include "qlabel4.h"
#include "qlabel3.h"
#include "dialog_numpad.h"
#include "top_sub.h"

namespace Ui {
class dialog_time;
}

class dialog_time : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_time(QWidget *parent = 0);
    ~dialog_time();

    void Update(int page);

public slots:
    void onClose();
    void onPageUp();
    void onPageDown();
    void onSelect();

private:
    Ui::dialog_time *ui;
    dialog_numpad* np;

    QVector<QLabel4*> m_vtPic;
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtVal;
    QVector<QLabel3*> m_vtNo;

    int m_nMaxPage;
    int m_nNowPage;

    void Display_PageNum();

    top_sub* top;
    void SubTop();          // Need SubTop

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_TIME_H
