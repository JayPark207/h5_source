#ifndef CNTYPE_H
#define CNTYPE_H

#include <string.h>

#define CN_MAX_JOINT    32
#define CN_MAX_TRANS    32
#define CN_MAX_USER     32

/* 1MB */
#define CN_MAX_DATA_LENGTH      1048576
#define CN_MAX_STRING_LENGTH    256

#ifndef NULL
#define NULL    0
#endif

typedef char                cn_char;
typedef unsigned char       cn_ui8;
typedef unsigned short      cn_ui16;
typedef unsigned short int	cn_uint16;
typedef unsigned int        cn_ui32;
typedef char                cn_i8;
typedef short               cn_i16;
typedef int                 cn_i32;
typedef char*               cn_string; //[CN_MAX_STRING_LENGTH];

/**
 * @fn cn_safestr
 *   safe string :
 *      first 2 bytes :
 */
typedef float           cn_f32;
typedef double          cn_f64;

/**
 * @fn The CNOBJ_TYPE enum
 */
enum cn_vartype
{
    CNVAR_NONE = -1,
    CNVAR_STRING = 0,   /**< type value is 0, then the value is string */
    CNVAR_UINT8 = 1,
    CNVAR_UINT16,
    CNVAR_UINT32,
    CNVAR_INT8,
    CNVAR_INT16,
    CNVAR_INT32,
    CNVAR_FLOAT,
    CNVAR_DOUBLE,
    CNVAR_JOINT,
    CNVAR_TMAT,
    CNVAR_VECTOR,
    CNVAR_TRANS,
    CNVAR_SAFESTR,
    CNVAR_ARYFLOAT,
    CNVAR_ARYINT32,
    CNVAR_ARYUINT32,
    CNVAR_ARYSTRING,
    CNVAR_DATE,
};

struct cn_safestr
{
    int length;
    char*   data;
};

struct cn_vec3
{
    float   x, y, z;

    void operator = (cn_vec3 vec)
    {
        x = vec.x;
        y = vec.y;
        z = vec.z;
    }
};

struct cn_joint
{
    int count;
    float   joint[CN_MAX_JOINT];
};

struct cn_trans
{
    float   n[3];
    float   o[3];
    float   a[3];
    float   p[3];

    float   eu[3];
    float   ext[6];
};

struct cn_trans2
{
    cn_vec3 n, o, a, p;
    float ext;
};

struct cn_conf
{
    unsigned char   cfx;            // configuration
    unsigned char   cfang[3];       // angle hint
    unsigned char   cfmaster[4];    // maser-slave order
};

struct cn_location
{
    unsigned short ntrans;
    unsigned short next;
    cn_trans    trans[2];
    float   ext[6];
};

struct cn_usercoord
{
    int count;
    float   uc[32];
};

struct cn_date
{
    unsigned short  year;
    unsigned char   month;
    unsigned char   day;
};


#define CN_MAX_CHARS    256
struct cn_variant {
    cn_vartype type;
    union {
        cn_ui8  ui8;
        cn_ui16 ui16;
        cn_ui32 ui32;
        cn_i8   i8;
        cn_i16  i16;
        cn_i32  i32;
        cn_f32  f32;
        cn_f64  f64;
        cn_char   str[CN_MAX_CHARS];    /**< if type is CNOBJ_STRING and str is not null, careful about alloc/free */
        cn_joint    joint;
        cn_trans    trans;
        cn_location location;
        cn_usercoord uc;
        cn_safestr  sstr;
        cn_date     date;
    } val;
};


/* trajectry data */
struct cn_trajectory
{
    float S;
    cn_i32 enc[32];
    cn_vec3 trans;
};

/* robot variable type */
#define CN_ROBOT_VARIABLE_JOINT     1
#define CN_ROBOT_VARIABLE_TRANS     2
#define CN_ROBOT_VARIABLE_NUMBER    3
#define CN_ROBOT_VARIABLE_STRING    4


#endif // CNTYPE_H

