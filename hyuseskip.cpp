#include "hyuseskip.h"

HyUseSkip::HyUseSkip(HyParam* param, HyRecipe* recipe)
{
    m_Param = param;
    m_Recipe = recipe;

    Init();
    m_vtUseSkip.clear();
    m_vtUseSkip.fill(false, USESKIP_NUM);
}

void HyUseSkip::Init()
{
    m_Name[BTN_BEEP] = QString(tr("Button Beep"));
    m_Name[CAPTURE] = QString(tr("Display Capture"));
    m_Name[TOOL_COORD] = QString(tr("Tool Coordinate"));
    m_Name[SOFTSENSOR_MON] = QString(tr("SoftSensor Monitoring"));
    m_Name[USESKIP_5] = QString(tr("Skip Ext.Machine"));  // temp.
    m_Name[USESKIP_6] = QString(tr("Skip Flame"));        // temp.
    m_Name[USESKIP_7] = QString(tr(""));
    m_Name[USESKIP_8] = QString(tr(""));

    m_Name[USESKIP_9] = QString(tr(""));
    m_Name[USESKIP_10] = QString(tr(""));
    m_Name[USESKIP_11] = QString(tr(""));
    m_Name[USESKIP_12] = QString(tr(""));
    m_Name[USESKIP_13] = QString(tr(""));
    m_Name[USESKIP_14] = QString(tr(""));
    m_Name[USESKIP_15] = QString(tr(""));
    m_Name[USESKIP_16] = QString(tr(""));

}

bool HyUseSkip::Read()
{
    // read userparam.
    QString str = m_Param->Get(HyParam::USESKIP);
    QStringList list = str.split(",");
    if(str.isEmpty() || list.size() != USESKIP_NUM)
    {
        qDebug() << "HyUseSkip Read() Exception. Make Default!!";
        Set_Default_Param();

        str = m_Param->Get(HyParam::USESKIP);
        list.clear();
        list = str.split(",");
    }

    m_vtUseSkip.clear();
    for(int i=0;i<list.count();i++)
        m_vtUseSkip.append((list[i].toInt() > 0));

    return true;
}

bool HyUseSkip::Write(bool save)
{
    // write userparam
    QString str = Get_Param_Format();
    qDebug() << str;
    m_Param->Set(HyParam::USESKIP, str);

    QVector<float> datas;
    datas.clear();
    for(int i=0;i<m_vtUseSkip.count();i++)
    {
        if(m_vtUseSkip[i])
            datas.append(1.0);
        else
            datas.append(0.0);
    }

    if(!m_Recipe->SetsArr("useskip", datas, save))
        return false;

    return true;
}
bool HyUseSkip::Sync(bool save)
{
    if(!Read())
        return false;
    if(!Write(save))
        return false;

    return true;
}

void HyUseSkip::Set(ENUM_USESKIP index, bool bUseSKip)
{
    if(index >= m_vtUseSkip.size())
        return;

    m_vtUseSkip[index] = bUseSKip;
}
bool HyUseSkip::Get(ENUM_USESKIP index)
{
    if(index >= m_vtUseSkip.size())
        return false;

    if(m_vtUseSkip[(int)index])
        return true;
    return false;
}
QString HyUseSkip::GetName(ENUM_USESKIP index)
{
    if(index >= USESKIP_NUM)
        return "";

    return m_Name[index];
}

int HyUseSkip::Count()
{
    return m_vtUseSkip.count();
}
void HyUseSkip::Set_Default_Param()
{
    QString str = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    m_Param->Set(HyParam::USESKIP, str);
}
QString HyUseSkip::Get_Param_Format()
{
    QString str;
    str.clear();
    for(int i=0;i<m_vtUseSkip.count();i++)
    {
        if(m_vtUseSkip[i])
            str += "1";
        else
            str += "0";

        if(i < (m_vtUseSkip.count()-1))
            str += ",";
    }

    return str;
}

