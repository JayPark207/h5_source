#include "hysasang_motion.h"

HySasang_Motion::HySasang_Motion()
{
    Init_String();

    m_Posi = new HyPos(0); // for trans function
}

void HySasang_Motion::Init_String()
{
    // Command_UiName & Command_Name must match refer to COMMAND enum
    Command_UiName.clear();
    Command_UiName.append(tr("P2P"));
    Command_UiName.append(tr("Continue"));
    Command_UiName.append(tr("Circle"));
    Command_UiName.append(tr("Output"));
    Command_UiName.append(tr("Delay"));
    Command_UiName.append(tr("Input")); // not sure

    Command_UiName_Start = tr("#START#");
    Command_UiName_End = tr("#END#");

    Command_Name.clear();
    for(int i=0;i<COMMAND_NUM;i++)
    {
        Command_Name.append(toStr((COMMAND)i));
        //qDebug() << Command_Name[i];
    }
}

bool HySasang_Motion::Set(int index, ST_SSMOTION_DATA &data)
{
    if(index < 0) return false;
    if(index >= MotionList.size()) return false;

    MotionList[index] = data;
    return true;
}
bool HySasang_Motion::Get(int index, ST_SSMOTION_DATA &data)
{
    if(index < 0) return false;
    if(index >= MotionList.size()) return false;

    data = MotionList[index];
    return true;
}
QString HySasang_Motion::GetName(int index)
{
    QString str;
    str.clear();
    if(index < 0) return str;
    if(index >= MotionList.size()) return str;

    int cmd_index = Command_Name.indexOf(MotionList[index].command);
    if(cmd_index < 0) return str;
    if(cmd_index >= Command_UiName.size()) return str;

    // index first
    if(index == 0)
    {
        if(cmd_index == p2p)
            return Command_UiName_Start;
        else
            return str;
    }

    // index last
    if(index == (MotionList.size()-1))
    {
        if(cmd_index == p2p)
            return Command_UiName_End;
        else
            return str;
    }

    return Command_UiName[cmd_index];
}
int HySasang_Motion::GetCount()
{
    return MotionList.count();
}
void HySasang_Motion::ClearList()
{
    MotionList.clear();
}

void HySasang_Motion::NewList()
{
    MotionList.clear();

    // default List (Start & End)
    ST_SSMOTION_DATA data;
    Default(data);
    data.command = toStr(p2p);
    MotionList.append(data);
    data.command = toStr(p2p);
    MotionList.append(data);
}

bool HySasang_Motion::Default(ST_SSMOTION_DATA &data)
{
    data.raw.clear();
    data.program.clear();

    data.command.clear();
    for(int i=0;i<CN_MAX_JOINT;i++)
    {
        data.joint[0].joint[i] = 0.0;
        data.joint[1].joint[i] = 0.0;
    }

    CNRobo* pCon = CNRobo::getInstance();
    pCon->calcForward(data.joint[0].joint, data.trans[0]);
    pCon->calcForward(data.joint[1].joint, data.trans[1]);

    data.speed_output = 0.0;
    data.accuracy_input = 0.0;
    data.type = 0;
    data.time = 0;

    return true;
}

bool HySasang_Motion::SetDefault(ST_SSMOTION_DATA data)
{
    m_DefaultData = data;
    return true;
}

int HySasang_Motion::IndexOf(QString command)
{
    return Command_Name.indexOf(command);
}


bool HySasang_Motion::Add(ST_SSMOTION_DATA &data, int index)
{
    if(index < 0)
    {
        // insert back.
        //MotionList.push_back(data);
        // insert front end.
        MotionList.insert((MotionList.size()-1), data);
    }
    else
    {
        // insert at index.
        if(index < 0) return false;
        if(index > MotionList.size()) return false;

        MotionList.insert(index, data);
    }

    qDebug() << "SSMotion Add " << index << data.command;
    return true;
}
bool HySasang_Motion::Append(ST_SSMOTION_DATA &data)
{
    MotionList.append(data);
    return true;
}

bool HySasang_Motion::Del(int index)
{
    if(index < 1) return false;
    if(index >= (MotionList.size()-1)) return false;

    ST_SSMOTION_DATA data;
    data = MotionList.takeAt(index);

    qDebug() << "SSMotion Del " << index << data.command;
    return true;
}
bool HySasang_Motion::Up(int index)
{
    if(index <= 1) return false;
    if(index >= (MotionList.size()-1)) return false;

    MotionList.move(index, index-1);

    // for debug
    for(int i=0;i<MotionList.count();i++)
        qDebug() << i << MotionList[i].command;

    return true;
}
bool HySasang_Motion::Down(int index)
{
    if(index >= MotionList.size()-2) return false;
    if(index < 1) return false;

    MotionList.move(index, index+1);

    // for debug
    for(int i=0;i<MotionList.count();i++)
        qDebug() << i << MotionList[i].command;

    return true;
}

bool HySasang_Motion::MakeTestMain()
{
    if(MotionList.isEmpty()) return false;

    qDebug() << "MakeTestMain() Start!";

    //1. make call program.
    QStringList program;
    QString str;
    int i;

    program.clear();
    for(i=0;i<MotionList.count();i++)
    {
        str = "Call ";
        str += GetName_TestSubMacro();
        str += QString().setNum(i);

        program.append(str);

        qDebug() << str;
    }

    //2. make & write main program
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    QString main_name = GetName_TestMainMacro();

    //2-1. If it don't have main macro, make file.
    if(!pCon->findProgram(main_name))
    {
        ret = pCon->createProgram(main_name);
        if(ret < 0)
        {
            qDebug() << "MakeTestMain() creatProgram ret =" << ret;
            qDebug() << "Main Name =" << main_name;
            return false;
        }
    }

    //2-2. Write main program
    ret = pCon->setProgramSteps(main_name, program);
    if(ret < 0)
    {
        qDebug() << "MakeTestMain() setProgramSteps ret =" << ret;
        qDebug() << "Main Name =" << main_name;
        return false;
    }

    return true;
}

bool HySasang_Motion::MakeTestProgram()    // all together running.
{
    if(MotionList.isEmpty()) return false;

    QElapsedTimer ti;
    ti.start();

    //1. Data2Prog & Prog2Macro
    int i;
    for(i=0;i<MotionList.count();i++)
    {
//        if(!Data2Prog(MotionList[i]))
//        {
//            qDebug() << "MakeTestProgram() Data2Prog fail index=" << i;
//            return false;
//        }

        if(!Prog2Macro(MotionList[i].program, i))
        {
            qDebug() << "MakeTestProgram() Prog2Macro fail index=" << i;
            return false;
        }
    }
    //2. MakeTestMain

    if(!MakeTestMain())
    {
        qDebug() << "MakeTestProgram() MakeTestMain fail!";
        return false;
    }

    qDebug() << "MakeTestProgram() processing time = " << ti.elapsed() << "msec";

    return true;
}

int HySasang_Motion::MakeTestProgram(QList<ST_SSMOTION_DATA>& comp_motion_list)    // all together running.
{
    if(MotionList.isEmpty()) return false;

    QElapsedTimer ti;
    ti.start();

    if(MotionList.size() != comp_motion_list.size())
    {
        qDebug() << "HySasang_Motion MakeTestProgram(...) Not Same List Size.";
        return -2;
    }

    //1. Data2Prog & Prog2Macro
    int i;
    for(i=0;i<MotionList.count();i++)
    {
        if(!IsSameProg(MotionList[i], comp_motion_list[i]))
        {
            qDebug() << "MakeTestProgram(..) Not Same index =" << i;
//            if(!Data2Prog(MotionList[i]))
//            {
//                qDebug() << "MakeTestProgram(..) Data2Prog fail index=" << i;
//                return -1;
//            }
            if(!Prog2Macro(MotionList[i].program, i))
            {
                qDebug() << "MakeTestProgram(..) Prog2Macro fail index=" << i;
                return -1;
            }
        }
    }
//    //2. MakeTestMain
//    if(!MakeTestMain())
//    {
//        qDebug() << "MakeTestProgram() MakeTestMain fail!";
//        return false;
//    }

    qDebug() << "MakeTestProgram() processing time = " << ti.elapsed() << "msec";

    return 0;
}


/** data <-> file protocol summary
 *
 * p2p,speed,x,y,z,a,b,c,j1,...,jn
 * con,speed,accuracy,x,y,z,a,b,c,j1,...,jn
 * cir,speed,x,y,z,a,b,c,j1,...,jn@x,y,z,a,b,c,j1,...,jn (1st=middle pos, 2nd=end pos)
 * out,number,type,pulse_time
 * wtt,wait_time
 *
**/

bool HySasang_Motion::Data2Raw(ST_SSMOTION_DATA& data)
{
    qDebug() << "Data2Raw Start!";

    int cmd_index = Command_Name.indexOf(data.command);
    if(cmd_index < 0)
        return false;

    QString str;
    QStringList raw,raw2;
    int i;

    raw.clear();
    raw2.clear();

    switch(cmd_index)
    {
    case p2p:
        raw.append(toStr(p2p));
        str.sprintf(FORMAT_SPEED, data.speed_output);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TX]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TY]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TZ]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TA]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TB]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TC]);
        raw.append(str);
        for(i=0;i<MAX_AXIS_NUM;i++)
        {
            str.sprintf(FORMAT_POS, data.joint[0].joint[i]);
            raw.append(str);
        }

        data.raw = raw.join(",");
        break;

    case con:
        raw.append(toStr(con));
        str.sprintf(FORMAT_SPEED, data.speed_output);
        raw.append(str);
        str.sprintf(FORMAT_SPEED, data.accuracy_input);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TX]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TY]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TZ]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TA]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TB]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TC]);
        raw.append(str);
        for(i=0;i<MAX_AXIS_NUM;i++)
        {
            str.sprintf(FORMAT_POS, data.joint[0].joint[i]);
            raw.append(str);
        }

        data.raw = raw.join(",");
        break;

    case cir:
        raw.append(toStr(cir));
        // middle point
        str.sprintf(FORMAT_SPEED, data.speed_output);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TX]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TY]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].p[TZ]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TA]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TB]);
        raw.append(str);
        str.sprintf(FORMAT_POS, data.trans[0].eu[TC]);
        raw.append(str);
        for(i=0;i<MAX_AXIS_NUM;i++)
        {
            str.sprintf(FORMAT_POS, data.joint[0].joint[i]);
            raw.append(str);
        }
        // end point
        str.sprintf(FORMAT_POS, data.trans[1].p[TX]);
        raw2.append(str);
        str.sprintf(FORMAT_POS, data.trans[1].p[TY]);
        raw2.append(str);
        str.sprintf(FORMAT_POS, data.trans[1].p[TZ]);
        raw2.append(str);
        str.sprintf(FORMAT_POS, data.trans[1].eu[TA]);
        raw2.append(str);
        str.sprintf(FORMAT_POS, data.trans[1].eu[TB]);
        raw2.append(str);
        str.sprintf(FORMAT_POS, data.trans[1].eu[TC]);
        raw2.append(str);
        for(i=0;i<MAX_AXIS_NUM;i++)
        {
            str.sprintf(FORMAT_POS, data.joint[1].joint[i]);
            raw2.append(str);
        }

        data.raw = raw.join(",");
        data.raw += QString("@");
        data.raw += raw2.join(",");
        break;

    case out:
        raw.append(toStr(out));
        str.sprintf("%.0f", data.speed_output);
        raw.append(str);
        str.sprintf("%.0f", data.type);
        raw.append(str);
        str.sprintf(FORMAT_TIME, data.time);
        raw.append(str);

        data.raw = raw.join(",");
        break;

    case wtt:
        raw.append(toStr(wtt));
        str.sprintf(FORMAT_TIME, data.time);
        raw.append(str);

        data.raw = raw.join(",");
        break;

    default:
        return false;
    }

    qDebug() << "Data2Raw Success!";
    qDebug() << "raw" << data.raw;
    qDebug() << "command" <<data.command;
    qDebug() << "trans[0] xyz" << data.trans[0].p[TX] << data.trans[0].p[TY] << data.trans[0].p[TZ];
    qDebug() << "trans[0] abc" << data.trans[0].eu[TA] << data.trans[0].eu[TB] << data.trans[0].eu[TC];
    qDebug() << "trans[1] xyz" << data.trans[1].p[TX] << data.trans[1].p[TY] << data.trans[1].p[TZ];
    qDebug() << "trans[1] abc" << data.trans[1].eu[TA] << data.trans[1].eu[TB] << data.trans[1].eu[TC];
    for(i=0;i<MAX_AXIS_NUM;i++)
        qDebug() << "joint[0],[1] J" << i+1 << data.joint[0].joint[i] << data.joint[1].joint[i];
    qDebug() << "speed_ouput" << data.speed_output;
    qDebug() << "accuracy_input" << data.accuracy_input;
    qDebug() << "type" << data.type;
    qDebug() << "time" << data.time;
    return true;
}

bool HySasang_Motion::Raw2Data(ST_SSMOTION_DATA& data)
{
    qDebug() << "Raw2Data Start!";

    QStringList raw_list;
    QStringList raw_list2, temp_list;
    raw_list.clear();
    raw_list2.clear();temp_list.clear();

    raw_list = data.raw.split(",");
    if(raw_list.isEmpty()) return false;

    int cmd_index = Command_Name.indexOf(raw_list[0]);
    if(cmd_index < 0)
        return false;

    int i;

    switch(cmd_index)
    {
    case p2p:
        data.command            = raw_list[0];
        data.speed_output       = raw_list[1].toFloat();
        data.trans[0].p[TX]     = raw_list[2].toFloat();
        data.trans[0].p[TY]     = raw_list[3].toFloat();
        data.trans[0].p[TZ]     = raw_list[4].toFloat();
        data.trans[0].eu[TA]    = raw_list[5].toFloat();
        data.trans[0].eu[TB]    = raw_list[6].toFloat();
        data.trans[0].eu[TC]    = raw_list[7].toFloat();
        for(i=0;i<MAX_AXIS_NUM;i++)
        {
            if((raw_list.count()-8) > i)
                data.joint[0].joint[i] = raw_list[8+i].toFloat();
            else
                data.joint[0].joint[i] = 0.0;
        }

        data.trans[1] = m_DefaultData.trans[1];
        data.joint[1] = m_DefaultData.joint[1];
        break;

    case con:
        data.command            = raw_list[0];
        data.speed_output       = raw_list[1].toFloat();
        data.accuracy_input     = raw_list[2].toFloat();
        data.trans[0].p[TX]     = raw_list[3].toFloat();
        data.trans[0].p[TY]     = raw_list[4].toFloat();
        data.trans[0].p[TZ]     = raw_list[5].toFloat();
        data.trans[0].eu[TA]    = raw_list[6].toFloat();
        data.trans[0].eu[TB]    = raw_list[7].toFloat();
        data.trans[0].eu[TC]    = raw_list[8].toFloat();
        for(i=0;i<MAX_AXIS_NUM;i++)
        {
            if((raw_list.count()-9) > i)
                data.joint[0].joint[i] = raw_list[9+i].toFloat();
            else
                data.joint[0].joint[i] = 0.0;
        }

        data.trans[1] = m_DefaultData.trans[1];
        data.joint[1] = m_DefaultData.joint[1];
        break;

    case cir:
        if(data.raw.indexOf("@") < 0) return false;
        temp_list = data.raw.split("@");
        if(temp_list.size() != 2) return false;

        raw_list = temp_list[0].split(",");
        raw_list2 = temp_list[1].split(",");

        if(raw_list.isEmpty() || raw_list2.isEmpty()) return false;

        data.command            = raw_list[0];
        data.speed_output       = raw_list[1].toFloat();
        data.trans[0].p[TX]     = raw_list[2].toFloat();
        data.trans[0].p[TY]     = raw_list[3].toFloat();
        data.trans[0].p[TZ]     = raw_list[4].toFloat();
        data.trans[0].eu[TA]    = raw_list[5].toFloat();
        data.trans[0].eu[TB]    = raw_list[6].toFloat();
        data.trans[0].eu[TC]    = raw_list[7].toFloat();
        for(i=0;i<MAX_AXIS_NUM;i++)
        {
            if((raw_list.count()-8) > i)
                data.joint[0].joint[i] = raw_list[8+i].toFloat();
            else
                data.joint[0].joint[i] = 0.0;
        }

        data.trans[1].p[TX]     = raw_list2[0].toFloat();
        data.trans[1].p[TY]     = raw_list2[1].toFloat();
        data.trans[1].p[TZ]     = raw_list2[2].toFloat();
        data.trans[1].eu[TA]    = raw_list2[3].toFloat();
        data.trans[1].eu[TB]    = raw_list2[4].toFloat();
        data.trans[1].eu[TC]    = raw_list2[5].toFloat();
        for(i=0;i<MAX_AXIS_NUM;i++)
        {
            if((raw_list2.count()-6) > i)
                data.joint[1].joint[i] = raw_list2[6+i].toFloat();
            else
                data.joint[1].joint[i] = 0.0;
        }
        break;

    case out:
        data.command            = raw_list[0];
        data.speed_output       = raw_list[1].toFloat();
        data.type               = raw_list[2].toFloat();
        data.time               = raw_list[3].toFloat();

        data.trans[0] = m_DefaultData.trans[0];
        data.trans[1] = m_DefaultData.trans[1];
        data.joint[0] = m_DefaultData.joint[0];
        data.joint[1] = m_DefaultData.joint[1];
        break;

    case wtt:
        data.command            = raw_list[0];
        data.time               = raw_list[1].toFloat();

        data.trans[0] = m_DefaultData.trans[0];
        data.trans[1] = m_DefaultData.trans[1];
        data.joint[0] = m_DefaultData.joint[0];
        data.joint[1] = m_DefaultData.joint[1];
        break;

    default:
        return false;
    }

    qDebug() << "Raw2Data Success!";
    qDebug() << "raw" << data.raw;
    qDebug() << "command" <<data.command;
    qDebug() << "trans[0] xyz" << data.trans[0].p[TX] << data.trans[0].p[TY] << data.trans[0].p[TZ];
    qDebug() << "trans[0] abc" << data.trans[0].eu[TA] << data.trans[0].eu[TB] << data.trans[0].eu[TC];
    qDebug() << "trans[1] xyz" << data.trans[1].p[TX] << data.trans[1].p[TY] << data.trans[1].p[TZ];
    qDebug() << "trans[1] abc" << data.trans[1].eu[TA] << data.trans[1].eu[TB] << data.trans[1].eu[TC];
    for(i=0;i<MAX_AXIS_NUM;i++)
        qDebug() << "joint[0],[1] J" << i+1 << data.joint[0].joint[i] << data.joint[1].joint[i];
    qDebug() << "speed_ouput" << data.speed_output;
    qDebug() << "accuracy_input" << data.accuracy_input;
    qDebug() << "type" << data.type;
    qDebug() << "time" << data.time;
    return true;
}

bool HySasang_Motion::Data2Prog(ST_SSMOTION_DATA &data)
{
    qDebug() << "Data2Prog Start!";

    data.program.clear();

    int cmd_index = Command_Name.indexOf(data.command);
    if(cmd_index < 0)
        return false;

    bool bRet = false;
    switch(cmd_index)
    {
    case p2p:
        //bRet = Prog_p2p(data);
        bRet = Prog_p2p_J(data);
        break;
    case con:
        //bRet = Prog_con(data);
        bRet = Prog_con_J(data);
        break;
    case cir:
        //bRet = Prog_cir(data);
        bRet = Prog_cir_J(data);
        break;
    case out:
        //bRet = Prog_out(data);
        bRet = Prog_out2(data);
        break;
    case wtt:
        bRet = Prog_wtt(data);
        break;
    }

    return bRet;
}

bool HySasang_Motion::Prog2Macro(QStringList prog, int list_index)
{
    if(prog.isEmpty()) return false;

    QString macroname;
    macroname = GetName_TestSubMacro();
    macroname += QString().setNum(list_index);
    qDebug() << "Prog2Macro Start! Macro Name =" << macroname;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    //1. If file is not exits, make file
    if(!pCon->findProgram(macroname))
    {
        ret = pCon->createProgram(macroname);
        if(ret < 0)
        {
            qDebug() << "Prog2Macro() creatProgram ret =" << ret;
            qDebug() << "Macro Name =" << macroname;
            return false;
        }
    }

    //2. use createProgramFile
    ret = pCon->setProgramSteps(macroname, prog);
    if(ret < 0)
    {
        qDebug() << "Prog2Macro() setProgramSteps ret =" << ret;
        qDebug() << "Macro Name =" << macroname;
        return false;
    }

    return true;
}

bool HySasang_Motion::Raw2Prog(QString raw, QStringList &prog)
{
    ST_SSMOTION_DATA data;
    data.raw = raw;

    if(!Raw2Data(data))
        return false;

    if(!Data2Prog(data))
        return false;

    prog = data.program;
    return true;
}

int HySasang_Motion::DataShift(ST_SSMOTION_DATA &data, ST_SSSHIFT_VALUE shift)
{
    if(!IsMoveCommand(data.command))
        return 0; // continue

    ST_SSMOTION_DATA shift_data;
    shift_data = data;

    // x,y,z translate
    if(shift.x != 0 || shift.y != 0 || shift.z != 0)
    {
        shift_data.trans[0].p[TX] += shift.x;
        shift_data.trans[0].p[TY] += shift.y;
        shift_data.trans[0].p[TZ] += shift.z;

        if(!m_Posi->Trans2Joint(shift_data.trans[0], data.joint[0], shift_data.joint[0]))
            return -1;

        if(data.command == toStr(cir))
        {
            shift_data.trans[1].p[TX] += shift.x;
            shift_data.trans[1].p[TY] += shift.y;
            shift_data.trans[1].p[TZ] += shift.z;

            if(!m_Posi->Trans2Joint(shift_data.trans[1], data.joint[1], shift_data.joint[1]))
                return -1;
        }
    }

    // rotation, swivel translate.
    if(shift.r != 0 || shift.s != 0 || shift.j6 != 0)
    {
        shift_data.joint[0].joint[JROT] += shift.r;
        shift_data.joint[0].joint[JSWV] += shift.s;
        shift_data.joint[0].joint[J6] += shift.j6;

        if(!m_Posi->Joint2Trans(shift_data.joint[0], shift_data.trans[0]))
            return -1;

        if(data.command == toStr(cir))
        {
            shift_data.joint[1].joint[JROT] += shift.r;
            shift_data.joint[1].joint[JSWV] += shift.s;
            shift_data.joint[1].joint[J6] += shift.j6;

            if(!m_Posi->Joint2Trans(shift_data.joint[1], shift_data.trans[1]))
                return -1;
        }
    }

    data = shift_data;
    return 1;
}
bool HySasang_Motion::IsMoveCommand(QString command)
{
    if(command == toStr(p2p)
    || command == toStr(con)
    || command == toStr(cir))
        return true;
    return false;
}

int HySasang_Motion::SetSpeed(ST_SSMOTION_DATA &data, float speed)
{
    if(!IsMoveCommand(data.command))
        return 0; // continue

    data.speed_output = speed;

    if(!Data2Raw(data))
        return -1;

    if(!Data2Prog(data))
        return -1;

    return 1;
}
int HySasang_Motion::SetAccuracy(ST_SSMOTION_DATA &data, float accuracy)
{
    if(!IsMoveCommand(data.command))
        return 0; // continue
    if(data.command != toStr(con))
        return 0; // continue

    data.accuracy_input = accuracy;

    if(!Data2Raw(data))
        return -1;

    if(!Data2Prog(data))
        return -1;

    return 1;
}


QString HySasang_Motion::toStr(COMMAND cmd)
{
    const QMetaObject mo = staticMetaObject;
    int index = mo.indexOfEnumerator("COMMAND");
    if(index == -1)
        return "";
    QMetaEnum metaenum = mo.enumerator(index);
    return QString(metaenum.valueToKey(cmd));
}

bool HySasang_Motion::IsSameData(ST_SSMOTION_DATA &data1, ST_SSMOTION_DATA &data2)
{
    int i;

    if(data1.command != data2.command)
        return false;
    for(i=0;i<3;i++)
    {
        if(data1.trans[0].p[i] != data2.trans[0].p[i])
            return false;
        if(data1.trans[1].p[i] != data2.trans[1].p[i])
            return false;

        if(data1.trans[0].eu[i] != data2.trans[0].eu[i])
            return false;
        if(data1.trans[1].eu[i] != data2.trans[1].eu[i])
            return false;
    }

    for(i=0;i<USE_AXIS_NUM;i++)
    {
        if(data1.joint[0].joint[i] != data2.joint[0].joint[i])
            return false;
        if(data1.joint[1].joint[i] != data2.joint[1].joint[i])
            return false;
    }

    if(data1.speed_output != data2.speed_output)
        return false;
    if(data1.accuracy_input != data2.accuracy_input)
        return false;
    if(data1.type != data2.type)
        return false;
    if(data1.time != data2.time)
        return false;

    return true;
}

bool HySasang_Motion::IsSameProg(ST_SSMOTION_DATA &data1, ST_SSMOTION_DATA &data2)
{
    if(data1.program != data2.program)
        return false;
    return true;
}
bool HySasang_Motion::IsSameRaw(ST_SSMOTION_DATA &data1, ST_SSMOTION_DATA &data2)
{
    if(data1.raw != data2.raw)
        return false;
    return true;
}

/** transfer program functions **/

bool HySasang_Motion::Prog_p2p(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(p2p))
        return false;

    QStringList prog;
    QString str;

    prog.clear();

    /**
     *  Point sstpos = Trans(X,Y,Z,A,B,C);
     *  Speed speed;
     *  //Accel speed;
     *  //Decel speed;
     *  MoveL sstpos;
     *  Break;
     *
    **/

    str.sprintf("Point sstpos = Trans(%.2f,%.2f,%.2f,%.2f,%.2f,%.2f);"
                ,data.trans[0].p[TX],data.trans[0].p[TY],data.trans[0].p[TZ]
                ,data.trans[0].eu[TA],data.trans[0].eu[TB],data.trans[0].eu[TC]);
    prog.append(str);
    str.sprintf("Speed %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("Accel %.1f;", data.speed_output);
    prog.append(str);
    str.sprintf("Decel %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("MoveL sstpos;");
    prog.append(str);
    str.sprintf("Break;");
    prog.append(str);

    data.program = prog;

    qDebug() << "Prog_p2p Success!";
    qDebug() << data.program;

    return true;
}
bool HySasang_Motion::Prog_con(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(con))
        return false;

    QStringList prog;
    QString str;

    prog.clear();

    /**
     *  Point sstpos = Trans(X,Y,Z,A,B,C);
     *  Speed speed;
     *  //Accel speed;
     *  //Decel speed;
     *  Accuracy accuracy;
     *  MoveL sstpos;
     *
    **/

    str.sprintf("Point sstpos = Trans(%.2f,%.2f,%.2f,%.2f,%.2f,%.2f);"
                ,data.trans[0].p[TX],data.trans[0].p[TY],data.trans[0].p[TZ]
                ,data.trans[0].eu[TA],data.trans[0].eu[TB],data.trans[0].eu[TC]);
    prog.append(str);
    str.sprintf("Speed %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("Accel %.1f;", data.speed_output);
    prog.append(str);
    str.sprintf("Decel %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("Accuracy %.1f;", data.accuracy_input);
    prog.append(str);
    str.sprintf("MoveL sstpos;");
    prog.append(str);

    data.program = prog;

    qDebug() << "Prog_con Success!";
    qDebug() << data.program;

    return true;
}
bool HySasang_Motion::Prog_cir(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(cir))
        return false;

    QStringList prog;
    QString str;

    prog.clear();

    /**
     *  Point sstpos = Trans(X,Y,Z,A,B,C);
     *  Point sstpos2 = Trans(X,Y,Z,A,B,C);
     *  Speed speed;
     *  //Accel speed;
     *  //Decel speed;
     *  MoveC sstpos,sstpos2;
     *  Break;
     *
    **/

    str.sprintf("Point sstpos = Trans(%.2f,%.2f,%.2f,%.2f,%.2f,%.2f);"
                ,data.trans[0].p[TX],data.trans[0].p[TY],data.trans[0].p[TZ]
                ,data.trans[0].eu[TA],data.trans[0].eu[TB],data.trans[0].eu[TC]);
    prog.append(str);
    str.sprintf("Point sstpos2 = Trans(%.2f,%.2f,%.2f,%.2f,%.2f,%.2f);"
                ,data.trans[1].p[TX],data.trans[1].p[TY],data.trans[1].p[TZ]
                ,data.trans[1].eu[TA],data.trans[1].eu[TB],data.trans[1].eu[TC]);
    prog.append(str);
    str.sprintf("Speed %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("Accel %.1f;", data.speed_output);
    prog.append(str);
    str.sprintf("Decel %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("MoveC sstpos,sstpos2;");
    prog.append(str);
    str.sprintf("Break;");
    prog.append(str);

    data.program = prog;

    qDebug() << "Prog_cir Success!";  
    qDebug() << data.program;

    return true;
}
bool HySasang_Motion::Prog_out(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(out))
        return false;

    QStringList prog;
    QString str;

    prog.clear();

    switch((int)data.type)
    {
    case 0: //OFF
        str.sprintf("SetDO -%.0f;", data.speed_output);
        prog.append(str);
        break;

    case 1: // ON
        str.sprintf("SetDO %.0f;", data.speed_output);
        prog.append(str);
        break;

    case 2: // PULSE
        str.sprintf("PulseDO %.0f,%.2f;", data.speed_output, data.time);
        prog.append(str);
        break;
    }

    data.program = prog;

    qDebug() << "Prog_out Success!";
    qDebug() << data.program;

    return true;
}
bool HySasang_Motion::Prog_out2(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(out))
        return false;

    QStringList prog;
    QString str;
    int state_no;

    prog.clear();

    // check for dual sol
    switch((int)data.speed_output)
    {
    case HyOutput::VAC1:
        state_no = 1;
        break;
    case HyOutput::VAC2:
        state_no = 2;
        break;
    case HyOutput::VAC3:
        state_no = 3;
        break;
    case HyOutput::VAC4:
        state_no = 4;
        break;
    case HyOutput::CHUCK:
        state_no = 5;
        break;
    // add here

    default:
        state_no = 0;
        break;
    }

    if(state_no >= 1)
    {
        switch((int)data.type)
        {
        case 0: //OFF
            str.sprintf("dsol_state[%d] = OFF", state_no);
            prog.append(str);
            break;

        case 1: // ON
            str.sprintf("dsol_state[%d] = ON", state_no);
            prog.append(str);
            break;

        case 2: // PULSE
            str.sprintf("dsol_state[%d] = ON", state_no);
            prog.append(str);
            str.sprintf("WaitTime %.2f", data.time);
            prog.append(str);
            str.sprintf("dsol_state[%d] = OFF", state_no);
            prog.append(str);
            break;
        }
    }
    else
    {
        // for normal output
        switch((int)data.type)
        {
        case 0: //OFF
            str.sprintf("SetDO -%.0f;", data.speed_output);
            prog.append(str);
            break;

        case 1: // ON
            str.sprintf("SetDO %.0f;", data.speed_output);
            prog.append(str);
            break;

        case 2: // PULSE
            str.sprintf("PulseDO %.0f,%.2f;", data.speed_output, data.time);
            prog.append(str);
            break;
        }
    }

    data.program = prog;

    qDebug() << "Prog_out Success!";
    qDebug() << data.program;

    return true;
}

bool HySasang_Motion::Prog_wtt(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(wtt))
        return false;

    QStringList prog;
    QString str;

    prog.clear();

    /**
     *  WaitTime time;
    **/

    str.sprintf("WaitTime %.2f;", data.time);
    prog.append(str);

    data.program = prog;

    qDebug() << "Prog_wtt Success!";
    qDebug() << data.program;

    return true;
}


bool HySasang_Motion::Prog_p2p_J(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(p2p))
        return false;

    QStringList prog;
    QString str;

    prog.clear();

    /**
     *  Point #ssjpos = Joint(J1,J2,J3,J4,J5,...);
     *  Speed speed;
     *  //Accel speed;
     *  //Decel speed;
     *  MoveL #sstpos;
     *  Break;
     *
    **/

    QStringList jval;
    jval.clear();
    for(int i=0;i<USE_AXIS_NUM;i++)
    {
        str.sprintf("%.2f", data.joint[0].joint[i]);
        jval.append(str);
    }

    str = "Point #ssjpos = Joint(";
    str += jval.join(",");
    str += ");";
    prog.append(str);
    str.sprintf("Speed %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("Accel %.1f;", data.speed_output);
    prog.append(str);
    str.sprintf("Decel %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("MoveL #ssjpos;");
    prog.append(str);
    str.sprintf("Break;");
    prog.append(str);

    data.program = prog;

    qDebug() << "Prog_p2p_J Success!";
    qDebug() << data.program;

    return true;
}

bool HySasang_Motion::Prog_con_J(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(con))
        return false;

    QStringList prog;
    QString str;

    prog.clear();

    /**
     *  Point #ssjpos = Joint(J1,J2,J3,J4,J5,...);
     *  Speed speed;
     *  //Accel speed;
     *  //Decel speed;
     *  Accuracy accuracy;
     *  MoveL #ssjpos;
     *
    **/

    QStringList jval;
    jval.clear();
    for(int i=0;i<USE_AXIS_NUM;i++)
    {
        str.sprintf("%.2f", data.joint[0].joint[i]);
        jval.append(str);
    }

    str = "Point #ssjpos = Joint(";
    str += jval.join(",");
    str += ");";
    prog.append(str);
    str.sprintf("Speed %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("Accel %.1f;", data.speed_output);
    prog.append(str);
    str.sprintf("Decel %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("Accuracy %.1f;", data.accuracy_input);
    prog.append(str);
    str.sprintf("MoveL #ssjpos;");
    prog.append(str);

    data.program = prog;

    qDebug() << "Prog_con_J Success!";
    qDebug() << data.program;

    return true;
}

bool HySasang_Motion::Prog_cir_J(ST_SSMOTION_DATA& data)
{
    if(data.command != toStr(cir))
        return false;

    QStringList prog;
    QString str;

    prog.clear();

    /**
     *  Point #ssjpos = Joint(J1,J2,J3,J4,J5,...);
     *  Point #ssjpos2 = Joint(J1,J2,J3,J4,J5,...);
     *  Speed speed;
     *  //Accel speed;
     *  //Decel speed;
     *  MoveC #ssjpos,#ssjpos2;
     *  Break;
     *
    **/

    QStringList jval, jval2;
    jval.clear();jval2.clear();
    for(int i=0;i<USE_AXIS_NUM;i++)
    {
        str.sprintf("%.2f", data.joint[0].joint[i]);
        jval.append(str);

        str.sprintf("%.2f", data.joint[1].joint[i]);
        jval2.append(str);
    }

    str = "Point #ssjpos = Joint(";
    str += jval.join(",");
    str += ");";
    prog.append(str);
    str = "Point #ssjpos2 = Joint(";
    str += jval2.join(",");
    str += ");";
    prog.append(str);
    str.sprintf("Speed %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("Accel %.1f;", data.speed_output);
    prog.append(str);
    str.sprintf("Decel %.1f;", data.speed_output);
    prog.append(str);

    str.sprintf("MoveC #ssjpos,#ssjpos2;");
    prog.append(str);
    str.sprintf("Break;");
    prog.append(str);

    data.program = prog;

    qDebug() << "Prog_cir_R Success!";
    qDebug() << data.program;

    return true;
}

/** ================= **/
