#include "dialog_robotdimension.h"
#include "ui_dialog_robotdimension.h"

dialog_robotdimension::dialog_robotdimension(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_robotdimension)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(reject()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtBtnPic.clear();
    m_vtBtnPic.append(ui->btnValuePic);
    m_vtBtnPic.append(ui->btnValuePic_2);
    m_vtBtnPic.append(ui->btnValuePic_3);
    m_vtBtnPic.append(ui->btnValuePic_4);

    m_vtBtnTxt.clear();
    m_vtBtnTxt.append(ui->btnValue);
    m_vtBtnTxt.append(ui->btnValue_2);
    m_vtBtnTxt.append(ui->btnValue_3);
    m_vtBtnTxt.append(ui->btnValue_4);

    int i;
    for(i=0;i<m_vtBtnPic.count();i++)
    {
        connect(m_vtBtnPic[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtBtnTxt[i],SIGNAL(mouse_press()),m_vtBtnPic[i],SLOT(press()));
        connect(m_vtBtnTxt[i],SIGNAL(mouse_release()),m_vtBtnPic[i],SLOT(release()));
    }


}

dialog_robotdimension::~dialog_robotdimension()
{
    delete ui;
}
void dialog_robotdimension::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_robotdimension::showEvent(QShowEvent *)
{
    Update();
}
void dialog_robotdimension::hideEvent(QHideEvent *)
{
}

void dialog_robotdimension::onOK()
{
    Save();
    accept();
}

void dialog_robotdimension::Update()
{
    RobotConfig->Read();

    m_slRobotConfigName.clear();
    // match array & display array
    m_slRobotConfigName.append(QString("CPARAM_H2"));   //use
    m_slRobotConfigName.append(QString("CPARAM_H3"));   //use
    m_slRobotConfigName.append(QString("CPARAM_V3"));   //use
    m_slRobotConfigName.append(QString("CPARAM_V2"));   //use
    m_slRobotConfigName.append(QString("CPARAM_H1"));   //0fix
    m_slRobotConfigName.append(QString("CPARAM_V1"));   //0fix

    int i;
    for(i=0;i<m_slRobotConfigName.count();i++)
        RobotConfig->Get(m_slRobotConfigName[i],m_slValue[i]);

    RobotConfig->Get(QString("ROBOT_NAME"), m_slRobotName);

    for(i=0;i<m_vtBtnTxt.count();i++)
        m_vtBtnTxt[i]->setText(m_slValue[i].at(0));

    ui->lbName->setText(m_slRobotName[0]);
}

void dialog_robotdimension::onSelect()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtBtnPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(QString("%1").arg(index+1));
    np->m_numpad->SetNum(m_vtBtnTxt[index]->text().toDouble());
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(999999);
    np->m_numpad->SetSosuNum(1);

    if(np->exec() == QDialog::Accepted)
    {
        QString str;
        str.sprintf("%.1f",(float)np->m_numpad->GetNumDouble());
        m_vtBtnTxt[index]->setText(str);
    }
}

void dialog_robotdimension::Save()
{
    int i;

    for(i=0;i<m_vtBtnPic.count();i++)
        m_slValue[i][0] = m_vtBtnTxt[i]->text();

    m_slValue[4][0] = QString().setNum(0);   // fix 0
    m_slValue[5][0] = QString().setNum(0);   // fix 0

    for(i=0;i<m_slRobotConfigName.count();i++)
        RobotConfig->Set(m_slRobotConfigName[i],m_slValue[i]);

    RobotConfig->Write();
}
