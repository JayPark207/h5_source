#ifndef DIALOG_MODE_EASY_VIEW_H
#define DIALOG_MODE_EASY_VIEW_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_confirm.h"
#include "dialog_message.h"

namespace Ui {
class dialog_mode_easy_view;
}

class dialog_mode_easy_view : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_mode_easy_view(QWidget *parent = 0);
    ~dialog_mode_easy_view();

    void Init(int index);

    void Update();

    void SelectGrp(int grp);

    void Update(int grp);
    void Redraw_List(int grp, int start_index);

    void Display_BtnGrp(int grp);

private:
    Ui::dialog_mode_easy_view *ui;

    int m_nIndex;
    QString m_strName;

    int m_nSelectedGrp;
    int m_nStartIndex;

    QVector<QLabel4*> m_vtBtnGrpPic;
    QVector<QLabel3*> m_vtBtnGrp;
    QVector<QLabel3*> m_vtLedGrp;

    QVector<QLabel3*> m_vtTbNo;
    QVector<QLabel3*> m_vtTbName;
    QVector<QLabel3*> m_vtTbData;


public slots:
    void onClose();

    void onChangeGrp();

    void onListUp();
    void onListDown();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_MODE_EASY_VIEW_H
