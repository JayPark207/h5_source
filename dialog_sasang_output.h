#ifndef DIALOG_SASANG_OUTPUT_H
#define DIALOG_SASANG_OUTPUT_H

#include <QDialog>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_sasang_output;
}

class dialog_sasang_output : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_sasang_output(QWidget *parent = 0);
    ~dialog_sasang_output();

    void Init(ST_SSMOTION_DATA data);
    void Get(ST_SSMOTION_DATA& data);

    void Update();

private:
    Ui::dialog_sasang_output *ui;

    QTimer* timer;

    QVector<QLabel4*> m_vtOutPic;
    QVector<QLabel3*> m_vtOutIcon;
    QVector<QLabel3*> m_vtOutSel;
    QVector<int>      m_vtOutputNum;

    QVector<QLabel4*> m_vtType;
    QVector<QLabel3*> m_vtTypeIcon;

    ST_SSMOTION_DATA m_DataNew, m_DataOld;

    void Display_Data(ST_SSMOTION_DATA data); // boss

    void MatchingOutput();
    void Display_Output(float output);

    void Display_Type(float type);
    void Display_Time(float time);

    bool IsSame(ST_SSMOTION_DATA old_data, ST_SSMOTION_DATA new_data);
    void Display_SaveEnable();


public slots:
    void onOK();
    void onCancel();

    void onTimer();

    void onOutput();
    void onType();
    void onTime();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SASANG_OUTPUT_H
