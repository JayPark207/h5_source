#include "hynetwork.h"

HyNetwork::HyNetwork(HyRecipe* recipe)
{
    m_Recipe = recipe;

    Init();
}

void HyNetwork::Init()
{
    Init_Code();
    Init_Pop();
    Init_Hys();
    Init_Weight();
    Init_Temp();
    Init_ESen();
    Init_Pop_Protocol();
    Init_Hys_Protocol();
}

void HyNetwork::Init_Code()
{
    Info_Code.clear();

    ST_NW_VARINFO var;

    // COMPANY
    var.name = "$ypcompanycd";
    var.type = CNVAR_STRING;
    Info_Code.append(var);
    // FACTORY
    var.name = "$ypfactorycd";
    var.type = CNVAR_STRING;
    Info_Code.append(var);
    // MACHINE
    var.name = "$ypmachinecd";
    var.type = CNVAR_STRING;
    Info_Code.append(var);
    // ROOM
    var.name = "$yroomcd";
    var.type = CNVAR_STRING;
    Info_Code.append(var);

    // add here...
}

bool HyNetwork::Read(QVector<ST_NW_VARINFO> &info)
{
    QStringList names;
    int i;
    names.clear();
    for(i=0;i<info.count();i++)
        names.append(info[i].name);

    QList<cn_variant> datas;

    if(!m_Recipe->GetVars(names, datas))
    {
        qDebug() << "HyNetwork::Read() GetVars fail!";
        return false;
    }

    for(i=0;i<info.count();i++)
    {
        if(i < datas.size())
            info[i].data = datas[i];
    }

    return true;
}
bool HyNetwork::Write(QVector<ST_NW_VARINFO> &info)
{
    QStringList names;
    QList<cn_variant> datas;
    int i;
    names.clear();
    datas.clear();
    for(i=0;i<info.count();i++)
    {
        names.append(info[i].name);
        info[i].data.type = info[i].type;
        datas.append(info[i].data);
    }

    if(!m_Recipe->SetVars(names, datas))
    {
        qDebug() << "HyNetwork::Write() SetVars fail!";
        return false;
    }

    return true;
}

void HyNetwork::Init_Pop()
{
    Info_Pop.clear();
    Info_PopIp.clear();

    ST_NW_VARINFO var;

    // pop use
    var.name = "ysuse";
    var.type = CNVAR_FLOAT;
    Info_Pop.append(var);
    // pop flag
    var.name = "ysflag";
    var.type = CNVAR_FLOAT;
    Info_Pop.append(var);
    // pop ip
    var.name = "$ysip1";
    var.type = CNVAR_STRING;
    Info_Pop.append(var);
    Info_PopIp.append(var); // add
    // pop port
    var.name = "ysport1";
    var.type = CNVAR_FLOAT;
    Info_Pop.append(var);
    Info_PopIp.append(var); // add
    // pop state
    var.name = "ysstate";
    var.type = CNVAR_FLOAT;
    Info_Pop.append(var);

    // add here...

}

void HyNetwork::Init_Hys()
{
    Info_Hys.clear();
    Info_HysIp.clear();

    ST_NW_VARINFO var;

    // hys use
    var.name = "ycuse";
    var.type = CNVAR_FLOAT;
    Info_Hys.append(var);
    // hys flag
    var.name = "ycflag";
    var.type = CNVAR_FLOAT;
    Info_Hys.append(var);
    // hys ip
    var.name = "$ycip1";
    var.type = CNVAR_STRING;
    Info_Hys.append(var);
    Info_HysIp.append(var); // add
    // hys port
    var.name = "ycport1";
    var.type = CNVAR_FLOAT;
    Info_Hys.append(var);
    Info_HysIp.append(var); // add
    // hys flag
    var.name = "ycstate";
    var.type = CNVAR_FLOAT;
    Info_Hys.append(var);
    // add here...
}

void HyNetwork::Init_Weight()
{
    Info_Weight.clear();

    ST_NW_VARINFO var;

    // use
    var.name = "mdWeight1";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // min
    var.name = "vWeightMin1";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // max
    var.name = "vWeightMax1";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // val
    var.name = "ywmvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);


    // use
    var.name = "mdWeight2";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // min
    var.name = "vWeightMin2";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // max
    var.name = "vWeightMax2";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // val
    var.name = "ywmvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);

    // use
    var.name = "mdWeight3";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // min
    var.name = "vWeightMin3";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // max
    var.name = "vWeightMax3";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // val
    var.name = "ywmvalue[3]";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);

    // use
    var.name = "mdWeight4";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // min
    var.name = "vWeightMin4";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // max
    var.name = "vWeightMax4";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);
    // val
    var.name = "ywmvalue[4]";
    var.type = CNVAR_FLOAT;
    Info_Weight.append(var);

    //add here...
}

void HyNetwork::Init_Temp()
{
    Info_Temp.clear();

    ST_NW_VARINFO var;

    // use
    var.name = "mdTemp1";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // min
    var.name = "vTempMin1";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // max
    var.name = "vTempMax1";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // val
    var.name = "ytmvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);

    // use
    var.name = "mdTemp2";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // min
    var.name = "vTempMin2";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // max
    var.name = "vTempMax2";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // val
    var.name = "ytmvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);

    // use
    var.name = "mdTemp3";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // min
    var.name = "vTempMin3";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // max
    var.name = "vTempMax3";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // val
    var.name = "ytmvalue[3]";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);

    // use
    var.name = "mdTemp4";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // min
    var.name = "vTempMin4";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // max
    var.name = "vTempMax4";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);
    // val
    var.name = "ytmvalue[4]";
    var.type = CNVAR_FLOAT;
    Info_Temp.append(var);

}

void HyNetwork::Init_ESen()
{
    Info_ESen.clear();

    ST_NW_VARINFO var;

    // use
    var.name = "mdESen1";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // min
    var.name = "vESenMin1";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // max
    var.name = "vESenMax1";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // val
    var.name = "yemvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);

    // use
    var.name = "mdESen2";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // min
    var.name = "vESenMin2";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // max
    var.name = "vESenMax2";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // val
    var.name = "yemvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);

    // use
    var.name = "mdESen3";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // min
    var.name = "vESenMin3";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // max
    var.name = "vESenMax3";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // val
    var.name = "yemvalue[3]";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);

    // use
    var.name = "mdESen4";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // min
    var.name = "vESenMin4";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // max
    var.name = "vESenMax4";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);
    // val
    var.name = "yemvalue[4]";
    var.type = CNVAR_FLOAT;
    Info_ESen.append(var);

}

void HyNetwork::Init_Pop_Protocol()
{
    Info_Pop_Protocol.clear();

    ST_NW_VARINFO var;

    var.ui_name = tr("Company Code");
    var.name = "$ypcompanycd";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Factory Code");
    var.name = "$ypfactorycd";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Time");
    var.name = "$dum1";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Error Code");
    var.name = "$yperrorcd";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Mold Number");
    var.name = "$ypmoldnum";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Mold Name");
    var.name = "$ypmoldname";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Target Qty");
    var.name = "vTargetProd";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Cavity Qty");
    var.name = "vCavity";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Product Qty");
    var.name = "vCurrProd";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Run Status");
    var.name = "$ypmsatate";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Machine Code");
    var.name = "$ypmachinecd";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Cycle Time");
    var.name = "tCycleTime";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Takeout Time");
    var.name = "tTOutTime";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("S/W Version");
    var.name = "$ypswver";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("H/W Version");
    var.name = "$yphwver";
    var.type = CNVAR_STRING;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight1 Value");
    var.name = "ywmvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight1 Min");
    var.name = "vWeightMin1";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight1 Max");
    var.name = "vWeightMax1";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight1 Judge");
    var.name = "yw1check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight2 Value");
    var.name = "ywmvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight2 Min");
    var.name = "vWeightMin2";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight2 Max");
    var.name = "vWeightMax2";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight2 Judge");
    var.name = "yw2check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight3 Value");
    var.name = "ywmvalue[3]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight3 Min");
    var.name = "vWeightMin3";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight3 Max");
    var.name = "vWeightMax3";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight3 Judge");
    var.name = "yw3check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight4 Value");
    var.name = "ywmvalue[4]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight4 Min");
    var.name = "vWeightMin4";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight4 Max");
    var.name = "vWeightMax4";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Weight4 Judge");
    var.name = "yw4check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp1 Value");
    var.name = "ytmvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp1 Min");
    var.name = "vTempMin1";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp1 Max");
    var.name = "vTempMax1";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp1 Judge");
    var.name = "yt1check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp2 Value");
    var.name = "ytmvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp2 Min");
    var.name = "vTempMin2";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp2 Max");
    var.name = "vTempMax2";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp2 Judge");
    var.name = "yt2check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp3 Value");
    var.name = "ytmvalue[3]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp3 Min");
    var.name = "vTempMin3";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp3 Max");
    var.name = "vTempMax3";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp3 Judge");
    var.name = "yt3check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);


    var.ui_name = tr("Temp4 Value");
    var.name = "ytmvalue[4]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp4 Min");
    var.name = "vTempMin4";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp4 Max");
    var.name = "vTempMax4";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("Temp4 Judge");
    var.name = "yt4check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);


    var.ui_name = tr("E-Sensor1 Value");
    var.name = "yemvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("E-Sensor1 Min");
    var.name = "vESenMin1";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("E-Sensor1 Max");
    var.name = "vESenMax1";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("E-Sensor1 Judge");
    var.name = "ye1check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);


    var.ui_name = tr("E-Sensor2 Value");
    var.name = "yemvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("E-Sensor2 Min");
    var.name = "vESenMin2";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("E-Sensor2 Max");
    var.name = "vESenMax2";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

    var.ui_name = tr("E-Sensor2 Judge");
    var.name = "ye2check";
    var.type = CNVAR_FLOAT;
    Info_Pop_Protocol.append(var);

}

void HyNetwork::Init_Hys_Protocol()
{
    Info_Hys_Protocol.clear();

    ST_NW_VARINFO var;

    var.ui_name = tr("Company Code");
    var.name = "$ypcompanycd";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Factory Code");
    var.name = "$ypfactorycd";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Room Code");
    var.name = "$yroomcd";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Time");
    var.name = "$dum1";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Number");
    var.name = "$yhyservernum";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Error Code");
    var.name = "$yperrorcd";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Mold Number");
    var.name = "$ypmoldnum";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Mold Name");
    var.name = "$ypmoldname";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Target Qty");
    var.name = "vTargetProd";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Cavity Qty");
    var.name = "vCavity";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Product Qty");
    var.name = "vCurrProd";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Run Status");
    var.name = "$ypmsatate";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Machine Code");
    var.name = "$ypmachinecd";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Cycle Time");
    var.name = "tCycleTime";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Takeout Time");
    var.name = "tTOutTime";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("TP Version");
    var.name = "$ytpver";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("SC Version");
    var.name = "$yscver";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("NAND Version");
    var.name = "$ynandver";
    var.type = CNVAR_STRING;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Weight1 Value");
    var.name = "ywmvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Weight1 Min");
    var.name = "vWeightMin1";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Weight1 Max");
    var.name = "vWeightMax1";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Weight2 Value");
    var.name = "ywmvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Weight2 Min");
    var.name = "vWeightMin2";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Weight2 Max");
    var.name = "vWeightMax2";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);



    var.ui_name = tr("Temp1 Value");
    var.name = "ytmvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp1 Min");
    var.name = "vTempMin1";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp1 Max");
    var.name = "vTempMax1";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp2 Value");
    var.name = "ytmvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp2 Min");
    var.name = "vTempMin2";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp2 Max");
    var.name = "vTempMax2";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp3 Value");
    var.name = "ytmvalue[3]";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp3 Min");
    var.name = "vTempMin3";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp3 Max");
    var.name = "vTempMax3";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp4 Value");
    var.name = "ytmvalue[4]";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp4 Min");
    var.name = "vTempMin4";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("Temp4 Max");
    var.name = "vTempMax4";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("E-Sensor1 Value");
    var.name = "yemvalue[1]";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("E-Sensor1 Min");
    var.name = "vESenMin1";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("E-Sensor1 Max");
    var.name = "vESenMax1";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("E-Sensor2 Value");
    var.name = "yemvalue[2]";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("E-Sensor2 Min");
    var.name = "vESenMin2";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    var.ui_name = tr("E-Sensor2 Max");
    var.name = "vESenMax2";
    var.type = CNVAR_FLOAT;
    Info_Hys_Protocol.append(var);

    // add here...
}

bool HyNetwork::Set_PopUse(float data)
{
    QString name = "ysuse";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_PopUse(float& data)
{
    QString name = "ysuse";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}

bool HyNetwork::Set_PopFlag(float data)
{
    QString name = "ysflag";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_PopFlag(float& data)
{
    QString name = "ysflag";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}
bool HyNetwork::Set_PopTrig(float data)
{
    QString name = "ystrig";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_PopTrig(float& data)
{
    QString name = "ystrig";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}
bool HyNetwork::Set_PopState(float data)
{
    QString name = "ysstate";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_PopState(float& data)
{
    QString name = "ysstate";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}

bool HyNetwork::Get_Pop(float& use, float &flag, float &trig, float &state)
{
    QStringList name;
    name.append("ysuse");
    name.append("ysflag");
    name.append("ystrig");
    name.append("ysstate");

    QList<cn_variant> data;

    if(!m_Recipe->GetVars(name, data))
        return false;

    if(data[0].type >= 0)
        use = data[0].val.f32;

    if(data[1].type >= 0)
        flag = data[1].val.f32;

    if(data[2].type >= 0)
        trig = data[2].val.f32;

    if(data[3].type >= 0)
        state = data[3].val.f32;

    return true;
}


bool HyNetwork::Set_HysUse(float data)
{
    QString name = "ycuse";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_HysUse(float& data)
{
    QString name = "ycuse";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}
bool HyNetwork::Set_HysFlag(float data)
{
    QString name = "ycflag";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_HysFlag(float& data)
{
    QString name = "ycflag";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}
bool HyNetwork::Set_HysTrig(float data)
{
    QString name = "yctrig";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_HysTrig(float& data)
{
    QString name = "yctrig";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}
bool HyNetwork::Set_HysState(float data)
{
    QString name = "ycstate";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_HysState(float& data)
{
    QString name = "ycstate";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}

bool HyNetwork::Get_Hys(float& use, float &flag, float &trig, float &state)
{
    QStringList name;
    name.append("ycuse");
    name.append("ycflag");
    name.append("yctrig");
    name.append("ycstate");

    QList<cn_variant> data;

    if(!m_Recipe->GetVars(name, data))
        return false;

    if(data[0].type >= 0)
        use = data[0].val.f32;

    if(data[1].type >= 0)
        flag = data[1].val.f32;

    if(data[2].type >= 0)
        trig = data[2].val.f32;

    if(data[3].type >= 0)
        state = data[3].val.f32;

    return true;
}


bool HyNetwork::Set_SerialFlag(float data)
{
    QString name = "yserialflag";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_SerialFlag(float& data)
{
    QString name = "yserialflag";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}

bool HyNetwork::Set_SerialCall(float data)
{
    QString name = "yscallflag";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_SerialCall(float& data)
{
    QString name = "yscallflag";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}


bool HyNetwork::Get_Serial(float &flag, float &call, float &cnterr, float &cntcall)
{
    QStringList name;
    name.append("yserialflag");
    name.append("yscallflag");
    name.append("yccountflag");
    name.append("ycdflag");

    QList<cn_variant> data;

    if(!m_Recipe->GetVars(name, data))
        return false;

    if(data[0].type >= 0)
        flag = data[0].val.f32;

    if(data[1].type >= 0)
        call = data[1].val.f32;

    if(data[2].type >= 0)
        cnterr = data[2].val.f32;

    if(data[3].type >= 0)
        cntcall = data[3].val.f32;

    return true;
}

bool HyNetwork::Set_CountError(float data)
{
    QString name = "yccountflag";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_CountError(float& data)
{
    QString name = "yccountflag";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}

bool HyNetwork::Set_CountCall(float data)
{
    QString name = "ycdflag";

    if(!m_Recipe->Set(name, data))
        return false;

    return true;
}
bool HyNetwork::Get_CountCall(float& data)
{
    QString name = "ycdflag";
    float result;

    if(!m_Recipe->Get(name, result))
        return false;

    data = result;
    return true;
}


bool HyNetwork::Set_IP(QString ip)
{
    QStringList ip_list;

    ip_list = ip.split(".");
    if(ip_list.size() != 4)
        return false;

    ip_list[3] = "1";
    QString gateway;
    gateway = ip_list.join(".");
    qDebug() << "ip:"<<ip;
    qDebug() << "gw:" <<gateway;

    return Set_IP(ip, gateway);
}

bool HyNetwork::Set_IP(QString ip, QString gateway)
{
    CNRobo* pCon = CNRobo::getInstance();

    ip = ip.trimmed();
    gateway = gateway.trimmed();

    QString ip_line, gw_line;

    ip_line.clear();
    ip_line  = "ifconfig ";
    ip_line += ETHERNET_NAME;
    ip_line += " ";
    ip_line += ip;
    ip_line += " up";
    qDebug() << ip_line;

    gw_line.clear();
    gw_line  = "route add -net 0.0.0.0 gw ";
    gw_line += gateway;
    gw_line += " ";
    gw_line += ETHERNET_NAME;
    qDebug() << gw_line;

    QString path = "/ecat/dm9000-up";
    QStringList line;

    line.clear();
    int ret = pCon->getProgramFile(path, line);
    if(ret < 0)
    {
        qDebug() << "HyNetwork::Set_IP() getProgramFile fail ret=" << ret;
        return false;
    }

    int index;
    QString temp;
    temp  = "ifconfig ";
    temp += ETHERNET_NAME;

    for(int i=0;i<line.count();i++)
    {
        index = line[i].indexOf(temp);
        if(index >= 0)
            line[i] = ip_line;

        index = line[i].indexOf("route add");
        if(index >= 0)
            line[i] = gw_line;
    }

    qDebug() << line;

    ret = pCon->createProgramFile(path, line);
    if(ret < 0)
    {
        qDebug() << "HyNetwork::Set_IP() createProgramFile fail ret=" << ret;
        return false;
    }

    return true;
}

bool HyNetwork::Get_IP(QString &ip, QString &gateway)
{
    CNRobo* pCon = CNRobo::getInstance();

    QString path = "/ecat/dm9000-up";
    QStringList line;

    line.clear();
    int ret = pCon->getProgramFile(path, line);
    if(ret < 0)
    {
        qDebug() << "HyNetwork::Get_IP() fail ret=" << ret;
        return false;
    }

    QString temp, raw_ip, raw_gw;
    int index;
    QString str;
    str  = "ifconfig ";
    str += ETHERNET_NAME;

    raw_ip.clear();raw_gw.clear();
    for(int i=0;i<line.count();i++)
    {
        temp = line[i];
        index = temp.indexOf(str);
        if(index >= 0)
            raw_ip = temp;

        index = temp.indexOf("route add");
        if(index >= 0)
            raw_gw = temp;
    }

    if(raw_ip.isEmpty() || raw_gw.isEmpty())
        return false;

    // ip
    raw_ip.remove("ifconfig");
    raw_ip.remove(ETHERNET_NAME);
    raw_ip.remove("up");
    raw_ip = raw_ip.trimmed();
    qDebug() << "ip:"<<raw_ip;

    // gw
    index = raw_gw.indexOf("gw");
    if(index < 0)
        raw_gw.clear();
    else
    {
        raw_gw = raw_gw.mid(index);
        raw_gw.remove("gw");
        raw_gw.remove(ETHERNET_NAME);
        raw_gw = raw_gw.trimmed();
    }
    qDebug() << "gw:" <<raw_gw;

    ip = raw_ip;
    gateway = raw_gw;
    return true;
}


