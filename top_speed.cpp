#include "top_speed.h"
#include "ui_top_speed.h"


top_speed::top_speed(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::top_speed)
{
    ui->setupUi(this);

    m_vtPic.clear();
    m_vtPic.append(ui->btn1Pic);
    m_vtPic.append(ui->btn5Pic);
    m_vtPic.append(ui->btn10Pic);

    m_vtText.clear();
    m_vtText.append(ui->btn1);
    m_vtText.append(ui->btn5);
    m_vtText.append(ui->btn10);

    int i;
    for(i=0;i<m_vtPic.count();i++)
    {
        connect(m_vtPic[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtText[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtText[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
    }

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

}

top_speed::~top_speed()
{
    delete ui;
}
void top_speed::showEvent(QShowEvent *)
{
    Update();
}
void top_speed::hideEvent(QHideEvent *)
{
}
void top_speed::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void top_speed::Update()
{
    m_nSpeed = Param->Get(HyParam::MAIN_SPEED).toInt();
    ui->pbSpeed->setValue(m_nSpeed);

    SetJumpNum(1); // default 5
}

void top_speed::onOK()
{
    // save. & Set Override Speed.
    Param->Set(HyParam::MAIN_SPEED, m_nSpeed);
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setSpeed((float)m_nSpeed);

    gSetTopPage(TOP_MAIN);
}
void top_speed::onCancel()
{
    gSetTopPage(TOP_MAIN);
}

void top_speed::SetJumpNum(int index)
{
    QImage img;

    img.load(":/image/image/button_rect2_0.png");

    for(int i=0;i<m_vtPic.count();i++)
        m_vtPic[i]->setPixmap(QPixmap::fromImage(img));

    img.load(":/image/image/button_rect2_4.png");
    m_vtPic[index]->setPixmap(QPixmap::fromImage(img));

    m_nJumpValue = m_vtText[index]->text().toInt();
}

void top_speed::onSelect()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtPic.indexOf(btn);
    if(index < 0) return;

    SetJumpNum(index);
}

void top_speed::onUp()
{
    m_nSpeed += m_nJumpValue;
    if(m_nSpeed > MAX_MAIN_SPEED)
        m_nSpeed = MAX_MAIN_SPEED;

    ui->pbSpeed->setValue(m_nSpeed);
}

void top_speed::onDown()
{
    m_nSpeed -= m_nJumpValue;
    if(m_nSpeed < MIN_MAIN_SPEED)
        m_nSpeed = MIN_MAIN_SPEED;

    ui->pbSpeed->setValue(m_nSpeed);
}
