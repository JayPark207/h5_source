#ifndef PAGE_MAIN2_MONITOR_H
#define PAGE_MAIN2_MONITOR_H

#include <QWidget>

#include "global.h"

#include "dialog_motor.h"
#include "dialog_io.h"
#include "dialog_view_dmcvar.h"
#include "dialog_euromap.h"

namespace Ui {
class page_main2_monitor;
}

class page_main2_monitor : public QWidget
{
    Q_OBJECT
    
public:
    explicit page_main2_monitor(QWidget *parent = 0);
    ~page_main2_monitor();

private slots:

    void onIO();
    void onSData();
    void onMotor();
    void onEuromap();
    
private:
    Ui::page_main2_monitor *ui;

    dialog_motor* dig_motor;
    dialog_io* dig_io;
    dialog_view_dmcvar* dig_view_dmcvar;
    dialog_euromap* dig_euromap;

    void UserLevel();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // PAGE_MAIN2_MONITOR_H
