#include "hyui_longpress.h"

HyUi_LongPress::HyUi_LongPress()
{
    m_nLongPressTime = 2000;
    m_nSignalIntervalTime = 100;

    timer = new QTimer(this);
    timer->setInterval(10);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    m_bPressed = false;
}

void HyUi_LongPress::Init(int long_press_time_ms, int signal_interval_ms)
{
    m_nLongPressTime = long_press_time_ms;
    m_nSignalIntervalTime = signal_interval_ms;

    m_bPressed = false;
}

void HyUi_LongPress::onTimer()
{
    // check over long press time.
    if(!m_bPressed)
        return;

    if(!watch.hasExpired(m_nLongPressTime))
        return;

    // trigger siganl on interval time.
    if(interval.hasExpired(m_nSignalIntervalTime))
    {
        interval.start();
        emit sigTrigger();
    }
}

void HyUi_LongPress::onPressed()
{
    watch.start();
    interval.start();
    m_bPressed = true;
    timer->start();
}
void HyUi_LongPress::onReleased()
{
    m_bPressed = false;
    timer->stop();
}
