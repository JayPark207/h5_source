#ifndef WIDGET_JOGMODULE_H
#define WIDGET_JOGMODULE_H

#include <QWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

namespace Ui {
class widget_jogmodule;
}

class widget_jogmodule : public QWidget
{
    Q_OBJECT

public:
    explicit widget_jogmodule(QWidget *parent = 0);
    ~widget_jogmodule();

public slots:
    void onJogType();
    void onSpeedUp();
    void onSpeedDown();
    void onJogInch();

    void onTimer();

private:
    Ui::widget_jogmodule *ui;

    QVector<QLabel3*> m_vtInch;
    QVector<QLabel4*> m_vtInchPic;

    int m_nJogSpeed;

    void SetJogInch(bool on);
    void SetInch(int i, bool on);

    QTimer* timer;
    void SetSpeed(int speed);
    void GetSpeed(int& speed);


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // WIDGET_JOGMODULE_H
