#ifndef HYSASANG_GROUP_H
#define HYSASANG_GROUP_H

#include <QObject>
#include <QDateTime>
#include <QDebug>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

struct ST_SSGROUP_DATA
{
    QString name;
    QDateTime datetime;

    //QStringList data;       // data contents
    QList<QStringList> data;
    //QStringList program;    // program contents
    QList< QList<QStringList> > program;

};

class HySasang_Group : public QObject
{
    Q_OBJECT

/** variables **/
public:
    QList<ST_SSGROUP_DATA> GroupList;
    QList<ST_SSGROUP_DATA> GroupList_Comp;
    QStringList GroupNameList;

private:



/** functions **/
public:
    HySasang_Group();

    // control database file.
    int ReadAll(); // boss (need to sorting! refer to time)
    int ReadList(QStringList& list);    // no sorting.
    int ReadFile(ST_SSGROUP_DATA& data);
    int SaveFile(ST_SSGROUP_DATA data);
    bool IsExistFile(QString name);
    int Sorting(QList<ST_SSGROUP_DATA>& group_list);

    bool Format2Data(QStringList format, ST_SSGROUP_DATA& data);
    bool Data2Format(ST_SSGROUP_DATA data, QStringList& format);
    bool MakingOneProgram(ST_SSGROUP_DATA data, QStringList& one_program);

    // edit.
    bool Default(ST_SSGROUP_DATA& data);
    bool New(ST_SSGROUP_DATA& data);
    bool Copy(int source_index, QString name);
    bool Del(int index);
    bool Rename(int index, QString name); // change folder & filename (backup -> delete -> make)
    bool IsExist(QString name);
    bool IsAbleName(QString name);

    // compare
    bool Update_CompData(); // new, copy, del if success => use this function.
    bool Reset_CompData();
    bool IsEdit();
    bool IsSame(ST_SSGROUP_DATA data1, ST_SSGROUP_DATA data2);


private:
    static bool CompareDateTime(const ST_SSGROUP_DATA& data1, const ST_SSGROUP_DATA& data2);

};

#endif // HYSASANG_GROUP_H
