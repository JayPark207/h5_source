#ifndef QLABEL4
#define QLABEL4

#include <QLabel>
#include <QMouseEvent>
#include <QVector>
#include <QDebug>

#include "global.h"

/** 버튼이미지+아이콘 형태로 구성한 버튼용
    단, connect시에 텍스트와 아이콘은 전부 Pic에 시그날전달로 해야한다.
    그리고 Pic만 해당클래스로 Promote 한다.**/

class QLabel4 : public QLabel
{
    Q_OBJECT
public:
    explicit QLabel4(QWidget *parent=0):QLabel(parent){}

signals:
    void mouse_press();
    void mouse_release();

public slots:
    void press() { mousePressEvent(0); }
    void release() { mouseReleaseEvent(0); }

protected:
    void mousePressEvent(QMouseEvent *)
    {
        gBeep();

        g_nPxmIndex = g_vtImgNoPress.indexOf(this->pixmap()->toImage());
        if(g_nPxmIndex >= 0)
            this->setPixmap(g_vtPxmPress[g_nPxmIndex]);
        else
            g_nPxmIndex = g_vtImgPress.indexOf(this->pixmap()->toImage());

        emit mouse_press();
    }

    void mouseReleaseEvent(QMouseEvent *event)
    {
        if(g_nPxmIndex >= 0)
            this->setPixmap(g_vtPxmNoPress[g_nPxmIndex]);

        if(gLabelWidget != 0)
        {
            //qDebug() << "click signal run Try?"<< gLabelWidget->objectName();

            int dx = abs(this->x() - gLabelWidget->x());
            int dy = abs(this->y() - gLabelWidget->y());

            QPoint mouse_point = gLabelReleasePoint;
            mouse_point.setX(mouse_point.x() + dx);
            mouse_point.setY(mouse_point.y() + dy);

            if(mouse_point.x() >= 0 && mouse_point.x() <= this->width()
            && mouse_point.y() >= 0 && mouse_point.y() <= this->height())
            {
                //qDebug() << "click signal run OK!";
                emit mouse_release();
            }
        }
        else
        {
            //qDebug() << "click QLabel4 Try?" << this->objectName();

            if(event == 0) // exception case
                return;

            if(event->x() >= 0 && event->x() <= this->width()
            && event->y() >= 0 && event->y() <= this->height())
            {
                //qDebug() << "click QLabel4 OK!";
                emit mouse_release();
            }
        }

        gLabelWidget = 0;

    }

private:


};

#endif // QLABEL4

