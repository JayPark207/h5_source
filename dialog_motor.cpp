#include "dialog_motor.h"
#include "ui_dialog_motor.h"

dialog_motor::dialog_motor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_motor)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    // List
    m_vtID.clear();m_vtModel.clear();
    m_vtID.append(ui->lbID);  m_vtModel.append(ui->lbModel);
    m_vtID.append(ui->lbID_2);m_vtModel.append(ui->lbModel_2);
    m_vtID.append(ui->lbID_3);m_vtModel.append(ui->lbModel_3);
    m_vtID.append(ui->lbID_4);m_vtModel.append(ui->lbModel_4);
    m_vtID.append(ui->lbID_5);m_vtModel.append(ui->lbModel_5);
    m_vtID.append(ui->lbID_6);m_vtModel.append(ui->lbModel_6);
    m_vtID.append(ui->lbID_7);m_vtModel.append(ui->lbModel_7);
    m_vtID.append(ui->lbID_8);m_vtModel.append(ui->lbModel_8);

    m_vtP.clear();m_vtS.clear();m_vtL.clear();
    m_vtP.append(ui->lbPower);  m_vtS.append(ui->lbServo);  m_vtL.append(ui->lbLimit);
    m_vtP.append(ui->lbPower_2);m_vtS.append(ui->lbServo_2);m_vtL.append(ui->lbLimit_2);
    m_vtP.append(ui->lbPower_3);m_vtS.append(ui->lbServo_3);m_vtL.append(ui->lbLimit_3);
    m_vtP.append(ui->lbPower_4);m_vtS.append(ui->lbServo_4);m_vtL.append(ui->lbLimit_4);
    m_vtP.append(ui->lbPower_5);m_vtS.append(ui->lbServo_5);m_vtL.append(ui->lbLimit_5);
    m_vtP.append(ui->lbPower_6);m_vtS.append(ui->lbServo_6);m_vtL.append(ui->lbLimit_6);
    m_vtP.append(ui->lbPower_7);m_vtS.append(ui->lbServo_7);m_vtL.append(ui->lbLimit_7);
    m_vtP.append(ui->lbPower_8);m_vtS.append(ui->lbServo_8);m_vtL.append(ui->lbLimit_8);

    m_vtErr.clear();m_vtRc.clear();
    m_vtErr.append(ui->lbError);  m_vtRc.append(ui->lbRealcount);  m_vtTq.append(ui->lbTorque);
    m_vtErr.append(ui->lbError_2);m_vtRc.append(ui->lbRealcount_2);m_vtTq.append(ui->lbTorque_2);
    m_vtErr.append(ui->lbError_3);m_vtRc.append(ui->lbRealcount_3);m_vtTq.append(ui->lbTorque_3);
    m_vtErr.append(ui->lbError_4);m_vtRc.append(ui->lbRealcount_4);m_vtTq.append(ui->lbTorque_4);
    m_vtErr.append(ui->lbError_5);m_vtRc.append(ui->lbRealcount_5);m_vtTq.append(ui->lbTorque_5);
    m_vtErr.append(ui->lbError_6);m_vtRc.append(ui->lbRealcount_6);m_vtTq.append(ui->lbTorque_6);
    m_vtErr.append(ui->lbError_7);m_vtRc.append(ui->lbRealcount_7);m_vtTq.append(ui->lbTorque_7);
    m_vtErr.append(ui->lbError_8);m_vtRc.append(ui->lbRealcount_8);m_vtTq.append(ui->lbTorque_8);

    m_vtLevel.clear();m_vtLevelPic.clear();
    m_vtLevel.append(ui->lbLevel);  m_vtLevelPic.append(ui->btnLevel);
    m_vtLevel.append(ui->lbLevel_2);m_vtLevelPic.append(ui->btnLevel_2);
    m_vtLevel.append(ui->lbLevel_3);m_vtLevelPic.append(ui->btnLevel_3);
    m_vtLevel.append(ui->lbLevel_4);m_vtLevelPic.append(ui->btnLevel_4);
    m_vtLevel.append(ui->lbLevel_5);m_vtLevelPic.append(ui->btnLevel_5);
    m_vtLevel.append(ui->lbLevel_6);m_vtLevelPic.append(ui->btnLevel_6);
    m_vtLevel.append(ui->lbLevel_7);m_vtLevelPic.append(ui->btnLevel_7);
    m_vtLevel.append(ui->lbLevel_8);m_vtLevelPic.append(ui->btnLevel_8);

    int i;
    for(i=0;i<m_vtID.count();i++)
    {
        connect(m_vtID[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtModel[i],SIGNAL(mouse_release()),m_vtID[i],SIGNAL(mouse_release()));
        connect(m_vtP[i],SIGNAL(mouse_release()),m_vtID[i],SIGNAL(mouse_release()));
        connect(m_vtS[i],SIGNAL(mouse_release()),m_vtID[i],SIGNAL(mouse_release()));
        connect(m_vtL[i],SIGNAL(mouse_release()),m_vtID[i],SIGNAL(mouse_release()));
        connect(m_vtErr[i],SIGNAL(mouse_release()),m_vtID[i],SIGNAL(mouse_release()));
        connect(m_vtRc[i],SIGNAL(mouse_release()),m_vtID[i],SIGNAL(mouse_release()));
    }

    for(i=0;i<m_vtLevel.count();i++)
    {
        connect(m_vtLevelPic[i],SIGNAL(mouse_release()),this,SLOT(onLevel()));
        connect(m_vtLevel[i],SIGNAL(mouse_press()),m_vtLevelPic[i],SLOT(press()));
        connect(m_vtLevel[i],SIGNAL(mouse_release()),m_vtLevelPic[i],SLOT(release()));
    }

    connect(ui->btnSetPic,SIGNAL(mouse_release()),this,SLOT(onSet()));
    connect(ui->btnSet,SIGNAL(mouse_press()),ui->btnSetPic,SLOT(press()));
    connect(ui->btnSet,SIGNAL(mouse_release()),ui->btnSetPic,SLOT(release()));
    connect(ui->btnSetIcon,SIGNAL(mouse_press()),ui->btnSetPic,SLOT(press()));
    connect(ui->btnSetIcon,SIGNAL(mouse_release()),ui->btnSetPic,SLOT(release()));
    connect(ui->btnSetIcon_2,SIGNAL(mouse_press()),ui->btnSetPic,SLOT(press()));
    connect(ui->btnSetIcon_2,SIGNAL(mouse_release()),ui->btnSetPic,SLOT(release()));


    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));
    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));


    // checkbox
    connect(ui->lbBox,SIGNAL(mouse_release()),this,SLOT(onCheckbox()));
    connect(ui->lbCheck,SIGNAL(mouse_release()),this,SLOT(onCheckbox()));
    connect(ui->lbCBText,SIGNAL(mouse_release()),this,SLOT(onCheckbox()));

    connect(ui->btnServoPic,SIGNAL(mouse_release()),this,SLOT(onServo()));
    connect(ui->btnServo,SIGNAL(mouse_press()),ui->btnServoPic,SLOT(press()));
    connect(ui->btnServo,SIGNAL(mouse_release()),ui->btnServoPic,SLOT(release()));
    connect(ui->btnServoIcon,SIGNAL(mouse_press()),ui->btnServoPic,SLOT(press()));
    connect(ui->btnServoIcon,SIGNAL(mouse_release()),ui->btnServoPic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnReset,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnReset,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnEcatResetPic,SIGNAL(mouse_release()),this,SLOT(onEcatReset()));
    connect(ui->btnEcatReset,SIGNAL(mouse_press()),ui->btnEcatResetPic,SLOT(press()));
    connect(ui->btnEcatReset,SIGNAL(mouse_release()),ui->btnEcatResetPic,SLOT(release()));
    connect(ui->btnEcatResetIcon,SIGNAL(mouse_press()),ui->btnEcatResetPic,SLOT(press()));
    connect(ui->btnEcatResetIcon,SIGNAL(mouse_release()),ui->btnEcatResetPic,SLOT(release()));


    Init_Color();

    top=0;
}

dialog_motor::~dialog_motor()
{
    delete ui;
}
void dialog_motor::showEvent(QShowEvent *)
{
    Update();

    timer->start();

    //gReadyJog(true);

    SubTop();

    m_nUserLevel = Param->Get(HyParam::USER_LEVEL).toInt();

    // control set button.
    Display_BtnSet(false);
    if(m_nUserLevel < USER_LEVEL_HIGH)
        ui->wigSet->setEnabled(false);
    else
        ui->wigSet->setEnabled(true);

}
void dialog_motor::hideEvent(QHideEvent *)
{
    timer->stop();

    if(ui->wigSet->isEnabled())
        Comp_Save_EEPROM();

    //gReadyJog(false);
}
void dialog_motor::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_motor::on_btnTest1_clicked()
{
    Motors->Debug_StatusWord();
}

void dialog_motor::Update()
{
    Read_MotorData();

    m_nStartIndex = 0;
    Redraw_List(m_nStartIndex);

    SetCheckbox(false);
    EnableControl(false);

    Read_OverloadLevel_1st(); // eeprom save -> read -> return false.
    Read_OverloadLevel_1st(); // 2-times is ok.
    ui->wigLevel->setEnabled(false);

}

void dialog_motor::Read_MotorData()
{
    QVector<CNR_SLAVE_TYPE> slave_type;
    QStringList slave_model;

    CNRobo* pCon = CNRobo::getInstance();

    pCon->getSlaveType(slave_type);
    pCon->getSlaveModel(slave_model);

    qDebug() << slave_type.size();
    for(int i=0;i<slave_type.count();i++)
        qDebug() << "slave_type=" << slave_type[i];

    for(int i=0;i<slave_model.count();i++)
        qDebug() << "slave_model=" << slave_model[i];

    m_MotorID.clear();
    m_MotorModel.clear();

    for(int i=0;i<slave_type.count();i++)
    {
        if(slave_type[i] == CNR_SLAVE_DRIVER)
        {
            m_MotorID.append(QString().setNum(i));
            m_MotorModel.append(slave_model[i]);
        }
    }

    // list up/down button control.
    if(m_MotorID.size() <= m_vtID.size())
        ui->grpUpDown->setEnabled(false);
    else
        ui->grpUpDown->setEnabled(true);

}

void dialog_motor::Redraw_List(int start_index)
{
    if(start_index < 0) return;

    int i,index;
    for(i=0;i<m_vtID.count();i++)
    {
        index = start_index + i;

        if(index < m_MotorID.size())
        {
            // fill data.
            m_vtID[i]->setText(m_MotorID[index]);
            m_vtModel[i]->setText(m_MotorModel[index]);
            m_vtP[i]->setText(QString("P"));
            m_vtS[i]->setText(QString("S"));
            m_vtL[i]->setText(QString("L"));
        }
        else
        {
            // fill blank.
            m_vtID[i]->setText(QString(""));
            m_vtModel[i]->setText(QString(""));
            m_vtP[i]->setText(QString(""));
            m_vtS[i]->setText(QString(""));
            m_vtL[i]->setText(QString(""));
            m_vtErr[i]->setText(QString(""));
            m_vtRc[i]->setText(QString(""));
            m_vtTq[i]->setText(QString(""));
            m_vtLevel[i]->setText(QString(""));
        }
    }
}


void dialog_motor::SetCheckbox(bool on)
{
    if(on)
        ui->lbCheck->show();
    else
        ui->lbCheck->hide();
}

void dialog_motor::EnableControl(bool enable)
{
    ui->grpControl->setEnabled(enable);
}

void dialog_motor::onCheckbox()
{
    if(ui->lbCheck->isHidden())
    {
        // on
        SetCheckbox(true);
        EnableControl(true);
        Display_SelectedLine(0);
    }
    else
    {
        //off
        SetCheckbox(false);
        EnableControl(false);
        Display_SelectedLine(-1);
    }
}

void dialog_motor::onListUp()
{
    if(m_nStartIndex <= 0) return;

    Redraw_List(--m_nStartIndex);
}
void dialog_motor::onListDown()
{
    if((m_nStartIndex + m_vtID.size()) >= m_MotorID.size())
        return;

    Redraw_List(++m_nStartIndex);
}

void dialog_motor::onTimer()
{
    if(!ui->wigLevel->isEnabled())
        Motors->Update();

    Update_Status();
    Display_BtnSet(ui->wigLevel->isEnabled());
}

void dialog_motor::Update_Status()
{
    int id;
    for(int i=0;i<m_vtID.count();i++)
    {
        if(m_vtID[i]->text() != QString(""))
        {
            id = m_vtID[i]->text().toInt();

            Display_Power(i, Motors->IsPower(id));
            Display_Servo(i, Motors->IsServo(id));
            Display_Limit(i, Motors->IsLimit(id));
            Display_Error(i, Motors->GetErrorCode(id));
            //Display_Realcount(i, Motors->GetRealCount(id));
            Display_OverloadRate(i, Motors->GetOverloadRate(id));
            Display_Torque(i, Motors->GetTorqueDemand(id), Motors->GetTorqueActual(id));
            Display_OverloadLevel(i, m_OverloadLevel[id]);
        }
    }
}

void dialog_motor::Init_Color()
{
    Pon.setBrush(QPalette::Window,QBrush(QColor(0,247,250)));
    Poff.setBrush(QPalette::Window,QBrush(QColor(190,190,190)));

    Son.setBrush(QPalette::Window,QBrush(QColor(88,250,0)));
    Soff.setBrush(QPalette::Window,QBrush(QColor(190,190,190)));

    Lon.setBrush(QPalette::Window,QBrush(QColor(255,255,0)));
    Loff.setBrush(QPalette::Window,QBrush(QColor(190,190,190)));

    Eon.setBrush(QPalette::Window,QBrush(QColor(255,0,0)));
    Won.setBrush(QPalette::Window,QBrush(QColor(255,255,0)));
    Eoff.setBrush(QPalette::Window,QBrush(QColor(255,255,255)));
}

void dialog_motor::Display_Power(int line, bool on)
{
    if(on)
        m_vtP[line]->setPalette(Pon);
    else
        m_vtP[line]->setPalette(Poff);
}
void dialog_motor::Display_Servo(int line, bool on)
{
    if(on)
        m_vtS[line]->setPalette(Son);
    else
        m_vtS[line]->setPalette(Soff);
}
void dialog_motor::Display_Limit(int line, bool on)
{
    if(on)
        m_vtL[line]->setPalette(Lon);
    else
        m_vtL[line]->setPalette(Loff);
}
void dialog_motor::Display_Error(int line, USHORT code, bool warn)
{

    if(code != 0x0000)
    {
        if(warn)
            m_vtErr[line]->setPalette(Won);
        else
            m_vtErr[line]->setPalette(Eon);
    }
    else
        m_vtErr[line]->setPalette(Eoff);

    QString str;
    if((code&0x00FF) < 0xA0)
        str.sprintf("%d", (code & 0x00FF));
    else
        str.sprintf("%X", (code & 0x00FF));

    m_vtErr[line]->setText(str);
}

void dialog_motor::Display_Realcount(int line, int count)
{
    m_vtRc[line]->setText(QString().setNum(count));
}
void dialog_motor::Display_OverloadRate(int line, USHORT rate)
{
    float _rate = (float)rate / 10.0;

    QString str;
    str.clear();
    str.sprintf("%.01f", _rate);
    str += "%";

    m_vtRc[line]->setText(str);
}

void dialog_motor::Display_Torque(int line, SHORT demand, SHORT actual)
{
    float _demand = (float)demand / 10.0;
    float _actual = (float)actual / 10.0;

    QString str,str2;
    str.clear();
    str.sprintf("%.01f", _demand);
    str += "%";
    str += "\n";
    str2.sprintf("%.01f", _actual);
    str += str2;
    str += "%";

    m_vtTq[line]->setText(str);
}

void dialog_motor::onServo()
{
    int id = ui->lbSelID->text().toInt();

    if(Motors->IsServo(id))
        Motors->Servo(id, false);
    else
        Motors->Servo(id, true);
}
void dialog_motor::onReset()
{
    int id = ui->lbSelID->text().toInt();
    Motors->Reset(id);
}

void dialog_motor::onSelect()
{
    if(ui->lbCheck->isHidden())
        return;

    QLabel3* sel = (QLabel3*)sender();
    int line = m_vtID.indexOf(sel);
    if(line < 0) return;
    if(sel->text() == QString("")) return;

    Display_SelectedLine(line);
}

// line = -1 is all no select.
void dialog_motor::Display_SelectedLine(int line)
{
    QPalette on,off;
    on.setBrush(QPalette::Window,QBrush(QColor(255,73,211)));
    off.setBrush(QPalette::Window,QBrush(QColor(255,255,255)));

    for(int i=0;i<m_vtID.count();i++)
    {
        if(i == line)
        {
            m_vtID[line]->setPalette(on);
            ui->lbSelID->setText(m_vtID[line]->text());
        }
        else
            m_vtID[i]->setPalette(off);
    }

    if(line < 0)
        ui->lbSelID->setText(QString(""));
}

void dialog_motor::onEcatReset()
{
    dialog_delaying* dig = new dialog_delaying();
    CNRobo* pCon = CNRobo::getInstance();
    pCon->resetEcatError();
    dig->Init(500);
    dig->exec();
    pCon->resetError();
    pCon->clearWarningCode();
}


void dialog_motor::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}


void dialog_motor::onLevel()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtLevelPic.indexOf(btn);
    QString strID = m_vtID[index]->text();
    if(strID.isEmpty()) return;

    int id = strID.toInt();

    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->lbHead6->text() + " ID:" + strID);
    np->m_numpad->SetNum((double)m_OverloadLevel[id]);
    np->m_numpad->SetMinValue(MT_PANASONIC_OVERLOAD_LEVEL_MIN);
    np->m_numpad->SetMaxValue(MT_PANASONIC_OVERLOAD_LEVEL_MAX);
    np->m_numpad->SetSosuNum(0);
    if(np->exec() == QDialog::Accepted)
    {
        int level = (int)np->m_numpad->GetNumDouble();
        if(!Motors->Set_OverloadLevel(id, level))
        {
            // message
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(tr("Error"));
            msg->Message(tr("Fail to set overload level!"), "ID : " + strID);
            msg->exec();
            return;
        }
        Read_OverloadLevel(id);
    }

}

void dialog_motor::onSet()
{
    if(m_nUserLevel < USER_LEVEL_HIGH)
        return;

    if(ui->wigLevel->isEnabled())
        ui->wigLevel->setEnabled(false);
    else
        ui->wigLevel->setEnabled(true);
}

// this is placed after Read_MotorData(). sync m_MotorID
void dialog_motor::Read_OverloadLevel_1st()
{
    m_OverloadLevel_1st.clear();
    m_OverloadLevel.clear();

    int _data=0;
    for(int i=0;i<m_MotorID.count();i++)
    {
        if(Motors->Get_OverloadLevel(i, _data))
        {
            m_OverloadLevel_1st.append(_data);
            m_OverloadLevel.append(_data);
        }
        else
        {
            m_OverloadLevel_1st.append(-1);
            m_OverloadLevel.append(-1);
            qDebug() << "Get_OverloadLevel() return false";
        }
    }
}

void dialog_motor::Read_OverloadLevel(int id)
{
    int _data=0;

    if(Motors->Get_OverloadLevel(id, _data))
        m_OverloadLevel[id] = _data;
    else
    {
        m_OverloadLevel[id] = -1;
        qDebug() << "Get_OverloadLevel() return false";
    }
}

void dialog_motor::Display_OverloadLevel(int line, int level)
{
    if(level == 0) level = 115; // panasonic 0 = 115%(max)
    if(level < 0)
    {
        // read error case
        m_vtLevel[line]->setText("---");
        return;
    }

    QString str;
    str.clear();
    str.sprintf("%d", level);
    str += "%";

    m_vtLevel[line]->setText(str);
}

void dialog_motor::Display_BtnSet(bool enable)
{
    ui->btnSet->setAutoFillBackground(enable);
    ui->btnSetIcon->setAutoFillBackground(enable);
    ui->btnSetIcon_2->setAutoFillBackground(enable);
}

void dialog_motor::Comp_Save_EEPROM()
{
    for(int i=0;i<m_OverloadLevel_1st.count();i++)
    {
        if(m_OverloadLevel_1st[i] != m_OverloadLevel[i])
        {
            Motors->Save_EEPROM(i);
            qDebug() << "Save to eeprom motor id = " << i;
        }
        else
            qDebug() << "Skip to eeprom save motor id = " << i;
    }
}
