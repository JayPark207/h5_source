#ifndef DIALOG_USESKIP_H
#define DIALOG_USESKIP_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "dialog_confirm.h"
#include "dialog_message.h"

namespace Ui {
class dialog_useskip;
}

class dialog_useskip : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_useskip(QWidget *parent = 0);
    ~dialog_useskip();

    void Update();

public slots:
    void onEnd();

    void onCheckbox();
    void onSelect();
    void onChange();

private:
    Ui::dialog_useskip *ui;

    QVector<QLabel3*> m_vtNo;
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtUse;

    void Display_Name();
    void Display_Data();
    void Display_Select(int index);
    void Display_Checkbox(bool bCheck);

    int m_nSelectedIndex;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_USESKIP_H
