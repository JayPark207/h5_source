#include "dialog_keyboard.h"
#include "ui_dialog_keyboard.h"

dialog_keyboard::dialog_keyboard(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_keyboard)
{
    ui->setupUi(this);

    m_kb = new keyboard(this);

    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setGeometry(0,0,MAX_WIDTH,MAX_HEIGHT);

    sw = new QStackedWidget(this);
    sw->addWidget(m_kb);

    sw->setGeometry(MAX_WIDTH-m_kb->width(),MAX_HEIGHT-m_kb->height(),m_kb->width(),m_kb->height());
    sw->setCurrentIndex(0);

    connect(m_kb,SIGNAL(Cancel()),this,SLOT(reject()));
    connect(m_kb,SIGNAL(Enter()),this,SLOT(accept()));

    setModal(true);

}

dialog_keyboard::~dialog_keyboard()
{
    delete ui;
}

void dialog_keyboard::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
