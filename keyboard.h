#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <QWidget>
#include <QLineEdit>

namespace Ui {
class keyboard;
}

class keyboard : public QWidget
{
    Q_OBJECT
    
public:
    explicit keyboard(QWidget *parent = 0);
    ~keyboard();

    void        SetText(QString text);
    QString     GetText();
    void        SetTitle(QString text);

signals:
    void        Cancel();
    void        Enter();

private slots:
    void KeyboardHandler();

    
    void on_btnBackspace_clicked();

    void on_txtView_textChanged(const QString &arg1);

    void on_btnEsc_clicked();

    void on_btnEnter_clicked();

    void on_txtView_lostFocus();

    void on_btnDel_clicked();

    void on_btnShift_toggled(bool checked);

    void on_btnFn_toggled(bool checked);

    void on_btnLeft_clicked();

    void on_btnRight_clicked();

private:
    Ui::keyboard *ui;

    QString     m_strOutputText;
    QLineEdit*  m_OutputLineEdit;
    bool        m_bShift;
    bool        m_bChar;

protected:
    void changeEvent(QEvent *);
};

#endif // KEYBOARD_H
