#ifndef TOP_MAIN_H
#define TOP_MAIN_H

#include <QWidget>

#include "global.h"

#include <QTimer>
#include <QDateTime>
#include <QDate>
#include <QTime>

#include "dialog_userlevel.h"
#include "dialog_confirm.h"
#include "dialog_error.h"

namespace Ui {
class top_main;
}

class top_main : public QWidget
{
    Q_OBJECT
    
public:
    explicit top_main(QWidget *parent = 0);
    ~top_main();

    QTimer* m_Timer;

public slots:
    void onTimer();
    void onDispTimer();
    void onSpeed();
    void onUser();

    void onSafetyZone();
    void onRobotIcon();


private:
    Ui::top_main *ui;

    QTimer* m_DispTimer;

    void Update();


    void DispRobotStatus(HyStatus::ENUM_ROBOT_STATUS status);
    void DispServo(bool onoff);
    void DispEmo(bool onoff);
    void DispJog(bool onoff); 
    void DispLed1(bool onoff); 
    void DispLed2(bool onoff); 
    void DispLed3(bool onoff);
    void DispSafetyZone(bool onoff);

    int m_nCount;

    QVector<QPixmap> pxmRobotStatus;
    QVector<QPixmap> pxmServo;
    QVector<QPixmap> pxmEmo;
    QVector<QPixmap> pxmJog;
    QVector<QPixmap> pxmLed1;
    QVector<QPixmap> pxmLed2;
    QVector<QPixmap> pxmLed3;
    QVector<QPixmap> pxmSafetyZone;



    // global io read update.
    void Update_IO();
    void Update_Input();
    void Update_Output();

    bool SoftSensor(bool use);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // TOP_MAIN_H
