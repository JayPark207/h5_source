#include "top_sub.h"
#include "ui_top_sub.h"

top_sub::top_sub(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::top_sub)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    pxmRobotStatus.clear();
    pxmServo.clear();
    pxmEmo.clear();
    pxmJog.clear();
    pxmLed1.clear();
    pxmLed2.clear();
    pxmLed3.clear();
    pxmSafetyZone.clear();
}

top_sub::~top_sub()
{
    delete ui;
}
void top_sub::showEvent(QShowEvent *)
{
    timer->start();
}
void top_sub::hideEvent(QHideEvent *)
{
    timer->stop();
}
void top_sub::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void top_sub::onTimer()
{
    ui->lbCurrMold->setText(Status->strNowRecipe);
    ui->pbSpeed->setValue(Status->nMainSpeed);
    ui->lbULevel->setText(QString().setNum(Status->nUserLevel));

    DispEmo(Status->bEmo);
    //DispServo(Status->bServo);
    DispSafetyZone(Status->bSafetyZone);
    DispJog(Status->bJog);
    DispRobotStatus(Status->nRobotStatus);

    DispLed1(Status->bLed1);
    DispLed2(Status->bLed2);
    DispLed3(Status->bLed3);
}


void top_sub::DispRobotStatus(HyStatus::ENUM_ROBOT_STATUS status)
{
    if(pxmRobotStatus.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-11.png");
        pxmRobotStatus.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-9.png");
        pxmRobotStatus.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-10.png");
        pxmRobotStatus.append(QPixmap::fromImage(img));
    }

    if((int)status < pxmRobotStatus.size())
        ui->lbRobotPic->setPixmap(pxmRobotStatus[status]);
}
void top_sub::DispServo(bool onoff)
{
    if(pxmServo.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-14.png");
        pxmServo.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-13.png");
        pxmServo.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmServo.size())
        ui->lbServoPic->setPixmap(pxmServo[(int)onoff]);
}
void top_sub::DispSafetyZone(bool onoff)
{
    if(pxmSafetyZone.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-73.png");
        pxmSafetyZone.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-74.png");
        pxmSafetyZone.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmSafetyZone.size())
        ui->lbServoPic->setPixmap(pxmSafetyZone[(int)onoff]);
}
void top_sub::DispJog(bool onoff)
{
    if(pxmJog.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-17.png");
        pxmJog.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-16.png");
        pxmJog.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmJog.size())
        ui->lbJogPic->setPixmap(pxmJog[(int)onoff]);
}
void top_sub::DispEmo(bool onoff)
{
    if(pxmEmo.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-2.png");
        pxmEmo.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-1.png");
        pxmEmo.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmEmo.size())
        ui->lbEmoPic->setPixmap(pxmEmo[(int)onoff]);
}

void top_sub::DispLed1(bool onoff)
{
    if(pxmLed1.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon992-0.png");
        pxmLed1.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon992-4.png");
        pxmLed1.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmLed1.size())
        ui->led1->setPixmap(pxmLed1[(int)onoff]);
}
void top_sub::DispLed2(bool onoff)
{
    if(pxmLed2.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon992-0.png");
        pxmLed2.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon992-5.png");
        pxmLed2.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmLed2.size())
        ui->led2->setPixmap(pxmLed2[(int)onoff]);
}
void top_sub::DispLed3(bool onoff)
{
    if(pxmLed3.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon992-0.png");
        pxmLed3.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon992-10.png");
        pxmLed3.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmLed3.size())
        ui->led3->setPixmap(pxmLed3[(int)onoff]);
}
