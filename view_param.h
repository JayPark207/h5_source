#ifndef VIEW_PARAM_H
#define VIEW_PARAM_H

#include <QDialog>

#include "global.h"
#include "dialog_keyboard.h"

#include "dialog_robotconfig.h"


namespace Ui {
class view_param;
}

class view_param : public QDialog
{
    Q_OBJECT

public:
    explicit view_param(QWidget *parent = 0);
    ~view_param();

public slots:
    void onSave();
    void onEdit();
    void onReset();

    void onTest();

private:
    Ui::view_param *ui;
    dialog_keyboard* kb;

    QStringList m_Header;

    void Update();
    void Save();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // VIEW_PARAM_H
