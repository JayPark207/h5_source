#include "dialog_jmotion.h"
#include "ui_dialog_jmotion.h"

dialog_jmotion::dialog_jmotion(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_jmotion)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));


    connect(ui->tbUsePic,SIGNAL(mouse_release()),this,SLOT(onUse()));
    connect(ui->tbUse,SIGNAL(mouse_press()),ui->tbUsePic,SLOT(press()));
    connect(ui->tbUse,SIGNAL(mouse_release()),ui->tbUsePic,SLOT(release()));

    m_vtSDist.clear();m_vtSDistPic.clear();
    m_vtSDist.append(ui->tbSDist);  m_vtSDistPic.append(ui->tbSDistPic);
    m_vtSDist.append(ui->tbSDist_2);m_vtSDistPic.append(ui->tbSDistPic_2);

    int i;
    for(i=0;i<m_vtSDistPic.count();i++)
    {
        connect(m_vtSDistPic[i],SIGNAL(mouse_release()),this,SLOT(onSDist()));
        connect(m_vtSDist[i],SIGNAL(mouse_press()),m_vtSDistPic[i],SLOT(press()));
        connect(m_vtSDist[i],SIGNAL(mouse_release()),m_vtSDistPic[i],SLOT(release()));
    }

    m_vtTbTitle.clear();
    m_vtTbTitle.append(ui->tbP12Title);
    m_vtTbTitle.append(ui->tbP21Title);

}

dialog_jmotion::~dialog_jmotion()
{
    delete ui;
}
void dialog_jmotion::showEvent(QShowEvent *)
{
    this->Update();
}
void dialog_jmotion::hideEvent(QHideEvent *){}
void dialog_jmotion::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_jmotion::Update()
{

    m_ItemName.clear();
    m_ItemName.append(tr("No Use"));
    m_ItemName.append(tr("Use"));

    vars.clear();
    vars.append(HyRecipe::mdJMotion);
    vars.append(HyRecipe::vJSDist1);
    vars.append(HyRecipe::vJSDist2);

    if(Recipe->Gets(vars, datas))
    {
        //use
        if(datas.first() < m_ItemName.size())
            ui->tbUse->setText(m_ItemName[(int)datas.first()]);

        QString str;
        for(int i=0;i<m_vtSDist.count();i++)
        {
            str.sprintf(FORMAT_POS,datas[1+i]);
            m_vtSDist[i]->setText(str);
        }

        ui->grpTable->setEnabled((int)datas.first() > 0);
    }
}

void dialog_jmotion::onUse()
{
    sel = (dialog_mode_select*)gGetDialog(DIG_MODE_SELECT);
    sel->InitTitle(ui->tbUseTitle->text());
    sel->InitRecipe((int)HyRecipe::mdJMotion);
    sel->InitNum(m_ItemName.size());
    for(int i=0;i<sel->GetNum();i++)
        sel->InitString(i,m_ItemName[i]);

    if(sel->exec() == QDialog::Accepted)
        Update();
}

void dialog_jmotion::onSDist()
{
    QLabel4* btn = (QLabel4*)sender();

    int index = m_vtSDistPic.indexOf(btn);
    if(index < 0) return;

    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(99999.99);
    np->m_numpad->SetSosuNum(SOSU_POS);
    np->m_numpad->SetTitle(m_vtTbTitle[index]->text());
    np->m_numpad->SetNum(datas[1+index]);
    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(vars[1+index],(float)np->m_numpad->GetNumDouble()))
            Update();
    }
}

