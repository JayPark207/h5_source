#ifndef DIALOG_COMMON_POSITION_H
#define DIALOG_COMMON_POSITION_H

#include <QDialog>
#include <QTimer>

#include "global.h"

#include "qlabel4.h"
#include "qlabel3.h"

#include "dialog_position_teaching2.h"

namespace Ui {
class dialog_common_position;
}

class dialog_common_position : public QDialog
{
    Q_OBJECT

public:
    enum COMMON_POS_INDEX
    {
        JIG_CHANGE=0,
        SAMPLE,
        FAULTY,

        POS_ALL,
    };

public:
    explicit dialog_common_position(QWidget *parent = 0);
    ~dialog_common_position();

    void Init(COMMON_POS_INDEX use_pos_index = POS_ALL, bool bTeachMode = true);

    void Update(int btn_index);

private slots:
    void onPosSelect();
    void onTimer();
    void onTeach();
    void onMove();

private:
    Ui::dialog_common_position *ui;

    int m_nSelectedIndex;

    QVector<HyPos::ENUM_POS_ARRAY> m_vtPosIndex;
    QStringList m_slProgName;
    QVector<QLabel4*> m_vtPosPic;
    QVector<QLabel3*> m_vtPosTxt;
    QVector<QLabel3*> m_vtPosIcon;

    QTimer* timer;

    void Display_SelectBtn(int btn_index);
    void SetMoving(bool bMoving);
    QVector<QPixmap> pxmMoving;

    bool m_bMoving, m_bHolding;

    void Enable_SelectBtn(bool enable);

    COMMON_POS_INDEX m_UsePosIndex;
    bool m_bTeachMode;
    void ModeChange();

    void Display_SetPos(cn_trans trans, cn_joint joint);
    void Display_RealPos(float *trans, float *joint);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // DIALOG_COMMON_POSITION_H
