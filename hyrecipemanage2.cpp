#include "hyrecipemanage2.h"

HyRecipeManage2::HyRecipeManage2(QObject *parent) : QObject(parent)
{

}

// when booting, need only one execution.
// now_no = Param read data.
bool HyRecipeManage2::Initial(QString now_no)
{
    if(now_no.toInt() < 1)
    {
        SetNowRecipeNo(QString().setNum(-1)); // recipe error.
        return false;
    }

    if(!ReadDatum())
    {
        SetNowRecipeNo(QString().setNum(0)); // recipe error.
        return false;
    }

    // no have recipe.
    if(m_No.size() < 1)
    {
        SetNowRecipeNo(QString().setNum(0));  // recipe error.
        return false;
    }

    // check have now_no recipe.
    int list_index = m_No.indexOf(now_no);
    if(list_index < 0)
    {
        //Load_Force(0);
        //SetNowRecipeNo(QString().setNum(-2));
        SetNowRecipeNo(now_no);
        return false;
    }

    // now recipe no. save (ok) & <force load.(?) - when initial fail!!>
    SetNowRecipeNo(now_no);
    return true;
}

void HyRecipeManage2::SetNowRecipeNo(QString no)
{
    m_NowNo = no;
}
QString HyRecipeManage2::GetNowRecipeNo()
{
    return m_NowNo;
}
QString HyRecipeManage2::GetNowRecipeNoName()
{
    // m_NowNo = 0 => Error display.
    return GetDisplayName(GetNowRecipeNo());
}
bool HyRecipeManage2::GetNowRecipeName(QString &name)
{
    QString no = GetNowRecipeNo();

    // recipe no = 0 case is recipe error.
    if(no.toInt() == 0)
        return false;
    else if(no.toInt() < 0)
        return false;

    int index = m_No.indexOf(no);
    if(index < 0) return false;

    name = m_Name.at(index);
    return true;
}

/** data update. **/
bool HyRecipeManage2::ReadDatum()
{
    m_No.clear();
    m_Name.clear();
    m_Date.clear();

    if(!ReadDatum_Inner())
        return false;
    if(!ReadDatum_SD())
        return false;
    if(!ReadInfo())
        return false;

    return true;
}

bool HyRecipeManage2::ReadDatum_Inner()
{
    // Recipe folder content -> data list.
    CNRobo* pCon = CNRobo::getInstance();
    int ret=0;
    QString path = RM_RECIPE_PATH;

    // (1) get recipe list
    QStringList list;
    ret = pCon->getDatabaseDirList(path, list);
    if(ret < 0)
    {
        qDebug() << "getDatabaseDirList ret =" << ret;
        return false;
    }
    // (2) remove don't need datas.
    list.removeAll(".");
    list.removeAll("..");
    list.removeAll("default");

    // (3) casting integer & sorting 1,2,3,~,N
    QList<int> int_list;
    foreach(QString str, list)
    {
        if(str.toInt() <= RM_MAX_RECIPE_INNER_NUM) // ignore sd data(big no).
            int_list << str.toInt();
    }
    qSort(int_list.begin(),int_list.end());

    // (4) make m_No data. (this is a key)
    foreach(int num, int_list)
    {
        m_No << QString().setNum(num);
    }

    return true;
}

bool HyRecipeManage2::ReadDatum_SD()
{
    if(!IsSD()) return true;    // ignore
    if(!IsSDRecipe()) return true;  // ignore.

    // Recipe folder content -> data list.
    CNRobo* pCon = CNRobo::getInstance();
    int ret=0;
    QString path = RM_SD_RECIPE_PATH;

    // (1) get recipe list
    QStringList list;
    ret = pCon->getDatabaseDirList(path, list);
    if(ret < 0)
    {
        qDebug() << "getDatabaseDirList ret =" << ret;
        return false;
    }
    // (2) remove don't need datas.
    list.removeAll(".");
    list.removeAll("..");
    list.removeAll("default");

    // (3) casting integer & sorting 1,2,3,~,N
    QList<int> int_list;
    foreach(QString str, list)
    {
        if(str.toInt() > RM_MAX_RECIPE_INNER_NUM) // ignore inner data(small no).
            int_list << str.toInt();
    }
    qSort(int_list.begin(),int_list.end());

    // (4) make m_No data. (this is a key)
    foreach(int num, int_list)
    {
        m_No << QString().setNum(num);
    }

    return true;
}

bool HyRecipeManage2::ReadInfo()
{
    // read info files using m_No.
    int i;
    QString name;
    QDateTime date;
    for(i=0;i<m_No.count();i++)
    {
        if(!GetInfo(m_No[i], name, date))
            return false;

        m_Name.append(name);
        m_Date.append(date.date().toString(RM_DATE_FORMAT));

        qDebug() << m_No[i] << m_Name[i] << m_Date[i];
    }

    return true;
}


/** private **/
bool HyRecipeManage2::SetInfo(QString no, QString name, QDateTime date)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret=0;
    QStringList list;
    list.append(no);
    list.append(name);
    list.append(date.toString(Qt::ISODate));

    QString path = GetRecipePath(no);

    qDebug() << path;

    if(!IsExist(path))
    {
        qDebug() << "SetInfo():Not exist dir." << path;
        return false;
    }

    path += RM_INFO_FILENAME;

    ret = pCon->createProgramFile(path, list);
    if(ret < 0)
    {
        qDebug() << "createProgramFile ret =" << ret;
        return false;
    }

    qDebug() << "SetInfo OK!";
    return true;
}

// rename function.
bool HyRecipeManage2::SetInfo(QString no, QString name)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    QString path = GetRecipePath(no);
    path += RM_INFO_FILENAME;

    qDebug() << path;

    if(!IsExist(path))
    {
        qDebug() << "SetInfo():Not exist info file." << path;
        return false;
    }

    QStringList list;
    ret = pCon->getProgramFile(path, list);
    if(ret < 0)
    {
        qDebug() << "getProgramFile ret =" << ret;
        return false;
    }

    list[INFO_NAME] = name;

    ret = pCon->createProgramFile(path, list);
    if(ret < 0)
    {
        qDebug() << "createProgramFile ret =" << ret;
        return false;
    }

    return true;
}

bool HyRecipeManage2::GetInfo(QString no, QString &name, QDateTime &date)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    QString path = GetRecipePath(no);
    path += RM_INFO_FILENAME;

    qDebug() << path;

    if(!IsExist(path))
    {
        qDebug() << "GetInfo():Not exist info file." << path;
        return false;
    }

    QStringList list;
    ret = pCon->getProgramFile(path, list);
    if(ret < 0)
    {
        qDebug() << "getProgramFile ret =" << ret;
        return false;
    }

    name = list[INFO_NAME];
    date = QDateTime::fromString(list[INFO_DATE], Qt::ISODate);

    return true;
}

bool HyRecipeManage2::GetInfo(QString no, QString& name)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    QString path = GetRecipePath(no);
    path += RM_INFO_FILENAME;

    qDebug() << path;

    if(!IsExist(path))
    {
        qDebug() << "GetInfo():Not exist info file." << path;
        return false;
    }

    QStringList list;
    ret = pCon->getProgramFile(path, list);
    if(ret < 0)
    {
        qDebug() << "getProgramFile ret =" << ret;
        return false;
    }

    name = list[INFO_NAME];

    return true;
}


/** index & list manager. **/
// only list var. save.
void HyRecipeManage2::AddRecipeList(int list_index, QString no, QString name, QDateTime date)
{
    m_No.insert(list_index, no);
    m_Name.insert(list_index, name);
    m_Date.insert(list_index, date.date().toString(RM_DATE_FORMAT));
}
void HyRecipeManage2::DelRecipeList(int list_index)
{
    m_No.removeAt(list_index);
    m_Name.removeAt(list_index);
    m_Date.removeAt(list_index);
}

int HyRecipeManage2::Find_NewListIndex()
{
    int i,index;
    for(i=0;i<m_No.count();i++)
    {
        // search no=1 ~ sequntialy
        index = m_No.indexOf(QString().setNum(i+1));
        if(index < 0)
            return i;
    }
    return i;
}
int HyRecipeManage2::ListIndexOf(QString no)
{
    return m_No.indexOf(no);
}

int HyRecipeManage2::GetCount()
{
    return m_No.count();
}
QString HyRecipeManage2::GetNo(int list_index)
{
    if(list_index >= m_No.size())
        return "";

    return m_No.at(list_index);
}
QString HyRecipeManage2::GetName(int list_index)
{
    if(list_index >= m_Name.size())
        return "";

    return m_Name.at(list_index);
}
QString HyRecipeManage2::GetName(QString no)
{
    int i = m_No.indexOf(no);
    if(i < 0) return "";

    return m_Name.at(i);
}

QString HyRecipeManage2::GetDate(int list_index)
{
    if(list_index >= m_Date.size())
        return "";

    return m_Date.at(list_index);
}
QString HyRecipeManage2::GetDate(QString no)
{
    int i = m_No.indexOf(no);
    if(i < 0) return "";

    return m_Date.at(i);
}

QString HyRecipeManage2::GetDisplayName(QString no)
{
    // recipe no = 0 case is recipe error.
    if(no.toInt() == 0)
        return tr("Please Mold New & Load!");
    else if(no.toInt() < 0)
        return tr("Please Mold Load");

    int i = m_No.indexOf(no);
    if(i < 0)
    {
        if(no.toInt() > RM_MAX_RECIPE_INNER_NUM)
            return QString(tr("[%1]Mold Error! Insert SD!!")).arg(no);
        else
            return QString(tr("[%1]Mold Error! Check!!")).arg(no);
    }

    QString name;
    name.sprintf("[%d] ",no.toInt());
    name += m_Name.at(i);

    return name;
}

QString HyRecipeManage2::GetDisplayName(int list_index)
{
    if(list_index >= m_No.size())
        return "";

    QString name;
    name.sprintf("[%d] ",m_No[list_index].toInt());
    name += m_Name.at(list_index);

    return name;
}




/** ======================== **/


/** Check functions **/
bool HyRecipeManage2::IsExist(QString path)
{
    CNRobo* pCon = CNRobo::getInstance();
    bool bExist=false;

    if(pCon->checkDirFileExist(path, bExist) < 0)
        return false;

    return bExist;
}
bool HyRecipeManage2::IsSD()
{
    CNRobo* pCon = CNRobo::getInstance();
    bool bExist=false;

    if(pCon->checkDirFileExist(SD_EXIST_PATH, bExist) < 0)
        return false;

    return bExist;
}
bool HyRecipeManage2::IsUSB()
{
    CNRobo* pCon = CNRobo::getInstance();
    bool bExist=false;

    if(pCon->checkDirFileExist(USB_EXIST_PATH, bExist) < 0)
        return false;

    return bExist;
}
bool HyRecipeManage2::IsSDRecipe()
{
    CNRobo* pCon = CNRobo::getInstance();
    bool bExist=false;

    if(pCon->checkDirFileExist(RM_SD_RECIPE_PATH, bExist) < 0)
        return false;

    return bExist;
}
bool HyRecipeManage2::IsTPRecipe()
{
    CNRobo* pCon = CNRobo::getInstance();
    bool bExist=false;

    if(pCon->checkDirFileExist(RM_RECIPE_PATH, bExist) < 0)
        return false;

    return bExist;
}


/** ============== **/


QString HyRecipeManage2::GetRecipePath(int list_index)
{
    // list must has this index.
    QString path;

    int no = m_No[list_index].toInt();

    if(no <= RM_MAX_RECIPE_INNER_NUM)
        path = RM_RECIPE_PATH;
    else
        path = RM_SD_RECIPE_PATH;

    path += "/";
    path += QString().setNum(no);

    return path;
}

QString HyRecipeManage2::GetRecipePath(QString no)
{
    // don't have list is ok.

    QString path;

    int num = no.toInt();

    if(num <= RM_MAX_RECIPE_INNER_NUM)
        path = RM_RECIPE_PATH;
    else
        path = RM_SD_RECIPE_PATH;

    path += "/";
    path += QString().setNum(num);

    return path;
}


/** mainly functions **/
bool HyRecipeManage2::Sync(int list_index)
{
    if(list_index >= m_No.size()) return false;
    if(list_index < 0) return false;

    QString recipe_path = GetRecipePath(list_index);
    if(!IsExist(recipe_path)) return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    QString path = recipe_path + RM_VAR_FILENAME;

    ret = pCon->exportVariable(path);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "exportVariable ret =" << ret;
        return false;
    }

    QString source = RM_PROG_PATH;
    QString target = recipe_path;

    ret = pCon->copyDatabaseDir(source, target);
    if(ret < 0)
    {
        qDebug() << source << "=>>" << target;
        qDebug() << "copyDatabaseDir ret =" << ret;
        return false;
    }

    qDebug() << "Recipe Sync Success!" << m_No[list_index];
    return true;
}
bool HyRecipeManage2::ASync(int list_index)
{
    if(list_index >= m_No.size()) return false;
    if(list_index < 0) return false;

    QString recipe_path = GetRecipePath(list_index);
    if(!IsExist(recipe_path)) return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    pCon->saveVariable();

    // 1. export variable file to recipe db.
    QString path = recipe_path + RM_VAR_FILENAME;
    ret = pCon->exportVariable(path);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "exportVariable ret =" << ret;
        return false;
    }

    // 2. program from tp to recipe db.
    // 2-1. read main in tp -> make using program list.
    QStringList main_list;
    ret = pCon->getProgramSteps(MACRO_MAIN, main_list);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps ret =" << ret;
        return false;
    }

    int i;
    QStringList prog_list;
    prog_list.clear();
    for(i=0;i<main_list.count();i++)
    {

        CallLine2MacroName(main_list[i]);
        prog_list.append(main_list[i]);

        qDebug() << main_list[i];
    }
    prog_list.append(MACRO_MAIN);

    // 2-2. copy file from tp to db
    QString source = RM_PROG_PATH;
    QString target = recipe_path;
    target += RM_PROG_DIRNAME;

    QString source_file,target_file;
    QStringList prog_step_list;
    for(i=0;i<prog_list.count();i++)
    {
        source_file = source + "/" + prog_list[i];
        target_file = target + "/" + prog_list[i];

        qDebug() << source_file << target_file;

        prog_step_list.clear();
        ret = pCon->getProgramFile(source_file, prog_step_list);
        if(ret < 0)
        {
            qDebug() << "getProgramFile ret =" << ret;
            return false;
        }

        ret = pCon->createProgramFile(target_file, prog_step_list);
        if(ret < 0)
        {
            qDebug() << "createProgramFile ret =" << ret;
            return false;
        }

        qDebug() << prog_list[i];
        /*
        ret = pCon->copyDatabaseFile(source_file, target_file);
        if(ret < 0)
        {
            qDebug() << source_file << "=>>" << target_file;
            qDebug() << "copyDatabaseFile ret =" << ret;
            return false;
        }
        */
    }

    qDebug() << "Recipe ASync Success!" << m_No[list_index];
    return true;
}

bool HyRecipeManage2::Load(int old_list_index, int new_list_index)
{
    if(old_list_index >= m_No.size()) return false;
    if(old_list_index < 0) return false;
    if(new_list_index >= m_No.size()) return false;
    if(new_list_index < 0) return false;

    if(!Sync(old_list_index))
        return false;

    QString recipe_path = GetRecipePath(new_list_index);
    QString path = recipe_path + RM_VAR_FILENAME;

    if(!IsExist(path))
        return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    ret = pCon->importVariable(path);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "importVariable ret =" << ret;
        return false;
    }

    QString source = recipe_path + RM_PROG_DIRNAME;
    //QString target = RM_BASE_PATH;

    // here modify
    QStringList prog_list;
    ret = pCon->getDatabaseFileList(source, prog_list);
    if(ret < 0)
    {
        qDebug() << source;
        qDebug() << "getDatabaseFileList ret =" << ret;
        return false;
    }

    for(int i=0;i<prog_list.count();i++)
    {
        prog_list[i].push_front(source + "/");
        qDebug() << prog_list[i];
    }

    ret = pCon->importProgramList(prog_list);
    if(ret < 0)
    {
        qDebug() << "importProgramList ret =" << ret;
        return false;
    }

    ret = pCon->saveProgramAll();
    if(ret < 0)
    {
        qDebug() << "saveProgramAll ret =" << ret;
        return false;
    }

    // save now recipe no.
    SetNowRecipeNo(m_No[new_list_index]);

    qDebug() << "Recipe Load Success!" << old_list_index << "->" << new_list_index;
    return true;
}
bool HyRecipeManage2::ALoad(int old_list_index, int new_list_index)
{
    if(old_list_index >= m_No.size()) return false;
    if(old_list_index < 0) return false;
    if(new_list_index >= m_No.size()) return false;
    if(new_list_index < 0) return false;

    ti.start();
    // 0. sync
    if(!ASync(old_list_index))
        return false;

    qDebug() << "async time = " << ti.elapsed() << "msec";

    // 1. make path & check exist var file.
    QString recipe_path = GetRecipePath(new_list_index);
    QString path = recipe_path + RM_VAR_FILENAME;

    if(!IsExist(path))
        return false;

    // 2. import var. file
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    ret = pCon->importVariable(path);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "importVariable ret =" << ret;
        return false;
    }

    // 3. var. stack => dmcvar.dat
    pCon->saveVariable();

    qDebug() << "aload1 time = " << ti.elapsed() << "msec";

    QString source = recipe_path + RM_PROG_DIRNAME;
    //QString target = RM_BASE_PATH;

    // here modify
    QStringList prog_list;
    prog_list.clear();
    ret = pCon->getDatabaseFileList(source, prog_list);
    if(ret < 0)
    {
        qDebug() << source;
        qDebug() << "getDatabaseFileList ret =" << ret;
        return false;
    }

    qDebug() << "aload2 time = " << ti.elapsed() << "msec";

    QStringList prog_list_path;
    QString str;
    int i;
    prog_list_path.clear();
    for(i=0;i<prog_list.count();i++)
    {
        str = source;
        str += "/";
        str += prog_list[i];
        prog_list_path.append(str);

        qDebug() << prog_list_path[i];
    }

    ret = pCon->importProgramList(prog_list_path);
    if(ret < 0)
    {
        qDebug() << "importProgramList ret =" << ret;
        return false;
    }
    qDebug() << "aload3 time = " << ti.elapsed() << "msec";

    /*for(i=0;i<prog_list.count();i++)
    {
        ret = pCon->saveProgram(prog_list[i]);
        if(ret < 0)
        {
            qDebug() << prog_list[i];
            qDebug() << "saveProgram ret =" << ret;
            return false;
        }
    }*/

    ret = pCon->savePrograms(prog_list);
    if(ret < 0)
    {
        qDebug() << "HyRecipeManage2::ALoad() savePrograms ret =" << ret;
        return false;
    }

    // save now recipe no.
    SetNowRecipeNo(m_No[new_list_index]);

    qDebug() << "aload4 time = " << ti.elapsed() << "msec";

    qDebug() << "Recipe ALoad Success!" << old_list_index << "->" << new_list_index;
    return true;
}
bool HyRecipeManage2::Load_Force(int list_index)
{
    if(list_index >= m_No.size()) return false;
    if(list_index < 0) return false;

    QString recipe_path = GetRecipePath(list_index);
    QString path = recipe_path + RM_VAR_FILENAME;

    if(!IsExist(path))
        return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    ret = pCon->importVariable(path);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "importVariable ret =" << ret;
        return false;
    }

    QString source = recipe_path + RM_PROG_DIRNAME;
    //QString target = RM_BASE_PATH;

    QStringList prog_list;
    ret = pCon->getDatabaseFileList(source, prog_list);
    if(ret < 0)
    {
        qDebug() << source;
        qDebug() << "getDatabaseFileList ret =" << ret;
        return false;
    }

    for(int i=0;i<prog_list.count();i++)
    {
        prog_list[i].push_front(source + "/");
        qDebug() << prog_list[i];
    }

    ret = pCon->importProgramList(prog_list);
    if(ret < 0)
    {
        qDebug() << "importProgramList ret =" << ret;
        return false;
    }

    ret = pCon->saveProgramAll();
    if(ret < 0)
    {
        qDebug() << "saveProgramAll ret =" << ret;
        return false;
    }

    // save now recipe no.
    SetNowRecipeNo(m_No[list_index]);

    qDebug() << "Recipe Only Load Success!" << list_index;
    return true;
}
bool HyRecipeManage2::ALoad_Force(int list_index)
{
    if(list_index >= m_No.size()) return false;
    if(list_index < 0) return false;


    // 1. make path & check exist var file.
    QString recipe_path = GetRecipePath(list_index);
    QString path = recipe_path + RM_VAR_FILENAME;

    if(!IsExist(path))
        return false;

    // 2. import var. file
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    ret = pCon->importVariable(path);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "importVariable ret =" << ret;
        return false;
    }

    QString source = recipe_path + RM_PROG_DIRNAME;
    //QString target = RM_BASE_PATH;

    // here modify
    QStringList prog_list;
    prog_list.clear();
    ret = pCon->getDatabaseFileList(source, prog_list);
    if(ret < 0)
    {
        qDebug() << source;
        qDebug() << "getDatabaseFileList ret =" << ret;
        return false;
    }

    QStringList prog_list_path;
    QString str;
    int i;
    prog_list_path.clear();
    for(i=0;i<prog_list.count();i++)
    {
        str = source;
        str += "/";
        str += prog_list[i];
        prog_list_path.append(str);

        qDebug() << prog_list_path[i];
    }

    ret = pCon->importProgramList(prog_list_path);
    if(ret < 0)
    {
        qDebug() << "importProgramList ret =" << ret;
        return false;
    }

    /*for(i=0;i<prog_list.count();i++)
    {
        ret = pCon->saveProgram(prog_list[i]);
        if(ret < 0)
        {
            qDebug() << prog_list[i];
            qDebug() << "saveProgram ret =" << ret;
            return false;
        }
    }*/

    ret = pCon->savePrograms(prog_list);
    if(ret < 0)
    {
        qDebug() << "HyRecipeManage2::ALoad_Force() savePrograms ret =" << ret;
        return false;
    }

    // save now recipe no.
    SetNowRecipeNo(m_No[list_index]);

    qDebug() << "Recipe ALoad_Force Success!" << list_index;
    return true;
}

bool HyRecipeManage2::New(QString name)
{
    int list_index = Find_NewListIndex();

    if(!New(list_index, name))
        return false;

    return true;
}
bool HyRecipeManage2::ANew(QString name)
{
    int list_index = Find_NewListIndex();

    if(!ANew(list_index, name))
        return false;

    return true;
}

// list_index = get Find_NewListIndex().
bool HyRecipeManage2::New(int list_index, QString name)
{
    QString no = QString().setNum(list_index + 1); // always.

    QString recipe_path = GetRecipePath(no);

    QString source = RM_RECIPE_PATH;
    source += RM_DEFAULT_DIRNAME;
    QString target = recipe_path;

    if(!IsExist(source)) return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->copyDatabaseDir(source, target, true);
    if(ret < 0)
    {
        qDebug() << source << "=>>" << target;
        qDebug() << "copyDatabaseDir ret =" << ret;
        return false;
    }

    // setup info file.
    QDateTime date = QDateTime::currentDateTime();
    if(!SetInfo(no, name, date))
        return false;

    // add list.
    AddRecipeList(list_index, no, name, date);

    return true;
}
bool HyRecipeManage2::ANew(int list_index, QString name)
{
    QString no = QString().setNum(list_index + 1); // always.

    QString recipe_path = GetRecipePath(no);

    QString source = RM_RECIPE_PATH;
    source += RM_DEFAULT_DIRNAME;
    QString target = recipe_path;

    if(!IsExist(source)) return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->copyDatabaseDir(source, target, true);
    if(ret < 0)
    {
        qDebug() << source << "=>>" << target;
        qDebug() << "copyDatabaseDir ret =" << ret;
        return false;
    }

    // setup info file.
    QDateTime date = QDateTime::currentDateTime();
    if(!SetInfo(no, name, date))
        return false;

    // add list.
    AddRecipeList(list_index, no, name, date);

    qDebug() << "Recipe ANew success!";
    return true;
}

// target_list_index = get Find_NewListIndex().
bool HyRecipeManage2::Copy(int source_list_index, int target_list_index, bool bVar, bool bProg)
{
    if(source_list_index >= m_No.size()) return false;
    if(source_list_index < 0) return false;

    QString no = QString().setNum(target_list_index + 1); // always.

    QString source = GetRecipePath(source_list_index);
    QString target = GetRecipePath(no);

    if(!IsExist(source)) return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->copyDatabaseDir(source, target, true);
    if(ret < 0)
    {
        qDebug() << source << "=>>" << target;
        qDebug() << "copyDatabaseDir ret =" << ret;
        return false;
    }

    QString default_dir = RM_RECIPE_PATH;
    default_dir += RM_DEFAULT_DIRNAME;
    // variable from default.
    if(!bVar)
    {
        QString var_path = default_dir + RM_VAR_FILENAME;
        ret = pCon->copyDatabaseFile(var_path, target);
        if(ret < 0)
        {
            qDebug() << source << "=>>" << target;
            qDebug() << "copyDatabaseFile ret =" << ret;
            return false;
        }
    }
    // program form default.
    if(!bProg)
    {
        QString prog_path = default_dir + RM_PROG_DIRNAME;
        ret = pCon->copyDatabaseDir(prog_path, target, true);
        if(ret < 0)
        {
            qDebug() << source << "=>>" << target;
            qDebug() << "copyDatabaseDir ret =" << ret;
            return false;
        }
    }

    // setup info file.
    QString name = m_Name[source_list_index];
    name += "_copy";
    QDateTime date = QDateTime::currentDateTime();
    if(!SetInfo(no, name, date))
        return false;

    // add list.
    AddRecipeList(target_list_index, no, name, date);

    return true;
}
bool HyRecipeManage2::ACopy(int source_list_index, int target_list_index, bool bVar, bool bProg)
{
    if(source_list_index >= m_No.size()) return false;
    if(source_list_index < 0) return false;

    QString no = QString().setNum(target_list_index + 1); // always.

    QString source = GetRecipePath(source_list_index);
    QString target = GetRecipePath(no);

    if(!IsExist(source)) return false;

    // if GetNo(source_list_index) == now recipe no



    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->copyDatabaseDir(source, target, true);
    if(ret < 0)
    {
        qDebug() << source << "=>>" << target;
        qDebug() << "copyDatabaseDir ret =" << ret;
        return false;
    }

    QString default_dir = RM_RECIPE_PATH;
    default_dir += RM_DEFAULT_DIRNAME;
    // variable from default.
    if(!bVar)
    {
        QString var_path = default_dir + RM_VAR_FILENAME;
        ret = pCon->copyDatabaseFile(var_path, target);
        if(ret < 0)
        {
            qDebug() << source << "=>>" << target;
            qDebug() << "copyDatabaseFile ret =" << ret;
            return false;
        }
    }
    // program form default.
    if(!bProg)
    {
        QString prog_path = default_dir + RM_PROG_DIRNAME;
        ret = pCon->copyDatabaseDir(prog_path, target, true);
        if(ret < 0)
        {
            qDebug() << source << "=>>" << target;
            qDebug() << "copyDatabaseDir ret =" << ret;
            return false;
        }
    }

    // setup info file.
    QString name = m_Name[source_list_index];
    name += "_copy";
    QDateTime date = QDateTime::currentDateTime();
    if(!SetInfo(no, name, date))
        return false;

    // add list.
    AddRecipeList(target_list_index, no, name, date);

    return true;
}

bool HyRecipeManage2::Delete(int list_index)
{
    if(list_index >= m_No.size()) return false;
    if(list_index < 0) return false;

    QString target = GetRecipePath(list_index);

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->deleteDatabaseDir(target);
    if(ret < 0)
    {
        qDebug() << target;
        qDebug() << "deleteDatabaseDir ret =" << ret;
        return false;
    }

    // delete list.
    DelRecipeList(list_index);

    return true;
}
bool HyRecipeManage2::ADelete(int list_index)
{
    if(list_index >= m_No.size()) return false;
    if(list_index < 0) return false;

    QString target = GetRecipePath(list_index);

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->deleteDatabaseDir(target);
    if(ret < 0)
    {
        qDebug() << target;
        qDebug() << "deleteDatabaseDir ret =" << ret;
        return false;
    }

    // delete list.
    DelRecipeList(list_index);

    return true;
}

bool HyRecipeManage2::Rename(int list_index, QString name)
{
    if(list_index >= m_No.size()) return false;
    if(list_index < 0) return false;

    // set
    if(!SetInfo(m_No[list_index], name))
        return false;

    // get
    QString _name;
    if(!GetInfo(m_No[list_index],_name))
        return false;

    // compare.
    if(name != _name)
        return false;

    // list update.
    m_Name[list_index] = name;

    return true;
}
bool HyRecipeManage2::ARename(int list_index, QString name)
{
    if(list_index >= m_No.size()) return false;
    if(list_index < 0) return false;

    // set
    if(!SetInfo(m_No[list_index], name))
        return false;

    // get
    QString _name;
    if(!GetInfo(m_No[list_index],_name))
        return false;

    // compare.
    if(name != _name)
        return false;

    // list update.
    m_Name[list_index] = name;

    return true;
}

// now var & programs => default Recipe.
// sync now tp to default folder.
bool HyRecipeManage2::MakeDefualt()
{
    QString default_path = RM_RECIPE_PATH;
    default_path += RM_DEFAULT_DIRNAME;

    if(!IsExist(default_path)) return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    pCon->saveVariable();

    // 1. export variable file to default folder.
    QString path = default_path + RM_VAR_FILENAME;
    ret = pCon->exportVariable(path);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "exportVariable ret =" << ret;
        return false;
    }

    // 2. program from tp to default folder
    // 2-1. read main in tp -> make using program list.
    QStringList main_list;
    ret = pCon->getProgramSteps(MACRO_MAIN, main_list);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps ret =" << ret;
        return false;
    }

    int i;
    QStringList prog_list;
    prog_list.clear();
    for(i=0;i<main_list.count();i++)
    {

        CallLine2MacroName(main_list[i]);
        prog_list.append(main_list[i]);

        qDebug() << main_list[i];
    }
    prog_list.append(MACRO_MAIN);

    // 2-2. copy file from tp to default folder.
    QString source = RM_PROG_PATH;
    QString target = default_path;
    target += RM_PROG_DIRNAME;

    QString source_file,target_file;
    QStringList prog_step_list;
    for(i=0;i<prog_list.count();i++)
    {
        source_file = source + "/" + prog_list[i];
        target_file = target + "/" + prog_list[i];

        qDebug() << source_file << target_file;

        prog_step_list.clear();
        ret = pCon->getProgramFile(source_file, prog_step_list);
        if(ret < 0)
        {
            qDebug() << "getProgramFile ret =" << ret;
            return false;
        }

        ret = pCon->createProgramFile(target_file, prog_step_list);
        if(ret < 0)
        {
            qDebug() << "createProgramFile ret =" << ret;
            return false;
        }

        qDebug() << prog_list[i];
    }

    /*QString source = RM_PROG_PATH;
    QString target = default_path;

    ret = pCon->copyDatabaseDir(source, target);
    if(ret < 0)
    {
        qDebug() << source << "=>>" << target;
        qDebug() << "copyDatabaseDir ret =" << ret;
        return false;
    }*/

    // 3. setup info file.
    QStringList list;
    list.append("0");
    list.append("default");
    list.append(QDateTime::currentDateTime().date().toString(Qt::ISODate));

    QString info_path = default_path + RM_INFO_FILENAME;

    ret = pCon->createProgramFile(info_path, list);
    if(ret < 0)
    {
        qDebug() << "createProgramFile ret =" << ret;
        return false;
    }

    qDebug() << "MakeDefault Success!";
    return true;
}

bool HyRecipeManage2::SetDefault_Info(QString ver, QDateTime date)
{
    QString default_path = RM_RECIPE_PATH;
    default_path += RM_DEFAULT_DIRNAME;

    QStringList list;
    list.clear();

    list.append(ver);
    list.append(date.toString(Qt::ISODate));

    QString info_path = default_path + RM_INFO_FILENAME;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    ret = pCon->createProgramFile(info_path, list);
    if(ret < 0)
    {
        qDebug() << "createProgramFile ret =" << ret;
        return false;
    }

    return true;
}

bool HyRecipeManage2::SetDefault_Var()
{
    // tp var => export default dir.
    QString default_path = RM_RECIPE_PATH;
    default_path += RM_DEFAULT_DIRNAME;

    QString path = default_path + RM_VAR_FILENAME;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    ret = pCon->exportVariable(path);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "exportVariable ret =" << ret;
        return false;
    }

    return true;
}

bool HyRecipeManage2::IsDefaultProgram(QString prgName)
{
    QString default_path = RM_RECIPE_PATH;
    default_path += RM_DEFAULT_DIRNAME;
    QString path = default_path + RM_PROG_DIRNAME;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    QStringList default_prog_list;
    ret = pCon->getDatabaseFileList(path, default_prog_list);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "getDatabaseFileList ret =" << ret;
        return false;
    }

    int index = default_prog_list.indexOf(prgName);
    if(index < 0)
        return false;

    return true;
}

bool HyRecipeManage2::SetDefault_Program(QString prgName)
{
    if(!IsDefaultProgram(prgName))
        return false;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    QStringList prog_step_list;
    ret = pCon->getProgramSteps(prgName, prog_step_list);
    if(ret < 0)
    {
        qDebug() << "getDatabaseFileList ret =" << ret;
        return false;
    }

    QString default_path = RM_RECIPE_PATH;
    default_path += RM_DEFAULT_DIRNAME;
    QString path = default_path + RM_PROG_DIRNAME;
    path += "/";
    path += prgName;

    ret = pCon->createProgramFile(path, prog_step_list);
    if(ret < 0)
    {
        qDebug() << "createProgramFile ret =" << ret;
        return false;
    }

    return true;
}

bool HyRecipeManage2::SetDefault_Program()
{
    QString default_path = RM_RECIPE_PATH;
    default_path += RM_DEFAULT_DIRNAME;
    QString path = default_path + RM_PROG_DIRNAME;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    QStringList default_prog_list;
    ret = pCon->getDatabaseFileList(path, default_prog_list);
    if(ret < 0)
    {
        qDebug() << path;
        qDebug() << "getDatabaseFileList ret =" << ret;
        return false;
    }

    for(int i=0;i<default_prog_list.count();i++)
    {
        if(SetDefault_Program(default_prog_list[i]))
            qDebug() << default_prog_list[i] << "-SetDefault Success!";
        else
            qDebug() << default_prog_list[i] << "-SetDefault Fail!";
    }

    return true;
}

bool HyRecipeManage2::CallLine2MacroName(QString &str)
{
    QStringList temp_list;

    temp_list = str.split("(");
    str = temp_list[0].remove("Call").trimmed();

    return true;
}
