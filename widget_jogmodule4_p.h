#ifndef WIDGET_JOGMODULE4_P_H
#define WIDGET_JOGMODULE4_P_H

#include <QWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

namespace Ui {
class widget_jogmodule4_p;
}

class widget_jogmodule4_p : public QWidget
{
    Q_OBJECT

public:
    explicit widget_jogmodule4_p(QWidget *parent = 0);
    ~widget_jogmodule4_p();

public slots:
    void onJogType();
    void onSpeedUp();
    void onSpeedDown();
    void onJogInch();

    void onTimer();

    void onH5Jog();

private:
    Ui::widget_jogmodule4_p *ui;

    QVector<QLabel3*> m_vtInch;
    QVector<QLabel4*> m_vtInchPic;

    int m_nJogSpeed;

    void SetJogInch(bool on);
    void SetInch(int i, bool on);

    QTimer* timer;
    void SetSpeed(int speed);
    void GetSpeed(int& speed);

    void Display_Tool(bool on);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // WIDGET_JOGMODULE4_P_H
