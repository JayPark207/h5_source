#include "hyparam.h"

HyParam::HyParam()
{
    Init_String();
}

void HyParam::Init_String()
{
    QString str;
    m_Name.clear();
    int i;
    for(i=0;i<PARAMS_NUM;i++)
    {
        str = QString("Config %1").arg(i);
        m_Name.append(str);
    }

    //
    m_Name[FIRST_SETTING] = QString(tr("First Setting"));
    m_Name[LANGUAGE] = QString(tr("Language"));
    m_Name[RECIPE] = QString(tr("Recipe"));
    // add here.
    m_Name[MAIN_SPEED] = QString(tr("Main Speed"));
    m_Name[USER_LEVEL] = QString(tr("User Level"));
    for(i=0;i<NUM_USER_LEVEL;i++)
        m_Name[LV0_PW+i] = QString(tr("Password Level %1")).arg(i);
    m_Name[START_DATE] = QString(tr("Factory-In Date"));
    m_Name[JIG_POS] = QString(tr("Don't Touch(Jig Pos.)"));
    m_Name[SAMPLE_POS] = QString(tr("Don't Touch(Sample Pos.)"));
    m_Name[FAULTY_POS] = QString(tr("Don't Touch(Faulty Pos.)"));
    m_Name[MOTOR_TUNE_DATA] = QString(tr("Don't Touch(Tune Data)"));
    m_Name[USESKIP] = QString(tr("Use/Skip"));
    m_Name[SOFT_SENSOR_MIN] = QString(tr("Soft Sensor Min"));
    m_Name[SOFT_SENSOR_MAX] = QString(tr("Soft Sensor Max"));
    m_Name[VAR_MONITOR_NAME] = QString(tr("Data Monitor Name"));
    m_Name[RECENTLY_ERROR] = QString(tr("Recently 10 Error"));
    m_Name[SUBTASK_AUTORUN] = QString(tr("Sub Task Auto Run"));
    m_Name[SUBTASK_MACRO] = QString(tr("Sub Task Macro Name"));


}

bool HyParam::Read()
{
    CNRobo* pCon = CNRobo::getInstance();

    m_Param.clear();
    int ret = pCon->getUserParams(m_Param);
    if(ret < 0)
    {
        qDebug() << "getUserParams Fail!! Code =" << ret;
        return false;
    }

    if(m_Param.size() < HyParam::PARAMS_NUM)
    {
        // add new data case.
        int num = HyParam::PARAMS_NUM - m_Param.size();
        for(int i=0;i<num;i++)
            m_Param.push_back("0");

        pCon->setUserParams(m_Param);
    }

    return true;
}

bool HyParam::Write()
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->setUserParams(m_Param);
    if(ret < 0)
    {
        qDebug() << "setUserParams Fail!! Code =" << ret;
        return false;
    }
    return true;
}

void HyParam::Set(PARAMS conf, QString data, bool bWrite)
{
    if(m_Param.size() <= conf)
        return;

    m_Param[conf] = data;

    if(bWrite)
        Write();
}
void HyParam::Set(PARAMS conf, float data, bool bWrite)
{
    QString str;
    str.sprintf("%f",data);

    Set(conf, str, bWrite);
}
void HyParam::Set(PARAMS conf, int data, bool bWrite)
{
    QString str;
    str.sprintf("%d",data);

    Set(conf, str, bWrite);
}


QString HyParam::Get(PARAMS conf)
{
    if(m_Param.size() <= conf)
        return "";

    return m_Param[conf];
}

QString HyParam::GetName(PARAMS conf)
{
    if(m_Param.size() <= conf)
        return "";

    return m_Name[conf];
}

void HyParam::Set(PARAMS conf, cn_trans trans, cn_joint joint, float speed, float delay, bool bWrite)
{
    QString str,result;
    str.clear();result.clear();

    // trans
    str.sprintf("%f,%f,%f,%f,%f,%f,",
                trans.p[0],trans.p[1],trans.p[2],trans.eu[0],trans.eu[1],trans.eu[2]);
    result = str;
    // joint
    str.clear();
    for(int i=0;i<USE_AXIS_NUM;i++)
    {
        str += QString().setNum(joint.joint[i]);
        str += QString(",");
    }
    result += str;
    // speed, delay
    str.clear();
    str.sprintf("%f,%f", speed, delay);
    result += str;

    qDebug() << result;

    Set(conf, result, bWrite);
}

bool HyParam::Get(PARAMS conf, cn_trans &trans, cn_joint &joint, float &speed, float &delay)
{
    if(m_Param.size() <= conf)
        return false;

    QStringList list = m_Param[conf].split(",");

    if(list.size() <= 1)
        return false;

    int i;
    // trans = always 6
    for(i=0;i<3;i++)
    {
        if(list.isEmpty()) return false;
        trans.p[i] = list.takeFirst().toFloat();
    }
    for(i=0;i<3;i++)
    {
        if(list.isEmpty()) return false;
        trans.eu[i] = list.takeFirst().toFloat();
    }

    // joint = changable (USE_AXIS_NUM)
    for(i=0;i<USE_AXIS_NUM;i++)
    {
        if(list.isEmpty()) return false;
        joint.joint[i] = list.takeFirst().toFloat();
    }

    if(list.isEmpty()) return false;
    speed = list.takeFirst().toFloat();
    if(list.isEmpty()) return false;
    delay = list.takeFirst().toFloat();

    return list.isEmpty();
}

void HyParam::Set(PARAMS conf, cn_joint joint, float speed, float delay, float accel, float decel, bool bWrite)
{
    QString str,result;
    str.clear();result.clear();

    // joint
    str.clear();
    for(int i=0;i<USE_AXIS_NUM;i++)
    {
        str += QString().setNum(joint.joint[i]);
        str += QString(",");
    }
    result += str;
    // speed, delay
    str.clear();
    str.sprintf("%f,%f,%f,%f", speed, delay, accel, decel);
    result += str;

    qDebug() << result;

    Set(conf, result, bWrite);
}

bool HyParam::Get(PARAMS conf, cn_joint &joint, float &speed, float &delay, float& accel, float& decel)
{
    if(m_Param.size() <= conf)
        return false;

    QStringList list = m_Param[conf].split(",");

    if(list.size() <= 1)
        return false;

    int i;
    // joint = changable (USE_AXIS_NUM)
    for(i=0;i<USE_AXIS_NUM;i++)
    {
        if(list.isEmpty()) return false;
        joint.joint[i] = list.takeFirst().toFloat();
    }

    if(list.isEmpty()) return false;
    speed = list.takeFirst().toFloat();
    if(list.isEmpty()) return false;
    delay = list.takeFirst().toFloat();
    if(list.isEmpty()) return false;
    accel = list.takeFirst().toFloat();
    if(list.isEmpty()) return false;
    decel = list.takeFirst().toFloat();

    return list.isEmpty();
}
