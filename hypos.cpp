#include "hypos.h"

HyPos::HyPos(HyRecipe *recipe)
{
    m_Recipe = recipe;

    cn_trans temp;
    temp.p[0] = 0.;temp.p[1] = 0.;temp.p[2] = 0.;       //X,Y,Z
    temp.eu[0] = 0.;temp.eu[1] = 0.;temp.eu[2] = 0.;    //A,B,C

    cn_joint _joint;
    memset(_joint.joint,0,sizeof(float)*CN_MAX_JOINT);

    Trans.clear();
    Trans.fill(temp, POS_MAX_NUM);
    Joint.clear();
    Joint.fill(_joint, POS_MAX_NUM);
    Speed.clear();
    Speed.fill(0.0, POS_MAX_NUM);
    Delay.clear();
    Delay.fill(0.0, POS_MAX_NUM);
    Name.clear();
    Name.fill("", POS_MAX_NUM);

    m_strArrName_TPos = QString("tpos"); // fix
    m_strArrName_JPos = QString("jpos"); // fix
    m_strArrName_Speed = QString("spd"); // fix
    m_strArrName_Delay = QString("pos_delay"); // fix

    Init();
}

void HyPos::Init()
{
    // name internationality,

    Name[ZERO]          = QString(tr("Mechanic Zero"));
    Name[HOME]          = QString(tr("Home1"));
    Name[JIG_CHANGE]    = QString(tr("Jig Change"));
    Name[SAMPLE]        = QString(tr("Sampling"));
    Name[FAULTY]        = QString(tr("Faulty Unload"));

    Name[WAIT]          = QString(tr("Wait"));
    Name[EXT_WAIT]      = QString(tr("Extenal Wait"));
    Name[TAKE_OUT]      = QString(tr("Take-out"));
    for(int i=0;i<MAX_UNDERCUT_POS;i++)
        Name[UNDERCUT1 + i] = QString(tr("Under-Cut %1")).arg(i+1);
    Name[UP1ST]         = QString(tr("Up Comp."));
    Name[UNLOAD]        = QString(tr("Unload Start"));
    Name[WEIGHT]        = QString(tr("Weight Measure"));
    Name[INSERT]        = QString(tr("Insert Start"));
    Name[INSERT_UNLOAD] = QString(tr("Insert Unload"));
    Name[STOPOVER1]     = QString(tr("StopOver"));
    Name[STOPOVER2]     = QString(tr("StopOver"));
    Name[UNLOAD_MID]    = QString(tr("Unload Middle"));
    Name[UNLOAD2]       = QString(tr("Unload Start 2"));
    Name[CLOSE_WAIT]    = QString(tr("Close Wait"));
    Name[INSERT_MID]    = QString(tr("Insert Middle"));
    Name[STOPOVER3]     = QString(tr("StopOver"));
    // add here...

}

// for dev
void HyPos::AllZero()
{
    cn_variant value;
    memset(value.val.joint.joint,0,sizeof(float)*CN_MAX_JOINT);
    cn_trans temp;
    temp.p[0] = 0.;temp.p[1] = 0.;temp.p[2] = 0.;       //X,Y,Z
    temp.eu[0] = 0.;temp.eu[1] = 0.;temp.eu[2] = 0.;    //A,B,C

    for(int i=0;i<POS_MAX_NUM;i++)
    {
        WR(i, temp, value.val.joint, 0.0, 0.0);
    }
}

QString HyPos::GetName(int i)
{
    return Name.at(i);
}

/** direct access case **/
bool HyPos::WR(int index, cn_trans trans, cn_joint joint, float speed, float delay)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();
    QString str;
    cn_variant var;

    // trans
    str = m_strArrName_TPos;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_TRANS;
    var.val.trans = trans;
    values.append(var);
    // joint
    str = m_strArrName_JPos;
    str.push_front("#");
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_JOINT;
    var.val.joint = joint;
    values.append(var);
    // speed
    str = m_strArrName_Speed;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_FLOAT;
    var.val.f32 = speed;
    values.append(var);
    // delay
    str = m_strArrName_Delay;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_FLOAT;
    var.val.f32 = delay;
    values.append(var);

    if(!m_Recipe->SetVars(var_names, values))
        return false;

    return true;
}
bool HyPos::WR_nvs(int index, cn_trans trans, cn_joint joint, float speed, float delay)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();
    QString str;
    cn_variant var;

    // trans
    str = m_strArrName_TPos;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_TRANS;
    var.val.trans = trans;
    values.append(var);
    // joint
    str = m_strArrName_JPos;
    str.push_front("#");
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_JOINT;
    var.val.joint = joint;
    values.append(var);
    // speed
    str = m_strArrName_Speed;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_FLOAT;
    var.val.f32 = speed;
    values.append(var);
    // delay
    str = m_strArrName_Delay;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_FLOAT;
    var.val.f32 = delay;
    values.append(var);

    if(!m_Recipe->SetVars(var_names, values,false))
        return false;

    return true;
}
bool HyPos::WR(int index, cn_trans trans, cn_joint joint)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();
    QString str;
    cn_variant var;

    // trans
    str = m_strArrName_TPos;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_TRANS;
    var.val.trans = trans;
    values.append(var);
    // joint
    str = m_strArrName_JPos;
    str.push_front("#");
    str += QString("[%1]").arg(index);
    var_names.append(str);
    var.type = CNVAR_JOINT;
    var.val.joint = joint;
    values.append(var);

    if(!m_Recipe->SetVars(var_names, values))
        return false;

    return true;
}

bool HyPos::WR_Pos(int i, cn_trans trans)
{
    if(!m_Recipe->SetArr(m_strArrName_TPos, i, trans))
        return false;
    return true;
}
bool HyPos::WR_Pos(int i, cn_joint joint)
{
    if(!m_Recipe->SetArr(m_strArrName_JPos, i, joint))
        return false;
    return true;
}
bool HyPos::WR_Speed(int i, float speed)
{
    if(!m_Recipe->SetArr(m_strArrName_Speed, i, speed))
        return false;
    return true;
}
bool HyPos::WR_Delay(int i, float delay)
{
    if(!m_Recipe->SetArr(m_strArrName_Delay, i, delay))
        return false;
    return true;
}

bool HyPos::RD(int index, cn_trans& trans, cn_joint& joint, float& speed, float& delay)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();
    QString str;

    // trans
    str = m_strArrName_TPos;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    // joint
    str = m_strArrName_JPos;
    str.push_front("#");
    str += QString("[%1]").arg(index);
    var_names.append(str);
    // speed
    str = m_strArrName_Speed;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    // delay
    str = m_strArrName_Delay;
    str += QString("[%1]").arg(index);
    var_names.append(str);

    if(!m_Recipe->GetVars(var_names, values))
        return false;

    if(values[0].type >= 0)
        trans = values[0].val.trans;
    if(values[1].type >= 0)
        joint = values[1].val.joint;
    if(values[2].type >= 0)
        speed = values[2].val.f32;
    if(values[3].type >= 0)
        delay = values[3].val.f32;

    return true;
}

bool HyPos::RD(int index, cn_trans& trans, cn_joint& joint)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();
    QString str;

    // trans
    str = m_strArrName_TPos;
    str += QString("[%1]").arg(index);
    var_names.append(str);
    // joint
    str = m_strArrName_JPos;
    str.push_front("#");
    str += QString("[%1]").arg(index);
    var_names.append(str);

    if(!m_Recipe->GetVars(var_names, values))
        return false;

    if(values[0].type >= 0)
        trans = values[0].val.trans;
    if(values[1].type >= 0)
        joint = values[1].val.joint;

    return true;
}

bool HyPos::RD_Pos(int i, cn_trans *trans)
{
    if(!m_Recipe->GetArr(m_strArrName_TPos, i, trans))
        return false;
    return true;
}
bool HyPos::RD_Pos(int i, cn_joint *joint)
{
    if(!m_Recipe->GetArr(m_strArrName_JPos, i, joint))
        return false;
    return true;
}
bool HyPos::RD_Speed(int i, float *speed)
{
    if(!m_Recipe->GetArr(m_strArrName_Speed, i, speed))
        return false;
    return true;
}
bool HyPos::RD_Delay(int i, float *delay)
{
    if(!m_Recipe->GetArr(m_strArrName_Delay, i, delay))
        return false;
    return true;
}


/** In-direct access case **/

bool HyPos::Read()
{
    for(int i=0;i<POS_MAX_NUM;i++)
    {
        if(!RD(i, Trans[i], Joint[i], Speed[i], Delay[i]))
            return false;
    }
    return true;
}
bool HyPos::Read(int i)
{
    if(!RD(i, Trans[i], Joint[i], Speed[i], Delay[i]))
        return false;
    return true;
}
bool HyPos::Write()
{
    for(int i=0;i<POS_MAX_NUM;i++)
    {
        if(!WR(i,Trans[i], Joint[i], Speed[i], Delay[i]))
            return false;
    }
    return true;
}
bool HyPos::Write(int i)
{
    if(!WR(i,Trans[i], Joint[i], Speed[i], Delay[i]))
        return false;
    return true;
}

void HyPos::Set_Trans(int i, cn_trans data)
{
    Trans[i] = data;
}
cn_trans HyPos::Get_Trans(int i)
{
    return Trans.at(i);
}
void HyPos::Set_Joint(int i, cn_joint data)
{
    Joint[i] = data;
}
cn_joint HyPos::Get_Joint(int i)
{
    return Joint.at(i);
}
void HyPos::Set_Speed(int i, float percent)
{
    Speed[i] = percent;
}
float HyPos::Get_Speed(int i)
{
    return Speed.at(i);
}
void HyPos::Set_Delay(int i, float time)
{
    Delay[i] = time;
}
float HyPos::Get_Delay(int i)
{
    return Delay.at(i);
}

/** Trans2Joint & Joint2Trans **/
bool HyPos::Trans2Joint(cn_trans in_trans, cn_joint old_joint, cn_joint &new_joint)
{
    qDebug() << "Trans2Joint() Try!";
    qDebug() << "Input Trans Value";
    qDebug() << "X=" <<in_trans.p[0] << "Y=" << in_trans.p[1] << "Z=" << in_trans.p[2];
    qDebug() << "A=" << in_trans.eu[0] << "B=" << in_trans.eu[1] << "C=" << in_trans.eu[2];
    qDebug() << "old Joint Value";
    qDebug() << old_joint.joint[0] << old_joint.joint[1] << old_joint.joint[2]
             << old_joint.joint[3] << old_joint.joint[4] << old_joint.joint[5];

    CNRobo* pCon = CNRobo::getInstance();
    cn_conf confIn;
    confIn.cfx = 0x00;
    memset(confIn.cfang,0,sizeof(unsigned char)*3);
    memset(confIn.cfmaster,0,sizeof(unsigned char)*4);

    int ret = pCon->calcInverse(in_trans, old_joint.joint, new_joint.joint, confIn);
    if(ret < 0)
    {
        qDebug() << "Trans2Joint >> calcInverse() ret = " << ret;
        return false;
    }

    qDebug() << "Trans2Joint Success!";
    qDebug() << "new Joint Value";
    qDebug() << new_joint.joint[0] << new_joint.joint[1] << new_joint.joint[2]
             << new_joint.joint[3] << new_joint.joint[4] << new_joint.joint[5];

    return true;
}

bool HyPos::Joint2Trans(cn_joint in_joint, cn_trans &new_trans)
{
    qDebug() << "Joint2Trans() Try!";

    qDebug() << "Input joint value";
    qDebug() << in_joint.joint[0] << in_joint.joint[1] << in_joint.joint[2]
             << in_joint.joint[3] << in_joint.joint[4] << in_joint.joint[5];

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->calcForward(in_joint.joint, new_trans);
    if(ret < 0)
    {
        qDebug() << "Joint2Trans() >> calcForward() ret = " << ret;
        return false;
    }

    qDebug() << "Joint2Trans Success!";
    qDebug() << "new Trans Value";
    qDebug() << "X=" << new_trans.p[0] << "Y=" << new_trans.p[1] << "Z=" << new_trans.p[2];
    qDebug() << "A=" << new_trans.eu[0] << "B=" << new_trans.eu[1] << "C=" << new_trans.eu[2];

    return true;
}

bool HyPos::Set_UndercutSpeed(int start_num, int total_num, float speed, bool save)
{
    QStringList var_names;
    QList<cn_variant> values;

    var_names.clear();
    values.clear();
    QString str;
    cn_variant var;

    if(start_num < 0) start_num = 0;
    else if(start_num > MAX_UNDERCUT_POS) start_num = MAX_UNDERCUT_POS;
    if(total_num < 1) total_num = 1;
    else if(total_num > MAX_UNDERCUT_POS) total_num = MAX_UNDERCUT_POS;

    if(start_num >= total_num)
        return true;

    for(int i=start_num;i<total_num;i++)
    {
        str = m_strArrName_Speed;
        str += QString("[%1]").arg(UNDERCUT1 + i);
        var_names.append(str);

        var.type = CNVAR_FLOAT;
        var.val.f32 = speed;
        values.append(var);
    }

    if(!m_Recipe->SetVars(var_names, values, save))
        return false;

    return true;
}
