#include "dialog_position_teaching2.h"
#include "ui_dialog_position_teaching2.h"

dialog_position_teaching2::dialog_position_teaching2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_position_teaching2)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    wmJog = new widget_jogmodule3(ui->grpJog);

    // Realcount update timer.
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    // confirm button event
    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtPos.clear();m_vtPosPic.clear();
    m_vtPos.append(ui->btnXval);m_vtPosPic.append(ui->btnXvalPic);
    m_vtPos.append(ui->btnYval);m_vtPosPic.append(ui->btnYvalPic);
    m_vtPos.append(ui->btnZval);m_vtPosPic.append(ui->btnZvalPic);
    m_vtPos.append(ui->btnRval);m_vtPosPic.append(ui->btnRvalPic);
    m_vtPos.append(ui->btnSval);m_vtPosPic.append(ui->btnSvalPic);
    m_vtPos.append(ui->btnJ6val);m_vtPosPic.append(ui->btnJ6valPic);

    m_vtReal.clear();m_vtRealPic.clear();
    m_vtReal.append(ui->btnXreal);m_vtRealPic.append(ui->btnXrealPic);
    m_vtReal.append(ui->btnYreal);m_vtRealPic.append(ui->btnYrealPic);
    m_vtReal.append(ui->btnZreal);m_vtRealPic.append(ui->btnZrealPic);
    m_vtReal.append(ui->btnRreal);m_vtRealPic.append(ui->btnRrealPic);
    m_vtReal.append(ui->btnSreal);m_vtRealPic.append(ui->btnSrealPic);
    m_vtReal.append(ui->btnJ6real);m_vtRealPic.append(ui->btnJ6realPic);

    m_vtPosName.clear();
    m_vtPosName.append(ui->lbX);
    m_vtPosName.append(ui->lbY);
    m_vtPosName.append(ui->lbZ);
    m_vtPosName.append(ui->btnR);
    m_vtPosName.append(ui->btnS);
    m_vtPosName.append(ui->btnJ6);

    int i;
    for(i=0;i<m_vtPos.count();i++)
    {
        connect(m_vtPosPic[i],SIGNAL(mouse_release()),this,SLOT(onPos()));
        connect(m_vtPos[i],SIGNAL(mouse_press()),m_vtPosPic[i],SLOT(press()));
        connect(m_vtPos[i],SIGNAL(mouse_release()),m_vtPosPic[i],SLOT(release()));

        connect(m_vtRealPic[i],SIGNAL(mouse_release()),this,SLOT(onReal()));
        connect(m_vtReal[i],SIGNAL(mouse_press()),m_vtRealPic[i],SLOT(press()));
        connect(m_vtReal[i],SIGNAL(mouse_release()),m_vtRealPic[i],SLOT(release()));
    }

    connect(ui->btnRotPic,SIGNAL(mouse_release()),this,SLOT(onRotation()));
    connect(ui->btnR,SIGNAL(mouse_press()),ui->btnRotPic,SLOT(press()));
    connect(ui->btnR,SIGNAL(mouse_release()),ui->btnRotPic,SLOT(release()));

    connect(ui->btnSwvPic,SIGNAL(mouse_release()),this,SLOT(onSwivel()));
    connect(ui->btnS,SIGNAL(mouse_press()),ui->btnSwvPic,SLOT(press()));
    connect(ui->btnS,SIGNAL(mouse_release()),ui->btnSwvPic,SLOT(release()));


    connect(ui->btnColPic2,SIGNAL(mouse_release()),this,SLOT(onCurr()));
    connect(ui->btnCol2,SIGNAL(mouse_press()),ui->btnColPic2,SLOT(press()));
    connect(ui->btnCol2,SIGNAL(mouse_release()),ui->btnColPic2,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnReset,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnReset,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnSpdPic,SIGNAL(mouse_release()),this,SLOT(onSpeed()));
    connect(ui->btnSpd,SIGNAL(mouse_press()),ui->btnSpdPic,SLOT(press()));
    connect(ui->btnSpd,SIGNAL(mouse_release()),ui->btnSpdPic,SLOT(release()));
    connect(ui->btnSpdIcon,SIGNAL(mouse_press()),ui->btnSpdPic,SLOT(press()));
    connect(ui->btnSpdIcon,SIGNAL(mouse_release()),ui->btnSpdPic,SLOT(release()));

    connect(ui->btnDelayPic,SIGNAL(mouse_release()),this,SLOT(onDelay()));
    connect(ui->btnDelay,SIGNAL(mouse_press()),ui->btnDelayPic,SLOT(press()));
    connect(ui->btnDelay,SIGNAL(mouse_release()),ui->btnDelayPic,SLOT(release()));
    connect(ui->btnDelayIcon,SIGNAL(mouse_press()),ui->btnDelayPic,SLOT(press()));
    connect(ui->btnDelayIcon,SIGNAL(mouse_release()),ui->btnDelayPic,SLOT(release()));

    connect(ui->btnMovePic1,SIGNAL(mouse_release()),this,SLOT(onMoveType()));
    connect(ui->btnMoveIcon1,SIGNAL(mouse_press()),ui->btnMovePic1,SLOT(press()));
    connect(ui->btnMoveIcon1,SIGNAL(mouse_release()),ui->btnMovePic1,SLOT(release()));

    connect(ui->btnMovePic2,SIGNAL(mouse_release()),this,SLOT(onMoveType()));
    connect(ui->btnMoveIcon2,SIGNAL(mouse_press()),ui->btnMovePic2,SLOT(press()));
    connect(ui->btnMoveIcon2,SIGNAL(mouse_release()),ui->btnMovePic2,SLOT(release()));

    connect(ui->btnCallPic,SIGNAL(mouse_release()),this,SLOT(onCall()));
    connect(ui->btnCall,SIGNAL(mouse_press()),ui->btnCallPic,SLOT(press()));
    connect(ui->btnCall,SIGNAL(mouse_release()),ui->btnCallPic,SLOT(release()));
    connect(ui->btnCallIcon,SIGNAL(mouse_press()),ui->btnCallPic,SLOT(press()));
    connect(ui->btnCallIcon,SIGNAL(mouse_release()),ui->btnCallPic,SLOT(release()));


    Init_String();
    // thinking.... later change or not.
    for(i=0;i<m_vtRealPic.count();i++)
    {
        m_vtRealPic[i]->hide();
        m_vtRealPic[i]->setEnabled(false);
    }

    /*
    m_vtPosPic[0]->hide();
    m_vtPosPic[0]->setEnabled(false);
    m_vtPosPic[1]->hide();
    m_vtPosPic[1]->setEnabled(false);
    m_vtPosPic[2]->hide();
    m_vtPosPic[2]->setEnabled(false);
    m_vtPosPic[3]->hide();
    m_vtPosPic[3]->setEnabled(false);
    m_vtPosPic[4]->hide();
    m_vtPosPic[4]->setEnabled(false);
    */
    ui->btnRotPic->hide();
    ui->btnRotPic->setEnabled(false);
    ui->btnSwvPic->hide();
    ui->btnSwvPic->setEnabled(false);
    ui->grpMoveType->hide();

    // pointer clear
    top = 0;
    dig_call_position = 0;

}

dialog_position_teaching2::~dialog_position_teaching2()
{
    delete ui;
}
void dialog_position_teaching2::showEvent(QShowEvent *)
{

#ifdef _H6_
    ui->btnJ6->setText(tr("J6"));
    ui->btnJ6real->setEnabled(true);
    ui->btnJ6val->setEnabled(false);
    ui->btnJ6valPic->setEnabled(false);

    ui->btnRval->setEnabled(false);
    ui->btnRvalPic->setEnabled(false);
    ui->btnSval->setEnabled(false);
    ui->btnSvalPic->setEnabled(false);
#else
    ui->btnJ6->setText("");
    ui->btnJ6real->setEnabled(false);
    ui->btnJ6val->setEnabled(false);
    ui->btnJ6valPic->setEnabled(false);

    ui->btnRval->setEnabled(true);
    ui->btnRvalPic->setEnabled(true);
    ui->btnSval->setEnabled(true);
    ui->btnSvalPic->setEnabled(true);
#endif


    Update();

    timer->start(200);

    gReadyJog(true);

    SubTop();

    m_nUserLevel = Param->Get(HyParam::USER_LEVEL).toInt();
}
void dialog_position_teaching2::hideEvent(QHideEvent *)
{
    timer->stop();

    gReadyJog(false);
}
void dialog_position_teaching2::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
        Init_String();
    }
}

void dialog_position_teaching2::Init_String()
{
    m_strDelayUnit = QString(tr("sec"));
    m_strSpdUnit = QString("%");
}

void dialog_position_teaching2::onOK()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(tr("Save"));
    conf->Message(tr("Do you want to save?"), ui->lbNowPos->text());
    if(conf->exec() != QDialog::Accepted)
        return;

    if(!Save())
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Save Error!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    emit accept();
}
void dialog_position_teaching2::onCancel()
{
    emit reject();
}

void dialog_position_teaching2::Init(int pos_index)
{
    m_nPosIndex = pos_index;
    m_nType = BASE;
}
void dialog_position_teaching2::Init(ENUM_TYPE type, int pos_index)
{
    m_nType = type;
    m_nPosIndex = pos_index;
}

void dialog_position_teaching2::Update()
{

    QString _name;
    cn_trans _pos;

    float _speed,_delay,_type;
    int _user_pos_index;
    cn_joint _joint;

    if(m_nType == USER)
    {
        ST_USERDATA_POS _userpos;
        // for display
        _user_pos_index = HyStepData::ADD_POS0 + m_nPosIndex;
        _name = StepEdit->m_StepData.Step[_user_pos_index].DispName;

        if(!StepEdit->m_StepData.Step[_user_pos_index].NickName.isEmpty())
            _name += " ("+ StepEdit->m_StepData.Step[_user_pos_index].NickName + ")";

        if(!UserData->RD(m_nPosIndex, &_userpos))
            return;

        _pos = _userpos.Trans;
        _joint = _userpos.Joint;
        _speed = _userpos.Speed;
        _delay = _userpos.Delay;
        _type = _userpos.Type;
    }
    else if(m_nType == WORK)
    {
        ST_USERDATA_WORK _userwork;
        // for display
        _user_pos_index = HyStepData::ADD_WORK0 + m_nPosIndex;
        _name = StepEdit->m_StepData.Step[_user_pos_index].DispName;

        if(!StepEdit->m_StepData.Step[_user_pos_index].NickName.isEmpty())
            _name += " ("+ StepEdit->m_StepData.Step[_user_pos_index].NickName + ")";

        if(!UserData->RD(m_nPosIndex, &_userwork))
            return;

        _pos = _userwork.Trans;
        _joint = _userwork.Joint;
        _speed = _userwork.Speed;
        _delay = _userwork.Delay;
        _type = _userwork.Type;
    }
    else // BASE
    {
        _name = Posi->GetName(m_nPosIndex);

        if(!Posi->RD(m_nPosIndex, _pos, _joint, _speed, _delay))
            return;

        _type = 0;
    }

    // type setting visible control
    ui->grpMoveType->setVisible(m_nType != BASE);

    m_TransDataOld = _pos;
    m_TransData = _pos;

    m_JointDataOld = _joint;
    m_JointData = _joint;

    m_SpdDataOld = _speed;
    m_SpdData = _speed;

    m_DelayDataOld = _delay;
    m_DelayData = _delay;

    m_TypeDataOld = _type;
    m_TypeData = _type;


    QString str;

    ui->lbNowPos->setText(_name);

    Display_SetPos(m_TransData, m_JointData);

    str.sprintf(FORMAT_SPEED, m_SpdData);
    str.append(m_strSpdUnit);
    ui->btnSpd->setText(str);

    str.sprintf(FORMAT_TIME, m_DelayData);
    str.append(m_strDelayUnit);
    ui->btnDelay->setText(str);

    // here user pos only - move type control
    SetMoveType((ENUM_MOVE_TYPE)((int)_type));

    // table select enable control
    ui->btnSelectPic->hide();


    qDebug() << "Trans Value";
    qDebug() << "X=" <<m_TransData.p[0] << "Y=" << m_TransData.p[1] << "Z=" << m_TransData.p[2];
    qDebug() << "A=" << m_TransData.eu[0] << "B=" << m_TransData.eu[1] << "C=" << m_TransData.eu[2];

    for(int i=0;i<USE_AXIS_NUM;i++)
        qDebug("Axis[%d] = %.2f", i, m_JointData.joint[i]);
}

void dialog_position_teaching2::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    float* trans = pCon->getCurTrans();
    float* joint = pCon->getCurJoint();

    Display_CurPos(trans, joint);

    for(int i=0;i<3;i++)
    {
        m_RealTransData.p[i] = trans[i];
        m_RealTransData.eu[i] = trans[3+i];
    }
    memcpy(m_RealJointData.joint,joint,sizeof(float) * USE_AXIS_NUM);

    Control_SaveBtn();
}

void dialog_position_teaching2::onPos()
{

    QLabel4* sel = (QLabel4*)sender();
    int i = m_vtPosPic.indexOf(sel);
    if(i < 0) return;

    cn_trans _trans;
    cn_joint _joint;
    float value;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    np->m_numpad->SetTitle(m_vtPosName[i]->text());
    np->m_numpad->SetNum(m_vtPos[i]->text().toDouble());
    value = m_vtPos[i]->text().toFloat() - POS_VALUE_SET_RANGE;
    np->m_numpad->SetMinValue((double)value);
    value = m_vtPos[i]->text().toFloat() + POS_VALUE_SET_RANGE;
    np->m_numpad->SetMaxValue((double)value);
    np->m_numpad->SetSosuNum(SOSU_POS);

    if(np->exec() == QDialog::Accepted)
    {
        //@@@rot
        if(i<JROT)
        {
            _trans = m_TransData;
            _trans.p[i] = (float)np->m_numpad->GetNumDouble();
            if(Posi->Trans2Joint(_trans, m_JointData, _joint))
            {
                //OK, result value insert variable.
                m_TransData = _trans;
                m_JointData = _joint;
            }
            else
            {
                //Error Message
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
            }
        }
        else
        {
            _joint = m_JointData;

#ifdef _H6_
            //_joint.joint[i] = (float)np->m_numpad->GetNumDouble();
            _joint.joint[i] = gGetDisp2Joint(i, (float)np->m_numpad->GetNumDouble(), _joint);
#else
            if(i == JROT)
            {
                float curr_wrist_angle = gGetWristAngle(_joint);
                float curr_input_angel = (float)np->m_numpad->GetNumDouble();
                float diff_angle = curr_input_angel - curr_wrist_angle;

                _joint.joint[i] += diff_angle;
            }
            else
                _joint.joint[i] = (float)np->m_numpad->GetNumDouble();
#endif

            if(Posi->Joint2Trans(_joint, _trans))
            {
                // OK, result value insert variable
                m_JointData = _joint;
                m_TransData = _trans;
            }
            else
            {
                //Error Message
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Value Transfer Error!"),
                             tr("Please, Try again!"));
                msg->exec();
            }
        }

        Display_SetPos(m_TransData, m_JointData);
    }
}
void dialog_position_teaching2::onReal() // no use
{
    // no use
    QLabel4* sel = (QLabel4*)sender();
    if(sel->isHidden())
        return;

    QString btn = sender()->objectName();

    for(int i=0;i<m_vtRealPic.count();i++)
    {
        if(btn == m_vtRealPic[i]->objectName())
        {
            // only display write. no save database.
            m_vtPos[i]->setText(m_vtReal[i]->text());

            //@@@rot (no use)
            if(i<JROT)
                m_TransData.p[i] = m_vtReal[i]->text().toFloat();
            else
                m_JointData.joint[i] = m_vtReal[i]->text().toFloat();
        }
    }
}
void dialog_position_teaching2::onCurr()
{
    QLabel4* sel = (QLabel4*)sender();
    if(sel->isHidden())
        return;

    for(int i=0;i<m_vtPos.count();i++)
    {
        // only display write. no save database.
        m_vtPos[i]->setText(m_vtReal[i]->text());
    }

    m_TransData = m_RealTransData;
    m_JointData = m_RealJointData;


    qDebug() << "Trans Value";
    qDebug() << "X=" <<m_TransData.p[0] << "Y=" << m_TransData.p[1] << "Z=" << m_TransData.p[2];
    qDebug() << "A=" << m_TransData.eu[0] << "B=" << m_TransData.eu[1] << "C=" << m_TransData.eu[2];

    for(int i=0;i<USE_AXIS_NUM;i++)
        qDebug("Axis[%d] = %.2f", i, m_JointData.joint[i]);

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(QString(tr("Setting")));
    msg->CloseTime(1000);
    msg->Message(tr("Set this position!"));
    msg->exec();

}

void dialog_position_teaching2::onSpeed()
{
    QString str;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(tr("Speed[%]"));
    np->m_numpad->SetNum((double)m_SpdData);

    if(m_nUserLevel < USER_LEVEL_HIGH)
        np->m_numpad->SetMinValue(1.0);
    else
        np->m_numpad->SetMinValue(0);

    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);

    if(np->exec() == QDialog::Accepted)
    {
        m_SpdData = (float)np->m_numpad->GetNumDouble();
        str.sprintf(FORMAT_SPEED, m_SpdData);
        str.append(m_strSpdUnit);
        ui->btnSpd->setText(str);
    }
}
void dialog_position_teaching2::onDelay()
{
    QString str;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(tr("Delay[sec]"));
    np->m_numpad->SetNum((double)m_DelayData);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(1000.0);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        m_DelayData = (float)np->m_numpad->GetNumDouble();
        str.sprintf(FORMAT_TIME, m_DelayData);
        str.append(m_strDelayUnit);
        ui->btnDelay->setText(str);
    }
}

bool dialog_position_teaching2::Save()
{
    if(m_nType == USER)
    {
        ST_USERDATA_POS _userpos;
        UserData->SetBuf_Default(&_userpos);
        _userpos.Trans = m_TransData;
        _userpos.Joint = m_JointData;
        _userpos.Speed = m_SpdData;
        _userpos.Delay = m_DelayData;
        // add move type
        _userpos.Type = (float)m_nMoveType;

        if(!UserData->WR(m_nPosIndex, _userpos))
            return false;
    }
    else if(m_nType == WORK)
    {
        ST_USERDATA_WORK _userwork;
        UserData->SetBuf_Default(&_userwork);
        _userwork.Trans = m_TransData;
        _userwork.Joint = m_JointData;
        _userwork.Speed = m_SpdData;
        _userwork.Delay = m_DelayData;
        // add move type
        _userwork.Type = (float)m_nMoveType;

        if(!UserData->WR(m_nPosIndex, _userwork))
            return false;
    }
    else
    {
        if(!Posi->WR(m_nPosIndex, m_TransData, m_JointData, m_SpdData, m_DelayData))
            return false;

        // working at only common postion index.
        Save_CommonPos(m_TransData, m_JointData, m_SpdData, m_DelayData);
    }

    return true;
}

void dialog_position_teaching2::Save_CommonPos(cn_trans trans, cn_joint joint, float speed, float delay)
{

    if(m_nPosIndex == HyPos::JIG_CHANGE)
        Param->Set(HyParam::JIG_POS,trans,joint,speed,delay);
    else if(m_nPosIndex == HyPos::SAMPLE)
        Param->Set(HyParam::SAMPLE_POS,trans,joint,speed,delay);
    else if(m_nPosIndex == HyPos::FAULTY)
        Param->Set(HyParam::FAULTY_POS,trans,joint,speed,delay);
}

void dialog_position_teaching2::Reset()
{
    m_TransData = m_TransDataOld;
    m_JointData = m_JointDataOld;
    Display_SetPos(m_TransData, m_JointData);

    QString str;

    m_SpdData = m_SpdDataOld;
    str.sprintf(FORMAT_SPEED, m_SpdData);
    str.append(m_strSpdUnit);
    ui->btnSpd->setText(str);

    m_DelayData = m_DelayDataOld;
    str.sprintf(FORMAT_TIME, m_DelayData);
    str.append(m_strDelayUnit);
    ui->btnDelay->setText(str);

    // add move type;
    m_TypeData = m_TypeDataOld;
    SetMoveType((ENUM_MOVE_TYPE)((int)m_TypeData));
}

void dialog_position_teaching2::onReset()
{
    Reset();
}

void dialog_position_teaching2::onRotation() //no use
{
    QLabel4* sel = (QLabel4*)sender();
    if(sel->isHidden())
        return;

    dialog_rotation* dig = new dialog_rotation();

    dig->Init(dialog_rotation::ROTATION, m_vtPos[3]->text());
    if(dig->exec() == QDialog::Accepted)
    {
        // only display write. no save database.
        m_vtPos[3]->setText(dig->GetValue());
    }
}
void dialog_position_teaching2::onSwivel() // no use
{
    QLabel4* sel = (QLabel4*)sender();
    if(sel->isHidden())
        return;

    dialog_rotation* dig = new dialog_rotation();

    dig->Init(dialog_rotation::SWIVEL, m_vtPos[4]->text());
    if(dig->exec() == QDialog::Accepted)
    {
        // only display write. no save database.
        m_vtPos[4]->setText(dig->GetValue());
    }
}

// for only USER type
void dialog_position_teaching2::onMoveType() // no use
{
    QString btn = sender()->objectName();

    if(btn == ui->btnMovePic1->objectName())
        SetMoveType(TYPE_SEQ);
    else if(btn == ui->btnMovePic2->objectName())
        SetMoveType(TYPE_ALL);
}

void dialog_position_teaching2::SetMoveType(ENUM_MOVE_TYPE move_type)
{

    m_nMoveType = move_type;
    m_TypeData = (float)move_type;

    if(move_type == TYPE_SEQ)
    {
        ui->btnMoveIcon1->setAutoFillBackground(true);
        ui->btnMoveIcon2->setAutoFillBackground(false);
    }
    else
    {
        ui->btnMoveIcon1->setAutoFillBackground(false);
        ui->btnMoveIcon2->setAutoFillBackground(true);
    }
}

void dialog_position_teaching2::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}

void dialog_position_teaching2::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtPos[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtPos[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtPos[2]->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    m_vtPos[3]->setText(str);
    str.sprintf(FORMAT_POS ,gGetJoint2Disp(JSWV, j));
    m_vtPos[4]->setText(str);
    str.sprintf(FORMAT_POS ,gGetJoint2Disp(J6, j));
    m_vtPos[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtPos[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtPos[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtPos[2]->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    m_vtPos[3]->setText(str);
    str.sprintf(FORMAT_POS ,j.joint[JSWV]);
    m_vtPos[4]->setText(str);

    m_vtPos[5]->setText(""); // no use j6
#endif

}
void dialog_position_teaching2::Display_CurPos(float* t, float* j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, t[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, t[UPDN]);
    m_vtReal[2]->setText(str);

    cn_joint _joint = gMakeCnJoint(j);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, _joint));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, _joint));
    m_vtReal[4]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, _joint));
    m_vtReal[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, t[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, t[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, t[UPDN]);
    m_vtReal[2]->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(j[J2], j[J3], j[J4]));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, j[JSWV]);
    m_vtReal[4]->setText(str);

    m_vtReal[5]->setText(""); // no use j6
#endif

}

void dialog_position_teaching2::onCall()
{
    if(dig_call_position == 0)
        dig_call_position = new dialog_call_position();

    if(dig_call_position->exec() == QDialog::Accepted)
    {
        m_TransData = dig_call_position->GetTrans();
        m_JointData = dig_call_position->GetJoint();
        Display_SetPos(m_TransData, m_JointData);
    }
}

void dialog_position_teaching2::Control_SaveBtn()
{
    bool bChange = false;

    int i;
    for(i=0;i<3;i++)
    {
        if(m_TransDataOld.p[i] != m_TransData.p[i])
            bChange = true;
        if(m_TransDataOld.eu[i] != m_TransData.eu[i])
            bChange = true;
    }

    for(i=0;i<USE_AXIS_NUM;i++)
    {
        if(m_JointDataOld.joint[i] != m_JointData.joint[i])
            bChange = true;
    }

    if(m_SpdDataOld != m_SpdData)
        bChange = true;

    if(m_DelayDataOld != m_DelayData)
        bChange = true;

    if(m_TypeDataOld != m_TypeData)
        bChange = true;

    ui->wigOK->setEnabled(bChange);
}
