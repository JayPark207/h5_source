#ifndef DIALOG_PICTUREVIEW_H
#define DIALOG_PICTUREVIEW_H

#include <QDialog>
#include <QMovie>

#include "global.h"

#define PICVIEW_BASE_PATH "/sdcard/HYPicture"

namespace Ui {
class dialog_pictureview;
}

class dialog_pictureview : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_pictureview(QWidget *parent = 0);
    ~dialog_pictureview();

    void Init(QString recipe_no);
    void Show();

private:
    Ui::dialog_pictureview *ui;

    QString m_strRecipeNo;

    void View_DefaultPic();
    bool View_Pic(QString path);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_PICTUREVIEW_H
