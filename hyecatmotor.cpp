#include "hyecatmotor.h"

HyEcatMotor::HyEcatMotor()
{
    // important variable initialization.
    UNI_STATUSWORD status;
    status.ushort = 0x0000;
    m_Status.clear();
    m_Status.fill(status,MAX_ECAT_NODE);
    UNI_CONTROLWORD control;
    control.ushort = 0x0000;
    control.bit.qs = 1; // NC
    m_Control.clear();
    m_Control.fill(control, MAX_ECAT_NODE);
    m_ErrorCode.clear();
    m_ErrorCode.fill(0x0000, MAX_ECAT_NODE);
    m_Realcount.clear();
    m_Realcount.fill(0, MAX_ECAT_NODE);
    m_Localcount.clear();
    m_Localcount.fill(0, MAX_ECAT_NODE);
    m_OverloadRate.clear();
    m_OverloadRate.fill(0x0000, MAX_ECAT_NODE);
    m_TorqueDemand.clear();
    m_TorqueDemand.fill(0x0000, MAX_ECAT_NODE);
    m_TorqueActual.clear();
    m_TorqueActual.fill(0x0000, MAX_ECAT_NODE);

}

// need cyclic triggering. (ex. timer)
void HyEcatMotor::Update(int motor_id)
{
    // read & write.
    Read(motor_id);
    //Write(motor_id);
}
void HyEcatMotor::Update()
{
    //Read();

    for(int i=0;i<USE_AXIS_NUM;i++)
        Read(i);
}
/*void HyEcatMotor::Read() // use this fuction. (realcount version)
{
    int param[2];
    param[0] = 0x603F0010;   // error code.
    param[1] = 0x60640020;   // position actual value (realcount)

    unsigned long datas[20];
    int datas_num;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->getSDOList(param, 2, datas, datas_num);
    if(ret < 0)
    {
        qDebug() << "getSDOList fail!! ret =" << ret;
        return;
    }
    int* status = pCon->getPDOT_StatusWord();

    // data mapping
    for(int i=0;i<USE_AXIS_NUM;i++)
    {
        m_Status[i].ushort = (USHORT)(status[i] & 0x0000FFFF);
        m_ErrorCode[i] = (USHORT)(datas[(i*2)] & 0x0000FFFF);
        m_Realcount[i] = (int)datas[(i*2)+1];
        m_Localcount[i] = 0;
    }
}*/
void HyEcatMotor::Read() // no use this fuction. (overload version)
{
    int param[2];
    param[0] = 0x603F0010;   // error code.
    param[1] = 0x4D290010;   // over load factor (0.1%)

    unsigned long datas[20];
    int datas_num;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->getSDOList(param, 2, datas, datas_num);
    if(ret < 0)
    {
        qDebug() << "getSDOList fail!! ret =" << ret;
        return;
    }
    int* status = pCon->getPDOT_StatusWord();

    // data mapping
    for(int i=0;i<USE_AXIS_NUM;i++)
    {
        m_Status[i].ushort = (USHORT)(status[i] & 0x0000FFFF);
        m_ErrorCode[i] = (USHORT)(datas[(i*2)] & 0x0000FFFF);
        m_OverloadRate[i] = (USHORT)(datas[(i*2)+1] & 0x0000FFFF);
        m_Realcount[i] = 0;
        m_Localcount[i] = 0;
    }
}

void HyEcatMotor::Debug_StatusWord()
{
    QString str;
    for(int i=0;i<USE_AXIS_NUM;i++)
    {
        str.sprintf("%04Xh", m_Status[i].ushort);
        qDebug() << "statusword" << "[" << i << "]" << str;

        str.sprintf("%04Xh", m_Control[i].ushort);
        qDebug() << "controlword" << "[" << i << "]" << str;
    }
}

/*void HyEcatMotor::Read(int motor_id)
{
    int param[4];
    param[0] = 0x60410010;   // statusword
    param[1] = 0x603F0010;   // error code.
    param[2] = 0x60620020;   // position demand value (localcount)
    param[3] = 0x60640020;   // position actual value (realcount)

    unsigned long data[4];
    memset(data, 0, sizeof(data));

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->getSDO(motor_id, param, 4, data);
    if(ret < 0)
    {
        qDebug() << "getSDO fail!! ret =" << ret;
        return;
    }
    // data mapping
    m_Status[motor_id].ushort = (USHORT)(data[0] & 0x0000FFFF);
    m_ErrorCode[motor_id] = (USHORT)(data[1] & 0x0000FFFF);
    m_Localcount[motor_id] = (int)data[2];
    m_Realcount[motor_id] = (int)data[3];

    // error reset trigger rising edge, so no-fault clear.
    if(!m_Status[motor_id].bit.f)
    {
        if(m_Control[motor_id].bit.fr)
            m_Control[motor_id].bit.fr = 0;
    }
}*/

void HyEcatMotor::Read(int motor_id) // use this function (overload version).
{
    int param[4];
    param[0] = 0x60770010;   // statusword(0x60410010) -> actual torque(0x60770010)
    param[1] = 0x603F0010;   // error code.
    param[2] = 0x4D290010;   // overload rate
    param[3] = 0x60740010;   // position actual value (realcount)

    unsigned long data[4];
    memset(data, 0, sizeof(data));

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->getSDO(motor_id, param, 4, data);
    if(ret < 0)
    {
        qDebug() << "getSDO fail!! ret =" << ret;
        return;
    }
    // data mapping
    m_Status[motor_id].ushort = (USHORT)(data[0] & 0x0000FFFF);
    m_ErrorCode[motor_id] = (USHORT)(data[1] & 0x0000FFFF);
    m_Localcount[motor_id] = 0;
    m_Realcount[motor_id] = 0;
    m_OverloadRate[motor_id] = (USHORT)(data[2] & 0x0000FFFF);
    m_TorqueDemand[motor_id] = (SHORT)(data[3] & 0x0000FFFF);
    m_TorqueActual[motor_id] = (SHORT)(data[0] & 0x0000FFFF);

    // error reset trigger rising edge, so no-fault clear.
    //if(!m_Status[motor_id].bit.f)
    //{
    //    if(m_Control[motor_id].bit.fr)
    //        m_Control[motor_id].bit.fr = 0;
    //}
}

void HyEcatMotor::Write(int motor_id)
{
    int param[2];
    param[0] = 0x60400010;   // controlword
    param[1] = 0;
    unsigned long data[2];
    data[0] = (unsigned long)m_Control[motor_id].ushort & 0x0000FFFF;
    data[1] = 0;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setSDO(motor_id, param, 1, data);
    if(ret < 0)
    {
        qDebug() << "setSDO fail!! ret =" << ret;
        return;
    }
}

bool HyEcatMotor::IsServo(int motor_id)
{
    if(m_Status[motor_id].bit.so)
        return true;
    return false;
}

bool HyEcatMotor::IsPower(int motor_id)
{
    if(m_Status[motor_id].bit.ve)
        return true;
    return false;
}

bool HyEcatMotor::IsError(int motor_id)
{
    if(m_Status[motor_id].bit.f)
        return true;
    return false;
}

bool HyEcatMotor::IsWarning(int motor_id)
{
    if(m_Status[motor_id].bit.w)
        return true;
    return false;
}

bool HyEcatMotor::IsLimit(int motor_id)
{
    if(m_Status[motor_id].bit.ila)
        return true;
    return false;
}

USHORT HyEcatMotor::GetErrorCode(int motor_id)
{
    return m_ErrorCode[motor_id];
}

int HyEcatMotor::GetLocalCount(int motor_id)
{
    return m_Localcount[motor_id];
}

int HyEcatMotor::GetRealCount(int motor_id)
{
    return m_Realcount[motor_id];
}

USHORT HyEcatMotor::GetOverloadRate(int motor_id)
{
    return m_OverloadRate[motor_id];
}
SHORT HyEcatMotor::GetTorqueDemand(int motor_id)
{
    return m_TorqueDemand[motor_id];
}
SHORT HyEcatMotor::GetTorqueActual(int motor_id)
{
    return m_TorqueActual[motor_id];
}


void HyEcatMotor::Servo(int motor_id, bool on)
{
    if(on)
    {
        m_Control[motor_id].bit.so = 1;
        m_Control[motor_id].bit.ev = 1;
        m_Control[motor_id].bit.qs = 1;
        m_Control[motor_id].bit.eo = 1;
    }
    else
    {
        m_Control[motor_id].bit.eo = 0;
    }

    Write(motor_id);
}

void HyEcatMotor::Reset(int motor_id)
{
    m_Control[motor_id].bit.fr = 1;

    Write(motor_id);
}

bool HyEcatMotor::Ready_MultiTurnClear(int motor_id)
{
    int param[2];
    param[0] = 0x4D010010;  // special function setting 9
    param[1] = 0x4D000120;  // special function start flag 1
    unsigned long data[2];
    data[0] = 0x0031;
    data[1] = 0x00000200;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setSDO(motor_id, param, 1, data);
    if(ret < 0)
    {
        qDebug() << "Ready_MultiTurnClear fail!! ret =" << ret;
        return false;
    }
    return true;
}
bool HyEcatMotor::Set_MultiTurnClear(int motor_id)
{
    int param[2];
    param[0] = 0x4D000120;  // special function start flag 1
    param[1] = 0x4D000120;  // special function setting 9
    unsigned long data[2];
    data[0] = 0x00000200;
    data[1] = 0;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setSDO(motor_id, param, 1, data);
    if(ret < 0)
    {
        qDebug() << "Set_MultiTurnClear fail!! ret =" << ret;
        return false;
    }
    return true;
}

bool HyEcatMotor::Reset_MultiTurnClear(int motor_id)
{
    int param[2];
    param[0] = 0x4D000120;  // special function start flag 1
    param[1] = 0x4D000120;  // special function setting 9
    unsigned long data[2];
    data[0] = 0;
    data[1] = 0;

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setSDO(motor_id, param, 1, data);
    if(ret < 0)
    {
        qDebug() << "Reset_MultiTurnClear fail!! ret =" << ret;
        return false;
    }
    return true;
}

bool HyEcatMotor::Set_OverloadLevel(int motor_id, int value)
{
    if(value < 0) value = 0;
    else if(value > 115) value = 115;

    int _param = 0x35120010;
    unsigned long _data = (unsigned long)value;
    if(!SetSDO(motor_id, _param, _data))
        return false;
    return true;
}

bool HyEcatMotor::Get_OverloadLevel(int motor_id, int& value)
{
    if(value < 0) value = 0;
    else if(value > 115) value = 115;

    int _param = 0x35120010;
    unsigned long _data;
    if(!GetSDO(motor_id, _param, _data))
        return false;

    value = (int)_data;
    return true;
}

void HyEcatMotor::Save_EEPROM(int motor_id)
{
    int _param = 0x10100120;
    unsigned long _data = 0x65766173;
    SetSDO(motor_id, _param, _data);
    qDebug() << "Completed Save_EEPROM process";
}

/** single SDO set&get **/
bool HyEcatMotor::GetSDO(int motor_id, int param, unsigned long& data)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->getSDO(motor_id, &param, 1, &data);
    if(ret < 0)
    {
        data = 0;
        return false;
    }
    return true;
}
bool HyEcatMotor::SetSDO(int motor_id, int param, unsigned long data)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setSDO(motor_id, &param, 1, &data);
    if(ret < 0)
        return false;

    return true;
}
/** single SDO set&get END **/
