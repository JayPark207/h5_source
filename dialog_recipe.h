#ifndef DIALOG_RECIPE_H
#define DIALOG_RECIPE_H

#include <QDialog>
#include <QTimer>
#include <QElapsedTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_keyboard.h"
#include "dialog_message.h"
#include "dialog_confirm.h"
#include "dialog_pictureview.h"
#include "dialog_common_position.h"
#include "dialog_backup.h"
#include "dialog_delaying.h"

#include "hyui_longpress.h"

#include "top_sub.h"

namespace Ui {
class dialog_recipe;
}

class dialog_recipe : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_recipe(QWidget *parent = 0);
    ~dialog_recipe();

    void Update();

private:
    void Redraw_List(int start_index);
    void Display_RawColor(int raw_index);

    bool CheckNameRule(QString name);
    void Display_MaxNo();
    void Display_Mark(int now_list_index);

    void Enable_BtnBackup(bool enable);

    void Stop_SubTask();
    void Start_SubTask();

private slots:
    void onClose();
    void onTimer();

    void onSelect();

    void onUp();
    void onDown();
    void onUpLong();
    void onDownLong();
    void onPress_UpDown();
    void onRelease_UpDown();

    void onLoad();
    void onNew();
    void onDelete();
    void onCopy();
    void onRename();

    void onPicture();
    void onBackup();
    void onJigChange();
    void onRefresh();   // refresh & sync.

    void on_pushButton_clicked();
    void onHiddenFunc();

private:
    Ui::dialog_recipe *ui;
    QTimer* timer;

    // important variable
    int m_nStartIndex;      // for display table list
    int m_nSelectedIndex;  // for access recipe list buffer.

    bool m_bDataError;

    // for ui vectors.
    QVector<QLabel3*> m_vtNo;
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtDate;
    QVector<QLabel3*> m_vtMark;

    // auto refresh
    int m_nRefreshCount;
    bool m_bRefreshCheck;

    // top page
    top_sub* top;
    void SubTop();          // Need SubTop

    QElapsedTimer ti;

    HyUi_LongPress* uiLongPressUp;
    HyUi_LongPress* uiLongPressDown;

    HyUi_LongPress* uiLongPressHidden;

    int m_nUserLevel;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // DIALOG_RECIPE_H
