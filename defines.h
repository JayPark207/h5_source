#ifndef DEFINES
#define DEFINES
#define GIT_TEST 12346778
// version
#ifdef _H6_
#define MODEL " (H6)"
#else
#define MODEL " (H5)"
#endif

#define GUI_VER     "3.11"
#define SEQ_VER     "0.00" // no use. in seq program.

#define MAX_ECAT_NODE   32

// common
#define USHORT  unsigned short
#define SHORT   short
#define UINT    unsigned int
#define INT     int
#define BYTE    char

// 7L vs 7H
#ifdef _CG4300_
// for UI.
#define MAX_WIDTH   1024
#define MAX_HEIGHT  600
#define TOP_HEIGHT  100

#define ETHERNET_NAME "eth0"

#define SD_EXIST_PATH       "/dev/mmcblk2"
#define USB_EXIST_PATH      "/dev/sda1"

#elif _CG3300_
// for UI.
#define MAX_WIDTH   800
#define MAX_HEIGHT  480
#define TOP_HEIGHT  80

#define ETHERNET_NAME "eth1"

#define SD_EXIST_PATH       "/dev/sdcard"
#define USB_EXIST_PATH      "/dev/udisk"

#endif

// for Task
#define MAIN_TASK   0
#define SUBMAIN_MACRO "sub_main"
#define SUB_TASK    110
#define SUB_TASK2   111
#define SUB_TASK3   112
#define SUB_TASK4   113
#define SUB_TASK_NUM 4


// for Motion.
#define MAX_AXIS_NUM 6

#ifdef _H6_
    #define USE_AXIS_NUM 6
#else
    #define USE_AXIS_NUM 5
#endif

#define TRANS_DATA_NUM  6

#define MAX_MAIN_SPEED      100
#define MIN_MAIN_SPEED      5
#define JUMP_MAIN_SPEED     5   //[%]

#define MAX_JOG_SPEED       30  //[%]
#define MIN_JOG_SPEED       5   //[%]
#define JUMP_JOG_SPEED      5

#define POS_VALUE_SET_RANGE     50.0 // [mm]

#define MAX_POS_VALUE       100000.0
#define MIN_POS_VALUE       -100000.0

// trans buffer data
#define TRAV                0
#define FWDBWD              1
#define UPDN                2
#define ROT                 1   // trans.eu[ROT]
#define SWV                 2   // trans.eu[SWV]
#define EU                  0   // no use

#define J1                  0
#define J2                  1
#define J3                  2
#define J4                  3
#define J5                  4
#define JROT                3
#define JSWV                4
#define J6                  5

#define TX                   0
#define TY                   1
#define TZ                   2
#define TA                   0
#define TB                   1
#define TC                   2

#define MIN_TIME_SET      0.0
#define MAX_TIME_SET      1000.0

#define USER_POS_KNOW_OFFSET   1000000

// user data max. number
#define MAX_USER_STEP_POS   100
#define MAX_USER_STEP_OUT   100
#define MAX_USER_STEP_IN    50
#define MAX_USER_STEP_TIME  32
#define MAX_USER_STEP_WORK  50


// User Level (0 ~ 9)
#define MIN_USER_LEVEL      0
#define MAX_USER_LEVEL      9
#define NUM_USER_LEVEL      10
#define USER_LEVEL_LOW           0   // operator
#define USER_LEVEL_MID           3   // engineer
#define USER_LEVEL_HIGH          7   // pro.
#define USER_LEVEL_DEV           9


// number display formats
#define FORMAT_POS          "%.02f"
#define FORMAT_SPEED        "%.01f"
#define FORMAT_TIME         "%.02f"
#define SOSU_POS            2
#define SOSU_SPEED          1
#define SOSU_TIME           2


// Recipe manage
// ext = sdcard
// max. inner save number over => sdcard save.

#define RM_BASE_PATH        "/mnt/mtd5"             //fixed
#define RM_PROG_PATH        "/mnt/mtd5/programs"    //fixed
#define RM_SD_BASE_PATH     "/sdcard"               //fixed
#define RM_USB_BASE_PATH    "/udisk"                //fixed

//#define RM_RECIPE_PATH      "/mnt/mtd6/Recipe"
#define RM_RECIPE_PATH      "/mnt/mtd5/Recipe"

#define RM_SD_RECIPE_PATH   "/sdcard/HYRecipe"
#define RM_DEFAULT_DIRNAME  "/default"              //fixed
#define RM_INFO_FILENAME    "/info"                 //fixed
#define RM_VAR_FILENAME     "/db.var"               //fixed
#define RM_PROG_DIRNAME     "/programs"             //fixed
#define RM_RECIPE_DIRNAME   "/Recipe"
#define RM_SD_RECIPE_DIRNAME "/HYRecipe"

#define RM_MAX_RECIPE_INNER_NUM    200     // over -> go to sdcard.
#define RM_DATE_FORMAT      "yyyy.MM.dd"

// Backup & Recovery
#define BACKUP_USB_PATH             "/udisk"
#define BACKUP_DIR_NAME             "/HYBackup"
#define BACKUP_PARAM_FILENAME       "/RobotParameter"
#define BACKUP_CONFIG_FILENAME      "/RobotConfig"

// Program Management
#define PM_USB_PROG_PATH         "/udisk/HYPrograms"

// Sasang
//#define PATH_SASANG_BASE "/mnt/mtd6/Sasang"
#define PATH_SASANG_BASE "/mnt/mtd5/Sasang"

// Big Mode
//#define PATH_BMODE_DATA_BASE "/mnt/mtd6/Mode/Data"
#define PATH_BMODE_DATA_BASE "/mnt/mtd5/Mode/Data"

//#define PATH_BMODE_IMAGE_BASE "/mnt/mtd6/Mode/Image"
#define PATH_BMODE_IMAGE_BASE "/mnt/mtd5/Mode/Image"

#define EXT_BMODE_DATA ".bmd"

// macro progarms
#define MACRO_MAIN      "main"

// position.
#define MAX_UNDERCUT_POS    16

// unload & insert array
#define MAX_ARR_X_NUM   100
#define MAX_ARR_Y_NUM   100
#define MAX_ARR_Z_NUM   100

// servo on mode.
#define SON_MODE_NORMAL     0
#define SON_MODE_SPECIAL    1

// error code
#define PATH_ERROR_CODE "/mnt/mtd5/hy_errorcode"

#endif // DEFINES

