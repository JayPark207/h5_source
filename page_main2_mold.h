#ifndef PAGE_MAIN2_MOLD_H
#define PAGE_MAIN2_MOLD_H

#include <QWidget>
#include "ui_page_main2_mold.h"
#include "global.h"

namespace Ui {
class page_main2_mold;
}

class page_main2_mold : public QWidget
{
    Q_OBJECT
    
public:
    explicit page_main2_mold(QWidget *parent = 0);
    ~page_main2_mold();

private:
    Ui::page_main2_mold *ui;

protected:
    void changeEvent(QEvent *);
};

#endif // PAGE_MAIN2_MOLD_H
