#include "dialog_motor_test.h"
#include "ui_dialog_motor_test.h"

dialog_motor_test::dialog_motor_test(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_motor_test)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    wmJog = new widget_jogmodule3(ui->grpJog);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnServoPic,SIGNAL(mouse_release()),this,SLOT(onServo()));
    connect(ui->btnServoIcon,SIGNAL(mouse_press()),ui->btnServoPic,SLOT(press()));
    connect(ui->btnServoIcon,SIGNAL(mouse_release()),ui->btnServoPic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    m_vtSel.clear();m_vtSelIcon.clear();
    m_vtSel.append(ui->btnSel);  m_vtSelIcon.append(ui->btnSelIcon);
    m_vtSel.append(ui->btnSel_2);m_vtSelIcon.append(ui->btnSelIcon_2);
    m_vtSel.append(ui->btnSel_3);m_vtSelIcon.append(ui->btnSelIcon_3);
    m_vtSel.append(ui->btnSel_4);m_vtSelIcon.append(ui->btnSelIcon_4);
    m_vtSel.append(ui->btnSel_5);m_vtSelIcon.append(ui->btnSelIcon_5);
    m_vtSel.append(ui->btnSel_6);m_vtSelIcon.append(ui->btnSelIcon_6);
    m_vtSel.append(ui->btnSel_7);m_vtSelIcon.append(ui->btnSelIcon_7);
    m_vtSel.append(ui->btnSel_8);m_vtSelIcon.append(ui->btnSelIcon_8);

    m_vtJNo.clear();m_vtType.clear();
    m_vtJNo.append(ui->lbJ);  m_vtType.append(ui->lbType);
    m_vtJNo.append(ui->lbJ_2);m_vtType.append(ui->lbType_2);
    m_vtJNo.append(ui->lbJ_3);m_vtType.append(ui->lbType_3);
    m_vtJNo.append(ui->lbJ_4);m_vtType.append(ui->lbType_4);
    m_vtJNo.append(ui->lbJ_5);m_vtType.append(ui->lbType_5);
    m_vtJNo.append(ui->lbJ_6);m_vtType.append(ui->lbType_6);
    m_vtJNo.append(ui->lbJ_7);m_vtType.append(ui->lbType_7);
    m_vtJNo.append(ui->lbJ_8);m_vtType.append(ui->lbType_8);

    m_vtDist.clear();m_vtDistPic.clear();
    m_vtDist.append(ui->btnDist);  m_vtDistPic.append(ui->btnDistPic);
    m_vtDist.append(ui->btnDist_2);m_vtDistPic.append(ui->btnDistPic_2);
    m_vtDist.append(ui->btnDist_3);m_vtDistPic.append(ui->btnDistPic_3);
    m_vtDist.append(ui->btnDist_4);m_vtDistPic.append(ui->btnDistPic_4);
    m_vtDist.append(ui->btnDist_5);m_vtDistPic.append(ui->btnDistPic_5);
    m_vtDist.append(ui->btnDist_6);m_vtDistPic.append(ui->btnDistPic_6);
    m_vtDist.append(ui->btnDist_7);m_vtDistPic.append(ui->btnDistPic_7);
    m_vtDist.append(ui->btnDist_8);m_vtDistPic.append(ui->btnDistPic_8);

    m_vtReal.clear();
    m_vtReal.append(ui->lbReal);
    m_vtReal.append(ui->lbReal_2);
    m_vtReal.append(ui->lbReal_3);
    m_vtReal.append(ui->lbReal_4);
    m_vtReal.append(ui->lbReal_5);
    m_vtReal.append(ui->lbReal_6);
    m_vtReal.append(ui->lbReal_7);
    m_vtReal.append(ui->lbReal_8);

    // each param setting.
    int i;
    for(i=0;i<m_vtDistPic.count();i++)
    {
        connect(m_vtDistPic[i],SIGNAL(mouse_release()),this,SLOT(onDist()));
        connect(m_vtDist[i],SIGNAL(mouse_press()),m_vtDistPic[i],SLOT(press()));
        connect(m_vtDist[i],SIGNAL(mouse_release()),m_vtDistPic[i],SLOT(release()));
    }

    connect(ui->btnSpdPic,SIGNAL(mouse_release()),this,SLOT(onSpd()));
    connect(ui->btnSpd,SIGNAL(mouse_press()),ui->btnSpdPic,SLOT(press()));
    connect(ui->btnSpd,SIGNAL(mouse_release()),ui->btnSpdPic,SLOT(release()));

    connect(ui->btnAccPic,SIGNAL(mouse_release()),this,SLOT(onAcc()));
    connect(ui->btnAcc,SIGNAL(mouse_press()),ui->btnAccPic,SLOT(press()));
    connect(ui->btnAcc,SIGNAL(mouse_release()),ui->btnAccPic,SLOT(release()));

    connect(ui->btnDecPic,SIGNAL(mouse_release()),this,SLOT(onDec()));
    connect(ui->btnDec,SIGNAL(mouse_press()),ui->btnDecPic,SLOT(press()));
    connect(ui->btnDec,SIGNAL(mouse_release()),ui->btnDecPic,SLOT(release()));

    connect(ui->btnIntervalPic,SIGNAL(mouse_release()),this,SLOT(onInterval()));
    connect(ui->btnInterval,SIGNAL(mouse_press()),ui->btnIntervalPic,SLOT(press()));
    connect(ui->btnInterval,SIGNAL(mouse_release()),ui->btnIntervalPic,SLOT(release()));

    // move joint select
    for(i=0;i<m_vtSel.count();i++)
    {
        connect(m_vtSel[i],SIGNAL(mouse_release()),this,SLOT(onSel()));
        connect(m_vtSelIcon[i],SIGNAL(mouse_release()),m_vtSel[i],SIGNAL(mouse_release()));
    }

    // realcount display.
    connect(ui->btnRealPic,SIGNAL(mouse_release()),this,SLOT(onReal()));
    connect(ui->btnReal,SIGNAL(mouse_press()),ui->btnRealPic,SLOT(press()));
    connect(ui->btnReal,SIGNAL(mouse_release()),ui->btnRealPic,SLOT(release()));

    ui->grpReal->setGeometry(ui->grpParam->x(),
                             ui->grpParam->y(),
                             ui->grpParam->width(),
                             ui->grpParam->height());
    ui->grpReal->hide();
    ui->grpParam->show();

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    // move
    connect(ui->btnMovePic,SIGNAL(mouse_release()),this,SLOT(onMove()));
    connect(ui->btnMove,SIGNAL(mouse_press()),ui->btnMovePic,SLOT(press()));
    connect(ui->btnMove,SIGNAL(mouse_release()),ui->btnMovePic,SLOT(release()));
    connect(ui->btnMoveIcon,SIGNAL(mouse_press()),ui->btnMovePic,SLOT(press()));
    connect(ui->btnMoveIcon,SIGNAL(mouse_release()),ui->btnMovePic,SLOT(release()));

    // continue checkbox
    connect(ui->lbBox,SIGNAL(mouse_release()),this,SLOT(onContinue()));
    connect(ui->lbCheck,SIGNAL(mouse_release()),this,SLOT(onContinue()));
    connect(ui->lbCBText,SIGNAL(mouse_release()),this,SLOT(onContinue()));

    // Move Mode checkbox
    connect(ui->lbMoveMode,SIGNAL(mouse_release()),this,SLOT(onMoveMode()));
    connect(ui->lbMoveModeCheck,SIGNAL(mouse_release()),this,SLOT(onMoveMode()));

    top = 0;
    pxmServo.clear();
    pxmMove.clear();

}
dialog_motor_test::~dialog_motor_test()
{
    delete ui;
}
void dialog_motor_test::showEvent(QShowEvent *)
{
    SetMoveMode(false);

    Update();

    timer->start();

    SubTop();

    SetContinue(false);
}
void dialog_motor_test::hideEvent(QHideEvent *)
{
    gReadyJog(false);
    gSetJogType(JOG_TYPE_HY);

    timer->stop();
}
void dialog_motor_test::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_motor_test::onClose()
{
    Save_Param_Data();
    accept();
}

void dialog_motor_test::Update()
{
    int i;

    // Read Robot config.
    RobotConfig->Read();
    RobotConfig->Get(QString("AXIS_TYPE"), m_Types);
    RobotConfig->Get(QString("AXIS_LIMITP"), m_PLimits);
    RobotConfig->Get(QString("AXIS_LIMITM"), m_MLimits);

    Read_Param_Data();

    // table initial.
    for(i=0;i<m_vtJNo.count();i++)
    {
        if(i < USE_AXIS_NUM)
        {
            //JNo from conf.
            m_vtJNo[i]->setText(QString("J%1").arg(i+1));
            //Type from conf.
            if(i < m_Types.size())
                m_vtType[i]->setText(m_Types[i]);

            m_vtDist[i]->setEnabled(true);
            m_vtDistPic[i]->setEnabled(true);
            m_vtSel[i]->setEnabled(true);
            m_vtSelIcon[i]->setEnabled(true);
            m_vtJNo[i]->setEnabled(true);
            m_vtType[i]->setEnabled(true);
        }
        else
        {
            m_vtJNo[i]->setText(QString(""));
            m_vtType[i]->setText(QString(""));

            m_vtDist[i]->setEnabled(false);
            m_vtDistPic[i]->setEnabled(false);
            m_vtSel[i]->setEnabled(false);
            m_vtSelIcon[i]->setEnabled(false);
            m_vtJNo[i]->setEnabled(false);
            m_vtType[i]->setEnabled(false);
        }
        // Sel is all Clear.
        m_vtSelIcon[i]->hide();
    }

    Redraw_Display();

    Write_Recipe();
}

void dialog_motor_test::Redraw_Display()
{
    QString str;
    int i;
    for(i=0;i<m_vtJNo.count();i++)
    {
        if(i < USE_AXIS_NUM)
        {
            //distance
            str.sprintf(FORMAT_POS, m_Dist.joint[i]);
            m_vtDist[i]->setText(str);
        }
        else
        {
            m_vtDist[i]->setText(QString(""));
        }
    }

    str.sprintf(FORMAT_SPEED,m_Speed);
    ui->btnSpd->setText(str);
    str.sprintf(FORMAT_SPEED,m_Accel);
    ui->btnAcc->setText(str);
    str.sprintf(FORMAT_SPEED,m_Decel);
    ui->btnDec->setText(str);
    str.sprintf(FORMAT_SPEED,m_Interval);
    ui->btnInterval->setText(str);
}

void dialog_motor_test::Read_Param_Data()
{
    // Read Param.
    if(!Param->Get(HyParam::MOTOR_TUNE_DATA,m_Dist,m_Speed,m_Interval,m_Accel,m_Decel))
    {
        for(int i=0;i<CN_MAX_JOINT;i++)
            m_Dist.joint[i] = 0;
        m_Speed = 0;
        m_Interval = 0;
        m_Accel = 0;
        m_Decel = 0;
    }
}

void dialog_motor_test::Save_Param_Data()
{
    Param->Set(HyParam::MOTOR_TUNE_DATA,m_Dist,m_Speed,m_Interval,m_Accel,m_Decel);
}

void dialog_motor_test::Write_Recipe()
{
    Write_Recipe_Dist();
    Write_Recipe_Speed();
    Write_Recipe_Accel();
    Write_Recipe_Decel();
    Write_Recipe_Interval();
}
void dialog_motor_test::Write_Recipe_Dist()
{
    cn_joint joint;
    for(int i=0;i<m_vtSelIcon.count();i++)
    {
        if(m_vtSelIcon[i]->isHidden())
            joint.joint[i] = 0.0;
        else
            joint.joint[i] = m_Dist.joint[i];
    }

    Recipe->Set(HyRecipe::jTuneDist, joint);
}
void dialog_motor_test::Write_Recipe_Speed()
{
    Recipe->Set(HyRecipe::vTuneSpd, m_Speed);
}
void dialog_motor_test::Write_Recipe_Accel()
{
    Recipe->Set(HyRecipe::vTuneAcc, m_Accel);
}
void dialog_motor_test::Write_Recipe_Decel()
{
    Recipe->Set(HyRecipe::vTuneDec, m_Decel);
}
void dialog_motor_test::Write_Recipe_Interval()
{
    Recipe->Set(HyRecipe::vTuneTime, m_Interval);
}


void dialog_motor_test::SetServo(bool onoff)
{
    if(pxmServo.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-14.png"); //off
        pxmServo.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-13.png"); //on
        pxmServo.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmServo.size())
        ui->btnServoIcon->setPixmap(pxmServo[(int)onoff]);

}

void dialog_motor_test::onServo()
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->getServoOn())
        pCon->setServoOn(false);
    else
        pCon->setServoOn(true);
}
void dialog_motor_test::onReset()
{
    dialog_delaying* dig = new dialog_delaying();
    CNRobo* pCon = CNRobo::getInstance();
    pCon->resetEcatError();
    dig->Init(500);
    dig->exec();
    pCon->resetError();
    pCon->clearWarningCode();
}

void dialog_motor_test::onDist()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtDistPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    double min = m_MLimits.at(index).toDouble();
    np->m_numpad->SetMinValue(min);
    double max = m_PLimits.at(index).toDouble();
    np->m_numpad->SetMaxValue(max);
    np->m_numpad->SetSosuNum(SOSU_POS);
    np->m_numpad->SetTitle(m_vtJNo.at(index)->text() + ui->lbDistTitle->text());
    np->m_numpad->SetNum(m_vtDist.at(index)->text());

    if(np->exec() == QDialog::Accepted)
    {
        m_Dist.joint[index] = (float)np->m_numpad->GetNumDouble();
        Redraw_Display();
        Write_Recipe_Dist();
    }

}
void dialog_motor_test::onSpd()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0.1);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);
    np->m_numpad->SetTitle(ui->lbSpdTitle->text());
    np->m_numpad->SetNum(ui->btnSpd->text());

    if(np->exec() == QDialog::Accepted)
    {
        m_Speed = (float)np->m_numpad->GetNumDouble();
        Redraw_Display();
        Write_Recipe_Speed();
    }
}
void dialog_motor_test::onAcc()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0.1);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);
    np->m_numpad->SetTitle(ui->lbAccTitle->text());
    np->m_numpad->SetNum(ui->btnAcc->text());

    if(np->exec() == QDialog::Accepted)
    {
        m_Accel = (float)np->m_numpad->GetNumDouble();
        Redraw_Display();
        Write_Recipe_Accel();
    }
}
void dialog_motor_test::onDec()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0.1);
    np->m_numpad->SetMaxValue(100.0);
    np->m_numpad->SetSosuNum(SOSU_SPEED);
    np->m_numpad->SetTitle(ui->lbDecTitle->text());
    np->m_numpad->SetNum(ui->btnDec->text());

    if(np->exec() == QDialog::Accepted)
    {
        m_Decel = (float)np->m_numpad->GetNumDouble();
        Redraw_Display();
        Write_Recipe_Decel();
    }
}
void dialog_motor_test::onInterval()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(999999.99);
    np->m_numpad->SetSosuNum(SOSU_TIME);
    np->m_numpad->SetTitle(ui->lbIntervalTitle->text());
    np->m_numpad->SetNum(ui->btnInterval->text());

    if(np->exec() == QDialog::Accepted)
    {
        m_Interval = (float)np->m_numpad->GetNumDouble();
        Redraw_Display();
        Write_Recipe_Interval();
    }
}

void dialog_motor_test::onSel()
{
    QLabel3* btn = (QLabel3*)sender();
    int index = m_vtSel.indexOf(btn);
    if(index < 0) return;

    if(m_vtSelIcon[index]->isHidden())
        m_vtSelIcon[index]->show();
    else
        m_vtSelIcon[index]->hide();

    Write_Recipe_Dist();

}

void dialog_motor_test::onReal()
{
    QImage img;
    img.load(":/image/image/button_midrect_2.png");
    QPixmap pxmon = QPixmap::fromImage(img);
    img.load(":/image/image/button_midrect_0.png");
    QPixmap pxmoff = QPixmap::fromImage(img);

    if(ui->grpReal->isHidden())
    {
        ui->grpReal->show();
        ui->grpParam->hide();
        ui->btnRealPic->setPixmap(pxmon);
    }
    else
    {
        ui->grpReal->hide();
        ui->grpParam->show();
        ui->btnRealPic->setPixmap(pxmoff);
    }
}

void dialog_motor_test::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    if(!ui->lbMoveModeCheck->isHidden())
        SetMoving(!pCon->getHoldRun()); // hold=1, run=0;
    else
        SetMoving(false);

    if(ui->grpReal->isHidden()) return;

    float* joint;
    joint = pCon->getCurJoint();

    QString str;
    for(int i=0;i<m_vtReal.count();i++)
    {
        if(i < USE_AXIS_NUM)
        {
            str.sprintf(FORMAT_POS, joint[i]);
            m_vtReal[i]->setText(str);
        }
        else
        {
            m_vtReal[i]->setText(QString(""));
        }
    }


}

void dialog_motor_test::SetMoving(bool onoff)
{


    if(pxmMove.isEmpty())
    {
        QImage img;
        img.load(":/image/image/play.png"); // off
        pxmMove.append(QPixmap::fromImage(img));
        img.load(":/image/image/stop.png"); // on
        pxmMove.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmMove.size())
    {
        ui->btnMoveIcon->setPixmap(pxmMove[(int)onoff]);
        m_bMoving = onoff;
        if(onoff)
        {
            ui->btnMove->setText(tr("Stop"));
            ui->lbMoveMode->setEnabled(false);
            ui->lbMoveModeCheck->setEnabled(false);
            ui->grpEnd->setEnabled(false);
            ui->grpServo->setEnabled(false);
        }
        else
        {
            ui->btnMove->setText(tr("Move"));
            ui->lbMoveMode->setEnabled(true);
            ui->lbMoveModeCheck->setEnabled(true);
            ui->grpEnd->setEnabled(true);
            ui->grpServo->setEnabled(true);
        }
    }
}

void dialog_motor_test::onMove()
{
    CNRobo* pCon = CNRobo::getInstance();
    QString prg_name = QString("move_motortune");

    int ret=0;
    int cycle=1;

    if(!m_bMoving)
    {
        if(ui->lbCheck->isHidden())
            cycle = 1;
        else
            cycle = -1;

        ret = pCon->executeProgram(0, prg_name, cycle);
        qDebug() << "executeProgram2 ret =" << ret;
    }
    else
    {
        ret = pCon->setProgramRunCount(0, 1);
        if(ret < 0)
            qDebug() << "setProgramRunCount ret=" << ret;
    }
}

void dialog_motor_test::onContinue()
{
    if(ui->lbCheck->isHidden())
        SetContinue(true);
    else
        SetContinue(false);
}

void dialog_motor_test::SetContinue(bool on)
{
    if(on)
        ui->lbCheck->show();
    else
        ui->lbCheck->hide();
}

void dialog_motor_test::SetMoveMode(bool on)
{
    if(on)
    {
        // move mode
        gReadyMacro(true);
        ui->lbMoveModeCheck->show();
        Delay(1000);
        ui->grpMove->setEnabled(true);
        ui->grpJog->setEnabled(false);

        ui->grpTable->setEnabled(false);
        ui->grpParam->setEnabled(false);
    }
    else
    {
        // jog mode
        ui->lbMoveModeCheck->hide();
        Delay(1000);
        gReadyJog(true);
        gSetJogType(JOG_TYPE_JOINT);

        ui->grpMove->setEnabled(false);
        ui->grpJog->setEnabled(true);

        ui->grpTable->setEnabled(true);
        ui->grpParam->setEnabled(true);
    }
}

void dialog_motor_test::onMoveMode()
{
    if(ui->lbMoveModeCheck->isHidden())
        SetMoveMode(true);
    else
        SetMoveMode(false);
}

void dialog_motor_test::Delay(int msec)
{
    dialog_delaying* dig = new dialog_delaying();
    dig->Init(msec);
    dig->exec();
}

void dialog_motor_test::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
