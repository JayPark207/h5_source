#ifndef HYOUTPUT
#define HYOUTPUT

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#define MAX_OUTPUT_NUM      128
#define USE_OUTPUT_NUM      48
#define USE_OUTPUT_BUFNUM   2

/** output list **/
class HyOutput : public QObject
{
    Q_OBJECT

public:

    // controled output matching number. no redundancy. start=1~ (real output num)
    enum ENUM_OUTPUT_NUM
    {
        VAC1        = 1,
        VAC1_OFF    = 2,

        VAC2        = 3,
        VAC2_OFF    = 4,

        VAC3        = 5,
        VAC3_OFF    = 6,

        VAC4        = 7,
        VAC4_OFF    = 8,

        CHUCK       = 9,
        CHUCK_OFF   = 10,

        GRIP        = 11,
        NIPPER      = 12,
        RGRIP       = 13,

        USER1       = 25,
        USER2       = 26,
        USER3       = 27,
        USER4       = 28,
        USER5       = 29,
        USER6       = 30,
        USER7       = 31,
        USER8       = 32,
    };

public:
    HyOutput()
    {
        Sym.clear();
        Sym.fill("Y000",MAX_OUTPUT_NUM);
        Name.clear();
        Name.fill("Output",MAX_OUTPUT_NUM);
        On.clear();
        On.fill(false,MAX_OUTPUT_NUM);
        UiName.clear();
        UiName.fill("", MAX_OUTPUT_NUM);

        Init();
    }

    void Init()
    {
        // default sym,name
        int i;
        QString str;
        for(i=0;i<USE_OUTPUT_NUM;i++)
        {
            str.sprintf("Y%03d", i);
            Sym[i] = str;
            str.sprintf("Output X%03d", i);
            Name[i] = str;
        }

        Sym[0]  = QString("Y001");Name[0]  = QString(tr("Vac.1 ON"));
        Sym[1]  = QString("Y002");Name[1]  = QString(tr("Vac.1 OFF"));
        Sym[2]  = QString("Y003");Name[2]  = QString(tr("Vac.2 ON"));
        Sym[3]  = QString("Y004");Name[3]  = QString(tr("Vac.2 OFF"));
        Sym[4]  = QString("Y005");Name[4]  = QString(tr("Vac.3 ON"));
        Sym[5]  = QString("Y006");Name[5]  = QString(tr("Vac.3 OFF"));
        Sym[6]  = QString("Y007");Name[6]  = QString(tr("Vac.4 ON"));
        Sym[7]  = QString("Y008");Name[7]  = QString(tr("Vac.4 OFF"));
        Sym[8]  = QString("Y009");Name[8]  = QString(tr("Chuck ON"));
        Sym[9]  = QString("Y010");Name[9]  = QString(tr("Chuck OFF"));
        Sym[10] = QString("Y011");Name[10] = QString(tr("Gripper"));
        Sym[11] = QString("Y012");Name[11] = QString(tr("Nipper"));
        Sym[12] = QString("Y013");Name[12] = QString(tr(""));   //old: R-Grip
        Sym[13] = QString("Y014");Name[13] = QString(tr(""));
        Sym[14] = QString("Y015");Name[14] = QString(tr("Laser ON"));
        Sym[15] = QString("Y016");Name[15] = QString(tr("Ionizer ON"));

        Sym[16] = QString("Y017");Name[16] = QString(tr("Buzzer"));
        Sym[17] = QString("Y018");Name[17] = QString(tr("Main Air Off"));
        Sym[18] = QString("Y019");Name[18] = QString(tr("Conveyor On"));
        Sym[19] = QString("Y020");Name[19] = QString(tr(""));
        Sym[20] = QString("Y021");Name[20] = QString(tr(""));
        Sym[21] = QString("Y022");Name[21] = QString(tr(""));
        Sym[22] = QString("Y023");Name[22] = QString(tr(""));
        Sym[23] = QString("Y024");Name[23] = QString(tr(""));
        Sym[24] = QString("Y025");Name[24] = QString(tr("User Output 1"));
        Sym[25] = QString("Y026");Name[25] = QString(tr("User Output 2"));
        Sym[26] = QString("Y027");Name[26] = QString(tr("User Output 3"));
        Sym[27] = QString("Y028");Name[27] = QString(tr("User Output 4"));
        Sym[28] = QString("Y029");Name[28] = QString(tr("User Output 5"));
        Sym[29] = QString("Y030");Name[29] = QString(tr("User Output 6"));
        Sym[30] = QString("Y031");Name[30] = QString(tr("User Output 7"));
        Sym[31] = QString("Y032");Name[31] = QString(tr("User Output 8"));

        Sym[32] = QString("Y033");Name[32] = QString(tr("Emergency Robot 1"));
        Sym[33] = QString("Y034");Name[33] = QString(tr("Emergency Robot 2"));
        Sym[34] = QString("Y035");Name[34] = QString(tr(""));
        Sym[35] = QString("Y036");Name[35] = QString(tr("Cycle Start"));
        Sym[36] = QString("Y037");Name[36] = QString(tr("Robot Operation Mode"));
        Sym[37] = QString("Y038");Name[37] = QString(tr("Enable Mold Close"));
        Sym[38] = QString("Y039");Name[38] = QString(tr("Mold Area Free"));
        Sym[39] = QString("Y040");Name[39] = QString(tr("Enable Ejector Bwd."));
        Sym[40] = QString("Y041");Name[40] = QString(tr("Enable Ejector Fwd."));
        Sym[41] = QString("Y042");Name[41] = QString(tr("Enable Core 1 in pos.1"));
        Sym[42] = QString("Y043");Name[42] = QString(tr("Enable Core 1 in pos.2"));
        Sym[43] = QString("Y044");Name[43] = QString(tr("Enable Core 2 in pos.1"));
        Sym[44] = QString("Y045");Name[44] = QString(tr("Enable Core 2 in pos.2"));
        Sym[45] = QString("Y046");Name[45] = QString(tr("Enable Full Mold Opening"));
        Sym[46] = QString("Y047");Name[46] = QString(tr(""));
        Sym[47] = QString("Y048");Name[47] = QString(tr(""));

        // add here ...

        // UI Name (= matcing ENUM_OUTPUT_NUM in USE_OUTPUT_NUM)
        UiName[VAC1-1] = tr("Vac.1");
        UiName[VAC2-1] = tr("Vac.2");
        UiName[VAC3-1] = tr("Vac.3");
        UiName[VAC4-1] = tr("Vac.4");
        UiName[CHUCK-1] = tr("Chuck");
        UiName[GRIP-1] = tr("Grip");
        UiName[NIPPER-1] = tr("Nipper");
        UiName[RGRIP-1] = tr("R Grip");
        UiName[USER1-1] = tr("User 1");
        UiName[USER2-1] = tr("User 2");
        UiName[USER3-1] = tr("User 3");
        UiName[USER4-1] = tr("User 4");
        UiName[USER5-1] = tr("User 5");
        UiName[USER6-1] = tr("User 6");
        UiName[USER7-1] = tr("User 7");
        UiName[USER8-1] = tr("User 8");

    }

    QVector<QString> Sym;
    QVector<QString> Name;
    QVector<bool>    On;
    QVector<QString> UiName;

    QString GetSym(int i) { return Sym.at(i); }
    QString GetName(int i) { return Name.at(i); }
    void SetOn(int i, bool on) { On[i] = on; }
    bool IsOn(int i) { return On.at(i); }

    QString GetUiName(int i) { return UiName[i]; }
};

#endif // HYOUTPUT

