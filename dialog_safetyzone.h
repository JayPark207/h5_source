#ifndef DIALOG_SAFETYZONE_H
#define DIALOG_SAFETYZONE_H

#include <QDialog>
#include <QStackedWidget>
#include <QTimer>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_manual.h"
#include "dialog_numpad.h"
#include "dialog_message.h"
#include "dialog_confirm.h"


#define SZ_IMM_BTM_OFFSET 300
#define SZ_RBT_TOP_OFFSET 50
#define SZ_MAX_X_SIZE   20000
#define SZ_MAX_Y_SIZE   20000
#define SZ_HEIGHT_THICKNESS 1000

namespace Ui {
class dialog_safetyzone;
}

class dialog_safetyzone : public QDialog
{
    Q_OBJECT

private:
    struct ST_SZ_DATA
    {
        cn_vec3 p1;
        cn_vec3 p2;
    };

public:
    explicit dialog_safetyzone(QWidget *parent = 0);
    ~dialog_safetyzone();

public slots:
    void onOK();
    void onCancel();
    void onTimer();

    void onNext();
    void onBack();

    void onManual();

    void onPBtn();
    void onP1();
    void onP2();

private:
    Ui::dialog_safetyzone *ui;
    QTimer* timer;
    dialog_manual* dig_manual;

    void Update();
    bool SetSafetyZone();

    QStackedWidget* pic_page;
    void Update_Page(int page_index);

    QVector<ST_SZ_DATA> m_PData;

    QVector<QLabel4*> m_vtPBtn;
    QVector<QLabel3*> m_vtPVal;
    QVector<QLabel3*> m_vtRowTitle;

    void Display_Data(int page_index);
    void Display_SaveBtn(int page_index);
    void Display_DataEnable(int page_index);

    void    SetPData(int page_index, int table_index, float value);
    float   GetPData(int page_index, int table_index);
    void    ZeroPData();
    cn_trans2 GetBaseFrame();

    bool m_bSafetyZone_old;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SAFETYZONE_H
