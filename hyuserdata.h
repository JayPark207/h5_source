#ifndef HYUSERDATA_H
#define HYUSERDATA_H

#include <QObject>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyrecipe.h"
#include "hystepedit.h"

//#define MAX_USERDATA_INOUT_SELECTNUM    8
#define MAX_USERDATA_IN_SELECTNUM       8
#define MAX_USERDATA_OUT_SELECTNUM      8

struct ST_USERDATA_POS
{
    cn_trans Trans;
    cn_joint Joint;
    float Speed;
    float Delay;
    float Type; //0=step by step, 1= all together.
};

struct ST_USERDATA_WORK
{
    cn_trans Trans;
    cn_joint Joint;
    float Speed;
    float Delay;
    float Type; //0=step by step, 1= all together.
};

struct ST_USERDATA_OUTPUT
{ 
    // now only use [0] : no multi selection.
    // value = output number.
    float Sel[MAX_USERDATA_OUT_SELECTNUM];

    float Delay;        // sec
    float Type;         // 0=Off, 1=On, 2=Pulse
    float OnTime;       // sec
};

struct ST_USERDATA_INPUT
{
    // now all use : multi selection.
    // value = input number.
    float Sel[MAX_USERDATA_IN_SELECTNUM];

    float Delay;        // sec
    float Type;         // 0=off, 1=on
    float Condition;    // 0=and, 1=or
};

struct ST_USERDATA_TIME
{
    float Delay;        // sec
};


class HyUserData : public QObject
{
    Q_OBJECT

public:
    enum ENUM_TYPE
    {
        TYPE_OFF = 0,
        TYPE_ON,
        TYPE_PULSE,
    };

    enum ENUM_CONDITION
    {
        COND_AND=0,
        COND_OR,
    };


public:
    explicit HyUserData(HyRecipe* recipe);

    void AllZero();

    // user pos
    bool WR(int index, ST_USERDATA_POS st, bool save=true);
    bool WR(int index, ST_USERDATA_OUTPUT st, bool save=true);
    bool WR(int index, ST_USERDATA_INPUT st, bool save=true);
    bool WR(int index, ST_USERDATA_TIME st, bool save=true);
    bool WR(int index, ST_USERDATA_WORK st, bool save=true);

    bool RD(int index, ST_USERDATA_POS* st);
    bool RD(int index, ST_USERDATA_OUTPUT* st);
    bool RD(int index, ST_USERDATA_INPUT* st);
    bool RD(int index, ST_USERDATA_TIME* st);
    bool RD(int index, ST_USERDATA_WORK* st);

    // use buffer
    void SetBuf_Default(ST_USERDATA_POS* st);
    void SetBuf_Default(ST_USERDATA_OUTPUT* st);
    void SetBuf_Default(ST_USERDATA_INPUT* st);
    void SetBuf_Default(ST_USERDATA_TIME* st);
    void SetBuf_Default(ST_USERDATA_WORK* st);

    QStringList m_UserPos_VarsName;
    QStringList m_UserOut_VarsName;
    QStringList m_UserIn_VarsName;
    QStringList m_UserTime_VarsName;
    QStringList m_UserWork_VarsName;

    // when it add, set 1st value. for modify.
    void Set_1stValue(ST_USERDATA_POS* st);
    void Set_1stValue(ST_USERDATA_OUTPUT* st);
    void Set_1stValue(ST_USERDATA_INPUT* st);
    void Set_1stValue(ST_USERDATA_TIME* st);
    void Set_1stValue(ST_USERDATA_WORK* st);



private:
    HyRecipe* m_Recipe;



};

#endif // HYUSERDATA_H
