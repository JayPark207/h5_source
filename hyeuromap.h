#ifndef HYEUROMAP_H
#define HYEUROMAP_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hyrecipe.h"
#include "hyinput.h"
#include "hyoutput.h"

struct ST_EUROMAP_MODULE
{
    int index;
    QString Name;
    QStringList DataName;
};

struct ST_EUROMAP_SIGNAL
{
    int IONum;
    QString Name;
    QString Type; // (0,1,2)=(S/EM67/EM12) -> split(,)
};

class HyEuromap : public QObject
{
    Q_OBJECT

public:
    enum ENUM_MODULE
    {
        COMMON=0,
        OPERATION,
        SAFETY,
        MOLD,
        EJECTOR,
        CORE1,
        CORE2,

        MODULE_NUM,
    };

private:
    HyRecipe*   m_Recipe;
    HyInput*    m_Input;
    HyOutput*   m_Output;

public:
    explicit HyEuromap(HyRecipe* recipe, HyInput* input, HyOutput* output);

    void Init();

    void Init_Module();
    QVector<ST_EUROMAP_MODULE> Module;

    void Init_Signal();
    QVector<ST_EUROMAP_SIGNAL> OutRob;
    QVector<ST_EUROMAP_SIGNAL> InImm;

    void Get_SimRun(bool& run);
    void Set_SimRun(bool run);

};

#endif // HYEUROMAP_H
