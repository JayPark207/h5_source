#include "hykey.h"
#include <QKeyEvent>
#include <QDebug>

bool HyKey::notify(QObject* object,QEvent* event)
{
    QKeyEvent* kev = (QKeyEvent*)event;
    char cKey;
    int nKey;
    nKey = kev->key();
    cKey = (char)nKey;

    if(event->type() == QEvent::KeyPress)
    {
        //qDebug() << "press-" + QString(cKey);
        //qDebug() << "press-" + QString().setNum(nKey);

        Proc_Right_Press(cKey);
        Proc_Left_Press(cKey);
        Pendent->Beep();

        return false;
    }
    else if(event->type() == QEvent::KeyRelease)
    {
        //qDebug() << "release-" + QString(cKey);
        //qDebug() << "release-" + QString().setNum(nKey);

        Proc_Right_Release(cKey);
        Proc_Left_Release(cKey);
        //Pendent->Beep();

        return false;
    }

    return QApplication::notify(object, event);
}


void HyKey::Proc_Right_Press(char key)
{
    CNRobo* pCon = CNRobo::getInstance();

    // jog type change
    switch(key)
    {
    case 0x41: // Right > L1
    case 0x42: // Right > R1
    case 0x43: // Right > L2
    case 0x44: // Right > R2
    case 0x45: // Right > L3
    case 0x46: // Right > R3

        if(gGetJogType() == JOG_TYPE_HY)
        {
            if(pCon->getCoordinate() != CNR_TC_BASE)
                pCon->setCoordinate(CNR_TC_BASE);
        }
        else if(gGetJogType() == JOG_TYPE_JOINT)
        {
            if(pCon->getCoordinate() != CNR_TC_JOINT)
                pCon->setCoordinate(CNR_TC_JOINT);

        }
        else if(gGetJogType() == JOG_TYPE_TRANS)
        {
            if(pCon->getCoordinate() != CNR_TC_BASE)
                pCon->setCoordinate(CNR_TC_BASE);
        }
        else if(gGetJogType() == JOG_TYPE_HY_TOOL)
        {
            if(pCon->getCoordinate() != CNR_TC_TOOL)
                pCon->setCoordinate(CNR_TC_TOOL);
        }

        break;

    case 0x47: // Right > L4
    case 0x48: // Right > R4
    case 0x49: // Right > L5
    case 0x4A: // Right > R5
#ifdef _H6_
    case 0x4B: // Right > L6
    case 0x4C: // Right > R6
#endif
        if(gGetJogType() == JOG_TYPE_HY)
        {
            if(pCon->getCoordinate() != CNR_TC_JOINT)
                pCon->setCoordinate(CNR_TC_JOINT);
        }
        else if(gGetJogType() == JOG_TYPE_JOINT)
        {
            if(pCon->getCoordinate() != CNR_TC_JOINT)
                pCon->setCoordinate(CNR_TC_JOINT);
        }
        else if(gGetJogType() == JOG_TYPE_TRANS)
        {
            if(pCon->getCoordinate() != CNR_TC_BASE)
                pCon->setCoordinate(CNR_TC_BASE);
        }
        else if(gGetJogType() == JOG_TYPE_HY_TOOL)
        {
            if(pCon->getCoordinate() != CNR_TC_JOINT)
                pCon->setCoordinate(CNR_TC_JOINT);
        }

        break;

#ifndef _H6_
    case 0x4B: // Right > L6
        break;
    case 0x4C: // Right > R6
        break;
#endif

    }

    // jog move
    switch(key)
    {
    case 0x41: // Right > L1
        pCon->setJogOn(0,MASK[0]);
        g_nKeyCommand_Press = KEYCMD_R_L1;
        break;
    case 0x42: // Right > R1
        pCon->setJogOn(MASK[0],0);
        g_nKeyCommand_Press = KEYCMD_R_R1;
        break;
    case 0x43: // Right > L2
        pCon->setJogOn(0,MASK[1]);
        g_nKeyCommand_Press = KEYCMD_R_L2;
        break;
    case 0x44: // Right > R2
        pCon->setJogOn(MASK[1],0);
        g_nKeyCommand_Press = KEYCMD_R_R2;
        break;
    case 0x45: // Right > L3
        pCon->setJogOn(0,MASK[2]);
        g_nKeyCommand_Press = KEYCMD_R_L3;
        break;
    case 0x46: // Right > R3
        pCon->setJogOn(MASK[2],0);
        g_nKeyCommand_Press = KEYCMD_R_R3;
        break;
    case 0x47: // Right > L4
        pCon->setJogOn(0,MASK[3]);
        g_nKeyCommand_Press = KEYCMD_R_L4;
        break;
    case 0x48: // Right > R4
        pCon->setJogOn(MASK[3],0);
        g_nKeyCommand_Press = KEYCMD_R_R4;
        break;
    case 0x49: // Right > L5
        pCon->setJogOn(0,MASK[4]);
        g_nKeyCommand_Press = KEYCMD_R_L5;
        break;
    case 0x4A: // Right > R5
        pCon->setJogOn(MASK[4],0);
        g_nKeyCommand_Press = KEYCMD_R_R5;
        break;
    case 0x4B: // Right > L6
#ifdef _H6_
        pCon->setJogOn(0,MASK[5]);
#endif
        g_nKeyCommand_Press = KEYCMD_R_L6;
        break;
    case 0x4C: // Right > R6
#ifdef _H6_
        pCon->setJogOn(MASK[5],0);
#endif
        g_nKeyCommand_Press = KEYCMD_R_R6;
        break;

    }
}
void HyKey::Proc_Right_Release(char key)
{
    CNRobo* pCon = CNRobo::getInstance();

    switch(key)
    {
    case 0x41: // Right > L1
        //pCon->setJogOff(0,MASK[0]);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_L1;
        break;
    case 0x42: // Right > R1
        //pCon->setJogOff(MASK[0],0);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_R1;
        break;
    case 0x43: // Right > L2
        //pCon->setJogOff(0,MASK[1]);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_L2;
        break;
    case 0x44: // Right > R2
        //pCon->setJogOff(MASK[1],0);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_R2;
        break;
    case 0x45: // Right > L3
        //pCon->setJogOff(0,MASK[2]);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_L3;
        break;
    case 0x46: // Right > R3
        //pCon->setJogOff(MASK[2],0);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_R3;
        break;
    case 0x47: // Right > L4
        //pCon->setJogOff(0,MASK[3]);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_L4;
        break;
    case 0x48: // Right > R4
        //pCon->setJogOff(MASK[3],0);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_R4;
        break;
    case 0x49: // Right > L5
        //pCon->setJogOff(0,MASK[4]);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_L5;
        break;
    case 0x4A: // Right > R5
        //pCon->setJogOff(MASK[4],0);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
        g_nKeyCommand_Release = KEYCMD_R_R5;
        break;
    case 0x4B: // Right > L6
#ifdef _H6_
        //pCon->setJogOff(0,MASK[5]);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
#else
        if(!UseSkip->Get(HyUseSkip::TOOL_COORD))
        {
            qDebug() << "Disabled Tool Coordinate!";
        }
        else
        {
            if(Status->bJog)
            {
                if(gGetJogType() == JOG_TYPE_HY)
                {
                    gSetJogType(JOG_TYPE_HY_TOOL);
                    qDebug() << "JOG_TYPE_HY_TOOL";
                }
                else if(gGetJogType() == JOG_TYPE_HY_TOOL)
                {
                    gSetJogType(JOG_TYPE_HY);
                    qDebug() << "JOG_TYPE_HY";
                }
            }
        }
#endif
        g_nKeyCommand_Release = KEYCMD_R_L6;
        break;

    case 0x4C: // Right > R6
#ifdef _H6_
        //pCon->setJogOff(MASK[5],0);
        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF);
#else
        Capture();
#endif
        g_nKeyCommand_Release = KEYCMD_R_R6;
        break;
    }

}
void HyKey::Proc_Left_Press(char key)
{
    switch(key)
    {
    case 0x12: // Left > left
        g_nKeyCommand_Press = KEYCMD_L_LEFT;
        break;
    case 0x13: // Left > up
        g_nKeyCommand_Press = KEYCMD_L_UP;
        break;
    case 0x14: // Left > right
        g_nKeyCommand_Press = KEYCMD_L_RIGHT;
        break;
    case 0x15: // Left > down
        g_nKeyCommand_Press = KEYCMD_L_DOWN;
        break;


    case 0x35: // Left > btn1
        g_nKeyCommand_Press = KEYCMD_L_BTN1;
        break;
    case 0x36: // Left > btn2
        g_nKeyCommand_Press = KEYCMD_L_BTN2;
        break;
    case 0x37: // Left > btn3
        g_nKeyCommand_Press = KEYCMD_L_BTN3;
        break;
    }
}
void HyKey::Proc_Left_Release(char key)
{
    switch(key)
    {
    case 0x12: // Left > left
        g_nKeyCommand_Release = KEYCMD_L_LEFT;
        break;
    case 0x13: // Left > up
        g_nKeyCommand_Release = KEYCMD_L_UP;
        break;
    case 0x14: // Left > right
        g_nKeyCommand_Release = KEYCMD_L_RIGHT;
        break;
    case 0x15: // Left > down
        g_nKeyCommand_Release = KEYCMD_L_DOWN;
        break;

    case 0x35: // Left > btn1
        g_nKeyCommand_Release = KEYCMD_L_BTN1;
        break;
    case 0x36: // Left > btn2
        g_nKeyCommand_Release = KEYCMD_L_BTN2;
        break;
    case 0x37: // Left > btn3
        g_nKeyCommand_Release = KEYCMD_L_BTN3;
        break;
    }
}

