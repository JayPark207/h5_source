#include "top_main.h"
#include "ui_top_main.h"

top_main::top_main(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::top_main)
{
    ui->setupUi(this);

    m_Timer = new QTimer(this); //start = after first connection
    connect(m_Timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    m_DispTimer = new QTimer(this);
    connect(m_DispTimer,SIGNAL(timeout()),this,SLOT(onDispTimer()));


    connect(ui->lbSpeed,SIGNAL(mouse_release()),this,SLOT(onSpeed()));
    connect(ui->lbSpeedPic,SIGNAL(mouse_release()),ui->lbSpeed,SIGNAL(mouse_release()));

    connect(ui->lbULevel,SIGNAL(mouse_release()),this,SLOT(onUser()));
    connect(ui->lbULevelPic,SIGNAL(mouse_release()),ui->lbULevel,SIGNAL(mouse_release()));
    connect(ui->lbULevelIcon,SIGNAL(mouse_release()),ui->lbULevel,SIGNAL(mouse_release()));

    connect(ui->lbServoPic,SIGNAL(mouse_release()),this,SLOT(onSafetyZone()));

    connect(ui->lbRobotPic,SIGNAL(mouse_release()),this,SLOT(onRobotIcon()));

    m_nCount = 0;


    pxmRobotStatus.clear();
    pxmServo.clear();
    pxmEmo.clear();
    pxmJog.clear();
    pxmLed1.clear();
    pxmLed2.clear();
    pxmLed3.clear();
    pxmSafetyZone.clear();

}

top_main::~top_main()
{
    delete ui;
}
void top_main::showEvent(QShowEvent *)
{
    m_DispTimer->start(200);
}
void top_main::hideEvent(QHideEvent *)
{
    m_DispTimer->stop();
}
void top_main::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void top_main::onTimer() // always on.
{
    this->Update();
}

void top_main::Update() // always on.
{
//    QElapsedTimer ti;
//    ti.start();
    Status->Update();
    /** IO Update **/
    if(gGet_InputUpdate())
        Update_Input();
    if(gGet_OutputUpdate())
        Update_Output();

    //Status->bEmo = (bError && ((m_nErrCode == -1009) || (m_nErrCode == -1010)));
    Status->bEmo = !Input->IsOn(HyInput::EMO-1);

//    qDebug() << "HyStatus Update() = " << ti.elapsed() << "mesc";

    // Now Recipe Update => HyStatus buffer
    Status->strNowRecipe = RM->GetNowRecipeNoName();

    ui->lbCurrMold->setText(Status->strNowRecipe);

    // Main Speed Update => HyStatus buffer
    Status->nMainSpeed = Param->Get(HyParam::MAIN_SPEED).toInt();
    ui->pbSpeed->setValue(Status->nMainSpeed);

    // User Level Update => HyStatus buffer
    Status->nUserLevel = Param->Get(HyParam::USER_LEVEL).toInt();
    ui->lbULevel->setText(QString().setNum(Status->nUserLevel));

    if(++m_nCount > 3)
    {
        m_nCount = 0;
        Status->bFlicker = !Status->bFlicker;
    }

    // SoftSensor monitoring (but, need long time. careful)
    Status->bLed3 = SoftSensor(UseSkip->Get(HyUseSkip::SOFTSENSOR_MON));


//    qDebug() << "top_main Update() = " << ti.elapsed() << "mesc";
}

void top_main::onDispTimer()
{
    // Time update
    ui->lbDate->setText(Status->strDate);
    ui->lbTime->setText(Status->strTime);

    DispEmo(Status->bEmo);
    //DispServo(Status->bServo);
    DispSafetyZone(Status->bSafetyZone);
    DispJog(Status->bJog);
    DispRobotStatus(Status->nRobotStatus);

    // Led1 = coreServer connect & login & lock.
    DispLed1(Status->bLed1);
    DispLed2(Status->bLed2);
    DispLed3(Status->bLed3);
}


void top_main::DispRobotStatus(HyStatus::ENUM_ROBOT_STATUS status)
{ 
    if(pxmRobotStatus.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-11.png");
        pxmRobotStatus.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-9.png");
        pxmRobotStatus.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-10.png");
        pxmRobotStatus.append(QPixmap::fromImage(img));
    }

    if((int)status < pxmRobotStatus.size())
        ui->lbRobotPic->setPixmap(pxmRobotStatus[status]);
}
void top_main::DispServo(bool onoff)
{
    if(pxmServo.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-14.png");
        pxmServo.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-13.png");
        pxmServo.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmServo.size())
        ui->lbServoPic->setPixmap(pxmServo[(int)onoff]);
}
void top_main::DispSafetyZone(bool onoff)
{
    if(pxmSafetyZone.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-73.png");
        pxmSafetyZone.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-74.png");
        pxmSafetyZone.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmSafetyZone.size())
        ui->lbServoPic->setPixmap(pxmSafetyZone[(int)onoff]);
}

void top_main::DispJog(bool onoff)
{
    if(pxmJog.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-17.png");
        pxmJog.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-16.png");
        pxmJog.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmJog.size())
        ui->lbJogPic->setPixmap(pxmJog[(int)onoff]);
}
void top_main::DispEmo(bool onoff)
{
    if(pxmEmo.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-2.png");
        pxmEmo.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-1.png");
        pxmEmo.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmEmo.size())
        ui->lbEmoPic->setPixmap(pxmEmo[(int)onoff]);
}


void top_main::DispLed1(bool onoff)
{
    if(pxmLed1.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon992-0.png");
        pxmLed1.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon992-4.png");
        pxmLed1.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmLed1.size())
        ui->led1->setPixmap(pxmLed1[(int)onoff]);
}
void top_main::DispLed2(bool onoff)
{
    if(pxmLed2.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon992-0.png");
        pxmLed2.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon992-5.png");
        pxmLed2.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmLed2.size())
        ui->led2->setPixmap(pxmLed2[(int)onoff]);
}
void top_main::DispLed3(bool onoff)
{
    if(pxmLed3.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon992-0.png");
        pxmLed3.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon992-10.png");
        pxmLed3.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmLed3.size())
        ui->led3->setPixmap(pxmLed3[(int)onoff]);
}
void top_main::onSpeed()
{
    gSetTopPage(TOP_SPEED);
}

void top_main::onUser()
{
    dialog_userlevel* dig = (dialog_userlevel*)gGetDialog(DIG_USERLEVEL);
    dig->exec();
}

void top_main::Update_IO()
{
    CNRobo* pCon = CNRobo::getInstance();
    cn_ui32 _in[USE_INPUT_BUFNUM];
    cn_ui32 _out[USE_OUTPUT_BUFNUM];
    bool _result;
    cn_ui32 _mask;
    int i;

    pCon->getDI(_in, USE_INPUT_BUFNUM);
    pCon->getDO(_out, USE_OUTPUT_BUFNUM);

    for(i=0;i<USE_INPUT_NUM; i++)
    {
        if((i % (sizeof(cn_ui32)*8)) == 0)
            _mask = 1;

        _result = ((_in[(int)(i/(sizeof(cn_ui32)*8))] & _mask) > 0);
        _mask <<= 1;

        Input->SetOn(i, _result);
    }

    for(i=0;i<USE_OUTPUT_NUM; i++)
    {
        if((i % (sizeof(cn_ui32)*8)) == 0)
            _mask = 1;

        _result = ((_out[(int)(i/(sizeof(cn_ui32)*8))] & _mask) > 0);
        _mask <<= 1;

        Output->SetOn(i, _result);
    }
}

void top_main::Update_Input()
{
    CNRobo* pCon = CNRobo::getInstance();
    cn_ui32 _in[USE_INPUT_BUFNUM];
    bool _result;
    cn_ui32 _mask;
    int i;

    int ret = pCon->getDI(_in, USE_INPUT_BUFNUM);
    if(ret < 0)
        return;

    for(i=0;i<USE_INPUT_NUM; i++)
    {
        if((i % (sizeof(cn_ui32)*8)) == 0)
            _mask = 1;

        _result = ((_in[(int)(i/(sizeof(cn_ui32)*8))] & _mask) > 0);
        _mask <<= 1;

        Input->SetOn(i, _result);
    }
}

void top_main::Update_Output()
{
    CNRobo* pCon = CNRobo::getInstance();
    cn_ui32 _out[USE_OUTPUT_BUFNUM];
    bool _result;
    cn_ui32 _mask;
    int i;

    int ret = pCon->getDO(_out, USE_OUTPUT_BUFNUM);
    if(ret < 0)
        return;

    for(i=0;i<USE_OUTPUT_NUM; i++)
    {
        if((i % (sizeof(cn_ui32)*8)) == 0)
            _mask = 1;

        _result = ((_out[(int)(i/(sizeof(cn_ui32)*8))] & _mask) > 0);
        _mask <<= 1;

        Output->SetOn(i, _result);
    }
}

void top_main::onSafetyZone()
{
    /*dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(tr("Safety Zone"));
    if(Status->bSafetyZone)
        conf->Message(tr("Safety Zone OFF?"), tr("Be careful!"));
    else
        conf->Message(tr("Safety Zone ON?"));

    if(conf->exec() == QDialog::Accepted)
    {
        CNRobo* pCon = CNRobo::getInstance();
        pCon->setWZCheckFlag(!Status->bSafetyZone);
        pCon->WZSaveAll();
    }*/
}

void top_main::onRobotIcon()
{
    if(Status->nRobotStatus == HyStatus::RBS_ERROR)
    {
        dialog_error* dig = (dialog_error*)gGetDialog(DIG_ERR);
        dig->exec();
    }
    else if(Status->nRobotStatus == HyStatus::RBS_NOT_RDY)
    {
        CNRobo* pCon = CNRobo::getInstance();

        if(pCon->isError())
            pCon->resetError();

        if(!pCon->getServoOn())
            pCon->setServoOn(true);
    }
}

bool top_main::SoftSensor(bool use)
{
    if(!use) return false;

    float data;
    if(!Recipe->Get(HyRecipe::emCmdInMold, &data))
        return false;
    if(data > 0.5)
        return true;
    return false;
}
