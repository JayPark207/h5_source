#include "dialog_anti_vibration.h"
#include "ui_dialog_anti_vibration.h"

dialog_anti_vibration::dialog_anti_vibration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_anti_vibration)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // main tab & panel
    Panel = new QStackedWidget(this);
    Panel->setGeometry(ui->frame->x(),
                       ui->frame->y(),
                       ui->frame->width(),
                       ui->frame->height());
    Panel->addWidget(ui->frame);
    Panel->addWidget(ui->frame_2);
    Panel->setCurrentIndex(0);

    m_vtTab.clear();             m_vtTabPic.clear();                m_vtTabLed.clear();
    m_vtTab.append(ui->btnTab);  m_vtTabPic.append(ui->btnTabPic);  m_vtTabLed.append(ui->lbTabLed);
    m_vtTab.append(ui->btnTab_2);m_vtTabPic.append(ui->btnTabPic_2);m_vtTabLed.append(ui->lbTabLed_2);
    int i;
    for(i=0;i<m_vtTabPic.count();i++)
    {
        connect(m_vtTabPic[i],SIGNAL(mouse_release()),this,SLOT(onTab()));
        connect(m_vtTab[i],SIGNAL(mouse_press()),m_vtTabPic[i],SLOT(press()));
        connect(m_vtTab[i],SIGNAL(mouse_release()),m_vtTabPic[i],SLOT(release()));
    }

    connect(ui->btnSUsePic,SIGNAL(mouse_release()),this,SLOT(onSUse()));
    connect(ui->btnSUse,SIGNAL(mouse_press()),ui->btnSUsePic,SLOT(press()));
    connect(ui->btnSUse,SIGNAL(mouse_release()),ui->btnSUsePic,SLOT(release()));

    connect(ui->btnSTimePic,SIGNAL(mouse_release()),this,SLOT(onSTime()));
    connect(ui->btnSTime,SIGNAL(mouse_press()),ui->btnSTimePic,SLOT(press()));
    connect(ui->btnSTime,SIGNAL(mouse_release()),ui->btnSTimePic,SLOT(release()));

}

dialog_anti_vibration::~dialog_anti_vibration()
{
    delete ui;
}
void dialog_anti_vibration::showEvent(QShowEvent *)
{
    Update();
}
void dialog_anti_vibration::hideEvent(QHideEvent *)
{

}
void dialog_anti_vibration::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_anti_vibration::onClose()
{
    emit accept();
}

void dialog_anti_vibration::Update()
{
    Display_Tab(0);
}

void dialog_anti_vibration::Update(int panel_index)
{
    switch(panel_index)
    {
    case 0:
        Update_Smooth();
        break;
    case 1:

        break;
    }
}

void dialog_anti_vibration::Display_Tab(int tab_index)
{
    Panel->setCurrentIndex(tab_index);
    Update(tab_index);

    for(int i=0;i<m_vtTabLed.count();i++)
        m_vtTabLed[i]->setEnabled(i == tab_index);
}

void dialog_anti_vibration::onTab()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtTabPic.indexOf(btn);
    if(index < 0) return;

    Display_Tab(index);
}

void dialog_anti_vibration::Update_Smooth()
{
    CNRobo* pCon = CNRobo::getInstance();

    bool use; float stime;
    int ret = pCon->getSmoothOption(use, stime);
    if(ret < 0)
    {
        qDebug() << "dialog_anti_vibration::getSmoothOption ret=" << ret;
        return;
    }

    m_bSUse=use; m_fSTime=stime;
    //display
    Display_Smooth(m_bSUse, m_fSTime);
}

void dialog_anti_vibration::Display_Smooth(bool use, float stime)
{
    ui->btnSUseIcon->setEnabled(use);
    if(use)
        ui->btnSUse->setText(tr("OFF"));
    else
        ui->btnSUse->setText(tr("ON"));

    QString str;
    str.sprintf(FORMAT_TIME, stime);
    ui->btnSTime->setText(str);
}

void dialog_anti_vibration::onSUse()
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setSmoothOption(!m_bSUse, m_fSTime);
    if(ret < 0)
    {
        qDebug() << "dialog_anti_vibration::setSmoothOption ret=" << ret;
        return;
    }

    Update_Smooth();
}

void dialog_anti_vibration::onSTime()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0.0);
    np->m_numpad->SetMaxValue(10.0);
    np->m_numpad->SetSosuNum(SOSU_TIME);
    np->m_numpad->SetTitle(ui->tbSTimeTitle->text());
    np->m_numpad->SetNum((double)m_fSTime);
    if(np->exec() == QDialog::Accepted)
    {
        float stime = (float)np->m_numpad->GetNumDouble();
        CNRobo* pCon = CNRobo::getInstance();
        int ret = pCon->setSmoothOption(m_bSUse, stime);
        if(ret < 0)
        {
            qDebug() << "dialog_anti_vibration::setSmoothOption ret=" << ret;
            return;
        }

        Update_Smooth();
    }
}
