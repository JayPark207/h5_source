
#ifndef _DTP7_BASE_ETC_H_
#define _DTP7_BASE_ETC_H_

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <linux/input.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <termios.h>
#include <signal.h>

#define DEVICE_FILENAME "/dev/dtp7-etc"

#define KEYPAD_UP		103
#define KEYPAD_DOWN		108
#define KEYPAD_LEFT		105
#define KEYPAD_RIGHT	106
#define KEYPAD_F6		64
#define KEYPAD_F7		65
#define KEYPAD_F8		66
#define KEYPAD_A		30
#define KEYPAD_B		48
#define KEYPAD_C		46
#define KEYPAD_D		32
#define KEYPAD_E		18
#define KEYPAD_F		33
#define KEYPAD_G		34
#define KEYPAD_H		35
#define KEYPAD_I		23
#define KEYPAD_J		36
#define KEYPAD_K		37
#define KEYPAD_L		38

typedef struct
{
    unsigned char arg[3];
} __attribute__((packed)) dtp7_etc_arg;
 
#define DTP7_BASE_ETC_DEV    0xDC   
 
#define ETC_IOCTL_IO    _IOWR(DTP7_BASE_ETC_DEV,0,dtp7_etc_arg)
#define ETC_IOCTL_IO2    _IOR(DTP7_BASE_ETC_DEV,0,dtp7_etc_arg)
 
#define SET_LED_ONOFF                 1
#define GET_LED_ONOFF                 2
#define SET_BUZZER                    4

#define DTP7_ETC_DEV	0xDC	

#endif

