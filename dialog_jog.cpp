#include "dialog_jog.h"
#include "ui_dialog_jog.h"

dialog_jog::dialog_jog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_jog)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

#ifdef _H6_
    //wig_jogmodule = new widget_jogmodule3_p(ui->wigJog);
    wig_jogmodule = new widget_jogmodule3(ui->wigJog);
#else
    wig_jogmodule = new widget_jogmodule3(ui->wigJog);
#endif

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtAxis.clear();m_vtReal.clear();
    m_vtAxis.append(ui->tbAxis);  m_vtReal.append(ui->tbReal);
    m_vtAxis.append(ui->tbAxis_2);m_vtReal.append(ui->tbReal_2);
    m_vtAxis.append(ui->tbAxis_3);m_vtReal.append(ui->tbReal_3);
    m_vtAxis.append(ui->tbAxis_4);m_vtReal.append(ui->tbReal_4);
    m_vtAxis.append(ui->tbAxis_5);m_vtReal.append(ui->tbReal_5);
    m_vtAxis.append(ui->tbAxis_6);m_vtReal.append(ui->tbReal_6);

    Init_String();

}

dialog_jog::~dialog_jog()
{
    delete ui;
}
void dialog_jog::showEvent(QShowEvent *)
{
    timer->start();

    gReadyJog(true);
}
void dialog_jog::hideEvent(QHideEvent *)
{
    timer->stop();

    gReadyJog(false);
}
void dialog_jog::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        Init_String();
        ui->retranslateUi(this);
    }
}
void dialog_jog::onClose()
{
    emit accept();
}

void dialog_jog::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    float* trans = pCon->getCurTrans();
    float* joint = pCon->getCurJoint();

    Display_Real(trans, joint);
}
void dialog_jog::Display_Real(float *trans, float *joint)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, trans[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, trans[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, trans[UPDN]);
    m_vtReal[2]->setText(str);

    cn_joint _joint = gMakeCnJoint(joint);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, _joint));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, _joint));
    m_vtReal[4]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, _joint));
    m_vtReal[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, trans[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, trans[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, trans[UPDN]);
    m_vtReal[2]->setText(str);
    str.sprintf(FORMAT_POS, gGetWristAngle(joint[J2], joint[J3], joint[J4]));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, joint[JSWV]);
    m_vtReal[4]->setText(str);
    str.clear();
    m_vtReal[5]->setText(str);
#endif

}

void dialog_jog::Init_String()
{
    QStringList list;
    list.clear();
    list.append(tr("Traverse"));
    list.append(tr("FwdBwd"));
    list.append(tr("UpDown"));
    list.append(tr("Rotation"));
    list.append(tr("Swivel"));
#ifdef _H6_
    list.append(tr("J6"));
#else
    list.append(tr(""));
#endif

    for(int i=0;i<m_vtAxis.count();i++)
        m_vtAxis[i]->setText(list[i]);
}
