#ifndef HYSTATUS_H
#define HYSTATUS_H

#include <QString>
#include <QDateTime>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

/** 로봇상태등을 통합관리(서버로부터 한군데에서만 읽어오도록)
    탑페이지에서 대표로 업데이트하고 다른 페이지는 리드온리
    상태와 관련된 글로벌 변수버퍼역할도...
**/

class HyStatus
{
public:
    enum ENUM_ROBOT_STATUS
    {
        RBS_RDY=0,
        RBS_ERROR,
        RBS_NOT_RDY,
    };

public:
    HyStatus();

    void Update();
    void Update_Time();

    // real status (update here)
    ENUM_ROBOT_STATUS nRobotStatus;
    bool    bServo;
    bool    bJog;
    bool    bTPEnable;
    bool    bTeach;
    bool    bConnect;
    bool    bLogin;
    bool    bLock;
    bool    bEmo;
    bool    bError;
    bool    bWarn;
    bool    bErr;

    bool    bLed1;
    bool    bLed2;
    bool    bLed3;

    bool    bSafetyZone;

    QDateTime   qDateTime;
    QString     strDate;
    QString     strTime;

    CNR_TASK_STATUS MainTask;
    CNR_TASK_STATUS SubTask;

    // only buffer.(update at top_main)
    QString strNowRecipe;
    int     nMainSpeed;
    int     nUserLevel;
    bool    bFlicker;

    void SetErrCode(int code);
    int  GetErrCode(); // return now error code.

private:
    int m_nErrCode;
};

#endif // HYSTATUS_H
