#ifndef DIALOG_NETWORK_H
#define DIALOG_NETWORK_H

#include <QDialog>
#include <QStackedWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_keyboard.h"
#include "dialog_confirm.h"
#include "dialog_weight_offset.h"

#include "top_sub.h"

namespace Ui {
class dialog_network;
}

class dialog_network : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_network(QWidget *parent = 0);
    ~dialog_network();

    void Update();
    void Update(int panel_index);

private:
    Ui::dialog_network *ui;

    QTimer* timer;

    QString toQString(cn_variant data, int sosu = 0); // sosu 0,1,2
    void Enable(bool on, QWidget* wig);

    QString toIP(QStringList ip4);
    QStringList toIP(QString ip1);

    // Main Tabs
    QStackedWidget* Panel;
    QVector<QLabel3*> m_vtTabLed;
    QVector<QLabel3*> m_vtTab;
    QVector<QLabel4*> m_vtTabPic;
    void Display_Tab(int tab_index);

    // Tab1 (pop)
    // Tab1-code
    QVector<QLabel4*> m_vtCodePic;
    QVector<QLabel3*> m_vtCode, m_vtCodeName;
    void Display_Code(QVector<HyNetwork::ST_NW_VARINFO>& code_info, QVector<QLabel3*>& code_ui);
    void Display_Code(int index, float data, QVector<QLabel3*>& code_ui);
    bool IsSame_Code(QVector<HyNetwork::ST_NW_VARINFO>& code_info, QVector<QLabel3*>& code_ui);
    bool Save_Code(QVector<HyNetwork::ST_NW_VARINFO>& code_info, QVector<QLabel3*>& code_ui);
    bool Read_Code(QVector<HyNetwork::ST_NW_VARINFO>& code_info);

    // Tab1-pop setting
    QVector<QLabel3*> m_vtPopIp;
    QVector<QLabel4*> m_vtPopIpPic;
    bool Read_Pop(QVector<HyNetwork::ST_NW_VARINFO>& pop_info);
    bool Save_PopIp(QVector<HyNetwork::ST_NW_VARINFO>& popip_info);
    void Display_Pop(QVector<HyNetwork::ST_NW_VARINFO>& pop_info); // boss
    void Refresh_PopCon();
    void Display_Pop_Use(bool use);
    void Display_Pop_Connect(bool connect);
    void Display_Pop_IpPort(QString ip, int port);
    bool IsSame_Pop_IpPort(QVector<HyNetwork::ST_NW_VARINFO>& popip_info);


    // Tab2 (hy server)
    QVector<QLabel4*> m_vtCode2Pic;
    QVector<QLabel3*> m_vtCode2, m_vtCode2Name;
    // Tab2-hyserver setting
    QVector<QLabel3*> m_vtHysIp;
    QVector<QLabel4*> m_vtHysIpPic;
    bool Read_Hys(QVector<HyNetwork::ST_NW_VARINFO>& hys_info);
    bool Save_HysIp(QVector<HyNetwork::ST_NW_VARINFO>& hysip_info);
    void Display_Hys(QVector<HyNetwork::ST_NW_VARINFO>& hys_info); // boss
    void Refresh_HysCon();
    void Display_Hys_Use(bool use);
    void Display_Hys_Connect(bool connect);
    void Display_Hys_IpPort(QString ip, int port);
    bool IsSame_Hys_IpPort(QVector<HyNetwork::ST_NW_VARINFO>& hysip_info);


    // Tab3 serial
    // Tab3 - Main Tabs
    QStackedWidget* SrPanel;
    QVector<QLabel3*> m_vtSrTabLed;
    QVector<QLabel3*> m_vtSrTab;
    QVector<QLabel4*> m_vtSrTabPic;
    void Display_SrTab(int tab_index);
    void Update_Sr(int srpanel_index);

    // Tab3 - common
    bool Read_Sr(QVector<HyNetwork::ST_NW_VARINFO>& sr_info);

    // Tab3 - connect
    QVector<QLabel3*> m_vtSrCountDisp;
    QVector<QLabel4*> m_vtSrCountDispPic;

    void Display_Sr_Spec();
    void Display_Sr_Connect(bool connect);
    void Display_Sr_ConStatus(float data);
    void Display_Sr_CallStatus(float data);
    void Display_Sr_CountCall(float data);
    void Refresh_SrCon();
    void Display_Sr_Count();

    // Tab3 - Weight
    QVector<QLabel3*> m_vtWName, m_vtWUse, m_vtWMin, m_vtWMax, m_vtWVal;
    QVector<QLabel4*> m_vtWUsePic, m_vtWMinPic, m_vtWMaxPic;
    void Display_SrW(QVector<HyNetwork::ST_NW_VARINFO>& srw_info);
    bool IsSame_SrW(QVector<HyNetwork::ST_NW_VARINFO>& srw_info);
    void Refresh_SrW();
    bool m_bWMeasure, m_bWReset;
    bool Save_SrW(QVector<HyNetwork::ST_NW_VARINFO>& sr_info);

    // Tab3 - Temp.
    QVector<QLabel3*> m_vtTName, m_vtTUse, m_vtTMin, m_vtTMax, m_vtTVal;
    QVector<QLabel4*> m_vtTUsePic, m_vtTMinPic, m_vtTMaxPic;
    void Display_SrT(QVector<HyNetwork::ST_NW_VARINFO>& srt_info);
    bool IsSame_SrT(QVector<HyNetwork::ST_NW_VARINFO>& srt_info);
    void Refresh_SrT();
    bool m_bTMeasure;
    bool Save_SrT(QVector<HyNetwork::ST_NW_VARINFO>& sr_info);


    // Tab3 - E-sensor
    QVector<QLabel3*> m_vtEName, m_vtEUse, m_vtEMin, m_vtEMax, m_vtEVal;
    QVector<QLabel4*> m_vtEUsePic, m_vtEMinPic, m_vtEMaxPic;
    void Display_SrE(QVector<HyNetwork::ST_NW_VARINFO>& sre_info);
    bool IsSame_SrE(QVector<HyNetwork::ST_NW_VARINFO>& sre_info);
    void Refresh_SrE();
    bool m_bEMeasure;
    bool Save_SrE(QVector<HyNetwork::ST_NW_VARINFO>& sr_info);

    // Tab4 Test
    QStackedWidget* TsPanel;
    QVector<QLabel3*> m_vtTsTabLed, m_vtTsTab;
    QVector<QLabel4*> m_vtTsTabPic;
    void Display_TsTab(int tstab_index);
    void Update_Ts(int tspanel_index);

    // common
    bool Read_Ts(QVector<HyNetwork::ST_NW_VARINFO>& ts_info);
    bool Save_Ts(QVector<HyNetwork::ST_NW_VARINFO>& ts_info);
    bool IsSame_Ts(QVector<HyNetwork::ST_NW_VARINFO>& ts_info,
                   QVector<HyNetwork::ST_NW_VARINFO>& dp_info);

    // Tab4 - Pop test
    QVector<HyNetwork::ST_NW_VARINFO> m_Info_Pop_Protocol;
    QVector<QLabel3*> m_vtTpNo, m_vtTpName, m_vtTpData;
    QVector<QLabel4*> m_vtTpDataPic;
    int m_nStartIndex_Tp;
    void Redraw_List_Tp(int start_index);
    void Refresh_Tp();

    // Tab4 - hyserver test
    QVector<HyNetwork::ST_NW_VARINFO> m_Info_Hys_Protocol;
    QVector<QLabel3*> m_vtThNo, m_vtThName, m_vtThData;
    QVector<QLabel4*> m_vtThDataPic;
    int m_nStartIndex_Th;
    void Redraw_List_Th(int start_index);
    void Refresh_Th();


    top_sub* top;
    void SubTop();          // Need SubTop

public slots:
    void onClose();
    void onTimer();

    void onTab();

    void onCode();
    void onSave_Code();

    void onPopUse();
    void onPopConnect();
    void onPopDisconnect();
    void onPopIp();
    void onPopPort();
    void onSave_PopIp();

    void onHysUse();
    void onHysConnect();
    void onHysDisconnect();
    void onHysIp();
    void onHysPort();
    void onSave_HysIp();


    void onCode2();
    void onSave_Code2();


    void onSrTab();
    void onSrConnect();
    void onSrDisconnect();
    void onCountDisplay();
    void onErrorCode();
    void onErrorDisplay();

    void onWUse();
    void onWMin();
    void onWMax();
    void onWSave();
    void onWMeasure();
    void onWReset();
    void onWOffset();

    void onTUse();
    void onTMin();
    void onTMax();
    void onTSave();
    void onTMeasure();
    void onTReset();

    void onEUse();
    void onEMin();
    void onEMax();
    void onESave();
    void onEMeasure();
    void onEReset();


    void onTsTab();

    void onTpData();
    void onTpUp();
    void onTpDown();
    void onTpSave();
    void onTpSend();
    void onTpReset();

    void onThData();
    void onThUp();
    void onThDown();
    void onThSave();
    void onThSend();
    void onThReset();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_NETWORK_H
