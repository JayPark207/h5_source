#ifndef QLABEL5
#define QLABEL5

#include <QLabel>
#include <QMouseEvent>
#include <QVector>

#include "global.h"

/** 버튼이미지+아이콘 형태로 구성한 버튼용
    단, connect시에 텍스트와 아이콘은 전부 Pic에 시그날전달로 해야한다.
    그리고 Pic만 해당클래스로 Promote 한다.**/

class QLabel5 : public QLabel
{
    Q_OBJECT
public:
    explicit QLabel5(QWidget *parent=0):QLabel(parent)
    {
    }

signals:
    void mouse_press();
    void mouse_release();

public slots:
    void press() { mousePressEvent(0); }
    void release() { mouseReleaseEvent(0); }

protected:
    void mousePressEvent(QMouseEvent *)
    {
        gBeep();

        g_nPxmIndex = g_vtImgNoPress.indexOf(this->pixmap()->toImage());
        if(g_nPxmIndex >= 0)
            this->setPixmap(g_vtPxmPress[g_nPxmIndex]);
        else
            g_nPxmIndex = g_vtImgPress.indexOf(this->pixmap()->toImage());

        emit mouse_press();
    }

    void mouseReleaseEvent(QMouseEvent *)
    {
        if(g_nPxmIndex >= 0)
            this->setPixmap(g_vtPxmNoPress[g_nPxmIndex]);

        emit mouse_release();
    }

private:


};
#endif // QLABEL5

