#include "hypendent.h"

HyPendent::HyPendent()
{

}

bool HyPendent::Led_L1(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_L1, color) < 0)
        return false;
    return true;
}
bool HyPendent::Led_L2(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_L2, color) < 0)
        return false;
    return true;
}
bool HyPendent::Led_L3(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_L3, color) < 0)
        return false;
    return true;
}

bool HyPendent::Led_R1(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_R1, color) < 0)
        return false;
    return true;
}
bool HyPendent::Led_R2(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_R2, color) < 0)
        return false;
    return true;
}
bool HyPendent::Led_R3(CNR_LED_COLOR color)
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPLed(CNR_LED_R3, color) < 0)
        return false;
    return true;
}

bool HyPendent::Beep()
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->setDTPBuzzerOn(1, 1) < 0)
        return false;
    return true;
}

bool HyPendent::Alert(bool onoff)
{
    CNRobo* pCon = CNRobo::getInstance();

    if(onoff)
    {
        // need to api debug.
        if(pCon->setDTPBuzzerOn2(INT_MAX, 30, 10) < 0)
            return false;
    }
    else
    {
        if(pCon->setDTPBuzzerOff() < 0)
            return false;
    }
    return true;
}


