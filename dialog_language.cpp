#include "dialog_language.h"
#include "ui_dialog_language.h"

dialog_language::dialog_language(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_language)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtLanPic.clear();m_vtLanIcon.clear();m_vtLan.clear();
    m_vtLanPic.append(ui->btnLanPic);   m_vtLanIcon.append(ui->btnLanIcon);   m_vtLan.append(ui->btnLan);
    m_vtLanPic.append(ui->btnLanPic_2); m_vtLanIcon.append(ui->btnLanIcon_2); m_vtLan.append(ui->btnLan_2);
    m_vtLanPic.append(ui->btnLanPic_3); m_vtLanIcon.append(ui->btnLanIcon_3); m_vtLan.append(ui->btnLan_3);
    m_vtLanPic.append(ui->btnLanPic_4); m_vtLanIcon.append(ui->btnLanIcon_4); m_vtLan.append(ui->btnLan_4);
    m_vtLanPic.append(ui->btnLanPic_5); m_vtLanIcon.append(ui->btnLanIcon_5); m_vtLan.append(ui->btnLan_5);
    m_vtLanPic.append(ui->btnLanPic_6); m_vtLanIcon.append(ui->btnLanIcon_6); m_vtLan.append(ui->btnLan_6);
    m_vtLanPic.append(ui->btnLanPic_7); m_vtLanIcon.append(ui->btnLanIcon_7); m_vtLan.append(ui->btnLan_7);
    m_vtLanPic.append(ui->btnLanPic_8); m_vtLanIcon.append(ui->btnLanIcon_8); m_vtLan.append(ui->btnLan_8);
    m_vtLanPic.append(ui->btnLanPic_9); m_vtLanIcon.append(ui->btnLanIcon_9); m_vtLan.append(ui->btnLan_9);
    m_vtLanPic.append(ui->btnLanPic_10);m_vtLanIcon.append(ui->btnLanIcon_10);m_vtLan.append(ui->btnLan_10);
    m_vtLanPic.append(ui->btnLanPic_11);m_vtLanIcon.append(ui->btnLanIcon_11);m_vtLan.append(ui->btnLan_11);
    m_vtLanPic.append(ui->btnLanPic_12);m_vtLanIcon.append(ui->btnLanIcon_12);m_vtLan.append(ui->btnLan_12);
    m_vtLanPic.append(ui->btnLanPic_13);m_vtLanIcon.append(ui->btnLanIcon_13);m_vtLan.append(ui->btnLan_13);
    m_vtLanPic.append(ui->btnLanPic_14);m_vtLanIcon.append(ui->btnLanIcon_14);m_vtLan.append(ui->btnLan_14);
    m_vtLanPic.append(ui->btnLanPic_15);m_vtLanIcon.append(ui->btnLanIcon_15);m_vtLan.append(ui->btnLan_15);

    int i;
    for(i=0;i<m_vtLanPic.count();i++)
    {
        connect(m_vtLanPic[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtLan[i],SIGNAL(mouse_press()),m_vtLanPic[i],SLOT(press()));
        connect(m_vtLan[i],SIGNAL(mouse_release()),m_vtLanPic[i],SLOT(release()));
        connect(m_vtLanIcon[i],SIGNAL(mouse_press()),m_vtLanPic[i],SLOT(press()));
        connect(m_vtLanIcon[i],SIGNAL(mouse_release()),m_vtLanPic[i],SLOT(release()));
    }

    top = 0;
}

dialog_language::~dialog_language()
{
    delete ui;
}
void dialog_language::showEvent(QShowEvent *)
{
    Update();
    SubTop();
}
void dialog_language::hideEvent(QHideEvent *)
{

}
void dialog_language::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
        Update();
    }
}
void dialog_language::onClose()
{
    emit accept();
}

void dialog_language::onSelect()
{
    QLabel4* flag = (QLabel4*)sender();
    int index = m_vtLanPic.indexOf(flag);
    if(index < 0) return;

    if(Param->Get(HyParam::LANGUAGE).toInt() == index) return;

    // change font
    gChange_Font((HyParam::PARAM_LANGUAGE)index);

    // change language
    gLocale = Language->m_Locale[index];
    QApplication::removeTranslator(&gTranslator);
    gTranslator.load(Language->m_QmFile[index].toAscii().constData());
    QApplication::installTranslator(&gTranslator);
    Param->Set(HyParam::LANGUAGE, QString().setNum(index));

    Display_Select(index);
}

void dialog_language::Update()
{
    for(int i=0;i<m_vtLanPic.count();i++)
    {
        if(i < Language->m_Flag.size())
        {
            m_vtLanPic[i]->setEnabled(true);
            m_vtLan[i]->setEnabled(true);
            m_vtLanIcon[i]->setEnabled(true);

            m_vtLanIcon[i]->setPixmap(Language->m_Flag[i]);
            m_vtLan[i]->setText(Language->m_LangName[i]);
        }
        else
        {
            m_vtLanPic[i]->setEnabled(false);
            m_vtLan[i]->setEnabled(false);
            m_vtLanIcon[i]->setEnabled(false);

            m_vtLanIcon[i]->setPixmap(0);
            m_vtLan[i]->setText("");
        }
    }

    Display_Select(Param->Get(HyParam::LANGUAGE).toInt());
}

void dialog_language::Display_Select(int lang_index)
{
    for(int i=0;i<m_vtLanPic.count();i++)
        m_vtLanPic[i]->setAutoFillBackground(i==lang_index);
}

void dialog_language::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
