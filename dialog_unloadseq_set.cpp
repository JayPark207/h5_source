#include "dialog_unloadseq_set.h"
#include "ui_dialog_unloadseq_set.h"

dialog_unloadseq_set::dialog_unloadseq_set(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_unloadseq_set)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    Stacked = new QStackedWidget(ui->frame);
    Stacked->setGeometry(ui->wigOffset->x(),
                         ui->wigOffset->y(),
                         ui->wigOffset->width(),
                         ui->wigOffset->height());
    Stacked->addWidget(ui->wigOffset);
    Stacked->addWidget(ui->wigOutput);
    Stacked->addWidget(ui->wigOutput_2);
    Stacked->addWidget(ui->wigHeight);
    Stacked->setCurrentIndex(0);
    Stacked->show();

    m_vtPosPic.clear();m_vtPos.clear();
    m_vtPosPic.append(ui->btnPosPic);  m_vtPos.append(ui->btnPos);
    m_vtPosPic.append(ui->btnPosPic_2);m_vtPos.append(ui->btnPos_2);
    m_vtPosPic.append(ui->btnPosPic_3);m_vtPos.append(ui->btnPos_3);
    m_vtPosPic.append(ui->btnPosPic_4);m_vtPos.append(ui->btnPos_4);

    int i;
    for(i=0;i<m_vtPosPic.count();i++)
    {
        connect(m_vtPosPic[i],SIGNAL(mouse_release()),this,SLOT(onPos()));
        connect(m_vtPos[i],SIGNAL(mouse_press()),m_vtPosPic[i],SLOT(press()));
        connect(m_vtPos[i],SIGNAL(mouse_release()),m_vtPosPic[i],SLOT(release()));
    }

    m_vtSelPic.clear();m_vtSel.clear();
    m_vtSelPic.append(ui->btnOffsetPic);m_vtSel.append(ui->btnOffset);
    m_vtSelPic.append(ui->btnOutputPic);m_vtSel.append(ui->btnOutput);

    for(i=0;i<m_vtSelPic.count();i++)
    {
        connect(m_vtSelPic[i],SIGNAL(mouse_release()),this,SLOT(onSel()));
        connect(m_vtSel[i],SIGNAL(mouse_press()),m_vtSelPic[i],SLOT(press()));
        connect(m_vtSel[i],SIGNAL(mouse_release()),m_vtSelPic[i],SLOT(release()));
    }

    m_vtOPagePic.clear();m_vtOPageIcon.clear();
    m_vtOPagePic.append(ui->oPagePic_2);m_vtOPageIcon.append(ui->oPageIcon_2);
    m_vtOPagePic.append(ui->oPagePic);  m_vtOPageIcon.append(ui->oPageIcon);

    for(i=0;i<m_vtOPagePic.count();i++)
    {
        connect(m_vtOPagePic[i],SIGNAL(mouse_release()),this,SLOT(onOutputPage()));
        connect(m_vtOPageIcon[i],SIGNAL(mouse_press()),m_vtOPagePic[i],SLOT(press()));
        connect(m_vtOPageIcon[i],SIGNAL(mouse_release()),m_vtOPagePic[i],SLOT(release()));
    }

    // Number
    connect(ui->tbNumPic,SIGNAL(mouse_release()),this,SLOT(onNum()));
    connect(ui->tbNum,SIGNAL(mouse_press()),ui->tbNumPic,SLOT(press()));
    connect(ui->tbNum,SIGNAL(mouse_release()),ui->tbNumPic,SLOT(release()));

    // Offset table
    m_vtName.clear();
    m_vtName.append(ui->tbName);
    m_vtName.append(ui->tbName_2);
    m_vtName.append(ui->tbName_3);

    m_vtOffsetPic.clear();m_vtOffset.clear();
    m_vtOffsetPic.append(ui->tbOffsetPic);  m_vtOffset.append(ui->tbOffset);
    m_vtOffsetPic.append(ui->tbOffsetPic_2);m_vtOffset.append(ui->tbOffset_2);
    m_vtOffsetPic.append(ui->tbOffsetPic_3);m_vtOffset.append(ui->tbOffset_3);

    for(i=0;i<m_vtOffsetPic.count();i++)
    {
        connect(m_vtOffsetPic[i],SIGNAL(mouse_release()),this,SLOT(onOffset()));
        connect(m_vtOffset[i],SIGNAL(mouse_press()),m_vtOffsetPic[i],SLOT(press()));
        connect(m_vtOffset[i],SIGNAL(mouse_release()),m_vtOffsetPic[i],SLOT(release()));
    }

    // output
    m_vtOPic.clear();m_vtOIcon.clear();
    m_vtOPic.append(ui->oPic);  m_vtOIcon.append(ui->oIcon);
    m_vtOPic.append(ui->oPic_2);m_vtOIcon.append(ui->oIcon_2);
    m_vtOPic.append(ui->oPic_3);m_vtOIcon.append(ui->oIcon_3);
    m_vtOPic.append(ui->oPic_4);m_vtOIcon.append(ui->oIcon_4);
    m_vtOPic.append(ui->oPic_5);m_vtOIcon.append(ui->oIcon_5);
    m_vtOPic.append(ui->oPic_6);m_vtOIcon.append(ui->oIcon_6);
    m_vtOPic.append(ui->oPic_7);m_vtOIcon.append(ui->oIcon_7);
    m_vtOPic.append(ui->oPic_8);m_vtOIcon.append(ui->oIcon_8);
    m_vtOPic.append(ui->oPic_9);m_vtOIcon.append(ui->oIcon_9);
    m_vtOPic.append(ui->oPic_10);m_vtOIcon.append(ui->oIcon_10);

    m_vtOName.clear();m_vtOSel.clear();
    m_vtOName.append(ui->oName);  m_vtOSel.append(ui->oSel);
    m_vtOName.append(ui->oName_2);m_vtOSel.append(ui->oSel_2);
    m_vtOName.append(ui->oName_3);m_vtOSel.append(ui->oSel_3);
    m_vtOName.append(ui->oName_4);m_vtOSel.append(ui->oSel_4);
    m_vtOName.append(ui->oName_5);m_vtOSel.append(ui->oSel_5);
    m_vtOName.append(ui->oName_6);m_vtOSel.append(ui->oSel_6);
    m_vtOName.append(ui->oName_7);m_vtOSel.append(ui->oSel_7);
    m_vtOName.append(ui->oName_8);m_vtOSel.append(ui->oSel_8);
    m_vtOName.append(ui->oName_9);m_vtOSel.append(ui->oSel_9);
    m_vtOName.append(ui->oName_10);m_vtOSel.append(ui->oSel_10);

    for(i=0;i<m_vtOPic.count();i++)
    {
        connect(m_vtOPic[i],SIGNAL(mouse_release()),this,SLOT(onOutput()));
        connect(m_vtOIcon[i],SIGNAL(mouse_press()),m_vtOPic[i],SLOT(press()));
        connect(m_vtOIcon[i],SIGNAL(mouse_release()),m_vtOPic[i],SLOT(release()));
        connect(m_vtOName[i],SIGNAL(mouse_press()),m_vtOPic[i],SLOT(press()));
        connect(m_vtOName[i],SIGNAL(mouse_release()),m_vtOPic[i],SLOT(release()));
        connect(m_vtOSel[i],SIGNAL(mouse_press()),m_vtOPic[i],SLOT(press()));
        connect(m_vtOSel[i],SIGNAL(mouse_release()),m_vtOPic[i],SLOT(release()));
    }

    connect(ui->tbHeightPic,SIGNAL(mouse_release()),this,SLOT(onHeight()));
    connect(ui->tbHeight,SIGNAL(mouse_press()),ui->tbHeightPic,SLOT(press()));
    connect(ui->tbHeight,SIGNAL(mouse_release()),ui->tbHeightPic,SLOT(release()));

}

dialog_unloadseq_set::~dialog_unloadseq_set()
{
    delete ui;
}
void dialog_unloadseq_set::showEvent(QShowEvent *)
{
    Display_Pos(0);
    Display_Sel(0);
    Update(0);
}
void dialog_unloadseq_set::hideEvent(QHideEvent *)
{

}
void dialog_unloadseq_set::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_unloadseq_set::onClose()
{
    emit accept();
}

void dialog_unloadseq_set::onPos()
{
    QLabel4* pos = (QLabel4*)sender();
    int index = m_vtPosPic.indexOf(pos);
    if(index < 0) return;

    //Display_Sel(0); // if change pos, always display offset.
    Display_Pos(index);
    Update(index);

}
void dialog_unloadseq_set::onSel()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtSelPic.indexOf(sel);
    if(index < 0) return;

    Display_Sel(index);
}
void dialog_unloadseq_set::Display_Sel(int sel_index)
{
    for(int i=0;i<m_vtSelPic.count();i++)
        m_vtSelPic[i]->setAutoFillBackground(i==sel_index);

    if(m_vtPosPic[0]->autoFillBackground() && sel_index == 0) // pos 1
        Stacked->setCurrentWidget(ui->wigHeight);
    else // other.
        Stacked->setCurrentIndex(sel_index);
}
void dialog_unloadseq_set::Display_Pos(int pos_index)
{
    int i;
    for(i=0;i<m_vtPosPic.count();i++)
        m_vtPosPic[i]->setAutoFillBackground(i==pos_index);

    if(pos_index == 0)
        ui->lbDispNum->setText("");
    else
        ui->lbDispNum->setText(QString().setNum(pos_index+1));

    for(i=0;i<m_vtSelPic.count();i++)
    {
        if(m_vtSelPic[i]->autoFillBackground())
            Display_Sel(i);
    }
}

void dialog_unloadseq_set::Update(int pos_index)
{
    Vars.clear();
    Datas.clear();

    Vars.append(HyRecipe::vSeqX1);
    Vars.append(HyRecipe::vSeqY1);
    Vars.append(HyRecipe::vSeqZ1);
    Vars.append(HyRecipe::vSeqX2);
    Vars.append(HyRecipe::vSeqY2);
    Vars.append(HyRecipe::vSeqZ2);
    Vars.append(HyRecipe::vSeqX3);
    Vars.append(HyRecipe::vSeqY3);
    Vars.append(HyRecipe::vSeqZ3);
    Vars.append(HyRecipe::vSeqHeight);
    Vars.append(HyRecipe::vSeqNum);

    m_nSeperate = Vars.size();

    Vars.append(HyRecipe::vSeqVac1);
    Vars.append(HyRecipe::vSeqVac2);
    Vars.append(HyRecipe::vSeqVac3);
    Vars.append(HyRecipe::vSeqVac4);
    Vars.append(HyRecipe::vSeqChuck);
    Vars.append(HyRecipe::vSeqGrip);
    Vars.append(HyRecipe::vSeqUser1);
    Vars.append(HyRecipe::vSeqUser2);
    Vars.append(HyRecipe::vSeqUser3);
    Vars.append(HyRecipe::vSeqUser4);

    if(Recipe->Gets(Vars, Datas))
    {
        Data_Offset.clear();
        Data_Output.clear();

        // pos1 data. always zero.
        Data_Offset.append(0.0);
        Data_Offset.append(0.0);
        Data_Offset.append(0.0);

        for(int i=0;i<Datas.count();i++)
        {
            if(i < m_nSeperate)
                Data_Offset.append(Datas[i]);
            else
                Data_Output.append(Datas[i]);
        }

        Display_Data(pos_index);
        Update_Output();
    }

}

void dialog_unloadseq_set::Display_Data(int pos_index)
{
    QString str;
    for(int i=0;i<m_vtOffset.count();i++)
    {
        str.sprintf(FORMAT_POS, Data_Offset[(pos_index*3) + i]);
        m_vtOffset[i]->setText(str);
    }
    // height
    int index = Vars.indexOf(HyRecipe::vSeqHeight);
    if(index >= 0)
    {
        str.sprintf(FORMAT_POS, Datas[index]);
        ui->tbHeight->setText(str);
    }

    // Seq. Pos Number
    str.sprintf("%d", (int)Data_Offset.last());
    ui->tbNum->setText(str);

    Enable_Pos((int)Data_Offset.last());
}

void dialog_unloadseq_set::Enable_Pos(int num)
{
    for(int i=0;i<m_vtPosPic.count();i++)
    {
        m_vtPosPic[i]->setEnabled(i < num);
        m_vtPos[i]->setEnabled(i < num);
    }
}

void dialog_unloadseq_set::onNum()
{
    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(1);
    np->m_numpad->SetMaxValue(4);
    np->m_numpad->SetSosuNum(0);
    np->m_numpad->SetTitle(ui->tbNumName->text());
    np->m_numpad->SetNum(ui->tbNum->text().toDouble());
    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(HyRecipe::vSeqNum, (float)np->m_numpad->GetNumDouble()))
        {
            Clear_Output((int)np->m_numpad->GetNumDouble());
            Display_Sel(0);
            Display_Pos(0);
            Update(0);
        }
    }
}

void dialog_unloadseq_set::onOffset()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtOffsetPic.indexOf(sel);
    if(index < 0) return;

    int pos_index = Get_NowPosIndex();
    if(pos_index < 1) return;

    int datas_index = ((pos_index-1)*3 + index);
    int vars_index  = datas_index;

    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-99999.999);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(SOSU_POS);
    QString str;
    str = m_vtName[index]->text() + " " + ui->tbOffsetTitle->text();
    np->m_numpad->SetTitle(str);
    np->m_numpad->SetNum((double)Datas[datas_index]);
    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(Vars[vars_index], (float)np->m_numpad->GetNumDouble()))
            Update(pos_index);
    }
}

int dialog_unloadseq_set::Get_NowPosIndex()
{
    for(int i=0;i<m_vtPosPic.count();i++)
    {
        if(m_vtPosPic[i]->autoFillBackground())
            return i;
    }

    return -1;
}

void dialog_unloadseq_set::Update_Output()
{
    float data;
    m_slOutput.clear();
    for(int i=0;i<m_vtOPic.count();i++)
    {
        data = Data_Output[i];

        if(data <= 0)
        {
            m_vtOSel[i]->hide();
            m_vtOSel[i]->setEnabled(true);
            m_vtOPic[i]->setEnabled(true);
            m_vtOIcon[i]->setEnabled(true);
            m_vtOName[i]->setEnabled(true);
        }
        else
        {
            m_vtOSel[i]->show();
            m_vtOSel[i]->setText(QString().setNum(data));

            if(data != (Get_NowPosIndex()+1))
            {
                m_vtOSel[i]->setEnabled(false);m_vtOPic[i]->setEnabled(false);
                m_vtOIcon[i]->setEnabled(false);m_vtOName[i]->setEnabled(false);
            }
            else
            {
                m_vtOSel[i]->setEnabled(true);m_vtOPic[i]->setEnabled(true);
                m_vtOIcon[i]->setEnabled(true);m_vtOName[i]->setEnabled(true);
                m_slOutput.append(m_vtOName[i]->text());
            }
        }

    }

    // display output
    QString str = m_slOutput.join(",");
    ui->lbDispOutput->setText(str);
}

void dialog_unloadseq_set::onOutput()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtOPic.indexOf(sel);
    if(index < 0) return;

    int pos_index = Get_NowPosIndex();
    if(pos_index < 0) return;

    int datas_index = m_nSeperate + index;
    int vars_index = datas_index;


    if((int)Datas[datas_index] > 0)
        Recipe->Set(Vars[vars_index], 0); // select off.
    else
        Recipe->Set(Vars[vars_index], (pos_index+1));

    Update(pos_index);
}

void dialog_unloadseq_set::onOutputPage()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtOPagePic.indexOf(sel);
    if(index < 0) return;

    Stacked->setCurrentIndex(index+1);
}

void dialog_unloadseq_set::Clear_Output(int number)
{
    QVector<HyRecipe::RECIPE_NUMBER> var;
    QVector<float> dat;

    int var_index;
    for(int i=0;i<Data_Output.count();i++)
    {
        if(Data_Output[i] > number)
        {
            var_index = m_nSeperate + i;
            var.append(Vars[var_index]);
            dat.append(0.0);
        }
    }

    Recipe->Sets(var, dat);
}

void dialog_unloadseq_set::onHeight()
{
    np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(0);
    np->m_numpad->SetMaxValue(99999.999);
    np->m_numpad->SetSosuNum(SOSU_POS);
    np->m_numpad->SetTitle(ui->tbHeightTitle->text());

    int datas_index = Vars.indexOf(HyRecipe::vSeqHeight);
    if(datas_index >= 0)
        np->m_numpad->SetNum((double)Datas[datas_index]);

    if(np->exec() == QDialog::Accepted)
    {
        if(Recipe->Set(Vars[datas_index], (float)np->m_numpad->GetNumDouble()))
            Update(0); // only pos1
    }
}
