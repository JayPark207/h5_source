#ifndef PAGE_MAIN2_LOG_H
#define PAGE_MAIN2_LOG_H

#include <QWidget>
#include "ui_page_main2_log.h"
#include "global.h"

namespace Ui {
class page_main2_log;
}

class page_main2_log : public QWidget
{
    Q_OBJECT
    
public:
    explicit page_main2_log(QWidget *parent = 0);
    ~page_main2_log();

private:
    Ui::page_main2_log *ui;

protected:
    void changeEvent(QEvent *);
};

#endif // PAGE_MAIN2_LOG_H
