#ifndef NUMPAD_H
#define NUMPAD_H

#include <QWidget>
#include <QTimer>

#include "qlabel3.h"
#include "qlabel4.h"


namespace Ui {
class numpad;
}

class numpad : public QWidget
{
    Q_OBJECT
    
public:
    explicit numpad(QWidget *parent = 0);
    ~numpad();

    void SetNum(QString num);
    void SetNum(double num);
    QString GetNum();
    double GetNumDouble();

    void SetMask(QString mask);

    bool CheckValidNum(QString num);
    bool CheckValidNum_PlusMinus(QString num);
    bool CheckMin(QString num);
    bool CheckMax(QString num);

    bool IsMin();
    bool IsMax();


    void SetMinValue(QString val);
    void SetMinValue(double val);
    void SetMaxValue(QString val);
    void SetMaxValue(double val);

    void SetSosuNum(int num); // 소수 자리수 설정 0~N.

    void SetTitle(QString title);

signals:
    void Enter();
    void Cancel();


private slots:
    void onNumpad();
    void onESC();
    void onOK();
    void onPM();
    void onBS();
    void onAC();

    void on_txtView_textChanged(const QString &arg1);

    void onSelectNum();
    void onPlusMinus();

    void onTimer();


private:
    Ui::numpad *ui;

    QString m_strOutputNum;
    QString m_strOldNum;
    QString m_MinVal;
    QString m_MaxVal;
    int     m_nSosuNum; // .밑에 입력 가능한 수 제어

    void ChangeSelectValue(int index); // 0~3
    double m_dbSelectValue;

    QTimer* timer;

    QVector<QLabel4*> m_vtNoPic;
    QVector<QLabel3*> m_vtNo;
    QVector<QLabel4*> m_vtSelPic;
    QVector<QLabel3*> m_vtSel;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // NUMPAD_H
