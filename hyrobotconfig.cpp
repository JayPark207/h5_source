#include "hyrobotconfig.h"

HyRobotConfig::HyRobotConfig()
{

}

bool HyRobotConfig::Read()
{
    CNRobo* pCon = CNRobo::getInstance();

    m_slRawData.clear();
    int ret = pCon->getRobotConf(m_slRawData);
    qDebug() << "getRobotConf ret =" << ret;
    if(ret < 0)
        return false;

    for(int i=0;i<m_slRawData.count();i++)
        qDebug() << m_slRawData[i];

    return true;
}

bool HyRobotConfig::Write()
{
    // test complete and open.
    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->setRobotConf(m_slRawData);
    qDebug() << "setRobotConf ret =" << ret;
    if(ret < 0)
        return false;

    for(int i=0;i<m_slRawData.count();i++)
        qDebug() << m_slRawData[i];

    return true;
}

bool HyRobotConfig::Get(QString name, QStringList& value)
{
    int i;
    int index = -1;

    value.clear();

    for(i=0;i<m_slRawData.count();i++)
    {
        if(m_slRawData[i].startsWith("#"))
            continue;

        if(m_slRawData[i].contains(name))
        {
            index = i;
            break;
        }
    }

    qDebug() << "Get() m_slRawData index=" << index;
    if(index < 0)
        return false;

    QString val = m_slRawData[index];
    int _pos = val.indexOf("=");
    val.remove(0,_pos+1).trimmed();


    if(val.indexOf(",") < 0)
        value.append(val);
    else
    {
        value = val.split(",");
    }

    for(i=0;i<value.count();i++)
    {
        value[i] = value[i].trimmed();
        qDebug() << value[i];
    }

    return true;
}

bool HyRobotConfig::Set(QString name, QStringList value)
{
    int i;
    int index = -1;


    if(value.size() < 1)
    {
        qDebug() << "Set() Value count 0 Error!!";
        return false;
    }

    for(i=0;i<m_slRawData.count();i++)
    {
        if(m_slRawData[i].startsWith("#"))
            continue;

        if(m_slRawData[i].contains(name))
        {
            index = i;
            break;
        }
    }

    qDebug() << "Set() m_slRawData index=" << index;
    if(index < 0)
        return false;

    QString line = name;
    line += QString(" = ");

    for(i=0;i<value.count();i++)
    {
        line += value[i];
        line += QString(", ");
    }

    line = line.trimmed();
    if(line.endsWith(","))
        line.remove(line.size()-1,1);

    m_slRawData[index] = line;
    qDebug() << m_slRawData[index];

    return true;
}
