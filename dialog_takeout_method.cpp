#include "dialog_takeout_method.h"
#include "ui_dialog_takeout_method.h"

dialog_takeout_method::dialog_takeout_method(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_takeout_method)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtPic.clear();m_vtIcon.clear();
    m_vtPic.append(ui->btnPic);   m_vtIcon.append(ui->btnIcon);
    m_vtPic.append(ui->btnPic_2); m_vtIcon.append(ui->btnIcon_2);
    m_vtPic.append(ui->btnPic_3); m_vtIcon.append(ui->btnIcon_3);
    m_vtPic.append(ui->btnPic_4); m_vtIcon.append(ui->btnIcon_4);
    m_vtPic.append(ui->btnPic_5); m_vtIcon.append(ui->btnIcon_5);
    m_vtPic.append(ui->btnPic_6); m_vtIcon.append(ui->btnIcon_6);
    m_vtPic.append(ui->btnPic_7); m_vtIcon.append(ui->btnIcon_7);
    m_vtPic.append(ui->btnPic_8); m_vtIcon.append(ui->btnIcon_8);
    m_vtPic.append(ui->btnPic_9); m_vtIcon.append(ui->btnIcon_9);
    m_vtPic.append(ui->btnPic_10);m_vtIcon.append(ui->btnIcon_10);
    m_vtPic.append(ui->btnPic_11);m_vtIcon.append(ui->btnIcon_11);
    m_vtPic.append(ui->btnPic_12);m_vtIcon.append(ui->btnIcon_12);

    m_vtName.clear();m_vtSel.clear();
    m_vtName.append(ui->btnName);   m_vtSel.append(ui->btnSel);
    m_vtName.append(ui->btnName_2); m_vtSel.append(ui->btnSel_2);
    m_vtName.append(ui->btnName_3); m_vtSel.append(ui->btnSel_3);
    m_vtName.append(ui->btnName_4); m_vtSel.append(ui->btnSel_4);
    m_vtName.append(ui->btnName_5); m_vtSel.append(ui->btnSel_5);
    m_vtName.append(ui->btnName_6); m_vtSel.append(ui->btnSel_6);
    m_vtName.append(ui->btnName_7); m_vtSel.append(ui->btnSel_7);
    m_vtName.append(ui->btnName_8); m_vtSel.append(ui->btnSel_8);
    m_vtName.append(ui->btnName_9); m_vtSel.append(ui->btnSel_9);
    m_vtName.append(ui->btnName_10);m_vtSel.append(ui->btnSel_10);
    m_vtName.append(ui->btnName_11);m_vtSel.append(ui->btnSel_11);
    m_vtName.append(ui->btnName_12);m_vtSel.append(ui->btnSel_12);

    for(int i=0;i<m_vtPic.count();i++)
    {
        connect(m_vtPic[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtIcon[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtIcon[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
        connect(m_vtName[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtName[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
        connect(m_vtSel[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtSel[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
    }

}

dialog_takeout_method::~dialog_takeout_method()
{
    delete ui;
}

void dialog_takeout_method::showEvent(QShowEvent *)
{
    Update();
    m_bSave = false;
}
void dialog_takeout_method::hideEvent(QHideEvent *)
{
    if(m_bSave)
        Recipe->SaveVariable();
}
void dialog_takeout_method::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_takeout_method::Update()
{
    vars.clear();                       outnums.clear();
    vars.append(HyRecipe::mdToutVac1);  outnums.append(HyOutput::VAC1);
    vars.append(HyRecipe::mdToutVac2);  outnums.append(HyOutput::VAC2);
    vars.append(HyRecipe::mdToutVac3);  outnums.append(HyOutput::VAC3);
    vars.append(HyRecipe::mdToutVac4);  outnums.append(HyOutput::VAC4);
    vars.append(HyRecipe::mdToutChuck); outnums.append(HyOutput::CHUCK);
    vars.append(HyRecipe::mdToutGrip);  outnums.append(HyOutput::GRIP);
    vars.append(HyRecipe::mdToutUser1); outnums.append(HyOutput::USER1);
    vars.append(HyRecipe::mdToutUser2); outnums.append(HyOutput::USER2);
    vars.append(HyRecipe::mdToutUser3); outnums.append(HyOutput::USER3);
    vars.append(HyRecipe::mdToutUser4); outnums.append(HyOutput::USER4);

    if(Recipe->Gets(vars, datas))
    {
        Display_Check();
    }
}

void dialog_takeout_method::Display_Check()
{
    for(int i=0;i<m_vtPic.count();i++)
    {
        if(i < datas.size())
        {
            // use slot.
            m_vtPic[i]->setEnabled(true);
            m_vtIcon[i]->setEnabled(true);
            m_vtName[i]->setEnabled(true);
            m_vtSel[i]->setEnabled(true);

            if((int)datas[i] == 0)
                m_vtSel[i]->hide();
            else
                m_vtSel[i]->show();

        }
        else
        {
            // reserved slot.
            m_vtPic[i]->setEnabled(false);
            m_vtIcon[i]->setEnabled(false);
            m_vtName[i]->setEnabled(false);
            m_vtSel[i]->setEnabled(false);

            m_vtSel[i]->hide();
        }
    }
}

void dialog_takeout_method::onSelect()
{

    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtPic.indexOf(sel);
    if(index < 0) return;


    if((int)datas[index] != 0)
        Recipe->Set(vars[index], 0.0, false);
    else
        Recipe->Set(vars[index], outnums[index], false);

    m_bSave = true;
    Update();
}
