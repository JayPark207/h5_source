#include "dialog_log.h"
#include "ui_dialog_log.h"

dialog_log::dialog_log(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_log)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    ui->grpList->setGeometry(ui->grpCalendar->x(),
                             ui->grpCalendar->y(),
                             ui->grpList->width(),
                             ui->grpList->height());
    ui->grpListControl->setGeometry(ui->grpList->x()+ ui->grpList->width(),
                                    ui->grpControl->y(),
                                    ui->grpListControl->width(),
                                    ui->grpListControl->height());

    ui->grpList->hide();
    ui->grpListControl->hide();

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnReturnPic,SIGNAL(mouse_release()),this,SLOT(onReturn()));
    connect(ui->btnReturnIcon,SIGNAL(mouse_press()),ui->btnReturnPic,SLOT(press()));
    connect(ui->btnReturnIcon,SIGNAL(mouse_release()),ui->btnReturnPic,SLOT(release()));

    connect(ui->btnNextPic,SIGNAL(mouse_release()),this,SLOT(onNext()));
    connect(ui->btnNextIcon,SIGNAL(mouse_press()),ui->btnNextPic,SLOT(press()));
    connect(ui->btnNextIcon,SIGNAL(mouse_release()),ui->btnNextPic,SLOT(release()));
    connect(ui->btnBackPic,SIGNAL(mouse_release()),this,SLOT(onBack()));
    connect(ui->btnBackIcon,SIGNAL(mouse_press()),ui->btnBackPic,SLOT(press()));
    connect(ui->btnBackIcon,SIGNAL(mouse_release()),ui->btnBackPic,SLOT(release()));

    connect(ui->btnViewPic,SIGNAL(mouse_release()),this,SLOT(onView()));
    connect(ui->btnView,SIGNAL(mouse_press()),ui->btnViewPic,SLOT(press()));
    connect(ui->btnView,SIGNAL(mouse_release()),ui->btnViewPic,SLOT(release()));
    connect(ui->btnViewIcon,SIGNAL(mouse_press()),ui->btnViewPic,SLOT(press()));
    connect(ui->btnViewIcon,SIGNAL(mouse_release()),ui->btnViewPic,SLOT(release()));

    connect(ui->btnSelPic1,SIGNAL(mouse_release()),this,SLOT(onFrom()));
    connect(ui->btnSelIcon1,SIGNAL(mouse_press()),ui->btnSelPic1,SLOT(press()));
    connect(ui->btnSelIcon1,SIGNAL(mouse_release()),ui->btnSelPic1,SLOT(release()));

    connect(ui->btnSelPic2,SIGNAL(mouse_release()),this,SLOT(onTo()));
    connect(ui->btnSelIcon2,SIGNAL(mouse_press()),ui->btnSelPic2,SLOT(press()));
    connect(ui->btnSelIcon2,SIGNAL(mouse_release()),ui->btnSelPic2,SLOT(release()));

    connect(ui->btnDayPic,SIGNAL(mouse_release()),this,SLOT(onSelect()));
    connect(ui->btnDay,SIGNAL(mouse_press()),ui->btnDayPic,SLOT(press()));
    connect(ui->btnDay,SIGNAL(mouse_release()),ui->btnDayPic,SLOT(release()));
    connect(ui->btnRangePic,SIGNAL(mouse_release()),this,SLOT(onSelect()));
    connect(ui->btnRange,SIGNAL(mouse_press()),ui->btnRangePic,SLOT(press()));
    connect(ui->btnRange,SIGNAL(mouse_release()),ui->btnRangePic,SLOT(release()));


    /** List Part **/
    connect(ui->btnExtPic,SIGNAL(mouse_release()),this,SLOT(onExt()));
    connect(ui->btnExtIcon,SIGNAL(mouse_press()),ui->btnExtPic,SLOT(press()));
    connect(ui->btnExtIcon,SIGNAL(mouse_release()),ui->btnExtPic,SLOT(release()));

    m_vtNo.clear();m_vtDate.clear();m_vtIcon.clear();
    m_vtNo.append(ui->tbNo);   m_vtDate.append(ui->tbTime);   m_vtIcon.append(ui->tbIcon);
    m_vtNo.append(ui->tbNo_2); m_vtDate.append(ui->tbTime_2); m_vtIcon.append(ui->tbIcon_2);
    m_vtNo.append(ui->tbNo_3); m_vtDate.append(ui->tbTime_3); m_vtIcon.append(ui->tbIcon_3);
    m_vtNo.append(ui->tbNo_4); m_vtDate.append(ui->tbTime_4); m_vtIcon.append(ui->tbIcon_4);
    m_vtNo.append(ui->tbNo_5); m_vtDate.append(ui->tbTime_5); m_vtIcon.append(ui->tbIcon_5);
    m_vtNo.append(ui->tbNo_6); m_vtDate.append(ui->tbTime_6); m_vtIcon.append(ui->tbIcon_6);
    m_vtNo.append(ui->tbNo_7); m_vtDate.append(ui->tbTime_7); m_vtIcon.append(ui->tbIcon_7);
    m_vtNo.append(ui->tbNo_8); m_vtDate.append(ui->tbTime_8); m_vtIcon.append(ui->tbIcon_8);
    m_vtNo.append(ui->tbNo_9); m_vtDate.append(ui->tbTime_9); m_vtIcon.append(ui->tbIcon_9);
    m_vtNo.append(ui->tbNo_10);m_vtDate.append(ui->tbTime_10);m_vtIcon.append(ui->tbIcon_10);
    //m_vtNo.append(ui->tbNo_11);m_vtDate.append(ui->tbTime_11);m_vtIcon.append(ui->tbIcon_11);

    m_vtCode.clear();m_vtMsg.clear();
    m_vtCode.append(ui->tbCode);   m_vtMsg.append(ui->tbMsg);
    m_vtCode.append(ui->tbCode_2); m_vtMsg.append(ui->tbMsg_2);
    m_vtCode.append(ui->tbCode_3); m_vtMsg.append(ui->tbMsg_3);
    m_vtCode.append(ui->tbCode_4); m_vtMsg.append(ui->tbMsg_4);
    m_vtCode.append(ui->tbCode_5); m_vtMsg.append(ui->tbMsg_5);
    m_vtCode.append(ui->tbCode_6); m_vtMsg.append(ui->tbMsg_6);
    m_vtCode.append(ui->tbCode_7); m_vtMsg.append(ui->tbMsg_7);
    m_vtCode.append(ui->tbCode_8); m_vtMsg.append(ui->tbMsg_8);
    m_vtCode.append(ui->tbCode_9); m_vtMsg.append(ui->tbMsg_9);
    m_vtCode.append(ui->tbCode_10);m_vtMsg.append(ui->tbMsg_10);
    //m_vtCode.append(ui->tbCode_11);m_vtMsg.append(ui->tbMsg_11);

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnUpPic,SIGNAL(mouse_press()),this,SLOT(onPress_UpDown()));
    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onRelease_UpDown()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),this,SLOT(onPress_UpDown()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),this,SLOT(onRelease_UpDown()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnDownPic,SIGNAL(mouse_press()),this,SLOT(onPress_UpDown()));
    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onRelease_UpDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),this,SLOT(onPress_UpDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),this,SLOT(onRelease_UpDown()));

    /** for long press operation. **/
    uiLongPressUp = new HyUi_LongPress();
    uiLongPressUp->Init(1000,100);
    connect(ui->btnUpPic,SIGNAL(mouse_press()),uiLongPressUp,SLOT(onPressed()));
    connect(ui->btnUpPic,SIGNAL(mouse_release()),uiLongPressUp,SLOT(onReleased()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),uiLongPressUp,SLOT(onPressed()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),uiLongPressUp,SLOT(onReleased()));
    connect(uiLongPressUp,SIGNAL(sigTrigger()),this,SLOT(onListUpPage()));
    uiLongPressDown = new HyUi_LongPress();
    uiLongPressDown->Init(1000,100);
    connect(ui->btnDownPic,SIGNAL(mouse_press()),uiLongPressDown,SLOT(onPressed()));
    connect(ui->btnDownPic,SIGNAL(mouse_release()),uiLongPressDown,SLOT(onReleased()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),uiLongPressDown,SLOT(onPressed()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),uiLongPressDown,SLOT(onReleased()));
    connect(uiLongPressDown,SIGNAL(sigTrigger()),this,SLOT(onListDownPage()));

    connect(ui->lbBox,SIGNAL(mouse_release()),this,SLOT(onCheckbox()));
    connect(ui->lbCheck,SIGNAL(mouse_release()),this,SLOT(onCheckbox()));
    connect(ui->lbCBText,SIGNAL(mouse_release()),this,SLOT(onCheckbox()));

    Init_LogIcon();

    // default important.
    m_dateFrom = QDate(2000,1,1);   // any date is ok but, all data make same.
    m_dateTo = m_dateFrom;
    m_dateFrom_Painted = m_dateFrom;
    m_dateTo_Painted = m_dateTo;

    m_bDateSetError = false;

}

dialog_log::~dialog_log()
{
    delete ui;
}

void dialog_log::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_log::showEvent(QShowEvent *)
{
    Update();

    timer->start();
}
void dialog_log::hideEvent(QHideEvent *)
{
    timer->stop();
}

void dialog_log::onTimer()
{
    if(m_bDateSetError)
    {
        timer->stop();

        dialog_message* msg;
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Pendant Date Setting Error")),
                     QString(tr("Please check the pendent Date/Time")));
        msg->exec();

        onClose();
    }

    ui->lbDate->setText(Status->strDate);
    ui->lbTime->setText(Status->strTime);
}

void dialog_log::onClose()
{
    emit accept();
}

void dialog_log::Update()
{
    // calendar language change
    // gLocale = current language set QLocale data.

    ui->calendar->setLocale(gLocale);

    QString str = Param->Get(HyParam::START_DATE);

    // compare start_date & current date. if start_date > current_date(rtc setting value), it happen segmentation.
    QDate start_date = QDate::fromString(str,"yyyy-MM-dd");
    QDate current_date = QDate::fromString(Status->strDate,Qt::ISODate);

    m_bDateSetError = false;
    if(start_date > current_date)
    {
        m_bDateSetError = true;
        qDebug() << "Pendant Date Setting Error!!";
        return;
    }

    ui->calendar->setMinimumDate(QDate::fromString(str,"yyyy-MM-dd")); // machine factory in date.
    ui->calendar->setMaximumDate(QDate::fromString(Status->strDate,Qt::ISODate));
    ui->calendar->setSelectedDate(ui->calendar->minimumDate()); //event occur trick.
    ui->calendar->setSelectedDate(QDate::fromString(Status->strDate,Qt::ISODate));
    ui->calendar->showSelectedDate();

    SetMode(0);
    Select_FromTo(-1);
    SetDateFrom(ui->calendar->selectedDate());
    SetDateTo(ui->calendar->selectedDate());

    on_calendar_selectionChanged(); // avoid same selected date
}

void dialog_log::onNext()
{
    ui->calendar->showNextMonth();
}
void dialog_log::onBack()
{
    ui->calendar->showPreviousMonth();
}

void dialog_log::on_calendar_currentPageChanged(int year, int month)
{
    QDate max_date = ui->calendar->maximumDate();
    QDate min_date = ui->calendar->minimumDate();

    if(year < max_date.year())
    {
        HideNextBtn(false);
    }
    else if(year == max_date.year())
    {
        if(month >= max_date.month())
            HideNextBtn(true);
        else
            HideNextBtn(false);
    }
    else if(year > max_date.year())
    {
        HideNextBtn(true);
    }

    if(year > min_date.year())
    {
        HideBackBtn(false);
    }
    else if(year == min_date.year())
    {
        if(month <= min_date.month())
            HideBackBtn(true);
        else
            HideBackBtn(false);
    }
    else if(year < min_date.year())
    {
        HideBackBtn(true);
    }

    ui->lbYear->setText(QString().setNum(year));
    ui->lbMonth->setText(QString().setNum(month));
}

void dialog_log::on_calendar_selectionChanged()
{
    if(ui->calendar->selectedDate() > QDate::fromString(Status->strDate,Qt::ISODate))
    {
        ui->calendar->setSelectedDate(QDate::fromString(Status->strDate,Qt::ISODate));
        return;
    }

    if(!ui->lbToPic->autoFillBackground() && !ui->lbFromPic->autoFillBackground())
    {
        SetDateFrom(ui->calendar->selectedDate());
        SetDateTo(ui->calendar->selectedDate());
    }
    else if(ui->lbToPic->autoFillBackground())
        SetDateTo(ui->calendar->selectedDate());
    else if(ui->lbFromPic->autoFillBackground())
        SetDateFrom(ui->calendar->selectedDate());


    PaintCalendarDate(m_dateFrom_Painted, m_dateTo_Painted, false);
    PaintCalendarDate(m_dateFrom, m_dateTo, true);
}

void dialog_log::Select_FromTo(int nfromto)
{
    if(nfromto < 0)
    {
        ui->lbFromPic->setAutoFillBackground(false);
        ui->lbToPic->setAutoFillBackground(false);
    }
    else if(nfromto == 0)
    {
        ui->lbFromPic->setAutoFillBackground(true);
        ui->lbToPic->setAutoFillBackground(false);
    }
    else
    {
        ui->lbFromPic->setAutoFillBackground(false);
        ui->lbToPic->setAutoFillBackground(true);
    }
}
void dialog_log::SetDateFrom(QDate date)
{
    if(date > m_dateTo)
    {
        SetDateTo(date);
    }

    m_dateFrom = date;
    ui->lbFrom->setText(date.toString(Qt::ISODate));
}
void dialog_log::SetDateTo(QDate date)
{
    if(date < m_dateFrom)
    {
        date = m_dateFrom;
        ui->calendar->setSelectedDate(date);
    }

    m_dateTo = date;
    ui->lbTo->setText(date.toString(Qt::ISODate));
}

void dialog_log::onFrom()
{
    Select_FromTo(0);
}
void dialog_log::onTo()
{
    Select_FromTo(1);
}

void dialog_log::onView()
{
    ShowList(true);
}

void dialog_log::HideNextBtn(bool onoff)
{
    if(onoff)
    {
        ui->btnNextPic->setEnabled(false);
        ui->btnNextPic->hide();
        ui->btnNextIcon->setEnabled(false);
        ui->btnNextIcon->hide();
    }
    else
    {
        ui->btnNextPic->setEnabled(true);
        ui->btnNextPic->show();
        ui->btnNextIcon->setEnabled(true);
        ui->btnNextIcon->show();
    }
}

void dialog_log::HideBackBtn(bool onoff)
{
    if(onoff)
    {
        ui->btnBackPic->setEnabled(false);
        ui->btnBackPic->hide();
        ui->btnBackIcon->setEnabled(false);
        ui->btnBackIcon->hide();
    }
    else
    {
        ui->btnBackPic->setEnabled(true);
        ui->btnBackPic->show();
        ui->btnBackIcon->setEnabled(true);
        ui->btnBackIcon->show();
    }
}

void dialog_log::onSelect()
{
    QString btn = sender()->objectName();

    if(btn == ui->btnDayPic->objectName())
    {
        SetMode(0);
        on_calendar_selectionChanged(); // avoid same selected date
    }
    else if(btn == ui->btnRangePic->objectName())
    {
        SetMode(1);
    }
}

void dialog_log::SetMode(int mode)
{
    // 0= day, 1=range (default 0)
    QImage img;
    QPixmap pxmon, pxmoff;
    img.load(":/icon/icon/icon992-5.png");
    pxmon = QPixmap::fromImage(img);
    img.load(":/icon/icon/icon992-0.png");
    pxmoff = QPixmap::fromImage(img);

    if(mode == 0)
    {
        ui->btnDayIcon->setPixmap(pxmon);
        ui->btnRangeIcon->setPixmap(pxmoff);

        ui->btnSelPic1->setEnabled(false);
        ui->btnSelPic2->setEnabled(false);
        ui->btnSelIcon1->setEnabled(false);
        ui->btnSelIcon2->setEnabled(false);

        ui->calendar->setSelectedDate(QDate::fromString(Status->strDate,Qt::ISODate));
        ui->calendar->showSelectedDate();
        Select_FromTo(-1);
        SetDateFrom(ui->calendar->selectedDate());
        SetDateTo(ui->calendar->selectedDate());
    }
    else
    {
        ui->btnDayIcon->setPixmap(pxmoff);
        ui->btnRangeIcon->setPixmap(pxmon);
        Select_FromTo(0);

        ui->btnSelPic1->setEnabled(true);
        ui->btnSelPic2->setEnabled(true);
        ui->btnSelIcon1->setEnabled(true);
        ui->btnSelIcon2->setEnabled(true);
    }

}
void dialog_log::PaintCalendarDate(QDate from, QDate to, bool bPaint)
{
    QBrush brush;
    if(bPaint)
        brush.setColor(Qt::yellow);
    else
        brush.setColor(Qt::white); // fix

    QTextCharFormat cf;
    QDate date;
    int i;
    for(i=0;i<(from.daysTo(to)+1);i++)
    {
        date = from.addDays(i);
        cf = ui->calendar->dateTextFormat(date);
        cf.setBackground(brush);
        ui->calendar->setDateTextFormat(date, cf);
    }

    if(bPaint)
    {
        m_dateFrom_Painted = from;
        m_dateTo_Painted = to;
    }
}
void dialog_log::DisableCalendarDate()
{
    QDate maxday = ui->calendar->maximumDate();
    QDate minday = ui->calendar->minimumDate();

    QTextCharFormat cf;
    /*QDate datemax,datemin;
    QBrush brush;
    brush.setColor(Qt::gray);
    int i;
    for(i=1;i<45;i++)  // 1.5 month => enough
    {
        datemax = maxday.addDays(i);
        cf = ui->calendar->dateTextFormat(datemax);
        cf.setBackground(brush);
        ui->calendar->setDateTextFormat(datemax, cf);

        datemin = minday.addDays((-1)*i);
        cf = ui->calendar->dateTextFormat(datemin);
        cf.setBackground(brush);
        ui->calendar->setDateTextFormat(datemin, cf);
    }*/

    /*QDate chkdate;
    QBrush white;
    white.setColor(Qt::white);
    for(i=0;i<60;i++) // 2 month check
    {
        chkdate = maxday.addDays((-1)*i);
        cf = ui->calendar->dateTextFormat(chkdate);
        if(cf.background() == brush)
        {
            cf.setBackground(white);
            ui->calendar->setDateTextFormat(chkdate, cf);
        }
        else
        {
            return;
        }
    }*/

    QFont font;
    cf = ui->calendar->dateTextFormat(maxday);
    font = cf.font();
    font.setPointSize(25);
    font.setBold(true);
    cf.setFont(font);
    ui->calendar->setDateTextFormat(maxday, cf);

    cf = ui->calendar->dateTextFormat(minday);
    font = cf.font();
    font.setPointSize(25);
    font.setBold(true);
    cf.setFont(font);
    ui->calendar->setDateTextFormat(minday, cf);

}



/** LIST PAGE **/
void dialog_log::Update_List()
{
//    // old api.
//    LogRead(m_slRawLogList);
//    LogParsing(m_slRawLogList, m_dateFrom, m_dateTo);

    // new api
    LogRead(m_slRawLogList,m_dateFrom,m_dateTo);
    LogParsing(m_slRawLogList);

    ui->lbFrom2->setText(ui->lbFrom->text());
    ui->lbTo2->setText(ui->lbTo->text());
    ui->lbItemNum->setText(QString().setNum(m_LogData.size()));

    m_nStartIndex = 0;
    Redraw_LogList(m_nStartIndex);

    SetCheckbox(false);
}

void dialog_log::ShowList(bool onoff)
{
    if(onoff)
    {
        ui->grpCalendar->hide();
        ui->grpControl->hide();

        ui->grpList->show();
        ui->grpListControl->show();

        Update_List();
    }
    else
    {
        ui->grpCalendar->show();
        ui->grpControl->show();

        ui->grpList->hide();
        ui->grpListControl->hide();
    }
}

void dialog_log::onReturn()
{
    ShowList(false);
}
void dialog_log::onExt()
{
    dialog_message* msg;

    if(!RM->IsExist(USB_EXIST_PATH))
    {
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Not find USB memory.")),
                     QString(tr("Plase insert USB and try again")));
        msg->exec();
        return;
    }

    // make file path
    QString path;
    path.clear();
    path = RM_USB_BASE_PATH;
    path += "/";
    path += QString("log_");
    path += QDateTime::currentDateTime().toString("yyyyMMddhhmmss");
    path += QString("_");
    path += m_dateFrom.toString("yyyyMMdd");
    path += QString("_");
    path += m_dateTo.toString("yyyyMMdd");
    path += QString(".csv");    // file type. (default csv file)


    // Question.
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(QString(tr("CONFIRM")));
    conf->Message(QString(tr("Would you want to save log in USB?")), path);
    if(conf->exec() == QDialog::Rejected)
        return;

    // make data list
    QStringList list;
    QString str;

    list.clear();

    // add header. (only english, because broken another language)
    str.clear();
    str =  QString("No,");
    str += QString("Date/Time,");
    str += QString("Logger,");
    str += QString("Code,");
    str += QString("Message");
    list.append(str);

    for(int i=0;i<m_LogData.count();i++)
    {
        str.clear();
        str = QString().setNum(i+1);
        str += QString(",");
        str += m_LogData[i].DateTime.toString(Qt::ISODate);
        str += QString(",");
        str += m_LogData[i].Logger;
        str += QString(",");
        str += m_LogData[i].Code;
        str += QString(",");
        str += m_LogData[i].Msg;

        list.append(str);
    }

    msg = (dialog_message*)gGetDialog(DIG_MSG);

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->createProgramFile(path, list);
    if(ret < 0)
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("FAIL")));
        msg->Message(QString(tr("Fail to log file export! Try again!")),
                     path);
    }
    else
    {
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(QString(tr("SUCCESS")));
        msg->Message(QString(tr("Success to log file export!")),
                     path);
    }
    msg->exec();
}

/** new api **/
void dialog_log::LogRead(QStringList &list, QDate from, QDate to)
{
    CNRobo* pCon = CNRobo::getInstance();
    list.clear();
    int ret = pCon->getSelectedlogList(list, from, to);
    if(ret < 0)
    {
        qDebug() << "getSelectedlogList ret=" << ret;
        list.clear();
    }

    qDebug() << "LogRead Success!! list count =" << list.count();
}
void dialog_log::LogParsing(QStringList list)
{
    QStringList temp;

    m_RawLogData.clear();
    m_LogData.clear();
    m_ErrLogData.clear();
    m_AllLogData.clear();

    ST_LOG_DATA default_data;
    default_data.DateTime = QDateTime::currentDateTime();
    default_data.Logger = QString("0");
    default_data.Code = QString("0");
    default_data.Msg = QString("");

    int i,j,code;
    ST_LOG_DATA log_data;
    for(i=0;i<list.count();i++)
    {
        m_RawLogData.append(default_data);
        // to make QDateTime
        temp = list[i].split(",");
        for(j=0;j<temp.count();j++)
        {
            if(j == 0)
            {
                m_RawLogData[i].DateTime = QDateTime::fromString(temp[j].trimmed(),"yyyy/MM/dd-hh:mm:ss");
                //qDebug() << "dateteime =" << m_RawLogData[i].DateTime.toString(Qt::ISODate);
            }
            else if(j == 1)
            {
                m_RawLogData[i].Logger = temp[j].trimmed();
                //qDebug() << "Logger =" << m_RawLogData[i].Logger;
            }
            else if(j == 2)
            {
                m_RawLogData[i].Code = temp[j].trimmed();
                //qDebug() << "Code =" << m_RawLogData[i].Code;
            }
            else if(j == 3)
            {
                m_RawLogData[i].Msg = temp[3].trimmed();
                //qDebug() << "Msg =" << m_RawLogData[i].Msg;
            }
        }

        // data gathering
        log_data = m_RawLogData[i];
        // Apply HyLog
        code = log_data.Code.toInt();
        if(Log->IsExist(code))
            log_data.Msg = Log->Get(code);

        if(code < 0)
            m_ErrLogData.push_back(log_data);       // only error sorting

        m_AllLogData.push_back(log_data);           // all log data.

        //@@ sorting arrange.
        //m_LogData.push_front(log_data);  // past -> future
        //m_LogData.push_back(log_data);   // future -> past
    }

    m_LogData = m_AllLogData;   // first all data view.
}

/** old api **/
void dialog_log::LogRead(QStringList &list)
{
    CNRobo* pCon = CNRobo::getInstance();
    list.clear();
    int ret = pCon->getLogList(list);
    if(ret < 0)
        list.clear();

    qDebug() << "LogRead Success!! list count =" << list.count();
}

void dialog_log::LogParsing(QStringList list, QDate from, QDate to)
{
    if(list.isEmpty())
        return;

    QStringList temp;

    m_RawLogData.clear();
    m_LogData.clear();
    m_ErrLogData.clear();
    m_AllLogData.clear();

    ST_LOG_DATA default_data;
    default_data.DateTime = QDateTime::currentDateTime();
    default_data.Logger = QString("0");
    default_data.Code = QString("0");
    default_data.Msg = QString("");

    int i,j,code;
    ST_LOG_DATA log_data;
    for(i=0;i<list.count();i++)
    {
        m_RawLogData.append(default_data);
        // to make QDateTime
        temp = list[i].split(",");
        for(j=0;j<temp.count();j++)
        {
            if(j == 0)
            {
                m_RawLogData[i].DateTime = QDateTime::fromString(temp[j].trimmed(),"yyyy/MM/dd-hh:mm:ss");
                //qDebug() << "dateteime =" << m_RawLogData[i].DateTime.toString(Qt::ISODate);
            }
            else if(j == 1)
            {
                m_RawLogData[i].Logger = temp[j].trimmed();
                //qDebug() << "Logger =" << m_RawLogData[i].Logger;
            }
            else if(j == 2)
            {
                m_RawLogData[i].Code = temp[j].trimmed();
                //qDebug() << "Code =" << m_RawLogData[i].Code;
            }
            else if(j == 3)
            {
                m_RawLogData[i].Msg = temp[3].trimmed();
                //qDebug() << "Msg =" << m_RawLogData[i].Msg;
            }
        }

        // Sorting
        if(m_RawLogData[i].DateTime.date() >= from
        && m_RawLogData[i].DateTime.date() <= to)
        {
            // data gathering
            log_data = m_RawLogData[i];
            // Apply HyLog
            code = log_data.Code.toInt();
            if(Log->IsExist(code))
                log_data.Msg = Log->Get(code);

            if(code < 0)
                m_ErrLogData.push_back(log_data);       // only error sorting

            m_AllLogData.push_back(log_data);           // all log data.

            //@@ sorting arrange.
            //m_LogData.push_front(log_data);  // past -> future
            //m_LogData.push_back(log_data);   // future -> past

        }

    }

    m_LogData = m_AllLogData;   // first all data view.

    /*// for debug..
    qDebug() << "Sorted LogData " << m_LogData.size()
             << from.toString(Qt::ISODate) << "~" << to.toString(Qt::ISODate);
    for(i=0;i<m_LogData.count();i++)
        qDebug() << m_LogData[i].DateTime.toString(Qt::ISODate)
                 << m_LogData[i].Logger
                 << m_LogData[i].Code
                 << m_LogData[i].Msg;*/

}
void dialog_log::Redraw_LogList(int start_index)
{
    if(start_index < 0) return;

    int i,index;
    for(i=0;i<m_vtNo.count();i++)
    {
        index = start_index + i;

        if(index < m_LogData.size())
        {
            // have data.
            // No
            m_vtNo[i]->setText(QString().setNum(index + 1));
            // DateTime
            m_vtDate[i]->setText(m_LogData[index].DateTime.toString("yyyy/MM/dd hh:mm:ss"));
            // Icon
            m_vtIcon[i]->setPixmap(GetLogIcon(index));
            //m_vtIcon[i]->setPixmap(0); // test
            // Code
            m_vtCode[i]->setText(m_LogData[index].Code);
            // Msg
            m_vtMsg[i]->setText(m_LogData[index].Msg);

        }
        else
        {
            // don't have data.
            m_vtNo[i]->setText(QString(""));
            m_vtDate[i]->setText(QString(""));
            m_vtIcon[i]->setPixmap(0);
            m_vtCode[i]->setText(QString(""));
            m_vtMsg[i]->setText(QString(""));
        }
    }

}

void dialog_log::Init_LogIcon()
{
    QImage img;
    m_vtLogIcon.clear();

    img.load(":/icon/icon/icon999-31.png");    // error
    m_vtLogIcon.append(QPixmap::fromImage(img));
    img.load(":/icon/icon/icon999-24.png");   // info
    m_vtLogIcon.append(QPixmap::fromImage(img));
}
QPixmap dialog_log::GetLogIcon(int list_index)
{
    int code = m_LogData[list_index].Code.toInt();
    int log_index=0;

    // if you add case, add condition using code range
    if(code < 0)
    {
        log_index = 0;
    }
    else
    {
        log_index = 1;
    }

    if(log_index >= 0 && log_index < m_vtLogIcon.size())
        return m_vtLogIcon[log_index];
    else
        return 0;
}

void dialog_log::onListUp()
{
    if(m_nStartIndex <= 0)
    {
        m_nStartIndex = 0;
        return;
    }

    Redraw_LogList(--m_nStartIndex);
}
void dialog_log::onListUpPage()
{
    /* // one page jump
    m_nStartIndex -= m_vtNo.size();

    if(m_nStartIndex <= 0)
        m_nStartIndex = 0;

    Redraw_LogList(m_nStartIndex);*/

    // one by one fast
    if(m_nStartIndex <= 0)
    {
        m_nStartIndex = 0;
        return;
    }

    Redraw_LogList(--m_nStartIndex);
}

void dialog_log::onListDown()
{
    if(m_nStartIndex <= 0)
        m_nStartIndex = 0;

    if(m_nStartIndex+m_vtNo.size() >= m_LogData.size())
    {
        m_nStartIndex = m_LogData.size() - m_vtNo.size();
        return;
    }

    Redraw_LogList(++m_nStartIndex);
}
void dialog_log::onListDownPage()
{
    /* // one page jump
    if(m_nStartIndex <= 0)
        m_nStartIndex = 0;

    m_nStartIndex += m_vtNo.size();

    if(m_nStartIndex+m_vtNo.size() >= m_LogData.size())
        m_nStartIndex = m_LogData.size() - m_vtNo.size();


    Redraw_LogList(m_nStartIndex);*/

    // one by one fast
    if(m_nStartIndex <= 0)
        m_nStartIndex = 0;

    if(m_nStartIndex+m_vtNo.size() >= m_LogData.size())
    {
        m_nStartIndex = m_LogData.size() - m_vtNo.size();
        return;
    }

    Redraw_LogList(++m_nStartIndex);
}

void dialog_log::onPress_UpDown()
{
    QLabel3* btn = (QLabel3*)sender();

    QImage img;
    img.load(":/image/image/button_rect3_2.png");
    QPixmap pxm = QPixmap::fromImage(img);
    if(btn == ui->btnUpPic || btn == ui->btnUpIcon)
    {
        ui->btnUpPic->setPixmap(pxm);
    }
    else if(btn == ui->btnDownPic || btn == ui->btnDownIcon)
    {
        ui->btnDownPic->setPixmap(pxm);
    }
}
void dialog_log::onRelease_UpDown()
{
    QLabel3* btn = (QLabel3*)sender();

    QImage img;
    img.load(":/image/image/button_rect3_0.png");
    QPixmap pxm = QPixmap::fromImage(img);
    if(btn == ui->btnUpPic || btn == ui->btnUpIcon)
    {
        ui->btnUpPic->setPixmap(pxm);
    }
    else if(btn == ui->btnDownPic || btn == ui->btnDownIcon)
    {
        ui->btnDownPic->setPixmap(pxm);
    }
}

void dialog_log::Change_OnlyErrorLog(bool on)
{
    if(on)
        m_LogData = m_ErrLogData;
    else
        m_LogData = m_AllLogData;

    ui->lbItemNum->setText(QString().setNum(m_LogData.size()));
    m_nStartIndex = 0;
    Redraw_LogList(m_nStartIndex);
}

void dialog_log::SetCheckbox(bool on)
{
    if(on)
        ui->lbCheck->show();
    else
        ui->lbCheck->hide();
}

void dialog_log::onCheckbox()
{
    if(ui->lbCheck->isHidden())
    {
        // make on
        SetCheckbox(true);
        Change_OnlyErrorLog(true);
    }
    else
    {
        // make off
        SetCheckbox(false);
        Change_OnlyErrorLog(false);
    }
}
