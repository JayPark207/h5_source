#ifndef WIDGET_BACKUP_RECOVERY2_H
#define WIDGET_BACKUP_RECOVERY2_H

#include <QWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"
#include "dialog_message.h"
#include "dialog_confirm.h"

namespace Ui {
class widget_backup_recovery2;
}

class widget_backup_recovery2 : public QWidget
{
    Q_OBJECT

public:
    explicit widget_backup_recovery2(QWidget *parent = 0);
    ~widget_backup_recovery2();

    void Init(int backup_list_index);
    void Update();

    void Display_USB(bool bExist);
    void Redraw_List(int start_index);
    void CheckAll(bool bCheck);
    void ConfirmCheck();
    void Display_SelectedBackupInfo();

signals:
    void sigBack();
    void sigRecovery();

private slots:
    void onList();
    void onUp();
    void onDown();
    void onCheckAll();
    void onParam();
    void onConfig();

    void onRecovery();

private:
    Ui::widget_backup_recovery2 *ui;

    int m_nBackupListIndex;

    int m_nStartIndex;

    QVector<QLabel3*> m_vtChk;
    QVector<QLabel3*> m_vtMark;
    QVector<QLabel3*> m_vtNo;
    QVector<QLabel3*> m_vtName;
    QVector<QLabel3*> m_vtDate;

    QVector<bool> m_bMark;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // WIDGET_BACKUP_RECOVERY2_H
