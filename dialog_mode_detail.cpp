#include "dialog_mode_detail.h"
#include "ui_dialog_mode_detail.h"

dialog_mode_detail::dialog_mode_detail(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_mode_detail)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onEnd()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // group buttons
    m_vtBtnGrpPic.clear();m_vtBtnGrp.clear();
    m_vtBtnGrpPic.append(ui->btnG0Pic);m_vtBtnGrp.append(ui->btnG0);
    m_vtBtnGrpPic.append(ui->btnG1Pic);m_vtBtnGrp.append(ui->btnG1);
    m_vtBtnGrpPic.append(ui->btnG2Pic);m_vtBtnGrp.append(ui->btnG2);
    m_vtBtnGrpPic.append(ui->btnG3Pic);m_vtBtnGrp.append(ui->btnG3);
    m_vtBtnGrpPic.append(ui->btnG4Pic);m_vtBtnGrp.append(ui->btnG4);

    int i;
    for(i=0;i<m_vtBtnGrpPic.count();i++)
    {
        connect(m_vtBtnGrpPic[i],SIGNAL(mouse_release()),this,SLOT(onChangeGrp()));
        connect(m_vtBtnGrp[i],SIGNAL(mouse_press()),m_vtBtnGrpPic[i],SLOT(press()));
        connect(m_vtBtnGrp[i],SIGNAL(mouse_release()),m_vtBtnGrpPic[i],SLOT(release()));
    }

    // Table
    m_vtTbNo.clear();m_vtTbData.clear();
    m_vtTbNo.append(ui->tbNo);  m_vtTbData.append(ui->tbData);
    m_vtTbNo.append(ui->tbNo_2);m_vtTbData.append(ui->tbData_2);
    m_vtTbNo.append(ui->tbNo_3);m_vtTbData.append(ui->tbData_3);
    m_vtTbNo.append(ui->tbNo_4);m_vtTbData.append(ui->tbData_4);
    m_vtTbNo.append(ui->tbNo_5);m_vtTbData.append(ui->tbData_5);
    m_vtTbNo.append(ui->tbNo_6);m_vtTbData.append(ui->tbData_6);
    m_vtTbNo.append(ui->tbNo_7);m_vtTbData.append(ui->tbData_7);

    m_vtTbName.clear();m_vtTbNamePic.clear();
    m_vtTbName.append(ui->tbName);  m_vtTbNamePic.append(ui->tbNamePic);
    m_vtTbName.append(ui->tbName_2);m_vtTbNamePic.append(ui->tbNamePic_2);
    m_vtTbName.append(ui->tbName_3);m_vtTbNamePic.append(ui->tbNamePic_3);
    m_vtTbName.append(ui->tbName_4);m_vtTbNamePic.append(ui->tbNamePic_4);
    m_vtTbName.append(ui->tbName_5);m_vtTbNamePic.append(ui->tbNamePic_5);
    m_vtTbName.append(ui->tbName_6);m_vtTbNamePic.append(ui->tbNamePic_6);
    m_vtTbName.append(ui->tbName_7);m_vtTbNamePic.append(ui->tbNamePic_7);

    for(i=0;i<m_vtTbNamePic.count();i++)
    {
        connect(m_vtTbNamePic[i],SIGNAL(mouse_release()),this,SLOT(onListItem()));
        connect(m_vtTbName[i],SIGNAL(mouse_press()),m_vtTbNamePic[i],SLOT(press()));
        connect(m_vtTbName[i],SIGNAL(mouse_release()),m_vtTbNamePic[i],SLOT(release()));
    }

    // List up/down
    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onListUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onListDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));


    // pointer clear
    dig_mode_select = 0;
    dig_moldclean = 0;
    dig_takeout_method = 0;
    dig_numpad = 0;
    dig_weight = 0;
    dig_temp = 0;
    dig_jmotion = 0;
    dig_syncback = 0;
    dig_unload_method = 0;
    dig_e_sensor = 0;
    dig_dualunload = 0;
    dig_unloadback = 0;
    dig_takeout_timing = 0;

    top = 0;

    m_bReadonly = false;
}

dialog_mode_detail::~dialog_mode_detail()
{
    delete ui;
}
void dialog_mode_detail::showEvent(QShowEvent *)
{
    Update();

    SubTop();
}
void dialog_mode_detail::hideEvent(QHideEvent *)
{

}
void dialog_mode_detail::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_mode_detail::onEnd()
{
    emit accept();
}

void dialog_mode_detail::Update()
{
    SelectGrp(0);
}

void dialog_mode_detail::SelectGrp(int grp)
{
    m_nSelectedGrp = grp;
    m_nStartIndex = 0;
    Update(m_nSelectedGrp);
    Display_BtnGrp(m_nSelectedGrp);
    Redraw_List(m_nSelectedGrp, m_nStartIndex);
}

void dialog_mode_detail::Update(int grp)
{
    if(!Mode->Read((HyMode::MODE_GROUP)grp))
    {
        qDebug() << "Mode Read() Error Group=" << grp;
    }
}

void dialog_mode_detail::Display_BtnGrp(int grp)
{
    for(int i=0;i<m_vtBtnGrpPic.count();i++)
        m_vtBtnGrpPic[i]->setAutoFillBackground(i == grp);
}

void dialog_mode_detail::Redraw_List(int grp, int start_index)
{
    int list_index;
    QString strNo,strName,strData;

    for(int i=0;i<m_vtTbNo.count();i++)
    {
        list_index = start_index + i;

        if(list_index >= Mode->m_Data[grp].count())
        {
            // no have data.
            strNo.clear();
            strName.clear();
            strData.clear();

            m_vtTbNo[i]->setEnabled(false);
            m_vtTbName[i]->setEnabled(false);
            m_vtTbNamePic[i]->setEnabled(false);
            m_vtTbData[i]->setEnabled(false);
        }
        else
        {
            strNo = QString().setNum(list_index+1);
            strName = Mode->m_Data[grp][list_index].Name;
            strData = Mode->m_Data[grp][list_index].DispResult;

            if(!Mode->m_Data[grp][list_index].bNA)
            {
                m_vtTbNo[i]->setEnabled(true);
                m_vtTbName[i]->setEnabled(true);
                m_vtTbNamePic[i]->setEnabled(true);
                m_vtTbData[i]->setEnabled(true);
            }
            else
            {
                m_vtTbNo[i]->setEnabled(false);
                m_vtTbName[i]->setEnabled(false);
                m_vtTbNamePic[i]->setEnabled(false);
                m_vtTbData[i]->setEnabled(false);
            }
        }

        m_vtTbNo[i]->setText(strNo);
        m_vtTbName[i]->setText(strName);
        m_vtTbData[i]->setText(strData);
    }

    ui->lbItemNum->setText(QString().setNum(Mode->m_Data[grp].size()));
}

void dialog_mode_detail::onChangeGrp()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtBtnGrpPic.indexOf(btn);
    if(index < 0) return;

    SelectGrp(index);
}

void dialog_mode_detail::onListUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(m_nSelectedGrp, --m_nStartIndex);
}
void dialog_mode_detail::onListDown()
{
    if(m_nStartIndex >= Mode->m_Data[m_nSelectedGrp].size() - m_vtTbNo.size())
        return;

    Redraw_List(m_nSelectedGrp, ++m_nStartIndex);
}

void dialog_mode_detail::onListItem()
{
    if(m_bReadonly) return;

    QLabel4* item = (QLabel4*)sender();
    int index = m_vtTbNamePic.indexOf(item);
    if(index < 0) return;

    int list_index = index + m_nStartIndex;

    if(!IsHighLevel(USER_LEVEL_HIGH))
    {
        if(IsMode(m_nSelectedGrp, list_index, HyRecipe::emUse))
            return;
        // add here..
    }

    SetItem(m_nSelectedGrp, list_index);
}

// Best important functions.
void dialog_mode_detail::SetItem(int grp, int index)
{
    if(index >= Mode->m_Data[grp].size())
        return;

    HyMode::MODE_TYPE type = Mode->m_Data[grp][index].Type;
    HyMode::ST_MODE_DATA stData = Mode->m_Data[grp][index];

    bool bAccepted = false;

    switch(type)
    {
    case HyMode::MODE_SELITEM:
        dig_mode_select = (dialog_mode_select*)gGetDialog(DIG_MODE_SELECT);
        dig_mode_select->InitTitle(stData.Name);
        dig_mode_select->InitNum(stData.ItemName.size());
        dig_mode_select->InitRecipe(stData.Vars.first());
        for(int i=0;i<dig_mode_select->GetNum();i++)
            dig_mode_select->InitString(i,stData.ItemName[i]);

        if(dig_mode_select->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    // MGRP_COMMON specila case
    case HyMode::MODE_MOLDCLEAN:
        if(dig_moldclean == 0)
            dig_moldclean = new dialog_moldclean();

        if(dig_moldclean->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    // MGRP_TAKEOUT specila case
    case HyMode::MODE_TAKEOUTMETHOD:
        if(dig_takeout_method == 0)
            dig_takeout_method = new dialog_takeout_method();

        if(dig_takeout_method->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    case HyMode::MODE_JMOTION:
        if(dig_jmotion == 0)
            dig_jmotion = new dialog_jmotion();
        if(dig_jmotion->exec() == QDialog::Accepted)
            bAccepted = true;
        break;

    case HyMode::MODE_UNDERCUT:

        dig_numpad = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
        dig_numpad->m_numpad->SetTitle(stData.Name);
        dig_numpad->m_numpad->SetMinValue(0.0);
        dig_numpad->m_numpad->SetMaxValue(MAX_UNDERCUT_POS);
        dig_numpad->m_numpad->SetSosuNum(0);
        dig_numpad->m_numpad->SetNum((double)stData.Datas.first());

        if(dig_numpad->exec() == QDialog::Accepted)
        {
            // make undercut speed 0.
            float _val = (float)dig_numpad->m_numpad->GetNumDouble();
            if(_val > 0)
                Posi->Set_UndercutSpeed(stData.Datas.first(), _val, 0.0, false);

            if(Recipe->Set(HyRecipe::mdUndercut, _val))
                bAccepted = true;
        }
        break;

    case HyMode::MODE_SYNCBACK:
        if(dig_syncback == 0)
            dig_syncback = new dialog_syncback();
        if(dig_syncback->exec() == QDialog::Accepted)
            bAccepted = true;
        break;

    case HyMode::MODE_CONVTIME:

        dig_numpad = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
        dig_numpad->m_numpad->SetTitle(stData.Name);
        dig_numpad->m_numpad->SetMinValue(0.0);
        dig_numpad->m_numpad->SetMaxValue(100000.0);
        dig_numpad->m_numpad->SetSosuNum(SOSU_TIME);
        dig_numpad->m_numpad->SetNum((double)stData.Datas.first());

        if(dig_numpad->exec() == QDialog::Accepted)
        {
            if(Recipe->Set(HyRecipe::tConvTime,(float)dig_numpad->m_numpad->GetNumDouble()))
                bAccepted = true;
        }
        break;

    case HyMode::MODE_GRABCOMPTIME:

        dig_numpad = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
        dig_numpad->m_numpad->SetTitle(stData.Name);
        dig_numpad->m_numpad->SetMinValue(0.0);
        dig_numpad->m_numpad->SetMaxValue(100000.0);
        dig_numpad->m_numpad->SetSosuNum(SOSU_TIME);
        dig_numpad->m_numpad->SetNum((double)stData.Datas.first());

        if(dig_numpad->exec() == QDialog::Accepted)
        {
            if(Recipe->Set(HyRecipe::tGrabCompTime,(float)dig_numpad->m_numpad->GetNumDouble()))
                bAccepted = true;
        }
        break;

    case HyMode::MODE_UNLOADMETHOD:
        if(dig_unload_method == 0)
            dig_unload_method = new dialog_unload_method();

        if(dig_unload_method->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    case HyMode::MODE_WEIGHT:
        if(dig_weight == 0)
            dig_weight = new dialog_weight();

        if(dig_weight->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    case HyMode::MODE_TEMP:
        if(dig_temp == 0)
            dig_temp = new dialog_temperature();

        if(dig_temp->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    case HyMode::MODE_ESENSOR:
        if(dig_e_sensor == 0)
            dig_e_sensor = new dialog_e_sensor();

        if(dig_e_sensor->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    case HyMode::MODE_DUALUNLOAD:
        if(dig_dualunload == 0)
            dig_dualunload = new dialog_dualunload();

        if(dig_dualunload->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    case HyMode::MODE_UNLOADBACK:
        if(dig_unloadback == 0)
            dig_unloadback = new dialog_unloadback();

        if(dig_unloadback->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    case HyMode::MODE_TAKEOUTTIMING:
        if(dig_takeout_timing == 0)
            dig_takeout_timing = new dialog_takeout_timing();

        if(dig_takeout_timing->exec() == QDialog::Accepted)
            bAccepted = true;

        break;

    default:
        break;
    }

    // result update.
    if(bAccepted)
    {
        Update(grp);
        Redraw_List(grp, m_nStartIndex);
    }
}

void dialog_mode_detail::Set_Readonly(bool on)
{
    m_bReadonly = on;
}

bool dialog_mode_detail::IsMode(int grp, int index, int var)
{
    int result = Mode->m_Data[grp][index].Vars.indexOf(var);
    if(result < 0)
        return false;
    return true;
}

bool dialog_mode_detail::IsHighLevel(int level)
{
    int now_level = Param->Get(HyParam::USER_LEVEL).toInt();
    if(level <= now_level)
        return true;
    return false;
}


void dialog_mode_detail::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
