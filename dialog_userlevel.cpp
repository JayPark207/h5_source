#include "dialog_userlevel.h"
#include "ui_dialog_userlevel.h"

dialog_userlevel::dialog_userlevel(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_userlevel)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    m_vtLed.clear();
    m_vtLed.append(ui->led1);
    m_vtLed.append(ui->led2);
    m_vtLed.append(ui->led3);

    m_vtBtnPic.clear();m_vtBtn.clear();m_vtBtnIcon.clear();
    m_vtBtnPic.append(ui->btn1stPic);m_vtBtn.append(ui->btn1st);m_vtBtnIcon.append(ui->btn1stIcon);
    m_vtBtnPic.append(ui->btn2ndPic);m_vtBtn.append(ui->btn2nd);m_vtBtnIcon.append(ui->btn2ndIcon);
    m_vtBtnPic.append(ui->btn3rdPic);m_vtBtn.append(ui->btn3rd);m_vtBtnIcon.append(ui->btn3rdIcon);

    m_vtPw.clear();
    m_vtPw.append(ui->lbWord0);
    m_vtPw.append(ui->lbWord1);
    m_vtPw.append(ui->lbWord2);
    m_vtPw.append(ui->lbWord3);

    int i;
    for(i=0;i<m_vtBtnPic.count();i++)
    {
        connect(m_vtBtnPic[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtBtn[i],SIGNAL(mouse_press()),m_vtBtnPic[i],SLOT(press()));
        connect(m_vtBtn[i],SIGNAL(mouse_release()),m_vtBtnPic[i],SLOT(release()));
        connect(m_vtBtnIcon[i],SIGNAL(mouse_press()),m_vtBtnPic[i],SLOT(press()));
        connect(m_vtBtnIcon[i],SIGNAL(mouse_release()),m_vtBtnPic[i],SLOT(release()));

    }

    connect(ui->lbKey0,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey1,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey2,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey3,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey4,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey5,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey6,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey7,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey8,SIGNAL(clicked()),this,SLOT(onNumpad()));
    connect(ui->lbKey9,SIGNAL(clicked()),this,SLOT(onNumpad()));

    connect(ui->lbBack,SIGNAL(clicked()),this,SLOT(onBack()));
    connect(ui->lbClear,SIGNAL(clicked()),this,SLOT(onClear()));
    //connect(ui->lbClear,SIGNAL(clicked()),this,SLOT(accept()));

    timer = new QTimer(this);
    timer->setInterval(50);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));
    m_nX = ui->grpPw->x();

    timer_clock = new QTimer(this);
    timer_clock->setInterval(1000);
    connect(timer_clock,SIGNAL(timeout()),this,SLOT(onTimer_Clock()));

}

dialog_userlevel::~dialog_userlevel()
{
    delete ui;
}
void dialog_userlevel::showEvent(QShowEvent *)
{
    Update();

    timer_clock->start();
}
void dialog_userlevel::hideEvent(QHideEvent *)
{
    timer_clock->stop();
}
void dialog_userlevel::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_userlevel::Update()
{
    // for test.
    m_strDefaulPassword = QString("0000");

    // read now level.
    m_nNowLevel = Param->Get(HyParam::USER_LEVEL).toInt();

    // read password per level1~9
    m_strlPassword.clear();
    for(int i=0;i<NUM_USER_LEVEL;i++)
    {
        m_strlPassword.append(QString("0000"));
        m_strlPassword[i] = Param->Get((HyParam::PARAMS)(HyParam::LV0_PW + i));
    }

    // btn icon
    m_vtBtnIcon[0]->setText(QString().setNum(USER_LEVEL_LOW));
    m_vtBtnIcon[1]->setText(QString().setNum(USER_LEVEL_MID));
    m_vtBtnIcon[2]->setText(QString().setNum(USER_LEVEL_HIGH));

    SetLed(m_nNowLevel);

    DelWordAll();

    // clock
    m_nClockCount = USERLEVEL_WAITTIME;
    ui->lcdClock->display(USERLEVEL_WAITTIME - 1);

}

void dialog_userlevel::onSelect()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtBtnPic.indexOf(btn);
    if(index < 0) return;
    if(m_vtBtnIcon[index]->text().toInt() == m_nNowLevel) return;

    m_nNowLevel = m_vtBtnIcon[index]->text().toInt();
    SetLed(m_nNowLevel);

    // clock
    m_nClockCount = USERLEVEL_WAITTIME;
}

void dialog_userlevel::SetLed(int level)
{
    int i;
    QImage img;
    img.load(":/icon/icon/icon992-0.png");
    for(i=0;i<m_vtLed.count();i++)
        m_vtLed[i]->setPixmap(QPixmap::fromImage(img));

    int index = m_vtLed.size();
    for(i=0;i<m_vtBtnIcon.count();i++)
    {
        if(m_vtBtnIcon[i]->text().toInt() == level)
            index = i;
    }

    if(index < m_vtLed.size())
    {
        img.load(":/icon/icon/icon992-10.png");
        m_vtLed[index]->setPixmap(QPixmap::fromImage(img));
    }
}

void dialog_userlevel::onNumpad()
{
    QPushButton* btn = (QPushButton*)sender();

    AddWord(btn->text().toInt());
}
void dialog_userlevel::onBack()
{
    DelWord();
}
void dialog_userlevel::onClear()
{
    DelWordAll();
}

void dialog_userlevel::SetPW(int word_count)
{
    QImage img;
    img.load(":/icon/icon/icon99-15.png");

    for(int i=0;i<m_vtPw.count();i++)
    {
        if(i < word_count)
            m_vtPw[i]->setPixmap(QPixmap::fromImage(img));
        else
            m_vtPw[i]->setPixmap(0);
    }

}

bool dialog_userlevel::CheckPW(int level, QStringList word)
{
    if(word.size() < m_vtPw.size())
        return false;

    QString str;
    str.clear();
    for(int i=0;i<m_vtPw.size();i++)
        str += word[i].trimmed().left(1);

    QString pw = m_strlPassword[level].trimmed().left(4);
    if(pw == str)
        return true;

    return false;
}

void dialog_userlevel::AddWord(int number)
{
    m_strlWord.append(QString().setNum(number));

    SetPW(m_strlWord.size());

    if(m_strlWord.size() >= m_vtPw.size())
    {
        // check password
        if(CheckPW(m_nNowLevel, m_strlWord))
        {
            dialog_delaying* dig = new dialog_delaying();
            dig->Init(500);
            dig->exec();

            Param->Set(HyParam::USER_LEVEL, m_nNowLevel);
            accept();
        }
        else
        {
            // password all clear & message.
            ui->grpNumpad->setEnabled(false);
            m_nCount = 0;
            timer->start();
        }

    }
}

void dialog_userlevel::DelWord()
{
    if(m_strlWord.size() <= 0)
        return;

    m_strlWord.pop_back();
    SetPW(m_strlWord.size());
}

void dialog_userlevel::DelWordAll()
{
    m_strlWord.clear();
    SetPW(m_strlWord.size());
}

void dialog_userlevel::onTimer()
{

    int _x;
    if(m_nCount%2 == 0)
        _x = m_nX + 5;
    else
        _x = m_nX - 5;

    ui->grpPw->setGeometry(_x,ui->grpPw->y(),ui->grpPw->width(),ui->grpPw->height());

    m_nCount++;
    if(m_nCount > 5)
    {
        m_nCount = 0;
        ui->grpPw->setGeometry(m_nX,ui->grpPw->y(),ui->grpPw->width(),ui->grpPw->height());
        DelWordAll();
        ui->grpNumpad->setEnabled(true);
        timer->stop();
    }
}

void dialog_userlevel::onTimer_Clock()
{
    if(m_strlWord.size() > 0)
        return;

    m_nClockCount--;
    ui->lcdClock->display(m_nClockCount);

    if(m_nClockCount <= 0)
    {
        dialog_delaying* dig = new dialog_delaying();
        dig->Init(500);
        dig->exec();

        Param->Set(HyParam::USER_LEVEL, USER_LEVEL_LOW);
        accept();
    }
}
