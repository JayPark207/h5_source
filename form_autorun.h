#ifndef FORM_AUTORUN_H
#define FORM_AUTORUN_H

#include <QWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_product_info.h"
#include "dialog_array_info.h"

#include "dialog_confirm.h"
#include "dialog_sampling.h"
#include "dialog_discharge.h"

// shorcut page
#include "dialog_varmonitor.h"
#include "dialog_position_auto.h"
#include "dialog_error.h"
#include "dialog_time.h"
#include "dialog_io.h"
#include "dialog_motor.h"
#include "dialog_home.h"
#include "dialog_home2.h"
#include "dialog_euromap.h"
#include "dialog_mode_detail.h"
#include "dialog_dualunload_mon.h"


#ifdef _CG3300_
#define SHORTCUT_MOVE_OFFSET    85
#elif _CG4300_
#define SHORTCUT_MOVE_OFFSET    105
#endif

#define SHORTCUT_VIEW_NUM       6


namespace Ui {
class form_autorun;
}

class form_autorun : public QWidget
{
    Q_OBJECT
    
public:
    explicit form_autorun(QWidget *parent = 0);
    ~form_autorun();

    void Update();

    bool m_bKeyHome;

    void BrokenHome();

public slots:
    void onClose();

    void onTimer_Data();        // for data update
    void onTimer_Status();      // for run status update (handling data update)

    void onProductInfo();
    void onArrayInfo();

    void onRun();
    void onStop();
    void on1Cycle();
    void onDischarge();
    void onSample();
    
    void onShortCut();
    void onSCutFwd();
    void onSCutBwd();

    void onHome_forKey();

private:
    Ui::form_autorun *ui;
    QTimer* timerData;
    QTimer* timerStatus;

    // Update = showevent 1time execution, Refresh = timer cyclic execution.
    // product info.
    void Update_ProductInfo();
    void Refresh_ProductInfo();
    int m_nCount_Refresh_ProductInfo;
    float m_fCycleTime;         // for calculated 1 day Product Qty.
    QVector<QLabel3*> m_vtProd;

    dialog_product_info* dig_product_info;
    dialog_array_info* dig_array_info;

    // array info. (uload & insert)
    void Update_ArrayInfo();
    void Refresh_ArrayInfo();
    int m_nCount_Refresh_ArrayInfo;
    QVector<QLabel3*> m_vtArray;

    // step info.
    QVector<QLabel3*> m_vtStepIcon;
    QVector<QLabel3*> m_vtStepNo;
    QVector<QLabel3*> m_vtStepName;
    int m_nStartIndex;

    int m_nMainRunIndex;
    CNR_TASK_STATUS m_nTaskStatus;

    void Update_StepInfo();
    void Refresh_StepInfo();

    void Redraw_List(int start_index);
    void Display_RunIcon(int run_index, CNR_TASK_STATUS task_status);
    void IconLine(int line, CNR_TASK_STATUS status);
    QVector<QPixmap> pxmRunIcon;

    // run control.
    bool m_bRun, m_bPause, m_bStop;

    void Update_RunCtrl();
    void Refresh_RunCtrl();

    void Display_BtnRun(bool bRun);
    QVector<QPixmap> pxmRun;

    bool m_bFisrt;
    void Display_1Cycle(bool enable);
    void Display_Discharge(bool enable, bool enable_1stDisc);
    void Display_Sample(bool enable);
    void SetBtnFlagAll(bool enable); // 1Cycle, Discharge, Sample flag data all set.


    // ShortCut
    QVector<QLabel4*> m_vtSCutPic;
    QVector<QLabel3*> m_vtSCutIcon;
    QVector<QLabel3*> m_vtSCut;

    int m_nSCutStartIndex;
    int m_nSCutX;

    void Redraw_SCut(int start_index);
    void Display_SCut();

    dialog_varmonitor* dig_varmonitor;
    dialog_position_auto* dig_position_auto;
    dialog_error* dig_error;
    dialog_time* dig_time;
    dialog_io* dig_io;
    dialog_motor* dig_motor;
    dialog_home* dig_home;
    dialog_home2* dig_home2;
    dialog_euromap* dig_euromap;
    dialog_mode_detail* dig_mode_detail;
    dialog_dualunload_mon* dig_dualunload_mon;

    dialog_sampling* dig_sampling;
    dialog_discharge* dig_discharge;

    bool Home(bool bCheck);
    void Process_FirstEntry();
    bool m_bFirstEntry;


    void Process_KeyHome();

    void onHome();

    bool m_bVarSave;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // FORM_AUTORUN_H
