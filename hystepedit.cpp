#include "hystepedit.h"

HyStepEdit::HyStepEdit()
{

}
HyStepEdit::~HyStepEdit()
{

}

bool HyStepEdit::Write_Basic()
{
    if(!IsCan()) return false;

    CNRobo* pCon = CNRobo::getInstance();
    QStringList temp;
    for(int i=0;i<4;i++)
        temp.append(m_StepData.Step[HyStepData::WAIT + i].ProgLine);

    if(pCon->setProgramSteps(m_StepData.m_strMainProgName, temp) < 0)
    {
        qDebug() << "setProgramSteps Fail!";
        return false;
    }

    if(pCon->saveProgram(m_StepData.m_strMainProgName) < 0) // add API
    {
        qDebug() << "saveProgram Fail!";
        return false;
    }

    qDebug() << "setProgramSteps Success!";
    return true;
}

bool HyStepEdit::Read_Main()
{
    if(!IsCan()) return false;

    CNRobo* pCon = CNRobo::getInstance();
    QStringList temp;

    if(pCon->getProgramSteps(m_StepData.m_strMainProgName, temp) < 0)
    {
        qDebug() << "getProgramSteps Fail!";
        return false;
    }

    ST_STEP_DATA data;
    int i,j;
    QStringList strlist;

    m_Step.clear(); // main list clear.

    for(i=0;i<temp.count();i++)
    {
        //data.ProgLine = temp.at(i).trimmed();

        strlist = temp.at(i).trimmed().split(";");
        data.ProgLine = strlist.at(0);
        if(strlist.size() >= 2)
            data.NickName = strlist.at(1);
        else
            data.NickName.clear();

        // remove "(...)"
        strlist = data.ProgLine.split("(");
        data.ProgLine = strlist.at(0);


        for(j=0;j<HyStepData::STEP_NUM;j++)
        {
            if(m_StepData.Step[j].ProgLine == data.ProgLine)
            {
                data.DispName = m_StepData.Step[j].DispName;
                data.Id = j;
                m_Step.append(data);
                // nick name save
                m_StepData.Step[j].NickName = data.NickName;
            }
        }
    }

    for(i=0;i<m_Step.count();i++)
        qDebug() << m_Step[i].DispName + "," + m_Step[i].ProgLine + "," + m_Step[i].NickName;

    qDebug() << "getProgramSteps Success!";
    return true;
}

bool HyStepEdit::Write_Main()
{
    if(!IsCan()) return false;
    CNRobo* pCon = CNRobo::getInstance();
    QStringList temp;

    // make QStringList
    temp.clear();

    // add default front step. but don't add ENUM_STEP.
    for(int i=0;i<m_Step.count();i++)
    {
        temp.append(m_Step[i].ProgLine+";"+m_Step[i].NickName);
    }
    // add default back step. but don't add ENUM_STEP.

    if(pCon->setProgramSteps(m_StepData.m_strMainProgName, temp) < 0)
    {
        qDebug() << "setProgramSteps Fail!";
        return false;
    }

    if(pCon->saveProgram(m_StepData.m_strMainProgName) < 0) // add API
    {
        qDebug() << "saveProgram Fail!";
        return false;
    }

    qDebug() << "setProgramSteps Success!";
    return true;
}

bool HyStepEdit::IsCan()
{
    CNRobo* pCon = CNRobo::getInstance();

    if(!pCon->isConnected()) return false;
    bool bOk;
    pCon->getLoginStatus(&bOk);
    if(!bOk) return false;
    pCon->getLockStatus(&bOk);
    if(!bOk) return false;

    return true;
}

// IsMode = mode or user.
// mode => no all editing
// mode is including sub_mode.
bool HyStepEdit::IsMode(ST_STEP_DATA StepData)
{
    switch(StepData.Id)
    {
    case HyStepData::WAIT:
    case HyStepData::TAKEOUT:
    case HyStepData::UP:
    case HyStepData::UNLOAD:
    case HyStepData::EXT_WAIT:
    case HyStepData::INSERT:
    case HyStepData::DEBURR:
    case HyStepData::INSERT_UNLOAD:
    case HyStepData::W_MEASURE:
    case HyStepData::W_RESET:
        return true;

    default:
        return false;
    }
}
bool HyStepEdit::IsMode(int i)
{
    return IsMode(m_Step[i]);
}

// sub_mode => no deleting, but yes editing.
bool HyStepEdit::IsSubMode(ST_STEP_DATA StepData)
{
    switch(StepData.Id)
    {
    case HyStepData::W_MEASURE:
    case HyStepData::W_RESET:
        return true;

    default:
        return false;
    }
}
bool HyStepEdit::IsSubMode(int i)
{
    return IsSubMode(m_Step[i]);
}

bool HyStepEdit::IsCanDelete(ST_STEP_DATA StepData)
{
    if(IsMode(StepData))
        return false;
    return true;
}
bool HyStepEdit::IsCanDelete(int i)
{
    return IsCanDelete(m_Step[i]);
}

bool HyStepEdit::IsCanRename(ST_STEP_DATA StepData)
{
    if(IsMode(StepData))
        return false;

    if(StepData.Id >= HyStepData::ADD_WORK0
    && StepData.Id <= HyStepData::ADD_WORKN)
        return false;

    return true;
}
bool HyStepEdit::IsCanRename(int i)
{
    return IsCanRename(m_Step[i]);
}

bool HyStepEdit::IsCanStepMove(ST_STEP_DATA StepData)
{
    if(IsMode(StepData))
    {
        if(IsSubMode(StepData))
            return true;
        else
            return false;
    }
    return true;
}
bool HyStepEdit::IsCanStepMove(int i)
{
    return IsCanStepMove(m_Step[i]);
}

ST_STEP_DATA HyStepEdit::View(int i)
{
    return m_Step.at(i);
}
bool HyStepEdit::HasNickname(int i)
{
    if(m_Step[i].NickName.isEmpty())
        return false;
    return true;
}


bool HyStepEdit::Insert(int i, ST_STEP_DATA StepData)
{
    if(i < 0) i=0;
    if(i > m_Step.size()) i=m_Step.size();

    m_Step.insert(i,StepData);

    qDebug("Insert [%d], Step=%d",i,StepData.Id);
    for(i=0;i<m_Step.count();i++)
        qDebug() << "[" << m_Step[i].Id << "]" << m_Step[i].DispName + "," + m_Step[i].ProgLine;

    return true;
}

bool HyStepEdit::Delete(int i)
{
    if(i < 0) i=0;
    if(i > m_Step.size()) i=m_Step.size();

    if(!IsCanDelete(i)) return false;

    ST_STEP_DATA data;
    data = m_Step.takeAt(i);

    qDebug("Delete [%d], Step=%d",i,data.Id);
    for(i=0;i<m_Step.count();i++)
        qDebug() << "[" << m_Step[i].Id << "]" << m_Step[i].DispName + "," + m_Step[i].ProgLine;

    return true;
}

bool HyStepEdit::Insert_forMode(int step)
{
    int ret;
    switch(step)
    {
    case HyStepData::EXT_WAIT:
        // 무조건 맨처음. (no use)

        if(!Make_forMode(m_StepData.Step[step]))
            return false;

        return(Insert(0,m_StepData.Step[step]));
        break;

    case HyStepData::INSERT:
        // case1. 무조건 맨뒤.
        //return(Insert(m_Step.size(),m_StepData.Step[step]));

        // case2. after UNLOAD
        ret = FindPos(HyStepData::UNLOAD);
        if(ret >= 1)
        {
            if(!Make_forMode(m_StepData.Step[step]))
                return false;

            return(Insert(ret+1,m_StepData.Step[step]));
        }

//        // case3. 무조건 맨앞으로...
//        if(!Make_forMode(m_StepData.Step[step]))
//            return false;

//        return(Insert(0,m_StepData.Step[step]));
        break;

    case HyStepData::INSERT_UNLOAD:

        ret = FindPos(HyStepData::TAKEOUT);
        if(ret >= 1)
        {
            if(!Make_forMode(m_StepData.Step[step]))
                return false;

            return(Insert(ret+1, m_StepData.Step[step]));
        }
        break;

    case HyStepData::DEBURR:
        ret = FindPos(HyStepData::UNLOAD);
        if(ret >= 1)
        {
            if(!Make_forMode(m_StepData.Step[step]))
                return false;

            return(Insert(ret,m_StepData.Step[step]));
        }

        break;

    case HyStepData::W_MEASURE:
        //case1. 무조건 맨뒤.
        return(Insert(m_Step.size(),m_StepData.Step[step]));
        break;

    case HyStepData::W_RESET:
        //case1. 무조건 맨뒤.
        return(Insert(m_Step.size(),m_StepData.Step[step]));
        break;
    }

    return false;
}

bool HyStepEdit::Delete_forMode(int step)
{
    if(!HasStep(step)) return true;

    switch(step)
    {
    case HyStepData::EXT_WAIT:
        return(ForceDelete(step));
        break;

    case HyStepData::INSERT:
        return(ForceDelete(step));
        break;

    case HyStepData::INSERT_UNLOAD:
        return(ForceDelete(step));
        break;

    case HyStepData::DEBURR:
        return(ForceDelete(step));
        break;

    case HyStepData::W_MEASURE:
        return(ForceDelete(step));
        break;

    case HyStepData::W_RESET:
        return(ForceDelete(step));
        break;
    }

    return false;
}

bool HyStepEdit::HasStep(int step)
{
    for(int i=0;i<m_Step.count();i++)
    {
        if(step == m_Step[i].Id)
            return true;
    }

    return false;
}

bool HyStepEdit::ForceDelete(int step)
{
    ST_STEP_DATA data;

    for(int i=0;i<m_Step.count();i++)
    {
        if(step == m_Step[i].Id)
        {
            data = m_Step.takeAt(i);

            qDebug("ForceDelete [%d], Step=%d",i,data.Id);
            for(i=0;i<m_Step.count();i++)
                qDebug() << "[" << m_Step[i].Id << "]" << m_Step[i].DispName + "," + m_Step[i].ProgLine;

            return true;
        }
    }
    return false;
}

// return false = over max. nunmber
bool HyStepEdit::Insert_Position(int pos)
{
    int i;
    QList<int> list;
    list.clear();

    int start = HyStepData::ADD_POS0;
    int end = start + MAX_USER_STEP_POS;
    for(i=0;i<m_Step.count();i++)
    {
        if(m_Step[i].Id >= start && m_Step[i].Id < end)
            list.append(m_Step[i].Id - start);
    }

    int next = FindNext(list);
    if(start+next < end)
    {
        if(!Make_UserPosition(m_StepData.Step[start+next]))
            return false;
        Insert(pos, m_StepData.Step[start+next]);
    }
    else
        return false;

    return true;
}

bool HyStepEdit::Insert_Work(int pos, QStringList work_macro)
{
    int i;
    QList<int> list;
    list.clear();

    int start = HyStepData::ADD_WORK0;
    int end = start + MAX_USER_STEP_WORK;
    for(i=0;i<m_Step.count();i++)
    {
        if(m_Step[i].Id >= start && m_Step[i].Id < end)
            list.append(m_Step[i].Id - start);
    }

    int next = FindNext(list);
    if(start+next < end)
    {
        if(!Make_UserWork(m_StepData.Step[start+next], work_macro))
            return false;
        Insert(pos, m_StepData.Step[start+next]);
    }
    else
        return false;

    return true;
}

// return false = over max. nunmber
bool HyStepEdit::Insert_Output(int pos)
{
    int i;
    QList<int> list;
    list.clear();

    int start = HyStepData::ADD_OUT0;
    int end = start + MAX_USER_STEP_OUT;
    for(i=0;i<m_Step.count();i++)
    {
        if(m_Step[i].Id >= start && m_Step[i].Id < end)
            list.append(m_Step[i].Id - start);
    }

    int next = FindNext(list);
    if(start+next < end)
    {
        if(!Make_UserOutput(m_StepData.Step[start+next]))
            return false;
        Insert(pos, m_StepData.Step[start+next]);
    }
    else
        return false;

    return true;
}
// return false = over max. nunmber
bool HyStepEdit::Insert_WaitInput(int pos)
{
    int i;
    QList<int> list;
    list.clear();

    int start = HyStepData::ADD_IN0;
    int end = start + MAX_USER_STEP_IN;
    for(i=0;i<m_Step.count();i++)
    {
        if(m_Step[i].Id >= start && m_Step[i].Id < end)
            list.append(m_Step[i].Id - start);
    }

    int next = FindNext(list);
    if(start+next < end)
    {
        if(!Make_UserWaitInput(m_StepData.Step[start+next]))
            return false;
        Insert(pos, m_StepData.Step[start+next]);
    }
    else
        return false;

    return true;
}
// return false = over max. nunmber
bool HyStepEdit::Insert_WaitTime(int pos)
{
    int i;
    QList<int> list;
    list.clear();

    int start = HyStepData::ADD_TIME0;
    int end = start + MAX_USER_STEP_TIME;
    for(i=0;i<m_Step.count();i++)
    {
        if(m_Step[i].Id >= start && m_Step[i].Id < end)
            list.append(m_Step[i].Id - start);
    }

    int next = FindNext(list);
    if(start+next < end)
    {
        if(!Make_UserWaitTime(m_StepData.Step[start+next]))
            return false;
        Insert(pos, m_StepData.Step[start+next]);
    }
    else
        return false;

    return true;
}

int HyStepEdit::FindNext(QList<int> lst)
{
    qSort(lst);

    int i=0;
    for(i=0;i<lst.count();i++)
    {
        if(lst[i] != i)
            return i;
    }
    return i;
}

bool HyStepEdit::Up(int pos)
{
    if(pos <= 0) return false;
    if(!IsCanStepMove(pos)) return false;

    m_Step.move(pos, pos-1);

    for(int i=0;i<m_Step.count();i++)
        qDebug() << "[" << m_Step[i].Id << "]" << m_Step[i].DispName + "," + m_Step[i].ProgLine;

    return true;
}

bool HyStepEdit::Down(int pos)
{
    if(pos >= m_Step.size()-1) return false;
    if(!IsCanStepMove(pos)) return false;

    m_Step.move(pos, pos+1);

    for(int i=0;i<m_Step.count();i++)
        qDebug() << "[" << m_Step[i].Id << "]" << m_Step[i].DispName + "," + m_Step[i].ProgLine;

    return true;
}

int HyStepEdit::FindPos(int step)
{
    for(int i=0;i<m_Step.count();i++)
    {
        if(m_Step[i].Id == step)
            return i;
    }

    return -1;
}


QString HyStepEdit::MakeDispName(int i)
{
    QString temp;
    temp.clear();

    if(i >= m_Step.size()) return temp;
    if(i < 0) return temp;

    temp = m_Step[i].DispName;
    if(HasNickname(i))
        temp += " ("+ m_Step[i].NickName + ")";

    return temp;
}

bool HyStepEdit::Make_UserPosition(ST_STEP_DATA data)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    // 1. read base program.
    QStringList steps;
    ret = pCon->getProgramSteps(m_StepData.m_strUserPositionProgBase, steps);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps ret=" << ret;
        return false;
    }

    // 2. add program line "arr = N"
    QString add_line = "arr = ";
    add_line += QString().setNum(data.Id - HyStepData::ADD_POS0);
    add_line += QString(";don't touch this value");

    steps.push_front(add_line);

    // 3. create program.
    QString prog_name = data.ProgLine.remove("Call").trimmed();
    qDebug() << prog_name;

    if(!pCon->findProgram(prog_name))
    {
        ret = pCon->createProgram(prog_name);
        if(ret < 0)
        {
            qDebug() << "createProgram ret=" << ret;
            return false;
        }
    }

    // 4. set program steps
    ret = pCon->setProgramSteps(prog_name, steps);
    if(ret < 0)
    {
        qDebug() << "setProgramSteps ret=" << ret;
        return false;
    }

    //5. save program
    ret = pCon->saveProgram(prog_name);
    if(ret < 0)
    {
        qDebug() << "saveProgram ret=" << ret;
        return false;
    }

    qDebug() << "Make_UserPosition Success!";
    return true;
}

bool HyStepEdit::Make_UserWork(ST_STEP_DATA data, QStringList macro)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    // 1. read base program.
    QStringList steps;
    ret = pCon->getProgramSteps(m_StepData.m_strUserWorkProgBase, steps);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps ret=" << ret;
        return false;
    }

    // 2. add program line "arr = N"
    QString add_line = "arr = ";
    add_line += QString().setNum(data.Id - HyStepData::ADD_WORK0);
    add_line += QString(";don't touch this value");

    steps.push_front(add_line);

    // 2-1. add work macro
    steps << ";Start Sasang Macro";
    steps << macro;

    // 3. create program.
    QString prog_name = data.ProgLine.remove("Call").trimmed();
    qDebug() << prog_name;

    if(!pCon->findProgram(prog_name))
    {
        ret = pCon->createProgram(prog_name);
        if(ret < 0)
        {
            qDebug() << "createProgram ret=" << ret;
            return false;
        }
    }

    // 4. set program steps
    ret = pCon->setProgramSteps(prog_name, steps);
    if(ret < 0)
    {
        qDebug() << "setProgramSteps ret=" << ret;
        return false;
    }

    //5. save program
    ret = pCon->saveProgram(prog_name);
    if(ret < 0)
    {
        qDebug() << "saveProgram ret=" << ret;
        return false;
    }

    qDebug() << "Make_UserWork Success!";
    return true;
}

bool HyStepEdit::Make_UserOutput(ST_STEP_DATA data)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    // 1. read base program.
    QStringList steps;
    ret = pCon->getProgramSteps(m_StepData.m_strUserOutputProgBase, steps);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps ret=" << ret;
        return false;
    }

    // 2. add program line "arr = N"
    QString add_line = "arr = ";
    add_line += QString().setNum(data.Id - HyStepData::ADD_OUT0);
    add_line += QString(";don't touch this value");

    steps.push_front(add_line);

    // 3. create program.
    QString prog_name = data.ProgLine.remove("Call").trimmed();
    qDebug() << prog_name;

    if(!pCon->findProgram(prog_name))
    {
        ret = pCon->createProgram(prog_name);
        if(ret < 0)
        {
            qDebug() << "createProgram ret=" << ret;
            return false;
        }
    }

    // 4. set program steps
    ret = pCon->setProgramSteps(prog_name, steps);
    if(ret < 0)
    {
        qDebug() << "setProgramSteps ret=" << ret;
        return false;
    }

    //5. save program
    ret = pCon->saveProgram(prog_name);
    if(ret < 0)
    {
        qDebug() << "saveProgram ret=" << ret;
        return false;
    }

    qDebug() << "Make_UserOutput Success!";
    return true;
}

bool HyStepEdit::Make_UserWaitInput(ST_STEP_DATA data)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    // 1. read base program.
    QStringList steps;
    ret = pCon->getProgramSteps(m_StepData.m_strUserWaitInputProgBase, steps);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps ret=" << ret;
        return false;
    }

    // 2. add program line "arr = N"
    QString add_line = "arr = ";
    add_line += QString().setNum(data.Id - HyStepData::ADD_IN0);
    add_line += QString(";don't touch this value");

    steps.push_front(add_line);

    // 3. create program.
    QString prog_name = data.ProgLine.remove("Call").trimmed();
    qDebug() << prog_name;

    if(!pCon->findProgram(prog_name))
    {
        ret = pCon->createProgram(prog_name);
        if(ret < 0)
        {
            qDebug() << "createProgram ret=" << ret;
            return false;
        }
    }

    // 4. set program steps
    ret = pCon->setProgramSteps(prog_name, steps);
    if(ret < 0)
    {
        qDebug() << "setProgramSteps ret=" << ret;
        return false;
    }

    //5. save program
    ret = pCon->saveProgram(prog_name);
    if(ret < 0)
    {
        qDebug() << "saveProgram ret=" << ret;
        return false;
    }

    qDebug() << "Make_UserWaitInput Success!";
    return true;
}

bool HyStepEdit::Make_UserWaitTime(ST_STEP_DATA data)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    // 1. read base program.
    QStringList steps;
    ret = pCon->getProgramSteps(m_StepData.m_strUserWaitTimeProgBase, steps);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps ret=" << ret;
        return false;
    }

    // 2. add program line "arr = N" N= 0~max.
    QString add_line = "arr = ";
    add_line += QString().setNum(data.Id - HyStepData::ADD_TIME0);
    add_line += QString(";don't touch this value");

    steps.push_front(add_line);

    // 3. create program.
    QString prog_name = data.ProgLine.remove("Call").trimmed();
    qDebug() << prog_name;

    if(!pCon->findProgram(prog_name))
    {
        ret = pCon->createProgram(prog_name);
        if(ret < 0)
        {
            qDebug() << "createProgram ret=" << ret;
            return false;
        }
    }

    // 4. set program steps
    ret = pCon->setProgramSteps(prog_name, steps);
    if(ret < 0)
    {
        qDebug() << "setProgramSteps ret=" << ret;
        return false;
    }

    //5. save program
    ret = pCon->saveProgram(prog_name);
    if(ret < 0)
    {
        qDebug() << "saveProgram ret=" << ret;
        return false;
    }

    qDebug() << "Make_UserWaitInput Success!";
    return true;
}

bool HyStepEdit::Make_forMode(ST_STEP_DATA data)
{
    qDebug() << "Make_forMode not use. this files have always in recipe programs.";
    return true;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    // 1. make path + program name.
    QString prog_name = data.ProgLine.remove("Call").trimmed();
    QString default_path = RM_RECIPE_PATH;
    default_path += RM_DEFAULT_DIRNAME;
    default_path += RM_PROG_DIRNAME;
    default_path += "/";
    default_path += prog_name;

    // 2. copy /default/programs/mode_macro_prog...

    qDebug() << default_path;
    ret = pCon->importProgram(default_path);
    if(ret < 0)
    {
        qDebug() << "getProgramSteps ret=" << ret;
        return false;
    }
    ret = pCon->saveProgram(prog_name);
    if(ret < 0)
    {
        qDebug() << "saveProgram ret =" << ret;
        return false;
    }

    qDebug() << "Make_forMode Success!" << prog_name;
    return true;
}

