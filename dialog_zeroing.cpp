#include "dialog_zeroing.h"
#include "ui_dialog_zeroing.h"

dialog_zeroing::dialog_zeroing(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_zeroing)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    wmJog = new widget_jogmodule3(ui->grpJog);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    m_vtTrans.clear();
    m_vtTrans.append(ui->lbXval);
    m_vtTrans.append(ui->lbYval);
    m_vtTrans.append(ui->lbZval);
    m_vtTrans.append(ui->lbAval);
    m_vtTrans.append(ui->lbBval);
    m_vtTrans.append(ui->lbCval);

    m_vtJoint.clear();
    m_vtJoint.append(ui->lbJ1val);
    m_vtJoint.append(ui->lbJ2val);
    m_vtJoint.append(ui->lbJ3val);
    m_vtJoint.append(ui->lbJ4val);
    m_vtJoint.append(ui->lbJ5val);
    m_vtJoint.append(ui->lbJ6val);

    m_vtJ.clear();
    m_vtJ.append(ui->lbJ1);
    m_vtJ.append(ui->lbJ2);
    m_vtJ.append(ui->lbJ3);
    m_vtJ.append(ui->lbJ4);
    m_vtJ.append(ui->lbJ5);
    m_vtJ.append(ui->lbJ6);

    m_vtJPic.clear();            m_vtJvalPic.clear();
    m_vtJPic.append(ui->lbJ1Pic);m_vtJvalPic.append(ui->lbJ1valPic);
    m_vtJPic.append(ui->lbJ2Pic);m_vtJvalPic.append(ui->lbJ2valPic);
    m_vtJPic.append(ui->lbJ3Pic);m_vtJvalPic.append(ui->lbJ3valPic);
    m_vtJPic.append(ui->lbJ4Pic);m_vtJvalPic.append(ui->lbJ4valPic);
    m_vtJPic.append(ui->lbJ5Pic);m_vtJvalPic.append(ui->lbJ5valPic);
    m_vtJPic.append(ui->lbJ6Pic);m_vtJvalPic.append(ui->lbJ6valPic);

    int i;
    for(i=0;i<m_vtJPic.count();i++)
    {
        connect(m_vtJPic[i],SIGNAL(mouse_release()),this,SLOT(onMultiTurnClear()));
        connect(m_vtJ[i],SIGNAL(mouse_press()),m_vtJPic[i],SLOT(press()));
        connect(m_vtJ[i],SIGNAL(mouse_release()),m_vtJPic[i],SLOT(release()));

        connect(m_vtJvalPic[i],SIGNAL(mouse_release()),this,SLOT(onEachZero()));
        connect(m_vtJoint[i],SIGNAL(mouse_press()),m_vtJvalPic[i],SLOT(press()));
        connect(m_vtJoint[i],SIGNAL(mouse_release()),m_vtJvalPic[i],SLOT(release()));
    }

    connect(ui->btnZeroPic,SIGNAL(mouse_release()),this,SLOT(onZero()));
    connect(ui->btnZero,SIGNAL(mouse_press()),ui->btnZeroPic,SLOT(press()));
    connect(ui->btnZero,SIGNAL(mouse_release()),ui->btnZeroPic,SLOT(release()));
    connect(ui->btnZeroIcon,SIGNAL(mouse_press()),ui->btnZeroPic,SLOT(press()));
    connect(ui->btnZeroIcon,SIGNAL(mouse_release()),ui->btnZeroPic,SLOT(release()));

    connect(ui->btnServoPic,SIGNAL(mouse_release()),this,SLOT(onServo()));
    connect(ui->btnServoIcon,SIGNAL(mouse_press()),ui->btnServoPic,SLOT(press()));
    connect(ui->btnServoIcon,SIGNAL(mouse_release()),ui->btnServoPic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnJogPic,SIGNAL(mouse_release()),this,SLOT(onJogChange()));
    connect(ui->btnJog,SIGNAL(mouse_press()),ui->btnJogPic,SLOT(press()));
    connect(ui->btnJog,SIGNAL(mouse_release()),ui->btnJogPic,SLOT(release()));

#ifdef _H6_
    ui->lbJ6->setText(tr("J6"));

    ui->lbJ6->setEnabled(true);
    ui->lbJ6Pic->setEnabled(true);
    ui->lbJ6val->setEnabled(true);
    ui->lbJ6valPic->setEnabled(true);
#else
    ui->lbJ6->setText("");

    ui->lbJ6->setEnabled(false);
    ui->lbJ6Pic->setEnabled(false);
    ui->lbJ6val->setEnabled(false);
    ui->lbJ6valPic->setEnabled(false);
#endif

    // init. variable & pointer.
    top = 0;
    pxmServo.clear();
}

dialog_zeroing::~dialog_zeroing()
{
    delete ui;
}
void dialog_zeroing::showEvent(QShowEvent *)
{
    gReadyJog(true);
    SetJog(false);

    timer->start(100);

    ui->btnSelectPic->hide();

    m_bZeroing = false;

    SubTop();
}
void dialog_zeroing::hideEvent(QHideEvent *)
{
    gReadyJog(false);
    gSetJogType(JOG_TYPE_HY);

    timer->stop();
}
void dialog_zeroing::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_zeroing::onClose()
{
    emit accept();
}

void dialog_zeroing::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    m_JointData = pCon->getCurJoint();
    m_TransData = pCon->getCurTrans();

    Display_CurPos(m_TransData, m_JointData);

    // servo
    if(pCon->getServoOn())
        SetServo(true);
    else
        SetServo(false);
}
void dialog_zeroing::onZero()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(QString(tr("Zeroing")));
    conf->Message(QString(tr("Would you want to zeroing all joint?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    CNRobo* pCon = CNRobo::getInstance();
    pCon->zeroing(-1);

    dialog_delaying* dig = new dialog_delaying();
    dig->Init(500);
    dig->exec();

    Save();
}
void dialog_zeroing::onServo()
{
    CNRobo* pCon = CNRobo::getInstance();
    if(pCon->getServoOn())
        pCon->setServoOn(false);
    else
        pCon->setServoOn(true);
}

void dialog_zeroing::SetServo(bool onoff)
{
    if(pxmServo.isEmpty())
    {
        QImage img;
        img.load(":/icon/icon/icon999-14.png");
        pxmServo.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon999-13.png");
        pxmServo.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmServo.size())
        ui->btnServoIcon->setPixmap(pxmServo[(int)onoff]);
}

void dialog_zeroing::Save()
{
    cn_trans pos;
    cn_joint jpos;

    int i;
    for(i=0;i<3;i++)
    {
        pos.p[i] = m_vtTrans[i]->text().toFloat();
        pos.eu[i] = m_vtTrans[i+3]->text().toFloat();
    }

    for(i=0;i<USE_AXIS_NUM;i++)
        jpos.joint[i] = m_vtJoint[i]->text().toFloat();

    Posi->WR(HyPos::ZERO, pos, jpos, 0.0, 0.0);

}

void dialog_zeroing::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}

void dialog_zeroing::Display_CurPos(float* t, float* j)
{
    QString str;

    int i;
    for(i=0;i<MAX_AXIS_NUM;i++)
    {
        if(i < 6)  // 6 is fix.
        {
            str.sprintf(FORMAT_POS, t[i]);
            m_vtTrans[i]->setText(str);
        }

        if(i < USE_AXIS_NUM)
        {
            str.sprintf(FORMAT_POS, j[i]);
            m_vtJoint[i]->setText(str);
        }
    }
}
void dialog_zeroing::onReset()
{
    dialog_delaying* dig = new dialog_delaying();
    CNRobo* pCon = CNRobo::getInstance();
    pCon->resetEcatError();
    dig->Init(500);
    dig->exec();
    pCon->resetError();
    pCon->clearWarningCode();
}
void dialog_zeroing::onJogChange()
{
    if(m_bJnT)
        SetJog(false);
    else
        SetJog(true);
}
void dialog_zeroing::SetJog(bool bJnT)
{
    if(bJnT)
    {
        ui->btnJog->setText(tr("T"));
        gSetJogType(JOG_TYPE_TRANS);
    }
    else
    {
        ui->btnJog->setText(tr("J"));
        gSetJogType(JOG_TYPE_JOINT);
    }
    m_bJnT = bJnT;
}

void dialog_zeroing::onEachZero()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtJvalPic.indexOf(btn);
    if(index < 0) return;
    if(index >= USE_AXIS_NUM) return;

    // confirm
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(QString(tr("Each Zeroing")));
    conf->Message(QString(tr("Would you want to zeroing this joint?")),m_vtJ[index]->text());
    if(conf->exec() != QDialog::Accepted)
        return;

    unsigned long mask = 0x0001;
    mask <<= index;

    CNRobo* pCon = CNRobo::getInstance();
    pCon->zeroing(mask);

    dialog_delaying* dig = new dialog_delaying();
    dig->Init(500);
    dig->exec();

}
void dialog_zeroing::onMultiTurnClear()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtJPic.indexOf(btn);
    if(index < 0) return;
    if(index >= USE_AXIS_NUM) return;

    // confirm
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(QString(tr("Multi-Turn Clear")));
    conf->Message(QString(tr("Would you want to clear multi-turn data?")),m_vtJ[index]->text());
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_delaying* dig = new dialog_delaying();
    dig->Init(500);
    Motors->Ready_MultiTurnClear(index);
    dig->exec();
    Motors->Set_MultiTurnClear(index);
    dig->exec();
    Motors->Reset_MultiTurnClear(index);
}

