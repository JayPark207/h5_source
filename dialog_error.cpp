#include "dialog_error.h"
#include "ui_dialog_error.h"

dialog_error::dialog_error(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_error)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));
    timer->setInterval(200);

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnReset,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnReset,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnRecentPic,SIGNAL(mouse_release()),this,SLOT(onRecently()));
    connect(ui->btnRecent,SIGNAL(mouse_press()),ui->btnRecentPic,SLOT(press()));
    connect(ui->btnRecent,SIGNAL(mouse_release()),ui->btnRecentPic,SLOT(release()));
    connect(ui->btnRecentIcon,SIGNAL(mouse_press()),ui->btnRecentPic,SLOT(press()));
    connect(ui->btnRecentIcon,SIGNAL(mouse_release()),ui->btnRecentPic,SLOT(release()));

    connect(ui->btnBzoffPic,SIGNAL(mouse_release()),this,SLOT(onBzOff()));
    connect(ui->btnBzoffIcon,SIGNAL(mouse_press()),ui->btnBzoffPic,SLOT(press()));
    connect(ui->btnBzoffIcon,SIGNAL(mouse_release()),ui->btnBzoffPic,SLOT(release()));

    m_bNewError = false;
    m_bNewErrorWrite = false;
    m_MotorErrorCode.clear();
    dig_error_list = 0;
}

dialog_error::~dialog_error()
{
    delete ui;
}
void dialog_error::showEvent(QShowEvent *)
{
    timer->start();
    m_nCode=0;m_nAxis=0;m_nSubCode=0;

}
void dialog_error::hideEvent(QHideEvent *)
{
    timer->stop();

    // 10 error list write user parameter.
    if(m_bNewErrorWrite)
    {
        qDebug() << "hide Error->Write() start";
        m_bNewErrorWrite = false;
        Error->Write();
        qDebug() << "hide Error->Write() end";
    }
}
void dialog_error::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_error::onClose()
{
    emit reject();
}

void dialog_error::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();
    int i;
    QString str;
    USHORT code;

    if(Status->bError)
    {
        if(m_nCode == 0)
        {
            if(Status->bErr)
                pCon->getLastErrorInfo(m_nCode, m_nAxis, m_nSubCode);
            else if(Status->bWarn)
                pCon->getWarningCode(m_nCode);

            // error 10 list.
            if(m_bNewError)
            {
                Error->Error(m_nCode);
                m_bNewError = false;
                m_bNewErrorWrite = true;
            }

            // motor error log & display
            m_MotorErrorCode.clear();
            if(m_nCode == -1505) // motor error
            {
                // read motor error code.
                Motors->Update();
                for(i=0;i<USE_AXIS_NUM;i++)
                {
                    code = Motors->GetErrorCode(i);
                    if((code&0x00FF) < 0xA0)
                        str.sprintf("%d", code & 0x00FF);
                    else
                        str.sprintf("%X", code & 0x00FF);

                    m_MotorErrorCode.append(str);
                }

                // logging motor error code.
                Log->Set(m_nCode, m_MotorErrorCode.join("/"));
            }

            // default error msg
            m_ErrorMsg.clear();
            pCon->getErrorMsg(m_ErrorMsg);

            for(i=0;i<MAX_ERROR_BUFFER;i++)
                qDebug() << i <<Error->GetCode(i);
        }
    }
    else
    {
        m_nCode = 0;
        m_MotorErrorCode.clear();
    }

    Status->SetErrCode(m_nCode);

    ui->lbCode->setText(QString().setNum(m_nCode));

    QString msg = Error->GetName(m_nCode);
    if(msg == "N/A")
        ui->lbName->setText(tr("Error"));
    else
        ui->lbName->setText(msg);

    if(m_MotorErrorCode.isEmpty())
    {
        msg = Error->GetDetail(m_nCode);
        if(msg == "N/A")
            ui->lbDetail->setText(m_ErrorMsg);
            //ui->lbDetail->setText(msg);
        else
            ui->lbDetail->setText(msg);
    }
    else
    {
        str = QString(tr("Error Code"));
        str += QString(" : ");
        str += m_MotorErrorCode.join(" / ");
        ui->lbDetail->setText(str);
    }

}
void dialog_error::onReset()
{
    dialog_delaying* dig = new dialog_delaying();
    CNRobo* pCon = CNRobo::getInstance();

    pCon->resetEcatError();
    dig->Init(500);
    dig->exec();
    pCon->resetError();
    pCon->clearWarningCode();
}

void dialog_error::onRecently()
{
    if(dig_error_list == 0)
        dig_error_list = new dialog_error_list();

    dig_error_list->exec();
}

void dialog_error::onBzOff()
{
    int out_num = -17;
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setDO(&out_num, 1);
}
