#ifndef DIALOG_MODE_DETAIL_H
#define DIALOG_MODE_DETAIL_H

#include <QDialog>

#include "global.h"
#include "qlabel4.h"
#include "qlabel3.h"

#include "dialog_mode_select.h"
#include "dialog_moldclean.h"
#include "dialog_takeout_method.h"
#include "dialog_numpad.h"
#include "dialog_weight.h"
#include "dialog_temperature.h"
#include "dialog_jmotion.h"
#include "dialog_syncback.h"
#include "dialog_unload_method.h"
#include "dialog_e_sensor.h"
#include "dialog_dualunload.h"
#include "dialog_unloadback.h"
#include "dialog_takeout_timing.h"

#include "top_sub.h"

namespace Ui {
class dialog_mode_detail;
}

class dialog_mode_detail : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_mode_detail(QWidget *parent = 0);
    ~dialog_mode_detail();

    void Update();

    void SelectGrp(int grp);

    void Update(int grp);
    void Redraw_List(int grp, int start_index);

    void Display_BtnGrp(int grp);

    void Set_Readonly(bool on);


public slots:
    void onEnd();

    void onChangeGrp();

    void onListUp();
    void onListDown();

    void onListItem();

private:
    Ui::dialog_mode_detail *ui;

    int m_nSelectedGrp;
    int m_nSelectedIndex;
    int m_nStartIndex;

    QVector<QLabel4*> m_vtBtnGrpPic;
    QVector<QLabel3*> m_vtBtnGrp;

    QVector<QLabel3*> m_vtTbNo;
    QVector<QLabel4*> m_vtTbNamePic;
    QVector<QLabel3*> m_vtTbName;
    QVector<QLabel3*> m_vtTbData;

    void SetItem(int grp, int index);

    bool m_bReadonly;

    bool IsMode(int grp, int index, int var);
    bool IsHighLevel(int level);

    top_sub* top;
    void SubTop();          // Need SubTop

private: // dialogs...
    dialog_mode_select* dig_mode_select;
    dialog_moldclean* dig_moldclean;
    dialog_takeout_method* dig_takeout_method;
    dialog_numpad* dig_numpad;
    dialog_weight* dig_weight;
    dialog_temperature* dig_temp;
    dialog_jmotion* dig_jmotion;
    dialog_syncback* dig_syncback;
    dialog_unload_method* dig_unload_method;
    dialog_e_sensor* dig_e_sensor;
    dialog_dualunload* dig_dualunload;
    dialog_unloadback* dig_unloadback;
    dialog_takeout_timing* dig_takeout_timing;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_MODE_DETAIL_H
