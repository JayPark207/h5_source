#ifndef FORM_STEPEDIT_USERINPUT_H
#define FORM_STEPEDIT_USERINPUT_H

#include <QWidget>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_message.h"

#define USERIN_EXTEND 7 // buffer index

namespace Ui {
class form_stepedit_userinput;
}

class form_stepedit_userinput : public QWidget
{
    Q_OBJECT

public:
    explicit form_stepedit_userinput(QWidget *parent = 0);
    ~form_stepedit_userinput();

    void Init(int index, QString title, int buf_index, bool only_time = false);

    void Update();


    void MatchingInput();                   // initial.


signals:
    void sigClose();

public slots:
    void onOK();
    void onCancel();
    void onInput();
    void onDelay();
    void onType();
    void onCond();

private:
    Ui::form_stepedit_userinput *ui;
    dialog_numpad* np;

    int m_nSelectedIndex;
    int m_nBufIndex;
    QString m_strTitle;

    bool    m_bOnlyTime;
    void SetDisplay(bool only_time);

    ST_USERDATA_INPUT m_Data;
    ST_USERDATA_TIME m_TimeData;

    QVector<QLabel3*> m_vtIcon;
    QVector<QLabel3*> m_vtSel;
    QVector<QLabel4*>   m_vtPic;          // m_vtInputNum 1:1 matching
    QVector<int>        m_vtInputNum;       // 1~128 (중복안됨)

    void DisplayInput();
    void SetInput(int index);
    void SetInput(int pic_index, int sel_index);
    void ResetInput(int index);

    void DisplayDelay();

    void SetType(HyUserData::ENUM_TYPE type);
    void SetCond(HyUserData::ENUM_CONDITION cond);

    bool Save();

    bool IsMaxSelect(ST_USERDATA_INPUT& data);
    int  GetEmptyIndex(ST_USERDATA_INPUT& data); // 0~7 empty index, -1 = max
    int  GetSelIndex(int pic_index);    // 0~7 sel_index, -1=none

    // for extend
    void Init_Extend(ST_USERDATA_INPUT& data);
    void Set_Extend(int extend_input_num);
    bool IsOK_Extend(int input_num);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // FORM_STEPEDIT_USERINPUT_H
