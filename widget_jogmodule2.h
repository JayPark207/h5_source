#ifndef WIDGET_JOGMODULE2_H
#define WIDGET_JOGMODULE2_H

#include <QWidget>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

namespace Ui {
class widget_jogmodule2;
}

class widget_jogmodule2 : public QWidget
{
    Q_OBJECT

public:
    explicit widget_jogmodule2(QWidget *parent = 0);
    ~widget_jogmodule2();

public slots:
    void onJogType();
    void onSpeedUp();
    void onSpeedDown();
    void onJogInch();

    void onTimer();

private:
    Ui::widget_jogmodule2 *ui;

    QVector<QLabel3*> m_vtInch;
    QVector<QLabel4*> m_vtInchPic;

    int m_nJogSpeed;

    void SetJogInch(bool on);
    void SetInch(int i, bool on);

    QTimer* timer;
    void SetSpeed(int speed);
    void GetSpeed(int& speed);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // WIDGET_JOGMODULE2_H
