#ifndef DIALOG_MODE_EASY_H
#define DIALOG_MODE_EASY_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_confirm.h"
#include "dialog_message.h"

#include "dialog_mode_detail.h"
#include "dialog_mode_easy_view.h"

#include "top_sub.h"

namespace Ui {
class dialog_mode_easy;
}

class dialog_mode_easy : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_mode_easy(QWidget *parent = 0);
    ~dialog_mode_easy();

    void Update();

private:
    Ui::dialog_mode_easy *ui;

    dialog_mode_detail* dig_mode_detail;
    dialog_mode_easy_view* dig_mode_easy_view;

    void Update_List();
    QVector<QLabel3*> m_vtList;
    int m_nSelectedIndex, m_nStartIndex;
    void Redraw_List(int start_index);
    void Display_ListOn(int table_index);

    void Display_Info(int select_index);

    top_sub* top;
    void SubTop();          // Need SubTop

public slots:
    void onClose();

    void onList();
    void onUp();
    void onDown();

    void onDetail();
    void onAdd();
    void onDel();

    void onSet();
    void onPreview();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_MODE_EASY_H
