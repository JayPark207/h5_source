#ifndef DIALOG_ANTI_VIBRATION_H
#define DIALOG_ANTI_VIBRATION_H

#include <QDialog>
#include <QStackedWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_anti_vibration;
}

class dialog_anti_vibration : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_anti_vibration(QWidget *parent = 0);
    ~dialog_anti_vibration();

    void Update();
    void Update(int panel_index);

private:
    Ui::dialog_anti_vibration *ui;

    // Main Tabs
    QStackedWidget* Panel;
    QVector<QLabel3*> m_vtTabLed;
    QVector<QLabel3*> m_vtTab;
    QVector<QLabel4*> m_vtTabPic;
    void Display_Tab(int tab_index);

    // Smooth
    void Update_Smooth();
    bool m_bSUse;float m_fSTime;
    void Display_Smooth(bool use, float stime);

public slots:
    void onClose();
    void onTab();

    void onSUse();
    void onSTime();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ANTI_VIBRATION_H
