#include "global.h"

// hy classes
HyPos*              Posi;
HyTime*             Time;
HyInput*            Input;
HyOutput*           Output;
HyUserData*         UserData;

HyParam*            Param;
HyRecipe*           Recipe;
HyRecipeManage2*    RM;
//HyLedBuzzer*        Pendent;
HyPendent*          Pendent;
HyStepEdit*         StepEdit;
HyError*            Error;
HyStatus*           Status;
HyRobotConfig*      RobotConfig;
HyTPonly*           TPonly;
HyLog*              Log;
HyBackupRecovery*   BR;
HyUseSkip*          UseSkip;
HyMode*             Mode;
HyLanguage*         Language;
HySoftSensor*       SoftSensor;
HyEcatMotor*        Motors;
HySasang*           Sasang;
HyEuromap*          Euromap;
HyNetwork*          Network;
HyBigMode*          BigMode;


// globals
PAGE_INDEX g_CurrentMainPage;
PAGE_INDEX g_PreMainPage;
TOP_INDEX g_CurrentTopPage;
TOP_INDEX g_PreTopPage;
int g_nAllSpeed=5;
int g_nJogSpeed=5;

ST_MOLDDATA g_stMoldData[MAX_MOLDDATA_NUM];   // db
ST_MOLDDATA g_stMoldDataLoaded;               // db
ST_MACHINE  Machine;

QTranslator gTranslator;
QLocale     gLocale;

KEY_COMMAND g_nKeyCommand_Press;
KEY_COMMAND g_nKeyCommand_Release;


// for button press action (label2, label4)
QWidget*    gLabelWidget;
QPoint      gLabelReleasePoint;
volatile int g_nPxmIndex;
QVector<QPixmap> g_vtPxmNoPress;
QVector<QImage> g_vtImgNoPress;
QVector<QPixmap> g_vtPxmPress;
QVector<QImage> g_vtImgPress;
QVector<QPixmap> g_vtPxmLabel2;


/** Global Functions **/
// 조그이동 설정해제하는 함수.
void gReadyJog(bool enable)
{
    CNRobo* pCon = CNRobo::getInstance();

    if(enable)
    {
        if(pCon->isError())
            pCon->resetError();

        pCon->resetEcatError();

        if(pCon->isWarning())
            pCon->clearWarningCode();

        pCon->setJogOff(0xFFFFFFFF,0xFFFFFFFF); // jog all stop

        if(!pCon->getServoOn())
            pCon->setServoOn(true);

        if(!pCon->getEnableOn())
            pCon->setEnableOn(true);

        if(!pCon->getTeachMode())
            pCon->setTeachMode(true);

        pCon->setTeachSpeed(0);

        if(pCon->getInchingOn())
            pCon->setInchingOn(false);
    }
    else
    {
        pCon->setEnableOn(false);
    }
}

void gReadyMacro(bool enable)
{
    CNRobo* pCon = CNRobo::getInstance();

    if(enable)
    {
        if(pCon->isError())
            pCon->resetError();

        pCon->resetEcatError();

        if(pCon->isWarning())
            pCon->clearWarningCode();

        if(!pCon->getServoOn())
            pCon->setServoOn(true);

        if(!pCon->getEnableOn())
            pCon->setEnableOn(true);

        if(pCon->getTeachMode())
            pCon->setTeachMode(false);

        float _main_speed = Param->Get(HyParam::MAIN_SPEED).toFloat();
        pCon->setSpeed(_main_speed);

        if(pCon->getInchingOn())
            pCon->setInchingOn(false);
    }
    else
    {
        pCon->setEnableOn(false);

        if(!pCon->getTeachMode())
            pCon->setTeachMode(true);
    }
}

// 현재스텝 모드에따른 사용위치리스트 만드는함수.
void gMakeUsePosList(HyStepData::ENUM_STEP step, QVector<int>& use_pos)
{
    float data;
    //float data2;

    if(step == HyStepData::WAIT)
    {
        if(Recipe->Get(HyRecipe::mdExternWait, &data))
        {
            if((int)data > 0)
                use_pos.append(HyPos::EXT_WAIT);
        }

        use_pos.append(HyPos::WAIT);

        if(Recipe->Get(HyRecipe::mdCloseWait, &data))
        {
            if((int)data > 0)
                use_pos.append(HyPos::CLOSE_WAIT);
        }

    }
    else if(step == HyStepData::TAKEOUT)
    {
        use_pos.append(HyPos::TAKE_OUT);
        if(Recipe->Get(HyRecipe::mdUndercut, &data))
        {
            for(int i=0;i<(int)data;i++)
                use_pos.append(HyPos::UNDERCUT1+i);
        }
    }
    else if(step == HyStepData::UP)
    {
        use_pos.append(HyPos::UP1ST);
    }
    else if(step == HyStepData::UNLOAD)
    {
        // unload middle postion
        if(Recipe->Get(HyRecipe::mdUnloadMidPos, &data))
        {
            if(data > 0)
                use_pos.append(HyPos::UNLOAD_MID);
        }

        use_pos.append(HyPos::UNLOAD);
        /* // not use weight position
        if(Recipe->Get(HyRecipe::mdWeight1, &data)
        && Recipe->Get(HyRecipe::mdWeight2, &data2))
        {
            if(data > 0 || data2 > 0)
                use_pos.append(HyPos::WEIGHT);
        }*/

        // dual unload
        if(Recipe->Get(HyRecipe::mdDualUnload, &data))
        {
            if(data > 0)
                use_pos.append(HyPos::UNLOAD2);
        }

    }
    else if(step == HyStepData::INSERT)
    {
        use_pos.append(HyPos::INSERT_MID);
        use_pos.append(HyPos::INSERT);
    }
    else if(step == HyStepData::INSERT_UNLOAD)
    {
        use_pos.append(HyPos::INSERT_UNLOAD);
    }
}
void gMakeUsePosList(QList<ST_STEP_DATA>& step_info, QVector<int>& use_pos)
{
    int i,_id=0;

    use_pos.clear();

    for(i=0;i<step_info.count();i++)
    {
        _id = step_info[i].Id;

        if(_id >= HyStepData::WAIT && _id <= HyStepData::INSERT_UNLOAD)
        {
            gMakeUsePosList((HyStepData::ENUM_STEP)_id, use_pos);
        }
        else if(_id >= HyStepData::ADD_POS0 &&
                _id < HyStepData::ADD_POS0 + MAX_USER_STEP_POS)
        {
            use_pos.append((_id - HyStepData::ADD_POS0) + USER_POS_KNOW_OFFSET);
        }
        else if(_id >= HyStepData::ADD_WORK0 &&
                _id < HyStepData::ADD_WORK0 + MAX_USER_STEP_WORK)
        {
            use_pos.append((_id - HyStepData::ADD_WORK0) + (USER_POS_KNOW_OFFSET*2));
        }
    }
}

// Change Jog Type
JOG_TYPE g_JogType = JOG_TYPE_HY;
void gSetJogType(JOG_TYPE jog_type)
{
    g_JogType = jog_type;
}
JOG_TYPE gGetJogType()
{
    return g_JogType;
}

void gStart_IOUpdate()
{
    gSet_IOUpdate(true, true);
}
void gStop_IOUpdate()
{
    gSet_IOUpdate(true, false);
}
bool g_bInputUpdate=false,g_bOutputUpdate=false;
void gSet_IOUpdate(bool input, bool output)
{
    g_bInputUpdate=input;
    g_bOutputUpdate=output;
}
bool gGet_InputUpdate() { return g_bInputUpdate; }
bool gGet_OutputUpdate() { return g_bOutputUpdate; }



float gGetWristAngle(cn_joint joint)
{
    // WristAngle = J2+J3+J4
    float wrist_angle = joint.joint[1] + joint.joint[2] + joint.joint[3];

    return wrist_angle;
}

float gGetWristAngle(float shoulder, float elbow, float wrist)
{
    float wrist_angle = shoulder + elbow + wrist;

    return wrist_angle;
}

#ifdef _H6_
bool g_bRotRealDisp; // false:origin J4, true:making rotation angle
bool g_bSwvRealDisp; // false:origin J5, true:making swivel angle

// gGetJoint2Disp & gGetDisp2Joint have relationship.
float gGetJoint2Disp(int joint_no, cn_joint joint)
{
    float value;

    switch (joint_no) {
    case JROT:
        //value = joint.joint[1]+joint.joint[2]+joint.joint[3]; // J6=0
        //value = joint.joint[JROT];
        if(joint.joint[J6] > -1.0 && joint.joint[J6] < 1.0)
        {
            value = joint.joint[J2]+joint.joint[J3]+joint.joint[J4];
            g_bRotRealDisp = true;
        }
        else
        {
            value = joint.joint[JROT];
            g_bRotRealDisp = false;
        }
        break;
    case JSWV:
        //value = joint.joint[1]+joint.joint[2]+joint.joint[4]; // J6=90 or -90
        //value = joint.joint[JSWV];
        if(joint.joint[J6] > 89.0 && joint.joint[J6] < 91.0)
        {
            if(joint.joint[J4] > 89.0 && joint.joint[J4] < 91.0)
            {
                // case 1

                value = joint.joint[J2] + joint.joint[J3] + 90.0 + joint.joint[J5];
                g_bSwvRealDisp = true;
                break;
            }
            else if(joint.joint[J4] > -91.0 && joint.joint[J4] < -89.0)
            {
                // case 3
                value = (joint.joint[J2] + joint.joint[J3] - 90.0 - joint.joint[J5]) * (-1.0);
                g_bSwvRealDisp = true;
                break;
            }
        }
        else if(joint.joint[J6] > -91.0 && joint.joint[J6] < -89.0)
        {
            if(joint.joint[J4] > 89.0 && joint.joint[J4] < 91.0)
            {
                // case 2
                value = (joint.joint[J2] + joint.joint[J3] + 90.0 - joint.joint[J5]) * (-1.0);
                g_bSwvRealDisp = true;
                break;
            }
            else if(joint.joint[J4] > -91.0 && joint.joint[J4] < -89.0)
            {
                // case 4
                value = joint.joint[J2] + joint.joint[J3] - 90.0 + joint.joint[J5];
                g_bSwvRealDisp = true;
                break;
            }
        }

        value = joint.joint[JSWV];
        g_bSwvRealDisp = false;
        break;
    case J6:
        value = joint.joint[J6];
        break;
    default:
        value = joint.joint[joint_no];
        break;
    }

    return value;
}
float gGetDisp2Joint(int joint_no, float input_value, cn_joint curr_joint)
{
    float value;
    int disp_value, diff_value;

    switch (joint_no) {
    case JROT:
        disp_value = gGetJoint2Disp(JROT, curr_joint);
        diff_value = input_value - disp_value;
        value = curr_joint.joint[JROT] + diff_value;

        value = input_value;
        break;
    case JSWV:
        disp_value = gGetJoint2Disp(JSWV, curr_joint);
        diff_value = input_value - disp_value;
        value = curr_joint.joint[JSWV] + diff_value;

        value = input_value;
        break;
    case J6:
        value = input_value;
        break;
    default:
        value = input_value;
        break;
    }

    qDebug() << "gGetDisp2Joint" << joint_no << value;
    return value;
}
cn_joint gMakeCnJoint(float *p_joint)
{
    cn_joint temp;
    for(int i=0;i<MAX_AXIS_NUM;i++)
        temp.joint[i] = p_joint[i];

    return temp;
}

bool gGetRotRealDisp()
{
    return g_bRotRealDisp;
}
bool gGetSwvRealDisp()
{
    return g_bSwvRealDisp;
}

#endif

void gSync_CommonPos()
{
    cn_trans t;
    cn_joint j;
    float speed,delay;

    if(Param->Get(HyParam::JIG_POS,t,j,speed,delay))
        Posi->WR(HyPos::JIG_CHANGE,t,j,speed,delay);

    if(Param->Get(HyParam::SAMPLE_POS,t,j,speed,delay))
        Posi->WR(HyPos::SAMPLE,t,j,speed,delay);

    if(Param->Get(HyParam::FAULTY_POS,t,j,speed,delay))
        Posi->WR(HyPos::FAULTY,t,j,speed,delay);

}

void gSync_CommonPos_nvs()
{
    cn_trans t;
    cn_joint j;
    float speed,delay;

    if(Param->Get(HyParam::JIG_POS,t,j,speed,delay))
        Posi->WR_nvs(HyPos::JIG_CHANGE,t,j,speed,delay);

    if(Param->Get(HyParam::SAMPLE_POS,t,j,speed,delay))
        Posi->WR_nvs(HyPos::SAMPLE,t,j,speed,delay);

    if(Param->Get(HyParam::FAULTY_POS,t,j,speed,delay))
        Posi->WR_nvs(HyPos::FAULTY,t,j,speed,delay);

}

void gBeep()
{
    if(UseSkip->Get(HyUseSkip::BTN_BEEP))
        Pendent->Beep();

}
void gBeep2()
{
    Pendent->Beep();
}

// chage font
void NanumGothic()
{
    QFont font("NanumGothic");
    QApplication::setFont(font);
    qDebug() << "Font -> NanumGothic";
}
void NanumMyeongjo()
{
    QFont font("NanumMyeongjo");
    QApplication::setFont(font);
    qDebug() << "Font -> NanumMyeongjo";
}
void KoPubWorldBatang()
{
    QFont font("KoPubWorldBatang");
    QApplication::setFont(font);
    qDebug() << "Font -> KoPubWorldBatang";
}

void KoPubWorldDotum()
{
    QFont font("KoPubWorldDotum");
    QApplication::setFont(font);
    qDebug() << "Font -> KoPubWorldDotum";
}
void Noto_Sans()
{
    QFont font("Noto Sans");
    QApplication::setFont(font);
    qDebug() << "Font -> Noto Sans";
}

void Noto_Sans_Thai()
{
    QFont font("Noto Sans Thai");
    QApplication::setFont(font);
    qDebug() << "Font -> Noto Sans Thai";
}

void SimHei()
{
    QFont font("SimHei");
    QApplication::setFont(font);
    qDebug() << "Font -> SimHei";
}

void gChange_Font(HyParam::PARAM_LANGUAGE lang_no)
{
    switch(lang_no)
    {
    case HyParam::LANG_ZH:
        SimHei();
        break;

    case HyParam::LANG_TH:
        Noto_Sans_Thai();
        break;

    default:
        NanumGothic();
        break;
    }
}
