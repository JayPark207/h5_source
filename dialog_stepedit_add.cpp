#include "dialog_stepedit_add.h"
#include "ui_dialog_stepedit_add.h"

dialog_stepedit_add::dialog_stepedit_add(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_stepedit_add)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);


    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(reject()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtPic.clear();
    m_vtPic.append(ui->btnPic);
    m_vtPic.append(ui->btnPic_2);
    m_vtPic.append(ui->btnPic_3);
    m_vtPic.append(ui->btnPic_4);
    m_vtPic.append(ui->btnPic_5);

    m_vtName.clear();
    m_vtName.append(ui->btnName);
    m_vtName.append(ui->btnName_2);
    m_vtName.append(ui->btnName_3);
    m_vtName.append(ui->btnName_4);
    m_vtName.append(ui->btnName_5);

    m_vtIcon.clear();
    m_vtIcon.append(ui->btnIcon);
    m_vtIcon.append(ui->btnIcon_2);
    m_vtIcon.append(ui->btnIcon_3);
    m_vtIcon.append(ui->btnIcon_4);
    m_vtIcon.append(ui->btnIcon_5);

    m_vtSel.clear();
    m_vtSel.append(ui->btnSel);
    m_vtSel.append(ui->btnSel_2);
    m_vtSel.append(ui->btnSel_3);
    m_vtSel.append(ui->btnSel_4);
    m_vtSel.append(ui->btnSel_5);

    int i;
    for(i=0;i<m_vtPic.count();i++)
    {
        connect(m_vtPic[i],SIGNAL(mouse_release()),this,SLOT(onAddItem()));
        connect(m_vtName[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtName[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
        connect(m_vtIcon[i],SIGNAL(mouse_press()),m_vtPic[i],SLOT(press()));
        connect(m_vtIcon[i],SIGNAL(mouse_release()),m_vtPic[i],SLOT(release()));
    }

    m_vtPosPic.clear();
    m_vtPosPic.append(ui->btnFrontPic);
    m_vtPosPic.append(ui->btnBackPic);

    m_vtPosIcon.clear();
    m_vtPosIcon.append(ui->btnFrontIcon);
    m_vtPosIcon.append(ui->btnBackIcon);

    m_vtPosLed.clear();
    m_vtPosLed.append(ui->lbFront);
    m_vtPosLed.append(ui->lbBack);

    for(i=0;i<m_vtPosPic.count();i++)
    {
        connect(m_vtPosPic[i],SIGNAL(mouse_release()),this,SLOT(onSelPos()));
        connect(m_vtPosIcon[i],SIGNAL(mouse_press()),m_vtPosPic[i],SLOT(press()));
        connect(m_vtPosIcon[i],SIGNAL(mouse_release()),m_vtPosPic[i],SLOT(release()));
    }

    connect(ui->btnAddNamePic,SIGNAL(mouse_release()),this,SLOT(onAddName()));
    connect(ui->btnAddName,SIGNAL(mouse_press()),ui->btnAddNamePic,SLOT(press()));
    connect(ui->btnAddName,SIGNAL(mouse_release()),ui->btnAddNamePic,SLOT(release()));
    connect(ui->btnAddNameIcon,SIGNAL(mouse_press()),ui->btnAddNamePic,SLOT(press()));
    connect(ui->btnAddNameIcon,SIGNAL(mouse_release()),ui->btnAddNamePic,SLOT(release()));

    // variable & pointer init.
    m_nCurrIndex = 0;

}

dialog_stepedit_add::~dialog_stepedit_add()
{
    delete ui;
}
void dialog_stepedit_add::showEvent(QShowEvent *)
{
    m_nSelectedItem = ADD_POS;
    m_bBackPos = false;

    Update();
}
void dialog_stepedit_add::hideEvent(QHideEvent *){}
void dialog_stepedit_add::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_stepedit_add::Init(int CurrIndex)
{
    m_nCurrIndex = CurrIndex;
    m_strNickName.clear();
}

void dialog_stepedit_add::Update()
{
    Select_AddItem(m_nSelectedItem);
    Select_SelPos(m_bBackPos);
    Display_Name(m_strNickName);
    ui->lbNowIndex->setText(QString().setNum(m_nCurrIndex + 1)); // 1~N
}

void dialog_stepedit_add::Select_AddItem(ADD_ITEM item)
{
    QImage img;
    img.load(":/icon/icon/icon992-0.png");
    QPixmap off = QPixmap::fromImage(img);
    img.load(":/icon/icon/icon992-4.png");
    QPixmap on = QPixmap::fromImage(img);

    for(int i=0;i<m_vtSel.count();i++)
        m_vtSel[i]->setPixmap(off);

    m_vtSel[item]->setPixmap(on);

    ui->wigName->setEnabled((item != ADD_WORK));
}

void dialog_stepedit_add::Select_SelPos(bool bBackPos)
{
    for(int i=0;i<m_vtPosLed.count();i++)
        m_vtPosLed[i]->setAutoFillBackground(i==(int)bBackPos);
}

void dialog_stepedit_add::Display_Name(QString name)
{
    ui->lbDispName->setText(name);
}

dialog_stepedit_add::ADD_ITEM dialog_stepedit_add::GetAddItem()
{
    return m_nSelectedItem;
}
bool dialog_stepedit_add::IsBackPos()
{
    return m_bBackPos;
}
QString dialog_stepedit_add::GetNickName()
{
    return m_strNickName;
}

void dialog_stepedit_add::onAddItem()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtPic.indexOf(btn);
    if(index < 0) return;

    m_nSelectedItem = (ADD_ITEM)index;
    Update();

}

void dialog_stepedit_add::onSelPos()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtPosPic.indexOf(btn);
    if(index < 0) return;

    m_bBackPos = (bool)index;
    Update();
}

void dialog_stepedit_add::onAddName()
{
    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->btnAddName->text());
    kb->m_kb->SetText(m_strNickName);
    if(kb->exec() == QDialog::Accepted)
    {
        m_strNickName = kb->m_kb->GetText();
        Update();
    }
}


