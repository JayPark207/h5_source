#ifndef HYLOG_H
#define HYLOG_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"


/** Log code & message management.
    Code is plus number. minus are managed HyError class.
    Use cnrobo.h -> setUserLog(int code, QString msg) **/

#define HY_INFO_LOG 9999
#define HY_SETVAR_LOG 9900

class HyLog : public QObject
{
    Q_OBJECT

public:
    HyLog();

    void        Init();

    void        Set(int code);
    void        Set(int code, QString message); //directly write log.

    bool        IsExist(int code);
    QString     Get(int code);

private:
    void        RegLog(int code, QString message);

    // both stringlist is always matching.
    QStringList m_slCode;
    QStringList m_slMsg;

    int         GetIndex(int code);

};

#endif // HYLOG_H
