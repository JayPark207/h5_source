#include "dialog_view_dmcvar.h"
#include "ui_dialog_view_dmcvar.h"

dialog_view_dmcvar::dialog_view_dmcvar(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_view_dmcvar)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnClearPic,SIGNAL(mouse_release()),this,SLOT(onDelete()));
    connect(ui->btnClear,SIGNAL(mouse_press()),ui->btnClearPic,SLOT(press()));
    connect(ui->btnClear,SIGNAL(mouse_release()),ui->btnClearPic,SLOT(release()));

    connect(ui->btnZeroPic,SIGNAL(mouse_release()),this,SLOT(onZero()));
    connect(ui->btnZero,SIGNAL(mouse_press()),ui->btnZeroPic,SLOT(press()));
    connect(ui->btnZero,SIGNAL(mouse_release()),ui->btnZeroPic,SLOT(release()));

    connect(ui->btnSavePic,SIGNAL(mouse_release()),this,SLOT(onSave()));
    connect(ui->btnSave,SIGNAL(mouse_press()),ui->btnSavePic,SLOT(press()));
    connect(ui->btnSave,SIGNAL(mouse_release()),ui->btnSavePic,SLOT(release()));

#ifdef _CG3300_
    m_nNameWidth = 130;
    m_nColWidth = 90;
#elif _CG4300_
    m_nNameWidth = 130;
    m_nColWidth = 90;
#endif

    int i;
    QString temp;

    //
    m_twTable.clear();
    m_twTable.append(ui->tblTrans);
    m_twTable.append(ui->tblJoint);
    m_twTable.append(ui->tblNum);
    m_twTable.append(ui->tblStr);


    // trans table
    m_TransHeader.clear();
    m_TransHeader.append(tr("Name"));
    m_TransHeader.append("X");
    m_TransHeader.append("Y");
    m_TransHeader.append("Z");
    m_TransHeader.append("A");
    m_TransHeader.append("B");
    m_TransHeader.append("C");

    ui->tblTrans->setColumnCount(m_TransHeader.count());
    ui->tblTrans->setHorizontalHeaderLabels(m_TransHeader);
    ui->tblTrans->setSortingEnabled(false);

    ui->tblTrans->setColumnWidth(0, m_nNameWidth);
    for(i=1;i<m_TransHeader.count();i++)
    {
        ui->tblTrans->setColumnWidth(i, m_nColWidth);
    }

    // joint table
    m_JointHeader.clear();
    m_JointHeader.append(tr("Name"));

    for(i=0;i<MAX_AXIS_NUM;i++)
    {
        temp = QString("J%1").arg(i+1);
        m_JointHeader.append(temp);
    }

    ui->tblJoint->setColumnCount(m_JointHeader.count());
    ui->tblJoint->setHorizontalHeaderLabels(m_JointHeader);
    ui->tblJoint->setSortingEnabled(false);

    ui->tblJoint->setColumnWidth(0, m_nNameWidth);
    for(i=1;i<m_JointHeader.count();i++)
    {
        ui->tblJoint->setColumnWidth(i, m_nColWidth);
    }

    // num table
    m_NumHeader.clear();
    m_NumHeader.append(tr("Name"));
    m_NumHeader.append("Value");

    ui->tblNum->setColumnCount(m_NumHeader.count());
    ui->tblNum->setHorizontalHeaderLabels(m_NumHeader);
    ui->tblNum->setSortingEnabled(false);

    ui->tblNum->setColumnWidth(0, m_nNameWidth * 3);
    ui->tblNum->setColumnWidth(1, m_nColWidth);
    ui->tblNum->horizontalHeader()->setStretchLastSection(true);

    // string table
    m_StrHeader.clear();
    m_StrHeader.append(tr("Name"));
    m_StrHeader.append("String");

    ui->tblStr->setColumnCount(m_StrHeader.count());
    ui->tblStr->setHorizontalHeaderLabels(m_StrHeader);
    ui->tblStr->setSortingEnabled(false);

    ui->tblStr->setColumnWidth(0, m_nNameWidth);
    ui->tblStr->setColumnWidth(1, m_nColWidth);
    ui->tblStr->horizontalHeader()->setStretchLastSection(true);

    ui->tblJoint->setAlternatingRowColors(true);
    ui->tblTrans->setAlternatingRowColors(true);
    ui->tblNum->setAlternatingRowColors(true);
    ui->tblStr->setAlternatingRowColors(true);

    // zero not use.
    ui->btnZero->setEnabled(false);
    ui->btnZeroPic->setEnabled(false);
}

dialog_view_dmcvar::~dialog_view_dmcvar()
{
    delete ui;
}

void dialog_view_dmcvar::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_view_dmcvar::showEvent(QShowEvent *)
{
    Update_Trans();
    Update_Joint();
    Update_Num();
    Update_Str();
}

void dialog_view_dmcvar::hideEvent(QHideEvent *){}

void dialog_view_dmcvar::onClose()
{
    emit accept();
}

void dialog_view_dmcvar::Update_Trans()
{
    QTableWidget* tbl = ui->tblTrans;
    tbl->clearContents();
    CNRobo* pCon = CNRobo::getInstance();

    QStringList names;
    QVector<cn_variant> datas;

    int ret;
    ret = pCon->getVariableList(CN_ROBOT_VARIABLE_TRANS, names, datas);
    if(ret < 0)
    {
        qDebug() << "Update_Trans() getVariableList ret =" << ret;
        return;
    }

    tbl->setRowCount(names.count());

    QString str;

    for(int i=0;i<names.count();i++)
    {
        for(int j=0;j<m_TransHeader.count();j++)
        {
            QTableWidgetItem* cell = tbl->item(i,j);
            if(!cell)
            {
                cell = new QTableWidgetItem("");
                tbl->setItem(i, j, cell);
            }
        }

        tbl->item(i,0)->setText(names[i]);

        str.sprintf(FORMAT_POS, datas[i].val.trans.p[0]);
        tbl->item(i,1)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.trans.p[1]);
        tbl->item(i,2)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.trans.p[2]);
        tbl->item(i,3)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.trans.eu[0]);
        tbl->item(i,4)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.trans.eu[1]);
        tbl->item(i,5)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.trans.eu[2]);
        tbl->item(i,6)->setText(str);

    }

}

void dialog_view_dmcvar::Update_Joint()
{
    QTableWidget* tbl = ui->tblJoint;
    tbl->clearContents();
    CNRobo* pCon = CNRobo::getInstance();

    QStringList names;
    QVector<cn_variant> datas;

    int ret;
    ret = pCon->getVariableList(CN_ROBOT_VARIABLE_JOINT, names, datas);
    if(ret < 0)
    {
        qDebug() << "Update_Joint() getVariableList ret =" << ret;
        return;
    }

    tbl->setRowCount(names.count());

    QString str;

    for(int i=0;i<names.count();i++)
    {
        for(int j=0;j<m_JointHeader.count();j++)
        {
            QTableWidgetItem* cell = tbl->item(i,j);
            if(!cell)
            {
                cell = new QTableWidgetItem("");
                tbl->setItem(i, j, cell);
            }
        }

        tbl->item(i,0)->setText(names[i]);

        str.sprintf(FORMAT_POS, datas[i].val.joint.joint[0]);
        tbl->item(i,1)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.joint.joint[1]);
        tbl->item(i,2)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.joint.joint[2]);
        tbl->item(i,3)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.joint.joint[3]);
        tbl->item(i,4)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.joint.joint[4]);
        tbl->item(i,5)->setText(str);
        str.sprintf(FORMAT_POS, datas[i].val.joint.joint[5]);
        tbl->item(i,6)->setText(str);

    }
}

void dialog_view_dmcvar::Update_Num()
{
    QTableWidget* tbl = ui->tblNum;
    tbl->clearContents();
    CNRobo* pCon = CNRobo::getInstance();

    QStringList names;
    QVector<cn_variant> datas;

    int ret;
    ret = pCon->getVariableList(CN_ROBOT_VARIABLE_NUMBER, names, datas);
    if(ret < 0)
    {
        qDebug() << "Update_Num() getVariableList ret =" << ret;
        return;
    }

    tbl->setRowCount(names.count());

    QString str;

    for(int i=0;i<names.count();i++)
    {
        for(int j=0;j<m_NumHeader.count();j++)
        {
            QTableWidgetItem* cell = tbl->item(i,j);
            if(!cell)
            {
                cell = new QTableWidgetItem("");
                tbl->setItem(i, j, cell);
            }
        }

        tbl->item(i,0)->setText(names[i]);

        str.sprintf(FORMAT_POS, datas[i].val.f32);
        tbl->item(i,1)->setText(str);
    }
}

void dialog_view_dmcvar::Update_Str()
{
    QTableWidget* tbl = ui->tblStr;
    tbl->clearContents();
    CNRobo* pCon = CNRobo::getInstance();

    QStringList names;
    QVector<cn_variant> datas;

    int ret;
    ret = pCon->getVariableList(CN_ROBOT_VARIABLE_STRING, names, datas);
    if(ret < 0)
    {
        qDebug() << "Update_Str() getVariableList ret =" << ret;
        return;
    }

    tbl->setRowCount(names.count());

    QString str;

    for(int i=0;i<names.count();i++)
    {
        for(int j=0;j<m_StrHeader.count();j++)
        {
            QTableWidgetItem* cell = tbl->item(i,j);
            if(!cell)
            {
                cell = new QTableWidgetItem("");
                tbl->setItem(i, j, cell);
            }
        }

        tbl->item(i,0)->setText(names[i]);

        str.sprintf("%s", datas[i].val.str);
        tbl->item(i,1)->setText(str);
    }
}

void dialog_view_dmcvar::onDelete()
{
    int tab_index = ui->tabWidget->currentIndex();
    QTableWidget* tb = m_twTable[tab_index];

    int row = tb->currentRow();
    if(row < 0) return;

    QString var_name = tb->item(row, 0)->text();

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::RED);
    conf->Title(QString(tr("Delete")));
    conf->Message(QString(tr("Would you want to delete data?")),
                  var_name);

    if(conf->exec() != QDialog::Accepted)
        return;

    // make type
    int type = CN_ROBOT_VARIABLE_NUMBER;
    switch (tab_index) {
    case 0:
        type = CN_ROBOT_VARIABLE_TRANS;
        break;
    case 1:
        type = CN_ROBOT_VARIABLE_JOINT;
        break;
    case 2:
        type = CN_ROBOT_VARIABLE_NUMBER;
        break;
    case 3:
        type = CN_ROBOT_VARIABLE_STRING;
        break;
    }

    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->deleteVariable(type, var_name);
    if(ret < 0)
    {
        qDebug() << "deleteVariable ret=" << ret;
        return;
    }

    switch (tab_index)
    {
    case 0:
        Update_Trans();
        break;
    case 1:
        Update_Joint();
        break;
    case 2:
        Update_Num();
        break;
    case 3:
        Update_Str();
        break;
    }

}

void dialog_view_dmcvar::onZero()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::RED);
    conf->Title(QString(tr("Clear")));
    conf->Message(QString(tr("Would you want to make zero?")));

    if(conf->exec() == QDialog::Accepted)
    {
        Recipe->AllZero();
        Posi->AllZero();
        Time->AllZero();
        UserData->AllZero();
    }
}

void dialog_view_dmcvar::onSave()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(QString(tr("Save")));
    conf->Message(QString(tr("Would you want to Save?")));

    if(conf->exec() != QDialog::Accepted)
        return;

    CNRobo* pCon = CNRobo::getInstance();
    pCon->saveVariable();
}
