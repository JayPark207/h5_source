#include "dialog_version.h"
#include "ui_dialog_version.h"

dialog_version::dialog_version(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_version)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(accept()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    timer = new QTimer(this);
    timer->setInterval(50);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    m_nLogoX = ui->lbLogo->x();
    m_nLogoY = ui->lbLogo->x();
    m_nLogoW = ui->lbLogo->width();
    m_nLogoH = ui->lbLogo->height();
    ui->lbLogo->hide();

    connect(ui->lbLogo,SIGNAL(mouse_release()),this,SLOT(onLogoMove()));

    //for test.
    m_vtTest.clear();
    m_vtTest.append(ui->lb);
    m_vtTest.append(ui->lb_2);
    m_vtTest.append(ui->lb_3);
    m_vtTest.append(ui->lb_4);

}

dialog_version::~dialog_version()
{
    delete ui;
}
void dialog_version::showEvent(QShowEvent *)
{
    StartLogoMoving();
    Update();
}
void dialog_version::hideEvent(QHideEvent *)
{
}
void dialog_version::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_version::onTimer()
{
    m_fPer += 0.05;

    float w = (float)m_nLogoW * m_fPer;
    float h = (float)m_nLogoH * m_fPer;

    ui->lbLogo->setGeometry(m_nLogoX,m_nLogoY, (int)w, (int)h);

    if(ui->lbLogo->isHidden())  // first start is from small
        ui->lbLogo->show();

    if(m_fPer >= 1.0)
    {
        timer->stop();
        ui->lbLogo->setGeometry(m_nLogoX,m_nLogoY,m_nLogoW,m_nLogoH);
        ui->lbLogo->show();
    }
}

void dialog_version::StartLogoMoving()
{
    m_fPer = 0.0;
    timer->start();
}

void dialog_version::Update()
{
    CNRobo* pCon = CNRobo::getInstance();

    unsigned int client_ver, server_ver;
    client_ver = pCon->getVersion();
    pCon->getServerVersion(&server_ver);

    QString str;
    str = GUI_VER;
    str += MODEL;
    ui->lbVal->setText(str);

    float seq_ver = 0;
    if(Recipe->Get(HyRecipe::SeqVer, &seq_ver))
        str.sprintf("%.02f", seq_ver);
    else
        str = "X.XX";
    ui->lbVal_2->setText(str);

    str.sprintf("%X", server_ver);
    ui->lbVal_3->setText(str);
    str.sprintf("%X", client_ver);
    ui->lbVal_4->setText(str);

    pCon->getVersion2(CNRobo::VT_MASTER, str);
    ui->lbVal_5->setText(str);

    pCon->getVersion2(CNRobo::VT_KERNEL, str);
    ui->lbVal_6->setText(str);

    QString str2;
    Network->Get_IP(str, str2);

    ui->lbVal_7->setText(str);

    ui->lbVal_8->setText(Param->Get(HyParam::START_DATE));

    /*

    ui->lbCol1_2->setText(GUI_VER);
    ui->lbCol1_3->setText(SEQ_VER);
    QString str;
    str.sprintf("%X", server_ver);
    ui->lbCol1_4->setText(str);
    str.sprintf("%X", client_ver);
    ui->lbCol1_5->setText(str);

    ui->lbCol1_6->setText(Param->Get(HyParam::START_DATE));



    for(int i=0;i<4;i++)
    {
        str.clear();
        pCon->getVersion2((CNRobo::VERSION_TYPE)i, str);
        m_vtTest[i]->setText(str);
    }*/

}

void dialog_version::onLogoMove()
{
    if(!timer->isActive())
        StartLogoMoving();
}
