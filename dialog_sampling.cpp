#include "dialog_sampling.h"
#include "ui_dialog_sampling.h"

dialog_sampling::dialog_sampling(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_sampling)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnUsePic,SIGNAL(mouse_release()),this,SLOT(onUse()));
    connect(ui->btnUse,SIGNAL(mouse_press()),ui->btnUsePic,SLOT(press()));
    connect(ui->btnUse,SIGNAL(mouse_release()),ui->btnUsePic,SLOT(release()));

    connect(ui->btnSQtyPic,SIGNAL(mouse_release()),this,SLOT(onSQty()));
    connect(ui->btnSQty,SIGNAL(mouse_press()),ui->btnSQtyPic,SLOT(press()));
    connect(ui->btnSQty,SIGNAL(mouse_release()),ui->btnSQtyPic,SLOT(release()));

    connect(ui->btnSCycPic,SIGNAL(mouse_release()),this,SLOT(onSCyc()));
    connect(ui->btnSCyc,SIGNAL(mouse_press()),ui->btnSCycPic,SLOT(press()));
    connect(ui->btnSCyc,SIGNAL(mouse_release()),ui->btnSCycPic,SLOT(release()));

    connect(ui->btnSCntPic,SIGNAL(mouse_release()),this,SLOT(onSCnt()));
    connect(ui->btnSCnt,SIGNAL(mouse_press()),ui->btnSCntPic,SLOT(press()));
    connect(ui->btnSCnt,SIGNAL(mouse_release()),ui->btnSCntPic,SLOT(release()));



}

dialog_sampling::~dialog_sampling()
{
    delete ui;
}

void dialog_sampling::showEvent(QShowEvent *)
{
    Update();
    timer->start();
}
void dialog_sampling::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_sampling::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_sampling::onClose()
{
    emit accept();
}
void dialog_sampling::onTimer()
{
    Update();
}

void dialog_sampling::Update()
{
    m_ItemName.clear();
    m_ItemName.append(tr("No Use"));
    m_ItemName.append(tr("Use"));

    vars.clear();
    vars.append(HyRecipe::bSampleUse);
    vars.append(HyRecipe::vSample);
    vars.append(HyRecipe::vSampleCycle);
    vars.append(HyRecipe::vSampleCnt);

    if(Recipe->Gets(vars, datas))
    {
        if((int)datas[0] < m_ItemName.size())
            ui->btnUse->setText(m_ItemName[(int)datas[0]]);

        QString str;
        str.sprintf("%.0f", datas[1]);
        ui->btnSQty->setText(str);

        str.sprintf("%.0f", datas[2]);
        ui->btnSCyc->setText(str);

        str.sprintf("%.0f", datas[3]);
        ui->btnSCnt->setText(str);

        ui->wigSQty->setEnabled((int)datas[0] > 0);
        ui->wigSCyc->setEnabled((int)datas[0] > 0);
        ui->wigSCnt->setEnabled((int)datas[0] > 0);
    }
}

bool dialog_sampling::IsUse()
{
    return ((int)datas[0] > 0);
}

void dialog_sampling::onUse()
{
    /*dialog_mode_select* sel = (dialog_mode_select*)gGetDialog(DIG_MODE_SELECT);
    sel->InitTitle(ui->lbTitle->text());
    sel->InitRecipe((int)vars[0]);
    sel->InitNum(m_ItemName.size());
    for(int i=0;i<sel->GetNum();i++)
        sel->InitString(i,m_ItemName[i]);

    if(sel->exec() == QDialog::Accepted)
        Update();*/


    // case2
    float _use;
    if(IsUse())
        _use = 0.0;
    else
        _use = 1.0;

    if(Recipe->Set(vars[0],_use))
        Update();
}

void dialog_sampling::onSQty()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GRAY);
    conf->Title(ui->lbSQty->text());
    conf->Message(tr("Do you want to clear?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    if(Recipe->Set(vars[1],0.0))
        Update();

}
void dialog_sampling::onSCyc()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->lbSCyc->text());
    np->m_numpad->SetNum(ui->btnSCyc->text().toDouble());
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(999999);
    np->m_numpad->SetSosuNum(0);

    if(np->exec() != QDialog::Accepted)
        return;

    if(Recipe->Set(vars[2],(float)np->m_numpad->GetNumDouble()))
        Update();

}
void dialog_sampling::onSCnt()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetTitle(ui->lbSCnt->text());
    np->m_numpad->SetNum(ui->btnSCnt->text().toDouble());
    np->m_numpad->SetMinValue(1.0);
    np->m_numpad->SetMaxValue(datas[2]);
    np->m_numpad->SetSosuNum(0);

    if(np->exec() != QDialog::Accepted)
        return;

    if(Recipe->Set(vars[3],(float)np->m_numpad->GetNumDouble()))
        Update();
}
