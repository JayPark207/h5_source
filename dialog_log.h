#ifndef DIALOG_LOG_H
#define DIALOG_LOG_H

#include <QDialog>
#include <QTimer>
#include <QElapsedTimer>
#include <QTextCharFormat>

#include "global.h"
#include "qlabel3.h"

#include "hyui_longpress.h"
#include "dialog_message.h"
#include "dialog_confirm.h"


namespace Ui {
class dialog_log;
}

class dialog_log : public QDialog
{
    Q_OBJECT

public:
    struct ST_LOG_DATA
    {
        QDateTime DateTime;
        QString Logger;
        QString Code;
        QString Msg;
    };

public:
    explicit dialog_log(QWidget *parent = 0);
    ~dialog_log();

    void Update();

private slots:
    void onClose();
    void onNext();
    void onBack();
    void on_calendar_currentPageChanged(int year, int month);
    void on_calendar_selectionChanged();
    void onFrom();
    void onTo();
    void onView();
    void onSelect();

    void onTimer();

    // list page
    void onReturn();
    void onExt();
    void onListUp();
    void onListDown();
    void onListUpPage();
    void onListDownPage();
    void onPress_UpDown();
    void onRelease_UpDown();

    void onCheckbox();

private:
    Ui::dialog_log *ui;

    QTimer* timer;

    QDate m_dateFrom, m_dateTo;
    QDate m_dateFrom_Painted, m_dateTo_Painted;
    void Select_FromTo(int nfromto);
    void SetDateFrom(QDate date);
    void SetDateTo(QDate date);
    void HideNextBtn(bool onoff);
    void HideBackBtn(bool onoff);
    void SetMode(int mode); // 0=day, 1=range

    void test();
    void PaintCalendarDate(QDate from, QDate to, bool bPaint = true);
    void DisableCalendarDate();

    /** list parts **/
    void Update_List();
    void ShowList(bool onoff);

    // important data.
    QStringList m_slRawLogList;
    QList<ST_LOG_DATA> m_RawLogData;
    QList<ST_LOG_DATA> m_LogData;
    QList<ST_LOG_DATA> m_ErrLogData;
    QList<ST_LOG_DATA> m_AllLogData;

    /** old api **/
    void LogRead(QStringList& list);
    void LogParsing(QStringList list, QDate from, QDate to);
    /** using new api getSelectedlogList() **/
    void LogRead(QStringList &list, QDate from, QDate to);
    void LogParsing(QStringList list);

    QVector<QLabel3*> m_vtNo;
    QVector<QLabel3*> m_vtDate;
    QVector<QLabel3*> m_vtIcon;
    QVector<QLabel3*> m_vtCode;
    QVector<QLabel3*> m_vtMsg;

    int m_nStartIndex;
    void Redraw_LogList(int start_index);   // m_LogData list up.

    QVector<QPixmap> m_vtLogIcon;
    void Init_LogIcon();
    QPixmap GetLogIcon(int list_index);

    HyUi_LongPress* uiLongPressUp;
    HyUi_LongPress* uiLongPressDown;

    void Change_OnlyErrorLog(bool on);

    void SetCheckbox(bool on);

    bool m_bDateSetError;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_LOG_H
