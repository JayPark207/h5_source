#ifndef HYINPUT
#define HYINPUT

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDebug>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#define MAX_INPUT_NUM       128
#define USE_INPUT_NUM       48
#define USE_INPUT_BUFNUM    2

/** input list **/
class HyInput : public QObject
{
    Q_OBJECT
public:
    enum ENUM_INPUT_NUM
    {
        VAC1        = 1,
        VAC2        = 3,
        VAC3        = 5,
        VAC4        = 7,
        CHUCK       = 9,
        GRIP        = 11,
        NIPPER      = 12,
        RGRIP       = 13,
        USER1       = 25,
        USER2       = 26,
        USER3       = 27,
        USER4       = 28,
        USER5       = 29,
        USER6       = 30,
        USER7       = 31,
        USER8       = 32,

        EMO         = 16,
        EMO_IMM1    = 33,
        EMO_IMM2    = 34,

        INPUT_OFFSET = 1000,
    };

public:
    HyInput()
    {
        Sym.clear();
        Sym.fill("X000",MAX_INPUT_NUM);
        Name.clear();
        Name.fill("Input",MAX_INPUT_NUM);
        On.clear();
        On.fill(false,MAX_INPUT_NUM);
        UiName.clear();
        UiName.fill("", MAX_INPUT_NUM);

        Init();
    }

    void Init()
    {
        // default sym,name
        int i;
        QString str;
        for(i=0;i<USE_INPUT_NUM;i++)
        {
            str.sprintf("X%03d", i);
            Sym[i] = str;
            str.sprintf("Input X%03d", i);
            Name[i] = str;
        }

        Sym[0]  = QString("X001");Name[0]  = QString(tr("Vac.1 Check"));
        Sym[1]  = QString("X002");Name[1]  = QString(tr(""));
        Sym[2]  = QString("X003");Name[2]  = QString(tr("Vac.2 Check"));
        Sym[3]  = QString("X004");Name[3]  = QString(tr(""));
        Sym[4]  = QString("X005");Name[4]  = QString(tr("Vac.3 Check"));
        Sym[5]  = QString("X006");Name[5]  = QString(tr(""));
        Sym[6]  = QString("X007");Name[6]  = QString(tr("Vac.4 Check"));
        Sym[7]  = QString("X008");Name[7]  = QString(tr(""));
        Sym[8]  = QString("X009");Name[8]  = QString(tr("Chuck Check"));
        Sym[9]  = QString("X010");Name[9]  = QString(tr(""));
        Sym[10] = QString("X011");Name[10] = QString(tr("Gripper Check"));
        Sym[11] = QString("X012");Name[11] = QString(tr("Nipper Check"));
        Sym[12] = QString("X013");Name[12] = QString(tr(""));   // old: R-Grip Check
        Sym[13] = QString("X014");Name[13] = QString(tr(""));
        Sym[14] = QString("X015");Name[14] = QString(tr(""));
        Sym[15] = QString("X016");Name[15] = QString(tr("Emergency"));

        Sym[16] = QString("X017");Name[16] = QString(tr(""));
        Sym[17] = QString("X018");Name[17] = QString(tr(""));
        Sym[18] = QString("X019");Name[18] = QString(tr(""));
        Sym[19] = QString("X020");Name[19] = QString(tr(""));
        Sym[20] = QString("X021");Name[20] = QString(tr(""));
        Sym[21] = QString("X022");Name[21] = QString(tr(""));
        Sym[22] = QString("X023");Name[22] = QString(tr(""));
        Sym[23] = QString("X024");Name[23] = QString(tr(""));
        Sym[24] = QString("X025");Name[24] = QString(tr("User Input 1"));
        Sym[25] = QString("X026");Name[25] = QString(tr("User Input 2"));
        Sym[26] = QString("X027");Name[26] = QString(tr("User Input 3"));
        Sym[27] = QString("X028");Name[27] = QString(tr("User Input 4"));
        Sym[28] = QString("X029");Name[28] = QString(tr("User Input 5"));
        Sym[29] = QString("X030");Name[29] = QString(tr("User Input 6"));
        Sym[30] = QString("X031");Name[30] = QString(tr("User Input 7"));
        Sym[31] = QString("X032");Name[31] = QString(tr("User Input 8"));

        Sym[32] = QString("X033");Name[32] = QString(tr("Emergency IMM 1"));
        Sym[33] = QString("X034");Name[33] = QString(tr("Emergency IMM 2"));
        Sym[34] = QString("X035");Name[34] = QString(tr("Safety Device IMM 1"));
        Sym[35] = QString("X036");Name[35] = QString(tr("Safety Device IMM 2"));
        Sym[36] = QString("X037");Name[36] = QString(tr("Enable Operation"));
        Sym[37] = QString("X038");Name[37] = QString(tr("Mold Closed"));
        Sym[38] = QString("X039");Name[38] = QString(tr("Mold Opened"));
        Sym[39] = QString("X040");Name[39] = QString(tr("Ejector Bwd. Pos."));
        Sym[40] = QString("X041");Name[40] = QString(tr("Ejector Fwd. Pos."));
        Sym[41] = QString("X042");Name[41] = QString(tr("Core 1 in pos.1"));
        Sym[42] = QString("X043");Name[42] = QString(tr("Core 1 in pos.2"));
        Sym[43] = QString("X044");Name[43] = QString(tr("Core 2 in pos.1"));
        Sym[44] = QString("X045");Name[44] = QString(tr("Core 2 in pos.2"));
        Sym[45] = QString("X046");Name[45] = QString(tr("Mold Opening"));
        Sym[46] = QString("X047");Name[46] = QString(tr("Reject"));
        Sym[47] = QString("X048");Name[47] = QString(tr(""));

        // add here ...

        // UI Name (= matcing ENUM_OUTPUT_NUM in USE_OUTPUT_NUM)
        UiName[VAC1-1] = tr("Vac.1");
        UiName[VAC2-1] = tr("Vac.2");
        UiName[VAC3-1] = tr("Vac.3");
        UiName[VAC4-1] = tr("Vac.4");
        UiName[CHUCK-1] = tr("Chuck");
        UiName[GRIP-1] = tr("Grip");
        UiName[NIPPER-1] = tr("Nipper");
        UiName[RGRIP-1] = tr("R Grip");
        UiName[USER1-1] = tr("User 1");
        UiName[USER2-1] = tr("User 2");
        UiName[USER3-1] = tr("User 3");
        UiName[USER4-1] = tr("User 4");
        UiName[USER5-1] = tr("User 5");
        UiName[USER6-1] = tr("User 6");
        UiName[USER7-1] = tr("User 7");
        UiName[USER8-1] = tr("User 8");

    }

    QVector<QString> Sym;
    QVector<QString> Name;
    QVector<bool>    On;
    QVector<QString> UiName;

    QString GetSym(int i) { return Sym.at(i); }
    QString GetName(int i) { return Name.at(i); }
    void SetOn(int i, bool on) { On[i] = on; }
    bool IsOn(int i) { return On.at(i); }

    QString GetUiName(int i) { return UiName[i]; }
};



#endif // HYINPUT

