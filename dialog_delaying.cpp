#include "dialog_delaying.h"
#include "ui_dialog_delaying.h"

dialog_delaying::dialog_delaying(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_delaying)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(10);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    m_nDelayMsec = 100; // default
}

dialog_delaying::~dialog_delaying()
{
    delete ui;
}
void dialog_delaying::showEvent(QShowEvent *)
{
    ui->pbIng->setValue(0);
    watch.start();
    timer->start();
}
void dialog_delaying::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_delaying::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_delaying::Init(int delay_msec)
{
    m_nDelayMsec = delay_msec;
}

void dialog_delaying::onTimer()
{

    int value = (int)(((float)watch.elapsed()/(float)m_nDelayMsec) * 100.0);

    ui->pbIng->setValue(value);

    if(watch.hasExpired((qint64)m_nDelayMsec))
    {
        accept();
    }
}
