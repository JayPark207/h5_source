#include "dialog_recipe.h"
#include "ui_dialog_recipe.h"

dialog_recipe::dialog_recipe(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_recipe)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    m_vtNo.clear();m_vtName.clear();
    m_vtNo.append(ui->lbNo);  m_vtName.append(ui->lbName);
    m_vtNo.append(ui->lbNo_2);m_vtName.append(ui->lbName_2);
    m_vtNo.append(ui->lbNo_3);m_vtName.append(ui->lbName_3);
    m_vtNo.append(ui->lbNo_4);m_vtName.append(ui->lbName_4);
    m_vtNo.append(ui->lbNo_5);m_vtName.append(ui->lbName_5);
    m_vtNo.append(ui->lbNo_6);m_vtName.append(ui->lbName_6);

    m_vtDate.clear();m_vtMark.clear();
    m_vtDate.append(ui->lbDate);  m_vtMark.append(ui->lbMark);
    m_vtDate.append(ui->lbDate_2);m_vtMark.append(ui->lbMark_2);
    m_vtDate.append(ui->lbDate_3);m_vtMark.append(ui->lbMark_3);
    m_vtDate.append(ui->lbDate_4);m_vtMark.append(ui->lbMark_4);
    m_vtDate.append(ui->lbDate_5);m_vtMark.append(ui->lbMark_5);
    m_vtDate.append(ui->lbDate_6);m_vtMark.append(ui->lbMark_6);

    int i;
    for(i=0;i<m_vtNo.count();i++)
    {
        connect(m_vtNo[i],SIGNAL(mouse_release()),this,SLOT(onSelect()));
        connect(m_vtName[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
        connect(m_vtDate[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
        connect(m_vtMark[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpPic,SIGNAL(mouse_press()),this,SLOT(onPress_UpDown()));
    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onRelease_UpDown()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),this,SLOT(onPress_UpDown()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),this,SLOT(onRelease_UpDown()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownPic,SIGNAL(mouse_press()),this,SLOT(onPress_UpDown()));
    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onRelease_UpDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),this,SLOT(onPress_UpDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),this,SLOT(onRelease_UpDown()));

    /** for long press operation. **/
    uiLongPressUp = new HyUi_LongPress();
    uiLongPressUp->Init(1000,200);
    connect(ui->btnUpPic,SIGNAL(mouse_press()),uiLongPressUp,SLOT(onPressed()));
    connect(ui->btnUpPic,SIGNAL(mouse_release()),uiLongPressUp,SLOT(onReleased()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),uiLongPressUp,SLOT(onPressed()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),uiLongPressUp,SLOT(onReleased()));
    connect(uiLongPressUp,SIGNAL(sigTrigger()),this,SLOT(onUpLong()));
    uiLongPressDown = new HyUi_LongPress();
    uiLongPressDown->Init(1000,200);
    connect(ui->btnDownPic,SIGNAL(mouse_press()),uiLongPressDown,SLOT(onPressed()));
    connect(ui->btnDownPic,SIGNAL(mouse_release()),uiLongPressDown,SLOT(onReleased()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),uiLongPressDown,SLOT(onPressed()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),uiLongPressDown,SLOT(onReleased()));
    connect(uiLongPressDown,SIGNAL(sigTrigger()),this,SLOT(onDownLong()));


    connect(ui->btnLoadPic,SIGNAL(mouse_release()),this,SLOT(onLoad()));
    connect(ui->btnLoad,SIGNAL(mouse_press()),ui->btnLoadPic,SLOT(press()));
    connect(ui->btnLoad,SIGNAL(mouse_release()),ui->btnLoadPic,SLOT(release()));
    connect(ui->btnLoadIcon,SIGNAL(mouse_press()),ui->btnLoadPic,SLOT(press()));
    connect(ui->btnLoadIcon,SIGNAL(mouse_release()),ui->btnLoadPic,SLOT(release()));

    connect(ui->btnNewPic,SIGNAL(mouse_release()),this,SLOT(onNew()));
    connect(ui->btnNew,SIGNAL(mouse_press()),ui->btnNewPic,SLOT(press()));
    connect(ui->btnNew,SIGNAL(mouse_release()),ui->btnNewPic,SLOT(release()));
    connect(ui->btnNewIcon,SIGNAL(mouse_press()),ui->btnNewPic,SLOT(press()));
    connect(ui->btnNewIcon,SIGNAL(mouse_release()),ui->btnNewPic,SLOT(release()));

    connect(ui->btnDelPic,SIGNAL(mouse_release()),this,SLOT(onDelete()));
    connect(ui->btnDel,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDel,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));
    connect(ui->btnDelIcon,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDelIcon,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));

    connect(ui->btnCopyPic,SIGNAL(mouse_release()),this,SLOT(onCopy()));
    connect(ui->btnCopy,SIGNAL(mouse_press()),ui->btnCopyPic,SLOT(press()));
    connect(ui->btnCopy,SIGNAL(mouse_release()),ui->btnCopyPic,SLOT(release()));
    connect(ui->btnCopyIcon,SIGNAL(mouse_press()),ui->btnCopyPic,SLOT(press()));
    connect(ui->btnCopyIcon,SIGNAL(mouse_release()),ui->btnCopyPic,SLOT(release()));

    connect(ui->btnRenamePic,SIGNAL(mouse_release()),this,SLOT(onRename()));
    connect(ui->btnRename,SIGNAL(mouse_press()),ui->btnRenamePic,SLOT(press()));
    connect(ui->btnRename,SIGNAL(mouse_release()),ui->btnRenamePic,SLOT(release()));
    connect(ui->btnRenameIcon,SIGNAL(mouse_press()),ui->btnRenamePic,SLOT(press()));
    connect(ui->btnRenameIcon,SIGNAL(mouse_release()),ui->btnRenamePic,SLOT(release()));

    connect(ui->btnPicturePic,SIGNAL(mouse_release()),this,SLOT(onPicture()));
    connect(ui->btnPicture,SIGNAL(mouse_press()),ui->btnPicturePic,SLOT(press()));
    connect(ui->btnPicture,SIGNAL(mouse_release()),ui->btnPicturePic,SLOT(release()));
    connect(ui->btnPictureIcon,SIGNAL(mouse_press()),ui->btnPicturePic,SLOT(press()));
    connect(ui->btnPictureIcon,SIGNAL(mouse_release()),ui->btnPicturePic,SLOT(release()));

    connect(ui->btnBackupPic,SIGNAL(mouse_release()),this,SLOT(onBackup()));
    connect(ui->btnBackup,SIGNAL(mouse_press()),ui->btnBackupPic,SLOT(press()));
    connect(ui->btnBackup,SIGNAL(mouse_release()),ui->btnBackupPic,SLOT(release()));
    connect(ui->btnBackupIcon,SIGNAL(mouse_press()),ui->btnBackupPic,SLOT(press()));
    connect(ui->btnBackupIcon,SIGNAL(mouse_release()),ui->btnBackupPic,SLOT(release()));

    connect(ui->btnJigPic,SIGNAL(mouse_release()),this,SLOT(onJigChange()));
    connect(ui->btnJig,SIGNAL(mouse_press()),ui->btnJigPic,SLOT(press()));
    connect(ui->btnJig,SIGNAL(mouse_release()),ui->btnJigPic,SLOT(release()));
    connect(ui->btnJigIcon,SIGNAL(mouse_press()),ui->btnJigPic,SLOT(press()));
    connect(ui->btnJigIcon,SIGNAL(mouse_release()),ui->btnJigPic,SLOT(release()));

    connect(ui->btnRefreshPic,SIGNAL(mouse_release()),this,SLOT(onRefresh()));
    connect(ui->btnRefresh,SIGNAL(mouse_press()),ui->btnRefreshPic,SLOT(press()));
    connect(ui->btnRefresh,SIGNAL(mouse_release()),ui->btnRefreshPic,SLOT(release()));
    connect(ui->btnRefreshIcon,SIGNAL(mouse_press()),ui->btnRefreshPic,SLOT(press()));
    connect(ui->btnRefreshIcon,SIGNAL(mouse_release()),ui->btnRefreshPic,SLOT(release()));

    // for special hidden function.
    ui->pushButton->setVisible(false);

    uiLongPressHidden = new HyUi_LongPress();
    uiLongPressUp->Init(2000,200);
    connect(ui->lbMoldPic,SIGNAL(mouse_press()),uiLongPressHidden,SLOT(onPressed()));
    connect(ui->lbMoldPic,SIGNAL(mouse_release()),uiLongPressHidden,SLOT(onReleased()));
    connect(uiLongPressHidden,SIGNAL(sigTrigger()),this,SLOT(onHiddenFunc()));

    top = 0;


}

dialog_recipe::~dialog_recipe()
{
    delete ui;
}
void dialog_recipe::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_recipe::showEvent(QShowEvent *)
{
    /** // in mainwindow init
    if(RM->Initial(Param->Get(HyParam::RECIPE)))
        Param->Set(HyParam::RECIPE, RM->GetNowRecipeNo());
    **/

    Update();

    timer->start();

    Enable_BtnBackup(false);

    SubTop();

    ui->pushButton->setVisible(false);

    m_nUserLevel = Param->Get(HyParam::USER_LEVEL).toInt();

}
void dialog_recipe::hideEvent(QHideEvent *)
{
    timer->stop();
}

void dialog_recipe::onClose()
{
    emit accept();
}

void dialog_recipe::Update()
{
    RM->ReadDatum();

    m_nStartIndex = 0;
    m_nSelectedIndex = RM->ListIndexOf(RM->GetNowRecipeNo());
    m_bDataError = false;
    // now loaded recipe in sd but, don't have sd.
    if(m_nSelectedIndex < 0)
    {
        m_nSelectedIndex = 0;
        m_bDataError = true;
    }

    Redraw_List(m_nStartIndex);

    Display_MaxNo();

    // for auto refresh.
    m_bRefreshCheck = RM->IsSD();
    m_nRefreshCount=0;
}

void dialog_recipe::Redraw_List(int start_index)
{
    int list_index;
    QString strNo,strName,strDate;

    for(int i=0;i<m_vtNo.count();i++)
    {
        list_index = start_index + i;

        if(list_index >= RM->GetCount())
        {
            // no have data.
            strNo.clear();
            strName.clear();
            strDate.clear();
        }
        else
        {
            // have data.
            strNo = RM->GetNo(list_index);
            strName = RM->GetName(list_index);
            strDate = RM->GetDate(list_index);
        }

        m_vtNo[i]->setText(strNo);
        m_vtName[i]->setText(strName);
        m_vtDate[i]->setText(strDate);
    }

    // display total number.
    ui->lbTotal->setText(QString().setNum(RM->GetCount()));
}

void dialog_recipe::onSelect()
{
    QLabel3* btn = (QLabel3*)sender();

    // get selected list index.
    int row_index = m_vtNo.indexOf(btn);
    if(row_index < 0) return;

    // ignore no data line.
    if(m_vtNo[row_index]->text().isEmpty())
        return;

    m_nSelectedIndex = m_nStartIndex + row_index;
}

void dialog_recipe::onUp()
{
    if(m_nSelectedIndex <= 0)
        return;

    m_nSelectedIndex--;
}
void dialog_recipe::onUpLong()
{
    if(m_nSelectedIndex <= 0)
        return;

    m_nSelectedIndex--;
}
void dialog_recipe::onDown()
{
    if(RM->GetCount() <= 0)
        return;
    if(m_nSelectedIndex >= (RM->GetCount()-1))
        return;

    m_nSelectedIndex++;
}
void dialog_recipe::onDownLong()
{
    if(RM->GetCount() <= 0)
        return;
    if(m_nSelectedIndex >= (RM->GetCount()-1))
        return;

    m_nSelectedIndex++;
}

void dialog_recipe::onPress_UpDown()
{
    QLabel3* btn = (QLabel3*)sender();

    QImage img;
    img.load(":/image/image/button_midrect_2.png");
    QPixmap pxm = QPixmap::fromImage(img);
    if(btn == ui->btnUpPic || btn == ui->btnUpIcon)
    {
        ui->btnUpPic->setPixmap(pxm);
    }
    else if(btn == ui->btnDownPic || btn == ui->btnDownIcon)
    {
        ui->btnDownPic->setPixmap(pxm);
    }
}
void dialog_recipe::onRelease_UpDown()
{
    QLabel3* btn = (QLabel3*)sender();

    QImage img;
    img.load(":/image/image/button_midrect_0.png");
    QPixmap pxm = QPixmap::fromImage(img);
    if(btn == ui->btnUpPic || btn == ui->btnUpIcon)
    {
        ui->btnUpPic->setPixmap(pxm);
    }
    else if(btn == ui->btnDownPic || btn == ui->btnDownIcon)
    {
        ui->btnDownPic->setPixmap(pxm);
    }
}

void dialog_recipe::onTimer()
{
    // list table auto redraw according to change m_nSelectedIndex.
    if(m_nSelectedIndex >= (m_nStartIndex+m_vtNo.size()))
    {
        m_nStartIndex = m_nSelectedIndex - (m_vtNo.size()-1);
        Redraw_List(m_nStartIndex);
    }
    else if(m_nSelectedIndex < m_nStartIndex)
    {
        m_nStartIndex = m_nSelectedIndex;
        Redraw_List(m_nStartIndex);
    }

    // delete last line => SelectedIndex move now program last line.
    if(m_nSelectedIndex >= RM->GetCount() && m_nSelectedIndex > 0)
        m_nSelectedIndex--;

    // to change raw color according to selected index.
    int raw_index = m_nSelectedIndex - m_nStartIndex;
    Display_RawColor(raw_index);

    // to mark now loaded recipe.
    int now_list_index = RM->ListIndexOf(RM->GetNowRecipeNo());
    Display_Mark(now_list_index);

    // auto refresh.
    if(++m_nRefreshCount > 5)
    {
        m_nRefreshCount=0;
        if(RM->IsSD() != m_bRefreshCheck)
        {
            Update();

            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::GREEN);
            msg->Title("SD Memory");
            if(m_bRefreshCheck)
                msg->Message(tr("SD Memory inserted!"));
            else
                msg->Message(tr("SD Memory removed!"));

            msg->exec();
        }
    }
}

void dialog_recipe::Display_RawColor(int raw_index)
{
    // raw_index = -1 -> all off

    QPalette* pal = new QPalette();
    pal->setBrush(QPalette::Window,QBrush(QColor(255,255,0)));

    for(int i=0;i<m_vtNo.count();i++)
    {
        if(i == raw_index)
        {
            m_vtNo[i]->setPalette(*pal);
            m_vtNo[i]->setAutoFillBackground(true);
            m_vtName[i]->setPalette(*pal);
            m_vtName[i]->setAutoFillBackground(true);
            m_vtDate[i]->setPalette(*pal);
            m_vtDate[i]->setAutoFillBackground(true);
        }
        else
        {
            m_vtNo[i]->setAutoFillBackground(false);
            m_vtName[i]->setAutoFillBackground(false);
            m_vtDate[i]->setAutoFillBackground(false);
        }

    }
}

void dialog_recipe::onNew()
{
    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->lbNameTitle->text());
    kb->m_kb->SetText("");

    if(kb->exec() != QDialog::Accepted) return;

    // check name rule
    QString name = kb->m_kb->GetText();
    if(!CheckNameRule(name))
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("INPUT ERROR"));
        msg->Message(tr("Invalid Name!"),name);
        msg->exec();
        return;
    }

    int new_list_index = RM->Find_NewListIndex();
    QString disp_name;
    disp_name.sprintf("[%d] ", (new_list_index+1));
    disp_name += name;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(ui->btnNew->text());
    conf->Message(tr("Would you want to new mold?"), disp_name);

    if(conf->exec() == QDialog::Accepted)
    {
        // new work.
        if(!RM->ANew(new_list_index, name))
        {
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(tr("New Error"));
            msg->Message(tr("New Fail!"),disp_name);
            msg->exec();
            return;
        }

        // focus new data.
        m_nSelectedIndex = new_list_index;
        Redraw_List(m_nStartIndex);

        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(ui->btnNew->text());
        msg->Message(tr("New Success!"),disp_name);
        msg->exec();
    }

}

bool dialog_recipe::CheckNameRule(QString name)
{
    if(name.count() < 1
    || name.count() > 255)
        return false;
    return true;
}

void dialog_recipe::Display_MaxNo()
{
    QString str;
    str.sprintf("/%d",RM_MAX_RECIPE_INNER_NUM);
    if(RM->IsSD())
    {
        str += "+";
        ui->lbSD->setEnabled(true);
    }
    else
        ui->lbSD->setEnabled(false);

    ui->lbMax->setText(str);
}

void dialog_recipe::onDelete()
{
    //no delete now loaded recipe.
    QString no = RM->GetNowRecipeNo();
    int now_list_index = RM->ListIndexOf(no);
    if(now_list_index == m_nSelectedIndex)
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Delete Error"));
        msg->Message(tr("Can not delete!!"), RM->GetDisplayName(m_nSelectedIndex));
        msg->exec();
        return;
    }

    QString disp_name = RM->GetDisplayName(m_nSelectedIndex);

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(ui->btnDel->text());
    conf->Message(tr("Would you want to delete?"), disp_name);

    if(conf->exec() == QDialog::Accepted)
    {
        if(!RM->ADelete(m_nSelectedIndex))
        {
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(tr("Delete Error"));
            msg->Message(tr("Delete Fail!"),disp_name);
            msg->exec();
            return;
        }

        Redraw_List(m_nStartIndex);

        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(ui->btnDel->text());
        msg->Message(tr("Delete Success!"),disp_name);
        msg->exec();
    }
}

void dialog_recipe::onCopy()
{
    int new_list_index = RM->Find_NewListIndex();
    QString disp_name = RM->GetDisplayName(m_nSelectedIndex);
    disp_name += " >>> ";
    disp_name += "[";
    disp_name += QString().setNum(new_list_index+1);
    disp_name += "] ";
    disp_name += RM->GetName(m_nSelectedIndex) + "_copy";

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(ui->btnCopy->text());
    conf->Message(tr("Would you want to copy?"), disp_name);

    if(conf->exec() == QDialog::Accepted)
    {
        // Refresh... Sync, if same source & now loaded recipe.
        int now_list_index = RM->ListIndexOf(RM->GetNowRecipeNo());
        if(m_nSelectedIndex == now_list_index)
        {
            if(!RM->ASync(now_list_index))
            {
                dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
                msg->SetColor(dialog_message::RED);
                msg->Title(ui->btnRefresh->text());
                msg->Message(tr("Sync Data Error"), tr("Please try again!"));
                msg->exec();
                return;
            }
        }

        if(!RM->ACopy(m_nSelectedIndex, new_list_index))
        {
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(tr("Copy Error"));
            msg->Message(tr("Copy Fail!"), disp_name);
            msg->exec();
            return;
        }

        m_nSelectedIndex = new_list_index;
        Redraw_List(m_nStartIndex);

        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(ui->btnCopy->text());
        msg->Message(tr("Copy Success!"), disp_name);
        msg->exec();
    }
}

void dialog_recipe::onRename()
{
    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(ui->btnRename->text());
    kb->m_kb->SetText(RM->GetName(m_nSelectedIndex));

    if(kb->exec() != QDialog::Accepted) return;
    // check name rule
    QString name = kb->m_kb->GetText();
    if(RM->GetName(m_nSelectedIndex) == name) return;

    if(!CheckNameRule(name))
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("INPUT ERROR"));
        msg->Message(tr("Invalid Name!"),name);
        msg->exec();
        return;
    }

    QString disp_name = RM->GetName(m_nSelectedIndex);
    disp_name += " >>> ";
    disp_name += name;

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(ui->btnRename->text());
    conf->Message(tr("Would you want to rename?"), disp_name);
    if(conf->exec() == QDialog::Accepted)
    {
        if(!RM->ARename(m_nSelectedIndex, name))
        {
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(tr("Rename Error"));
            msg->Message(tr("Rename Fail!"), disp_name);
            msg->exec();
            return;
        }

        Redraw_List(m_nStartIndex);

        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(ui->btnRename->text());
        msg->Message(tr("Rename Success!"), disp_name);
        msg->exec();
    }
}

void dialog_recipe::onLoad()
{
    if(RM->GetNo(m_nSelectedIndex) == RM->GetNowRecipeNo())
        return;

    QString recipe_name;
    dialog_delaying* dig = new dialog_delaying();

    if(m_bDataError)
    {
        QString disp_name = RM->GetDisplayName(m_nSelectedIndex);

        dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        conf->SetColor(dialog_confirm::SKYBLUE);
        conf->Title(ui->btnLoad->text());
        conf->Message(tr("Would you want to Fored Load?"), disp_name);

        if(conf->exec() == QDialog::Accepted)
        {
            Recipe->Set(HyRecipe::bRecipeChange, 1.0, false); // init macro vars.

            dig->Init(500);
            dig->exec();

            Stop_SubTask();

            if(!RM->ALoad_Force(m_nSelectedIndex))
            {
                dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
                msg->SetColor(dialog_message::RED);
                msg->Title(tr("Force Load Error"));
                msg->Message(tr("Force Load Fail!"), disp_name);
                msg->exec();
                return;
            }

            Param->Set(HyParam::RECIPE, RM->GetNowRecipeNo());
            if(RM->GetNowRecipeName(recipe_name))
                Recipe->Set(HyRecipe::s_RecipeName, recipe_name, false);
            Recipe->Set(HyRecipe::iNowRecipeNo, RM->GetNowRecipeNo().toFloat(), false);

            // Sync..
            gSync_CommonPos_nvs();
            UseSkip->Sync(false);
            SoftSensor->Sync(false);
            Recipe->SaveVariable();

            Update();

            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::SKYBLUE);
            msg->Title(ui->btnLoad->text());
            msg->Message(tr("Force Load Success!"), disp_name);
            msg->exec();

            Recipe->Set(HyRecipe::bSubInit, 1.0, false); // init macro vars.
            Recipe->Set(HyRecipe::bRecipeChange, 0, false);

            Start_SubTask();
        }

        return;
    }

    QString disp_name = RM->GetDisplayName(RM->GetNowRecipeNo());
    disp_name += " >>> ";
    disp_name += RM->GetDisplayName(m_nSelectedIndex);

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(ui->btnLoad->text());
    conf->Message(tr("Would you want to Load?"), disp_name);

    int old_list_index = RM->ListIndexOf(RM->GetNowRecipeNo());

    if(conf->exec() == QDialog::Accepted)
    {
        ti.start();

        Recipe->Set(HyRecipe::bRecipeChange, 1.0, false); // init macro vars.

        dig->Init(500);
        dig->exec();

        Stop_SubTask();

        if(!RM->ALoad(old_list_index, m_nSelectedIndex))
        {
            dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
            msg->SetColor(dialog_message::RED);
            msg->Title(tr("Load Error"));
            msg->Message(tr("Load Fail!"), disp_name);
            msg->exec();
            return;
        }

        qDebug() << "p1 time = " << ti.elapsed() << "msec";

        Param->Set(HyParam::RECIPE, RM->GetNowRecipeNo());
        if(RM->GetNowRecipeName(recipe_name))
            Recipe->Set(HyRecipe::s_RecipeName, recipe_name, false);
        Recipe->Set(HyRecipe::iNowRecipeNo, RM->GetNowRecipeNo().toFloat(), false);

        // Sync..
        gSync_CommonPos_nvs();
        UseSkip->Sync(false);
        SoftSensor->Sync(false);
        Recipe->SaveVariable();

        Redraw_List(m_nStartIndex);

        qDebug() << "p2 time = " << ti.elapsed() << "msec";

        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::SKYBLUE);
        msg->Title(ui->btnLoad->text());
        msg->Message(tr("Load Success!"), disp_name);
        msg->exec();

        Recipe->Set(HyRecipe::bSubInit, 1.0, false); // init macro vars.
        Recipe->Set(HyRecipe::bRecipeChange, 0, false);

        Start_SubTask();
    }

}

void dialog_recipe::on_pushButton_clicked()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(tr("Default Making"));
    conf->Message(tr("Would you want to make defalut recipe?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(tr("Default Making"));

    if(!RM->MakeDefualt())
        msg->Message(tr("Fail to Make Default!"));
    else
        msg->Message(tr("Success to Make Default!"));

    msg->exec();
}

void dialog_recipe::onPicture()
{
    if(!RM->IsSD())
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("No SD Memory"));
        msg->Message(tr("Not found SD Memory!"), tr("Please insert SD Memory."));
        msg->exec();
        return;
    }

    dialog_pictureview* picview = new dialog_pictureview();
    picview->Init(RM->GetNo(m_nSelectedIndex));

    picview->exec();
}

void dialog_recipe::onBackup()
{
    timer->stop();

    dialog_backup* dig = new dialog_backup();

    dig->exec();

    Update();
    timer->start();
}

void dialog_recipe::onJigChange()
{
    dialog_common_position* dig = new dialog_common_position();
    dig->Init(dialog_common_position::JIG_CHANGE, false);
    dig->exec();
}

void dialog_recipe::onRefresh()
{

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(ui->btnRefresh->text());
    conf->Message(tr("Would you want to Refresh & Sync?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    // list refresh & now recipe sync.
    Update();

    int now_list_index = RM->ListIndexOf(RM->GetNowRecipeNo());
    if(now_list_index < 0)
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(ui->btnRefresh->text());
        msg->Message(tr("Not found Now Data!"), tr("Please insert SD Memory. Try again!"));
        msg->exec();
        return;
    }

    if(!RM->ASync(now_list_index))
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(ui->btnRefresh->text());
        msg->Message(tr("Sync Data Error"), tr("Please try again!"));
        msg->exec();
        return;
    }

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(ui->btnRefresh->text());
    msg->Message(tr("Refresh List & Sync data Success!"));
    msg->exec();

    Enable_BtnBackup(true);
}

void dialog_recipe::Display_Mark(int now_list_index)
{
    int list_index;
    for(int i=0;i<m_vtNo.count();i++)
    {
        list_index = m_nStartIndex + i;

        if(list_index == now_list_index)
            m_vtMark[i]->show();
        else
            m_vtMark[i]->hide();
    }
}

void dialog_recipe::Enable_BtnBackup(bool enable)
{
    ui->btnBackupPic->setEnabled(enable);
    ui->btnBackup->setEnabled(enable);
    ui->btnBackupIcon->setEnabled(enable);
}


void dialog_recipe::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}

void dialog_recipe::onHiddenFunc()
{
    if(m_nUserLevel < USER_LEVEL_HIGH)
        return;

    if(!ui->pushButton->isVisible())
        ui->pushButton->setVisible(true);
}

void dialog_recipe::Stop_SubTask()
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    for(int i=0;i<SUB_TASK_NUM;i++)
    {
        if(pCon->getTaskStatus(SUB_TASK+i) == CNR_TASK_RUNNING)
        {
            ret = pCon->holdProgram(SUB_TASK+i);
            if(ret < 0)
                qDebug() << "fail to holdProgram! ret=" << ret << SUB_TASK+i;
            else
                qDebug() << "Stop Subtask " << SUB_TASK+i;
        }
    }
}

void dialog_recipe::Start_SubTask()
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret;
    QStringList autorun, macro;
    QString str;
    int i;
    str = Param->Get(HyParam::SUBTASK_AUTORUN);
    autorun = str.split(",");
    if(autorun.count() != SUB_TASK_NUM)
    {
        autorun.clear();
        for(i=0;i<SUB_TASK_NUM;i++)
            autorun.append("0");
    }
    autorun[0] = "1"; // prefect fix.

    str = Param->Get(HyParam::SUBTASK_MACRO);
    macro = str.split(",");
    if(macro.count() != SUB_TASK_NUM)
    {
        macro.clear();
        for(i=0;i<SUB_TASK_NUM;i++)
            autorun.append("");
    }
    macro[0] = SUBMAIN_MACRO; // perfect fix

    for(i=0;i<SUB_TASK_NUM;i++)
    {
        if(autorun[i].toInt() > 0)
        {
            if(!pCon->findProgram(macro[i])) continue;

            ret = pCon->clearCurProgram(SUB_TASK + i);
            if(ret < 0)
                qDebug() << "fail to clearCurProgram! ret =" << ret << SUB_TASK+i << macro[i];
            ret = pCon->executeProgram(SUB_TASK + i, macro[i], -1);
            if(ret < 0)
                qDebug() << "fail to executeProgram! ret =" << ret << SUB_TASK+i << macro[i];
            else
                qDebug() << "Start Subtask " << SUB_TASK+i;
        }
    }
}
