#include "widget_backup_recovery.h"
#include "ui_widget_backup_recovery.h"

widget_backup_recovery::widget_backup_recovery(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::widget_backup_recovery)
{
    ui->setupUi(this);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));


    connect(ui->btnSelectPic,SIGNAL(mouse_release()),this,SIGNAL(sigSelect()));
    connect(ui->btnSelect,SIGNAL(mouse_press()),ui->btnSelectPic,SLOT(press()));
    connect(ui->btnSelect,SIGNAL(mouse_release()),ui->btnSelectPic,SLOT(release()));
    connect(ui->btnSelectIcon,SIGNAL(mouse_press()),ui->btnSelectPic,SLOT(press()));
    connect(ui->btnSelectIcon,SIGNAL(mouse_release()),ui->btnSelectPic,SLOT(release()));

    m_vtDate.clear();m_vtTime.clear();m_vtName.clear();
    m_vtDate.append(ui->tbDate);  m_vtTime.append(ui->tbTime);  m_vtName.append(ui->tbName);
    m_vtDate.append(ui->tbDate_2);m_vtTime.append(ui->tbTime_2);m_vtName.append(ui->tbName_2);
    m_vtDate.append(ui->tbDate_3);m_vtTime.append(ui->tbTime_3);m_vtName.append(ui->tbName_3);
    m_vtDate.append(ui->tbDate_4);m_vtTime.append(ui->tbTime_4);m_vtName.append(ui->tbName_4);
    m_vtDate.append(ui->tbDate_5);m_vtTime.append(ui->tbTime_5);m_vtName.append(ui->tbName_5);
    m_vtDate.append(ui->tbDate_6);m_vtTime.append(ui->tbTime_6);m_vtName.append(ui->tbName_6);

    int i;
    for(i=0;i<m_vtDate.count();i++)
    {
        connect(m_vtDate[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtTime[i],SIGNAL(mouse_release()),m_vtDate[i],SIGNAL(mouse_release()));
        connect(m_vtName[i],SIGNAL(mouse_release()),m_vtDate[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));


}

widget_backup_recovery::~widget_backup_recovery()
{
    delete ui;
}
void widget_backup_recovery::showEvent(QShowEvent *)
{
    Update();

    timer->start();
}
void widget_backup_recovery::hideEvent(QHideEvent *)
{
    timer->stop();
}
void widget_backup_recovery::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void widget_backup_recovery::onClose()
{
    emit sigClose();
}

void widget_backup_recovery::Display_USB(bool bExist)
{
    ui->lbUSB->setEnabled(bExist);
}

void widget_backup_recovery::Update()
{
    BR->Read_BackupDir();

    m_nStartIndex = 0;
    m_nSelectedIndex = 0;

    Redraw_List(m_nStartIndex);

    Enable_BtnSelect(BR->IsExist(USB_EXIST_PATH));
}

void widget_backup_recovery::onList()
{
    QLabel3* sel = (QLabel3*)sender();

    int row_index = m_vtDate.indexOf(sel);
    if(row_index < 0) return;

    if(m_vtDate[row_index]->text().isEmpty())
        return;

    m_nSelectedIndex = m_nStartIndex + row_index;
}

void widget_backup_recovery::Redraw_List(int start_index)
{
    int list_index;
    QString date, time, name;

    for(int i=0;i<m_vtDate.count();i++)
    {
        list_index = start_index + i;

        if(list_index >= BR->m_BackupList.size())
        {
            // no have data.
            date.clear();
            time.clear();
            name.clear();
        }
        else
        {
            // have data.
            BR->Parsing_BackupDir(BR->m_BackupList[list_index], date, time, name);
        }

        m_vtDate[i]->setText(date);
        m_vtTime[i]->setText(time);
        m_vtName[i]->setText(name);
    }

    ui->lbTotal->setText(QString().setNum(BR->m_BackupList.size()));
}

void widget_backup_recovery::onUp()
{
    if(m_nSelectedIndex <= 0)
        return;

    m_nSelectedIndex--;
}
void widget_backup_recovery::onDown()
{
    if(BR->m_BackupList.size() <= 0)
        return;
    if(m_nSelectedIndex >= (BR->m_BackupList.size()-1))
        return;

    m_nSelectedIndex++;
}

void widget_backup_recovery::onTimer()
{
    // list table auto redraw according to change m_nSelectedIndex.
    if(m_nSelectedIndex >= (m_nStartIndex+m_vtDate.size()))
    {
        m_nStartIndex = m_nSelectedIndex - (m_vtDate.size()-1);
        Redraw_List(m_nStartIndex);
    }
    else if(m_nSelectedIndex < m_nStartIndex)
    {
        m_nStartIndex = m_nSelectedIndex;
        Redraw_List(m_nStartIndex);
    }

    // delete last line => SelectedIndex move now program last line.
    if(m_nSelectedIndex >= BR->m_BackupList.size() && m_nSelectedIndex > 0)
        m_nSelectedIndex--;

    int row_index = m_nSelectedIndex - m_nStartIndex;
    Display_RowColor(row_index);

}

void widget_backup_recovery::Display_RowColor(int row_index)
{
    // raw_index = -1 -> all off

    QPalette* pal = new QPalette();
    pal->setBrush(QPalette::Window,QBrush(QColor(255,255,0)));

    for(int i=0;i<m_vtDate.count();i++)
    {
        if(i == row_index)
        {
            m_vtDate[i]->setPalette(*pal);
            m_vtDate[i]->setAutoFillBackground(true);
            m_vtTime[i]->setPalette(*pal);
            m_vtTime[i]->setAutoFillBackground(true);
            m_vtName[i]->setPalette(*pal);
            m_vtName[i]->setAutoFillBackground(true);
        }
        else
        {
            m_vtDate[i]->setAutoFillBackground(false);
            m_vtTime[i]->setAutoFillBackground(false);
            m_vtName[i]->setAutoFillBackground(false);
        }

    }
}

int widget_backup_recovery::GetSelectedIndex()
{
    return m_nSelectedIndex;
}

void widget_backup_recovery::Enable_BtnSelect(bool bEnable)
{
    ui->btnSelectPic->setEnabled(bEnable);
    ui->btnSelect->setEnabled(bEnable);
    ui->btnSelectIcon->setEnabled(bEnable);
}
