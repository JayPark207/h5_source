#include "hybigmode.h"

HyBigMode::HyBigMode(HyMode* mode)
{
    m_Mode = mode;

    Init();
}

void HyBigMode::Init()
{
    Init_Info();
    m_DefaultName = tr("Mode");
}

void HyBigMode::Init_Info()
{
    ST_BMODE_INFO info;

    m_Info.clear();

    // 0
    info.name = tr("Mode 1");
    info.summary = tr("Basic Operation");
    m_Info.append(info);

    // 1
    info.name = tr("Mode 2");
    info.summary = tr("Basic Operation & Insert");
    m_Info.append(info);

    // add here.

}
bool HyBigMode::Get_Info(int index, ST_BMODE_INFO &info)
{
    if(index < m_Info.size() && index >= 0)
    {
        // have info
        info = m_Info[index];
    }
    else
    {
        // don't have info
        info.name = m_DefaultName;
        info.name += " ";
        info.name += QString().setNum(index + 1);
        info.summary = tr("N/A");
    }

    return true;
}

bool HyBigMode::Read_List(QStringList &list)
{
    CNRobo* pCon = CNRobo::getInstance();

    QString path = PATH_BMODE_DATA_BASE;
    QStringList file_list;

    int ret = pCon->getDatabaseFileList(path, file_list);
    if(ret < 0)
    {
        qDebug() << "HyBigMode::Read_List() getDatabaseFileList ret=" << ret;
        return false;
    }

    file_list.removeAll(".");
    file_list.removeAll("..");

    for(int i=0;i<file_list.count();i++)
        file_list[i].remove(EXT_BMODE_DATA);

    list = file_list;
    qDebug() << list;
    return true;
}


bool HyBigMode::Read_ModeData()
{
    for(int i=0;i<HyMode::MGRP_NUM;i++)
    {
        if(!m_Mode->Read((HyMode::MODE_GROUP)i))
            return false;
    }
    return true;
}

bool HyBigMode::ModeData2FileData(QStringList &file_data)
{
    int i,j,k;
    QStringList temp, result;
    QString str;
    int var_no;

    result.clear();
    for(i=0;i<HyMode::MGRP_NUM;i++)
    {
        for(j=0;j<m_Mode->m_Data[i].count();j++)
        {
            for(k=0;k<m_Mode->m_Data[i][j].Vars.count();k++)
            {
                temp.clear();
                var_no = m_Mode->m_Data[i][j].Vars[k];
                str = m_Mode->m_Recipe->toName((HyRecipe::RECIPE_NUMBER)var_no);
                temp.append(str);
                str.sprintf("%.1f", m_Mode->m_Data[i][j].Datas[k]);
                temp.append(str);

                result.append(temp.join("="));
            }
        }
    }

    file_data = result;

    qDebug() << file_data;
    return true;
}

bool HyBigMode::Save_FileData(int index, QStringList file_data)
{
    QString file_path = MakePath_FileData(index);

    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->createProgramFile(file_path, file_data);
    if(ret < 0)
    {
        qDebug() << "Fail to Save_FileData!";
        qDebug() << "HyBigMode::Save_FileData() createProgramFile ret=" << ret;
        return false;
    }

    qDebug() << "Success to Save_FileData!";
    return true;
}

bool HyBigMode::Delete(int index)
{
    QString file_path = MakePath_FileData(index);

    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->deleteDatabaseFile(file_path);
    if(ret < 0)
    {
        qDebug() << "Fail to Delete!";
        qDebug() << "HyBigMode::Delete() deleteDatabaseFile ret=" << ret;
        return false;
    }

    qDebug() << "Success to Delete!";
    return true;
}


bool HyBigMode::IsExistFile(QString file_path)
{
    CNRobo* pCon = CNRobo::getInstance();

    bool bExist = false;
    if(pCon->checkDirFileExist(file_path, bExist) < 0)
        return false;

    return bExist;
}

QString HyBigMode::MakePath_FileData(int index)
{
    QString str;
    QString file_path = PATH_BMODE_DATA_BASE;
    file_path += "/";
    str.sprintf("%03d.bmd", index+1);
    file_path += str;

    qDebug() << file_path;
    return file_path;
}

QString HyBigMode::Make_FileName(int index)
{
    QString str;
    str.sprintf("%03d", index+1);

    qDebug() << str;
    return str;
}

bool HyBigMode::Read_FileData(int index, QStringList &file_data)
{
    QString file_path = MakePath_FileData(index);

    if(!IsExistFile(file_path))
        return false;

    CNRobo* pCon = CNRobo::getInstance();

    QStringList temp_data;
    int ret = pCon->getProgramFile(file_path, temp_data);
    if(ret < 0)
    {
        qDebug() << "HyBigMode::Read_FileData() getProgramFile ret=" << ret;
        return false;
    }

    if(temp_data.isEmpty())
        return false;

    file_data = temp_data;
    return true;
}

bool HyBigMode::Write_AllVar(QStringList file_data)
{
    QStringList names;
    QList<cn_variant> datas;

    QStringList name_data;
    cn_variant data;
    data.type = CNVAR_FLOAT;

    bool bOK;

    names.clear();
    datas.clear();
    for(int i=0;i<file_data.count();i++)
    {
        name_data.clear();
        name_data = file_data[i].split("=");
        if(name_data.size() != 2)
            continue;

        data.val.f32 = name_data[1].toFloat(&bOK);
        if(!bOK)
            continue;

        names.append(name_data[0]);
        datas.append(data);
    }

    if(names.size() != datas.size())
        return false;

    if(!m_Mode->m_Recipe->SetVars(names, datas))
    {
        qDebug() << "HyBigMode::Write_AllVar() SetVars fail!";
        return false;
    }

    return true;
}

bool HyBigMode::FileData2ViewModeData(QStringList file_data)
{
    // 1. Parsing_File
    if(!Parsing_FileData(file_data))
        return false;

    // 2. Read_ViewData (or not => insert mode change part)
    for(int i=0;i<HyMode::MGRP_NUM;i++)
    {
        if(!Read_ViewData((HyMode::MODE_GROUP)i))
            return false;
    }

    return true;
}

// parsing for searching name & data
bool HyBigMode::Parsing_FileData(QStringList file_data)
{
    QStringList name_data;
    QStringList temp_name, temp_val;

    temp_name.clear();
    temp_val.clear();
    for(int i=0;i<file_data.count();i++)
    {
        name_data = file_data[i].split("=");
        if(name_data.size() != 2)
            continue;

        temp_name.append(name_data[0]);
        temp_val.append(name_data[1]);
    }

    if(temp_name.size() != temp_val.size())
        return false;

    m_Name = temp_name;
    m_Val = temp_val;

    return true;
}

// HyMode m_Data share (this is mode Read() same!!!!)
bool HyBigMode::Read_ViewData(HyMode::MODE_GROUP grp)
{
    int i,j;
    QString var_name, var_val;
    int var_no;

    for(i=0;i<m_Mode->m_Data[grp].count();i++)
    {
        m_Mode->m_Data[grp][i].Datas.clear();
        for(j=0;j<m_Mode->m_Data[grp][i].Vars.count();j++)
        {
            var_no = m_Mode->m_Data[grp][i].Vars[j];
            var_name = m_Mode->m_Recipe->toName((HyRecipe::RECIPE_NUMBER)var_no);

            Get_FileData(var_name, var_val); // from file data.

            m_Mode->m_Data[grp][i].Datas.append(var_val.toFloat());
        }
    }

    m_Mode->MakeResult(grp);
    return true;
}
bool HyBigMode::Gets_FileData(QStringList names, QStringList &values)
{
    if(names.isEmpty())
        return false;

    QStringList temp_val;
    int index;

    temp_val.clear();
    for(int i=0;i<names.count();i++)
    {
        index = m_Name.indexOf(names[i]);
        if(index < 0)
        {
            temp_val.append("0"); // don't have data.
            continue;
        }

        temp_val.append(m_Val[index]);
    }

    if(names.size() != temp_val.size())
        return false;

    values = temp_val;
    return true;
}
bool HyBigMode::Get_FileData(QString name, QString &value)
{
    QString temp_val;
    int index;

    index = m_Name.indexOf(name);
    if(index < 0)
        temp_val = "0"; // don't have data.
    else
        temp_val = m_Val[index];

    value = temp_val;
    return true;
}

QString HyBigMode::MakePath_Image(int index)
{
    QString str;
    QString file_path = PATH_BMODE_IMAGE_BASE;
    file_path += "/";
    str.sprintf("%03d", index+1);
    file_path += str;
    file_path += ".PNG";

    qDebug() << file_path;
    return file_path;
}

bool HyBigMode::Read_Image(int index, QPixmap &pxm)
{
    QString img_path = MakePath_Image(index);
    QString default_Pic = ":/image/image/Logo_HY.png";

    QImage img;
    if(!img.load(img_path))
    {
        img.load(default_Pic);
    }

    pxm = QPixmap::fromImage(img);
    return true;
}

