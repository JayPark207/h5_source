#ifndef DIALOG_ROTATION_H
#define DIALOG_ROTATION_H

#include <QDialog>
#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"

namespace Ui {
class dialog_rotation;
}

class dialog_rotation : public QDialog
{
    Q_OBJECT

public:
    enum ENUM_TYPE
    {
        ROTATION=0,
        SWIVEL,

        TYPE_NUM
    };

public:
    explicit dialog_rotation(QWidget *parent = 0);
    ~dialog_rotation();

    void Init(ENUM_TYPE type, QString now_value);    // 0=rotation, 1=swivel
    QString GetValue();

private:

    void Update();
    void Init_String();
    void Select(int i, bool on);
    void Setting(bool on);

    void Update_Value();

public slots:
    void onOK();
    void onCancel();
    void onSelect();
    void onSetting();
    void onChange();

private:
    Ui::dialog_rotation *ui;

    dialog_numpad* np;
    int m_Type;
    QString m_strNowValue;
    int m_nSelectedIndex;
    bool m_bSetting;

    QString m_strTitle[TYPE_NUM];
    QString m_strPosName[TYPE_NUM][5];

    QVector<QLabel3*> m_vtPosName, m_vtSetName;
    QVector<QLabel4*> m_vtPosPic, m_vtSetPic;
    QVector<QLabel3*> m_vtPosVal, m_vtSetVal;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ROTATION_H
