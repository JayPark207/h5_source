#ifndef PAGE_MAIN2_SPECIAL_H
#define PAGE_MAIN2_SPECIAL_H

#include <QWidget>

#include "global.h"
#include "dialog_task_control.h"
#include "dialog_sasang_group.h"

#include "dialog_anti_vibration.h"
#include "dialog_delaying.h"

namespace Ui {
class page_main2_special;
}

class page_main2_special : public QWidget
{
    Q_OBJECT
    
public:
    explicit page_main2_special(QWidget *parent = 0);
    ~page_main2_special();

private slots:
    void onDeburr();
    void onTask();

    void onTest();

private:
    Ui::page_main2_special *ui;

    dialog_sasang_group* dig_sasang_group;

    void UserLevel();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // PAGE_MAIN2_SPECIAL_H
