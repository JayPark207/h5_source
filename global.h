#ifndef GLOBAL_H
#define GLOBAL_H

#include <QApplication>
#include <QFontDatabase>
#include <QStackedWidget>
#include <QDesktopWidget>

#include <QWidget>
#include <QDebug>

#include "defines.h"

#include "dtp7-etc.h"
#include <QString>
#include <QTranslator>

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"

#include "hypos.h"
#include "hytime.h"
#include "hyinput.h"
#include "hyoutput.h"
#include "hyuserdata.h"

#include "hyparam.h"
#include "hyrecipe.h"
#include "hyrecipemanage2.h"
#include "hykey.h"
#include "hypendent.h"
#include "hystepedit.h"
#include "hyerror.h"
#include "hystatus.h"
#include "hyrobotconfig.h"
#include "hytponly.h"
#include "hylog.h"
#include "hybackuprecovery.h"
#include "hyuseskip.h"
#include "hymode.h"
#include "hylanguage.h"
#include "hysoftsensor.h"
#include "hyecatmotor.h"
#include "hysasang.h"
#include "hyeuromap.h"
#include "hynetwork.h"
#include "hybigmode.h"


enum PAGE_INDEX{
    MAIN2_PAGE=0,
    AUTORUN_FORM,
    SPECIAL_SETTING_FORM,

    //...
};

enum MENU_PAGE_INDEX{
    MENU_PAGE_MOLD=0,
    MENU_PAGE_TEACH,
    MENU_PAGE_DEVICE,
    MENU_PAGE_MANAGE,

};

enum MAIN2_PAGE_INDEX{
    MAIN2_PAGE_TEACH=0,
    MAIN2_PAGE_MONITOR,
    MAIN2_PAGE_SETTING,
    MAIN2_PAGE_SPECIAL,

    MAIN2_PAGE_NONE=999,

};

enum TOP_INDEX{
    TOP_MAIN = 0,
    TOP_SPEED,
};

enum DIALOG_INDEX{
    DIG_MODE_SELECT=0,
    DIG_NUMPAD,
    DIG_POS_TEACH,
    DIG_CONFIRM,
    DIG_MSG,
    DIG_KEYBOARD,
    DIG_HOME,
    DIG_ERR,
    DIG_USERLEVEL,

};

enum JOG_TYPE{
    JOG_TYPE_HY = 0,
    JOG_TYPE_JOINT,
    JOG_TYPE_TRANS,
    JOG_TYPE_HY_TOOL,
};


// hy global classes
extern HyPos*               Posi;
extern HyTime*              Time;
extern HyInput*             Input;
extern HyOutput*            Output;
extern HyUserData*          UserData;   // data set for user add step.

extern HyParam*             Param;
extern HyRecipe*            Recipe;
extern HyRecipeManage2*     RM;
//extern HyLedBuzzer*         Pendent;
extern HyPendent*           Pendent;
extern HyStepEdit*          StepEdit;
extern HyError*             Error;
extern HyStatus*            Status;
extern HyRobotConfig*       RobotConfig;
extern HyTPonly*            TPonly;
extern HyLog*               Log;
extern HyBackupRecovery*    BR;
extern HyUseSkip*           UseSkip;
extern HyMode*              Mode;
extern HyLanguage*          Language;
extern HySoftSensor*        SoftSensor;
extern HyEcatMotor*         Motors;
extern HySasang*            Sasang;
extern HyEuromap*           Euromap;
extern HyNetwork*           Network;
extern HyBigMode*           BigMode;


// global valiable
extern PAGE_INDEX g_CurrentMainPage;
extern PAGE_INDEX g_PreMainPage;
extern TOP_INDEX g_CurrentTopPage;
extern TOP_INDEX g_PreTopPage;
extern QTranslator gTranslator;
extern QLocale gLocale;

extern QWidget* gLabelWidget;
extern QPoint gLabelReleasePoint;
extern volatile int g_nPxmIndex;
extern QVector<QPixmap> g_vtPxmNoPress;
extern QVector<QImage> g_vtImgNoPress;
extern QVector<QPixmap> g_vtPxmPress;
extern QVector<QImage> g_vtImgPress;
extern QVector<QPixmap> g_vtPxmLabel2;


enum KEY_COMMAND
{
    KEYCMD_NONE=0,

    KEYCMD_L_LEFT,
    KEYCMD_L_UP,
    KEYCMD_L_RIGHT,
    KEYCMD_L_DOWN,
    KEYCMD_L_BTN1,
    KEYCMD_L_BTN2,
    KEYCMD_L_BTN3,

    KEYCMD_R_L1,
    KEYCMD_R_R1,
    KEYCMD_R_L2,
    KEYCMD_R_R2,
    KEYCMD_R_L3,
    KEYCMD_R_R3,
    KEYCMD_R_L4,
    KEYCMD_R_R4,
    KEYCMD_R_L5,
    KEYCMD_R_R5,
    KEYCMD_R_L6,
    KEYCMD_R_R6,
};
extern KEY_COMMAND g_nKeyCommand_Press;
extern KEY_COMMAND g_nKeyCommand_Release;



// global functions
void gSetMainPage(PAGE_INDEX index);
PAGE_INDEX gGetMainPage();
PAGE_INDEX gGetPreMainPage();
void* gGetMainPage(PAGE_INDEX index);

void gSetTopPage(TOP_INDEX index);
TOP_INDEX gGetTopPage();
TOP_INDEX gGetPreTopPage();
void* gGetTopPage(TOP_INDEX index);

void* gGetDialog(DIALOG_INDEX index);

void gStart_IOUpdate();
void gStop_IOUpdate();
bool gIs_IOUpdate();
void gSet_IOUpdate(bool input, bool output);
bool gGet_InputUpdate();
bool gGet_OutputUpdate();


/* 모션관련 글로벌함수*/
void gReadyJog(bool enable);  // ready to jog move.
void gReadyMacro(bool enable); // ready to macro run.
void gMakeUsePosList(HyStepData::ENUM_STEP step, QVector<int>& use_pos);
void gMakeUsePosList(QList<ST_STEP_DATA>& step_info, QVector<int>& use_pos);
void gSetJogType(JOG_TYPE jog_type);
JOG_TYPE gGetJogType();
float gGetWristAngle(cn_joint joint);
float gGetWristAngle(float shoulder, float elbow, float wrist);
void gSync_CommonPos();
void gSync_CommonPos_nvs(); // nvs = no variable save.

#ifdef _H6_
float gGetJoint2Disp(int joint_no, cn_joint joint);
float gGetDisp2Joint(int joint_no, float input_value, cn_joint curr_joint);
cn_joint gMakeCnJoint(float* p_joint);

extern bool g_bRotRealDisp; // false:origin J4, true:making rotation angle
extern bool g_bSwvRealDisp; // false:origin J5, true:making swivel angle
bool gGetRotRealDisp();
bool gGetSwvRealDisp();
#endif

void Capture();
void gBeep();
void gBeep2();

void NanumGothic();
void NanumMyeongjo();
void KoPubWorldBatang();
void KoPubWorldDotum();
void Noto_Sans();
void Noto_Sans_Thai();
void SimHei();
void gChange_Font(HyParam::PARAM_LANGUAGE lang_no);
/**************************************************************/
/**************************************************************/



/* class hy recipemange 로 대체 한다. */
#define MAX_MOLDDATA_NUM    100
#define MOLDDATA_NUM_PER_PAGE   5
#define MAX_MOLDDATA_PAGE   (MAX_MOLDDATA_NUM / MOLDDATA_NUM_PER_PAGE)
struct ST_MOLDDATA{
    int nIndex;
    QString strName;
    QString strDate;

};
extern ST_MOLDDATA g_stMoldData[MAX_MOLDDATA_NUM];
extern ST_MOLDDATA g_stMoldDataLoaded;
/* 여기까지. */

enum DEF_MC_STATUS{
    MC_READY=0,
    MC_ERROR,
    MC_WARN,
    MC_SLEEP
};

struct MC_STATUS_BIT
{
    USHORT bEmo:1;
    USHORT bReserved:15;
};

enum DEF_MC_RUNMODE
{
    MC_RUNMODE_MANUAL=0,
    MC_RUNMODE_AUTO
};

struct ST_ROBOT{

};

struct ST_MACHINE
{
    ST_ROBOT Robot;
    MC_STATUS_BIT StatusBit;
    DEF_MC_STATUS Status;
    DEF_MC_RUNMODE RunMode;
    int nUserLevel;

};
extern ST_MACHINE Machine;


#endif // GLOBAL_H
