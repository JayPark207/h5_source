#include "dialog_program_manage.h"
#include "ui_dialog_program_manage.h"

dialog_program_manage::dialog_program_manage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_program_manage)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // Database
    m_vtDbNo.clear();m_vtDbProg.clear();
    m_vtDbNo.append(ui->tbDbNo);  m_vtDbProg.append(ui->tbDbProg);
    m_vtDbNo.append(ui->tbDbNo_2);m_vtDbProg.append(ui->tbDbProg_2);
    m_vtDbNo.append(ui->tbDbNo_3);m_vtDbProg.append(ui->tbDbProg_3);
    m_vtDbNo.append(ui->tbDbNo_4);m_vtDbProg.append(ui->tbDbProg_4);
    m_vtDbNo.append(ui->tbDbNo_5);m_vtDbProg.append(ui->tbDbProg_5);
    m_vtDbNo.append(ui->tbDbNo_6);m_vtDbProg.append(ui->tbDbProg_6);
    m_vtDbNo.append(ui->tbDbNo_7);m_vtDbProg.append(ui->tbDbProg_7);

    int i;
    for(i=0;i<m_vtDbNo.count();i++)
    {
        connect(m_vtDbNo[i],SIGNAL(mouse_release()),this,SLOT(onDbList()));
        connect(m_vtDbProg[i],SIGNAL(mouse_release()),m_vtDbNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnRecipeUpPic,SIGNAL(mouse_release()),this,SLOT(onRecipe_Up()));
    connect(ui->btnRecipeUpIcon,SIGNAL(mouse_press()),ui->btnRecipeUpPic,SLOT(press()));
    connect(ui->btnRecipeUpIcon,SIGNAL(mouse_release()),ui->btnRecipeUpPic,SLOT(release()));

    connect(ui->btnRecipeDownPic,SIGNAL(mouse_release()),this,SLOT(onRecipe_Down()));
    connect(ui->btnRecipeDownIcon,SIGNAL(mouse_press()),ui->btnRecipeDownPic,SLOT(press()));
    connect(ui->btnRecipeDownIcon,SIGNAL(mouse_release()),ui->btnRecipeDownPic,SLOT(release()));

    connect(ui->btnDbUpPic,SIGNAL(mouse_release()),this,SLOT(onDbList_Up()));
    connect(ui->btnDbUpIcon,SIGNAL(mouse_press()),ui->btnDbUpPic,SLOT(press()));
    connect(ui->btnDbUpIcon,SIGNAL(mouse_release()),ui->btnDbUpPic,SLOT(release()));

    connect(ui->btnDbDownPic,SIGNAL(mouse_release()),this,SLOT(onDbList_Down()));
    connect(ui->btnDbDownIcon,SIGNAL(mouse_press()),ui->btnDbDownPic,SLOT(press()));
    connect(ui->btnDbDownIcon,SIGNAL(mouse_release()),ui->btnDbDownPic,SLOT(release()));

    connect(ui->btnDbViewPic,SIGNAL(mouse_release()),this,SLOT(onDbTool_Edit()));
    connect(ui->btnDbViewIcon,SIGNAL(mouse_press()),ui->btnDbViewPic,SLOT(press()));
    connect(ui->btnDbViewIcon,SIGNAL(mouse_release()),ui->btnDbViewPic,SLOT(release()));

    connect(ui->btnDbDelPic,SIGNAL(mouse_release()),this,SLOT(onDbTool_Del()));
    connect(ui->btnDbDelIcon,SIGNAL(mouse_press()),ui->btnDbDelPic,SLOT(press()));
    connect(ui->btnDbDelIcon,SIGNAL(mouse_release()),ui->btnDbDelPic,SLOT(release()));

    // DB <-> TP
    connect(ui->btnTp2DbPic,SIGNAL(mouse_release()),this,SLOT(onTp2Db()));
    connect(ui->btnTp2DbIcon,SIGNAL(mouse_press()),ui->btnTp2DbPic,SLOT(press()));
    connect(ui->btnTp2DbIcon,SIGNAL(mouse_release()),ui->btnTp2DbPic,SLOT(release()));

    connect(ui->btnDb2TpPic,SIGNAL(mouse_release()),this,SLOT(onDb2Tp()));
    connect(ui->btnDb2TpIcon,SIGNAL(mouse_press()),ui->btnDb2TpPic,SLOT(press()));
    connect(ui->btnDb2TpIcon,SIGNAL(mouse_release()),ui->btnDb2TpPic,SLOT(release()));

    // TP
    m_vtTpNo.clear();m_vtTpProg.clear();
    m_vtTpNo.append(ui->tbTpNo);  m_vtTpProg.append(ui->tbTpProg);
    m_vtTpNo.append(ui->tbTpNo_2);m_vtTpProg.append(ui->tbTpProg_2);
    m_vtTpNo.append(ui->tbTpNo_3);m_vtTpProg.append(ui->tbTpProg_3);
    m_vtTpNo.append(ui->tbTpNo_4);m_vtTpProg.append(ui->tbTpProg_4);
    m_vtTpNo.append(ui->tbTpNo_5);m_vtTpProg.append(ui->tbTpProg_5);
    m_vtTpNo.append(ui->tbTpNo_6);m_vtTpProg.append(ui->tbTpProg_6);
    m_vtTpNo.append(ui->tbTpNo_7);m_vtTpProg.append(ui->tbTpProg_7);
    m_vtTpNo.append(ui->tbTpNo_8);m_vtTpProg.append(ui->tbTpProg_8);
    m_vtTpNo.append(ui->tbTpNo_9);m_vtTpProg.append(ui->tbTpProg_9);

    for(i=0;i<m_vtTpNo.count();i++)
    {
        connect(m_vtTpNo[i],SIGNAL(mouse_release()),this,SLOT(onTpList()));
        connect(m_vtTpProg[i],SIGNAL(mouse_release()),m_vtTpNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnTpUpPic,SIGNAL(mouse_release()),this,SLOT(onTpList_Up()));
    connect(ui->btnTpUpIcon,SIGNAL(mouse_press()),ui->btnTpUpPic,SLOT(press()));
    connect(ui->btnTpUpIcon,SIGNAL(mouse_release()),ui->btnTpUpPic,SLOT(release()));

    connect(ui->btnTpDownPic,SIGNAL(mouse_release()),this,SLOT(onTpList_Down()));
    connect(ui->btnTpDownIcon,SIGNAL(mouse_press()),ui->btnTpDownPic,SLOT(press()));
    connect(ui->btnTpDownIcon,SIGNAL(mouse_release()),ui->btnTpDownPic,SLOT(release()));

    connect(ui->btnTpViewPic,SIGNAL(mouse_release()),this,SLOT(onTpTool_Edit()));
    connect(ui->btnTpViewIcon,SIGNAL(mouse_press()),ui->btnTpViewPic,SLOT(press()));
    connect(ui->btnTpViewIcon,SIGNAL(mouse_release()),ui->btnTpViewPic,SLOT(release()));

    connect(ui->btnTpDelPic,SIGNAL(mouse_release()),this,SLOT(onTpTool_Del()));
    connect(ui->btnTpDelIcon,SIGNAL(mouse_press()),ui->btnTpDelPic,SLOT(press()));
    connect(ui->btnTpDelIcon,SIGNAL(mouse_release()),ui->btnTpDelPic,SLOT(release()));


    // TP <-> USB
    connect(ui->btnTp2UsbPic,SIGNAL(mouse_release()),this,SLOT(onTp2Usb()));
    connect(ui->btnTp2UsbIcon,SIGNAL(mouse_press()),ui->btnTp2UsbPic,SLOT(press()));
    connect(ui->btnTp2UsbIcon,SIGNAL(mouse_release()),ui->btnTp2UsbPic,SLOT(release()));

    connect(ui->btnUsb2TpPic,SIGNAL(mouse_release()),this,SLOT(onUsb2Tp()));
    connect(ui->btnUsb2TpIcon,SIGNAL(mouse_press()),ui->btnUsb2TpPic,SLOT(press()));
    connect(ui->btnUsb2TpIcon,SIGNAL(mouse_release()),ui->btnUsb2TpPic,SLOT(release()));

    // USB
    m_vtUsbNo.clear();m_vtUsbProg.clear();
    m_vtUsbNo.append(ui->tbUsbNo);  m_vtUsbProg.append(ui->tbUsbProg);
    m_vtUsbNo.append(ui->tbUsbNo_2);m_vtUsbProg.append(ui->tbUsbProg_2);
    m_vtUsbNo.append(ui->tbUsbNo_3);m_vtUsbProg.append(ui->tbUsbProg_3);
    m_vtUsbNo.append(ui->tbUsbNo_4);m_vtUsbProg.append(ui->tbUsbProg_4);
    m_vtUsbNo.append(ui->tbUsbNo_5);m_vtUsbProg.append(ui->tbUsbProg_5);
    m_vtUsbNo.append(ui->tbUsbNo_6);m_vtUsbProg.append(ui->tbUsbProg_6);
    m_vtUsbNo.append(ui->tbUsbNo_7);m_vtUsbProg.append(ui->tbUsbProg_7);
    m_vtUsbNo.append(ui->tbUsbNo_8);m_vtUsbProg.append(ui->tbUsbProg_8);
    m_vtUsbNo.append(ui->tbUsbNo_9);m_vtUsbProg.append(ui->tbUsbProg_9);

    m_rectShow = ui->wigUsb->geometry();
    m_rectHide = ui->wigUsbRefresh->geometry();

    for(i=0;i<m_vtUsbNo.count();i++)
    {
        connect(m_vtUsbNo[i],SIGNAL(mouse_release()),this,SLOT(onUsbList()));
        connect(m_vtUsbProg[i],SIGNAL(mouse_release()),m_vtUsbNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUsbUpPic,SIGNAL(mouse_release()),this,SLOT(onUsbList_Up()));
    connect(ui->btnUsbUpIcon,SIGNAL(mouse_press()),ui->btnUsbUpPic,SLOT(press()));
    connect(ui->btnUsbUpIcon,SIGNAL(mouse_release()),ui->btnUsbUpPic,SLOT(release()));

    connect(ui->btnUsbDownPic,SIGNAL(mouse_release()),this,SLOT(onUsbList_Down()));
    connect(ui->btnUsbDownIcon,SIGNAL(mouse_press()),ui->btnUsbDownPic,SLOT(press()));
    connect(ui->btnUsbDownIcon,SIGNAL(mouse_release()),ui->btnUsbDownPic,SLOT(release()));

    connect(ui->btnUsbViewPic,SIGNAL(mouse_release()),this,SLOT(onUsbTool_Edit()));
    connect(ui->btnUsbViewIcon,SIGNAL(mouse_press()),ui->btnUsbViewPic,SLOT(press()));
    connect(ui->btnUsbViewIcon,SIGNAL(mouse_release()),ui->btnUsbViewPic,SLOT(release()));

    connect(ui->btnUsbDelPic,SIGNAL(mouse_release()),this,SLOT(onUsbTool_Del()));
    connect(ui->btnUsbDelIcon,SIGNAL(mouse_press()),ui->btnUsbDelPic,SLOT(press()));
    connect(ui->btnUsbDelIcon,SIGNAL(mouse_release()),ui->btnUsbDelPic,SLOT(release()));

    connect(ui->btnUsbRefreshPic,SIGNAL(mouse_release()),this,SLOT(onUsbRefresh()));
    connect(ui->btnUsbRefreshIcon,SIGNAL(mouse_press()),ui->btnUsbRefreshPic,SLOT(press()));
    connect(ui->btnUsbRefreshIcon,SIGNAL(mouse_release()),ui->btnUsbRefreshPic,SLOT(release()));

    //clear
    dig_program_manage_edit = 0;
    dig_program_manage_edit2 = 0;
    dig_program_manage_edit3 = 0;

}

dialog_program_manage::~dialog_program_manage()
{
    delete ui;
}
void dialog_program_manage::showEvent(QShowEvent *)
{
    Update_Recipe();
    Update_Tp();
    Update_Usb();

    timer->start();

}
void dialog_program_manage::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_program_manage::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_program_manage::onTimer()
{
    Check_UsbOut();
}

void dialog_program_manage::onClose()
{
    emit accept();
}

void dialog_program_manage::Update_Recipe()
{
    Make_RecipeList(m_RecipeList);
    m_nRecipeSelectedIndex = 0;
    Display_RecipeList(m_nRecipeSelectedIndex);

}

bool dialog_program_manage::Make_RecipeList(QStringList& recipe_list)
{
    // read recipe folder list
    QString recipe_path = RM_RECIPE_PATH;
    QStringList recipe_dir_list;

    CNRobo* pCon = CNRobo::getInstance();

    recipe_dir_list.clear();
    int ret = pCon->getDatabaseDirList(recipe_path, recipe_dir_list);
    if(ret < 0)
    {
        qDebug() << "getDatabaseDirList() ret = " << ret;
        return false;
    }

    recipe_dir_list.removeAll(".");
    recipe_dir_list.removeAll("..");
    recipe_dir_list.removeAll("default");

    QList<int> int_list;
    foreach(QString str, recipe_dir_list)
    {
        int_list << str.toInt();
    }
    qSort(int_list.begin(),int_list.end());

    recipe_list.clear();
    recipe_list.append("default");
    foreach(int num, int_list)
    {
        recipe_list << QString().setNum(num);
    }

    return true;
}

void dialog_program_manage::Display_RecipeList(int recipe_selected_index)
{
    if(recipe_selected_index < 0)
        return;
    if(recipe_selected_index >= m_RecipeList.size())
        return;

    ui->lbRecipeName->setText(m_RecipeList[recipe_selected_index]);

    QString str;
    str.sprintf("%d/%d",recipe_selected_index+1, m_RecipeList.size());
    ui->lbRecipeNo->setText(str);

    // add program list view function.
    Make_RecipeProgList(m_RecipeList[recipe_selected_index], m_RecipeProgList);
    m_nDbStartIndex=0;m_nDbSelectedIndex=0;
    Redraw_RecipeProgList(m_nDbStartIndex);
}

bool dialog_program_manage::Make_RecipeProgList(QString recipe_name, QStringList &prog_list)
{
    QString path = RM_RECIPE_PATH;
    path += "/";
    path += recipe_name;
    path += RM_PROG_DIRNAME;

    CNRobo* pCon = CNRobo::getInstance();

    prog_list.clear();
    int ret = pCon->getDatabaseFileList(path, prog_list);
    if(ret < 0)
    {
        prog_list.clear();
        qDebug() << "getDatabaseFileList() ret = " << ret << "path=" << path;
        return false;
    }

    prog_list.removeAll(".");
    prog_list.removeAll("..");

    return true;
}

void dialog_program_manage::onRecipe_Up()
{
    if(m_nRecipeSelectedIndex <= 0)
        return;

    Display_RecipeList(--m_nRecipeSelectedIndex);
}
void dialog_program_manage::onRecipe_Down()
{
    if(m_nRecipeSelectedIndex >= (m_RecipeList.size()-1))
        return;

    Display_RecipeList(++m_nRecipeSelectedIndex);
}


void dialog_program_manage::Redraw_RecipeProgList(int start_index)
{

    int cal_size = m_RecipeProgList.size() - start_index;
    int index;
    QString prog,no;

    Display_DbListOn(-1); // all off

    for(int i=0;i<m_vtDbNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            prog = m_RecipeProgList[index];
            no = QString().setNum(index+1);
        }
        else
        {
            // no have data.
            prog.clear();
            no.clear();
        }

        m_vtDbProg[i]->setText(prog);
        m_vtDbNo[i]->setText(no);

        if((start_index + i) == m_nDbSelectedIndex)
        {
            // display
            Display_DbListOn(i);
        }
    }
}

void dialog_program_manage::Display_DbListOn(int table_index)
{
    for(int i=0;i<m_vtDbNo.count();i++)
    {
        m_vtDbNo[i]->setAutoFillBackground(i == table_index);
        m_vtDbProg[i]->setAutoFillBackground(i == table_index);
    }
}

void dialog_program_manage::onDbList()
{
    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtDbNo.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nDbStartIndex + index;
    if(list_index == m_nDbSelectedIndex) return;

    if(list_index < m_RecipeProgList.size())
    {
        m_nDbSelectedIndex = list_index;
        Redraw_RecipeProgList(m_nDbStartIndex);
    }
}
void dialog_program_manage::onDbList_Up()
{
    if(m_nDbStartIndex <= 0)
        return;

    Redraw_RecipeProgList(--m_nDbStartIndex);
}
void dialog_program_manage::onDbList_Down()
{
    if(m_nDbStartIndex >= (m_RecipeProgList.size() - m_vtDbNo.size()))
        return;

    Redraw_RecipeProgList(++m_nDbStartIndex);
}

void dialog_program_manage::onDbTool_Edit()
{
    if(dig_program_manage_edit == 0)
        dig_program_manage_edit = new dialog_program_manage_edit();

    QString path = RM_RECIPE_PATH;
    path += "/";
    path += m_RecipeList[m_nRecipeSelectedIndex];
    path += RM_PROG_DIRNAME;
    path += "/";
    path += m_RecipeProgList[m_nDbSelectedIndex];

    dig_program_manage_edit->wig_program->Init(path, true);
    dig_program_manage_edit->exec();
}

void dialog_program_manage::onDbTool_Del()
{
    QString path = RM_RECIPE_PATH;
    path += "/";
    path += m_RecipeList[m_nRecipeSelectedIndex];
    path += RM_PROG_DIRNAME;
    path += "/";
    path += m_RecipeProgList[m_nDbSelectedIndex];

    qDebug() << "Del Path:" << path;

    CNRobo* pCon = CNRobo::getInstance();
    bool bExist = false;
    pCon->checkDirFileExist(path, bExist);

    qDebug() << bExist;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!bExist)
    {

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Error"));
        msg->Message(tr("Don't exist this file!"), path);
        msg->exec();
        return;
    }

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title("File Delete");
    conf->Message(tr("Would you want to Delete this file?"), path);
    if(conf->exec() != QDialog::Accepted) return;

    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title("File Delete");
    conf->Message(tr("Really, Would you want to Delete this file?"), path);
    if(conf->exec() != QDialog::Accepted) return;

    // work delete
    int ret = pCon->deleteDatabaseFile(path);
    if(ret < 0)
    {
        qDebug() << "deleteDatabaseFile ret=" << ret << path;
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("File Delete Error"));
        msg->Message(tr("Fail to delete this file!"), path);
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::BLUE);
    msg->Title(tr("File Delete"));
    msg->Message(tr("Success to delete this file!"), path);
    msg->exec();

    // re-make recipe prog list & display.
    Make_RecipeProgList(m_RecipeList[m_nRecipeSelectedIndex], m_RecipeProgList);
    m_nDbStartIndex=0;m_nDbSelectedIndex=0;
    Redraw_RecipeProgList(m_nDbStartIndex);
}


/** For Tp **/
void dialog_program_manage::Update_Tp()
{
    Make_TpProgList(m_TpProgList);
    m_nTpSelectedIndex=0;m_nTpStartIndex=0;
    Redraw_TpProgList(m_nTpStartIndex);
}
bool dialog_program_manage::Make_TpProgList(QStringList &prog_list)
{
    QString path = RM_PROG_PATH;

    CNRobo* pCon = CNRobo::getInstance();

    prog_list.clear();
    int ret = pCon->getDatabaseFileList(path, prog_list);
    if(ret < 0)
    {
        prog_list.clear();
        qDebug() << "getDatabaseFileList() ret = " << ret << "path=" << path;
        return false;
    }

    prog_list.removeAll(".");
    prog_list.removeAll("..");

    return true;
}
void dialog_program_manage::Redraw_TpProgList(int start_index)
{
    int cal_size = m_TpProgList.size() - start_index;
    int index;
    QString prog,no;

    Display_TpListOn(-1); // all off

    for(int i=0;i<m_vtTpNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            prog = m_TpProgList[index];
            no = QString().setNum(index+1);
        }
        else
        {
            // no have data.
            prog.clear();
            no.clear();
        }

        m_vtTpProg[i]->setText(prog);
        m_vtTpNo[i]->setText(no);

        if((start_index + i) == m_nTpSelectedIndex)
        {
            // display
            Display_TpListOn(i);
        }
    }
}

void dialog_program_manage::Display_TpListOn(int table_index)
{
    for(int i=0;i<m_vtTpNo.count();i++)
    {
        m_vtTpNo[i]->setAutoFillBackground(i == table_index);
        m_vtTpProg[i]->setAutoFillBackground(i == table_index);
    }
}

void dialog_program_manage::onTpList()
{
    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtTpNo.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nTpStartIndex + index;
    if(list_index == m_nTpSelectedIndex) return;

    if(list_index < m_TpProgList.size())
    {
        m_nTpSelectedIndex = list_index;
        Redraw_TpProgList(m_nTpStartIndex);
    }
}
void dialog_program_manage::onTpList_Up()
{
    if(m_nTpStartIndex <= 0)
        return;

    Redraw_TpProgList(--m_nTpStartIndex);
}
void dialog_program_manage::onTpList_Down()
{
    if(m_nTpStartIndex >= (m_TpProgList.size() - m_vtTpNo.size()))
        return;

    Redraw_TpProgList(++m_nTpStartIndex);
}
void dialog_program_manage::onTpTool_Edit()
{
    if(dig_program_manage_edit2 == 0)
        dig_program_manage_edit2 = new dialog_program_manage_edit();

    QString path = RM_PROG_PATH;
    path += "/";
    path += m_TpProgList[m_nTpSelectedIndex];

    dig_program_manage_edit2->wig_program->Init(path, true);
    dig_program_manage_edit2->exec();
}
void dialog_program_manage::onTpTool_Del()
{
    QString path = RM_PROG_PATH;
    path += "/";
    path += m_TpProgList[m_nTpSelectedIndex];

    qDebug() << "Del Path:" << path;

    CNRobo* pCon = CNRobo::getInstance();
    bool bExist = false;
    pCon->checkDirFileExist(path, bExist);

    qDebug() << bExist;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!bExist)
    {

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Error"));
        msg->Message(tr("Don't exist this file!"), path);
        msg->exec();
        return;
    }

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title("File Delete");
    conf->Message(tr("Would you want to Delete this file?"), path);
    if(conf->exec() != QDialog::Accepted) return;

    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title("File Delete");
    conf->Message(tr("Really, Would you want to Delete this file?"), path);
    if(conf->exec() != QDialog::Accepted) return;

    // work delete
    int ret = pCon->deleteDatabaseFile(path);
    if(ret < 0)
    {
        qDebug() << "deleteDatabaseFile ret=" << ret << path;
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("File Delete Error"));
        msg->Message(tr("Fail to delete this file!"), path);
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::BLUE);
    msg->Title(tr("File Delete"));
    msg->Message(tr("Success to delete this file!"), path);
    msg->exec();

    // re-make tp prog list & display.
    Update_Tp();
}

void dialog_program_manage::onTp2Db()
{
    QString prog_name = m_TpProgList[m_nTpSelectedIndex];

    QString source_path = RM_PROG_PATH;
    source_path += "/";
    source_path += prog_name;

    QString target_path = RM_RECIPE_PATH;
    target_path += "/";
    target_path += m_RecipeList[m_nRecipeSelectedIndex];
    target_path += RM_PROG_DIRNAME;
    target_path += "/";
    target_path += prog_name;

    qDebug() << prog_name;
    qDebug() << source_path << "=>" << target_path;

    QString str,_source,_target;
    _source = source_path;
    _target = target_path;
    str = _source.remove(RM_PROG_PATH);
    str += " >> ";
    str += _target.remove(RM_RECIPE_PATH);

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title("File Export");
    conf->Message(tr("Export File! (TP >> DB)"), str);
    if(conf->exec() != QDialog::Accepted) return;

    CNRobo* pCon = CNRobo::getInstance();

    qDebug() << prog_name;
    qDebug() << source_path << "=>" << target_path;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    QStringList prog;
    int ret;

    ret = pCon->getProgramFile(source_path, prog);
    if(ret < 0)
    {
        qDebug() << "getProgramFile ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Export Error"));
        msg->Message(tr("Fail to Export!"), str);
        msg->exec();
        return;
    }

    ret = pCon->createProgramFile(target_path, prog);
    if(ret < 0)
    {
        qDebug() << "createProgramFile ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Export Error"));
        msg->Message(tr("Fail to Export!"), str);
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::BLUE);
    msg->Title(tr("Export Success"));
    msg->Message(tr("Success to Export!"), str);
    msg->exec();

    // DB Update
    // re-make recipe prog list & display.
    Make_RecipeProgList(m_RecipeList[m_nRecipeSelectedIndex], m_RecipeProgList);
    m_nDbStartIndex=0;m_nDbSelectedIndex=0;
    Redraw_RecipeProgList(m_nDbStartIndex);
}

void dialog_program_manage::onDb2Tp()
{
    QString prog_name = m_RecipeProgList[m_nDbSelectedIndex];

    QString source_path = RM_RECIPE_PATH;
    source_path += "/";
    source_path += m_RecipeList[m_nRecipeSelectedIndex];
    source_path += RM_PROG_DIRNAME;
    source_path += "/";
    source_path += prog_name;

    QString target_path = RM_PROG_PATH;
    target_path += "/";
    target_path += prog_name;

    qDebug() << prog_name;
    qDebug() << source_path << "=>" << target_path;

    QString str,_source,_target;
    _source = source_path;
    _target = target_path;
    str = _source.remove(RM_RECIPE_PATH);
    str += " >> ";
    str += _target.remove(RM_PROG_PATH);

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title("File Import");
    conf->Message(tr("Import File! (DB >> TP)"), str);
    if(conf->exec() != QDialog::Accepted) return;

    CNRobo* pCon = CNRobo::getInstance();

    qDebug() << prog_name;
    qDebug() << source_path << "=>" << target_path;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    QStringList prog;
    int ret;

    ret = pCon->getProgramFile(source_path, prog);
    if(ret < 0)
    {
        qDebug() << "getProgramFile ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Import Error"));
        msg->Message(tr("Fail to Import!"), str);
        msg->exec();
        return;
    }

    ret = pCon->setProgramSteps(prog_name, prog);
    if(ret < 0)
    {
        qDebug() << "setProgramSteps ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Import Error"));
        msg->Message(tr("Fail to Import!"), str);
        msg->exec();
        return;
    }

    ret = pCon->saveProgram(prog_name);
    if(ret <0)
    {
        qDebug() << "saveProgram ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Import Error"));
        msg->Message(tr("Fail to Import!"), str);
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::BLUE);
    msg->Title(tr("Import Success"));
    msg->Message(tr("Success to Import!"), str);
    msg->exec();

    //TP Update
    // re-make tp prog list & display.
    Update_Tp();
}

/** USB **/
void dialog_program_manage::Update_Usb()
{
    bool bCheck = IsUsbPath();
    Display_UsbRefresh(bCheck);

    if(bCheck)
    {
        Make_UsbProgList(m_UsbProgList);
        m_nUsbStartIndex=0;m_nUsbSelectedIndex=0;
        Redraw_UsbProgList(m_nUsbStartIndex);
    }
}
bool dialog_program_manage::IsUsbPath()
{
    CNRobo* pCon = CNRobo::getInstance();
    bool bExist=false;
    int ret = pCon->checkDirFileExist(PM_USB_PROG_PATH, bExist);
    if(ret < 0)
    {
        qDebug() << "checkDirFileExist ret=" << ret;
        return false;
    }

    return bExist;
}
void dialog_program_manage::Display_UsbRefresh(bool bUsbPath)
{
    if(bUsbPath)
        ui->wigUsbRefresh->setGeometry(m_rectHide);
    else
        ui->wigUsbRefresh->setGeometry(m_rectShow);

    ui->wigUsbTool->setEnabled(bUsbPath);
    ui->wigTp2Usb->setEnabled(bUsbPath);
}

void dialog_program_manage::onUsbRefresh()
{
    Update_Usb();
}

void dialog_program_manage::Check_UsbOut()
{
    if(!ui->wigUsbTool->isEnabled())
        return;

    bool bCheck = IsUsbPath();
    if(!bCheck)
        Update_Usb();
}

bool dialog_program_manage::Make_UsbProgList(QStringList &prog_list)
{
    QString path = PM_USB_PROG_PATH;

    CNRobo* pCon = CNRobo::getInstance();

    prog_list.clear();
    int ret = pCon->getDatabaseFileList(path, prog_list);
    if(ret < 0)
    {
        prog_list.clear();
        qDebug() << "getDatabaseFileList() ret = " << ret << "path=" << path;
        return false;
    }

    prog_list.removeAll(".");
    prog_list.removeAll("..");

    return true;
}
void dialog_program_manage::Redraw_UsbProgList(int start_index)
{
    int cal_size = m_UsbProgList.size() - start_index;
    int index;
    QString prog,no;

    Display_UsbListOn(-1); // all off

    for(int i=0;i<m_vtUsbNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            prog = m_UsbProgList[index];
            no = QString().setNum(index+1);
        }
        else
        {
            // no have data.
            prog.clear();
            no.clear();
        }

        m_vtUsbProg[i]->setText(prog);
        m_vtUsbNo[i]->setText(no);

        if((start_index + i) == m_nUsbSelectedIndex)
        {
            // display
            Display_UsbListOn(i);
        }
    }
}
void dialog_program_manage::Display_UsbListOn(int table_index)
{
    for(int i=0;i<m_vtUsbNo.count();i++)
    {
        m_vtUsbNo[i]->setAutoFillBackground(i == table_index);
        m_vtUsbProg[i]->setAutoFillBackground(i == table_index);
    }
}

void dialog_program_manage::onUsbList()
{
    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtUsbNo.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nUsbStartIndex + index;
    if(list_index == m_nUsbSelectedIndex) return;

    if(list_index < m_UsbProgList.size())
    {
        m_nUsbSelectedIndex = list_index;
        Redraw_UsbProgList(m_nUsbStartIndex);
    }
}
void dialog_program_manage::onUsbList_Up()
{
    if(m_nUsbStartIndex <= 0)
        return;

    Redraw_UsbProgList(--m_nUsbStartIndex);
}
void dialog_program_manage::onUsbList_Down()
{
    if(m_nUsbStartIndex >= (m_UsbProgList.size() - m_vtUsbNo.size()))
        return;

    Redraw_UsbProgList(++m_nUsbStartIndex);
}

void dialog_program_manage::onUsbTool_Edit()
{
    if(dig_program_manage_edit3 == 0)
        dig_program_manage_edit3 = new dialog_program_manage_edit();

    QString path = PM_USB_PROG_PATH;
    path += "/";
    path += m_UsbProgList[m_nUsbSelectedIndex];

    dig_program_manage_edit3->wig_program->Init(path, true);
    dig_program_manage_edit3->exec();
}
void dialog_program_manage::onUsbTool_Del()
{
    QString path = PM_USB_PROG_PATH;
    path += "/";
    path += m_UsbProgList[m_nUsbSelectedIndex];

    qDebug() << "Del Path:" << path;

    CNRobo* pCon = CNRobo::getInstance();
    bool bExist = false;
    pCon->checkDirFileExist(path, bExist);

    qDebug() << bExist;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!bExist)
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Error"));
        msg->Message(tr("Don't exist this file!"), path);
        msg->exec();
        return;
    }

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title("File Delete");
    conf->Message(tr("Would you want to Delete this file?"), path);
    if(conf->exec() != QDialog::Accepted) return;

    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title("File Delete");
    conf->Message(tr("Really, Would you want to Delete this file?"), path);
    if(conf->exec() != QDialog::Accepted) return;

    // work delete
    int ret = pCon->deleteDatabaseFile(path);
    if(ret < 0)
    {
        qDebug() << "deleteDatabaseFile ret=" << ret << path;
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("File Delete Error"));
        msg->Message(tr("Fail to delete this file!"), path);
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::BLUE);
    msg->Title(tr("File Delete"));
    msg->Message(tr("Success to delete this file!"), path);
    msg->exec();

    // re-make tp prog list & display.
    Update_Usb();
}

void dialog_program_manage::onTp2Usb()
{
    if(!ui->wigUsbTool->isEnabled())
        return;

    QString prog_name = m_TpProgList[m_nTpSelectedIndex];

    QString source_path = RM_PROG_PATH;
    source_path += "/";
    source_path += prog_name;

    QString target_path = PM_USB_PROG_PATH;
    target_path += "/";
    target_path += prog_name;

    qDebug() << prog_name;
    qDebug() << source_path << "=>" << target_path;

    QString str,_source,_target;
    _source = source_path;
    _target = target_path;
    str = _source.remove(RM_PROG_PATH);
    str += " >> ";
    str += _target.remove(PM_USB_PROG_PATH);

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title("File Export");
    conf->Message(tr("Export File! (TP >> USB)"), str);
    if(conf->exec() != QDialog::Accepted) return;

    CNRobo* pCon = CNRobo::getInstance();

    qDebug() << prog_name;
    qDebug() << source_path << "=>" << target_path;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    QStringList prog;
    int ret;

    ret = pCon->getProgramFile(source_path, prog);
    if(ret < 0)
    {
        qDebug() << "getProgramFile ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Export Error"));
        msg->Message(tr("Fail to Export!"), str);
        msg->exec();
        return;
    }

    ret = pCon->createProgramFile(target_path, prog);
    if(ret < 0)
    {
        qDebug() << "createProgramFile ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Export Error"));
        msg->Message(tr("Fail to Export!"), str);
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::BLUE);
    msg->Title(tr("Export Success"));
    msg->Message(tr("Success to Export!"), str);
    msg->exec();

    // re-make usb prog list & display.
    Update_Usb();
}

void dialog_program_manage::onUsb2Tp()
{
    if(!ui->wigUsbTool->isEnabled())
        return;

    QString prog_name = m_UsbProgList[m_nUsbSelectedIndex];

    QString source_path = PM_USB_PROG_PATH;
    source_path += "/";
    source_path += prog_name;

    QString target_path = RM_PROG_PATH;
    target_path += "/";
    target_path += prog_name;

    qDebug() << prog_name;
    qDebug() << source_path << "=>" << target_path;

    QString str,_source,_target;
    _source = source_path;
    _target = target_path;
    str = _source.remove(PM_USB_PROG_PATH);
    str += " >> ";
    str += _target.remove(RM_PROG_PATH);

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::BLUE);
    conf->Title("File Import");
    conf->Message(tr("Import File! (USB >> TP)"), str);
    if(conf->exec() != QDialog::Accepted) return;

    CNRobo* pCon = CNRobo::getInstance();

    qDebug() << prog_name;
    qDebug() << source_path << "=>" << target_path;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
    QStringList prog;
    int ret;

    ret = pCon->getProgramFile(source_path, prog);
    if(ret < 0)
    {
        qDebug() << "getProgramFile ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Import Error"));
        msg->Message(tr("Fail to Import!"), str);
        msg->exec();
        return;
    }

    ret = pCon->setProgramSteps(prog_name, prog);
    if(ret < 0)
    {
        qDebug() << "createProgramFile ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Import Error"));
        msg->Message(tr("Fail to Import!"), str);
        msg->exec();
        return;
    }

    ret = pCon->saveProgram(prog_name);
    if(ret <0)
    {
        qDebug() << "saveProgram ret=" << ret;
        qDebug() << source_path << "<<" << target_path;

        msg->SetColor(dialog_message::RED);
        msg->Title(tr("Import Error"));
        msg->Message(tr("Fail to Import!"), str);
        msg->exec();
        return;
    }

    msg->SetColor(dialog_message::BLUE);
    msg->Title(tr("Import Success"));
    msg->Message(tr("Success to Import!"), str);
    msg->exec();

    // re-make tp prog list & display.
    Update_Tp();
}
