#ifndef DIALOG_MOLDCLEAN_H
#define DIALOG_MOLDCLEAN_H

#include <QDialog>
#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_mode_select.h"


namespace Ui {
class dialog_moldclean;
}

class dialog_moldclean : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_moldclean(QWidget *parent = 0);
    ~dialog_moldclean();

    void Update();

public slots:
    void onClose();

    void onUse();
    void onValue();

private:
    Ui::dialog_moldclean *ui;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;
    QStringList m_ItemName;

    QVector<QLabel4*> m_vtValPic;
    QVector<QLabel3*> m_vtVal;
    QVector<QLabel3*> m_vtValName;

    dialog_mode_select* sel;
    dialog_numpad* np;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_MOLDCLEAN_H
