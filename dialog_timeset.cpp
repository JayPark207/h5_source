#include "dialog_timeset.h"
#include "ui_dialog_timeset.h"

dialog_timeset::dialog_timeset(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_timeset)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    // confirm button event
    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(reject()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    m_vtUp.clear();
    m_vtUp.append(ui->btnUpPic);
    m_vtUp.append(ui->btnUpPic_2);
    m_vtUp.append(ui->btnUpPic_3);
    m_vtUp.append(ui->btnUpPic_4);
    m_vtUp.append(ui->btnUpPic_5);
    m_vtUp.append(ui->btnUpPic_6);

    m_vtDn.clear();
    m_vtDn.append(ui->btnDownPic);
    m_vtDn.append(ui->btnDownPic_2);
    m_vtDn.append(ui->btnDownPic_3);
    m_vtDn.append(ui->btnDownPic_4);
    m_vtDn.append(ui->btnDownPic_5);
    m_vtDn.append(ui->btnDownPic_6);

    m_vtUpIcon.clear();
    m_vtUpIcon.append(ui->btnUpIcon);
    m_vtUpIcon.append(ui->btnUpIcon_2);
    m_vtUpIcon.append(ui->btnUpIcon_3);
    m_vtUpIcon.append(ui->btnUpIcon_4);
    m_vtUpIcon.append(ui->btnUpIcon_5);
    m_vtUpIcon.append(ui->btnUpIcon_6);

    m_vtDnIcon.clear();
    m_vtDnIcon.append(ui->btnDownIcon);
    m_vtDnIcon.append(ui->btnDownIcon_2);
    m_vtDnIcon.append(ui->btnDownIcon_3);
    m_vtDnIcon.append(ui->btnDownIcon_4);
    m_vtDnIcon.append(ui->btnDownIcon_5);
    m_vtDnIcon.append(ui->btnDownIcon_6);

    for(int i=0;i<m_vtUp.count();i++)
    {
        connect(m_vtUp[i],SIGNAL(mouse_release()),this,SLOT(onUp()));
        connect(m_vtUpIcon[i],SIGNAL(mouse_press()),m_vtUp[i],SLOT(press()));
        connect(m_vtUpIcon[i],SIGNAL(mouse_release()),m_vtUp[i],SLOT(release()));

        connect(m_vtDn[i],SIGNAL(mouse_release()),this,SLOT(onDown()));
        connect(m_vtDnIcon[i],SIGNAL(mouse_press()),m_vtDn[i],SLOT(press()));
        connect(m_vtDnIcon[i],SIGNAL(mouse_release()),m_vtDn[i],SLOT(release()));
    }


}

dialog_timeset::~dialog_timeset()
{
    delete ui;
}
void dialog_timeset::showEvent(QShowEvent *)
{
    timer->start(500);

    Update();
}
void dialog_timeset::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_timeset::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_timeset::onOK()
{
    Save();
    accept();
}

void dialog_timeset::onTimer()
{
    ui->lbDate->setText(Status->strDate);
    ui->lbTime->setText(Status->strTime);
}

void dialog_timeset::Update()
{
    CNRobo* pCon = CNRobo::getInstance();
    QString str;
    pCon->getDateTime(str);
    m_DateTime = QDateTime::fromString(str, Qt::ISODate);

    Display();
}

void dialog_timeset::Display()
{
    QString str;
    str.sprintf("%04d",m_DateTime.date().year());
    ui->tbValue->setText(str);
    str.sprintf("%02d",m_DateTime.date().month());
    ui->tbValue_2->setText(str);
    str.sprintf("%02d",m_DateTime.date().day());
    ui->tbValue_3->setText(str);

    str.sprintf("%02d",m_DateTime.time().hour());
    ui->tbValue_4->setText(str);
    str.sprintf("%02d",m_DateTime.time().minute());
    ui->tbValue_5->setText(str);
    str.sprintf("%02d",m_DateTime.time().second());
    ui->tbValue_6->setText(str);

    qDebug() << m_DateTime.toString(Qt::ISODate);
}

void dialog_timeset::onUp()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtUp.indexOf(btn);
    if(index < 0) return;

    switch(index)
    {
    case 0: // year
        m_DateTime = m_DateTime.addYears(1);
        break;
    case 1: // month
        m_DateTime = m_DateTime.addMonths(1);
        break;
    case 2: // day
        m_DateTime = m_DateTime.addDays(1);
        break;
    case 3: // hour
        m_DateTime = m_DateTime.addSecs(3600);
        break;
    case 4: // minute
        m_DateTime = m_DateTime.addSecs(60);
        break;
    case 5: // second
        m_DateTime = m_DateTime.addSecs(1);
        break;
    }

    Display();
}
void dialog_timeset::onDown()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtDn.indexOf(btn);
    if(index < 0) return;

    switch(index)
    {
    case 0: // year
        m_DateTime = m_DateTime.addYears(-1);
        break;
    case 1: // month
        m_DateTime = m_DateTime.addMonths(-1);
        break;
    case 2: // day
        m_DateTime = m_DateTime.addDays(-1);
        break;
    case 3: // hour
        m_DateTime = m_DateTime.addSecs(-3600);
        break;
    case 4: // minute
        m_DateTime = m_DateTime.addSecs(-60);
        break;
    case 5: // second
        m_DateTime = m_DateTime.addSecs(-1);
        break;
    }

    Display();
}

void dialog_timeset::Save()
{
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setDateTime(m_DateTime.toString(Qt::ISODate));

    qDebug() << m_DateTime.toString(Qt::ISODate);
}

