#ifndef WIDGET_STEPEDIT_PROGRAMRUN_H
#define WIDGET_STEPEDIT_PROGRAMRUN_H

#include <QWidget>
#include "global.h"

#include "qlabel3.h"

namespace Ui {
class widget_stepedit_programrun;
}

class widget_stepedit_programrun : public QWidget
{
    Q_OBJECT

public:
    explicit widget_stepedit_programrun(QWidget *parent = 0);
    ~widget_stepedit_programrun();

    void Init(QString prg_name);

    void Read(QString prg_name);
    void Redraw(int start_index);
    void CyclicUpdate_Index(int run_index, int plan_index, CNR_TASK_STATUS task_status);
    void SetIcon(int line, CNR_TASK_STATUS task_status);

private:
    Ui::widget_stepedit_programrun *ui;

    QString m_strPrgName;
    int m_nRunIndex;
    int m_nMaxIndex;
    int m_nStartIndex;
    QStringList m_Program;

    QVector<QLabel3*> m_vtIcon;
    QVector<QLabel3*> m_vtNo;
    QVector<QLabel3*> m_vtText;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // WIDGET_STEPEDIT_PROGRAMRUN_H
