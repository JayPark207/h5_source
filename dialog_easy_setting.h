#ifndef DIALOG_EASY_SETTING_H
#define DIALOG_EASY_SETTING_H

#include <QDialog>
#include <QTimer>
#include <QStackedWidget>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_delaying.h"
#include "dialog_home2.h"
#include "dialog_message.h"
#include "widget_jogmodule4.h"
#include "dialog_confirm.h"
#include "dialog_easy_setting_io.h"

#include "top_sub.h"

#define EASYSET_DELAY_MOVE  20   // 50ms * 20 = 1000 msec
#define EASYSET_DELAY_NEXT  5    // 50ms * 5 = 250 msec

#define EASYSET_SPEED_MAX   80 // [%] max speed.
#define EASYSET_SPEED_MIN   5
#define EASYSET_SPEED_GAP   5   // [%] up/down gap
#define EASYSET_SPEED_INIT  30  // [%] init speed.(set low speed for safety)

namespace Ui {
class dialog_easy_setting;
}

class dialog_easy_setting : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_easy_setting(QWidget *parent = 0);
    ~dialog_easy_setting();

    void Update();

    void Init_NoSetIndexList();


private:
    Ui::dialog_easy_setting *ui;
    QTimer* timer;
    QTimer* fast_timer;
    dialog_easy_setting_io* dig_easy_setting_io;

    // key variable
    int  m_nStateIndexComp, m_nStateBtnPress;
    int  m_nBtnPressCnt;
    bool m_bMoving, m_bEnableON;    //from coreserver
    bool m_bIndexComp_Fwd, m_bIndexComp_Bwd;              //making signal
    bool m_bBtn_Fwd, m_bBtn_Bwd;
    int m_nIndex;

    // display
    void Display_Moving(bool on);
    void Display_EnableON(bool on);
    void Display_IndexComp(bool fwd, bool bwd);

    QVector<QLabel3*> m_vtTbIndex;
    void Redraw_IndexTable(int index);
    void Redraw_SetPos(int index);

    QVector<QLabel3*> m_vtSet, m_vtReal;
    cn_trans m_RealTrans; cn_joint m_RealJoint;
    void Display_RealPos(float* t, float* j);
    void Make_RealPos(float* t, float* j);

    cn_trans m_Trans; cn_joint m_Joint;
    void Display_SetPos(cn_trans t, cn_joint j);
    bool SavePos(int index, cn_trans t, cn_joint j);
    bool CopyPos(HyPos::ENUM_POS_ARRAY source = HyPos::WAIT,
                 HyPos::ENUM_POS_ARRAY target = HyPos::UP1ST);

    // action displays...
    QVector<QLabel3*> m_vtFwding, m_vtBwding;
    int m_nFwdBwding;
    void Display_FwdBwding(bool fwd, bool bwd);
    QVector<QLabel3*> m_vtFwding2, m_vtBwding2;
    int m_nFwdBwding2;
    void Display_FwdBwding2(bool fwd, bool bwd);
    QVector<QPixmap> pxmFwding,pxmBwding;
    void Display_FwdBwding2_1(bool fwd, bool bwd);

    QStackedWidget* SidePanel;
    widget_jogmodule4* wig_jogmodule;
    void Display_BtnJog(bool on);
    void Enter_Jog(bool enter);
    bool IsJog();

    void Display_Speed(float speed);

    // robot control
    bool ServoOnMode(int mode);
    void SetEnableON(bool on);
    bool Move(QString PosName);
    float m_Speed;
    void Speed(float speed);
    float Speed();

    // timer functions
    void Process_IndexComp();
    void Process_BtnPress();

    // test temporary
    dialog_delaying* dig_delaying;
    void Delay(int msec);

    // make position vector
    QVector<int> m_IndexList, m_NoSetIndexList;
    QStringList m_PosList, m_DispList;
    bool Make_PosList(QStringList& pos_list, QStringList& disp_list, QVector<int>& index_list);   // boss
    bool Save_StopoverPos(int StopoverNum = 0,            // 0 = Stopover1, 1 = Stopover2
                          HyPos::ENUM_POS_ARRAY start_index = HyPos::WAIT,
                          HyPos::ENUM_POS_ARRAY end_index = HyPos::TAKE_OUT); // no use

    bool Save_StopoverPos1(cn_trans real_trans, cn_joint real_joint,
                           HyPos::ENUM_POS_ARRAY end_index = HyPos::TAKE_OUT);
    bool ASave_StopoverPos1(cn_trans real_trans, cn_joint real_joint,
                            int end_index = HyPos::TAKE_OUT, bool is_bwd = false); // is_bwd false: cal fwd timing, true : cal bwd timing.
    bool m_bStopover1;

    bool Save_StopoverPos2(cn_trans real_trans, cn_joint real_joint,
                           HyPos::ENUM_POS_ARRAY end_index = HyPos::UP1ST);
    bool ASave_StopoverPos2(cn_trans real_trans, cn_joint real_joint,
                            int end_index = HyPos::UP1ST, bool is_bwd = false);
    bool m_bStopover2;

    QString toPosName_Trans(int index);
    QString toPosName_Joint(int index);

    QString toDispName(int index);

    dialog_home2* dig_home2;
    bool Home(bool bCheck);

    // enaable control
    void Enable_Control(bool on);
    void Enable_BtnSet(bool on);
    void Enable_BtnJog(bool on);

    bool m_bFirstEntry;
    void Process_FirstEntry();

    top_sub* top;
    void SubTop();          // Need SubTop

    bool m_bError;

    bool m_bDualSolOff;

public slots:
    void onClose();
    void onTimer();
    void onFastTimer();

    void onFwd_Press();
    void onFwd_Release();
    void onBwd_Press();
    void onBwd_Release();

    void onHome();

    void onJog();
    void onSet();

    void onSpdUp();
    void onSpdDown();

    void onIO();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_EASY_SETTING_H
