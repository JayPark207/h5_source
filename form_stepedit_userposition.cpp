#include "form_stepedit_userposition.h"
#include "ui_form_stepedit_userposition.h"

form_stepedit_userposition::form_stepedit_userposition(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::form_stepedit_userposition)
{
    ui->setupUi(this);

    // confirm button event
    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // button group - match mode enum
    StackedBtn = new QStackedWidget(this);
    StackedBtn->setGeometry(ui->wigUser->geometry());

    StackedBtn->addWidget(ui->wigUser);
    StackedBtn->addWidget(ui->wigWork);
    StackedBtn->show();

    m_vtType.clear();
    m_vtType.append(ui->lbType);
    m_vtType.append(ui->lbType_2);

    connect(ui->btnTypePic,SIGNAL(mouse_release()),this,SLOT(onType()));
    connect(ui->btnTypeIcon,SIGNAL(mouse_press()),ui->btnTypePic,SLOT(press()));
    connect(ui->btnTypeIcon,SIGNAL(mouse_release()),ui->btnTypePic,SLOT(release()));

    // USERPOS
    connect(ui->btnUser_TeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnUser_Teach,SIGNAL(mouse_press()),ui->btnUser_TeachPic,SLOT(press()));
    connect(ui->btnUser_Teach,SIGNAL(mouse_release()),ui->btnUser_TeachPic,SLOT(release()));
    connect(ui->btnUser_TeachIcon,SIGNAL(mouse_press()),ui->btnUser_TeachPic,SLOT(press()));
    connect(ui->btnUser_TeachIcon,SIGNAL(mouse_release()),ui->btnUser_TeachPic,SLOT(release()));


    // USERWORK
    connect(ui->btnWork_TeachPic,SIGNAL(mouse_release()),this,SLOT(onTeach()));
    connect(ui->btnWork_Teach,SIGNAL(mouse_press()),ui->btnWork_TeachPic,SLOT(press()));
    connect(ui->btnWork_Teach,SIGNAL(mouse_release()),ui->btnWork_TeachPic,SLOT(release()));
    connect(ui->btnWork_TeachIcon,SIGNAL(mouse_press()),ui->btnWork_TeachPic,SLOT(press()));
    connect(ui->btnWork_TeachIcon,SIGNAL(mouse_release()),ui->btnWork_TeachPic,SLOT(release()));

    connect(ui->btnWork_PatPic,SIGNAL(mouse_release()),this,SLOT(onPattern()));
    connect(ui->btnWork_Pat,SIGNAL(mouse_press()),ui->btnWork_PatPic,SLOT(press()));
    connect(ui->btnWork_Pat,SIGNAL(mouse_release()),ui->btnWork_PatPic,SLOT(release()));
    connect(ui->btnWork_PatIcon,SIGNAL(mouse_press()),ui->btnWork_PatPic,SLOT(press()));
    connect(ui->btnWork_PatIcon,SIGNAL(mouse_release()),ui->btnWork_PatPic,SLOT(release()));

    connect(ui->btnWork_RefreshPic,SIGNAL(mouse_release()),this,SLOT(onRefresh()));
    connect(ui->btnWork_Refresh,SIGNAL(mouse_press()),ui->btnWork_RefreshPic,SLOT(press()));
    connect(ui->btnWork_Refresh,SIGNAL(mouse_release()),ui->btnWork_RefreshPic,SLOT(release()));
    connect(ui->btnWork_RefreshIcon,SIGNAL(mouse_press()),ui->btnWork_RefreshPic,SLOT(press()));
    connect(ui->btnWork_RefreshIcon,SIGNAL(mouse_release()),ui->btnWork_RefreshPic,SLOT(release()));


#ifdef _H6_
    ui->lbJ6->setText(tr("J6"));

    ui->lbJ6->setEnabled(true);
    ui->lbJ6val->setEnabled(true);
#else
    ui->lbJ6->setText("");

    ui->lbJ6->setEnabled(false);
    ui->lbJ6val->setEnabled(false);
#endif

    // clear point
    dig_sasang_pattern = 0;
}

form_stepedit_userposition::~form_stepedit_userposition()
{
    delete ui;
}
void form_stepedit_userposition::showEvent(QShowEvent *)
{
    Update();
    Display_Info();
}
void form_stepedit_userposition::hideEvent(QHideEvent *)
{

}
void form_stepedit_userposition::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void form_stepedit_userposition::onClose()
{
    emit sigClose();
}

void form_stepedit_userposition::Init(int select_index, int step_index, PAGE_MODE mode, QString title)
{
    m_nSelectedIndex = select_index;
    m_nStepIndex = step_index;
    m_PageMode = mode;
    m_strTitle = title;
}

void form_stepedit_userposition::Update()
{
    Display_BtnGroup(m_PageMode);
    Display_Data();
}

void form_stepedit_userposition::Display_BtnGroup(PAGE_MODE mode)
{
    StackedBtn->setCurrentIndex((int)mode);
}

void form_stepedit_userposition::Display_Data()
{
    cn_trans trans;
    cn_joint joint;
    float speed, delay, type;

    switch(m_PageMode)
    {
    case USERPOS:
        ST_USERDATA_POS pos;
        UserData->SetBuf_Default(&pos);

        if(!UserData->RD((m_nStepIndex - HyStepData::ADD_POS0), &pos))
            return;

        trans = pos.Trans;
        joint = pos.Joint;
        speed = pos.Speed;
        delay = pos.Delay;
        type  = pos.Type;
        m_UserPos = pos;
        break;

    case USERWORK:
        ST_USERDATA_WORK work;

        UserData->SetBuf_Default(&work);

        if(!UserData->RD((m_nStepIndex - HyStepData::ADD_WORK0), &work))
            return;

        trans = work.Trans;
        joint = work.Joint;
        speed = work.Speed;
        delay = work.Delay;
        type  = work.Type;
        m_UserWork = work;
        break;

    default:
        return;
    }

    Display_SetPos(trans, joint);
    Display_Spd(speed);
    Display_Delay(delay);
    Display_Type(type);
}

void form_stepedit_userposition::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    ui->lbXval->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    ui->lbYval->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    ui->lbZval->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    ui->lbRval->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, j));
    ui->lbSval->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, j));
    ui->lbJ6val->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    ui->lbXval->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    ui->lbYval->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    ui->lbZval->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    ui->lbRval->setText(str);
    str.sprintf(FORMAT_POS, j.joint[JSWV]);
    ui->lbSval->setText(str);

    ui->lbJ6val->setText("");
#endif

}
void form_stepedit_userposition::Display_Spd(float speed)
{
    QString str;
    str.sprintf(FORMAT_SPEED, speed);
    ui->btnSpd->setText(str);
    ui->lbSpdVal->setText(str);
}
void form_stepedit_userposition::Display_Delay(float delay)
{
    QString str;
    str.sprintf(FORMAT_TIME, delay);
    ui->btnDelay->setText(str);
    ui->lbDelayVal->setText(str);
}
void form_stepedit_userposition::Display_Type(float type)
{
    for(int i=0;i<m_vtType.count();i++)
        m_vtType[i]->setEnabled((int)type == i);
}

void form_stepedit_userposition::Display_Info()
{
    ui->lbTitle->setText(m_strTitle);
    ui->lbCurrStepNo->setText(QString().setNum(m_nSelectedIndex + 1));
    ui->lbCurrName->setText(StepEdit->MakeDispName(m_nSelectedIndex));
}


bool form_stepedit_userposition::SaveType(float type)
{
    if(type < 0) type = 0;
    if(type > 1.0) type = 1.0;

    switch(m_PageMode)
    {
    case USERPOS:
        m_UserPos.Type = type;
        if(!UserData->WR((m_nStepIndex - HyStepData::ADD_POS0), m_UserPos))
            return false;
        break;

    case USERWORK:
        m_UserWork.Type = type;
        if(!UserData->WR((m_nStepIndex - HyStepData::ADD_WORK0), m_UserWork))
            return false;
        break;
    }

    return true;
}

void form_stepedit_userposition::onType()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title("Confirm");
    conf->Message(QString(tr("Would you want to change type?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    for(int i=0;i<m_vtType.count();i++)
    {
        if(!m_vtType[i]->isEnabled())
            SaveType((float)i);
    }

    Display_Data();
}

void form_stepedit_userposition::onTeach()
{
    dialog_position_teaching2* dig = (dialog_position_teaching2*)gGetDialog(DIG_POS_TEACH);

    switch(m_PageMode)
    {
    case USERPOS:
        dig->Init(dialog_position_teaching2::USER, (m_nStepIndex - HyStepData::ADD_POS0));
        break;

    case USERWORK:
        dig->Init(dialog_position_teaching2::WORK, (m_nStepIndex - HyStepData::ADD_WORK0));
        break;

    default:
        return;
    }

    dig->exec();

    Display_Data();
}

void form_stepedit_userposition::onPattern()
{
    QString group_name = StepEdit->m_StepData.Step[m_nStepIndex].NickName;
    qDebug() << group_name;

    if(group_name.isEmpty()) return;

    ST_SSGROUP_DATA data;
    data.name = group_name;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(Sasang->Group->ReadFile(data) < 0)
    {
        // error case.
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Read Group Data!")),
                     QString(tr("Please, Check Nickname and Try again!")));
        msg->exec();
        return;
    }

    if(!Sasang->Group2Patterns(data, Sasang->Pattern->PatternList))
    {
        // error case.
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Transfer Pattern Data!")),
                     QString(tr("Please, Try again!")));
        msg->exec();
        return;
    }

    if(dig_sasang_pattern == 0)
        dig_sasang_pattern = new dialog_sasang_pattern();

    dig_sasang_pattern->Init(data, true);
    if(dig_sasang_pattern->exec() != QDialog::Accepted)
        return;

    data = dig_sasang_pattern->GetGrpData();

    /* // not use dialog close save.
    if(!Sasang->Patterns2Group(Sasang->Pattern->PatternList, data))
    {
        // error case.
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Transfer Group Data!")),
                     QString(tr("Please, Try again!")));
        msg->exec();
        return;
    }

    if(Sasang->Group->SaveFile(data) < 0)
    {
        // error case.
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Save Group Data!")),
                     QString(tr("Please, Try again!")));
        msg->exec();
        return;
    }*/

    // work update function
    QStringList temp_macro;
    Sasang->Group->MakingOneProgram(data, temp_macro);
    if(!StepEdit->Make_UserWork(StepEdit->m_StepData.Step[m_nStepIndex], temp_macro))
    {
        // error case.
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Update!")),
                     QString(tr("Please, Try again!")));
        msg->exec();
        return;
    }

    qDebug() << "Success Group data save & update.";

}

void form_stepedit_userposition::onRefresh()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title("Confirm");
    conf->Message(QString(tr("Would you want to Refresh This Work?")));
    if(conf->exec() != QDialog::Accepted)
        return;

    QString group_name = StepEdit->m_StepData.Step[m_nStepIndex].NickName;
    qDebug() << group_name;

    if(group_name.isEmpty()) return;

    ST_SSGROUP_DATA data;
    data.name = group_name;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(Sasang->Group->ReadFile(data) < 0)
    {
        // error case.
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Read Group Data!")),
                     QString(tr("Please, Check Nickname and Try again!")));
        msg->exec();
        return;
    }

    // work update function
    QStringList temp_macro;
    Sasang->Group->MakingOneProgram(data, temp_macro);
    if(!StepEdit->Make_UserWork(StepEdit->m_StepData.Step[m_nStepIndex], temp_macro))
    {
        // error case.
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to Refresh!")),
                     QString(tr("Please, Try again!")));
        msg->exec();
        return;
    }

    msg = (dialog_message*)gGetDialog(DIG_MSG);
    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(QString(tr("Success")));
    msg->Message(QString(tr("Success to Refresh!")),
                 group_name);
    msg->exec();
}
