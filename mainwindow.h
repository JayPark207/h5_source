#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>

#include "global.h"

#include "page_main2.h"

#include "top_main.h"
#include "top_speed.h"
#include "top_sub.h"

#include "form_autorun.h"
#include "form_specialsetting.h"

#include "dialog_mode_select.h"
#include "dialog_numpad.h"
#include "dialog_position_teaching2.h"
#include "dialog_confirm.h"
#include "dialog_message.h"
#include "dialog_keyboard.h"
#include "dialog_home.h"
#include "dialog_error.h"
#include "dialog_userlevel.h"

#include "dialog_home2.h"
#include "dialog_jog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    // main forms..
    QStackedWidget*         StackedTop;
    top_main*               topMain;
    top_speed*              topSpeed;


    QStackedWidget*         StackedForm;
    page_main2*             pageMain2;
    form_autorun*           frmAutoRun;
    form_specialsetting*    frmSpecialSetting;

    void Init();
    void Init_Forms();
    void Init_Dialogs();
    void Init_Values();

    void Init_SubMain();
    void Init_Variable();

    // dialogs...(many use dialogs)
    QVector<QDialog*>       Dialogs;
    dialog_mode_select*     digModeSelect;
    dialog_numpad*          digNumpad;
    dialog_position_teaching2* digPosTeach;
    dialog_confirm*         digConfirm;
    dialog_message*         digMsg;
    dialog_keyboard*        digKeyboard;
    dialog_home*            digHome;
    dialog_error*           digError;
    dialog_userlevel*       digUserLevel;

public slots:

    void OnStatckedForm_CurrentChanged(int index);

    void onWatchDog();
    void onReConnect();
    void onErrCheck();

    void onFirstRun();

    void onTimer_Pendent();

public: //Pendent(Key & Led) Control.
    bool ActiveModalClose(int check_max_num);
    void Key_Reset();
    void Key_OpenAuto();
    void Key_OpenHome();
    void Key_Jog(bool onoff);
    bool IsOpenDialog(QString dialog_name);

    void Key_StartPause();

    void Process_PendentLED();
    bool m_bLed1,m_bLed1_old;
    bool m_bLed2,m_bLed2_old;
    bool m_bLed3,m_bLed3_old;

    void Key_Speed(bool updown); // false:down, true:up

    //exection.
    bool IsMacroRun();


    dialog_home2* dig_home2;
    dialog_jog* dig_jog;

public:
    Ui::MainWindow *ui;

    QTimer* WatchDog;       // watchdog + clock update.
    QTimer* ReConnect;
    QTimer* tErrCheck;
    QTimer* tFirstRun;
    QTimer* tPendent;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

private:
    int m_StateReConnect;

    QElapsedTimer  timer;
    QElapsedTimer tUserLevel;

    bool m_bError,m_bErrorOld;
    QString m_strRecipeName;

    QVector<int> m_vtHomeBroken_ErrorCode;
    void Init_HomeBroken_ErrorCode();
    bool IsHomeBroken(int err_code);

};

#endif // MAINWINDOW_H
