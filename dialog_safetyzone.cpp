#include "dialog_safetyzone.h"
#include "ui_dialog_safetyzone.h"

dialog_safetyzone::dialog_safetyzone(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_safetyzone)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    // stacked widget. (for picture per page)
    pic_page = new QStackedWidget(this);
    pic_page->setGeometry(ui->widget->x(),ui->widget->y(),
                         ui->widget->width(),ui->widget->height());

    pic_page->addWidget(ui->widget);
    pic_page->addWidget(ui->widget_2);
    pic_page->addWidget(ui->widget_3);
    pic_page->addWidget(ui->widget_4);
    pic_page->setCurrentIndex(0);

    connect(ui->btnNextPic,SIGNAL(mouse_release()),this,SLOT(onNext()));
    connect(ui->btnNext,SIGNAL(mouse_press()),ui->btnNextPic,SLOT(press()));
    connect(ui->btnNext,SIGNAL(mouse_release()),ui->btnNextPic,SLOT(release()));

    connect(ui->btnBackPic,SIGNAL(mouse_release()),this,SLOT(onBack()));
    connect(ui->btnBack,SIGNAL(mouse_press()),ui->btnBackPic,SLOT(press()));
    connect(ui->btnBack,SIGNAL(mouse_release()),ui->btnBackPic,SLOT(release()));

    connect(ui->btnManualPic,SIGNAL(mouse_release()),this,SLOT(onManual()));
    connect(ui->btnManual,SIGNAL(mouse_press()),ui->btnManualPic,SLOT(press()));
    connect(ui->btnManual,SIGNAL(mouse_release()),ui->btnManualPic,SLOT(release()));
    connect(ui->btnManualIcon,SIGNAL(mouse_press()),ui->btnManualPic,SLOT(press()));
    connect(ui->btnManualIcon,SIGNAL(mouse_release()),ui->btnManualPic,SLOT(release()));

    m_vtPBtn.clear();             m_vtPVal.clear();
    m_vtPBtn.append(ui->tbXPic);  m_vtPVal.append(ui->tbX);
    m_vtPBtn.append(ui->tbYPic);  m_vtPVal.append(ui->tbY);
    m_vtPBtn.append(ui->tbZPic);  m_vtPVal.append(ui->tbZ);
    m_vtPBtn.append(ui->tbXPic_2);m_vtPVal.append(ui->tbX_2);
    m_vtPBtn.append(ui->tbYPic_2);m_vtPVal.append(ui->tbY_2);
    m_vtPBtn.append(ui->tbZPic_2);m_vtPVal.append(ui->tbZ_2);

    m_vtRowTitle.clear();
    m_vtRowTitle.append(ui->tbXTitle);
    m_vtRowTitle.append(ui->tbYTitle);
    m_vtRowTitle.append(ui->tbZTitle);

    int i;
    for(i=0;i<m_vtPBtn.count();i++)
    {
        connect(m_vtPBtn[i],SIGNAL(mouse_release()),this,SLOT(onPBtn()));
        connect(m_vtPVal[i],SIGNAL(mouse_press()),m_vtPBtn[i],SLOT(press()));
        connect(m_vtPVal[i],SIGNAL(mouse_release()),m_vtPBtn[i],SLOT(release()));
    }

    connect(ui->btnP1SetPic,SIGNAL(mouse_release()),this,SLOT(onP1()));
    connect(ui->btnP1SetIcon,SIGNAL(mouse_press()),ui->btnP1SetPic,SLOT(press()));
    connect(ui->btnP1SetIcon,SIGNAL(mouse_release()),ui->btnP1SetPic,SLOT(release()));

    connect(ui->btnP2SetPic,SIGNAL(mouse_release()),this,SLOT(onP2()));
    connect(ui->btnP2SetIcon,SIGNAL(mouse_press()),ui->btnP2SetPic,SLOT(press()));
    connect(ui->btnP2SetIcon,SIGNAL(mouse_release()),ui->btnP2SetPic,SLOT(release()));

    // pointer & varible clear.
    dig_manual = 0;

    ST_SZ_DATA sz_data;
    sz_data.p1.x = 0.0;sz_data.p2.x = 0.0;
    sz_data.p1.y = 0.0;sz_data.p2.y = 0.0;
    sz_data.p1.z = 0.0;sz_data.p2.z = 0.0;
    m_PData.clear();
    m_PData.fill(sz_data, pic_page->count());

}

dialog_safetyzone::~dialog_safetyzone()
{
    delete ui;
}

void dialog_safetyzone::showEvent(QShowEvent *)
{
    Update();
    Update_Page(0);

    timer->start();
}
void dialog_safetyzone::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_safetyzone::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_safetyzone::onOK()
{
    if(!SetSafetyZone())
    {
        // Exception Message.
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Safety Zone Setting is wrong!")),
                     QString(tr("Plase Check Setting Data.")));
        msg->exec();
        return;
    }

    CNRobo* pCon = CNRobo::getInstance();
    pCon->setWZCheckFlag(m_bSafetyZone_old);

    emit accept();
}

void dialog_safetyzone::onCancel()
{
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setWZCheckFlag(m_bSafetyZone_old);

    emit reject();
}

void dialog_safetyzone::Update()
{
    // read before safety zone setting value => write m_PData

    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    //WZ check off
    m_bSafetyZone_old = pCon->getWZCheckFlag();
    ret = pCon->setWZCheckFlag(false);
    if(ret < 0)
    {
        ZeroPData();
        qDebug() << "setWZCheckFlag(false) ret=" << ret;
        return;
    }

    int count[2]={0,0};
    ret = pCon->WZGetEntityCount(0, count[0]);
    if(ret < 0)
    {
        ZeroPData();
        qDebug() << "WZGetEntityCount index[0] ret=" << ret;
        return;
    }
    if(count[0] <= 0)
    {
        ZeroPData();
        qDebug() << "index[0] Entity count=" << count[0];
        return;
    }

    ret = pCon->WZGetEntityCount(1, count[1]);
    if(ret < 0)
    {
        ZeroPData();
        qDebug() << "WZGetEntityCount index[1] ret=" << ret;
        return;
    }
    if(count[1] <= 0)
    {
        ZeroPData();
        qDebug() << "index[1] Entity count=" << count[1];
        return;
    }

    int i;
    int index = 0;
    QVector<cn_vec3> vmin[2], vmax[2];
    cn_vec3 vdef;
    vdef.x = 0.0;vdef.y = 0.0;vdef.z = 0.0;

    for(index=0;index<2;index++)
    {
        vmin[index].fill(vdef, count[index]);
        vmax[index].fill(vdef, count[index]);

        for(i=0;i<count[index];i++)
        {
            ret = pCon->WZGetEntityBox(index, i, vmin[index][i], vmax[index][i]);
            if(ret < 0)
            {
                vmin[index][i].x = 0;vmax[index][i].x = 0;
                vmin[index][i].y = 0;vmax[index][i].y = 0;
                vmin[index][i].z = 0;vmax[index][i].z = 0;
                qDebug() << "WZGetEntityBox ret=" << ret;
                continue;
            }

            qDebug() << vmin[index][i].x << vmin[index][i].y << vmin[index][i].z;
            qDebug() << vmax[index][i].x << vmax[index][i].y << vmax[index][i].z;
        }
    }

    ZeroPData();

    if(count[0] > 0 && count[1] > 0)
    {
        m_PData[0].p1.x = vmax[0][0].x;
        m_PData[0].p1.y = vmax[0][0].y;
        m_PData[0].p1.z = vmax[0][0].z;
        m_PData[0].p2.x = vmin[0][0].x;
        m_PData[0].p2.y = vmin[0][0].y;
        m_PData[0].p2.z = vmin[0][0].z;
    }

    if(count[0] > 1 && count[1] > 1)
    {
        m_PData[1].p1.x = vmax[0][1].x;
        m_PData[1].p1.y = vmax[0][1].y;
        m_PData[1].p2.x = vmin[0][1].x;
        m_PData[1].p2.y = vmin[0][1].y;

        m_PData[2].p2.z = vmax[0][1].z;
        m_PData[2].p1.z = vmax[1][1].z;
    }

    if(count[0] > 2 && count[1] > 2)
    {
        m_PData[3].p1.z = vmax[0][2].z; // new
    }

}

bool dialog_safetyzone::SetSafetyZone()
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret;

    // add index 0
    ret = pCon->WZClear(0);
    if(ret < 0)
    {
        qDebug() << "WZClear(0) ret=" << ret;
        return false;
    }

    cn_vec3 vmin, vmax;
    //cn_tmat base_frame = GetBaseFrame();
    cn_trans2 base_frame = GetBaseFrame();

    // add index 0 & entity 0
    vmin.x = m_PData[0].p2.x;
    vmin.y = m_PData[0].p2.y;
    vmin.z = m_PData[0].p2.z;
    vmax.x = m_PData[0].p1.x;
    vmax.y = m_PData[0].p1.y;
    //vmax.z = 1000.0;          // fix. big number over IMM height.
    vmax.z = m_PData[2].p2.z + SZ_IMM_BTM_OFFSET;  // fix. big number over IMM height. mold open z height + 1000mm

    ret = pCon->WZAddBox(0, 1, vmin, vmax, base_frame);
    if(ret < 0)
    {
        qDebug() << "WZAddBox 0,0 ret=" << ret;
        return false;
    }

    // add index 0 & entity 1 (index 0 = mold open)
    vmin.x = m_PData[1].p2.x;
    vmin.y = m_PData[1].p2.y;
    vmin.z = m_PData[0].p2.z - SZ_RBT_TOP_OFFSET; // IMM Zmin minus over.
    vmax.x = m_PData[1].p1.x;
    vmax.y = m_PData[1].p1.y;
    vmax.z = m_PData[2].p2.z;

    ret = pCon->WZAddBox(0, -1, vmin, vmax, base_frame);
    if(ret < 0)
    {
        qDebug() << "WZAddBox 0,1 ret=" << ret;
        return false;
    }

    // add index 0 & entity 2 (for height limit)
    // if more lower open area, make same.
    if(m_PData[3].p1.z > (m_PData[0].p2.z - SZ_RBT_TOP_OFFSET))
        m_PData[3].p1.z = (m_PData[0].p2.z - SZ_RBT_TOP_OFFSET);

    vmin.x = (-1)*SZ_MAX_X_SIZE;
    vmin.y = (-1)*SZ_MAX_Y_SIZE;
    vmin.z = m_PData[3].p1.z; // Height value.
    vmax.x = SZ_MAX_X_SIZE;
    vmax.y = SZ_MAX_Y_SIZE;
    vmax.z = m_PData[3].p1.z - SZ_HEIGHT_THICKNESS;

    ret = pCon->WZAddBox(0, 1, vmin, vmax, base_frame);
    if(ret < 0)
    {
        qDebug() << "WZAddBox 0,2 ret=" << ret;
        return false;
    }

    // add index 1
    ret = pCon->WZClear(1);
    if(ret < 0)
    {
        qDebug() << "WZClear(1) ret=" << ret;
        return false;
    }

    // add index 1 & entity 0
    vmin.x = m_PData[0].p2.x;
    vmin.y = m_PData[0].p2.y;
    vmin.z = m_PData[0].p2.z;
    vmax.x = m_PData[0].p1.x;
    vmax.y = m_PData[0].p1.y;
    //vmax.z = 1000.0;          // fix. big number over IMM height.
    vmax.z = m_PData[2].p2.z + SZ_IMM_BTM_OFFSET;  // fix. big number over IMM height. mold open z height + 1000mm

    ret = pCon->WZAddBox(1, 1, vmin, vmax, base_frame);
    if(ret < 0)
    {
        qDebug() << "WZAddBox 1,0 ret=" << ret;
        return false;
    }

    // add index 1 & entity 1 (index 0 = mold close)
    vmin.x = m_PData[1].p2.x;
    vmin.y = m_PData[1].p2.y;
    vmin.z = m_PData[0].p2.z - SZ_RBT_TOP_OFFSET; // IMM Zmin minus over.
    vmax.x = m_PData[1].p1.x;
    vmax.y = m_PData[1].p1.y;
    vmax.z = m_PData[2].p1.z;

    ret = pCon->WZAddBox(1, -1, vmin, vmax, base_frame);
    if(ret < 0)
    {
        qDebug() << "WZAddBox 1,1 ret=" << ret;
        return false;
    }

    // add index 1 & entity 2 (for height limit)
    // if more lower open area, make same.
    if(m_PData[3].p1.z > (m_PData[0].p2.z - SZ_RBT_TOP_OFFSET))
        m_PData[3].p1.z = (m_PData[0].p2.z - SZ_RBT_TOP_OFFSET);

    vmin.x = (-1)*SZ_MAX_X_SIZE;
    vmin.y = (-1)*SZ_MAX_Y_SIZE;
    vmin.z = m_PData[3].p1.z; // Height value.
    vmax.x = SZ_MAX_X_SIZE;
    vmax.y = SZ_MAX_Y_SIZE;
    vmax.z = m_PData[3].p1.z - SZ_HEIGHT_THICKNESS;

    ret = pCon->WZAddBox(1, 1, vmin, vmax, base_frame);
    if(ret < 0)
    {
        qDebug() << "WZAddBox 1,2 ret=" << ret;
        return false;
    }

    // default Enable & Check flag (negate is ok)
    ret = pCon->WZEnable(0, 1);
    if(ret < 0)
    {
        qDebug() << "WZEnable 0,1 ret=" << ret;
        return false;
    }
    ret = pCon->WZDisable(1);
    if(ret < 0)
    {
        qDebug() << "WZDisable 1 ret=" << ret;
        return false;
    }

    // Save.
    ret = pCon->WZSaveAll();
    if(ret < 0)
    {
        qDebug() << "WZSaveAll ret=" << ret;
        return false;
    }

    return true;
}

void dialog_safetyzone::Update_Page(int page_index)
{
    // change picture.
    pic_page->setCurrentIndex(page_index);

    // change page num.
    QString str;
    str.sprintf("%d/%d",page_index+1,pic_page->count());
    ui->lbNowPage->setText(str);

    // change data.
    Display_Data(page_index);

    Display_SaveBtn(page_index);

}

void dialog_safetyzone::onNext()
{
    int page = pic_page->currentIndex();

    if(++page >= pic_page->count())
        return;

    Update_Page(page);
}
void dialog_safetyzone::onBack()
{
    int page = pic_page->currentIndex();

    if(--page < 0)
        return;

    Update_Page(page);
}

void dialog_safetyzone::onTimer()
{
    CNRobo* pCon = CNRobo::getInstance();

    float* trans = pCon->getCurTrans();

    QString str;
    str.sprintf(FORMAT_POS, trans[TRAV]);
    ui->tbXReal->setText(str);
    str.sprintf(FORMAT_POS, trans[FWDBWD]);
    ui->tbYReal->setText(str);
    str.sprintf(FORMAT_POS, trans[UPDN]);
    ui->tbZReal->setText(str);

}

void dialog_safetyzone::onManual()
{
    if(dig_manual == 0)
        dig_manual = new dialog_manual();

    dig_manual->exec();
}

void dialog_safetyzone::onPBtn()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtPBtn.indexOf(btn);
    if(index < 0) return;

    int page_index = pic_page->currentIndex();

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-1000000.0);
    np->m_numpad->SetMaxValue(1000000.0);
    np->m_numpad->SetSosuNum(SOSU_POS);
    QString str;
    if(index < 3)
        str = ui->tbP1Title->text();
    else
        str = ui->tbP2Title->text();

    str += " ";
    str += m_vtRowTitle[index%3]->text();
    np->m_numpad->SetTitle(str);
    np->m_numpad->SetNum(m_vtPVal[index]->text().toDouble());

    if(np->exec() == QDialog::Accepted)
    {
        SetPData(page_index, index, (float)np->m_numpad->GetNumDouble());
        Display_Data(page_index);
    }

}
void dialog_safetyzone::onP1()
{
    int page_index = pic_page->currentIndex();
    CNRobo* pCon = CNRobo::getInstance();
    float* trans = pCon->getCurTrans();

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(QString(tr("CONFIRM")));
    QString str;
    str.sprintf("[P1] X:%.02f, Y:%.02f, Z:%.02f",trans[TRAV],trans[FWDBWD],trans[UPDN]);
    conf->Message(QString(tr("Would you want to apply this value?")),str);
    if(conf->exec() == QDialog::Rejected)
        return;

    m_PData[page_index].p1.x = trans[TRAV];
    m_PData[page_index].p1.y = trans[FWDBWD];
    m_PData[page_index].p1.z = trans[UPDN];

    Display_Data(page_index);
}
void dialog_safetyzone::onP2()
{
    int page_index = pic_page->currentIndex();
    CNRobo* pCon = CNRobo::getInstance();
    float* trans = pCon->getCurTrans();

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(QString(tr("CONFIRM")));
    QString str;
    str.sprintf("[P2] X:%.02f, Y:%.02f, Z:%.02f",trans[TRAV],trans[FWDBWD],trans[UPDN]);
    conf->Message(QString(tr("Would you want to apply this value?")),str);
    if(conf->exec() == QDialog::Rejected)
        return;

    m_PData[page_index].p2.x = trans[TRAV];
    m_PData[page_index].p2.y = trans[FWDBWD];
    m_PData[page_index].p2.z = trans[UPDN];

    Display_Data(page_index);
}

void dialog_safetyzone::Display_Data(int page_index)
{
    QString str;
    str.sprintf(FORMAT_POS, m_PData[page_index].p1.x);
    ui->tbX->setText(str);
    str.sprintf(FORMAT_POS, m_PData[page_index].p1.y);
    ui->tbY->setText(str);
    str.sprintf(FORMAT_POS, m_PData[page_index].p1.z);
    ui->tbZ->setText(str);

    str.sprintf(FORMAT_POS, m_PData[page_index].p2.x);
    ui->tbX_2->setText(str);
    str.sprintf(FORMAT_POS, m_PData[page_index].p2.y);
    ui->tbY_2->setText(str);
    str.sprintf(FORMAT_POS, m_PData[page_index].p2.z);
    ui->tbZ_2->setText(str);

    Display_DataEnable(page_index);
}
void dialog_safetyzone::Display_DataEnable(int page_index)
{
    for(int i=0;i<m_vtPBtn.count();i++)
    {
        m_vtPBtn[i]->setEnabled(true);
        m_vtPVal[i]->setEnabled(true);
    }

    switch (page_index) {
    case 0:
        ui->tbZ->setEnabled(false);
        ui->tbZPic->setEnabled(false);
        ui->tbZ->setText(tr("N/A"));
        break;

    case 1:
        ui->tbZ->setEnabled(false);
        ui->tbZPic->setEnabled(false);
        ui->tbZ->setText(tr("N/A"));
        ui->tbZ_2->setEnabled(false);
        ui->tbZPic_2->setEnabled(false);
        ui->tbZ_2->setText(tr("N/A"));
        break;

    case 2:
        ui->tbX->setEnabled(false);
        ui->tbXPic->setEnabled(false);
        ui->tbX->setText(tr("N/A"));
        ui->tbY->setEnabled(false);
        ui->tbYPic->setEnabled(false);
        ui->tbY->setText(tr("N/A"));

        ui->tbX_2->setEnabled(false);
        ui->tbXPic_2->setEnabled(false);
        ui->tbX_2->setText(tr("N/A"));
        ui->tbY_2->setEnabled(false);
        ui->tbYPic_2->setEnabled(false);
        ui->tbY_2->setText(tr("N/A"));
        break;

    case 3:
        ui->tbX->setEnabled(false);
        ui->tbXPic->setEnabled(false);
        ui->tbX->setText(tr("N/A"));
        ui->tbY->setEnabled(false);
        ui->tbYPic->setEnabled(false);
        ui->tbY->setText(tr("N/A"));

        ui->tbX_2->setEnabled(false);
        ui->tbXPic_2->setEnabled(false);
        ui->tbX_2->setText(tr("N/A"));
        ui->tbY_2->setEnabled(false);
        ui->tbYPic_2->setEnabled(false);
        ui->tbY_2->setText(tr("N/A"));
        ui->tbZ_2->setEnabled(false);
        ui->tbZPic_2->setEnabled(false);
        ui->tbZ_2->setText(tr("N/A"));
        break;

    }
}

void dialog_safetyzone::Display_SaveBtn(int page_index)
{
    if(page_index >= pic_page->count()-1)
        ui->grpYes->setEnabled(true);
    else
        ui->grpYes->setEnabled(false);
}
void dialog_safetyzone::SetPData(int page_index, int table_index, float value)
{
    switch (table_index) {
    case 0:
        m_PData[page_index].p1.x = value;
        break;
    case 1:
        m_PData[page_index].p1.y = value;
        break;
    case 2:
        m_PData[page_index].p1.z = value;
        break;

    case 3:
        m_PData[page_index].p2.x = value;
        break;
    case 4:
        m_PData[page_index].p2.y = value;
        break;
    case 5:
        m_PData[page_index].p2.z = value;
        break;
    }
}
float dialog_safetyzone::GetPData(int page_index, int table_index)
{
    float result = 0.0;

    switch (table_index) {
    case 0:
        result = m_PData[page_index].p1.x;
        break;
    case 1:
        result = m_PData[page_index].p1.y;
        break;
    case 2:
        result = m_PData[page_index].p1.z;
        break;

    case 3:
        result = m_PData[page_index].p2.x;
        break;
    case 4:
        result = m_PData[page_index].p2.y;
        break;
    case 5:
        result = m_PData[page_index].p2.z;
        break;
    }

    return result;
}
void dialog_safetyzone::ZeroPData()
{
    for(int i=0;i<m_PData.count();i++)
    {
        m_PData[i].p1.x = 0;
        m_PData[i].p1.y = 0;
        m_PData[i].p1.z = 0;
        m_PData[i].p2.x = 0;
        m_PData[i].p2.y = 0;
        m_PData[i].p2.z = 0;
    }
}
cn_trans2 dialog_safetyzone::GetBaseFrame()
{
    cn_trans2 frame;

    frame.n.x = 0;
    frame.n.y = 0;
    frame.n.z = 0;

    frame.o.x = 0;
    frame.o.y = 0;
    frame.o.z = 0;

    frame.a.x = 0;
    frame.a.y = 0;
    frame.a.z = 0;

    frame.p.x = 0;
    frame.p.y = 0;
    frame.p.z = 0;

    frame.ext = 0;

    return frame;
}
