#ifndef FORM_SPECIALSETTING_H
#define FORM_SPECIALSETTING_H

#include <QWidget>

#include "global.h"

#include "dialog_confirm.h"
#include "dialog_message.h"

#include "view_param.h"
#include "dialog_zeroing.h"
#include "dialog_robotconfig.h"
#include "dialog_robotdimension.h"
#include "dialog_motor_test.h"
#include "dialog_safetyzone.h"
#include "dialog_softsensor.h"
#include "dialog_program_manage.h"
#include "dialog_tp_ipgw.h"
#include "dialog_anti_vibration.h"
#include "dialog_delaying.h"
#include "dialog_task_control.h"

namespace Ui {
class form_specialsetting;
}

class form_specialsetting : public QWidget
{
    Q_OBJECT

public:
    explicit form_specialsetting(QWidget *parent = 0);
    ~form_specialsetting();

    void Update();

public slots:
    void onClose();

    void onParam();
    void onZero();
    void onMotion();
    void onRobot();
    void onReboot_press();
    void onReboot();
    void onFirstView();
    void onMotorTest();
    void onSafetyZone();
    void onSoftSen();
    void onProgManage();

    void onIpGw();
    void onAntiVib();
    void onTask();

    void onDataUpdate();

private:
    Ui::form_specialsetting *ui;

    void SetFirstLed(bool onoff);
    void Check_update(QString var_name, float default_data);
    int UpdateCount;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // FORM_SPECIALSETTING_H
