#include "hysasang_pattern.h"

HySasang_Pattern::HySasang_Pattern()
{
    m_MotionFunc = new HySasang_Motion();

}

// making basic pattern structure.
bool HySasang_Pattern::Default(ST_SSPATTERN_DATA &data)
{
    data.name.clear(); // name is no handling.
    data.nickname.clear();

    m_MotionFunc->NewList();
    data.motions.clear();

    for(int i=0;i<m_MotionFunc->GetCount();i++)
    {
        if(!m_MotionFunc->Data2Raw(m_MotionFunc->MotionList[i]))
            return false;

        data.motions.append(m_MotionFunc->MotionList[i].raw);
    }

    if(!Data2Prog(data))
        return false;

    return true;
}

bool HySasang_Pattern::Data2Prog(ST_SSPATTERN_DATA &data)
{
    if(data.motions.isEmpty())
        return false;

    QList<QStringList> program_old = data.program;

    data.program.clear();

    QStringList motion_prog;
    int i;
    for(i=0;i<data.motions.count();i++)
    {
        motion_prog.clear();
        if(!m_MotionFunc->Raw2Prog(data.motions[i], motion_prog))
        {
            data.program = program_old;
            return false;
        }

        data.program.append(motion_prog);
    }

    return true;
}
bool HySasang_Pattern::Prog2Macro(QList<QStringList> prog, int list_index)
{
    if(prog.isEmpty()) return false;

    QString macroname;
    macroname = GetName_TestSubMacro();
    macroname += QString().setNum(list_index);
    qDebug() << "HySasang_Pattern Prog2Macro Start! Macro Name =" << macroname;

    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    //1. If file is not exits, make file
    if(!pCon->findProgram(macroname))
    {
        ret = pCon->createProgram(macroname);
        if(ret < 0)
        {
            qDebug() << "HySasang_Pattern Prog2Macro() creatProgram ret =" << ret;
            qDebug() << "Macro Name =" << macroname;
            return false;
        }
    }

    //2. use createProgramFile
    QStringList _program = SumProgram(prog);
    ret = pCon->setProgramSteps(macroname, _program);
    if(ret < 0)
    {
        qDebug() << "HySasang_Pattern Prog2Macro() setProgramSteps ret =" << ret;
        qDebug() << "Macro Name =" << macroname;
        return false;
    }

    //3. Save program => no save for program file space.

    return true;
}

QStringList HySasang_Pattern::SumProgram(QList<QStringList> prog)
{
    QStringList result;
    result.clear();

    for(int i=0;i<prog.count();i++)
        result << prog[i];

    return result;
}

bool HySasang_Pattern::MakeTestMain()
{
    if(PatternList.isEmpty()) return false;

    qDebug() << "HySasang_Pattern::MakeTestMain() Start!";

    //1. make call program.
    QStringList program;
    QString str;
    int i;

    program.clear();
    for(i=0;i<PatternList.count();i++)
    {
        str = "Call ";
        str += GetName_TestSubMacro();
        str += QString().setNum(i);

        program.append(str);

        qDebug() << str;
    }

    //2. make & write main program
    CNRobo* pCon = CNRobo::getInstance();
    int ret;
    QString main_name = GetName_TestMainMacro();

    //2-1. If it don't have main macro, make file.
    if(!pCon->findProgram(main_name))
    {
        ret = pCon->createProgram(main_name);
        if(ret < 0)
        {
            qDebug() << "HySasang_Pattern::MakeTestMain() creatProgram ret =" << ret;
            qDebug() << "Main Name =" << main_name;
            return false;
        }
    }

    //2-2. Write main program
    ret = pCon->setProgramSteps(main_name, program);
    if(ret < 0)
    {
        qDebug() << "HySasang_Pattern::MakeTestMain() setProgramSteps ret =" << ret;
        qDebug() << "Main Name =" << main_name;
        return false;
    }

    return true;
}

bool HySasang_Pattern::DataAllChange(ST_SSPATTERN_DATA &data, ST_SSSHIFT_VALUE shift, float speed, float accuracy)
{
    if(data.motions.isEmpty()) return false;

    ST_SSPATTERN_DATA data_old = data;
    QStringList result;
    QList<QStringList> program;

    int i, ret;
    ST_SSMOTION_DATA motion_data;

    result.clear();
    program.clear();
    for(i=0;i<data.motions.count();i++)
    {
        motion_data.raw = data.motions[i];

        if(!m_MotionFunc->Raw2Data(motion_data))
        {
            data = data_old;
            return false;
        }

        // change position
        ret = m_MotionFunc->DataShift(motion_data, shift);
        if(ret < 0)
        {
            data = data_old;
            return false;
        }

        // change speed
        if(speed >= 0)
        {
            if(m_MotionFunc->IsMoveCommand(motion_data.command))
                motion_data.speed_output = speed;
        }

        // change accuracy
        if(accuracy >= 0)
        {
            if(motion_data.command == m_MotionFunc->toStr(HySasang_Motion::con))
                motion_data.accuracy_input = accuracy;
        }

        // Data2Raw => make pattern.motions
        if(!m_MotionFunc->Data2Raw(motion_data))
        {
            data = data_old;
            return false;
        }

        // Data2Prog
        if(!m_MotionFunc->Data2Prog(motion_data))
        {
            data = data_old;
            return false;
        }

        result.append(motion_data.raw);
        program.append(motion_data.program);
    }

    data.motions.clear();
    data.motions = result;
    data.program.clear();
    data.program = program;
    return true;
}

bool HySasang_Pattern::IsSame(ST_SSPATTERN_DATA &data1, ST_SSPATTERN_DATA &data2)
{
    if(data1.name != data2.name)
        return false;
    if(data1.nickname != data2.nickname)
        return false;
    if(data1.motions != data2.motions)
        return false;
    if(data1.program != data2.program)
        return false;

    return true;
}

bool HySasang_Pattern::IsSameData(ST_SSPATTERN_DATA &data1, ST_SSPATTERN_DATA &data2)
{
    if(data1.motions != data2.motions)
        return false;
    return true;
}
bool HySasang_Pattern::IsSameProg(ST_SSPATTERN_DATA &data1, ST_SSPATTERN_DATA &data2)
{
    if(data1.program != data2.program)
        return false;
    return true;
}
bool HySasang_Pattern::IsSameName(ST_SSPATTERN_DATA &data1, ST_SSPATTERN_DATA &data2)
{
    if(data1.name != data2.name)
        return false;
    return true;
}

bool HySasang_Pattern::MakeTestProgram()
{
    if(PatternList.isEmpty()) return false;

    //1. Prog2Macro
    int i;
    for(i=0;i<PatternList.count();i++)
    {
        if(!Prog2Macro(PatternList[i].program, i))
        {
            qDebug() << "HySasang_Pattern::MakeTestProgram() Prog2Macro fail index=" << i;
            return false;
        }
    }
    //2. MakeTestMain

    if(!MakeTestMain())
    {
        qDebug() << "HySasang_Pattern::MakeTestProgram() MakeTestMain fail!";
        return false;
    }

    return true;
}

int HySasang_Pattern::MakeTestProgram(QList<ST_SSPATTERN_DATA>& comp_pattern_list)
{
    if(PatternList.isEmpty()) return false;

    if(PatternList.size() != comp_pattern_list.size())
    {
        qDebug() << "HySasang_Pattern::MakeTestProgram(...) Not Same List Size.";
        return -2;
    }

    //1. Data2Prog & Prog2Macro
    int i;
    for(i=0;i<PatternList.count();i++)
    {
        if(!IsSameProg(PatternList[i], comp_pattern_list[i]))
        {
            qDebug() << "HySasang_Pattern::MakeTestProgram(..) Not Same index =" << i;
            if(!Data2Prog(PatternList[i]))
            {
                qDebug() << "HySasang_Pattern::MakeTestProgram(..) Data2Prog fail index=" << i;
                return -1;
            }
            if(!Prog2Macro(PatternList[i].program, i))
            {
                qDebug() << "HySasang_Pattern::MakeTestProgram(..) Prog2Macro fail index=" << i;
                return -1;
            }
        }
    }

    return 0;
}

bool HySasang_Pattern::Add(ST_SSPATTERN_DATA &data, int index)
{
    if(index < 0)
    {
        // insert rare.
        PatternList.push_back(data);
    }
    else
    {
        if(index < 0) return false;
        if(index > PatternList.size()) return false;

        PatternList.insert(index, data);
    }

    qDebug() << "HySasang_Pattern::Add()" << index << data.name;
    return true;
}

bool HySasang_Pattern::Copy(int source_index, int target_index)
{
    if(source_index < 0) return false;
    if(source_index >= PatternList.size()) return false;

    ST_SSPATTERN_DATA data;
    data = PatternList[source_index];

    data.name += "_copy";

    if(target_index < 0)
    {
        PatternList.push_back(data);
    }
    else
    {
        if(target_index < 0) return false;
        if(target_index > PatternList.size()) return false;

        PatternList.insert(target_index, data);
    }

    qDebug() << "HySasang_Pattern::Copy()"
             << source_index << PatternList[source_index].name
             << "=>"
             << target_index << data.name;
    return true;
}

bool HySasang_Pattern::Del(int index)
{
    if(index < 0) return false;
    if(index >= PatternList.size()) return false;

    ST_SSPATTERN_DATA data;
    data = PatternList.takeAt(index);

    qDebug() << "HySasang_Pattern::Del()" << index << data.name;
    return true;
}

bool HySasang_Pattern::Rename(int index, QString name)
{
    if(index < 0) return false;
    if(index >= PatternList.size()) return false;

    PatternList[index].name = name;

    qDebug() << "HySasang_Pattern::Rename()" << index << name;
    return true;
}

bool HySasang_Pattern::Up(int index)
{
    if(index <= 0) return false;
    if(index >= PatternList.size()) return false;

    PatternList.move(index, index-1);

    return true;
}
bool HySasang_Pattern::Down(int index)
{
    if(index >= (PatternList.size()-1)) return false;
    if(index < 0) return false;

    PatternList.move(index, index+1);

    return true;
}

int HySasang_Pattern::GetCount()
{
    return PatternList.count();
}
bool HySasang_Pattern::GetName(int index, QString &name)
{
    if(index < 0) return false;
    if(index >= PatternList.size()) return false;

    name = PatternList[index].name;
    return true;
}
bool HySasang_Pattern::SetName(int index, QString &name)
{
    if(index < 0) return false;
    if(index >= PatternList.size()) return false;

    PatternList[index].name = name;
    return true;
}
bool HySasang_Pattern::GetNickname(int index, QString &nickname)
{
    if(index < 0) return false;
    if(index >= PatternList.size()) return false;

    nickname = PatternList[index].nickname;
    return true;
}
bool HySasang_Pattern::SetNickname(int index, QString &nickname)
{
    if(index < 0) return false;
    if(index >= PatternList.size()) return false;

    PatternList[index].nickname = nickname;
    return true;
}

int HySasang_Pattern::GetMotionCount(int index)
{
    if(index < 0) return -1;
    if(index >= PatternList.size()) return -1;

    int motion_count = PatternList[index].motions.count();
    //qDebug() << "HySasang_Pattern::GetMotionCount = " << motion_count;

    return motion_count;
}

bool HySasang_Pattern::Data2GroupFormat(ST_SSPATTERN_DATA data, QStringList &format)
{
    // pattern save format in group data file.
    // name,nickname
    // p2p,...
    // ...

    // make header
    QStringList list;
    list.clear();
    list << data.name;
    list << data.nickname;

    QString header;
    header = list.join(",");

    QStringList result;
    result.clear();
    result << header;
    result << data.motions;

    format = result;
    qDebug() << format;
    return true;
}
bool HySasang_Pattern::GroupFormat2Data(QStringList format, ST_SSPATTERN_DATA &data)
{
    if(format.isEmpty())
        return false;

    ST_SSPATTERN_DATA data_old = data;

    // parsing header data
    QString header = format.front();
    QStringList header_data = header.split(",");
    if(header_data.count() < 2)
    {
        data = data_old;
        return false;
    }

    data.name = header_data[0];
    data.nickname = header_data[1];

    // parsing main data.
    format.pop_front();

    data.motions = format;

    return true;
}

bool HySasang_Pattern::IsAbleName(QString name)
{
    if(name.isEmpty())
        return false;

    for(int i=0;i<PatternList.count();i++)
    {
        if(PatternList[i].name == name)
            return false;
    }

    // add more.

    return true;
}

bool HySasang_Pattern::Update_CompData()
{
    PatternList_Comp = PatternList;
    return true;
}
bool HySasang_Pattern::Reset_CompData()
{
    PatternList = PatternList_Comp;
    return true;
}

bool HySasang_Pattern::IsEdit()
{
    if(PatternList.size() != PatternList_Comp.size())
        return true;

    for(int i=0;i<PatternList.count();i++)
    {
        if(!IsSame(PatternList[i], PatternList_Comp[i]))
            return true;
    }

    return false;
}

