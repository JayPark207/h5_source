#ifndef DIALOG_NUMPAD_H
#define DIALOG_NUMPAD_H

#include <QDialog>
#include <QStackedWidget>

#include <numpad.h>
#include "global.h"

#include "dialog_message.h"

namespace Ui {
class dialog_numpad;
}

class dialog_numpad : public QDialog
{
    Q_OBJECT
    
public:
    explicit dialog_numpad(QWidget *parent = 0);
    ~dialog_numpad();

    numpad* m_numpad;

public slots:
    void onOK();
    
private:
    Ui::dialog_numpad *ui;

    QStackedWidget* sw;

protected:
    void changeEvent(QEvent *);

};

#endif // DIALOG_NUMPAD_H
