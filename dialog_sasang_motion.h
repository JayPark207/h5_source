#ifndef DIALOG_SASANG_MOTION_H
#define DIALOG_SASANG_MOTION_H

#include <QDialog>
#include <QStackedWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_sasang_motion_teach.h"
#include "dialog_sasang_motion_test.h"

#include "dialog_message.h"
#include "dialog_confirm.h"
#include "dialog_numpad.h"
#include "dialog_sasang_shift.h"


namespace Ui {
class dialog_sasang_motion;
}

class dialog_sasang_motion : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_sasang_motion(QWidget *parent = 0);
    ~dialog_sasang_motion();

    void Init(QString group_name, QString pattern_name);
    void Update();


private:
    Ui::dialog_sasang_motion *ui;

    QString m_GrpName, m_PatName;

    QTimer* timer;

    QStackedWidget* Page;
    QStackedWidget* Panel;
    QStackedWidget* SubPanel;

    dialog_sasang_motion_teach* page_teach;
    dialog_sasang_motion_test* page_test;

    QVector<QLabel3*> m_vtTbNo;
    QVector<QLabel3*> m_vtTbMot;

    int m_nSelectedIndex;
    int m_nStartIndex;

    void Redraw_List(int start_index);      // boss
    void Display_ListOn(int table_index);   // boss


    QVector<QLabel3*> m_vtAxis;
    QVector<QLabel4*> m_vtValPic,m_vtVal2Pic;
    QVector<QLabel3*> m_vtVal,m_vtVal2;
    QVector<QLabel4*> m_vtType;
    QVector<QLabel3*> m_vtTypeIcon;

    QVector<QLabel4*> m_vtOutPic;
    QVector<QLabel3*> m_vtOutIcon;
    QVector<QLabel3*> m_vtOutSel;
    QVector<int>      m_vtOutputNum;

    ST_SSMOTION_DATA  m_DataOld;
    ST_SSMOTION_DATA  m_DataNew;
    bool IsSame(ST_SSMOTION_DATA old_data, ST_SSMOTION_DATA new_data);


    void Display_Data(int list_index); // boss (enable control & all control)
    bool Display_Data_Write(ST_SSMOTION_DATA data); // semi boss (data write control)
    void Display_SetPos(cn_trans t, cn_joint j);
    void Display_SetPos2(cn_trans t, cn_joint j);
    void Display_Speed(float speed);
    void Display_Accuracy(float accuracy);
    void Display_Type(float type);
    void Display_Time(float time);
    void Display_PosTitle(QString command);

    void MatchingOutput();
    void Display_Output(float output);

    void Display_SaveEnable();

    bool Save();
    bool Reset();

    QVector<QPixmap> pxmListEditIcon;
    void Control_ListEdit(bool bOn);

    dialog_sasang_shift* dig_shift;

    void Test_Make_RawProg();
    void Test_View_RawProg(ST_SSMOTION_DATA data);

    bool m_bEditing;
    void Display_Editing(bool bEditing);


private: // for test
    void Test_Make_List();

public slots:
    void onClose();
    void onClose_Page();
    void onTimer();

    // Page
    void onTeach();
    void onTest();

    // List Control
    void onList();
    void onUp(); //table up
    void onDown();
    void onListUp(); // selected item position up.
    void onListDown();
    void onListDel();
    void onListEdit();
    void onShift();

    void onSave();
    void onReset();

    // data modify (using m_DataNew)
    void onPos();
    void onPos2();
    void onSpeed();
    void onAccuracy();
    void onOutput();
    void onTime();
    void onType();

    // test page edit
    void onEditing();
    void onEditBack();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SASANG_MOTION_H
