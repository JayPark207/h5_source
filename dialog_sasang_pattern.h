#ifndef DIALOG_SASANG_PATTERN_H
#define DIALOG_SASANG_PATTERN_H

#include <QDialog>
#include <QStackedWidget>
#include <QTimer>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_sasang_pattern_test.h"

#include "dialog_sasang_motion.h"

#include "dialog_message.h"
#include "dialog_confirm.h"
#include "dialog_keyboard.h"
#include "dialog_sasang_shift.h"


namespace Ui {
class dialog_sasang_pattern;
}

class dialog_sasang_pattern : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_sasang_pattern(QWidget *parent = 0);
    ~dialog_sasang_pattern();

    void Init(QString group_name);
    void Init(ST_SSGROUP_DATA group_data, bool save_accepted = false);
    void Update();
    ST_SSGROUP_DATA GetGrpData();

private:
    Ui::dialog_sasang_pattern *ui;

    QString m_GrpName;
    ST_SSGROUP_DATA m_GrpData;
    bool m_bSave_Accepted;

    QTimer* timer;

    QStackedWidget* Page;
    dialog_sasang_pattern_test* page_test;

    dialog_sasang_motion* dig_sasang_motion;

    QVector<QLabel3*> m_vtTbNo,m_vtTbPat,m_vtTbMot;

    int m_nSelectedIndex;
    int m_nStartIndex;

    void Redraw_List(int start_index);      // boss
    void Display_ListOn(int table_index);   // boss

    void Enable_Save(bool bEdit);

    dialog_sasang_shift* dig_shift;

//private:
//    void Test_Make_List();

public slots:
    void onSave();
    void onCancel();
    void onClose();
    void onClose_Page();

    void onTimer();

    void onTest();

    void onList();
    void onUp();
    void onDown();

    void onNew();
    void onCopy();
    void onRename();
    void onDelete();
    void onListUp();
    void onListDn();

    void onInside();
    void onShift();


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SASANG_PATTERN_H
