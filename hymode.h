#ifndef HYMODE_H
#define HYMODE_H

/** 모드에대한 모든 데이터 및 국제화를 담당하는 클래스.
 *  그룹별 관리를 기본으로 하되, 전체 데이터를 읽고 설정하는 것이 가능하도록...
 * **/

#include <QObject>
#include <defines.h>

#include <hyrecipe.h>

class HyMode : public QObject
{
    Q_OBJECT

public:

    enum MODE_GROUP
    {
        MGRP_COMMON=0,
        MGRP_TAKEOUT,
        MGRP_UNLOAD,
        MGRP_INTLOCK,
        MGRP_SENSOR,

        MGRP_NUM,
    };

    enum MODE_TYPE
    {
        MODE_SELITEM=0, // only select item (Normal)
        // special case.
        MODE_MOLDCLEAN,
        MODE_TAKEOUTMETHOD,
        MODE_JMOTION,
        MODE_UNDERCUT,
        MODE_SYNCBACK,

        MODE_CONVTIME,
        MODE_GRABCOMPTIME,
        MODE_UNLOADMETHOD,

        MODE_WEIGHT,
        MODE_TEMP,
        MODE_ESENSOR,

        MODE_DUALUNLOAD,
        MODE_UNLOADBACK,

        MODE_TAKEOUTTIMING,
    };

    struct ST_MODE_DATA
    {
        QString Name;           // Display Mode Name
        QStringList ItemName;   // Display Mode Select Item.
        QVector<int> Vars;      // Related Recipe Variables
        QVector<float> Datas;   // Readed Recipe Variables Datas.
        MODE_TYPE Type;         // Select Type

        bool bNA;               // Ignore N/A (true)
        bool bDiff;             // false = same default, true = no same default.

        QString DispResult;     // Display Result value.

        void (HyMode::*Func)(int, int); // if DispSpecial=true, execute this func()
    };

public:
    HyMode(HyRecipe* recipe);

    void Init();

    QVector<ST_MODE_DATA> m_Data[MGRP_NUM]; // important data.
    bool Read(MODE_GROUP grp);          // read Recipe data + MakeResult_Common().

    HyRecipe* m_Recipe;

    void MakeResult(MODE_GROUP grp);    // Make Result string.

    // MGRP_COMMON
    void Init_Common();

    // MGRP_TAKEOUT
    void Init_Takeout();
    void MakeResult_TakeoutMethod(int grp, int index);

    // MGRP_UNLOAD
    void Init_Unload();
    void MakeResult_ConvTime(int grp, int index);
    void MakeResult_GrapCompTime(int grp, int index);
    void MakeResult_UnloadMethod(int grp, int index);

    // MGRP_INTLOCK
    void Init_Intlock();

    // MGRP_SENSOR
    void Init_Sensor();
    void MakeResult_Weight(int grp, int index);
    void MakeResult_Temp(int grp, int index);
    void MakeResult_ESensor(int grp, int index);


    // private function.
    void MakeResult_Normal(int grp, int index);
    void Clear(ST_MODE_DATA& dat);

};

#endif // HYMODE_H
