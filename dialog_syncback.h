#ifndef DIALOG_SYNCBACK_H
#define DIALOG_SYNCBACK_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_numpad.h"
#include "dialog_mode_select.h"


namespace Ui {
class dialog_syncback;
}

class dialog_syncback : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_syncback(QWidget *parent = 0);
    ~dialog_syncback();

    void Update();

public slots:
    void onClose();

    void onUse();
    void onValue();

private:
    Ui::dialog_syncback *ui;

    QVector<QLabel4*> m_vtValPic;
    QVector<QLabel3*> m_vtVal;
    QVector<QLabel3*> m_vtValName;

    QVector<HyRecipe::RECIPE_NUMBER> vars;
    QVector<float> datas;
    QStringList m_ItemName;

    void Update_CalSpeed();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SYNCBACK_H
