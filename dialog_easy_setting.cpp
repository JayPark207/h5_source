#include "dialog_easy_setting.h"
#include "ui_dialog_easy_setting.h"

dialog_easy_setting::dialog_easy_setting(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_easy_setting)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);

    SidePanel = new QStackedWidget(this);
    SidePanel->setGeometry(ui->wigControl->x(),
                           ui->wigControl->y(),
                           ui->wigControl->width(),
                           ui->wigControl->height());
    SidePanel->addWidget(ui->wigControl);
    wig_jogmodule = new widget_jogmodule4(this);
    SidePanel->addWidget(wig_jogmodule);
    SidePanel->setCurrentIndex(0);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    fast_timer = new QTimer(this);
    fast_timer->setInterval(50);
    connect(fast_timer,SIGNAL(timeout()),this,SLOT(onFastTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    connect(ui->btnFwdPic,SIGNAL(mouse_press()),this,SLOT(onFwd_Press()));
    connect(ui->btnFwdPic,SIGNAL(mouse_release()),this,SLOT(onFwd_Release()));
    connect(ui->btnFwd,SIGNAL(mouse_press()),ui->btnFwdPic,SLOT(press()));
    connect(ui->btnFwd,SIGNAL(mouse_release()),ui->btnFwdPic,SLOT(release()));
    connect(ui->btnFwdIcon,SIGNAL(mouse_press()),ui->btnFwdPic,SLOT(press()));
    connect(ui->btnFwdIcon,SIGNAL(mouse_release()),ui->btnFwdPic,SLOT(release()));

    connect(ui->btnBwdPic,SIGNAL(mouse_press()),this,SLOT(onBwd_Press()));
    connect(ui->btnBwdPic,SIGNAL(mouse_release()),this,SLOT(onBwd_Release()));
    connect(ui->btnBwd,SIGNAL(mouse_press()),ui->btnBwdPic,SLOT(press()));
    connect(ui->btnBwd,SIGNAL(mouse_release()),ui->btnBwdPic,SLOT(release()));
    connect(ui->btnBwdIcon,SIGNAL(mouse_press()),ui->btnBwdPic,SLOT(press()));
    connect(ui->btnBwdIcon,SIGNAL(mouse_release()),ui->btnBwdPic,SLOT(release()));

    connect(ui->btnHomePic,SIGNAL(mouse_release()),this,SLOT(onHome()));
    connect(ui->btnHome,SIGNAL(mouse_press()),ui->btnHomePic,SLOT(press()));
    connect(ui->btnHome,SIGNAL(mouse_release()),ui->btnHomePic,SLOT(release()));

    m_vtTbIndex.clear();
    m_vtTbIndex.append(ui->tbIndex);
    m_vtTbIndex.append(ui->tbIndex_2);
    m_vtTbIndex.append(ui->tbIndex_3);
    m_vtTbIndex.append(ui->tbIndex_4);
    m_vtTbIndex.append(ui->tbIndex_5);

    m_vtSet.clear();            m_vtReal.clear();
    m_vtSet.append(ui->tbSet);  m_vtReal.append(ui->tbReal);
    m_vtSet.append(ui->tbSet_2);m_vtReal.append(ui->tbReal_2);
    m_vtSet.append(ui->tbSet_3);m_vtReal.append(ui->tbReal_3);
    m_vtSet.append(ui->tbSet_4);m_vtReal.append(ui->tbReal_4);
    m_vtSet.append(ui->tbSet_5);m_vtReal.append(ui->tbReal_5);
    m_vtSet.append(ui->tbSet_6);m_vtReal.append(ui->tbReal_6);

    m_vtFwding.clear();               m_vtBwding.clear();
    m_vtFwding.append(ui->lbFwding);  m_vtBwding.append(ui->lbBwding);
    m_vtFwding.append(ui->lbFwding_2);m_vtBwding.append(ui->lbBwding_2);
    m_vtFwding.append(ui->lbFwding_3);m_vtBwding.append(ui->lbBwding_3);
    m_vtFwding.append(ui->lbFwding_4);m_vtBwding.append(ui->lbBwding_4);
    m_vtFwding.append(ui->lbFwding_5);m_vtBwding.append(ui->lbBwding_5);
    m_vtFwding.append(ui->lbFwding_6);m_vtBwding.append(ui->lbBwding_6);

    m_vtFwding2.clear();               m_vtBwding2.clear();
    m_vtFwding2.append(ui->lbFwd);  m_vtBwding2.append(ui->lbBwd);
    m_vtFwding2.append(ui->lbFwd_2);m_vtBwding2.append(ui->lbBwd_2);
    m_vtFwding2.append(ui->lbFwd_3);m_vtBwding2.append(ui->lbBwd_3);

    connect(ui->btnJogPic,SIGNAL(mouse_release()),this,SLOT(onJog()));
    connect(ui->btnJog,SIGNAL(mouse_press()),ui->btnJogPic,SLOT(press()));
    connect(ui->btnJog,SIGNAL(mouse_release()),ui->btnJogPic,SLOT(release()));

    connect(ui->btnSetPic,SIGNAL(mouse_release()),this,SLOT(onSet()));
    connect(ui->btnSet,SIGNAL(mouse_press()),ui->btnSetPic,SLOT(press()));
    connect(ui->btnSet,SIGNAL(mouse_release()),ui->btnSetPic,SLOT(release()));

    connect(ui->btnSpdUpPic,SIGNAL(mouse_release()),this,SLOT(onSpdUp()));
    connect(ui->btnSpdUpIcon,SIGNAL(mouse_press()),ui->btnSpdUpPic,SLOT(press()));
    connect(ui->btnSpdUpIcon,SIGNAL(mouse_release()),ui->btnSpdUpPic,SLOT(release()));

    connect(ui->btnSpdDownPic,SIGNAL(mouse_release()),this,SLOT(onSpdDown()));
    connect(ui->btnSpdDownIcon,SIGNAL(mouse_press()),ui->btnSpdDownPic,SLOT(press()));
    connect(ui->btnSpdDownIcon,SIGNAL(mouse_release()),ui->btnSpdDownPic,SLOT(release()));

    connect(ui->btnIOPic,SIGNAL(mouse_release()),this,SLOT(onIO()));
    connect(ui->btnIO,SIGNAL(mouse_press()),ui->btnIOPic,SLOT(press()));
    connect(ui->btnIO,SIGNAL(mouse_release()),ui->btnIOPic,SLOT(release()));

    Init_NoSetIndexList();

#ifdef _H6_
    ui->tbAxis_6->setText(tr("J6"));

    ui->tbAxis_6->setEnabled(true);
    ui->tbSet_6->setEnabled(true);
    ui->tbReal_6->setEnabled(true);
#else
    ui->tbAxis_6->setText("");

    ui->tbAxis_6->setEnabled(false);
    ui->tbSet_6->setEnabled(false);
    ui->tbReal_6->setEnabled(false);
#endif

    ui->pbarSpeed->setMaximum(EASYSET_SPEED_MAX);

    // init var. pointer.
    dig_delaying = 0;
    dig_easy_setting_io = 0;
    top = 0;
    dig_home2 = 0;
    pxmBwding.clear();pxmFwding.clear();
    m_bStopover1 = true;
    m_bStopover2 = true;
}

dialog_easy_setting::~dialog_easy_setting()
{
    delete ui;
}
void dialog_easy_setting::showEvent(QShowEvent *)
{
    Update();
    timer->start();
    fast_timer->start();

    m_bFirstEntry = true;

    m_bDualSolOff = false;

    SubTop();
}
void dialog_easy_setting::hideEvent(QHideEvent *)
{
    timer->stop();
    fast_timer->stop();

    ServoOnMode(SON_MODE_NORMAL);
    gReadyJog(false);

    if(m_bDualSolOff)
        Recipe->Set(HyRecipe::bDualSolOff, 1, false);

}
void dialog_easy_setting::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_easy_setting::onClose()
{
    emit accept();
}
void dialog_easy_setting::onTimer()
{
    Display_Moving(m_bMoving);
    Display_EnableON(m_bEnableON);
    Display_IndexComp(m_bIndexComp_Fwd, m_bIndexComp_Bwd);
    //Display_FwdBwding(m_bIndexComp_Fwd, m_bIndexComp_Bwd);
    Display_FwdBwding2_1(m_bIndexComp_Fwd, m_bIndexComp_Bwd);

    Process_FirstEntry();
}
void dialog_easy_setting::onFastTimer()
{
    CNRobo* pCon = CNRobo::getInstance();
    m_bMoving = pCon->isMoving();
    m_bEnableON = pCon->isTPEnable();

    Process_IndexComp();
    Process_BtnPress();

    float* pTrans = pCon->getCurTrans();
    float* pJoint = pCon->getCurJoint();

    Display_RealPos(pTrans, pJoint);
    Make_RealPos(pTrans, pJoint);

}

void dialog_easy_setting::Process_FirstEntry()
{
    if(!m_bFirstEntry) return;
    m_bFirstEntry = false;
    m_bStopover1 = true;
    m_bStopover2 = true;

    if(!Home(true))
    {
        // no home, All control disable...
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Notice")));
        msg->Message(tr("Home not completed!!"),
                     tr("Please make sure home."));
        msg->exec();
        Enable_Control(false);
    }
    else
    {
        m_nIndex = 0;
        m_bIndexComp_Fwd = true;
        m_bIndexComp_Bwd = true;
        Redraw_IndexTable(m_nIndex);
        Enable_Control(true);
    }

    Delay(1000);
    ServoOnMode(SON_MODE_SPECIAL);
}

void dialog_easy_setting::Process_IndexComp()
{
    if(IsJog()) return;

    CNRobo* pCon = CNRobo::getInstance();

    m_bError = pCon->isError();

    switch(m_nStateIndexComp)
    {
    case 0:
        if(m_bMoving)
        {
            if(m_bBtn_Fwd)
            {
                m_bIndexComp_Fwd = false;
                m_bIndexComp_Bwd = true;
            }
            else if(m_bBtn_Bwd)
            {
                m_bIndexComp_Fwd = true;
                m_bIndexComp_Bwd = false;
            }
            m_nStateIndexComp = 1;
        }
        break;
    case 1:
        if(!m_bMoving)
        {
            if(m_bEnableON)
            {
                if(!m_bError)
                {
                    m_bIndexComp_Fwd = true;
                    m_bIndexComp_Bwd = true;
                    gBeep2();
                }
            }
            m_nStateIndexComp = 0;
        }
        break;
    }
}

void dialog_easy_setting::Process_BtnPress()
{
    if(IsJog()) return;

    switch(m_nStateBtnPress)
    {
    case 0:
        if(!ui->wigFwd->isEnabled())
            ui->wigFwd->setEnabled(true);
        if(!ui->wigBwd->isEnabled())
            ui->wigBwd->setEnabled(true);

        if(m_bBtn_Fwd)
        {
            SetEnableON(true);
            m_nBtnPressCnt = 0;
            ui->wigBwd->setEnabled(false);

            m_nStateBtnPress = 10;
        }
        else if(m_bBtn_Bwd)
        {
            SetEnableON(true);
            m_nBtnPressCnt = 0;
            ui->wigFwd->setEnabled(false);

            m_nStateBtnPress = 20;
        }
        break;

    // fwd press
    case 10:
        if(!m_bBtn_Fwd)
        {
            m_nStateBtnPress = 0;
            break;
        }

        m_nBtnPressCnt++;
        if(m_nBtnPressCnt > EASYSET_DELAY_MOVE) // 50msec * 20 = 1000 msec
            m_nStateBtnPress = 11;
        break;

    case 11:
        if(m_bIndexComp_Fwd)
        {
            m_nIndex++;
            m_bIndexComp_Fwd = false;
            m_bIndexComp_Bwd = true;

            // calculate fwd stopover1 (Stopover1 -> TAKEOUT)
            if(m_bStopover1)
            {
                if(m_IndexList[m_nIndex] == HyPos::STOPOVER1 && m_bIndexComp_Bwd)
                {
                    if(!ASave_StopoverPos1(m_RealTrans, m_RealJoint))
                    {
                        qDebug() << "Save_StopoverPos1 fwd fail!!";
                        m_nIndex--;
                        m_nStateBtnPress = 0;
                        break;
                    }
                    m_bStopover1 = false;
                }
            }
            // calculate fwd stopover2&3 (Stopover2-> Stopover3 -> UP1ST)
            if(m_bStopover2)
            {
                if(m_IndexList[m_nIndex] == HyPos::STOPOVER2 && m_bIndexComp_Bwd)
                {
                    if(!ASave_StopoverPos2(m_RealTrans, m_RealJoint))
                    {
                        qDebug() << "Save_StopoverPos2 fwd fail!!";
                        m_nIndex--;
                        m_nStateBtnPress = 0;
                        break;
                    }
                    m_bStopover2 = false;
                }
            }
        }

        Move(m_PosList[m_nIndex]);
        Redraw_IndexTable(m_nIndex);
        m_nStateBtnPress = 12;
        break;

    case 12:
        if(!m_bBtn_Fwd)
        {
            m_nBtnPressCnt = 0;
            ui->wigFwd->setEnabled(false);
            m_nStateBtnPress = 13;
        }
        break;

    case 13:
        m_nBtnPressCnt++;
        if(m_nBtnPressCnt > EASYSET_DELAY_NEXT)
            m_nStateBtnPress = 0;
        break;

    // bwd press
    case 20:
        if(!m_bBtn_Bwd)
        {
            m_nStateBtnPress = 0;
            break;
        }

        m_nBtnPressCnt++;
        if(m_nBtnPressCnt > EASYSET_DELAY_MOVE) // 50msec * 20 = 1000 msec
            m_nStateBtnPress = 21;
        break;

    case 21:
        if(m_bIndexComp_Bwd)
        {
            m_nIndex--;
            m_bIndexComp_Fwd = true;
            m_bIndexComp_Bwd = false;

            // calculate bwd stopover1 (TAKEOUT -(Backward)-> Stopover1)
            if(m_bStopover1)
            {
                if(m_IndexList[m_nIndex] == HyPos::STOPOVER1 && m_bIndexComp_Fwd)
                {
                    if(!ASave_StopoverPos1(m_RealTrans, m_RealJoint, m_IndexList[m_nIndex-1], true))
                    {
                        qDebug() << "Save_StopoverPos1 bwd fail!!";
                        m_nIndex++;
                        m_nStateBtnPress = 0;
                        break;
                    }
                    m_bStopover1 = false;
                }
            }
            // calculate bwd stopover2&3 (UP -(Backward)-> Stopover3)
            if(m_bStopover2)
            {
                if(m_IndexList[m_nIndex] == HyPos::STOPOVER3 && m_bIndexComp_Fwd)
                {
                    if(!ASave_StopoverPos2(m_RealTrans, m_RealJoint, m_IndexList[m_nIndex-2], true))
                    {
                        qDebug() << "Save_StopoverPos2 bwd fail!!";
                        m_nIndex++;
                        m_nStateBtnPress = 0;
                        break;
                    }
                    m_bStopover2 = false;
                }
            }

        }

        Move(m_PosList[m_nIndex]);
        Redraw_IndexTable(m_nIndex);
        m_nStateBtnPress = 22;
        break;

    case 22:
        if(!m_bBtn_Bwd)
        {
            m_nBtnPressCnt = 0;
            ui->wigBwd->setEnabled(false);
            m_nStateBtnPress = 23;
        }
        break;

    case 23:
        m_nBtnPressCnt++;
        if(m_nBtnPressCnt > EASYSET_DELAY_NEXT)
            m_nStateBtnPress = 0;
        break;

    }


    // pbarPress
    float rate_fwd, rate_bwd;

    if(m_bBtn_Fwd)
        rate_fwd = ((float)m_nBtnPressCnt/(float)EASYSET_DELAY_MOVE) * 100.0;
    else
        rate_fwd = 0.0;

    if(m_bBtn_Bwd)
        rate_bwd = ((float)m_nBtnPressCnt/(float)EASYSET_DELAY_MOVE) * 100.0;
    else
        rate_bwd = 0.0;

    ui->pbarPressFwd->setValue((int)rate_fwd);
    ui->pbarPressBwd->setValue((int)rate_bwd);

}

void dialog_easy_setting::Update()
{
    gReadyJog(true);
    SetEnableON(false);

    m_nStateIndexComp = 0;
    m_nStateBtnPress = 0;
    m_nBtnPressCnt = 0;
    m_bIndexComp_Fwd = false;
    m_bIndexComp_Bwd = false;
    m_bBtn_Fwd = false;
    m_bBtn_Bwd = false;
    m_nFwdBwding = 0;
    m_nFwdBwding2 = 0;

    Make_PosList(m_PosList, m_DispList, m_IndexList);
    m_nIndex = 0;
    Redraw_IndexTable(m_nIndex);
    Enter_Jog(false);

    m_Speed = EASYSET_SPEED_INIT;
    Speed(m_Speed);


}

void dialog_easy_setting::Init_NoSetIndexList()
{
    m_NoSetIndexList.clear();
    m_NoSetIndexList.append(HyPos::HOME);
    m_NoSetIndexList.append(HyPos::STOPOVER1);
    m_NoSetIndexList.append(HyPos::STOPOVER2);
    m_NoSetIndexList.append(HyPos::STOPOVER3);
    // add here...
}

void dialog_easy_setting::onFwd_Press()
{
    if(m_bEnableON || m_bMoving || m_bBtn_Bwd) return;
    if(!m_bIndexComp_Fwd && !m_bIndexComp_Bwd)
        return;
    if(m_nIndex >= (m_PosList.size()-1) && m_bIndexComp_Fwd)
        return;
    if(m_bError)
        return;

    m_bBtn_Fwd = true;

    qDebug() << "Fwd Press";
}
void dialog_easy_setting::onFwd_Release()
{
    SetEnableON(false);
    m_bBtn_Fwd = false;
    qDebug() << "Fwd Release";
}
void dialog_easy_setting::onBwd_Press()
{
    if(m_bEnableON || m_bMoving || m_bBtn_Fwd) return;
    if(!m_bIndexComp_Fwd && !m_bIndexComp_Bwd)
        return;
    if(m_nIndex <= 1 && m_bIndexComp_Bwd)
        return;
    if(m_bError)
        return;

    m_bBtn_Bwd = true;

    qDebug() << "Bwd Press";
}
void dialog_easy_setting::onBwd_Release()
{
    SetEnableON(false);
    m_bBtn_Bwd = false;
    qDebug() << "Bwd Release";
}

void dialog_easy_setting::Display_Moving(bool on)
{
    ui->ledMoving->setAutoFillBackground(on);
}
void dialog_easy_setting::Display_EnableON(bool on)
{
    ui->ledEnableON->setAutoFillBackground(on);
}
void dialog_easy_setting::Display_IndexComp(bool fwd, bool bwd)
{
    ui->ledIndexCompFwd->setAutoFillBackground(fwd);
    ui->ledIndexCompBwd->setAutoFillBackground(bwd);

    ui->ledArrival->setAutoFillBackground(fwd && bwd);
    ui->tbIndex_3->setAutoFillBackground(fwd && bwd);
}

bool dialog_easy_setting::ServoOnMode(int mode)
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret = pCon->setServoOnMode(mode);
    if(ret < 0)
    {
        qDebug() << "setServoOnMode ret=" << ret;
        return false;
    }

    return true;
}

void dialog_easy_setting::SetEnableON(bool on)
{
    CNRobo* pCon = CNRobo::getInstance();
    pCon->setEnableOn(on);
}
bool dialog_easy_setting::Move(QString PosName)
{
    CNRobo* pCon = CNRobo::getInstance();
    int ret = pCon->moveToPosition2(PosName, 1.0, true);
    if(ret < 0)
    {
        qDebug() << "moveToPosition2 ret=" << ret;
        return false;
    }
    return true;
}
void dialog_easy_setting::Speed(float speed)
{
    if(speed <= 0) speed = 1;

    CNRobo* pCon = CNRobo::getInstance();
    float ratio = speed / 100.0;
    pCon->setJogSpeed(0, ratio);
    pCon->setTeachSpeed(0);

    Display_Speed(speed);
}
float dialog_easy_setting::Speed()
{
    CNRobo* pCon = CNRobo::getInstance();
    float ratio=0.0;
    pCon->getJogSpeed(0, ratio);
    return ratio * 100;
}

void dialog_easy_setting::Delay(int msec)
{
    if(dig_delaying == 0)
        dig_delaying = new dialog_delaying();
    dig_delaying->Init(msec);
    dig_delaying->exec();
}

void dialog_easy_setting::onHome()
{
    // confirm..
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnHome->text());
    conf->Message(tr("Do you want to homing?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    if(Home(true))
    {
        m_nIndex = 0;
        m_bIndexComp_Fwd = true;
        m_bIndexComp_Bwd = true;

        Redraw_IndexTable(m_nIndex);

        // if control diable -> enable
        Enable_Control(true);
    }
    else
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Notice")));
        msg->Message(tr("Home not completed!!"),
                     tr("Please make sure home."));
        msg->exec();
        Enable_Control(false);
    }
}

bool dialog_easy_setting::Home(bool bCheck)
{
    fast_timer->stop();
    ServoOnMode(SON_MODE_NORMAL);

    if(dig_home2 == 0)
        dig_home2 = new dialog_home2();

    if(bCheck)
        dig_home2->Enable_Check();
    int res = dig_home2->exec();

    fast_timer->start();

    Delay(1000);
    ServoOnMode(SON_MODE_SPECIAL);

    if(res == QDialog::Accepted)
        return true;

    return false;
}

bool dialog_easy_setting::Make_PosList(QStringList &pos_list, QStringList &disp_list, QVector<int> &index_list)
{
    int i,index;

    //1. Read use pos.
    QVector<int> UsePos;
    UsePos.clear();

    if(!StepEdit->Read_Main())
    {
        qDebug() << "Make_PosList() StepEdit->Read_Main() Fail!";
        return false;
    }
    gMakeUsePosList(StepEdit->m_Step, UsePos);
    qDebug() << UsePos;

    //2. delete vector pos.
    //2-1. delete front wait.
    index = UsePos.indexOf(HyPos::WAIT);
    if(index < 0) return false;
    for(i=0;i<index;i++)
        UsePos.pop_front();
    qDebug() << UsePos;

    /*//2-2. delete position between wait and takeout.
    index = UsePos.indexOf(HyPos::TAKE_OUT);
    if(index < 0) return false;
    if(index > 1)
        UsePos.remove(1, index-1);
    qDebug() << UsePos;*/

    //2-3. delete back up1st.
    index = UsePos.indexOf(HyPos::UP1ST);
    if(index < 0) return false;
    UsePos = UsePos.mid(0, index+1);
    qDebug() << UsePos;

    /*//3. save & insert stopover pos.
    if(!Save_StopoverPos())
        return false;*/

    index = UsePos.indexOf(HyPos::TAKE_OUT);
    if(index < 0) return false;
    UsePos.insert(index, HyPos::STOPOVER1);

    index = UsePos.indexOf(HyPos::UP1ST);
    if(index < 0) return false;
    UsePos.insert(index, HyPos::STOPOVER3);

    index = UsePos.indexOf(HyPos::STOPOVER3);
    if(index < 0) return false;
    UsePos.insert(index, HyPos::STOPOVER2);

    //4. insert home.
    UsePos.push_front(HyPos::HOME);

    //5. selected delete
    //5-1. INSERT_UNLOAD
    index = UsePos.indexOf(HyPos::INSERT_UNLOAD);
    if(index >= 0)
        UsePos.remove(index);
    //5-2. EXT_WAIT
    index = UsePos.indexOf(HyPos::EXT_WAIT);
    if(index >= 0)
        UsePos.remove(index);


    //6. transfer index to pos_name & disp_name
    QStringList temp_list, temp_list2;
    temp_list.clear();
    temp_list2.clear();
    for(i=0;i<UsePos.count();i++)
    {
        temp_list.append(toPosName_Trans(UsePos[i]));
        temp_list2.append(toDispName(UsePos[i]));
    }

    index_list = UsePos;

#ifdef _H6_
    temp_list.replaceInStrings("tpos[5]", "#jpos[5]");
#endif

    qDebug() << temp_list;
    qDebug() << temp_list2;
    qDebug() << index_list;

    pos_list = temp_list;
    disp_list = temp_list2;
    return true;
}

bool dialog_easy_setting::Save_StopoverPos
(int StopoverNum, HyPos::ENUM_POS_ARRAY start_index, HyPos::ENUM_POS_ARRAY end_index)
{
    QStringList vars;
    QList<cn_variant> values;
    cn_variant val;

    // 1. get start,end index trans, joint values.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(start_index));
    vars.append(toPosName_Joint(start_index));
    vars.append(toPosName_Trans(end_index));
    vars.append(toPosName_Joint(end_index));

    if(!Recipe->GetVars(vars, values))
        return false;

    for(int i=0;i<values.count();i++)
    {
        if(values[i].type < 0)
            return false;
    }

    //2. making stopover postion.
    cn_trans start, end, stopover;
    cn_joint jstart, jstopover;
    start = values[0].val.trans;
    end = values[2].val.trans;
    jstart = values[1].val.joint;

    stopover = start;
    stopover.p[UPDN] = end.p[UPDN];
    if(!Posi->Trans2Joint(stopover, jstart, jstopover))
        return false;

    //3. Save stopover postion.
    int stopover_index;
    if(StopoverNum < 1)
        stopover_index = HyPos::STOPOVER1;
    else
        stopover_index = HyPos::STOPOVER2;

    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(stopover_index));
    val.type = CNVAR_TRANS;
    val.val.trans = stopover;
    values.append(val);

    vars.append(toPosName_Joint(stopover_index));
    val.type = CNVAR_JOINT;
    val.val.joint = jstopover;
    values.append(val);

    if(!Recipe->SetVars(vars, values, false))
        return false;

    return true;
}

bool dialog_easy_setting::Save_StopoverPos1
(cn_trans real_trans, cn_joint real_joint,
 HyPos::ENUM_POS_ARRAY end_index)
{
    QStringList vars;
    QList<cn_variant> values;
    cn_variant val;

    // 1. get end index trans, joint values.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(end_index));
    vars.append(toPosName_Joint(end_index));

    if(!Recipe->GetVars(vars, values))
        return false;

    for(int i=0;i<values.count();i++)
    {
        if(values[i].type < 0)
            return false;
    }

    //2. making stopover postion.
    cn_trans start, end, stopover;
    cn_joint jstart, jstopover;
    start = real_trans;
    end = values[0].val.trans;
    jstart = real_joint;

    stopover = start;
    stopover.p[UPDN] = end.p[UPDN];
    if(!Posi->Trans2Joint(stopover, jstart, jstopover))
        return false;

    //3. Save stopover postion.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(HyPos::STOPOVER1));
    val.type = CNVAR_TRANS;
    val.val.trans = stopover;
    values.append(val);

    vars.append(toPosName_Joint(HyPos::STOPOVER1));
    val.type = CNVAR_JOINT;
    val.val.joint = jstopover;
    values.append(val);

    if(!Recipe->SetVars(vars, values, false))
        return false;

    return true;
}

bool dialog_easy_setting::ASave_StopoverPos1
(cn_trans real_trans, cn_joint real_joint,
 int end_index, bool is_bwd)
{
    QStringList vars;
    QList<cn_variant> values;
    cn_variant val;

    // 1. get end index trans, joint values.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(end_index));
    vars.append(toPosName_Joint(end_index));

    if(!Recipe->GetVars(vars, values))
        return false;

    for(int i=0;i<values.count();i++)
    {
        if(values[i].type < 0)
            return false;
    }

    //2. making stopover postion.
    cn_trans start, end, stopover;
    cn_joint jstart, jstopover;
    if(!is_bwd)
    {
        start = real_trans;
        end = values[0].val.trans;
        jstart = real_joint;
    }
    else
    {
        start = values[0].val.trans;
        end = real_trans;
        jstart = values[1].val.joint;
    }

    //stopover = start;
    //stopover.p[UPDN] = end.p[UPDN];
    stopover = end;
    stopover.p[FWDBWD] = start.p[FWDBWD];
    if(!Posi->Trans2Joint(stopover, jstart, jstopover))
        return false;

    //3. Save stopover postion.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(HyPos::STOPOVER1));
    val.type = CNVAR_TRANS;
    val.val.trans = stopover;
    values.append(val);

    vars.append(toPosName_Joint(HyPos::STOPOVER1));
    val.type = CNVAR_JOINT;
    val.val.joint = jstopover;
    values.append(val);

    if(!Recipe->SetVars(vars, values, false))
        return false;

    return true;
}

bool dialog_easy_setting::Save_StopoverPos2
(cn_trans real_trans, cn_joint real_joint,
 HyPos::ENUM_POS_ARRAY end_index)
{
    QStringList vars;
    QList<cn_variant> values;
    cn_variant val;

    // 1. get end index trans, joint values.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(end_index));
    vars.append(toPosName_Joint(end_index));

    if(!Recipe->GetVars(vars, values))
        return false;

    for(int i=0;i<values.count();i++)
    {
        if(values[i].type < 0)
            return false;
    }

    //2. making stopover postion.
    //2-1. making stopover2
    cn_trans start, end, stopover;
    cn_joint jstart, jstopover;
    start = real_trans;
    end = values[0].val.trans;
    jstart = real_joint;

    stopover = start; //X
    stopover.p[TRAV] = end.p[TRAV];
    stopover.p[FWDBWD] = end.p[FWDBWD];
    if(!Posi->Trans2Joint(stopover, jstart, jstopover))
        return false;

    //2-2 making stopover3
    cn_trans stopover3;
    cn_joint jstopover3;

    stopover3 = start;
    stopover3.p[TRAV] = end.p[TRAV];
    stopover3.p[FWDBWD] = end.p[FWDBWD];
    stopover3.p[UPDN] = end.p[UPDN];
    if(!Posi->Trans2Joint(stopover3, jstart, jstopover3))
        return false;

    //3. Save stopover2 & stopover3 postion.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(HyPos::STOPOVER2));
    val.type = CNVAR_TRANS;
    val.val.trans = stopover;
    values.append(val);

    vars.append(toPosName_Joint(HyPos::STOPOVER2));
    val.type = CNVAR_JOINT;
    val.val.joint = jstopover;
    values.append(val);

    vars.append(toPosName_Trans(HyPos::STOPOVER3));
    val.type = CNVAR_TRANS;
    val.val.trans = stopover3;
    values.append(val);

    vars.append(toPosName_Joint(HyPos::STOPOVER3));
    val.type = CNVAR_JOINT;
    val.val.joint = jstopover3;
    values.append(val);

    if(!Recipe->SetVars(vars, values, false))
        return false;

    return true;
}

bool dialog_easy_setting::ASave_StopoverPos2
(cn_trans real_trans, cn_joint real_joint,
 int end_index, bool is_bwd)
{
    QStringList vars;
    QList<cn_variant> values;
    cn_variant val;

    // 1. get end index trans, joint values.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(end_index));
    vars.append(toPosName_Joint(end_index));

    if(!Recipe->GetVars(vars, values))
        return false;

    for(int i=0;i<values.count();i++)
    {
        if(values[i].type < 0)
            return false;
    }

    //2. making stopover postion.
    //2-1. making stopover2
    cn_trans start, end, stopover;
    cn_joint jstart, jstopover;
    if(!is_bwd)
    {
        start = real_trans;
        end = values[0].val.trans;
        jstart = real_joint;
    }
    else
    {
        start = values[0].val.trans;
        end = real_trans;
        jstart = values[1].val.joint;
    }

    stopover = start; //X
    stopover.p[TRAV] = end.p[TRAV];
    stopover.p[FWDBWD] = end.p[FWDBWD];
    if(!Posi->Trans2Joint(stopover, jstart, jstopover))
        return false;

    //2-2 making stopover3
    cn_trans stopover3;
    cn_joint jstopover3;

    stopover3 = start;
    stopover3.p[TRAV] = end.p[TRAV];
    stopover3.p[FWDBWD] = end.p[FWDBWD];
    stopover3.p[UPDN] = end.p[UPDN];
    if(!Posi->Trans2Joint(stopover3, jstart, jstopover3))
        return false;

    //3. Save stopover2 & stopover3 postion.
    vars.clear();
    values.clear();

    vars.append(toPosName_Trans(HyPos::STOPOVER2));
    val.type = CNVAR_TRANS;
    val.val.trans = stopover;
    values.append(val);

    vars.append(toPosName_Joint(HyPos::STOPOVER2));
    val.type = CNVAR_JOINT;
    val.val.joint = jstopover;
    values.append(val);

    vars.append(toPosName_Trans(HyPos::STOPOVER3));
    val.type = CNVAR_TRANS;
    val.val.trans = stopover3;
    values.append(val);

    vars.append(toPosName_Joint(HyPos::STOPOVER3));
    val.type = CNVAR_JOINT;
    val.val.joint = jstopover3;
    values.append(val);

    if(!Recipe->SetVars(vars, values, false))
        return false;

    return true;
}


QString dialog_easy_setting::toDispName(int index)
{
    QString name;
    int user_index;

    if(index < USER_POS_KNOW_OFFSET)
    {
        // hypos.
        name = Posi->GetName(index);
    }
    else
    {
        //hyuserdata.
        if(index < USER_POS_KNOW_OFFSET*2)
        {
            // user pos
            user_index = HyStepData::ADD_POS0 + (index - USER_POS_KNOW_OFFSET);
            name = StepEdit->m_StepData.Step[user_index].DispName;
        }
        else
        {
            // s-work pos
            user_index = HyStepData::ADD_WORK0 + (index - (USER_POS_KNOW_OFFSET*2));
            name = StepEdit->m_StepData.Step[user_index].DispName;
        }
    }

    return name;
}


QString dialog_easy_setting::toPosName_Trans(int index)
{
    QString name;
    int no;

    if(index < USER_POS_KNOW_OFFSET)
    {
        // hypos.
        name = Posi->m_strArrName_TPos;
        no = index;
    }
    else
    {
        //hyuserdata.
        if(index < USER_POS_KNOW_OFFSET*2)
        {
            // user pos
            name = UserData->m_UserPos_VarsName[0];
            no = index - USER_POS_KNOW_OFFSET;
        }
        else
        {
            // s-work pos
            name = UserData->m_UserWork_VarsName[0];
            no = index - (USER_POS_KNOW_OFFSET*2);
        }
    }

    name += "[";
    name += QString().setNum(no);
    name += "]";
    return name;
}

QString dialog_easy_setting::toPosName_Joint(int index)
{
    QString name;
    int no;

    if(index < USER_POS_KNOW_OFFSET)
    {
        // hypos.
        name = Posi->m_strArrName_JPos;
        name.push_front("#");
        no = index;
    }
    else
    {
        //hyuserdata.
        if(index < USER_POS_KNOW_OFFSET*2)
        {
            // user pos
            name = UserData->m_UserPos_VarsName[1];
            no = index - USER_POS_KNOW_OFFSET;
        }
        else
        {
            // s-work pos
            name = UserData->m_UserWork_VarsName[1];
            no = index - (USER_POS_KNOW_OFFSET*2);
        }
    }

    name += "[";
    name += QString().setNum(no);
    name += "]";
    return name;
}


void dialog_easy_setting::Redraw_IndexTable(int index)
{
    // origin routine.
    int ref_index = 2; // center index.

    int offset_index, cal_index;
    QString name;

    for(int i=0;i<m_vtTbIndex.count();i++)
    {
        offset_index = i - ref_index;

        cal_index = index + offset_index;

        if(cal_index < 0)
            name.clear();
        else if(cal_index >= m_DispList.size())
            name.clear();
        else
            name = m_DispList[cal_index];

        m_vtTbIndex[i]->setText(name);
    }

    Redraw_SetPos(index);
}

void dialog_easy_setting::Redraw_SetPos(int index)
{
    if(index < 0) return;
    if(index >= m_IndexList.size()) return;

    int pos_index = m_IndexList[index];
    int temp_index;
    cn_trans trans;
    cn_joint joint;
    float speed, delay; // no use.
    ST_USERDATA_POS userpos;
    ST_USERDATA_WORK userwork;

    if(pos_index < USER_POS_KNOW_OFFSET)
    {
        if(!Posi->RD(pos_index, trans, joint, speed, delay))
        {
            // error...
            return;
        }
    }
    else
    {
        if(pos_index < (USER_POS_KNOW_OFFSET*2))
        {
            temp_index = pos_index - USER_POS_KNOW_OFFSET;
            if(!UserData->RD(temp_index, &userpos))
            {
                // error...
                return;
            }
            trans = userpos.Trans;
            joint = userpos.Joint;
        }
        else
        {
            temp_index = pos_index - (USER_POS_KNOW_OFFSET*2);
            if(!UserData->RD(temp_index, &userwork))
            {
                // error...
                return;
            }
            trans = userwork.Trans;
            joint = userwork.Joint;
        }
    }

    m_Trans = trans;
    m_Joint = joint;
    Display_SetPos(m_Trans, m_Joint);

    // control enable to set button.
    int i = m_NoSetIndexList.indexOf(pos_index);
    Enable_BtnSet(i < 0);
    Enable_BtnJog(i < 0);

}

void dialog_easy_setting::Enable_Control(bool on)
{
    ui->wigFwdBwd->setEnabled(on);
}
void dialog_easy_setting::Enable_BtnSet(bool on)
{
    ui->wigBtnSet->setEnabled(on);
}
void dialog_easy_setting::Enable_BtnJog(bool on)
{
    ui->wigBtnJog->setEnabled(on);
}

void dialog_easy_setting::Display_RealPos(float *t, float *j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, t[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, t[UPDN]);
    m_vtReal[2]->setText(str);

    cn_joint _joint = gMakeCnJoint(j);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, _joint));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(JSWV, _joint));
    m_vtReal[4]->setText(str);
    str.sprintf(FORMAT_POS, gGetJoint2Disp(J6, _joint));
    m_vtReal[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, t[TRAV]);
    m_vtReal[0]->setText(str);
    str.sprintf(FORMAT_POS, t[FWDBWD]);
    m_vtReal[1]->setText(str);
    str.sprintf(FORMAT_POS, t[UPDN]);
    m_vtReal[2]->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(j[J2], j[J3], j[J4]));
    m_vtReal[3]->setText(str);
    str.sprintf(FORMAT_POS, j[JSWV]);
    m_vtReal[4]->setText(str);
    str.clear();
    m_vtReal[5]->setText(str);
#endif

}

void dialog_easy_setting::Make_RealPos(float *t, float *j)
{
    int i;
    for(i=0;i<3;i++)
    {
        m_RealTrans.p[i] = t[i];
        m_RealTrans.eu[i] = t[3+i];
    }

    for(i=0;i<MAX_AXIS_NUM;i++)
    {
        if(i < USE_AXIS_NUM)
            m_RealJoint.joint[i] = j[i];
        else
            m_RealJoint.joint[i] = 0.0;
    }
}

void dialog_easy_setting::Display_SetPos(cn_trans t, cn_joint j)
{
    QString str;

#ifdef _H6_
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtSet[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtSet[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtSet[2]->setText(str);

    str.sprintf(FORMAT_POS, gGetJoint2Disp(JROT, j));
    m_vtSet[3]->setText(str);
    str.sprintf(FORMAT_POS ,gGetJoint2Disp(JSWV, j));
    m_vtSet[4]->setText(str);
    str.sprintf(FORMAT_POS ,gGetJoint2Disp(J6, j));
    m_vtSet[5]->setText(str);
#else
    str.sprintf(FORMAT_POS, t.p[TRAV]);
    m_vtSet[0]->setText(str);
    str.sprintf(FORMAT_POS, t.p[FWDBWD]);
    m_vtSet[1]->setText(str);
    str.sprintf(FORMAT_POS, t.p[UPDN]);
    m_vtSet[2]->setText(str);

    str.sprintf(FORMAT_POS, gGetWristAngle(j));
    m_vtSet[3]->setText(str);
    str.sprintf(FORMAT_POS ,j.joint[JSWV]);
    m_vtSet[4]->setText(str);
    str.clear();
    m_vtSet[5]->setText(str);
#endif

}

bool dialog_easy_setting::SavePos(int index, cn_trans real_trans, cn_joint real_joint)
{
    if(index < 0) return false;
    if(index >= m_IndexList.size()) return false;

    int pos_index = m_IndexList[index];
    int check = m_NoSetIndexList.indexOf(pos_index);
    if(check >= 0) return false;

    ST_USERDATA_POS userpos;
    ST_USERDATA_WORK userwork;
    int temp_pos_index;

    if(pos_index < USER_POS_KNOW_OFFSET)
    {
        if(!Posi->WR(pos_index, real_trans, real_joint))
            return false;
    }
    else
    {
        if(pos_index < (USER_POS_KNOW_OFFSET*2))
        {
            temp_pos_index = pos_index - USER_POS_KNOW_OFFSET;
            if(!UserData->RD(temp_pos_index, &userpos))
                return false;

            userpos.Trans = real_trans;
            userpos.Joint = real_joint;

            if(!UserData->WR(temp_pos_index, userpos))
                return false;
        }
        else
        {
            temp_pos_index = pos_index - (USER_POS_KNOW_OFFSET*2);
            if(!UserData->RD(temp_pos_index, &userwork))
                return false;

            userwork.Trans = real_trans;
            userwork.Joint = real_joint;

            if(!UserData->WR(temp_pos_index, userwork))
                return false;
        }
    }

    return true;
}

bool dialog_easy_setting::CopyPos(HyPos::ENUM_POS_ARRAY source, HyPos::ENUM_POS_ARRAY target)
{
    cn_trans trans;
    cn_joint joint;

    if(!Posi->RD(source, trans, joint))
        return false;

    if(!Posi->WR(target, trans, joint))
        return false;

    return true;
}

void dialog_easy_setting::Display_FwdBwding(bool fwd, bool bwd)
{

    if(!fwd && bwd)
    {
        // fwding...
        ui->wigFwding->setVisible(true);
        ui->wigBwding->setVisible(false);

    }
    else if(fwd && !bwd)
    {
        // bwding...
        ui->wigFwding->setVisible(false);
        ui->wigBwding->setVisible(true);
    }
    else
    {
        // none
        ui->wigFwding->setVisible(false);
        ui->wigBwding->setVisible(false);
    }


    for(int i=0;i<m_vtFwding.count();i++)
    {
        if(ui->wigFwding->isVisible())
            m_vtFwding[i]->setVisible(i == m_nFwdBwding);
        if(ui->wigBwding->isVisible())
            m_vtBwding[i]->setVisible(i == m_nFwdBwding);
    }

    m_nFwdBwding++;
    if(m_nFwdBwding >= m_vtFwding.count())
        m_nFwdBwding = 0;
}
void dialog_easy_setting::Display_FwdBwding2(bool fwd, bool bwd)
{
    ui->wigFwding->setVisible(false);
    ui->wigBwding->setVisible(false);

    for(int i=0;i<m_vtFwding2.count();i++)
    {
        if(!fwd && bwd)
            m_vtFwding2[i]->setVisible(i == m_nFwdBwding2);
        else
            m_vtFwding2[i]->setVisible(true);

        if(fwd && !bwd)
            m_vtBwding2[i]->setVisible(i == m_nFwdBwding2);
        else
            m_vtBwding2[i]->setVisible(true);
    }

    m_nFwdBwding2++;
    if(m_nFwdBwding2 >= m_vtFwding2.count())
        m_nFwdBwding2 = 0;
}
void dialog_easy_setting::Display_FwdBwding2_1(bool fwd, bool bwd)
{
    QImage img;
    if(pxmFwding.isEmpty())
    {
        pxmFwding.clear();
        img.load(":/icon/icon/icon0-20.png");
        pxmFwding.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon0-21.png");
        pxmFwding.append(QPixmap::fromImage(img));
    }
    if(pxmBwding.isEmpty())
    {
        pxmBwding.clear();
        img.load(":/icon/icon/icon0-18.png");
        pxmBwding.append(QPixmap::fromImage(img));
        img.load(":/icon/icon/icon0-19.png");
        pxmBwding.append(QPixmap::fromImage(img));
    }

    for(int i=0;i<m_vtFwding2.count();i++)
    {
        if(!fwd && bwd)
            m_vtFwding2[i]->setPixmap(pxmFwding[(i == m_nFwdBwding2)]);
        else
            m_vtFwding2[i]->setPixmap(pxmFwding[0]);

        if(fwd && !bwd)
            m_vtBwding2[i]->setPixmap(pxmBwding[(i == m_nFwdBwding2)]);
        else
            m_vtBwding2[i]->setPixmap(pxmBwding[0]);
    }

    m_nFwdBwding2++;
    if(m_nFwdBwding2 >= m_vtFwding2.count())
        m_nFwdBwding2 = 0;
}

void dialog_easy_setting::Display_BtnJog(bool on)
{
    ui->btnJogIcon->setAutoFillBackground(on);
}
void dialog_easy_setting::Enter_Jog(bool enter)
{
    if(enter)
    {
        // enter jog
        Display_BtnJog(true);
        SidePanel->setCurrentIndex(1);
        SetEnableON(true);
    }
    else
    {
        // out jog.
        Display_BtnJog(false);
        SidePanel->setCurrentIndex(0);
        SetEnableON(false);
        Speed(m_Speed);
    }
}

void dialog_easy_setting::onJog()
{
    if(!ui->wigBtnSet->isEnabled())
        return;

    if(IsJog())
        Enter_Jog(false);
    else
        Enter_Jog(true);
}

bool dialog_easy_setting::IsJog()
{
    return (SidePanel->currentIndex() == 1);
}

void dialog_easy_setting::onSet()
{
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(ui->btnSet->text());
    conf->Message(tr("Do you want to set this position?"), m_DispList[m_nIndex]);
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!SavePos(m_nIndex, m_RealTrans, m_RealJoint))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Error")));
        msg->Message(tr("Fail to save position!"),
                     m_DispList[m_nIndex]);
        msg->exec();
        return;
    }


    // special position functions.
    int pos_index = m_IndexList[m_nIndex];

    /*// 1. stopover -> wait & takeout (must)
    if(pos_index == HyPos::WAIT || pos_index == HyPos::TAKE_OUT)
    {
        if(!Save_StopoverPos())
        {
            msg->SetColor(dialog_message::RED);
            msg->Title(QString(tr("Error")));
            msg->Message(tr("Fail to auto save position!"),
                         Posi->GetName(HyPos::STOPOVER1));
            msg->exec();
        }
    }*/

    // 2. wait position -> up position same set. (optional)

    if(pos_index == HyPos::WAIT)
    {
        conf->SetColor(dialog_confirm::GREEN);
        conf->Title(ui->btnSet->text());
        conf->Message(tr("Do you want to set it to the same position?"), Posi->GetName(HyPos::UP1ST));
        if(conf->exec() == QDialog::Accepted)
        {
            if(!CopyPos())
            {
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("Error")));
                msg->Message(tr("Fail to save position!"),
                             Posi->GetName(HyPos::UP1ST));
                msg->exec();
            }
        }
    }

    // finish
    m_bIndexComp_Fwd = true;
    m_bIndexComp_Bwd = true;
    Redraw_IndexTable(m_nIndex);
    m_bStopover1 = true; // enable to calculate stopover1
    m_bStopover2 = true; // enable to calculate stopover2
}

void dialog_easy_setting::onSpdUp()
{
    if(m_Speed > EASYSET_SPEED_MAX)
        return;

    m_Speed += EASYSET_SPEED_GAP;
    Speed(m_Speed);
}
void dialog_easy_setting::onSpdDown()
{
    if(m_Speed <= EASYSET_SPEED_MIN)
        return;

    m_Speed -= EASYSET_SPEED_GAP;
    Speed(m_Speed);
}

void dialog_easy_setting::Display_Speed(float speed)
{
    if(speed < 0) speed = 0;
    else if(speed >= 100.0) speed = 100.0;

    ui->pbarSpeed->setValue((int)speed);
}

void dialog_easy_setting::onIO()
{
    if(dig_easy_setting_io == 0)
        dig_easy_setting_io = new dialog_easy_setting_io();

    dig_easy_setting_io->exec();

    m_bDualSolOff = true;
}


void dialog_easy_setting::SubTop()
{
    if(top != 0) return;

    top = new top_sub(this);
    top->setGeometry(MAX_WIDTH-top->width(),0,top->width(),top->height());
    top->show();
}
