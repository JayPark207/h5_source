#ifndef DIALOG_LANGUAGE_H
#define DIALOG_LANGUAGE_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "top_sub.h"

namespace Ui {
class dialog_language;
}

class dialog_language : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_language(QWidget *parent = 0);
    ~dialog_language();

public slots:

    void onClose();
    void onSelect();

private:
    Ui::dialog_language *ui;

    QVector<QLabel4*> m_vtLanPic;
    QVector<QLabel3*> m_vtLanIcon;
    QVector<QLabel3*> m_vtLan;

    void Update();
    void Display_Select(int lang_index);

    top_sub* top;
    void SubTop();          // Need SubTop

protected:
     void changeEvent(QEvent *);
     void showEvent(QShowEvent *);
     void hideEvent(QHideEvent *);
};

#endif // DIALOG_LANGUAGE_H
