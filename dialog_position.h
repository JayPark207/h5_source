#ifndef DIALOG_POSITION_H
#define DIALOG_POSITION_H

#include <QDialog>

#include "global.h"

#include "qlabel3.h"
#include "dialog_numpad.h"
#include "dialog_position_teaching2.h"

#include "top_sub.h"

namespace Ui {
class dialog_position;
}

class dialog_position : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_position(QWidget *parent = 0);
    ~dialog_position();

    void Update_List();
    void Redraw_List(int start_index);
    void Display_Data(int list_index);
    QVector<int> m_UsePos;

public slots:
    void onClose();

    void onList();
    void onListUp();
    void onListDown();

    void onTeach();

private:
    Ui::dialog_position *ui;

    QVector<QLabel3*> m_vtList;

    int m_nStartIndex;
    int m_nSelectedIndex;

    void ListOn(int table_index);
    void ListOff();

    top_sub* top;
    void SubTop();          // Need SubTop

    void Display_SetPos(cn_trans t, cn_joint j);

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_POSITION_H
