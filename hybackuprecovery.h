#ifndef HYBACKUPRECOVERY_H
#define HYBACKUPRECOVERY_H

#include <QObject>
#include <QStringList>
#include <QDebug>
#include <QDateTime>

#include "defines.h"

#include "cnrobo.h"
#include "cntype.h"
#include "cnhelper.h"
#include "cnerror.h"


/** for backup & recovery functions **/
class HyBackupRecovery : public QObject
{
    Q_OBJECT

public:
    HyBackupRecovery();

    //for backup
    QString MakeDirName(QString input_name);

    bool IsExist(QString path);
    bool MakeDir(QString path);

    bool Backup_Recipe(QString tp_recipe_path, QString sd_recipe_path, QString target_path);
    bool Backup_Param(QString target_path);
    bool Backup_Config(QString target_path);

    //for recovery
    bool Read_BackupDir();
    QStringList m_BackupList;       // this is backup dir path.
    bool Parsing_BackupDir(QString backup_list_data,
                           QString& date, QString& time, QString& name);

    // backup_list_data = m_BackupList[] : this is backup detail dir. path.

    //for recovery2
    bool Read_Recipe(QString backup_list_data); // recipe dir & info.
    QStringList m_RecipeNo, m_RecipeName, m_RecipeDate; // m_RecipeNo is reference.
    bool HasParam(QString backup_list_data);
    bool HasConfig(QString backup_list_data);

    // for recovery functions
    //bool Recovery_Recipe(QString backup_list_data, QStringList& reciep_no_list);   // All list
    bool Recovery_Recipe(QString backup_list_data, QString recipe_no);            // each
    bool Recovery_Param(QString backup_list_data);
    bool Recovery_Config(QString backup_list_data);



private:

};

#endif // HYBACKUPRECOVERY_H
