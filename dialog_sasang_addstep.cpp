#include "dialog_sasang_addstep.h"
#include "ui_dialog_sasang_addstep.h"

dialog_sasang_addstep::dialog_sasang_addstep(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_sasang_addstep)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtTbNo.clear();m_vtTbGrp.clear();m_vtTbDate.clear();
    m_vtTbNo.append(ui->tbNo);  m_vtTbGrp.append(ui->tbGrp);  m_vtTbDate.append(ui->tbDate);
    m_vtTbNo.append(ui->tbNo_2);m_vtTbGrp.append(ui->tbGrp_2);m_vtTbDate.append(ui->tbDate_2);
    m_vtTbNo.append(ui->tbNo_3);m_vtTbGrp.append(ui->tbGrp_3);m_vtTbDate.append(ui->tbDate_3);
    m_vtTbNo.append(ui->tbNo_4);m_vtTbGrp.append(ui->tbGrp_4);m_vtTbDate.append(ui->tbDate_4);
    m_vtTbNo.append(ui->tbNo_5);m_vtTbGrp.append(ui->tbGrp_5);m_vtTbDate.append(ui->tbDate_5);
    m_vtTbNo.append(ui->tbNo_6);m_vtTbGrp.append(ui->tbGrp_6);m_vtTbDate.append(ui->tbDate_6);
    m_vtTbNo.append(ui->tbNo_7);m_vtTbGrp.append(ui->tbGrp_7);m_vtTbDate.append(ui->tbDate_7);
    m_vtTbNo.append(ui->tbNo_8);m_vtTbGrp.append(ui->tbGrp_8);m_vtTbDate.append(ui->tbDate_8);

    int i;
    for(i=0;i<m_vtTbNo.count();i++)
    {
        connect(m_vtTbNo[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtTbGrp[i],SIGNAL(mouse_release()),m_vtTbNo[i],SIGNAL(mouse_release()));
        connect(m_vtTbDate[i],SIGNAL(mouse_release()),m_vtTbNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));

    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));


}

dialog_sasang_addstep::~dialog_sasang_addstep()
{
    delete ui;
}
void dialog_sasang_addstep::showEvent(QShowEvent *)
{
    Update();
}
void dialog_sasang_addstep::hideEvent(QHideEvent *)
{

}
void dialog_sasang_addstep::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_sasang_addstep::onOK()
{
    emit accept();
}
void dialog_sasang_addstep::onCancel()
{
    emit reject();
}

void dialog_sasang_addstep::Update()
{
    if(Sasang->Group->ReadAll() < 0)
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Read Error!")),
                     QString(tr("Please, try again")));
        msg->exec();
        return;
    }

    m_nStartIndex=0;m_nSelectedIndex=0;
    Redraw_List(m_nStartIndex);
}

void dialog_sasang_addstep::Redraw_List(int start_index)
{
    int cal_size = Sasang->Group->GroupList.size() - start_index;
    int index;
    QString no,grp,date;

    Display_ListOn(-1); // all off

    for(int i=0;i<m_vtTbNo.count();i++)
    {
        if(cal_size > i)
        {
            // have data.
            index = start_index + i;
            no = QString().setNum(index + 1);
            grp = Sasang->Group->GroupList[index].name;
            date = Sasang->Group->GroupList[index].datetime.toString("yyyy.MM.dd hh:mm:ss");
        }
        else
        {
            // no data.
            no.clear();
            grp.clear();
            date.clear();
        }

        m_vtTbNo[i]->setText(no);
        m_vtTbGrp[i]->setText(grp);
        m_vtTbDate[i]->setText(date);

        if((start_index + i) == m_nSelectedIndex)
            Display_ListOn(i);
    }

    // display list size.
    ui->lbListSize->setText(QString().setNum(Sasang->Group->GroupList.size()));
    Display_PatternNo(m_nSelectedIndex);
}

void dialog_sasang_addstep::Display_ListOn(int table_index)
{
    for(int i=0;i<m_vtTbNo.count();i++)
    {
        m_vtTbNo[i]->setAutoFillBackground(i==table_index);
        m_vtTbGrp[i]->setAutoFillBackground(i==table_index);
        m_vtTbDate[i]->setAutoFillBackground(i==table_index);
    }
}

void dialog_sasang_addstep::onList()
{
    QLabel3* sel = (QLabel3*)sender();

    int index = m_vtTbNo.indexOf(sel);
    if(index < 0) return;

    int list_index = m_nStartIndex + index;
    if(list_index == m_nSelectedIndex) return;

    if(list_index < Sasang->Group->GroupList.size())
    {
        m_nSelectedIndex = list_index;
        Redraw_List(m_nStartIndex);
    }
}
void dialog_sasang_addstep::onUp()
{
    if(m_nStartIndex <= 0)
        return;

    Redraw_List(--m_nStartIndex);
}
void dialog_sasang_addstep::onDown()
{
    if(m_nStartIndex >= (Sasang->Group->GroupList.size() - m_vtTbNo.size()))
        return;

    Redraw_List(++m_nStartIndex);
}

bool dialog_sasang_addstep::GetSelected(ST_SSGROUP_DATA &data)
{
    if(Sasang->Group->GroupList.isEmpty()) return false;
    if(Sasang->Group->GroupList.size() <= m_nSelectedIndex) return false;

    data = Sasang->Group->GroupList[m_nSelectedIndex];
    return true;
}

void dialog_sasang_addstep::Display_PatternNo(int select_index)
{
    int pat_no;
    if(Sasang->Group->GroupList.size() <= select_index)
        pat_no = 0;
    else
        pat_no = Sasang->Group->GroupList[select_index].data.size();

    ui->tbPatNo->setText(QString().setNum(pat_no));
}
