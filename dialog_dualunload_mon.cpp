#include "dialog_dualunload_mon.h"
#include "ui_dialog_dualunload_mon.h"

dialog_dualunload_mon::dialog_dualunload_mon(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_dualunload_mon)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(1000);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtCurrPic.clear();                 m_vtCurr.clear();
    m_vtCurrPic.append(ui->btnCurrPic);  m_vtCurr.append(ui->btnCurr);
    m_vtCurrPic.append(ui->btnCurrPic_2);m_vtCurr.append(ui->btnCurr_2);

    int i;
    for(i=0;i<m_vtCurrPic.count();i++)
    {
        connect(m_vtCurrPic[i],SIGNAL(mouse_release()),this,SLOT(onCurr()));
        connect(m_vtCurr[i],SIGNAL(mouse_press()),m_vtCurrPic[i],SLOT(press()));
        connect(m_vtCurr[i],SIGNAL(mouse_release()),m_vtCurrPic[i],SLOT(release()));
    }

}

dialog_dualunload_mon::~dialog_dualunload_mon()
{
    delete ui;
}
void dialog_dualunload_mon::showEvent(QShowEvent *)
{
    Update();
    timer->start();
    bSave = false;
}
void dialog_dualunload_mon::hideEvent(QHideEvent *)
{
    timer->stop();

    if(bSave)
        Recipe->SaveVariable();
}
void dialog_dualunload_mon::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_dualunload_mon::onClose()
{
    emit accept();
}

void dialog_dualunload_mon::onTimer()
{
    Update();
}

void dialog_dualunload_mon::Update()
{
    vars.clear();
    vars.append(HyRecipe::mdDualUnload);
    vars.append(HyRecipe::vDualCurr);
    //vars.append(HyRecipe::vDualRevX);
    //vars.append(HyRecipe::vDualRevY);
    //vars.append(HyRecipe::vDualRevZ);

    if(Recipe->Gets(vars, datas))
    {
        Display_Use(datas[0]);
        Display_Curr(datas[1]);

        ui->wigCurr->setEnabled((int)datas[0] > 0);
    }
}

void dialog_dualunload_mon::Display_Use(float data)
{
    ui->wigUse->setEnabled((int)data > 0);
}
void dialog_dualunload_mon::Display_Curr(float data)
{
    m_vtCurr[0]->setAutoFillBackground(data < 0.5);
    m_vtCurr[1]->setAutoFillBackground(data > 0.5);
}

void dialog_dualunload_mon::onCurr()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtCurrPic.indexOf(btn);
    if(index < 0) return;

    if(m_vtCurr[index]->autoFillBackground())
        return;

    timer->stop();

    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GRAY);
    conf->Title(tr("Change Unload Position"));
    QString str;
    str.clear();
    if(index == 0)
        str = m_vtCurr[1]->text() + " -> " + m_vtCurr[0]->text();
    else
        str = m_vtCurr[0]->text() + " -> " + m_vtCurr[1]->text();
    conf->Message(tr("Do you want to change unload position?"), str);
    if(conf->exec() != QDialog::Accepted)
    {
        timer->start();
        return;
    }

    bSave = true;
    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!Recipe->Set(HyRecipe::vDualCurr, index, false))
    {
        // error
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to change unload position!")),
                     QString(tr("Plase try again!!")));
        msg->exec();

        Update();
        timer->start();
        return;
    }

    // if array unload , clear array info. or not
    float data;
    if(!Recipe->Get(HyRecipe::mdProdUnload, &data))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("ERROR")));
        msg->Message(QString(tr("Fail to change unload position!")),
                     QString(tr("Plase try again!!")));
        msg->exec();

        Update();
        timer->start();
        return;
    }

    if((int)data == 2 || (int)data == 4)
    {
        conf->SetColor(dialog_confirm::GRAY);
        conf->Title(tr("Clear Array Data"));
        conf->Message(tr("Do you want to clear array data?"));
        if(conf->exec() == QDialog::Accepted)
        {
            // clear array data.
            QVector<HyRecipe::RECIPE_NUMBER> _var;
            QVector<float> _data;
            _var.clear();
            _var.append(HyRecipe::vArrXCurr);
            _var.append(HyRecipe::vArrYCurr);
            _var.append(HyRecipe::vArrZCurr);

            _data.clear();
            _data.append(0.0);
            _data.append(0.0);
            _data.append(0.0);

            if(!Recipe->Sets(_var, _data, false))
            {
                msg->SetColor(dialog_message::RED);
                msg->Title(QString(tr("ERROR")));
                msg->Message(QString(tr("Fail to clear array data!")),
                             QString(tr("Plase try again!!")));
                msg->exec();

                Update();
                timer->start();
                return;
            }

            // complete message.
            msg->SetColor(dialog_message::GRAY);
            msg->Title(QString(tr("Clear Array Data")));
            msg->Message(QString(tr("Success to clear array data!")));
            msg->exec();
        }

    }

    Update();
    timer->start();
    return;
}
