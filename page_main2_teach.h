#ifndef PAGE_MAIN2_TEACH_H
#define PAGE_MAIN2_TEACH_H

#include <QWidget>

#include "global.h"
#include "dialog_common_position.h"
#include "dialog_position.h"
#include "dialog_time.h"
#include "dialog_stepedit.h"
#include "dialog_mode_entry.h"
#include "dialog_easy_setting.h"
#include "dialog_mode_detail.h"

namespace Ui {
class page_main2_teach;
}

class page_main2_teach : public QWidget
{
    Q_OBJECT
    
public:
    explicit page_main2_teach(QWidget *parent = 0);
    ~page_main2_teach();

private slots:
    void onMode();
    void onStepEdit();
    void onPos();
    void onTimer();
    void onComPos();
    void onEasySet();
    
private:
    Ui::page_main2_teach *ui;

    dialog_common_position* dig_common_position;
    dialog_position* dig_position;
    dialog_time* dig_time;
    dialog_stepedit* dig_stepedit;
    dialog_mode_entry* dig_mode_entry;
    dialog_easy_setting* dig_easy_setting;
    dialog_mode_detail* dig_mode_detail;

    void UserLevel();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);

};

#endif // PAGE_MAIN2_TEACH_H
