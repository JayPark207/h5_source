#include "dialog_robot_notuse.h"
#include "ui_dialog_robot_notuse.h"

dialog_robot_notuse::dialog_robot_notuse(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_robot_notuse)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(200);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnResumePic,SIGNAL(mouse_release()),this,SLOT(onResume()));
    connect(ui->btnResume,SIGNAL(mouse_press()),ui->btnResumePic,SLOT(press()));
    connect(ui->btnResume,SIGNAL(mouse_release()),ui->btnResumePic,SLOT(release()));
    connect(ui->btnResumeIcon,SIGNAL(mouse_press()),ui->btnResumePic,SLOT(press()));
    connect(ui->btnResumeIcon,SIGNAL(mouse_release()),ui->btnResumePic,SLOT(release()));


    dig_home2 = 0;
}

dialog_robot_notuse::~dialog_robot_notuse()
{
    delete ui;
}
void dialog_robot_notuse::showEvent(QShowEvent *)
{
    Recipe->Set(HyRecipe::sNotUse, 1.0, false);

    m_nXpos = ui->lbDisplay->x();

    timer->start();
}
void dialog_robot_notuse::hideEvent(QHideEvent *)
{
    Recipe->Set(HyRecipe::sNotUse, 0, false);

    timer->stop();

    ui->lbDisplay->setGeometry(0,
                               ui->lbDisplay->y(),
                               ui->lbDisplay->width(),
                               ui->lbDisplay->height());
}
void dialog_robot_notuse::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_robot_notuse::onClose()
{
    emit accept();
}

bool dialog_robot_notuse::Home(bool bCheck)
{
    if(dig_home2 == 0)
        dig_home2 = new dialog_home2();

    if(bCheck)
        dig_home2->Enable_Check();
    int res = dig_home2->exec();

    if(res == QDialog::Accepted)
        return true;

    return false;
}

void dialog_robot_notuse::onResume()
{
    // confirm..
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::GREEN);
    conf->Title(tr("Confirm"));
    conf->Message(tr("Do you want to enable this robot?"));
    if(conf->exec() != QDialog::Accepted)
        return;

    onClose();
}

void dialog_robot_notuse::onTimer()
{
    m_nXpos += 20;
    if(m_nXpos > 700)
        m_nXpos = -700;

    ui->lbDisplay->setGeometry(m_nXpos,
                               ui->lbDisplay->y(),
                               ui->lbDisplay->width(),
                               ui->lbDisplay->height());
}
