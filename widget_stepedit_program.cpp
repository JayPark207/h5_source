#include "widget_stepedit_program.h"
#include "ui_widget_stepedit_program.h"

widget_stepedit_program::widget_stepedit_program(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::widget_stepedit_program)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    m_vtIcon.clear();              m_vtNo.clear();            m_vtText.clear();
    m_vtIcon.append(ui->lbIcon);   m_vtNo.append(ui->lbNo);   m_vtText.append(ui->lbText);
    m_vtIcon.append(ui->lbIcon_2); m_vtNo.append(ui->lbNo_2); m_vtText.append(ui->lbText_2);
    m_vtIcon.append(ui->lbIcon_3); m_vtNo.append(ui->lbNo_3); m_vtText.append(ui->lbText_3);
    m_vtIcon.append(ui->lbIcon_4); m_vtNo.append(ui->lbNo_4); m_vtText.append(ui->lbText_4);
    m_vtIcon.append(ui->lbIcon_5); m_vtNo.append(ui->lbNo_5); m_vtText.append(ui->lbText_5);
    m_vtIcon.append(ui->lbIcon_6); m_vtNo.append(ui->lbNo_6); m_vtText.append(ui->lbText_6);
    m_vtIcon.append(ui->lbIcon_7); m_vtNo.append(ui->lbNo_7); m_vtText.append(ui->lbText_7);
    m_vtIcon.append(ui->lbIcon_8); m_vtNo.append(ui->lbNo_8); m_vtText.append(ui->lbText_8);
    m_vtIcon.append(ui->lbIcon_9); m_vtNo.append(ui->lbNo_9); m_vtText.append(ui->lbText_9);
    m_vtIcon.append(ui->lbIcon_10);m_vtNo.append(ui->lbNo_10);m_vtText.append(ui->lbText_10);
    m_vtIcon.append(ui->lbIcon_11);m_vtNo.append(ui->lbNo_11);m_vtText.append(ui->lbText_11);
    m_vtIcon.append(ui->lbIcon_12);m_vtNo.append(ui->lbNo_12);m_vtText.append(ui->lbText_12);
    //m_vtIcon.append(ui->lbIcon_13);m_vtNo.append(ui->lbNo_13);m_vtText.append(ui->lbText_13);
    //m_vtIcon.append(ui->lbIcon_14);m_vtNo.append(ui->lbNo_14);m_vtText.append(ui->lbText_14);
    //m_vtIcon.append(ui->lbIcon_15);m_vtNo.append(ui->lbNo_15);m_vtText.append(ui->lbText_15);

    int i;
    for(i=0;i<m_vtNo.count();i++)
    {
        connect(m_vtNo[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtText[i],SIGNAL(mouse_release()),m_vtNo[i],SIGNAL(mouse_release()));
    }

    connect(ui->btnUpPic,SIGNAL(mouse_release()),this,SLOT(onUp()));
    connect(ui->btnUpIcon,SIGNAL(mouse_press()),ui->btnUpPic,SLOT(press()));
    connect(ui->btnUpIcon,SIGNAL(mouse_release()),ui->btnUpPic,SLOT(release()));
    connect(ui->btnDownPic,SIGNAL(mouse_release()),this,SLOT(onDown()));
    connect(ui->btnDownIcon,SIGNAL(mouse_press()),ui->btnDownPic,SLOT(press()));
    connect(ui->btnDownIcon,SIGNAL(mouse_release()),ui->btnDownPic,SLOT(release()));

    connect(ui->btnEditPic,SIGNAL(mouse_release()),this,SLOT(onEdit()));
    connect(ui->btnEditIcon,SIGNAL(mouse_press()),ui->btnEditPic,SLOT(press()));
    connect(ui->btnEditIcon,SIGNAL(mouse_release()),ui->btnEditPic,SLOT(release()));

    connect(ui->btnDelPic,SIGNAL(mouse_release()),this,SLOT(onDel()));
    connect(ui->btnDelIcon,SIGNAL(mouse_press()),ui->btnDelPic,SLOT(press()));
    connect(ui->btnDelIcon,SIGNAL(mouse_release()),ui->btnDelPic,SLOT(release()));

    connect(ui->btnAddUpPic,SIGNAL(mouse_release()),this,SLOT(onAdd()));
    connect(ui->btnAddUpIcon,SIGNAL(mouse_press()),ui->btnAddUpPic,SLOT(press()));
    connect(ui->btnAddUpIcon,SIGNAL(mouse_release()),ui->btnAddUpPic,SLOT(release()));

    connect(ui->btnAddDnPic,SIGNAL(mouse_release()),this,SLOT(onAdd()));
    connect(ui->btnAddDnIcon,SIGNAL(mouse_press()),ui->btnAddDnPic,SLOT(press()));
    connect(ui->btnAddDnIcon,SIGNAL(mouse_release()),ui->btnAddDnPic,SLOT(release()));

    connect(ui->btnSavePic,SIGNAL(mouse_release()),this,SLOT(onSave()));
    connect(ui->btnSaveIcon,SIGNAL(mouse_press()),ui->btnSavePic,SLOT(press()));
    connect(ui->btnSaveIcon,SIGNAL(mouse_release()),ui->btnSavePic,SLOT(release()));

    connect(ui->btnResetPic,SIGNAL(mouse_release()),this,SLOT(onReset()));
    connect(ui->btnResetIcon,SIGNAL(mouse_press()),ui->btnResetPic,SLOT(press()));
    connect(ui->btnResetIcon,SIGNAL(mouse_release()),ui->btnResetPic,SLOT(release()));

    connect(ui->btnInsidePic,SIGNAL(mouse_release()),this,SLOT(onInside()));
    connect(ui->btnInsideIcon,SIGNAL(mouse_press()),ui->btnInsidePic,SLOT(press()));
    connect(ui->btnInsideIcon,SIGNAL(mouse_release()),ui->btnInsidePic,SLOT(release()));

    connect(ui->btnOutsidePic,SIGNAL(mouse_release()),this,SLOT(onOutside()));
    connect(ui->btnOutsideIcon,SIGNAL(mouse_press()),ui->btnOutsidePic,SLOT(press()));
    connect(ui->btnOutsideIcon,SIGNAL(mouse_release()),ui->btnOutsidePic,SLOT(release()));

    //clear
    pxmModifyIcon.clear();
    m_bUseFullPath = false;

}

widget_stepedit_program::~widget_stepedit_program()
{
    delete ui;
}
void widget_stepedit_program::showEvent(QShowEvent *)
{
    Update();
    timer->start(100);
}
void widget_stepedit_program::hideEvent(QHideEvent *)
{
    timer->stop();
}
void widget_stepedit_program::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void widget_stepedit_program::onClose()
{
    if(m_bModify)
    {
        // confirm
        dialog_confirm* dig = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
        dig->Title(tr("Confirm"));
        dig->SetColor(dialog_confirm::DEFAULT);
        dig->Message(tr("Do you want save and close?"), tr("Program are modified."));

        if(dig->exec() == QDialog::Accepted)
            Write(m_strPrgName);

    }

    this->hide();
    emit sigClose();
}


void widget_stepedit_program::Init(QString prg_name, bool use_fullpath)
{
    m_strPrgName = prg_name;
    m_nSelectedIndex = 0;
    m_nStartIndex = 0;
    m_bUseFullPath = use_fullpath;
}
void widget_stepedit_program::Update()
{
    m_Stack.clear();m_Stack_SelectedIndex.clear();m_Stack_StartIndex.clear();
    Read(m_strPrgName);
    Redraw(m_nStartIndex);
}
void widget_stepedit_program::Update_Call()
{
    Read(m_strPrgName);
    Redraw(m_nStartIndex);
}



void widget_stepedit_program::Read(QString prg_name)
{
    CNRobo* pCon = CNRobo::getInstance();
    QStringList temp;
    int ret;
    bool bExist;

    if(!m_bUseFullPath)
    {
        bExist = pCon->findProgram(prg_name);
        if(bExist)
        {
            // mnt/mtd5/Programs
            ret = pCon->getProgramSteps(prg_name, temp);
            if(ret < 0)
            {
                qDebug() << "getProgramSteps =" << ret;
                return;
            }
        }
        else
        {
            temp.clear();
            temp.append(tr("Don't have program!!"));
        }
    }
    else
    {
        pCon->checkDirFileExist(prg_name, bExist);

        if(bExist)
        {
            // other path.
            ret = pCon->getProgramFile(prg_name, temp);
            if(ret < 0)
            {
                qDebug() << "getProgramFile ret = " << ret;
                return;
            }
        }
        else
        {
            temp.clear();
            temp.append(tr("Don't have program!!"));
        }
    }

    qDebug() << "Read Program :" << prg_name;
    /*for(int i=0;i<temp.count();i++)
    {
        temp[i] = temp[i].trimmed();
        //qDebug() << temp[i];
    }*/

    m_Program = temp;       // real data
    m_Program_Old = temp;   // old data for reset.
    m_Program_Comp = temp;  // comp data.

    m_bModify = false;      // modify check.
}

void widget_stepedit_program::Write(QString prg_name)
{
    CNRobo* pCon = CNRobo::getInstance();

    int ret;
    bool bExist;

    if(!m_bUseFullPath)
    {
        bExist = pCon->findProgram(prg_name);
        if(!bExist)
        {
            qDebug() << prg_name << "=> No Exist!!";
            return;
        }

        ret = pCon->setProgramSteps(prg_name, m_Program);
        if(ret < 0)
        {
            qDebug() << "setProgramSteps ret = " << ret << prg_name;
            return;
        }

        ret = pCon->saveProgram(prg_name);
        if(ret < 0) // add API
        {
            qDebug() << "saveProgram ret = " << ret << prg_name;
            return;
        }
    }
    else
    {
        pCon->checkDirFileExist(prg_name, bExist);
        if(!bExist)
        {
            qDebug() << prg_name << "=> No Exist!!";
            return;
        }

        ret = pCon->createProgramFile(prg_name, m_Program);
        if(ret < 0)
        {
            qDebug() << "createProgramFile ret = " << ret << prg_name;
            return;
        }
    }

    qDebug() << "Write Program :" << prg_name;
    //for(int i=0;i<m_Program.count();i++)
    //    qDebug() << m_Program[i];
}

void widget_stepedit_program::Redraw(int start_index)
{
    int list_index;
    QString strNo, strText;
    for(int i=0;i<m_vtNo.count();i++)
    {
        list_index = start_index + i;

        if(list_index >= m_Program.size())
        {
            // no have data.

            strNo.clear();
            strText.clear();
            SetModifyIcon(i,false); // modify icon off
        }
        else
        {
            // have data.

            strNo = QString().setNum(list_index + 1);
            strText = m_Program[list_index];

            // check modify line.
            if(m_Program[list_index] != m_Program_Comp[list_index])
                SetModifyIcon(i,true);
            else
                SetModifyIcon(i,false);
        }

        m_vtNo[i]->setText(strNo);
        m_vtText[i]->setText(strText);

    }
}

void widget_stepedit_program::SetModifyIcon(int line_index, bool onoff)
{
    if(pxmModifyIcon.isEmpty())
    {
        QImage img;
        pxmModifyIcon.append(0);
        //img.load(":/icon/icon/icon99-15.png");
        img.load(":/icon/icon/icon99-16.png");
        pxmModifyIcon.append(QPixmap::fromImage(img));
    }

    if((int)onoff < pxmModifyIcon.size())
    {
        m_vtIcon[line_index]->setPixmap(pxmModifyIcon[onoff]);

        if(onoff)
            m_bModify = true;
    }

}

void widget_stepedit_program::onList()
{
    QLabel3* btn = (QLabel3*)sender();

    int line_index = m_vtNo.indexOf(btn);
    if(line_index < 0) return;

    if(m_vtNo[line_index]->text().isEmpty())
        return;

    m_nSelectedIndex = m_nStartIndex + line_index;
}

void widget_stepedit_program::onTimer()
{
    // list view up&down.
    if(m_nSelectedIndex >= (m_nStartIndex+m_vtNo.size()))
    {
        m_nStartIndex = m_nSelectedIndex - (m_vtNo.size()-1);
        Redraw(m_nStartIndex);
    }
    else if(m_nSelectedIndex < m_nStartIndex)
    {
        m_nStartIndex = m_nSelectedIndex;
        Redraw(m_nStartIndex);
    }
    // delete last line => SelectedIndex move now program last line.
    if(m_nSelectedIndex >= m_Program.size()
    && m_nSelectedIndex > 0)
        m_nSelectedIndex--;

    int line_index = m_nSelectedIndex - m_nStartIndex;
    SetLineColor(line_index);

    SetProgramName(m_strPrgName);
    SetProgramIndex(m_nSelectedIndex+1, m_Program.size());

    ui->btnSavePic->setEnabled(m_bModify);
    ui->btnSaveIcon->setEnabled(m_bModify);
    ui->btnResetPic->setEnabled(m_bModify);
    ui->btnResetIcon->setEnabled(m_bModify);


    // for inside/outside
    QString str = m_Program[m_nSelectedIndex];
    int temp = str.indexOf(";");
    if(temp >= 0)
        str = str.remove(temp, str.size()-temp);
    int str_index = str.indexOf("Call");
    if(str_index < 0)
        SetInsideBtn(false);
    else
        SetInsideBtn(true);

    if(m_Stack.isEmpty())
        SetOutsideBtn(false);
    else
        SetOutsideBtn(true);

}

void widget_stepedit_program::onUp()
{
   if(m_nSelectedIndex <= 0)
       return;

   m_nSelectedIndex--;
}
void widget_stepedit_program::onDown()
{
    if(m_Program.size() <= 0)
        return;
    if(m_nSelectedIndex >= (m_Program.size()-1))
        return;

    m_nSelectedIndex++;
}

void widget_stepedit_program::SetLineColor(int line_index)
{
    // if line_index = -1 => all off

    QPalette* pal = new QPalette();
    pal->setBrush(QPalette::Window,QBrush(QColor(255,255,0)));

    for(int i=0;i<m_vtNo.count();i++)
    {
        if(i == line_index)
        {
            m_vtText[i]->setPalette(*pal);
            m_vtText[i]->setAutoFillBackground(true);
        }
        else
        {
            m_vtText[i]->setAutoFillBackground(false);
        }

    }
}

void widget_stepedit_program::SetProgramName(QString prg_name)
{
    ui->lbPrgName->setText(prg_name);
}
void widget_stepedit_program::SetProgramIndex(int select_index, int max_index)
{
    ui->lbNowIndex->setText(QString().setNum(select_index));
    ui->lbTotalIndex->setText(QString().setNum(max_index));
}

void widget_stepedit_program::onEdit()
{
    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(tr("Edit"));

    QString temp;
    if(m_bUseFullPath)
        temp = m_Program[m_nSelectedIndex];
    else
        temp = m_Program[m_nSelectedIndex].trimmed();

    kb->m_kb->SetText(temp);

    if(kb->exec() == QDialog::Accepted)
    {
        m_Program[m_nSelectedIndex] = kb->m_kb->GetText();
        Redraw(m_nStartIndex);
    }

}
void widget_stepedit_program::onDel()
{
    if(m_Program.size() <= 1)   // must have over 1-line.
        return;

    // confirm
    dialog_confirm* dig = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    dig->Title(tr("Confirm"));
    dig->SetColor(dialog_confirm::DEFAULT);
    dig->Message(tr("Do you want delete this line?"), tr("You can reset for recoverly."));

    if(dig->exec() == QDialog::Accepted)
    {
        m_Program.takeAt(m_nSelectedIndex);
        m_Program_Comp.takeAt(m_nSelectedIndex);

        Redraw(m_nStartIndex);
        m_bModify = true;
    }
}
void widget_stepedit_program::onAdd()
{
    QString btn = sender()->objectName();
    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(tr("Add"));
    kb->m_kb->SetText("");
    if(kb->exec() == QDialog::Accepted)
    {
        if(btn == ui->btnAddUpPic->objectName())
        {
            m_Program.insert(m_nSelectedIndex, kb->m_kb->GetText());
            m_Program_Comp.insert(m_nSelectedIndex, "@@new line!!");    // any different word.
        }
        else if(btn == ui->btnAddDnPic->objectName())
        {
            m_Program.insert(m_nSelectedIndex+1, kb->m_kb->GetText());
            m_Program_Comp.insert(m_nSelectedIndex+1, "@@new line!!");  // any different word.
        }

        Redraw(m_nStartIndex);
    }

}
void widget_stepedit_program::onSave()
{
    Write(m_strPrgName);
    Read(m_strPrgName);
    Redraw(m_nStartIndex);
}
void widget_stepedit_program::onReset()
{
    Read(m_strPrgName);
    m_nStartIndex=0;
    m_nSelectedIndex=0;
    Redraw(m_nStartIndex);
}

void widget_stepedit_program::SetInsideBtn(bool onoff)
{
    if(onoff)
    {
        ui->btnInsidePic->show();
        ui->btnInsideIcon->show();
    }
    else
    {
        ui->btnInsidePic->hide();
        ui->btnInsideIcon->hide();
    }
}
void widget_stepedit_program::SetOutsideBtn(bool onoff)
{
    if(onoff)
    {
        ui->btnOutsidePic->show();
        ui->btnOutsideIcon->show();
    }
    else
    {
        ui->btnOutsidePic->hide();
        ui->btnOutsideIcon->hide();
    }
}

void widget_stepedit_program::onInside()
{
    QString prg_name = m_Program[m_nSelectedIndex];
    int temp = prg_name.indexOf(";");
    if(temp >= 0)
        prg_name = prg_name.remove(temp, prg_name.size()-temp);

    prg_name = prg_name.remove(QString("Call")).trimmed();

    temp = prg_name.indexOf("(");
    if(temp >= 0)
        prg_name = prg_name.remove(temp, prg_name.size()-temp);

    qDebug() << prg_name;

    QString path;
    if(!m_bUseFullPath)
        path = prg_name;
    else
    {
        QString temp = m_strPrgName;
        int prog_index = temp.lastIndexOf("/");
        qDebug() << prog_index;
        path = temp.left(prog_index);
        qDebug() << path;
        path += "/";
        path += prg_name;
        qDebug() << path;
    }

    m_Stack.push_front(m_strPrgName);
    m_Stack_SelectedIndex.push_front(QString().setNum(m_nSelectedIndex));
    m_Stack_StartIndex.push_front(QString().setNum(m_nStartIndex));
    this->Init(path, m_bUseFullPath);
    this->Update_Call();
}
void widget_stepedit_program::onOutside()
{
    if(m_Stack.isEmpty()) return;

    QString prg_name = m_Stack.takeFirst();
    QString select_index = m_Stack_SelectedIndex.takeFirst();
    QString start_index = m_Stack_StartIndex.takeFirst();

    this->Init(prg_name, m_bUseFullPath);
    m_nStartIndex = start_index.toInt();
    m_nSelectedIndex = select_index.toInt();
    this->Update_Call();
}
