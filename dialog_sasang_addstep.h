#ifndef DIALOG_SASANG_ADDSTEP_H
#define DIALOG_SASANG_ADDSTEP_H

#include <QDialog>

#include "global.h"
#include "qlabel3.h"
#include "qlabel4.h"

#include "dialog_message.h"

namespace Ui {
class dialog_sasang_addstep;
}

class dialog_sasang_addstep : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_sasang_addstep(QWidget *parent = 0);
    ~dialog_sasang_addstep();

    void Update();

    bool GetSelected(ST_SSGROUP_DATA& data);    // false:no have data, true: it's ok.

private:
    Ui::dialog_sasang_addstep *ui;

    QVector<QLabel3*> m_vtTbNo,m_vtTbGrp,m_vtTbDate;

    int m_nSelectedIndex;
    int m_nStartIndex;
    void Redraw_List(int start_index);      // boss
    void Display_ListOn(int table_index);   // boss

    void Display_PatternNo(int select_index);

public slots:
    void onOK();
    void onCancel();

    void onList();
    void onUp();
    void onDown();

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_SASANG_ADDSTEP_H
