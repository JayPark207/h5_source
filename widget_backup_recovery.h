#ifndef WIDGET_BACKUP_RECOVERY_H
#define WIDGET_BACKUP_RECOVERY_H

#include <QWidget>
#include <QTimer>

#include "global.h"

#include "qlabel3.h"
#include "qlabel4.h"

namespace Ui {
class widget_backup_recovery;
}

class widget_backup_recovery : public QWidget
{
    Q_OBJECT

public:
    explicit widget_backup_recovery(QWidget *parent = 0);
    ~widget_backup_recovery();

    void Update();
    void Display_USB(bool bExist);
    void Redraw_List(int start_index);
    void Display_RowColor(int row_index);

    int GetSelectedIndex();
    void Enable_BtnSelect(bool bEnable);

signals:
    void sigClose();
    void sigSelect();

private slots:
    void onClose();

    void onTimer();
    void onList();
    void onUp();
    void onDown();

private:
    Ui::widget_backup_recovery *ui;

    QTimer* timer;

    QVector<QLabel3*> m_vtDate;
    QVector<QLabel3*> m_vtTime;
    QVector<QLabel3*> m_vtName;

    int m_nStartIndex;
    int m_nSelectedIndex;



protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // WIDGET_BACKUP_RECOVERY_H
