#include "widget_backup.h"
#include "ui_widget_backup.h"

widget_backup::widget_backup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::widget_backup)
{
    ui->setupUi(this);

    connect(ui->btnEndPic,SIGNAL(mouse_release()),this,SLOT(onClose()));
    connect(ui->btnEndIcon,SIGNAL(mouse_press()),ui->btnEndPic,SLOT(press()));
    connect(ui->btnEndIcon,SIGNAL(mouse_release()),ui->btnEndPic,SLOT(release()));

    // page backup
    m_vtMark.clear();m_vtCheck.clear();m_vtItem.clear();
    m_vtMark.append(ui->tbMark);  m_vtCheck.append(ui->tbCheck);  m_vtItem.append(ui->tbItem);
    m_vtMark.append(ui->tbMark_2);m_vtCheck.append(ui->tbCheck_2);m_vtItem.append(ui->tbItem_2);
    m_vtMark.append(ui->tbMark_3);m_vtCheck.append(ui->tbCheck_3);m_vtItem.append(ui->tbItem_3);
    m_vtMark.append(ui->tbMark_4);m_vtCheck.append(ui->tbCheck_4);m_vtItem.append(ui->tbItem_4);
    m_vtMark.append(ui->tbMark_5);m_vtCheck.append(ui->tbCheck_5);m_vtItem.append(ui->tbItem_5);

    int i;
    for(i=0;i<m_vtCheck.count();i++)
    {
        connect(m_vtCheck[i],SIGNAL(mouse_release()),this,SLOT(onList()));
        connect(m_vtMark[i],SIGNAL(mouse_release()),m_vtCheck[i],SIGNAL(mouse_release()));
        connect(m_vtItem[i],SIGNAL(mouse_release()),m_vtCheck[i],SIGNAL(mouse_release()));
    }

    connect(ui->lbBox_All,SIGNAL(mouse_release()),this,SLOT(onChkAll()));
    connect(ui->lbCheck_All,SIGNAL(mouse_release()),ui->lbBox_All,SIGNAL(mouse_release()));
    connect(ui->lbCBText_All,SIGNAL(mouse_release()),ui->lbBox_All,SIGNAL(mouse_release()));

    connect(ui->btnBackupPic,SIGNAL(mouse_release()),this,SLOT(onBackup()));
    connect(ui->btnBackup,SIGNAL(mouse_press()),ui->btnBackupPic,SLOT(press()));
    connect(ui->btnBackup,SIGNAL(mouse_release()),ui->btnBackupPic,SLOT(release()));
    connect(ui->btnBackupIcon,SIGNAL(mouse_press()),ui->btnBackupPic,SLOT(press()));
    connect(ui->btnBackupIcon,SIGNAL(mouse_release()),ui->btnBackupPic,SLOT(release()));

    Init();

}

widget_backup::~widget_backup()
{
    delete ui;
}
void widget_backup::showEvent(QShowEvent *)
{
    Update();
}
void widget_backup::hideEvent(QHideEvent *)
{

}
void widget_backup::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void widget_backup::onClose()
{
    emit sigClose();
}

void widget_backup::Init()
{
    m_Items.clear();
    m_Items.append(QString(tr("Mold Recipe Data")));
    m_Items.append(QString(tr("Robot Parameter")));
    m_Items.append(QString(tr("Robot Configulation")));
    // max. 5 (2 items reserved)

    // init item name
    for(int i=0;i<m_vtItem.count();i++)
    {
        if(m_Items.size() > i)
            m_vtItem[i]->setText(m_Items[i]);
        else
            m_vtItem[i]->setText("");
    }

    CheckAll(true);
}

void widget_backup::Update()
{
    // USB not exist btnBackup Disable.
    bool bExist = RM->IsExist(USB_EXIST_PATH);
    ui->btnBackupPic->setEnabled(bExist);
    ui->btnBackup->setEnabled(bExist);
    ui->btnBackupIcon->setEnabled(bExist);
}

void widget_backup::CheckAll(bool bCheck)
{
    for(int i=0;i<m_vtMark.count();i++)
    {
        if(m_Items.size() <= i)
        {
            m_vtMark[i]->hide();
            continue;
        }

        if(bCheck)
            m_vtMark[i]->show();
        else
            m_vtMark[i]->hide();
    }

    ConfirmCheck();
}

void widget_backup::ConfirmCheck()
{
    bool bCheck=true;

    for(int i=0;i<m_vtMark.count();i++)
    {
        if(m_Items.size() <= i)
            continue;

        if(m_vtMark[i]->isHidden())
        {
            bCheck = false;
            break;
        }
    }

    if(bCheck)
        ui->lbCheck_All->show();
    else
        ui->lbCheck_All->hide();
}

void widget_backup::onList()
{
    QLabel3* sel = (QLabel3*)sender();
    int index = m_vtCheck.indexOf(sel);
    if(index < 0) return;

    if(index >= m_Items.size())
        return;

    if(m_vtMark[index]->isHidden())
        m_vtMark[index]->show();
    else
        m_vtMark[index]->hide();

    ConfirmCheck();
}

void widget_backup::onChkAll()
{
    if(ui->lbCheck_All->isHidden())
        CheckAll(true);
    else
        CheckAll(false);
}

void widget_backup::onBackup()
{
    qDebug() << "onBackup() Start";
    qDebug() << "1. check no select items.";

    int i;bool have=false;
    for(i=0;i<m_vtMark.count();i++)
    {
        if(m_vtMark[i]->isVisible())
        {
            have = true;
            break;
        }
    }

    dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);

    if(!have)
    {
        msg->SetColor(dialog_message::RED);
        msg->Title("Backup Error");
        msg->Message(tr("Not found Selected Item!"), tr("Please Select Item. Try again!"));
        msg->exec();
        return;
    }

    qDebug() << "2. confirm & name input";
    dialog_confirm* conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(ui->btnBackup->text());
    conf->Message(tr("Would you want to Backup Process?"), tr("Next Step is input name."));
    if(conf->exec() != QDialog::Accepted)
        return;

    dialog_keyboard* kb = (dialog_keyboard*)gGetDialog(DIG_KEYBOARD);
    kb->m_kb->SetTitle(tr("Name"));
    kb->m_kb->SetText("");
    if(kb->exec() != QDialog::Accepted)
        return;

    QString input_name = kb->m_kb->GetText();
    if(!CheckNameRule(input_name))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title(tr("INPUT ERROR"));
        msg->Message(tr("Invalid Name!"),input_name);
        msg->exec();
        return;
    }

    qDebug() << "3. make dir name.";
    QString dir_full_name = BR->MakeDirName(input_name);

    conf = (dialog_confirm*)gGetDialog(DIG_CONFIRM);
    conf->SetColor(dialog_confirm::SKYBLUE);
    conf->Title(ui->btnBackup->text());
    conf->Message(tr("Would you want to Backup this name?"), dir_full_name);
    if(conf->exec() != QDialog::Accepted)
        return;


    qDebug() << "4. make base dir.";
    QString dir_path = BACKUP_USB_PATH;
    dir_path += BACKUP_DIR_NAME;

    if(!BR->MakeDir(dir_path))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title("Backup Error");
        msg->Message(tr("Make Directory Fail!"), dir_path);
        msg->exec();
        return;
    }

    qDebug() << "5. make backup dir.";
    QString target_path = dir_path + "/" + dir_full_name;

    if(!BR->MakeDir(target_path))
    {
        msg->SetColor(dialog_message::RED);
        msg->Title("Backup Error");
        msg->Message(tr("Fail to Make Directory!"), target_path);
        msg->exec();
        return;
    }

    qDebug() << "6. main.";
    if(m_vtMark[0]->isVisible())
    {
        qDebug() << "TP & SD recipe backup";
        if(!BR->Backup_Recipe(RM_RECIPE_PATH, RM_SD_RECIPE_PATH, target_path))
        {
            msg->SetColor(dialog_message::RED);
            msg->Title("Backup Error");
            msg->Message(tr("Fail to Bakcup Molde Recipe"), target_path);
            msg->exec();
            return;
        }
    }

    if(m_vtMark[1]->isVisible())
    {
        qDebug() << "userparam backup";
        if(!BR->Backup_Param(target_path))
        {
            msg->SetColor(dialog_message::RED);
            msg->Title("Backup Error");
            msg->Message(tr("Fail to Bakcup Parameters"), target_path);
            msg->exec();
            return;
        }
    }

    if(m_vtMark[2]->isVisible())
    {
        qDebug() << "robot config backup";

        if(!BR->Backup_Config(target_path))
        {
            msg->SetColor(dialog_message::RED);
            msg->Title("Backup Error");
            msg->Message(tr("Fail to Bakcup Configuration"), target_path);
            msg->exec();
            return;
        }
    }

    msg->SetColor(dialog_message::SKYBLUE);
    msg->Title(ui->btnBackup->text());
    msg->Message(tr("Backup Success!"), dir_full_name);
    msg->exec();
}

bool widget_backup::CheckNameRule(QString name)
{
    if(name.count() < 1
    || name.count() > 255
    || name.indexOf(' ') >= 0
    || name.indexOf('.') >= 0
    || name.indexOf('/') >= 0
    || name.indexOf('\\') >= 0)
        return false;

    return true;
}

void widget_backup::Display_USB(bool bExist)
{
    ui->lbUSB->setEnabled(bExist);
}
