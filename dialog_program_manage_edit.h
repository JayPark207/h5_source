#ifndef DIALOG_PROGRAM_MANAGE_EDIT_H
#define DIALOG_PROGRAM_MANAGE_EDIT_H

#include <QDialog>

#include "global.h"
#include "widget_stepedit_program.h"

namespace Ui {
class dialog_program_manage_edit;
}

class dialog_program_manage_edit : public QDialog
{
    Q_OBJECT

public:
    explicit dialog_program_manage_edit(QWidget *parent = 0);
    ~dialog_program_manage_edit();

    widget_stepedit_program* wig_program;

private:
    Ui::dialog_program_manage_edit *ui;


protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_PROGRAM_MANAGE_EDIT_H
