#ifndef DIALOG_ROBOTCONFIG_H
#define DIALOG_ROBOTCONFIG_H

#include <QDialog>

#include "global.h"
#include "qlabel4.h"
#include "qlabel3.h"

#include "dialog_numpad.h"
#include "dialog_keyboard.h"


namespace Ui {
class dialog_robotconfig;
}

class dialog_robotconfig : public QDialog
{
    Q_OBJECT

    enum ENUM_TABLE
    {
        TB_AIXS=0,
        TB_LIMIT,
        TB_MOTION,
    };

public:
    explicit dialog_robotconfig(QWidget *parent = 0);
    ~dialog_robotconfig();

    void Update(ENUM_TABLE table);

    void UserLevel();

public slots:
    void onSave();
    void onCancel();
    void onButton();
    void onEdit();


private:
    Ui::dialog_robotconfig *ui;

    ENUM_TABLE m_nNowTable;
    void update_axis();
    void update_limit();
    void update_motion();

    QStringList m_slHeader,m_slColHeader;
    QVector<QLabel4*> m_vtBtnPic;
    QVector<QLabel3*> m_vtBtnTxt;

    void SetButton(ENUM_TABLE table);
    QStringList m_slRobotConfigName;

    void edit_axis(int row, int col);
    void edit_limit(int row, int col);
    void edit_motion(int row, int col);

    int m_nUserLevel;

protected:
    void changeEvent(QEvent *);
    void showEvent(QShowEvent *);
    void hideEvent(QHideEvent *);
};

#endif // DIALOG_ROBOTCONFIG_H
