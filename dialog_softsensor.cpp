#include "dialog_softsensor.h"
#include "ui_dialog_softsensor.h"

dialog_softsensor::dialog_softsensor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_softsensor)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint/*|Qt::WindowStaysOnTopHint*/);

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(reject()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    m_vtMinPic.clear();m_vtMaxPic.clear();
    m_vtMinPic.append(ui->tbMinPic);  m_vtMaxPic.append(ui->tbMaxPic);
    m_vtMinPic.append(ui->tbMinPic_2);m_vtMaxPic.append(ui->tbMaxPic_2);

    m_vtMin.clear();m_vtMax.clear();
    m_vtMin.append(ui->tbMin);  m_vtMax.append(ui->tbMax);
    m_vtMin.append(ui->tbMin_2);m_vtMax.append(ui->tbMax_2);

    int i;
    for(i=0;i<m_vtMinPic.count();i++)
    {
        connect(m_vtMinPic[i],SIGNAL(mouse_release()),this,SLOT(onMin()));
        connect(m_vtMin[i],SIGNAL(mouse_press()),m_vtMinPic[i],SLOT(press()));
        connect(m_vtMin[i],SIGNAL(mouse_release()),m_vtMinPic[i],SLOT(release()));
    }
    for(i=0;i<m_vtMaxPic.count();i++)
    {
        connect(m_vtMaxPic[i],SIGNAL(mouse_release()),this,SLOT(onMax()));
        connect(m_vtMax[i],SIGNAL(mouse_press()),m_vtMaxPic[i],SLOT(press()));
        connect(m_vtMax[i],SIGNAL(mouse_release()),m_vtMaxPic[i],SLOT(release()));
    }


}

dialog_softsensor::~dialog_softsensor()
{
    delete ui;
}
void dialog_softsensor::showEvent(QShowEvent *)
{
    Update();
}
void dialog_softsensor::hideEvent(QHideEvent *)
{

}
void dialog_softsensor::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}

void dialog_softsensor::onOK()
{
    if(!Save())
    {
        dialog_message* msg = (dialog_message*)gGetDialog(DIG_MSG);
        msg->SetColor(dialog_message::RED);
        msg->Title(QString(tr("Save Error")));
        msg->Message(tr("Fail to save data!!"));
        msg->exec();
    }
    emit accept();
}
void dialog_softsensor::Update()
{
    int i;

    // read data & display.
    if(SoftSensor->Read())
    {
        for(i=0;i<m_vtMin.count();i++)
        {
            m_vtMin[i]->setText(SoftSensor->m_Min[i]);
            m_vtMax[i]->setText(SoftSensor->m_Max[i]);
        }
    }
}

void dialog_softsensor::onMin()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtMinPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-100000.0);
    np->m_numpad->SetMaxValue(100000);
    np->m_numpad->SetSosuNum(SOSU_POS);
    np->m_numpad->SetTitle(ui->tbMinTitle->text());
    np->m_numpad->SetNum(m_vtMin[index]->text().toDouble());
    QString str;
    if(np->exec() == QDialog::Accepted)
    {
        str.sprintf(FORMAT_POS, (float)np->m_numpad->GetNumDouble());
        m_vtMin[index]->setText(str);
    }
}
void dialog_softsensor::onMax()
{
    QLabel4* btn = (QLabel4*)sender();
    int index = m_vtMaxPic.indexOf(btn);
    if(index < 0) return;

    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);
    np->m_numpad->SetMinValue(-100000.0);
    np->m_numpad->SetMaxValue(100000);
    np->m_numpad->SetSosuNum(SOSU_POS);
    np->m_numpad->SetTitle(ui->tbMaxTitle->text());
    np->m_numpad->SetNum(m_vtMax[index]->text().toDouble());
    QString str;
    if(np->exec() == QDialog::Accepted)
    {
        str.sprintf(FORMAT_POS, (float)np->m_numpad->GetNumDouble());
        m_vtMax[index]->setText(str);
    }
}

bool dialog_softsensor::Save()
{
    int i;
    for(i=0;i<m_vtMin.count();i++)
    {
        if(SoftSensor->m_Min.size() > i)
            SoftSensor->m_Min[i] = m_vtMin[i]->text();
    }
    for(i=0;i<m_vtMax.count();i++)
    {
        if(SoftSensor->m_Max.size() > i)
            SoftSensor->m_Max[i] = m_vtMax[i]->text();
    }

    if(!SoftSensor->Write())
        return false;

    return true;
}
