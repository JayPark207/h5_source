#-------------------------------------------------
#
# Project created by QtCreator 2016-09-04T17:12:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = HYArm
TEMPLATE = app

ESTDIR = ../lib

target.path = /mnt/mtd5/
INSTALLS += target

DEFINES += _CG3300_

#DEFINES += _H6_

# You can change this dir. path !!
COREDEV_DIR = /home/aram/Work/coreDev_cg3300

#INCLUDEPATH += .. ../include
INCLUDEPATH += $${COREDEV_DIR}/include

# cnrobo library name
CONFIG(debug, release | debug) {
    BUILD_TYPE = "DEBUG"
    LCNROBO=cnrobo_d
}else{
    LCNROBO=cnrobo
    BUILD_TYPE = "RELEASE"
}

# cnrobo library folder :
#CNROBO_LIB_ROOT_DIR = $$_PRO_FILE_PWD_/../lib   #$$_PRO_FILE_PWD_/../lib
CNROBO_LIB_ROOT_DIR = $${COREDEV_DIR}/lib

# local lib directory
win32 {

    DEFINES += DEV_TYPE=DEV_PCWIN

    msvc{
        message("coreCUI for MSVC.")

        CNROBO_LIB_DIR = $$shell_path($${CNROBO_LIB_ROOT_DIR}/msvc)

        LIBS += -L$$CNROBO_LIB_DIR $${LCNROBO}.lib ws2_32.lib
    }
    gcc{
        message("coreCUI for MinGW.")

        CNROBO_LIB_DIR = $${CNROBO_LIB_ROOT_DIR}/mingw

        LIBS += -L$$CNROBO_LIB_DIR -l$${LCNROBO} -lws2_32

    }

}

unix {

    cg3300 | cg3300_dbg {
        message("coreCUI : " $$BUILD_TYPE " build for DTP7L.")
        DEFINES += DEV_TYPE=DEV_DTP7
        CNROBO_LIB_DIR = $${CNROBO_LIB_ROOT_DIR}/linuxarm
    }else{
        message("coreCUI : " $$BUILD_TYPE " build for PC-Linux.")
        DEFINES += DEV_TYPE=DEV_PCLINUX
        CNROBO_LIB_DIR = $${CNROBO_LIB_ROOT_DIR}/linux86
    }

        LIBS += -L$$CNROBO_LIB_DIR -l$${LCNROBO}
}

android {
        message("coreCUI for Android.")
        CNROBO_LIB_DIR = $${CNROBO_LIB_ROOT_DIR}/android

        DEFINES += DEV_TYPE=DEV_ANDROID

        LIBS += -L$$CNROBO_LIB_DIR -l$${LCNROBO}

}

## deploy css file
#mypackage.files = styles/
#mypackage.path = /home/app
#INSTALLS += mypackage

#win32{
#    CONFIG(debug, debug|release) {
#        message("$$shell_path($$OUT_PWD/debug)")
#        copydata.commands = $(COPY_FILE) "$$shell_path($$PWD/styles/defstyle.css)" "$$shell_path($$OUT_PWD/debug)"
#    } else {
#        message("$$shell_path($$OUT_PWD/release)")
#        copydata.commands = $(COPY_FILE) "$$shell_path($$PWD/styles/defstyle.css)" "$$shell_path($$OUT_PWD/release)"
#    }
#}
#unix{
#    copydata.commands = $(COPY_FILE) $$PWD/styles/defstyle.css $$OUT_PWD
#}

#first.depends = $(first) copydata
#export(first.depends)
#export(copydata.commands)

#QMAKE_EXTRA_TARGETS += first copydata

TRANSLATIONS += lang/lang_en.ts \
                lang/lang_ko.ts \
                lang/lang_zh.ts \
                lang/lang_ja.ts \
                lang/lang_de.ts \
                lang/lang_ru.ts \
                lang/lang_cs.ts \
                lang/lang_fr.ts \
                lang/lang_es.ts \
                lang/lang_it.ts \
                lang/lang_th.ts \

SOURCES += main.cpp\
        mainwindow.cpp \
    dialog_keyboard.cpp \
    dialog_message.cpp \
    dialog_numpad.cpp \
    form_autorun.cpp \
    global.cpp \
    keyboard.cpp \
    numpad.cpp \
    page_main2.cpp \
    page_main2_log.cpp \
    page_main2_mold.cpp \
    page_main2_monitor.cpp \
    page_main2_setting.cpp \
    page_main2_special.cpp \
    page_main2_teach.cpp \
    top_main.cpp \
    top_speed.cpp \
    hyrecipe.cpp \
    dialog_confirm.cpp \
    dialog_mode_select.cpp \
    dialog_takeout_method.cpp \
    dialog_weight.cpp \
    dialog_temperature.cpp \
    dialog_moldclean.cpp \
    dialog_jmotion.cpp \
    hykey.cpp \
    hyledbuzzer.cpp \
    hystepedit.cpp \
    dialog_stepedit_add.cpp \
    form_stepedit_useroutput.cpp \
    form_stepedit_userinput.cpp \
    form_stepedit_base.cpp \
    hypos.cpp \
    hytime.cpp \
    hyuserdata.cpp \
    dialog_position_teaching.cpp \
    dialog_rotation.cpp \
    dialog_home.cpp \
    hyparam.cpp \
    view_param.cpp \
    hyerror.cpp \
    dialog_error.cpp \
    dialog_zeroing.cpp \
    dialog_timeset.cpp \
    widget_jogmodule.cpp \
    widget_stepedit_program.cpp \
    widget_stepedit_programrun.cpp \
    top_sub.cpp \
    hystatus.cpp \
    dialog_userlevel.cpp \
    dialog_delaying.cpp \
    hyrobotconfig.cpp \
    dialog_robotconfig.cpp \
    form_specialsetting.cpp \
    hyui_longpress.cpp \
    hytponly.cpp \
    dialog_version.cpp \
    hylog.cpp \
    dialog_motor.cpp \
    hyecatmotor_panasonic.cpp \
    hyecatmotor.cpp \
    hyecatmotor_yaskawa.cpp \
    dialog_common_position.cpp \
    dialog_motor_test.cpp \
    hyrecipemanage2.cpp \
    dialog_pictureview.cpp \
    dialog_backup.cpp \
    widget_backup.cpp \
    widget_backup_recovery.cpp \
    widget_backup_recovery2.cpp \
    hybackuprecovery.cpp \
    widget_jogmodule2.cpp \
    dialog_manual.cpp \
    dialog_io.cpp \
    dialog_language.cpp \
    dialog_position.cpp \
    dialog_time.cpp \
    dialog_view_dmcvar.cpp \
    dialog_log.cpp \
    dialog_recipe.cpp \
    dialog_stepedit.cpp \
    dialog_call_position.cpp \
    dialog_product_info.cpp \
    dialog_array_info.cpp \
    dialog_safetyzone.cpp \
    dialog_useskip.cpp \
    hyuseskip.cpp \
    dialog_mode_detail.cpp \
    hymode.cpp \
    dialog_array_set.cpp \
    dialog_insertmain_set.cpp \
    dialog_unloadseq_set.cpp \
    dialog_insertsub_set.cpp \
    hylanguage.cpp \
    dialog_syncback.cpp \
    hysoftsensor.cpp \
    dialog_softsensor.cpp \
    dialog_varmonitor.cpp \
    dialog_position_auto.cpp \
    dialog_program_manage.cpp \
    dialog_program_manage_edit.cpp \
    dialog_error_list.cpp \
    hysasang.cpp \
    hysasang_motion.cpp \
    hysasang_pattern.cpp \
    hysasang_group.cpp \
    dialog_sasang_motion.cpp \
    dialog_sasang_motion_teach.cpp \
    dialog_sasang_motion_test.cpp \
    dialog_task_control.cpp \
    dialog_robotdimension.cpp \
    dialog_sasang_shift.cpp \
    dialog_sasang_output.cpp \
    dialog_jog.cpp \
    dialog_sasang_pattern.cpp \
    dialog_sasang_pattern_test.cpp \
    dialog_sasang_group.cpp \
    dialog_sasang_addstep.cpp \
    form_stepedit_userposition.cpp \
    dialog_euromap.cpp \
    hyeuromap.cpp \
    dialog_network.cpp \
    hynetwork.cpp \
    hybigmode.cpp \
    dialog_mode_entry.cpp \
    dialog_mode_easy.cpp \
    dialog_mode_easy_view.cpp \
    dialog_easy_setting.cpp \
    dialog_easy_setting_io.cpp \
    dialog_home2.cpp \
    hypendent.cpp \
    dialog_sampling.cpp \
    dialog_robot_notuse.cpp \
    dialog_tp_ipgw.cpp \
    dialog_unload_method.cpp \
    dialog_weight_offset.cpp \
    dialog_e_sensor.cpp \
    dialog_position_teaching2.cpp \
    widget_jogmodule3.cpp \
    widget_jogmodule4.cpp \
    dialog_anti_vibration.cpp \
    dialog_dualunload.cpp \
    dialog_dualunload_mon.cpp \
    dialog_unloadback.cpp \
    dialog_takeout_timing.cpp \
    dialog_discharge.cpp \
    widget_jogmodule3_p.cpp \
    widget_jogmodule4_p.cpp

HEADERS  += mainwindow.h \
    $${COREDEV_DIR}/include/cnerror.h \
    $${COREDEV_DIR}/include/cnhelper.h \
    $${COREDEV_DIR}/include/cnrobo.h \
    $${COREDEV_DIR}/include/cntype.h \
    dialog_keyboard.h \
    dialog_message.h \
    dialog_numpad.h \
    dtp7-etc.h \
    form_autorun.h \
    global.h \
    keyboard.h \
    numpad.h \
    page_main2.h \
    page_main2_log.h \
    page_main2_mold.h \
    page_main2_monitor.h \
    page_main2_setting.h \
    page_main2_special.h \
    page_main2_teach.h \
    qlabel2.h \
    top_main.h \
    top_speed.h \
    hyrecipe.h \
    dialog_confirm.h \
    qlabel3.h \
    dialog_mode_select.h \
    dialog_takeout_method.h \
    dialog_weight.h \
    dialog_temperature.h \
    dialog_moldclean.h \
    dialog_jmotion.h \
    hykey.h \
    hyledbuzzer.h \
    hystepedit.h \
    dialog_stepedit_add.h \
    hypos.h \
    hytime.h \
    hyinput.h \
    hyoutput.h \
    qlabel4.h \
    form_stepedit_useroutput.h \
    form_stepedit_userinput.h \
    form_stepedit_base.h \
    hyuserdata.h \
    dialog_position_teaching.h \
    defines.h \
    dialog_rotation.h \
    dialog_home.h \
    hystepdata.h \
    hyparam.h \
    view_param.h \
    hyerror.h \
    dialog_error.h \
    dialog_zeroing.h \
    dialog_timeset.h \
    widget_jogmodule.h \
    widget_stepedit_program.h \
    widget_stepedit_programrun.h \
    top_sub.h \
    hystatus.h \
    dialog_userlevel.h \
    dialog_delaying.h \
    hyrobotconfig.h \
    dialog_robotconfig.h \
    form_specialsetting.h \
    dialog_robotdimension.h \
    hyui_longpress.h \
    hytponly.h \
    dialog_version.h \
    hylog.h \
    dialog_motor.h \
    hyecatmotor.h \
    hyecatmotor_panasonic.h \
    hyecatmotor_yaskawa.h \
    dialog_common_position.h \
    dialog_motor_test.h \
    hyrecipemanage2.h \
    dialog_pictureview.h \
    dialog_backup.h \
    widget_backup.h \
    widget_backup_recovery.h \
    widget_backup_recovery2.h \
    hybackuprecovery.h \
    widget_jogmodule2.h \
    dialog_manual.h \
    dialog_io.h \
    dialog_language.h \
    dialog_position.h \
    dialog_time.h \
    dialog_view_dmcvar.h \
    dialog_log.h \
    dialog_recipe.h \
    dialog_stepedit.h \
    dialog_call_position.h \
    dialog_product_info.h \
    dialog_array_info.h \
    dialog_safetyzone.h \
    dialog_useskip.h \
    hyuseskip.h \
    dialog_mode_detail.h \
    hymode.h \
    dialog_array_set.h \
    dialog_insertmain_set.h \
    dialog_unloadseq_set.h \
    dialog_insertsub_set.h \
    hylanguage.h \
    dialog_syncback.h \
    hysoftsensor.h \
    dialog_softsensor.h \
    dialog_varmonitor.h \
    dialog_position_auto.h \
    dialog_program_manage.h \
    dialog_program_manage_edit.h \
    dialog_error_list.h \
    hysasang.h \
    hysasang_motion.h \
    hysasang_pattern.h \
    hysasang_group.h \
    dialog_sasang_motion.h \
    dialog_sasang_motion_teach.h \
    dialog_sasang_motion_test.h \
    dialog_task_control.h \
    dialog_sasang_shift.h \
    dialog_sasang_output.h \
    dialog_jog.h \
    dialog_sasang_pattern.h \
    dialog_sasang_pattern_test.h \
    dialog_sasang_group.h \
    dialog_sasang_addstep.h \
    form_stepedit_userposition.h \
    dialog_euromap.h \
    hyeuromap.h \
    dialog_network.h \
    hynetwork.h \
    hybigmode.h \
    dialog_mode_entry.h \
    dialog_mode_easy.h \
    dialog_mode_easy_view.h \
    dialog_easy_setting.h \
    qlabel5.h \
    dialog_easy_setting_io.h \
    dialog_home2.h \
    hypendent.h \
    dialog_sampling.h \
    dialog_robot_notuse.h \
    dialog_tp_ipgw.h \
    dialog_unload_method.h \
    dialog_weight_offset.h \
    dialog_e_sensor.h \
    dialog_position_teaching2.h \
    widget_jogmodule3.h \
    widget_jogmodule4.h \
    dialog_anti_vibration.h \
    dialog_dualunload.h \
    dialog_dualunload_mon.h \
    dialog_unloadback.h \
    dialog_takeout_timing.h \
    dialog_discharge.h \
    widget_jogmodule3_p.h \
    widget_jogmodule4_p.h

FORMS    += mainwindow.ui \
    dialog_keyboard.ui \
    dialog_message.ui \
    dialog_numpad.ui \
    form_autorun.ui \
    keyboard.ui \
    numpad.ui \
    page_main2.ui \
    page_main2_log.ui \
    page_main2_mold.ui \
    page_main2_monitor.ui \
    page_main2_setting.ui \
    page_main2_special.ui \
    page_main2_teach.ui \
    top_main.ui \
    top_speed.ui \
    dialog_confirm.ui \
    dialog_mode_select.ui \
    dialog_takeout_method.ui \
    dialog_weight.ui \
    dialog_temperature.ui \
    dialog_moldclean.ui \
    dialog_jmotion.ui \
    dialog_stepedit_add.ui \
    form_stepedit_useroutput.ui \
    form_stepedit_userinput.ui \
    form_stepedit_base.ui \
    dialog_position_teaching.ui \
    dialog_rotation.ui \
    dialog_home.ui \
    view_param.ui \
    dialog_error.ui \
    dialog_zeroing.ui \
    dialog_timeset.ui \
    widget_jogmodule.ui \
    widget_stepedit_program.ui \
    widget_stepedit_programrun.ui \
    top_sub.ui \
    dialog_userlevel.ui \
    dialog_delaying.ui \
    dialog_robotconfig.ui \
    form_specialsetting.ui \
    dialog_robotdimension.ui \
    dialog_version.ui \
    dialog_motor.ui \
    dialog_common_position.ui \
    dialog_motor_test.ui \
    dialog_pictureview.ui \
    dialog_backup.ui \
    widget_backup.ui \
    widget_backup_recovery.ui \
    widget_backup_recovery2.ui \
    widget_jogmodule2.ui \
    dialog_manual.ui \
    dialog_io.ui \
    dialog_language.ui \
    dialog_position.ui \
    dialog_time.ui \
    dialog_view_dmcvar.ui \
    dialog_log.ui \
    dialog_recipe.ui \
    dialog_stepedit.ui \
    dialog_call_position.ui \
    dialog_product_info.ui \
    dialog_array_info.ui \
    dialog_safetyzone.ui \
    dialog_useskip.ui \
    dialog_mode_detail.ui \
    dialog_array_set.ui \
    dialog_insertmain_set.ui \
    dialog_unloadseq_set.ui \
    dialog_insertsub_set.ui \
    dialog_syncback.ui \
    dialog_softsensor.ui \
    dialog_varmonitor.ui \
    dialog_position_auto.ui \
    dialog_program_manage.ui \
    dialog_program_manage_edit.ui \
    dialog_error_list.ui \
    dialog_sasang_motion.ui \
    dialog_sasang_motion_teach.ui \
    dialog_sasang_motion_test.ui \
    dialog_task_control.ui \
    dialog_sasang_shift.ui \
    dialog_sasang_output.ui \
    dialog_jog.ui \
    dialog_sasang_pattern.ui \
    dialog_sasang_pattern_test.ui \
    dialog_sasang_group.ui \
    dialog_sasang_addstep.ui \
    form_stepedit_userposition.ui \
    dialog_euromap.ui \
    dialog_network.ui \
    dialog_mode_entry.ui \
    dialog_mode_easy.ui \
    dialog_mode_easy_view.ui \
    dialog_easy_setting.ui \
    dialog_easy_setting_io.ui \
    dialog_home2.ui \
    dialog_sampling.ui \
    dialog_robot_notuse.ui \
    dialog_tp_ipgw.ui \
    dialog_unload_method.ui \
    dialog_weight_offset.ui \
    dialog_e_sensor.ui \
    dialog_position_teaching2.ui \
    widget_jogmodule3.ui \
    widget_jogmodule4.ui \
    dialog_anti_vibration.ui \
    dialog_dualunload.ui \
    dialog_dualunload_mon.ui \
    dialog_unloadback.ui \
    dialog_takeout_timing.ui \
    dialog_discharge.ui \
    widget_jogmodule3_p.ui \
    widget_jogmodule4_p.ui

RESOURCES += \
    HYArmResource.qrc
