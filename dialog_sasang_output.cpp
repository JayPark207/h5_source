#include "dialog_sasang_output.h"
#include "ui_dialog_sasang_output.h"

dialog_sasang_output::dialog_sasang_output(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog_sasang_output)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint /*| Qt::WindowStaysOnTopHint*/);
    this->setAttribute(Qt::WA_TranslucentBackground);

    timer = new QTimer(this);
    timer->setInterval(500);
    connect(timer,SIGNAL(timeout()),this,SLOT(onTimer()));

    connect(ui->btnOKPic,SIGNAL(mouse_release()),this,SLOT(onOK()));
    connect(ui->btnOK,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOK,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));
    connect(ui->btnOKIcon,SIGNAL(mouse_press()),ui->btnOKPic,SLOT(press()));
    connect(ui->btnOKIcon,SIGNAL(mouse_release()),ui->btnOKPic,SLOT(release()));

    connect(ui->btnCancelPic,SIGNAL(mouse_release()),this,SLOT(onCancel()));
    connect(ui->btnCancel,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancel,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_press()),ui->btnCancelPic,SLOT(press()));
    connect(ui->btnCancelIcon,SIGNAL(mouse_release()),ui->btnCancelPic,SLOT(release()));

    MatchingOutput();
    int i;
    for(i=0;i<m_vtOutPic.count();i++)
    {
        connect(m_vtOutPic[i],SIGNAL(mouse_release()),this,SLOT(onOutput()));
        connect(m_vtOutIcon[i],SIGNAL(mouse_press()),m_vtOutPic[i],SLOT(press()));
        connect(m_vtOutIcon[i],SIGNAL(mouse_release()),m_vtOutPic[i],SLOT(release()));
        connect(m_vtOutSel[i],SIGNAL(mouse_press()),m_vtOutPic[i],SLOT(press()));
        connect(m_vtOutSel[i],SIGNAL(mouse_release()),m_vtOutPic[i],SLOT(release()));
    }

    m_vtType.clear();m_vtTypeIcon.clear();
    m_vtType.append(ui->tbTypeOffPic);  m_vtTypeIcon.append(ui->tbTypeOffIcon);
    m_vtType.append(ui->tbTypeOnPic);   m_vtTypeIcon.append(ui->tbTypeOnIcon);
    m_vtType.append(ui->tbTypePulsePic);m_vtTypeIcon.append(ui->tbTypePulseIcon);

    for(i=0;i<m_vtType.count();i++)
    {
        connect(m_vtType[i],SIGNAL(mouse_release()),this,SLOT(onType()));
        connect(m_vtTypeIcon[i],SIGNAL(mouse_press()),m_vtType[i],SLOT(press()));
        connect(m_vtTypeIcon[i],SIGNAL(mouse_release()),m_vtType[i],SLOT(release()));
    }

    connect(ui->tbTimePic,SIGNAL(mouse_release()),this,SLOT(onTime()));
    connect(ui->tbTime,SIGNAL(mouse_press()),ui->tbTimePic,SLOT(press()));
    connect(ui->tbTime,SIGNAL(mouse_release()),ui->tbTimePic,SLOT(release()));


}

dialog_sasang_output::~dialog_sasang_output()
{
    delete ui;
}
void dialog_sasang_output::showEvent(QShowEvent *)
{
    Update();

    timer->start();
}
void dialog_sasang_output::hideEvent(QHideEvent *)
{
    timer->stop();
}
void dialog_sasang_output::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
}
void dialog_sasang_output::onOK()
{
    emit accept();
}
void dialog_sasang_output::onCancel()
{
    emit reject();
}

void dialog_sasang_output::MatchingOutput()
{
    m_vtOutPic.clear();m_vtOutputNum.clear();
    // here "m_vtOutputNum in output number" is modify ~~~~!!!!!
    m_vtOutPic.append(ui->btnPic);    m_vtOutputNum.append(HyOutput::VAC1); // ouput num 1~128 not 0
    m_vtOutPic.append(ui->btnPic_2);  m_vtOutputNum.append(HyOutput::VAC2);
    m_vtOutPic.append(ui->btnPic_3);  m_vtOutputNum.append(HyOutput::VAC3);
    m_vtOutPic.append(ui->btnPic_4);  m_vtOutputNum.append(HyOutput::VAC4);
    m_vtOutPic.append(ui->btnPic_5);  m_vtOutputNum.append(HyOutput::CHUCK);
    m_vtOutPic.append(ui->btnPic_6);  m_vtOutputNum.append(HyOutput::GRIP);
    m_vtOutPic.append(ui->btnPic_7);  m_vtOutputNum.append(HyOutput::NIPPER);
    m_vtOutPic.append(ui->btnPic_8);  m_vtOutputNum.append(HyOutput::RGRIP);
    m_vtOutPic.append(ui->btnPic_9);  m_vtOutputNum.append(HyOutput::USER1);
    m_vtOutPic.append(ui->btnPic_10); m_vtOutputNum.append(HyOutput::USER2);
    m_vtOutPic.append(ui->btnPic_11); m_vtOutputNum.append(HyOutput::USER3);
    m_vtOutPic.append(ui->btnPic_12); m_vtOutputNum.append(HyOutput::USER4);
    m_vtOutPic.append(ui->btnPic_13); m_vtOutputNum.append(HyOutput::USER5);
    m_vtOutPic.append(ui->btnPic_14); m_vtOutputNum.append(HyOutput::USER6);
    m_vtOutPic.append(ui->btnPic_15); m_vtOutputNum.append(HyOutput::USER7);
    m_vtOutPic.append(ui->btnPic_16); m_vtOutputNum.append(HyOutput::USER8);


    // etc...
    m_vtOutIcon.clear();m_vtOutSel.clear();
    m_vtOutIcon.append(ui->btnIcon);   m_vtOutSel.append(ui->btnSel);
    m_vtOutIcon.append(ui->btnIcon_2); m_vtOutSel.append(ui->btnSel_2);
    m_vtOutIcon.append(ui->btnIcon_3); m_vtOutSel.append(ui->btnSel_3);
    m_vtOutIcon.append(ui->btnIcon_4); m_vtOutSel.append(ui->btnSel_4);
    m_vtOutIcon.append(ui->btnIcon_5); m_vtOutSel.append(ui->btnSel_5);
    m_vtOutIcon.append(ui->btnIcon_6); m_vtOutSel.append(ui->btnSel_6);
    m_vtOutIcon.append(ui->btnIcon_7); m_vtOutSel.append(ui->btnSel_7);
    m_vtOutIcon.append(ui->btnIcon_8); m_vtOutSel.append(ui->btnSel_8);
    m_vtOutIcon.append(ui->btnIcon_9); m_vtOutSel.append(ui->btnSel_9);
    m_vtOutIcon.append(ui->btnIcon_10);m_vtOutSel.append(ui->btnSel_10);
    m_vtOutIcon.append(ui->btnIcon_11);m_vtOutSel.append(ui->btnSel_11);
    m_vtOutIcon.append(ui->btnIcon_12);m_vtOutSel.append(ui->btnSel_12);
    m_vtOutIcon.append(ui->btnIcon_13);m_vtOutSel.append(ui->btnSel_13);
    m_vtOutIcon.append(ui->btnIcon_14);m_vtOutSel.append(ui->btnSel_14);
    m_vtOutIcon.append(ui->btnIcon_15);m_vtOutSel.append(ui->btnSel_15);
    m_vtOutIcon.append(ui->btnIcon_16);m_vtOutSel.append(ui->btnSel_16);
}

void dialog_sasang_output::Init(ST_SSMOTION_DATA data)
{
    m_DataNew = data;
    m_DataOld = data;

}

void dialog_sasang_output::Get(ST_SSMOTION_DATA &data)
{
    data = m_DataNew;
}

void dialog_sasang_output::Update()
{
    Display_Data(m_DataNew);
}

void dialog_sasang_output::onTimer()
{
    Display_SaveEnable();
}

void dialog_sasang_output::onOutput()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtOutPic.indexOf(sel);
    if(index < 0) return;

    m_DataNew.speed_output = m_vtOutputNum[index];
    Display_Data(m_DataNew);
}

void dialog_sasang_output::onType()
{
    QLabel4* sel = (QLabel4*)sender();
    int index = m_vtType.indexOf(sel);
    if(index < 0) return;

    m_DataNew.type = (float)index;
    Display_Data(m_DataNew);
}
void dialog_sasang_output::onTime()
{
    dialog_numpad* np = (dialog_numpad*)gGetDialog(DIG_NUMPAD);

    np->m_numpad->SetTitle(ui->tbTimeTitle->text());
    np->m_numpad->SetNum(m_DataNew.time);
    np->m_numpad->SetMinValue(MIN_TIME_SET);
    np->m_numpad->SetMaxValue(MAX_TIME_SET);
    np->m_numpad->SetSosuNum(SOSU_TIME);

    if(np->exec() == QDialog::Accepted)
    {
        m_DataNew.time = (float)np->m_numpad->GetNumDouble();
        Display_Data(m_DataNew);
    }
}

void dialog_sasang_output::Display_Data(ST_SSMOTION_DATA data)
{
    if(Sasang->Motion->IsMoveCommand(data.command))
        return;

    int command = Sasang->Motion->IndexOf(data.command);
    if(command < 0)
        return;

    bool bOutput=false, bType=false, bTime=false;

    switch (command)
    {
    case HySasang_Motion::out:
        bOutput=true;
        bType=true;

        if(data.type == HyUserData::TYPE_PULSE)
        {
            bTime=true;
            Display_Time(data.time);
        }

        Display_Output(data.speed_output);
        Display_Type(data.type);
        break;

    case HySasang_Motion::wtt:
        bTime=true;
        Display_Time(data.time);
        break;
    }

    ui->wigOutput->setEnabled(bOutput);
    ui->wigType->setEnabled(bType);
    ui->wigTime->setEnabled(bTime);
}

void dialog_sasang_output::Display_Output(float output)
{
    for(int i=0;i<m_vtOutSel.count();i++)
    {
        if(m_vtOutputNum[i] == (int)output)
            m_vtOutSel[i]->show();
        else
            m_vtOutSel[i]->hide();
    }
}
void dialog_sasang_output::Display_Time(float time)
{
    QString str;
    str.sprintf(FORMAT_TIME, time);
    ui->tbTime->setText(str);
    ui->tbTimeUnit->setText(tr("sec"));

    ui->wigTime->setEnabled(true);
}
void dialog_sasang_output::Display_Type(float type)
{
    for(int i=0;i<m_vtType.count();i++)
        m_vtType[i]->setAutoFillBackground((int)type == i);
}

bool dialog_sasang_output::IsSame(ST_SSMOTION_DATA old_data, ST_SSMOTION_DATA new_data)
{
    int i;

    if(old_data.command != new_data.command)
        return false;
    for(i=0;i<3;i++)
    {
        if(old_data.trans[0].p[i] != new_data.trans[0].p[i])
            return false;
        if(old_data.trans[1].p[i] != new_data.trans[1].p[i])
            return false;

        if(old_data.trans[0].eu[i] != new_data.trans[0].eu[i])
            return false;
        if(old_data.trans[1].eu[i] != new_data.trans[1].eu[i])
            return false;
    }

    for(i=0;i<USE_AXIS_NUM;i++)
    {
        if(old_data.joint[0].joint[i] != new_data.joint[0].joint[i])
            return false;
        if(old_data.joint[1].joint[i] != new_data.joint[1].joint[i])
            return false;
    }

    if(old_data.speed_output != new_data.speed_output)
        return false;
    if(old_data.accuracy_input != new_data.accuracy_input)
        return false;
    if(old_data.type != new_data.type)
        return false;
    if(old_data.time != new_data.time)
        return false;

    return true;
}
void dialog_sasang_output::Display_SaveEnable()
{
    bool bSame = IsSame(m_DataOld, m_DataNew);

    ui->wigOK->setEnabled(!bSame);
}
